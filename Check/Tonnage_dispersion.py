'''
Created on 15 october 2020
@author: lgonzalez, cplumejeaud
ANR PORTIC : used to check the tonnage dispersion values for a same ship
This requires :
1. to have loaded data (pointcall, taxes, cargo) from navigo with LoadFilemaker.py. In schema navigo, navigocheck.
2. to have build a table ports.port_points listing all ports ( using geo_general ) with additional data and manual editing for admiralty, province.
3. to have build a table navigoviz.pointcall listing all pointcalls  with additional data and manual editing for admiralty, province.

'''

# Librairies
import pandas as pd
import seaborn as sns
import xlwt
import logging
import configparser
import pandas.io.sql as sql
from sqlalchemy import create_engine


class CheckTonnage(object):

    def __init__(self, config):
        """
        Ouvre les fichiers de log et une connexion à la base de données (en fonction des paramètres de config)
        """
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('CheckTonnage')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

    def get_clean_data_by_deleting_unique_values(self, data, grouped, variable):
        to_delete = data.groupby(grouped)[variable].nunique()==1
        to_delete_index = to_delete[to_delete].index.tolist()
        return data[~data.ship_id.isin(to_delete_index)]

    def loadDataFromCsv(self, config):
        filename = config['input']['file_name']
        # Load data 
        data = pd.read_csv(filename, sep="\t") #"data_1787_e.csv"

        ## Convert tonnage
        data["tonnage"] = data[["tonnage","tonnage_unit"]].apply(lambda x: x["tonnage"]/24 if (x["tonnage_unit"]=="quintaux" or x["tonnage_unit"]=="Quintaux")  else x["tonnage"], axis=1)
        #62667 records (deleting na tonnage )
        data = data.dropna(subset=["tonnage"])

        ### EDA

        # 61504 en G5 et 1163 en Marseille
        data.groupby("source_suite")["tonnage"].count()

        #  datablock_leader_marker = A, 31636 records
        #data_m = data.loc[:, ["source_doc_id","ship_id","data_block_leader_marker", "tonnage"]].query('data_block_leader_marker=="A"')
        data_m = data.loc[:, ["source_doc_id","ship_id", "tonnage", "source_main_port_toponyme"]]
        print(list(data_m.columns) )
        return data_m

    def loadDataFromDB(self, config):
        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')

        engine = create_engine('postgresql://'+user+':'+password+'@'+host+':'+port+'/'+dbname)
        print(engine)
        query = """select source_doc_id, source_suite, ship_id, tonnage::float, tonnage_unit, tonnage_uncertainity, source_main_port_toponyme 
            from navigoviz.pointcall where tonnage_uncertainity <= 0"""
        #On ne veut pas les données déjà contrôlées (code 1 ou 2)

        data = sql.read_sql_query(query, engine)

        ## Convert tonnage
        data["tonnage"] = data[["tonnage","tonnage_unit"]].apply(lambda x: x["tonnage"]/24 if (x["tonnage_unit"]=="quintaux" or x["tonnage_unit"]=="Quintaux" or x["tonnage_unit"]=="quintaux]")  else x["tonnage"], axis=1)
        #62667 records (deleting na tonnage )
        data = data.dropna(subset=["tonnage"])

        ### EDA

        # 61504 en G5 et 1163 en Marseille
        print(data.groupby("source_suite")["tonnage"].count())

        #  datablock_leader_marker = A, 31636 records
        #data_m = data.loc[:, ["source_doc_id","ship_id","data_block_leader_marker", "tonnage"]].query('data_block_leader_marker=="A"')
        data_m = data.loc[:, ["source_doc_id","ship_id", "tonnage", "source_main_port_toponyme"]]
        print(list(data_m.columns) )
        return data_m

    def unicityAnalysis(self, data_m):
        # 9954 different ships 
        data_m.loc[:,"ship_id"].nunique()

        # Voir si ship_tonnage pour même ship_id coïncide
        # if 1 <-> ok,  >1 <-> There's not coincidence
        data_m.groupby("ship_id")["tonnage"].nunique()
        # Exemple: data_m.loc[data_m.ship_id == "0000004N",:] Output: 2 records -> 1) tonnage: 55 2) tonnage 50
        sum(data_m.groupby("ship_id")["tonnage"].nunique()==1)
        #Conclusion: There are only 8180 (out 12001) ship_id where tonnage is the same

    def divergenceAnalysis(self, data_m, viz=False):

        ### PROCESS
        #1 and 2. Delete ship_id with one only source_doc_id
        #5447 ship_id to delete
        sum(data_m.groupby("ship_id")["source_doc_id"].nunique()==1)
        #26189 rows
        data_m = self.get_clean_data_by_deleting_unique_values(data_m, "ship_id", "source_doc_id")

        #3 and 4. Delete ship_id which tonnage is the same
        #2733 ship_id to delete
        sum(data_m.groupby("ship_id")["tonnage"].nunique()==1)
        #12010 rows
        data_m = self.get_clean_data_by_deleting_unique_values(data_m, "ship_id", "tonnage")


        #5. Summary table ordering by count and std of all 1774 ship_id
        groups_ships_id = data_m.groupby("ship_id")["tonnage"]
        table_ships = groups_ships_id.describe().sort_values(["count", "std"], ascending=False)
        #Conclusion: 1774 ship_id 
        table_ships['dispersion'] = table_ships['std']/table_ships['mean']*100
        #(table_ships['dispersion'] > 30 and table_ships['dispersion'] < 40).sum() 
        #With the help of count and std we can have a very close idea of the phenomen
        # Define the porcentages (10%, 20%, ...) can be mean different if we have only two or more than 30 values

        if viz:
            ## Visualization: It's important to define (by default: 30) the quantity of data points would be enough to plot
            sum(data_m.groupby("ship_id")["tonnage"].count()>30)
            data_to_plot = data_m.groupby("ship_id")["tonnage"].count()>30
            data_to_plot_index = data_to_plot[data_to_plot].index.tolist()
            data_to_plot = data_m[data_m.ship_id.isin(data_to_plot_index)]
            #sns.boxplot(data_to_plot.ship_id,data_to_plot.tonnage)
            sns.boxplot(data = data_to_plot, x="ship_id", y="tonnage")

        #6. Ships_id which their tonnages are almost the same (less than 5 ton)
        #Using range <-> max() - min()
        ship_id_ton_correct = groups_ships_id.apply(lambda x: (max(x)-min(x))<5)

        table_ships['ok_less_5'] = ship_id_ton_correct

        table_ships.sort_values(["dispersion", "ok_less_5"], ascending=False)

        #429 ship_id
        sum(ship_id_ton_correct)
        ship_id_ton_correct_index = ship_id_ton_correct[ship_id_ton_correct].index.tolist()
        if viz:
            print("The following ship_id have their tonnages almost negligables (less than 5 tons among them) {}".format(ship_id_ton_correct_index))
            #Sample 
            data_to_plot = data_m[data_m.ship_id.isin(ship_id_ton_correct_index[100:120])]
            sns.boxplot(data_to_plot.ship_id,data_to_plot.tonnage)

        #7. Add a code_erreur column : 0 pas grave, 1 et 2 moyens, 3 grave
        table_ships = table_ships.reset_index()
        #table_ships["maximum"] = table_ships["max"]
        table_ships = table_ships.rename(columns={"max": "maximum"})
        table_ships["error_code"] = ""
        #print('----------------------------------------------------------------')
        #print(list(table_ships.columns))
        #print(table_ships.maximum[0])
        #print(table_ships.dispersion[0])


        for i in range(len(table_ships)):
            if(table_ships.dispersion[i]>50 and table_ships.maximum[i] > 20):
                table_ships.error_code[i]= '3'
            elif(table_ships.dispersion[i]<20):
                if(table_ships.ok_less_5[i] ==True):
                    table_ships.error_code[i]= '0'
                else :
                    table_ships.error_code[i]= '1'
            else: #entre 20 et 50
                if(table_ships.ok_less_5[i] ==True):
                    table_ships.error_code[i]= '0'
                else :
                    table_ships.error_code[i]= '2'
        
        return table_ships

    def outputAnalysis(self, config, table_ships):
        filename = config['output']['file_name']
        sheetname = config['output']['sheet_name']

        # Excel file
        
        table_ships = table_ships.astype(str)
        style0 = xlwt.easyxf('font: color-index black')
        style1 = xlwt.easyxf('font: color-index black;pattern: pattern solid, fore_color yellow')
        style2 = xlwt.easyxf('font: color-index black; pattern: pattern solid, fore_color orange')
        style3 = xlwt.easyxf('font: color-index black;pattern: pattern solid, fore_color red')

        wb = xlwt.Workbook()

        #ws1 = wb.add_sheet('tonnage_classification')
        ws1 = wb.add_sheet('sheetname')
        
        for k in range(len(table_ships.columns)) :
            ws1.write(0,k, table_ships.columns[k], style0)
        for i in range(len(table_ships)):
            for j in range(len(table_ships.columns)) :
                if (table_ships.error_code[i]=='1'):
                    sty = style0
                elif(table_ships.error_code[i]=='3'):
                    sty = style3 
                elif(table_ships.error_code[i]=="1"):
                    sty = style1
                elif(table_ships.error_code[i]=="2"): sty=style2
                #print(i)
                ws1.write(i+1,j, table_ships.iloc[i,j], sty)
        #wb.save('tonnage.xls')
        wb.save(filename)


if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_tonnage.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))   
    c = CheckTonnage(config)
    #data = c.loadDataFromCsv(config)
    data = c.loadDataFromDB(config)
    table_ships = c.divergenceAnalysis(data)
    c.outputAnalysis(config, table_ships)

