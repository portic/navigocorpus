DROP TABLE public.csv_extract;

 create table public.csv_extract as (
 select 1789 as annee, pkid, record_id,
 source_doc_id, source_suite, source_component, source_main_port_uhgs_id, source_main_port_toponyme,
 pointcall_rankfull, data_block_leader_marker, pointcall_action, pointcall_function, pointcall_out_date, pointcall_in_date, outdate_fixed, indate_fixed, net_route_marker, navigo_status,
 pointcall, pointcall_uhgs_id, latitude, longitude, pointcall_admiralty, pointcall_province, shiparea,
 -- pointcall_states, pointcall_substates, 
 pointcall_status, p.source_1789_available as source_available, 
 homeport_uhgs_id , homeport,  homeport_latitude, homeport_longitude, homeport_admiralty, homeport_province, homeport_states, homeport_substates, homeport_status, homeport_shiparea,
 ship_id , flag, ship_flag_id , ship_flag_standardized_fr, ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit ,
 captain_id, captain_name, birthplace, status, citizenship, in_crew,
 json_array_length(all_cargos::json) as nb_products, 
 commodity_permanent_coding, commodity_purpose, commodity_standardized, commodity_standardized_fr, quantity , 
 commodity_permanent_coding2, commodity_purpose2, commodity_standardized2, commodity_standardized2_fr, quantity2, 
 commodity_permanent_coding3, commodity_purpose3, commodity_standardized3, commodity_standardized3_fr, quantity3, 
 commodity_permanent_coding4, commodity_purpose4, commodity_standardized4, commodity_standardized4_fr, quantity4,
 json_array_length(all_taxes::json) as nb_taxes,
 tax_concept , q01, q02, q03, ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe,
 
 (all_taxes->>2)::json->>'tax_concept' as tax_concept2, (all_taxes->>2)::json->>'q01' as t2q01, (all_taxes->>2)::json->>'q02' as t2q02, (all_taxes->>2)::json->>'q03' as t2q03,
 ((case when (all_taxes->>2)::json->>'q01' is null then 0 else ((all_taxes->>2)::json->>'q01')::float*240 end) + (case when (all_taxes->>2)::json->>'q02' is null then 0 else ((all_taxes->>2)::json->>'q02')::float*12 end) + (case when (all_taxes->>2)::json->>'q03' is null then 0 else ((all_taxes->>2)::json->>'q03')::float end)) as taxe2,
 
 (all_taxes->>3)::json->>'tax_concept' as tax_concept3,
 ((case when (all_taxes->>3)::json->>'q01' is null then 0 else ((all_taxes->>3)::json->>'q01')::float*240 end) + (case when (all_taxes->>3)::json->>'q02' is null then 0 else ((all_taxes->>3)::json->>'q02')::float*12 end) + (case when (all_taxes->>3)::json->>'q03' is null then 0 else ((all_taxes->>3)::json->>'q03')::float end)) as taxe3,

 (all_taxes->>4)::json->>'tax_concept' as tax_concept4,
  ((case when (all_taxes->>4)::json->>'q01' is null then 0 else ((all_taxes->>4)::json->>'q01')::float*240 end) + (case when (all_taxes->>4)::json->>'q02' is null then 0 else ((all_taxes->>4)::json->>'q02')::float*12 end) + (case when (all_taxes->>4)::json->>'q03' is null then 0 else ((all_taxes->>4)::json->>'q03')::float end)) as taxe4,

 (all_taxes->>5)::json->>'tax_concept' as tax_concept5,
  ((case when (all_taxes->>5)::json->>'q01' is null then 0 else ((all_taxes->>5)::json->>'q01')::float*240 end) + (case when (all_taxes->>5)::json->>'q02' is null then 0 else ((all_taxes->>5)::json->>'q02')::float*12 end) + (case when (all_taxes->>5)::json->>'q03' is null then 0 else ((all_taxes->>5)::json->>'q03')::float end)) as taxe5,

 ship_uncertainity, tonnage_uncertainity, flag_uncertainity, homeport_uncertainity, pointcall_uncertainity, captain_uncertainity, cargo_uncertainity, taxe_uncertainity,
 state_1789_fr as state_1789, homeport_state_1789_fr as homeport_state_1789, 
 substate_1789_fr, homeport_substate_1789_fr , 
 ferme_bureau , ferme_bureau_uncertainty , ferme_direction , 
 partner_balance_1789 , partner_balance_1789_uncertainty , partner_balance_supp_1789 , partner_balance_supp_1789_uncertainty ,
 homeport_ferme_bureau , homeport_ferme_bureau_uncertainty , homeport_ferme_direction ,
 homeport_partner_balance_1789 , homeport_partner_balance_1789_uncertainty , homeport_partner_balance_supp_1789 , homeport_partner_balance_supp_1789_uncertainty,  
 toponyme_fr , homeport_toponyme_fr ,  source_subset 
 from navigoviz.pointcall p 
 where  (substring(pointcall_out_date for 4)::int = 1789 OR substring(pointcall_in_date for 4)::int= 1789)
 )
 -- 31278 en v5 --> 32589 en V6
 
insert into public.csv_extract  (
 select 1787 as annee, pkid, record_id,
 source_doc_id, source_suite, source_component, source_main_port_uhgs_id, source_main_port_toponyme,
 pointcall_rankfull, data_block_leader_marker, pointcall_action, pointcall_function, pointcall_out_date, pointcall_in_date, outdate_fixed, indate_fixed, net_route_marker, navigo_status,
 pointcall, pointcall_uhgs_id, latitude, longitude, pointcall_admiralty, pointcall_province, shiparea,
 -- pointcall_states, pointcall_substates, 
 pointcall_status, p.source_1789_available as source_available, 
 homeport_uhgs_id , homeport,  homeport_latitude, homeport_longitude, homeport_admiralty, homeport_province, homeport_states, homeport_substates, homeport_status, homeport_shiparea,
 ship_id , flag, ship_flag_id , ship_flag_standardized_fr, ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit ,
 captain_id, captain_name, birthplace, status, citizenship, in_crew,
 json_array_length(all_cargos::json) as nb_products, 
 commodity_permanent_coding, commodity_purpose, commodity_standardized, commodity_standardized_fr, quantity , 
 commodity_permanent_coding2, commodity_purpose2, commodity_standardized2, commodity_standardized2_fr, quantity2, 
 commodity_permanent_coding3, commodity_purpose3, commodity_standardized3, commodity_standardized3_fr, quantity3, 
 commodity_permanent_coding4, commodity_purpose4, commodity_standardized4, commodity_standardized4_fr, quantity4,
 json_array_length(all_taxes::json) as nb_taxes,
 tax_concept , q01, q02, q03, ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe,
 (all_taxes->>2)::json->>'tax_concept' as tax_concept2, (all_taxes->>2)::json->>'q01' as t2q01, (all_taxes->>2)::json->>'q02' as t2q02, (all_taxes->>2)::json->>'q03' as t2q03,
 ((case when (all_taxes->>2)::json->>'q01' is null then 0 else ((all_taxes->>2)::json->>'q01')::float*240 end) + (case when (all_taxes->>2)::json->>'q02' is null then 0 else ((all_taxes->>2)::json->>'q02')::float*12 end) + (case when (all_taxes->>2)::json->>'q03' is null then 0 else ((all_taxes->>2)::json->>'q03')::float end)) as taxe2,
  
 (all_taxes->>3)::json->>'tax_concept' as tax_concept3,
 ((case when (all_taxes->>3)::json->>'q01' is null then 0 else ((all_taxes->>3)::json->>'q01')::float*240 end) + (case when (all_taxes->>3)::json->>'q02' is null then 0 else ((all_taxes->>3)::json->>'q02')::float*12 end) + (case when (all_taxes->>3)::json->>'q03' is null then 0 else ((all_taxes->>3)::json->>'q03')::float end)) as taxe3,

 (all_taxes->>4)::json->>'tax_concept' as tax_concept4,
  ((case when (all_taxes->>4)::json->>'q01' is null then 0 else ((all_taxes->>4)::json->>'q01')::float*240 end) + (case when (all_taxes->>4)::json->>'q02' is null then 0 else ((all_taxes->>4)::json->>'q02')::float*12 end) + (case when (all_taxes->>4)::json->>'q03' is null then 0 else ((all_taxes->>4)::json->>'q03')::float end)) as taxe4,

 (all_taxes->>5)::json->>'tax_concept' as tax_concept5,
  ((case when (all_taxes->>5)::json->>'q01' is null then 0 else ((all_taxes->>5)::json->>'q01')::float*240 end) + (case when (all_taxes->>5)::json->>'q02' is null then 0 else ((all_taxes->>5)::json->>'q02')::float*12 end) + (case when (all_taxes->>5)::json->>'q03' is null then 0 else ((all_taxes->>5)::json->>'q03')::float end)) as taxe5,

 ship_uncertainity, tonnage_uncertainity, flag_uncertainity, homeport_uncertainity, pointcall_uncertainity, captain_uncertainity, cargo_uncertainity, taxe_uncertainity,
 state_1789_fr as state_1789, homeport_state_1789_fr as homeport_state_1789,
 substate_1789_fr, homeport_substate_1789_fr , 
 ferme_bureau , ferme_bureau_uncertainty , ferme_direction , 
 partner_balance_1789 , partner_balance_1789_uncertainty , partner_balance_supp_1789 , partner_balance_supp_1789_uncertainty ,
 homeport_ferme_bureau , homeport_ferme_bureau_uncertainty , homeport_ferme_direction ,
 homeport_partner_balance_1789 , homeport_partner_balance_1789_uncertainty , homeport_partner_balance_supp_1789 , homeport_partner_balance_supp_1789_uncertainty,  
 toponyme_fr , homeport_toponyme_fr ,  source_subset 
 from navigoviz.pointcall p 
 where  (substring(pointcall_out_date for 4)::int = 1787 OR substring(pointcall_in_date for 4)::int= 1787)
 )
 -- 75815 en v5 --> 75941 en v6

grant select on public.csv_extract to api_user;
grant select on public.csv_extract to porticapi;
 
-- Ajout de la classe du navire après recodage automatique 
alter table public.csv_extract add column if not exists ship_class_codage  text;
select pkid, ship_class,pscc.ship_class_codage  from public."public.ship_class_codage" pscc where ship_class is not null ;
update  public.csv_extract p set ship_class_codage = pscc.ship_class_codage from public."public.ship_class_codage" pscc where p.pkid = pscc.pkid;

-- Fabrication du fichier des alignement flag-homeport

--------------------------
-- 4 mars 2021 : alignement états des homeports et pavillons (flags)
--------------------------

/* select distinct homeport_state_1789 , homeport_uhgs_id , count(homeport_state_1789), ship_flag_id, ship_flag_standardized_fr  as c
 from public.csv_extract
 where homeport_state_1789 is not null
 group by homeport_state_1789, homeport_uhgs_id, ship_flag_id, ship_flag_standardized_fr
 order by c desc */
 
 insert into ports.generiques_inclusions_geo_csv (toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria, done)
 select distinct  ship_flag_standardized_fr, ship_flag_id, homeport_uhgs_id, homeport_state_1789, 'alignement  flags - homeports (états correspondant) ', count(homeport_state_1789) as c
 from public.csv_extract
 where homeport_state_1789 is not null 
 and ship_flag_standardized_fr = 'français' and homeport_state_1789 = 'France'
 group by homeport_state_1789, homeport_uhgs_id, ship_flag_id, ship_flag_standardized_fr;
 
insert into ports.generiques_inclusions_geo_csv (toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria, done)
  select distinct  ship_flag_standardized_fr, ship_flag_id, homeport_uhgs_id, homeport_state_1789, 'alignement  flags - homeports (états correspondant) ', count(homeport_state_1789) as c
 from public.csv_extract
 where homeport_state_1789 is not null 
 and (ship_flag_standardized_fr != 'français' and homeport_state_1789 != 'France')
 group by homeport_state_1789, homeport_uhgs_id, ship_flag_id, ship_flag_standardized_fr;

-- corrections pour : 
-- Pays-Bas autrichien	A0049520	A0686629	Lubeck : lubeckois
-- Pays-Bas autrichien	A0049520	A0743522	Hambourg : hambourgeois
-- napolitain	A1964382	A0251559	Principauté de Piombino
-- hollandais	A0617755	A0642340	Prusse : prussien
-- britannique	A0395415	B0000050	Etats-Unis d'Amérique : etatsunien
-- britannique	A0395415	A0617755	Provinces-Unies : hollandais

 delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and toponyme_sup='Pays-Bas autrichien' and toponyme in ('Lubeck', 'Hambourg');
 
 delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='A0642340' and toponyme_sup = 'hollandais';
 
 delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='B0000050' and toponyme_sup = 'britannique';
 
  delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='A0617755' and toponyme_sup = 'britannique';
  
 update ports.generiques_inclusions_geo_csv set toponyme_sup = 'Piombino', ughs_id_sup = null 
 where ughs_id_sup = 'A1964382' and ughs_id = 'A0251559' and toponyme = 'Principauté de Piombino' and criteria like 'alignement%';

-- export CSV pour config du programme
select distinct toponyme_sup as ship_flag_standardized_fr, toponyme as homeport_state_fr 
from ports.generiques_inclusions_geo_csv 
 where criteria like 'alignement%'

----------------------------------------
-- 8 mars 2021 : rajouts de nouveaux attributs pour les contrôles dans l'API
----------------------------------------

 alter table navigoviz.pointcall add column shipclass_uncertainity int;
alter table navigoviz.pointcall add column birthplace_uhgs_id_uncertainity int ;
alter table navigoviz.pointcall add column birthplace_uncertainity int;
alter table navigoviz.pointcall add column citizenship_uncertainity int;
alter table navigoviz.pointcall add column birthplace_uhgs_id text ;

update navigoviz.pointcall p set birthplace_uhgs_id=cp.captain_birthplace_id, 
shipclass_uncertainity=up.ship_class,
birthplace_uhgs_id_uncertainity = up.captain_birthplace_id, 
birthplace_uncertainity = up.captain_birthplace,
citizenship_uncertainity = up.captain_citizenship
from navigocheck.uncertainity_pointcall up , navigocheck.check_pointcall cp 
where p.pkid = up.pkid and cp.pkid = p.pkid

select p.pkid, cp.captain_birthplace_id as birthplace_uhgs_id, up.ship_class as shipclass_uncertainity, 
up.captain_birthplace_id as birthplace_uhgs_id_uncertainity, 
up.captain_birthplace as birthplace_uncertainity, up.captain_citizenship as citizenship_uncertainity 
from navigoviz.pointcall p, navigocheck.uncertainity_pointcall up , navigocheck.check_pointcall cp 
where p.pkid = up.pkid and cp.pkid = p.pkid


alter table public.csv_extract add column if not exists shipclass_uncertainity  int;
alter table public.csv_extract add column if not exists birthplace_uhgs_id_uncertainity  int;
alter table public.csv_extract add column if not exists birthplace_uncertainity  int;
alter table public.csv_extract add column if not exists citizenship_uncertainity  int;
alter table public.csv_extract add column if not exists birthplace_uhgs_id  text;

update public.csv_extract c set shipclass_uncertainity=p.shipclass_uncertainity,
birthplace_uhgs_id_uncertainity = p.birthplace_uhgs_id_uncertainity,
birthplace_uncertainity = p.birthplace_uncertainity,
citizenship_uncertainity = p.citizenship_uncertainity,
birthplace_uhgs_id = p.birthplace_uhgs_id
from navigoviz.pointcall p
where c.pkid = p.pkid