'''
Created on 20 janvier 2022
@author:  cplumejeaud
ANR PORTIC : used to check the succession of pointcalls and associated Net_route_marker
This requires :
1. to have loaded data (pointcall, taxes, cargo) from navigo with LoadFilemaker.py. In schema navigo, navigocheck.
2. to have build a table ports.port_points listing all ports ( using geo_general ) with additional data and manual editing for admiralty, province.
3. to have build a table navigoviz.pointcall listing all pointcalls  with additional data and manual editing for admiralty, province.

Version avec contrôle possible sur toute la base. 
Une table public.verif et une table navigoviz.test sont créées par le programme.
- navigoviz.test contient les suites ordonnées des points testés, 
- public.vérif contient un sous-ensemble  de test correspondant à ceux qui ont pu être vérifiés (les points entre O et O).
Les navires vus dans un seul document source ne peuvent donc pas être contrôlés, ni les premiers points avant O d'une suite, ni les derniers. 

'''

# Librairies
from queue import Empty
import pandas as pd
import xlsxwriter as xlwt
import logging
import configparser
import pandas.io.sql as sql
from sqlalchemy import create_engine # installer en python 3.9.10


class CheckPointcalls(object):

    def __init__(self, config):
        """
        Ouvre les fichiers de log et une connexion à la base de données (en fonction des paramètres de config)
        """
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('CheckPointcalls itineraries')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')

        self.engine = create_engine('postgresql://'+user+':'+password+'@'+host+':'+port+'/'+dbname)
        print(self.engine)


        self.start_seq = 0
        self.end_seq = 0
        self.current_ship_id = ''
        self.current_doc_id = None
        self.previousDate = pd.to_datetime('1787-01-01')
        #self.current_ship_id = None
        #self.current_doc_id = None

        #Liste des colonnes du dataframe qui doit correspondre à la requête qui crée la table navigoviz.test de loadDataFromDB
        self.data_columns = [ "ship_id", "pointcall_rankfull",  "pointcall_uhgs_id", "pointcall", "pointcall_function", "net_route_marker", "pointcall_out_date" , "pointcall_in_date", 
            "pointcall_action", "has_input_done", "pointcall_admiralty", "pointcall_province", "navigo_status", "date_precise", "date_fixed", "source_suite", "tonnage", "list_sup_uhgs_id", "list_inf_uhgs_id", "record_id",  "source_doc_id", "pkid", "pointcall_rank_dedieu" ]

    def get_clean_data_by_deleting_unique_values(self, data, grouped, variable):
        to_delete = data.groupby(grouped)[variable].nunique()==1
        to_delete_index = to_delete[to_delete].index.tolist()
        return data[~data.ship_id.isin(to_delete_index)]


    def loadDataFromDB(self, config):
        table = config.get('itineraries_input', 'table_name')

        query = """drop table if exists %s"""%(table)
        sql.execute(query, self.engine)


        filter = config.get('itineraries_input', 'filter')
        query = """ create table %s as (
            select pkid, ship_id , pointcall_rank_dedieu, p.pointcall_rankfull, record_id, source_suite, source_doc_id , pointcall_function,
            pointcall,  p.pointcall_uhgs_id , p.pointcall_admiralty,  p.pointcall_province, ship_name, tonnage::float, tonnage_unit, p.navigo_status , p.pointcall_out_date , p.pointcall_in_date , 
            pointcall_action ,  net_route_marker ,data_block_leader_marker, pointcall_uncertainity, 
            (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) as date_precise,
            list_sup_uhgs_id  ,list_inf_uhgs_id, date_fixed,
            (case when p.state_1789_fr = 'France' then 
                (case when p.source_suite != 'G5' then 
                    true 
                else 
                    (case when extract(year from coalesce(p.outdate_fixed, p.indate_fixed)) = 1787 then 
                        nb_conges_1787_inputdone is not null and p.nb_conges_1787_inputdone > 0 
                    else p.nb_conges_1789_inputdone is not null and p.nb_conges_1789_inputdone > 0 
                    end)
                end)
                else false
                end) as has_input_done
            from navigoviz.pointcall p left join 

            (select ughs_id, array_agg(ughs_id_sup) as list_sup_uhgs_id 
            -- from ports.generiques_inclusions_geo_csv gigc 
            from ports.generiques_geoinclusions gigc
            group by ughs_id) as k on k.ughs_id = p.pointcall_uhgs_id

            left join 
            (select ughs_id_sup, array_agg(ughs_id) as list_inf_uhgs_id 
            -- from ports.generiques_inclusions_geo_csv gigc 
            from ports.generiques_geoinclusions gigc
            group by ughs_id_sup) as k2 on k2.ughs_id_sup = p.pointcall_uhgs_id

            where -- p.ship_id = '0014650N' --'0000447N' -- '0000305N' / -- '0014815N' -- 0012800N
            -- p.source_doc_id in ('00332297', '00310958', '00307963', '00352540') or
            -- ship_id in ( '0014650N', '0000011N', '0012406N', '0002344N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
            ship_id is not null and (net_route_marker is null or net_route_marker != 'Q' ) -- and ship_id >= '0000388N'
             %s
            order by ship_id, pointcall_rankfull
            )
        """% (table, filter)
        print(query)
        sql.execute(query, self.engine)

        query = """select * from %s """% (table)

        data = sql.read_sql_query(query, self.engine)

        ## Convert tonnage
        data["tonnage"] = data[["tonnage","tonnage_unit"]].apply(lambda x: x["tonnage"]/24 if (x["tonnage_unit"]=="quintaux" or x["tonnage_unit"]=="Quintaux" or x["tonnage_unit"]=="quintaux]")  else x["tonnage"], axis=1)
        #62667 records (deleting na ship_id )
        #data = data.dropna(subset=["ship_id"])

        # Nombre de lignes par ship_id
        #print(data.groupby("ship_id")["ship_id"].count())

        #  datablock_leader_marker = A, 31636 records
        #data_m = data.loc[:, ["source_doc_id","ship_id","data_block_leader_marker", "tonnage"]].query('data_block_leader_marker=="A"')
        data_m = data.loc[:, self.data_columns]
        print(list(data_m.columns) )
        print(data_m.shape)
        print(data_m.shape[0])

        return data_m

    def take_a_sequence(self, data_m, ship_id = None):
        """
        Extract a sequence of ports to process : marche très bien sur les ship_id 
        end_seq : current index for last item in the suite ; increment it untill the ship is a new one or a new point O is found or the delta in time > 365
        start_seq : current index for first item in the suite
        current_ship_id : current ship_id we are processing in the sequence
        current_doc_id : current source_doc_id we will be processing after that (if there is, else it is None)
        return: a small dataframe of pointcalls between O and O for same current_ship_id. Or None if we are changing of ship_id. 
        Note : très mal codé, à refaire
        """

        if ship_id != None : 
            index = data_m['ship_id'].loc[lambda x: x==ship_id].index
            print("searching for a ship "+ship_id)
            print(index[0])
            self.end_seq = index[0]+1
            self.start_seq = index[0]
            self.current_ship_id = ship_id
            
        
        if self.end_seq > 0 : 
            self.start_seq = self.end_seq - 1
        self.current_ship_id = data_m.iloc[[self.end_seq]]["ship_id"].item()
        self.previousDate = pd.to_datetime(data_m.iloc[[self.start_seq]]["date_fixed"].item())

        duration=(pd.to_datetime(data_m.iloc[[self.end_seq]]["date_fixed"].item())-self.previousDate).days
        if (self.current_ship_id is not None) : 
            #print("PROCESSING SHIP "+self.current_ship_id)
            while (self.end_seq < data_m.shape[0] and data_m.iloc[[self.end_seq]]["pointcall_function"].item() != 'O' and duration < 365 ) : 
                #print (data_m.iloc[[self.end_seq]]["pointcall_function"].item())
                self.end_seq = self.end_seq + 1
                if self.end_seq < data_m.shape[0] : 
                    duration=(pd.to_datetime(data_m.iloc[[self.end_seq]]["date_fixed"].item())-self.previousDate).days
                    self.previousDate = pd.to_datetime(data_m.iloc[[self.end_seq-1]]["date_fixed"].item())
            # On avance pour la séquence suivante
            if self.end_seq < data_m.shape[0] : 
                # Le dernier point est à garder, c'est un O
                self.end_seq = self.end_seq + 1
        else : 
            # Fin de la liste des navires avec ship_id, on arrive sur des doc_id sans ship_id
            self.current_doc_id = data_m.iloc[[self.end_seq]]["source_doc_id"].item()

        
        if self.end_seq >= data_m.shape[0]:
            print("finished")
            ports = data_m.iloc[self.start_seq : self.end_seq-1]#-1 avant
            return ports.copy()
        elif self.current_ship_id is not None and data_m.iloc[[self.end_seq-1]]["ship_id"].item() != self.current_ship_id : 
            print("CHANGING ship_id : "+self.current_ship_id)
            self.current_ship_id = data_m.iloc[[self.end_seq]]["ship_id"].item()
            return None 
        elif self.current_ship_id is not None and duration > 365 : 
            #print("NEW YEAR for ship_id : "+self.current_ship_id)
            self.current_ship_id = data_m.iloc[[self.end_seq]]["ship_id"].item()
            return None 
        else :
            ports = data_m.iloc[self.start_seq : self.end_seq]
            return ports.copy()

    def take_a_document(self, data_m, doc_id = None):
        """
        Extract a sequence of ports to process having the same doc_id. 
        end_seq : current index for last item in the suite ; increment it untill the doc is a new one the suite is ended
        start_seq : current index for first item in the suite
        current_doc_id : current source_doc_id we are processing in the sequence
        return: a small dataframe of pointcalls having same source_doc_id (same document source). Or None if we are changing of current_doc_id. 
        """
        
        self.start_seq = self.end_seq 
        #print("STARTING DOC "+self.current_doc_id)
                
        ## Facon 2 : sur les documents
        while (self.end_seq < data_m.shape[0] and data_m.iloc[[self.end_seq]]["source_doc_id"].item() == self.current_doc_id) : 
            #print (data_m.iloc[[self.end_seq]]["pointcall_function"].item())
            self.end_seq = self.end_seq + 1

        # Le dernier document est atteint, signaler la fin de la recherche avec current_doc_id = None
        if self.end_seq >= data_m.shape[0]:
            print("finished")
            self.current_doc_id = None
        else: 
            #print("changing doc_id") 
            self.current_doc_id = data_m.iloc[[self.end_seq]]["source_doc_id"].item()
        #print("Doc from "+str(self.start_seq)+" :to "+str(self.end_seq  ))
        ports = data_m.iloc[self.start_seq : self.end_seq]
        return ports.copy()
        

    def initAlgo(self, port):
        port['certitude'] = "Déclaré"   
        port['certitude_reason'] = "Default"
        #port['net_route_marker'] = "A" 
        # # Je ne change pas le net_route_marker à l'init sinon je ne verrai pas ce que l'algo à changé vraiment par rapport à celui de Silvia.

        #print(port.query('date_precise == True').certitude.size)

        ## Préférence de Christine
        #port.loc[port.eval('date_precise == True & (source_suite != "G5")') , 'certitude'] = "Observé"

        ## Préférence de Silvia
        #port.loc[port['navigo_status'].str.contains("PC-") , 'certitude'] = "Observé"
        #port.loc[port['navigo_status'].str.contains("PC-") , 'certitude_reason'] = "navigo_status is PC like"

        #port.loc[port['navigo_status'].str.contains("PU-") , 'certitude'] = "Confirmé"
        #port.loc[port['navigo_status'].str.contains("PU-") , 'certitude_reason'] = "navigo_status is PU like : il n'est jamais allé là"
        #port.loc[port['navigo_status'].str.contains("PU-") , 'net_route_marker'] = "Z"
        #print(port)  

    def passage1Algo(self, ports):
        i = 0
        previousDate = pd.to_datetime(ports.iloc[[0]].date_fixed.item())
        duration = -1
        etapes = pd.DataFrame(columns=self.data_columns.append(["certitude", "certitude_reason"]))
        for i in range(0, ports.shape[0]) : 
            porti = ports.iloc[[i]].copy()
            if i>0 : 
                duration = (pd.to_datetime(ports.iloc[[i]].date_fixed.item()) - previousDate).days
                #print('################### iterate on '+str(i))
                #print('duration '+str(ports.iloc[[i]].date_fixed.item())+' -> '+str(duration))
                previousDate = pd.to_datetime(ports.iloc[[0]].date_fixed.item())
            #print('################### iterate on '+str(i))
            #print(porti)
            if 'PC-RS' in porti.navigo_status.item() or 'PU-RS' in porti.navigo_status.item() or 'PC-ML' in porti.navigo_status.item() :  
                # PC sur Marseille : à garder par défaut. Ajout du 22 janvier 2023.
                # Sauf si dans la suite l'algo contredit (cas de Suzanne 12925N, Paris 131N, Actif 2454N) 
                # parce que Marseille parle d'un passé réalisé dans le G5 (souvent quand on vient de Dunkerque en passant par Gilbraltar)
                porti.net_route_marker = "A"
                porti.certitude = "Observé"
                porti.certitude_reason = "navigo_status is PC like"
            
            if porti.pointcall_function.item() == 'O' : 
                porti.net_route_marker = "A"
                porti.certitude = "Observé"
            elif 'PU' in porti.navigo_status.item():
                porti.net_route_marker = "Z"
                porti.certitude = "Confirmé"
                porti.certitude_reason = "navigo_status is PU like : il n'est jamais allé là"
            elif 'PC' in porti.navigo_status.item() and (porti.pointcall_function.item() is None or porti.net_route_marker.item() is None): 
                #cas très fréquent à Marseille sur les points intermédiaires
                porti.net_route_marker = "A"
                porti.certitude = "Observé"
                porti.certitude_reason = "navigo_status is PC like"
            elif ( porti.pointcall_function.item() == 'A' or porti.pointcall_function.item() == 'Z' or porti.pointcall_function.item() is None) : 
                #and 'PC' not in porti.navigo_status.item() sinon 0000068N, cas de la Charente ne marche pas ?
                uhgs_id = porti.pointcall_uhgs_id.item()
                amiraute = porti.pointcall_admiralty.item()
                #print ("################### SEARCHING in previous steps :  ")
                foundInPrevious  = False
                previousFunction = "Unknown"
                sameAmiralty = False
                for j in range(0, etapes.shape[0]) : 
                    if etapes.iloc[[j]].pointcall_uhgs_id.item() == uhgs_id or (etapes.iloc[[j]].pointcall_admiralty.item() is not None and etapes.iloc[[j]].pointcall_admiralty.item() == amiraute) or (etapes.iloc[[j]].list_sup_uhgs_id.item() is not None and uhgs_id in  etapes.iloc[[j]].list_sup_uhgs_id.item()):
                        foundInPrevious = True
                        previousFunction = etapes.iloc[[j]].pointcall_function.item()
                        if etapes.iloc[[j]].pointcall_admiralty.item() == amiraute:
                            sameAmiralty = True
                #print(foundInPrevious)
                if foundInPrevious :
                    if previousFunction == 'O' :
                        porti.net_route_marker = "Z"
                        porti.certitude = "Confirmé"
                        porti.certitude_reason = "Vu à un point O précédent : il vient bien de là"
                    else : 
                        porti.net_route_marker = "Z" #Je me demande si il faut pas tenir compte de si ce sont des points de la même amirauté et mettre A dans ce cas
                        porti.certitude = "Confirmé"
                        porti.certitude_reason = "Vu à un point précédent où il dit aller là"
                        if sameAmiralty : 
                            porti.net_route_marker = "A" #Cas de Libourne après Bordeaux  pour 0000684N
                            porti.certitude_reason = "Vu à un point précédent où il dit aller là, dans la même amirauté"
                else :
                    #not found
                    porti.net_route_marker = "A"   
                
            etapes = pd.concat([etapes,porti], ignore_index=True)
            #print(etapes)
        return etapes

    def passage2Algo(self, etapes):
        i = 0
        result = pd.DataFrame(columns=self.data_columns.append(["certitude", "certitude_reason"]))
        for i in range(0, etapes.shape[0]) : 
            porti = etapes.iloc[[i]].copy()
            #print('################### iterate on '+str(i))
            #print(porti)
            if porti.pointcall_function.item() == 'O':
                previousObservedAmiraute = porti.pointcall_admiralty.item()
                previousObservedUhgs_id = porti.pointcall_uhgs_id.item()
                #est-ce que ce sera un aller-retour dans la même amirauté ?
                #print(porti.pointcall +" <-- "+previousObservedAmiraute)
            if (porti.pointcall_function.item() == 'T' or porti.pointcall_function.item() == 'Z' or porti.pointcall_function.item() is None) and ('PU' not in porti.navigo_status.item()) and ('PC' not in porti.navigo_status.item()):  
                uhgs_id = porti.pointcall_uhgs_id.item()
                amiraute = porti.pointcall_admiralty.item()
                #print ("################### SEARCHING in next steps :  ")
                foundInNext  = False
                nextFunction = "Unknown"
                nextAdmiralty = False
                sameUHGS_id = False
                nextObservedUhgs_id = ''
                for j in range(i+1, etapes.shape[0]) : 
                    if etapes.iloc[[j]].pointcall_uhgs_id.item() == uhgs_id or  (etapes.iloc[[j]].pointcall_admiralty.item() is not None and etapes.iloc[[j]].pointcall_admiralty.item() == amiraute) or (etapes.iloc[[j]].list_sup_uhgs_id.item() is not None and uhgs_id in etapes.iloc[[j]].list_sup_uhgs_id.item()):
                        foundInNext = True
                        nextFunction = etapes.iloc[[j]].pointcall_function.item()
                        nextAdmiralty = etapes.iloc[[j]].pointcall_admiralty.item()
                        nextObservedUhgs_id = etapes.iloc[[j]].pointcall_uhgs_id.item()
                        #print(porti.pointcall +" --> "+nextAdmiralty)
                        if etapes.iloc[[j]].pointcall_uhgs_id.item() == uhgs_id : 
                            sameUHGS_id = True
                #print(foundInNext)
                if foundInNext :
                    if (porti.has_input_done.item() is False and sameUHGS_id is False and nextAdmiralty==amiraute)  : 
                        # garde les trajets internes à une même amirauté lorsque le point destination T (Hennebont) n'a pas de sorties mais est amirauté de Lorient. 
                        # comme Bayonne (O) - Hennebont (T) - Lorient (O) avec Hennebont.net_route_marker <- A et ship_id = 0004355N 
                        # comme :  Rochefort (O) - Ile de Ré (T) - Saint Martin de Ré (O) avec Ile de Ré.net_route_marker <- A et ship_id = 0002350N
                        porti.net_route_marker = "A" 
                        #Mais du coup Carteret avant Port-Bail devient aussi A pour le ship 0000063N
                        if nextFunction != 'O':
                            porti.certitude = "Confirmé"
                            porti.certitude_reason = "Vu à un point suivant dans la même amirauté où il dit venir de là "
                        elif nextFunction == 'O':                      
                            porti.certitude = "Confirmé"
                            porti.certitude_reason = "Vu à un point O suivant dans la même amirauté : il est bien allé là"
                    elif ( previousObservedAmiraute is not None and previousObservedAmiraute==nextAdmiralty and sameUHGS_id is False and nextObservedUhgs_id==previousObservedUhgs_id) : 
                        # garde les trajets internes allers-retours à une même amirauté 
                        # comme Aligre (O) - Esnandes (T) - Aligre (O) avec Esnandes.net_route_marker <- A et ship_id = 0012406N
                         
                        porti.net_route_marker = "A" 
                        porti.certitude_reason = "Vu à un point O suivant de la même amirauté (un aller-retour dans la même amirauté)"
                        porti.certitude = "Confirmé"
                    else : 
                        porti.net_route_marker = "Z" #cas de Marseille comme destination annoncée depuis Le Havre pour 0012925N. Et on trouve bien Marseille plus tard comme destination en O 
                        if nextFunction != 'O':
                            porti.certitude = "Confirmé"
                            porti.certitude_reason = "Vu à un point suivant où il dit venir de là "
                        elif nextFunction == 'O':                      
                            porti.certitude = "Confirmé"
                            porti.certitude_reason = "Vu à un point O suivant : il est bien allé là"
                else :
                    #not found
                    if porti.has_input_done.item() is True : 
                        #print("le port "+uhgs_id+" a des sources, il devrait etre la")
                        #or if porti est un truc générique comme "Ile de Ré"
                        porti.net_route_marker = "Z"
                        porti.certitude = "Infirmé" 
                        porti.certitude_reason = "Pas vu plus tard et le port a des sources : il devrait etre la"  
                    else : # Si durée plausible (//vérifier en fonction du tonnage, et de la distance à parcourir et faire vérifier à Silvia ensuite)
                        porti.net_route_marker = "A"
                        porti.certitude_reason = "plausible, mais reste à vérifier la durée du trajet"  
            result = pd.concat([result,porti], ignore_index=True)
            #print(result)
        return result

    def outputAnalysis(self, config, table_ships):
        """ output in a Excel file easy to read for Silvia """
        filename = config.get('itineraries_output', 'file_name')
        sheetname = config.get('itineraries_output', 'sheet_name')

        #config['output']['file_name']
        #config['output']['sheet_name']

        # Excel file
        wb = xlwt.Workbook(filename)

        table_ships = table_ships.astype(str)
        #print("outputAnalysis "+ str(table_ships.shape))
        #print("outputAnalysis "+ str(table_ships.columns))

        # add new colour to palette and set RGB colour value
        cell_format_black = wb.add_format()
        cell_format_orange = wb.add_format() #Cas identiques à l'analyse de Silvia
        cell_format_orange.set_bg_color('orange') #unused
        cell_format_yellow = wb.add_format()
        cell_format_yellow.set_bg_color('yellow')     #Inverse le Z marqueur par rapport à Silvia
        cell_format_grey = wb.add_format()
        cell_format_grey.set_bg_color('#A9A9A9') #Non contrôlés en gris
        cell_format_blue = wb.add_format()
        cell_format_blue.set_bg_color('#7FFFD4') #Silvia devra vérifier la durée du trajet car l'algo modifie le Z en A

        ws1 = wb.add_worksheet(sheetname)
        
        indexVerif = table_ships.columns.get_loc("verif");
        #print(indexVerif)
        #Ecrire le Header
        for k in range(len(table_ships.columns)) :
            ws1.write(0,k, table_ships.columns[k], cell_format_black)
        #Ecrire le contenu de la table
        for i in range(len(table_ships)):
            if (table_ships.iloc[i,indexVerif]=='idem'):
                sty = cell_format_black
            elif(table_ships.iloc[i,indexVerif]=='idem#certitude#'):
                sty = cell_format_black 
            elif(table_ships.iloc[i,indexVerif]=="#net_route_marker##certitude#"):
                sty = cell_format_yellow
            elif(table_ships.iloc[i,indexVerif]=="#net_route_marker#"): 
                sty=cell_format_blue
            elif(table_ships.iloc[i,indexVerif]=="None"): 
                sty=cell_format_grey

            for j in range(len(table_ships.columns)) :
                #print(i)
                ws1.write(i+1,j, table_ships.iloc[i,j], sty)
        wb.close()


if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_pointcall.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))   
    c = CheckPointcalls(config)
    data = c.loadDataFromDB(config)

    c.initAlgo(data)



    ## Init the compilation of results
    resultat = pd.DataFrame(columns=c.data_columns.append(["certitude", "certitude_reason", "verif"]))

    #print ("########### "+str(c.end_seq))
    ship_id =  None #'0000388N' # None #'0000684N' #'0000068N' 
    #ports = c.take_a_sequence(data, ship_id)
    
    while c.end_seq < data.shape[0] and c.current_doc_id is None:
        ports = c.take_a_sequence(data, ship_id)
        
        if ports is not None:
            etapes = c.passage1Algo(ports)
            result = c.passage2Algo(etapes)
            result['verif'] = "idem"  #Add a new column verif with values "idem"

            saveit = False
            indexNet = result.columns.get_loc("net_route_marker");
            indexCert = result.columns.get_loc("certitude");
            indexVerif = result.columns.get_loc("verif");

            for i in range(0, result.shape[0]) :
                if (result.iloc[i, indexNet] != ports.iloc[i, indexNet]):
                    #print('net_route_marker différent')
                    saveit = True
                    result.iloc[i, indexVerif]='#net_route_marker#'
                if (result.iloc[i, indexCert] != ports.iloc[i, indexCert]):
                    #print('cert différent')
                    result.iloc[i, indexVerif]=result.iloc[i, indexVerif]+'#certitude#'
                    saveit = True
            '''if saveit:
                #print(ports)
                #print(result)'''
            resultat = pd.concat([resultat, result], ignore_index=True)
            '''test = result["net_route_marker"].reset_index(drop=True).compare(ports["net_route_marker"].reset_index(drop=True))
            #Marche aussi, mais moins précis. Ne renseigne pas verif avec la nature de la différence
            '''
            #print ("______________________________ "+str(c.end_seq))


    while c.end_seq < data.shape[0] and c.current_doc_id is not None:
        ports = c.take_a_document(data)
        
        if ports is not None:
            etapes = c.passage1Algo(ports)
            result = c.passage2Algo(etapes)
            result['verif'] = "idem"  

            saveit = False
            indexNet = result.columns.get_loc("net_route_marker");
            indexCert = result.columns.get_loc("certitude");
            indexVerif = result.columns.get_loc("verif");

            for i in range(0, result.shape[0]) :
                if (result.iloc[i, indexNet] != ports.iloc[i, indexNet]):
                    #print('net_route_marker différent')
                    saveit = True
                    result.iloc[i, indexVerif]='#net_route_marker#'
                if (result.iloc[i, indexCert] != ports.iloc[i, indexCert]):
                    #print('cert différent')
                    result.iloc[i, indexVerif]=result.iloc[i, indexVerif]+'#certitude#'
                    saveit = True
            '''if saveit:
                #print(ports)
                #print(result)'''
            resultat = pd.concat([resultat, result], ignore_index=True)
            '''test = result["net_route_marker"].reset_index(drop=True).compare(ports["net_route_marker"].reset_index(drop=True))
            #Marche aussi, mais moins précis. Ne renseigne pas verif avec la nature de la différence
            '''
            print ("______________________________ "+str(c.end_seq))
            #if c.end_seq < data.shape[0] : 
            #    ports = c.take_a_sequence(data, ship_id)


    verifs = resultat.drop_duplicates(subset=['pkid'])
    #print(verifs)
    #https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_sql.html
    tableoutput = config.get('itineraries_output', 'table_name')
    verifs.to_sql(tableoutput, con=c.engine, if_exists='replace', method='multi')
    #Ecrase la vérif ancienne.

    tableinput = config.get('itineraries_input', 'table_name')

    '''
    query = """
    select t.ship_id,t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as fixed_net_route_marker,t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.has_input_done,t.pointcall_admiralty,t.pointcall_province,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from %s t left join %s v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull 
    """%(tableinput, tableoutput)
    '''
    query = """select t.ship_id, t.ship_name, t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as net_route_marker,
	t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.date_fixed,
	t.has_input_done,t.pointcall_admiralty,t.pointcall_province,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,
	t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from %s t left join %s v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull 
    """%(tableinput, tableoutput)
    #print(query)
    #Si l'algo n'a pas vérifié le  net_route_marker on prend la valeur de celui de Silvia : coalesce(v.net_route_marker, t.net_route_marker) as net_route_marker


    data = sql.read_sql_query(query, c.engine)

    c.outputAnalysis(config, data)

    finaltableoutput = config.get('itineraries_final_output', 'table_name')
    #navigoviz.pointcall_checked
    data.to_sql(finaltableoutput, con=c.engine, if_exists='replace', method='multi')


