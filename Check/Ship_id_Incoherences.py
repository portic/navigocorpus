'''
Created on 15 october 2020
@author: lgonzalez, cplumejeaud
ANR PORTIC : used to check the coherence of homeport, flag, tonnage, ship_class for a same ship
This requires :
1. to have loaded data (pointcall, taxes, cargo) from navigo with LoadFilemaker.py. In schema navigo, navigocheck.
2. to have build a table ports.port_points listing all ports ( using geo_general ) with additional data and manual editing for admiralty, province.
3. to have build a table navigoviz.pointcall listing all pointcalls  with additional data and manual editing for admiralty, province.

Contrôle par navire (ship_id)
1.	Si le tonnage est vide dans un des pointcall mais renseigné dans un autre, signaler 
2.	Si le homeport est vide dans un des pointcall mais renseigné dans un autre, signaler ship_id
3.	Si le flag est vide dans un des pointcall mais renseigné dans un autre, signaler ship_id
4.	SI ship_type est vide dans un des pointcall mais renseigné dans un autre, signaler ship_id
5.	Si le flag_id diverge, signaler ship_id

Clarification: All the records_id (mistakes) identified have a reference record to verify (i.e x_uncertainity == 0). 
        When this is not possible, we don't represent them in the output. We might have a differente aproach to integrate them.
'''
# Libraries
import pandas as pd
import xlwt
import logging
import configparser
import pandas.io.sql as sql
from sqlalchemy import create_engine
import openpyxl
from openpyxl import load_workbook, Workbook
from openpyxl.styles import NamedStyle, Font, Border, Side, Fill
import os
from datetime import datetime
import Levenshtein

class CheckShipCoherence(object):

    records_to_fix_homeport = []
    records_to_fix_homeport_id = []
    records_to_fix_flag = []
    records_to_fix_flag_id = []
    records_to_fix_flag_homeport = []
    records_to_fix_tonnage = []
    records_to_fix_shipclass = []
    errorcode_shipclass =  {}

    style0 = NamedStyle(name="style0") #gris
    style1 = NamedStyle(name="style1") #jaune
    style2 = NamedStyle(name="style2") #noir normal
    style3 = NamedStyle(name="style3") #rouge

    my_black = openpyxl.styles.colors.Color(rgb='00000000')
    my_red = openpyxl.styles.colors.Color(rgb='00FF0000')
    my_yellow = openpyxl.styles.colors.Color(rgb='00FFFF00')
    my_grey = openpyxl.styles.colors.Color(rgb='00C0C0C0')
        
    style0.font = Font(size=10, color=my_black)
    style0.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_grey)
    style1.font = Font(size=10, color=my_black)
    style1.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_yellow) 
    style2.font = Font(size=10, color=my_black)
    style3.font = Font(bold=True, size=10, color=my_black)
    style3.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_red)

    def __init__(self, config):
        """
        Ouvre les fichiers de log et une connexion à la base de données (en fonction des paramètres de config)
        """
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('CheckShipCoherence')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')
        # Init arrays of errors
        self.records_to_fix_homeport = []
        self.records_to_fix_homeport_id = []
        self.records_to_fix_flag = []
        self.records_to_fix_flag_id = []
        self.records_to_fix_flag_homeport = []
        self.records_to_fix_tonnage = []
        self.records_to_fix_shipclass = []
        self.errorcode_shipclass =  {}
        

    def loadDataFromCsv(self, config):
        '''
        Marche sauf pour ship_flag_standardized_fr, qui n'est pas dans le fichier CSV (le 5 mars 2021)
        '''
        filename = config['input']['file_name']
        # Load data 
        data = pd.read_csv(filename, sep="\t") #"data_1787_e.csv"
        data = data.query('annee == 1787')

        print("The variable names are: {}".format(list(data)))

        #Interested Variables 
        interest_variables = ["record_id","ship_id", "homeport", "homeport_uhgs_id", "tonnage", "flag", "ship_flag_id", "homeport_uncertainity", "flag_uncertainity", "tonnage_uncertainity", "pointcall_rankfull", "pointcall_function", "homeport_state_1789", "pointcall_uncertainity", "ship_class"]
        data_manipulate = data.loc[:,interest_variables]
        interest_variables_iloc = [data.columns.get_loc(c) for c in interest_variables if c in data]

        ###Quantity of values that are not NA (NA in python are different)
        print(sum(data_manipulate.homeport == data_manipulate.homeport))
        print(sum(data_manipulate.flag == data_manipulate.flag))
        print("#tonnage non null : " + str(sum(data.tonnage == data.tonnage)))
        print("#ship_class non null : " + str(sum(data.ship_class == data.ship_class)))

        #Delete records when ship_id is NaN (there's no way to verify them). We get 70803 records
        data_manipulate = data_manipulate.dropna(subset=["ship_id"])
        #Sort data (by ship_id and then point_call_ranking)
        data_manipulate = data_manipulate.sort_values(by=["ship_id", "pointcall_rankfull"])
        print(list(data_manipulate.columns) )
        print(list(data_manipulate.shape) ) #[70803, 12]

        return data_manipulate

    def loadDataFromDB(self, config):
        '''
        get data from a table public.csv_extract that was built to prepare data for statistical analysis, from navigoviz.pointcall
        '''
        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')

        engine = create_engine('postgresql://'+user+':'+password+'@'+host+':'+port+'/'+dbname)
        print(engine)
        query = """select record_id, ship_id, homeport, homeport_uhgs_id, tonnage::float, flag, ship_flag_id, 
                homeport_uncertainity, flag_uncertainity, tonnage_uncertainity, 
                pointcall_rankfull, pointcall_function, homeport_state_1789, 
                ship_flag_standardized_fr, pointcall_uncertainity, ship_class, shipclass_uncertainity
            from public.csv_extract where annee = 1787"""
        #where tonnage_uncertainity <= 0
        #On ne veut pas les données déjà contrôlées (code 1 ou 2)

        data = sql.read_sql_query(query, engine)

        #Interested Variables 
        interest_variables = ["record_id","ship_id", "homeport", "homeport_uhgs_id", "tonnage", "flag", "ship_flag_id", "homeport_uncertainity", "flag_uncertainity", "tonnage_uncertainity", "pointcall_rankfull", "pointcall_function", "homeport_state_1789", "ship_flag_standardized_fr", "pointcall_uncertainity", "ship_class", "shipclass_uncertainity"]
        interest_variables_iloc = [data.columns.get_loc(c) for c in interest_variables if c in data]

        ###Quantity of values that are not NA (NA in python are different)
        print("#homeports non null : " + str(sum(data.homeport == data.homeport)))
        print("#flags non null : " + str(sum(data.flag == data.flag)))
        print("#tonnage non null : " + str(sum(data.tonnage == data.tonnage)))
        print("#ship_class non null : " + str(sum(data.ship_class == data.ship_class)))

        #Delete records when ship_id is NaN (there's no way to verify them). We get 70803 records
        data = data.dropna(subset=["ship_id"])
        #Sort data (by ship_id and then point_call_ranking)
        data = data.sort_values(by=["ship_id", "pointcall_rankfull"])
        print(list(data.columns) )
        print(list(data.shape) ) #[70984, 14]
        return data

    def checkHomeport(self, config, data_manipulate):    
        '''
        Vérifier l'homogénéité des homeports renseignés par ship_id, par rapport au dernier point renseigné 
        '''
        ship_id_unique = data_manipulate.ship_id.unique().tolist()
        by_ship_id = data_manipulate.groupby("ship_id")

        good_records=[]

        for i in range(len(ship_id_unique)):
            homeport_ref=None
            homeport_id_ref=None
            record_id_homeport_ref =None
            record_id_homeport_id_ref = None
            group = by_ship_id.get_group(ship_id_unique[i])
            for j in range(len(group)):
                if (group.iloc[j,group.columns.get_loc("homeport_uncertainity")]==0):
                    #print(j)
                    homeport_ref = group.iloc[j,group.columns.get_loc("homeport")]
                    homeport_id_ref = group.iloc[j,group.columns.get_loc("homeport_uhgs_id")]
                    record_id_homeport_ref = group.iloc[j,group.columns.get_loc("record_id")]
                    record_id_homeport_id_ref = group.iloc[j,group.columns.get_loc("record_id")]
                if (group.iloc[j,group.columns.get_loc("homeport_uncertainity")]==-2 and homeport_ref!=None):
                    if(group.iloc[j,group.columns.get_loc("homeport")] != homeport_ref):
                        good_records.append(record_id_homeport_ref)
                        self.records_to_fix_homeport.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])
                    if(group.iloc[j,group.columns.get_loc("homeport_uhgs_id")] != homeport_id_ref):
                        good_records.append(record_id_homeport_id_ref)
                        self.records_to_fix_homeport_id.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])

        record_home_total = set(self.records_to_fix_homeport + self.records_to_fix_homeport_id)
        good_records = set(good_records)

        #DF
        data_manipulate_cp = data_manipulate
        data_manipulate_cp = data_manipulate_cp.set_index("record_id")
        data_homeport = data_manipulate_cp.loc[list(record_home_total)+list(good_records)]
        data_homeport = data_homeport.reset_index()
        data_homeport = data_homeport.loc[:, ["record_id","ship_id", "homeport", "homeport_uhgs_id", "homeport_uncertainity",  "pointcall_rankfull"]]

        data_homeport = data_homeport.sort_values(by=["ship_id", "pointcall_rankfull"])
        data_homeport = data_homeport.reset_index(drop=True)
        #If we'd like to export the csv, execute the following line
        #data_homeport.to_csv("data_cool.csv", sep=";")

        #Important for the Excel file
        data_homeport=data_homeport.astype(str)

        return data_homeport

    def checkFlag(self, config, data_manipulate):    
        '''
        DEPRECATED
        '''
        ship_id_unique = data_manipulate.ship_id.unique().tolist()
        by_ship_id = data_manipulate.groupby("ship_id")

        #records_to_fix_flag = []
        #records_to_fix_flag_id = []
        good_records_flag=[]

        for i in range(len(ship_id_unique)):
            flag_ref=None
            flag_id_ref=None
            record_id_flag_ref =None
            record_id_flag_id_ref = None
            group = by_ship_id.get_group(ship_id_unique[i])
            for j in range(len(group)):
                if (group.iloc[j,group.columns.get_loc("flag_uncertainity")]==0):
                    #print(j)
                    flag_ref = group.iloc[j,group.columns.get_loc("flag")]
                    flag_id_ref = group.iloc[j,group.columns.get_loc("ship_flag_id")]
                    record_id_flag_ref = group.iloc[j,group.columns.get_loc("record_id")]
                    record_id_flag_id_ref = group.iloc[j,group.columns.get_loc("record_id")]
                # if (group.iloc[j,group.columns.get_loc("flag_uncertainity")]==-2 and flag_ref!=None):
                #Contrôler les flags, indépendamment des crochets ou parenthèse 
                if ( flag_ref!=None):
                    if(group.iloc[j,group.columns.get_loc("flag")] != flag_ref):
                        good_records_flag.append(record_id_flag_ref)
                        self.records_to_fix_flag.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])
                    if(group.iloc[j,group.columns.get_loc("ship_flag_id")] != flag_id_ref):
                        good_records_flag.append(record_id_flag_id_ref)
                        self.records_to_fix_flag_id.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])

        record_flag_total = set(self.records_to_fix_flag + self.records_to_fix_flag_id)
        good_records_flag = set(good_records_flag)

        #DF
        data_manipulate_cpf = data_manipulate
        data_manipulate_cpf = data_manipulate_cpf.set_index("record_id")
        data_flag = data_manipulate_cpf.loc[list(record_flag_total)+list(good_records_flag)]
        data_flag = data_flag.reset_index()
        data_flag = data_flag.loc[:, ["record_id","ship_id", "flag", "ship_flag_id", "flag_uncertainity",  "pointcall_rankfull"]]

        data_flag = data_flag.sort_values(by=["ship_id", "pointcall_rankfull"])
        data_flag = data_flag.reset_index(drop=True)

        #Important for the Excel file
        data_flag=data_flag.astype(str)
        return data_flag

    def checkFlagHomeportConsistency(self, config, data_manipulate):    
        '''
        check Flag and also the consistency of the homeport'state compared to the flag
        '''
        filename = config['input']['flag_homeport']
        # Load data 
        dicoflag = pd.read_csv(filename, sep=";") #"data_1787_e.csv"
        print(list(dicoflag.columns) )
        print(list(dicoflag.shape) ) #

        ship_id_unique = data_manipulate.ship_id.unique().tolist()
        by_ship_id = data_manipulate.groupby("ship_id")

        good_records_flag=[]

        for i in range(len(ship_id_unique)):
            flag_ref=None
            flag_id_ref=None
            record_id_flag_ref =None
            record_id_flag_id_ref = None
            group = by_ship_id.get_group(ship_id_unique[i])
            for j in range(len(group)):
                if (group.iloc[j,group.columns.get_loc("flag_uncertainity")]==0):
                    #print(j)
                    flag_ref = group.iloc[j,group.columns.get_loc("flag")]
                    flag_id_ref = group.iloc[j,group.columns.get_loc("ship_flag_id")]
                    record_id_flag_ref = group.iloc[j,group.columns.get_loc("record_id")]
                    record_id_flag_id_ref = group.iloc[j,group.columns.get_loc("record_id")]
                # if (group.iloc[j,group.columns.get_loc("flag_uncertainity")]==-2 and flag_ref!=None):
                #Contrôler les flags, indépendamment des crochets ou parenthèse 
                if ( flag_ref!=None):
                    if(group.iloc[j,group.columns.get_loc("flag")] != flag_ref):
                        good_records_flag.append(record_id_flag_ref)
                        self.records_to_fix_flag.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])
                    if(group.iloc[j,group.columns.get_loc("ship_flag_id")] != flag_id_ref):
                        good_records_flag.append(record_id_flag_id_ref)
                        self.records_to_fix_flag_id.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])
                    ##Contrôle de la cohérence du homeport si renseigné
                    if(group.iloc[j,group.columns.get_loc("homeport_state_1789")] != None):
                        myhomeport = group.iloc[j,group.columns.get_loc("homeport_state_1789")]
                        #dicoflag.get_loc("homeport_state_fr") ['ship_flag_standardized_fr', 'homeport_state_fr']
                        a = dicoflag.loc[dicoflag['homeport_state_fr'] == myhomeport, ['ship_flag_standardized_fr']]
                        myflagshouldbe = a.values[0, 0]
                        if (myflagshouldbe != group.iloc[j,group.columns.get_loc("ship_flag_standardized_fr")]) :
                            #good_records_flag.append(record_id_flag_ref)
                            self.records_to_fix_flag_homeport.append(group.iloc[j,group.columns.get_loc("record_id")])

        record_flag_total = set(self.records_to_fix_flag + self.records_to_fix_flag_id + self.records_to_fix_flag_homeport)
        good_records_flag = set(good_records_flag)


        #DF
        data_manipulate_cpf = data_manipulate
        data_manipulate_cpf = data_manipulate_cpf.set_index("record_id")
        data_flag = data_manipulate_cpf.loc[list(record_flag_total)+list(good_records_flag)]
        data_flag = data_flag.reset_index()
        data_flag = data_flag.loc[:, ["record_id","ship_id", "flag", "ship_flag_id", "flag_uncertainity",  "pointcall_rankfull", "homeport_state_1789", "ship_flag_standardized_fr"]]

        data_flag = data_flag.sort_values(by=["ship_id", "pointcall_rankfull"])
        data_flag = data_flag.reset_index(drop=True)

        #Important for the Excel file
        data_flag=data_flag.astype(str)
        return data_flag

    def checkTonnage(self, config, data_manipulate) :
        '''
        If tonnage is missing, or not copied identically from previous existing entries
        '''
        ship_id_unique = data_manipulate.ship_id.unique().tolist()
        by_ship_id = data_manipulate.groupby("ship_id")

        ### Tonnage
        good_records_ton=[]

        for i in range(len(ship_id_unique)):
            tonnage_ref=None
            record_id_ton_ref =None
            group = by_ship_id.get_group(ship_id_unique[i])
            for j in range(len(group)):
                if (group.iloc[j,group.columns.get_loc("tonnage_uncertainity")]==0):
                    #print(j)
                    tonnage_ref = group.iloc[j,group.columns.get_loc("tonnage")]
                    record_id_ton_ref = group.iloc[j,group.columns.get_loc("record_id")]
                if (group.iloc[j,group.columns.get_loc("tonnage_uncertainity")]==-2 and tonnage_ref!=None):
                    if(group.iloc[j,group.columns.get_loc("tonnage")] != tonnage_ref):
                        good_records_ton.append(record_id_ton_ref)
                        self.records_to_fix_tonnage.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])
                if (group.iloc[j,group.columns.get_loc("tonnage_uncertainity")]!=-2 and tonnage_ref!=None):
                    #print(group.iloc[j,group.columns.get_loc("tonnage")])
                    if(pd.isnull(group.iloc[j,group.columns.get_loc("tonnage")])): 
                        #print("j is null "+ str(j) + " in group i "+str(i))
                        good_records_ton.append(record_id_ton_ref)
                        self.records_to_fix_tonnage.append(group.iloc[j,group.columns.get_loc("record_id")])

        self.records_to_fix_tonnage = set(self.records_to_fix_tonnage)
        good_records_ton = set(good_records_ton)

        #DF
        data_manipulate_cpt = data_manipulate
        data_manipulate_cpt = data_manipulate_cpt.set_index("record_id")
        data_tonnage = data_manipulate_cpt.loc[list(self.records_to_fix_tonnage)+list(good_records_ton)]
        data_tonnage = data_tonnage.reset_index()
        data_tonnage = data_tonnage.loc[:, ["record_id","ship_id", "tonnage", "tonnage_uncertainity",  "pointcall_rankfull"]]

        data_tonnage = data_tonnage.sort_values(by=["ship_id", "pointcall_rankfull"])
        data_tonnage = data_tonnage.reset_index(drop=True)

        #Important for the Excel file
        data_tonnage=data_tonnage.astype(str)
        return data_tonnage

    def checkShipclass(self, config, data_manipulate) :
        '''
        Ship class must be the same for all entries, since the first observation point (pointcall_uncertainity == 0)
        '''
        ship_id_unique = data_manipulate.ship_id.unique().tolist()
        by_ship_id = data_manipulate.groupby("ship_id")

        ### Ship class
        good_records=[]

        for i in range(len(ship_id_unique)):
            shipclass_ref=None
            record_id_ref =None
            group = by_ship_id.get_group(ship_id_unique[i])
            for j in range(len(group)):
                if (group.iloc[j,group.columns.get_loc("shipclass_uncertainity")]==0):
                    #print(j)
                    shipclass_ref = group.iloc[j,group.columns.get_loc("ship_class")]
                    record_id_ref = group.iloc[j,group.columns.get_loc("record_id")]
                if (group.iloc[j,group.columns.get_loc("shipclass_uncertainity")]!=0 and shipclass_ref!=None):
                    if(group.iloc[j,group.columns.get_loc("ship_class")] != shipclass_ref):
                        d = Levenshtein.distance(str(group.iloc[j,group.columns.get_loc("ship_class")]),str(shipclass_ref))
                        # rajouter le niveau d'erreur dans un dictionnaire avec key = group.iloc[j,group.columns.get_loc("record_id")]
                        #print ("found " + record_id_ref)
                        if (d > 4 or pd.isnull(group.iloc[j,group.columns.get_loc("ship_class")])):
                            self.errorcode_shipclass[str(group.iloc[j,group.columns.get_loc("record_id")])] = 3
                        else:
                            self.errorcode_shipclass[str(group.iloc[j,group.columns.get_loc("record_id")])] = 2
                        good_records.append(record_id_ref)
                        self.records_to_fix_shipclass.append(group.iloc[j,group.columns.get_loc("record_id")])
                        #print(group.iloc[j,group.columns.get_loc("record_id")])

        self.records_to_fix_shipclass = set(self.records_to_fix_shipclass)
        good_records = set(good_records)

        #DF
        data_manipulate_cpt = data_manipulate
        data_manipulate_cpt = data_manipulate_cpt.set_index("record_id")
        data_shipclass = data_manipulate_cpt.loc[list(self.records_to_fix_shipclass)+list(good_records)]
        data_shipclass = data_shipclass.reset_index()
        data_shipclass = data_shipclass.loc[:, ["record_id","ship_id", "ship_class", "shipclass_uncertainity",  "pointcall_rankfull"]]

        data_shipclass = data_shipclass.sort_values(by=["ship_id", "pointcall_rankfull"])
        data_shipclass = data_shipclass.reset_index(drop=True)

        #Important for the Excel file
        data_shipclass=data_shipclass.astype(str)
        return data_shipclass


    def outputHomeportAnalysis(self, config, data_homeport):
        filename = config['output']['file_name']
        sheetname = config['output']['sheet_name_homeport']

        #Report in Excel
        #wb = xlwt.Workbook()  
        #print("Open the workbook") 
        wb = load_workbook(filename)
        #print("Open the workbook - ok") 

        ws = wb.create_sheet(title=sheetname)

        for k in range(len(data_homeport.columns)) :
            _ = ws.cell(column=k+1, row=0+1, value=data_homeport.columns[k])
            ws.cell(0+1, k+1).style = "style2"
        for i in range(len(data_homeport)):
                for j in range(len(data_homeport.columns)) :
                    if(data_homeport.homeport_uncertainity[i]=="-2" and j==2 and list(map(str,self.records_to_fix_homeport)).count(data_homeport.record_id[i])>0 and list(map(str,self.records_to_fix_homeport_id)).count(data_homeport.record_id[i])>0):
                        sty = "style3"
                    elif(data_homeport.homeport_uncertainity[i]=="-2" and j==2 and list(map(str,self.records_to_fix_homeport)).count(data_homeport.record_id[i])>0):
                        sty = "style1" 
                    elif(data_homeport.homeport_uncertainity[i]=="-2" and j==3 and list(map(str,self.records_to_fix_homeport_id)).count(data_homeport.record_id[i])>0):
                        sty = "style3"
                    elif(data_homeport.homeport_uncertainity[i]=="0"):
                        sty="style0"
                    else: sty="style2"
                    #print(i)
                    _ = ws.cell(column=j+1, row=i+2, value=data_homeport.iloc[i,j])
                    ws.cell(i+2, j+1).style = sty
        wb.save(filename)
        ## Report in python
        print("The following {} records_id have mistakes for the variable homeport: {}".format(len(self.records_to_fix_homeport),self.records_to_fix_homeport))
        print("The following {} records_id have mistakes for the variable homeport_id: {}".format(len(self.records_to_fix_homeport_id),self.records_to_fix_homeport_id))



    def outputFlagAnalysis(self, config, data_flag):
        filename = config['output']['file_name']
        sheetname2 = config['output']['sheet_name_flag']

        #Report in Excel
        wb = load_workbook(filename)

        #Create othe sheet for flags
        ws3 = wb.create_sheet(title=sheetname2)

        for k in range(len(data_flag.columns)) :
            _ = ws3.cell(column=k+1, row=0+1, value=data_flag.columns[k])
            ws3.cell(0+1, k+1).style = "style2"
        for i in range(len(data_flag)):
                for j in range(len(data_flag.columns)) :
                    # remplacer data_flag.flag_uncertainity[i]=="-2" par data_flag.flag_uncertainity[i]!="0"
                    if (data_flag.flag_uncertainity[i]!="0" and j==2 and list(map(str,self.records_to_fix_flag)).count(data_flag.record_id[i])>0 and list(map(str,self.records_to_fix_flag_id)).count(data_flag.record_id[i])>0):
                        sty = "style3"
                    elif(data_flag.flag_uncertainity[i]!="0" and j==2 and list(map(str,self.records_to_fix_flag)).count(data_flag.record_id[i])>0):
                        sty = "style1" 
                    elif(data_flag.flag_uncertainity[i]!="0" and j==3 and list(map(str,self.records_to_fix_flag_id)).count(data_flag.record_id[i])>0):
                        sty = "style3"
                    elif(data_flag.flag_uncertainity[i]=="0" and j!=6):
                        sty = "style0"
                    elif (j==6 and list(map(str,self.records_to_fix_flag_homeport)).count(data_flag.record_id[i])>0):
                        sty = "style3"
                    else: sty = "style2"
                    #print(i)
                    _ = ws3.cell(column=j+1, row=i+1+1, value=data_flag.iloc[i,j])
                    ws3.cell(i+1+1, j+1).style = sty

        wb.save(filename)

        print("The following {} records_id have mistakes for the variable flag: {}".format(len(self.records_to_fix_flag),self.records_to_fix_flag))
        print("The following {} records_id have mistakes for the variable ship_flag_id: {}".format(len(self.records_to_fix_flag_id),self.records_to_fix_flag_id))
        print("The following {} records_id have mistakes for the homeport's state consistency with the flag:".format(len(self.records_to_fix_flag_homeport)))

    def outputTonnageAnalysis(self, config, data):
        filename = config['output']['file_name']
        sheetname = config['output']['sheet_name_tonnage']

        #Report in Excel
        wb = load_workbook(filename)
        ws = wb.create_sheet(title=sheetname)

        for k in range(len(data.columns)) :
            _ = ws.cell(column=k+1, row=0+1, value=data.columns[k])
            ws.cell(0+1, k+1).style = "style2"
        for i in range(len(data)):
                for j in range(len(data.columns)) :
                    #data.tonnage_uncertainity[i]=="-2" and
                    if( j==2 and list(map(str,self.records_to_fix_tonnage)).count(data.record_id[i])>0):
                        sty = "style3"
                    elif(data.tonnage_uncertainity[i]=="0"):
                        sty="style0"
                    else: sty="style2"
                    #print(i)
                    _ = ws.cell(column=j+1, row=i+2, value=data.iloc[i,j])
                    ws.cell(i+2, j+1).style = sty
        wb.save(filename)
        ## Report in python
        print("The following {} records_id have mistakes for the variable tonnage: {}".format(len(self.records_to_fix_tonnage),self.records_to_fix_tonnage))

    def outputShipclassAnalysis(self, config, data):
        filename = config['output']['file_name']
        sheetname = config['output']['sheet_name_shipclass']

        #Report in Excel
        wb = load_workbook(filename)
        ws = wb.create_sheet(title=sheetname)

        for k in range(len(data.columns)) :
            _ = ws.cell(column=k+1, row=0+1, value=data.columns[k])
            ws.cell(0+1, k+1).style = "style2"
        for i in range(len(data)):
                for j in range(len(data.columns)) :
                    if( j==2 and list(map(str,self.records_to_fix_shipclass)).count(data.record_id[i])>0):
                        #print (data.record_id[i])
                        if (self.errorcode_shipclass[data.record_id[i]]) == 3:
                            sty = "style3"
                        else :
                            sty = "style1"
                    elif(data.shipclass_uncertainity[i]=="0"):
                        sty="style0"
                    else: 
                        sty="style2"
                    #print(i)
                    _ = ws.cell(column=j+1, row=i+2, value=data.iloc[i,j])
                    ws.cell(i+2, j+1).style = sty
        wb.save(filename)
        ## Report in python
        print("The following {} records_id have mistakes for the variable ship_class: {}".format(len(self.records_to_fix_shipclass),self.records_to_fix_shipclass))


    def resetExcelFile(self, config):
        ''' 
        Must be called one at the starts of the programme to delete et reset the styles in the Excel file.
        '''
        if os.path.exists(config['output']['file_name']):
            os.remove(config['output']['file_name'])
        else:
            print("The file does not exist "+config['output']['file_name'])
        wb = Workbook()
        #Refresh styles
        print(wb.named_styles)
        # if "style0" in wb.named_styles : 
        #     del wb._named_styles[ wb.style_names.index("style0")]
        wb.add_named_style(self.style0)
        
        wb.add_named_style(self.style1)
        
        wb.add_named_style(self.style2)
        
        wb.add_named_style(self.style3)

        #Remove all sheets
        print(wb.sheetnames)
        for sheetname in wb.sheetnames:
            old=wb[sheetname]
            wb.remove_sheet(old)

        #Create one sheet with metadata
        ws = wb.create_sheet(title="metadata")
        _ = ws.cell(column=1, row=1, value="METADATA")
        ws.cell(1, 1).style = "style0"
        _ = ws.cell(column=1, row=2, value="File generated at "+str(datetime.now()))
        _ = ws.cell(column=1, row=3, value="Database was "+config.get('base', 'dbname') +" on host "+config.get('base', 'host'))
        wb.save(config['output']['file_name'])


if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_ship.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))   
    c = CheckShipCoherence(config)
    c.resetExcelFile(config)
    
    ## Load data
    #data = c.loadDataFromCsv(config)
    data = c.loadDataFromDB(config)

    ## Make the checks
    data_homeport = c.checkHomeport(config, data)
    data_flag = c.checkFlagHomeportConsistency(config, data)
    data_tonnage = c.checkTonnage(config, data)
    data_shipclass = c.checkShipclass(config, data)
    
    ## Save them
    c.outputHomeportAnalysis(config, data_homeport)
    c.outputFlagAnalysis(config, data_flag)
    c.outputTonnageAnalysis(config, data_tonnage)
    c.outputShipclassAnalysis(config, data_shipclass)