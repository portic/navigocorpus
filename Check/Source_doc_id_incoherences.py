'''
Created on 15 october 2020
@author: lgonzalez, cplumejeaud
ANR PORTIC : used to check the coherence of homeport, flag, tonnage, ship_class for a same ship
This requires :
1. to have loaded data (pointcall, taxes, cargo) from navigo with LoadFilemaker.py. In schema navigo, navigocheck.
2. to have build a table ports.port_points listing all ports ( using geo_general ) with additional data and manual editing for admiralty, province.
3. to have build a table navigoviz.pointcall listing all pointcalls  with additional data and manual editing for admiralty, province.

Pour chaque documentary_unit, vérifier que tous les points de la même documentary_unit aient :
o	Même ship_name 
o	Même ship_id
o	Même homeport_UHGS
o	Même homeport (graphie)
o	Même tonnage 
o	Même flag 
o	Même captain_name 
o	Même capt_id 
o	Même capt_birthplace si renseigné (graphie)
o	Même capt_birthplace_UHGS 
o	Même ship_type (hors parenthèses)
En cas de divergence, fournir la liste des erreurs constatées par documentary_unit (source_doc_id dans l’API) et indiquer le record_id de la ligne correspondante dans FileMaker et le ship_id
NB : les divergences ne sont pas forcément toutes des erreurs : à Marseille, un navire peut entrer en disant que le capitaine du départ est resté dans un autre port en chemin

#### Important to consider in the Red data
#len(restored_data.loc[restored_data.record_id=="149478","captain_name"].values[0]) output:16
#len(restored_data.loc[restored_data.record_id=="155448","captain_name"].values[0]) output:17
#len(restored_data.loc[restored_data.record_id=="296362","captain_name"].values[0]) output:17

# There are cases of extra spaces after the record, so if we look it only with our eyes, the mistake can't be perceived
# Thus it's necessary to use len() and verify it.
'''

#Import libraries
import pandas as pd
import numpy as np
import xlwt
import Levenshtein

import logging
import configparser
import pandas.io.sql as sql
from sqlalchemy import create_engine
import openpyxl
from openpyxl import load_workbook, Workbook
from openpyxl.styles import NamedStyle, Font, Border, Side, Fill
import os
from datetime import datetime

class CheckDocumentCoherence(object):
    style0 = NamedStyle(name="style0") #ecrit en rouge
    style1 = NamedStyle(name="style1") #ecrit en noir
    style2 = NamedStyle(name="style2") #fond vert
    style3 = NamedStyle(name="style3") #fond gris
    style4 = NamedStyle(name="style4") # ecrit en rouge, surligné en jaune
    style5 = NamedStyle(name="style5") # fond bleu

    my_black = openpyxl.styles.colors.Color(rgb='00000000')
    my_red = openpyxl.styles.colors.Color(rgb='00FF0000')
    my_green = openpyxl.styles.colors.Color(rgb='0000FF00')
    my_yellow = openpyxl.styles.colors.Color(rgb='00FFFF00')
    my_grey = openpyxl.styles.colors.Color(rgb='00C0C0C0')
    my_bluecyan = openpyxl.styles.colors.Color(rgb='0000FFFF')

    style0.font = Font(size=10, color=my_red)
    style1.font = Font(size=10, color=my_black)
    
    style2.font = Font(size=10, color=my_green)
    style2.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_green) 

    style3.font = Font(size=10, color=my_black)
    style3.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_grey)

    style4.font = Font(bold=True, size=10, color=my_red)
    style4.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_yellow)

    style5.font = Font(size=10, color=my_black)
    style5.fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_bluecyan)

    ##### This code is to obtain the Excel Output with the code of colors
    # Red: Mistake, Black: Good, Green: There's not enough info
    # Gray fond: All records with "data_block_leader_marker"=="A"
    # Yellow fond: Probabliy a big mistake, using Levenshtein distance > 4 as metric

    """     style0 = xlwt.easyxf('font: color-index red')
    style1 = xlwt.easyxf('font: color-index black')
    style2 = xlwt.easyxf('font: color-index green; pattern: pattern solid, fore_color green')
    style3 = xlwt.easyxf('font: color-index black; pattern: pattern solid, fore_color grey25')
    style4 = xlwt.easyxf('font: color-index red;pattern: pattern solid, fore_color yellow') """


    def __init__(self, config):
        """
        Ouvre les fichiers de log et une connexion à la base de données (en fonction des paramètres de config)
        """
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('CheckDocumentCoherence')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

    def loadDataFromCsv(self, config):
        filename = config['input']['file_name']
        # Load data 
        data = pd.read_csv(filename, sep="\t") #"data_1787_e.csv"
        data = data.query('annee == 1787')

        print("The variable names are: {}".format(list(data)))
        # Column names
        list(data)
        return data

    def loadDataFromDB(self, config):
        '''
        get data from a table public.csv_extract that was built to prepare data for statistical analysis, from navigoviz.pointcall
        '''
        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')

        engine = create_engine('postgresql://'+user+':'+password+'@'+host+':'+port+'/'+dbname)
        print(engine)
        query = """select pkid, record_id, source_doc_id, source_component, 
                ship_id, ship_name, ship_flag_id, flag, ship_flag_standardized_fr, citizenship, birthplace, birthplace_uhgs_id,
                ship_class, tonnage::float, tonnage_unit, 
                homeport, homeport_uhgs_id, homeport_state_1789,
                captain_id, captain_name, status,  
                tax_concept, q01, q02, q03, 
                data_block_leader_marker, pointcall_rankfull, pointcall_function, pointcall_uncertainity
            from public.csv_extract where annee = 1787 order by source_doc_id, pointcall_rankfull """
        #where tonnage_uncertainity <= 0
        #On ne veut pas les données déjà contrôlées (code 1 ou 2)
       
        data = sql.read_sql_query(query, engine)

        print (data.shape)#(75941, 29)
        return data

    ## Function: Evaluate the status of a record by cell {-2,-1,0,1} where 1: Good, 0: Unknown, -1: Big mistake, -2: Small mistake    
    def eval_diff(self, bad_record: list, good_record: list) -> list:
        status_record = []
        for i in range(len(good_record)) :
            if (str(bad_record[i])==str(good_record[i])) : status_record.append("1") #OK
            elif (str(bad_record[i])=='?') : status_record.append("0") #Missing
            elif (Levenshtein.distance(str(bad_record[i]),str(good_record[i]))>4): status_record.append("-1") #Erreur très grave (grosse différence)
            elif (Levenshtein.distance(str(bad_record[i]),str(good_record[i]))==0) : status_record.append("1")  #Ok malgré la petite différence
            else : status_record.append("-2") #Erreur moins grave (différence mais faible)
        return status_record

    def dothejob(self, config, data) :   
        #Interested Variables 
        #Avant (qui marche avec CSV de portic_v5)
        #interest_variables = ["ship_id", "ship_name", "ship_flag_id", "ship_class", "tonnage", "tonnage_unit", "source_component", "homeport", "homeport_uhgs_id", "captain_id", "captain_name","birthplace",  "status", "citizenship", "tax_concept",  "q01", "q02", "q03", "flag"]


        # Sort values by source_doc_id
        data = data.sort_values(by=["source_doc_id"])

        # Two subsets: good records (those with data_block_leader_marker == A)
        good_data = data.loc[data.data_block_leader_marker=="A"]
        good_data = good_data.sort_values(by=["source_doc_id"])
        good_data=good_data.reset_index(drop=True)
        good_data = good_data.fillna("?")

        # Two subsets: bad records 
        bad_data = data.loc[data.data_block_leader_marker!="A"]
        bad_data = bad_data.sort_values(by=["source_doc_id"])
        bad_data=bad_data.reset_index(drop=True)
        bad_data = bad_data.fillna("?")

        #print(bad_data_1787.columns)
        #print(good_data_1787.columns)

        ##Delete the problematic record (Imperative to delete): It's not fixed and has a unique source_doc_id
        ##Spécifique to data coming from CSV files extracted from portic_v5
        ## Detail: source_doc_id = 18377, data_1787.loc[data_1787.source_doc_id==183777,"data_block_leader_marker"] output: NaN
        ## print (data_1787.loc[data_1787.source_doc_id==183777,"data_block_leader_marker"].values)
        # bad_data_1787 = bad_data_1787.drop(bad_data_1787.index[31232])
        # bad_data_1787=bad_data_1787.reset_index(drop=True)

        print(bad_data.shape)#(38273, 29)
        print(good_data.shape)#(37668, 29)

        # Dictionary of interested variables to verify the incoherences. This help in the time execution
        interest_variables = ["source_component", "ship_id", "ship_name", "ship_class", "tonnage", "tonnage_unit", "captain_id", "captain_name", "birthplace", "birthplace_uhgs_id", "citizenship", "flag",  "ship_flag_standardized_fr", "ship_flag_id", "homeport", "homeport_uhgs_id" ]
        #print(len(interest_variables))

        # Same dictionary but with the column indexes
        interest_variables_iloc = [bad_data.columns.get_loc(c) for c in interest_variables if c in bad_data]
        # for c in interest_variables:
        #     if c not in bad_data :
        #         print(' bad_data_1787 has not '+c)
        #     if c not in good_data :
        #         print(' good_data_1787 has not '+c)

        # DataFrame with the code status: 
        cod_bad_data = []

        ### Goal: Obtain the code {-2,-1,0,1} for bad records {data_block_leader_marker != A}
        number_bad_records =len(bad_data)
        index_source_doc_id = bad_data.columns.get_loc('source_doc_id')
        #print(index_source_doc_id)
        for i in range(number_bad_records):
            listx = ((good_data.loc[good_data.source_doc_id==bad_data.iloc[i,index_source_doc_id],interest_variables]).values.tolist())
            if len(listx) > 0:
                if (len(bad_data.iloc[i,interest_variables_iloc].tolist()) == len(listx[0])):
                    x = self.eval_diff(bad_data.iloc[i,interest_variables_iloc].tolist(), listx[0])
                    #print(x) #['1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1']
                    cod_bad_data.append(x)
                else : 
                    # print("-------------- lengths differs --------------------")
                    # print(bad_data.iloc[i,index_source_doc_id])
                    # print('BAD :'+ ', '.join(map(str, bad_data.iloc[i,interest_variables_iloc].tolist()))) 
                    # print('GOOD :' + ', '.join(map(str, listx[0])))
                    print(i) #38196
            else :
                print(bad_data.iloc[i,index_source_doc_id])
                print(listx)
                print(i) #38196

        ### Final code bad data
        cod_bad_data
        df_cod_bad_data = pd.DataFrame(cod_bad_data, columns=interest_variables)
        df_cod_bad_data["record_id"] = bad_data["record_id"]
        v = [1] * len(cod_bad_data)  # Christine : parce que 1 est le code erreur (pas d'erreur)
        df_cod_bad_data['source_doc_id'] = v #? Christine :  ajout
        df_cod_bad_data['data_block_leader_marker'] = v #? Christine :  ajout
        #df_cod_bad_data_1787[['source_doc_id','data_block_leader_marker']]=1 #KeyError: "None of [Index(['source_doc_id', 'data_block_leader_marker'], dtype='object')] are in the [columns]"
        #? Christine : pourquoi cette ligne au-dessus bugue  ?
        df_cod_bad_data = df_cod_bad_data[['source_doc_id','record_id','data_block_leader_marker']+interest_variables]

        ###### Adicional importante: Only for df_cod_bad_data
        records_to_delete_in_bad=[]
        df_cod_bad_data_p = df_cod_bad_data.copy()
        df_cod_bad_data_p["record_id"] = 1
        df_cod_bad_data_p["source_doc_id"] = 1
        df_cod_bad_data_p["data_block_leader_marker"] = 1
        df_cod_bad_data_p = df_cod_bad_data_p.astype("int64")
        for i in range(len(df_cod_bad_data_p)):
            if (sum(df_cod_bad_data_p.iloc[i,:])==3+len(interest_variables)): # 20 := Nombre des variables d'intéret + recordid, source_doc_id, data_block_leader_marker
                records_to_delete_in_bad.append(int(df_cod_bad_data.record_id[i]))
                #print(i)
        #print ('to delete '+str(len(records_to_delete_in_bad)))

        ######
        b = (df_cod_bad_data["record_id"].astype("int64")).isin(records_to_delete_in_bad)
        df_cod_bad_data = df_cod_bad_data.drop(b[b == True].index.tolist())
        df_cod_bad_data=df_cod_bad_data.reset_index(drop=True)
        bad_data = bad_data.drop(b[b == True].index.tolist())
        bad_data=bad_data.reset_index(drop=True)
        # sum(bad_data.record_id == df_cod_bad_data.record_id)
        # 10867 rows

        ### Final code good data
        #####  Rows that are Ok (A) and aren't reference of others (!A)
        records_good_source_doc_id = []
        for i in range(len(good_data)):
            #print(i)
            if((bad_data.source_doc_id==good_data.source_doc_id[i]).any() == False):
                records_good_source_doc_id.append(good_data.record_id[i])
        #28500 to delete
        ####
        df_cod_good_data = pd.DataFrame(columns=['source_doc_id','record_id','data_block_leader_marker']+interest_variables)
        df_cod_good_data["record_id"] = good_data["record_id"]
        df_cod_good_data[['source_doc_id','data_block_leader_marker']+interest_variables]=2 #parce que 2 est le code erreur (fond gris)
        df_cod_good_data = df_cod_good_data[['source_doc_id','record_id','data_block_leader_marker']+interest_variables]
        ####Delete 
        a = (df_cod_good_data["record_id"].astype("int64")).isin(records_good_source_doc_id)
        df_cod_good_data = df_cod_good_data.drop(a[a == True].index.tolist())
        df_cod_good_data=df_cod_good_data.reset_index(drop=True)
        good_data = good_data.drop(a[a == True].index.tolist())
        good_data=good_data.reset_index(drop=True)


        ### Final Dataset with the codes: {-1,0,1,2}
        #print(df_cod_bad_data_1787.columns) #'source_doc_id', 'record_id', 'data_block_leader_marker', 'ship_id'...
        #print(df_cod_good_data_1787.columns) #'source_doc_id', 'record_id', 'data_block_leader_marker', 'ship_id'...
        df_cod_complet = pd.concat([df_cod_bad_data, df_cod_good_data])
        #df_cod_complet = df_cod_complet.set_index("record_id") #? Christine : commenté
        #La colonne record_id devient l'index du dataframe et donc df.loc[10, :] fonctionnera, si 10 est la valeur d'un recordid
        # La colonne record_id disparait par le set_index, donc on la rétablit (mais on ne peut pas) : ValueError: cannot reindex from a duplicate axis
        #df_cod_complet["record_id"] = 1 #? Christine : 1 parce que ok (sachant que recordid est l'index du dataframe...bof). 
        #print(df_cod_complet.columns)
        #df_cod_complet = df_cod_complet[['source_doc_id','record_id','data_block_leader_marker']+interest_variables] #? Christine : pourquoi ?
        #df_cod_complet = df_cod_complet.astype("int") #? Christine : pourquoi ?
        df_cod_complet = df_cod_complet.astype(str) #? Christine : ajout
        df_cod_complet.to_csv("restored_code_data_1787.csv", index=False, sep=";", encoding="utf-8")
        print (df_cod_complet.shape) #(22433, 20)

        ### Final Dataset with the info
        restored_data =  pd.concat([good_data.loc[:,['source_doc_id','record_id','data_block_leader_marker']+interest_variables],bad_data.loc[:,['source_doc_id','record_id','data_block_leader_marker']+interest_variables]])
        restored_data = restored_data.sort_values(["source_component", "source_doc_id", "record_id"])
        restored_data = restored_data.astype(str)
        restored_data.to_csv("restored_data_1787.csv", index=False, sep=";", encoding="utf-8")
        print (restored_data.shape) #(22433, 20)
        return restored_data, df_cod_complet

    def resetExcelFile(self, config):
        ''' 
        Must be called one at the starts of the programme to delete et reset the styles in the Excel file.
        '''
        if os.path.exists(config['output']['file_name']):
            os.remove(config['output']['file_name'])
        else:
            print("The file does not exist "+config['output']['file_name'])
        wb = Workbook()
        #Refresh styles
        print(wb.named_styles)
        wb.add_named_style(self.style0)      
        wb.add_named_style(self.style1)
        wb.add_named_style(self.style2)
        wb.add_named_style(self.style3)
        wb.add_named_style(self.style4)
        wb.add_named_style(self.style5)

        #Remove all sheets
        print(wb.sheetnames)
        for sheetname in wb.sheetnames:
            old=wb[sheetname]
            wb.remove_sheet(old)

        #Create one sheet with metadata
        ws = wb.create_sheet(title="metadata")
        _ = ws.cell(column=1, row=1, value="METADATA")
        ws.cell(1, 1).style = "style3"
        _ = ws.cell(column=1, row=2, value="File generated at "+str(datetime.now()))
        _ = ws.cell(column=1, row=3, value="Database was "+config.get('base', 'dbname') +" on host "+config.get('base', 'host'))
        wb.save(config['output']['file_name'])

    def outputAnalysis(self, config, restored_data, df_cod_complet):
        filename = config['output']['file_name']
        sheetname1 = config['output']['sheet_name']

        #Report in Excel
        wb = load_workbook(filename)
        ws1 = wb.create_sheet(title=sheetname1)

        _ = ws1.cell(column=1, row=1, value='error_level')
        ws1.cell(1,1).style = "style1"

        for k in range(len(restored_data.columns)) :
            _ = ws1.cell(column=1+k+1, row=1, value=restored_data.columns[k])
            ws1.cell(k+2,1).style = "style1"
            #ws1.write(0,k+1, restored_data_1787.columns[k], style1)

        print('------------output -----------------')
        #print(df_cod_complet.columns)
        #df_cod_complet = df_cod_complet.astype({'record_id': 'int64'}) #a faire si on utilise df_cod_complet.loc[str(recordid),:]
        #df_cod_complet = df_cod_complet.set_index("record_id")
        print(df_cod_complet.columns)
        for i in range(len(restored_data)):
            max_error_level = 2
            
            #print (int(restored_data_1787.iloc[i,1])) #valeur du record_id
            recordid = format(int(restored_data.iloc[i,1]), '08d')
            #print("searching for :" + recordid)
            #error_codes = df_cod_complet.loc[str(recordid),:] #marche que si index sur recordid
            error_codes = df_cod_complet.loc[df_cod_complet['record_id']==recordid].values[0]
            #print(error_codes)
            for j in range(len(restored_data.columns)) :
                colj = restored_data.columns[j]
                # if (i%100==0) :
                #     print("checked col : " + colj +" n° "+str(j))
                #     print(error_codes[j])
                #sty = "style5" #init the style
                
                #recordid = format(int(restored_data_1787.iloc[i,1]), '08d')
                #print("searching for :" + recordid)
                #print (df_cod_complet.loc[df_cod_complet['record_id']==int(restored_data_1787.iloc[i,1]),[colj]])
                #print (df_cod_complet.iloc[int(restored_data_1787.iloc[i,1]),:])
                #print (df_cod_complet.loc[str(restored_data_1787.iloc[i,1]),:])
                #print (df_cod_complet.loc[str(recordid),:])
                #print (df_cod_complet.loc[str(restored_data_1787.iloc[i,1]),:][j])
                #print (df_cod_complet.loc[str(restored_data_1787.iloc[i,1]),:][colj]) #Valeur de la colonne j correspondante
                # if colj in error_codes.columns:
                #     print(error_codes.loc[colj])
                # else:
                #     print("col is missing")
                if(error_codes[j] == '-1'):
                    sty = "style4"
                    max_error_level = -1
                    #print(sty +" col "+colj+"  has bad value "+error_codes[j])
                elif (error_codes[j] == '0'):
                    sty = "style2"
                    if max_error_level != -1 : max_error_level = 0
                    #print(sty +" col "+colj+"  has missing value "+error_codes[j])
                elif (error_codes[j] == '1'):
                    sty = "style1"
                    #print(sty +" col "+colj+"  has good value "+error_codes[j])
                elif(error_codes[j] == '2'):
                    sty = "style3"
                    if max_error_level != -1 : max_error_level = 2
                    #print(sty +" col "+colj+"  is Leader A "+error_codes[j])
                elif ( error_codes[j] == '-2'): 
                    sty = "style0"
                    if (j==4  or j==9 or j==13 or j==17 or j==19) :
                        #ship_id, captain_id, birthplace_uhgs_id, ship_flag_id, homeport_uhgs_id, 
                        sty = "style4"
                        #print(sty +" col "+colj+"  has VERY VERY bad value "+error_codes[j])
                    if max_error_level != -1 : max_error_level = -2
                #print(i)
                #print(j)
                _ = ws1.cell(column=j+2, row=i+2, value=restored_data.iloc[i,j])
                ws1.cell(column=j+2,row=i+2).style = sty
            _ = ws1.cell(column=1, row=i+2, value=max_error_level)
            ws1.cell(column=1,row=i+2).style = sty
            #print(i)

        wb.save(filename)



if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_doc.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))   
    c = CheckDocumentCoherence(config)
    c.resetExcelFile(config)
    
    ## Load data
    #data = c.loadDataFromCsv(config)
    data = c.loadDataFromDB(config)

    ## Make the checks
    restored_data_1787, df_cod_complet = c.dothejob(config, data)

    c.outputAnalysis(config, restored_data_1787, df_cod_complet)