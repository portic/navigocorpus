# Contrôles à faire sur navigocorpus

## Enumération des cas repérés comme erreurs

-----------------------------

```sql
SELECT documentary_unit_id, pointcall_uhgs_id,pointcall_indate, pointcall_outdate, * 
FROM navigocheck.check_pointcall
WHERE ship_local_id = '0017223N'
ORDER BY pointcall_rank
```

***pointcall_uhgs_id non renseigné*** 
***le bateau fait une boucle mais pas de 'Z' dans le pointcall_function***

-----------------------------

***Dans la table check_pointcall les first name et last name sont inversés***

-----------------------------


```sql
SELECT pointcall_function, pointcall_rank, pointcall_indate, pointcall_outdate, documentary_unit_id, * 
from navigocheck.check_pointcall
WHERE documentary_unit_id = '00125543'
ORDER BY pointcall_date
```

***pointcall_rank inversé*** 

-----------------------------

```sql
SELECT pointcall_function, pointcall_rank, documentary_unit_id, * 
from navigocheck.check_pointcall
WHERE pointcall_function = 'A' and pointcall_rank != '1.0'
```

***rank >1 mais function 'A'***

-----------------------------

```sql
SELECT pointcall_rank,pointcall_uhgs_id,documentary_unit_id,* 
FROM navigocheck.check_pointcall 
WHERE pointcall_rank is null or documentary_unit_id is null 
```

***Certains pointcall_rank sont null ou le documentary_unit_id est null mais pas le pointcall_rank -> 451 lignes***


-----------------------------

```sql
SELECT captain_local_id,* 
from navigocheck.check_pointcall
WHERE char_length(captain_local_id) >8 --
```

***Local _id supérieur a 8 caractères dérangeant ?***


-----------------------------

***Il y a des entrées comme Portsmouth codées avec un UHGS_id qui n'a pas d'entrée dans Geo_general : B0000720***

```sql
select * from navigoviz.travel where id = 11184
--"B1965595";"B0000720"

select * from navigoviz.port_points 
where uhgs_id in ('B1965595', 'B0000720')
--"B1965595" --> "Saint Pierre et Miquelon"

select * from navigoviz.port_points 
where trim(lower(toponyme)) like 'portsm%th'
--portsmooth : "Portsmouth"

SELECT * FROM navigocheck.check_pointcall where ship_local_id = '0000532N'
--"Saint Pierre et Miquelon"
--"Portsmouth"

SELECT distinct  pointcall_name,  pointcall_uhgs_id , count(*)
FROM navigocheck.check_pointcall 
where trim(lower(pointcall_name)) like 'portsmouth'
group by pointcall_name, pointcall_uhgs_id

----"Portsmouth";"A0385489";42 -- en angleterre, ok
--"Portsmouth";"B0000038";1 -- aux US, ok
--***"Portsmouth";"B0000720";5 # Erreur de geo-codage : aurait pu être codé A0385489***
--***"Portsmouth";"";16 # aurait pu être géocodé mais ne l'est pas *** 

select * from navigoviz.port_points 
where uhgs_id in ('A0385489', 'B0000038', 'B0000720')
```

-- ***B0000720 n'a pas d'entrée dans Geo_general *** (=port_points)

-----------------------------
***Certaine date sont mal renseignée***
- pointcall_date;pkid
> - 1787=02=06! ; 184489
> - 1787=07=03] ; 70863
> - 1787=11=24! ; 185089
> - 1792=02=11] ; 184137
> - 1795)08=21 ; 30235
> - 1795=03=28] ; 28020
> - 1795=11-23 ; 3663
> - 1796=04-16 ; 24807

-----------------------------
***Certain Ship_tonnage ont une syntaxe non documentées***
```
select ship_tonnage from navigo.pointcall group by ship_tonnage order by ship_tonnage;
```
- Notes :
> - ligne 1757 "acheté à Ostende"
> - ligne 1758 "nd"
> - ligne 1759 "null"
> - ligne 959 "0"
> - ligne 530 (405)[45]
> - ligne 552 (428 / 200)
> - ligne 558 (4300 Q)
> - ligne 585 (4575 Q)
> - ligne 1 ((((50)[150])))
> - ligne 2 ((((8)[18])))
> - ligne 3 (((71)))
> - ligne 5 ((113 (?)))

-----------------------------

***Cargo et dictionnaire des commodities***

Mail de Silvia du 6 mai 2019
---

*Pour clarifier les choses: nous insérons les produits comme dans les sources, puis nous attribuons un code-produit à l'item de la cargaison. Ce code_produit est lié à la table Dictonnary_commodity qu ielle comporte les traductions standardisés du produits, et le permanent_code
doc café, caffé, coffee et cafés se voient tous attribuer le même code (en l'occurrance, 00000138) (alors que café Moka a comme code 00000481)
ce code, sur Dictionnary_commodity, renvoie à la traduction dans plusieurs langues (et donc  permet une recherche par plusieurs langues) et il est lié au permanent_coding (dans ce cas précis, trois permanent coding différents, mais qui commencenent tous par ID-GCC)
Voici ce que ça donne  en termes de complétude:
le dictionnary comporte à ce jour 635 code_produit différents, dont 308 sont pourvus d'un permanent coding
Les congés de 1787 comportent un total de 21187 items, dont 21507 déjà pourvus d'un code. (il ne reste donc que les 600 derniers ajouts à coder et ça sera a priori assez vite fait) et 19557 pourvus d'un permanent code. C'est donc une bonne nouvelle car il ne reste pas grande chose à faire. Le b-mol est qu'il y a quelques erreurs de codage (si j'ai corrigé "riz" en "vin", je n'ai pas corrigé le code, ce qui fait qu'il reste lié à "rice" et au permanent_code du riz. )
Pour Marseille nous avons un total de 27000 item de cargaisons dont 22245 pourvus d'un code et 18792 d'un permanent_code. 
Je vais pour ma part me charger de nettoyer les erreurs (je verrais avec Christine si elle peut me prémacher la tâche) et de coller l'identifiant du code là où cela manque (e me faisant expliquer par Jean-Pierre comment en créer de nouveaux si besoin pour des produits qu'on n'aurait jamais rencontré auparavant). 
Le permanent_code et l'équivalent en anglais s'afficheront automatiquement dans la base Pointcall et Christine pourra l'utiliser pour les visualisations.
Il restera donc essentiellement à Pierrick à créer un permanent code (même sommaire, sans distinguer dans le détail) pour les produits codés qui n'en auraient pas. Si on le faisait sur toute la basa Navigo, a serait 325 produits à coder (635-308) , mais si on se limite dans un premier temps aux seuls codes présents dans G5+Marseille, ça sera nettement moins: pour les congés, nous avons un total de 234 codes, dont 164 déjà permacodés: il n'y a donc "que" 70 produits à permacoder
Pour Marseille, nous avons 352 codes de produits différents, dont 219 permacodés, donc il reste 133 produits à permacoder. Comme il y parfois  les mêmes produits dans les deux listes, au total il y a environ  150 produits à permacoder.*

Liste les items prioritaires à permacoder dans le dictionnaire. 
- missing_permanent_codings_....csv

Mais aussi, nous avons remarqué deux choses sur le dictionnaire : 
- des **doublons de record_id** que nous avons corrigé à la main, en fluo dans le fichier Excel ci joint (le numero du record_id).
- des items dans **commodity_standardized de cargo** qu'on ne retrouve pas ni dans aucune des colonnes 
Dictionary_french	Dictionary_english

L' algo de contrôle vérifie
- commodity_purpose <=> commodity_standardized (donc il cherche d'abord dans la colonne en anglais du dictionnaire des commodities)
- commodity_id du dico ayant le   commodity_standardized   <=>  commodity_id de cargo pour la ligne (et parfois çà ne matche pas)


- liste d'items (missing_items_....csv) qu'il faudrait vérifier et ou rajouter dans le dictionnaire des commodities

pour le reste, Clément est en train de coder qui détecte possiblement : 
Flag 11 - 1) les incohérences entre commodity_purpose et commodity_standardized dans cargo (et les équivalent fr ou en du dictionnaire)
Flag 12 - 2) les incohérences entre commodity_standardized de cargo et commodity_id de cargo
Flag 13 - 3) les incohérences entre commodity_id de cargo et permanent_coding du dictionnaire
Flag 14 - 4) les incohérences entre  commodity_permanent_coding   de cargo et permanent_coding du dictionnaire

Pour ces entrées, TODO: listing des pointcall, et cargo correspondants. 

Requetes SQL

```sql
select * from cargo where commodity_id = '00000011'

set search_path = 'navigo', 'navigocheck', 'navigoviz', 'public'

select count(*) from cargo c, pointcall p 
where c.pointcall_id = p.record_id 
and p.component_description__suite_id = '00000003' 
and p.pointcall_date like '1787%'
and commodity_id is not null

select count(*) from cargo c, pointcall p 
where c.pointcall_id = p.record_id 
and p.component_description__suite_id = '00000003' 
and p.pointcall_date like '1787%'
and commodity_permanent_coding is not null

select commodity_purpose, commodity_standardized, commodity_id, commodity_permanent_coding
from cargo c, pointcall p 
where c.pointcall_id = p.record_id 
and p.component_description__suite_id = '00000003' 
and p.pointcall_date like '1787%'
and lower(commodity_purpose) like lower('Wine') 


select distinct  commodity_purpose, commodity_standardized, commodity_id, commodity_permanent_coding
from cargo c, pointcall p 
where c.pointcall_id = p.record_id 
and p.component_description__suite_id = '00000003' 
and p.pointcall_date like '1787%'
and lower(commodity_purpose) like lower('vin') 


create table navigo.dictionnary_commodity (
record_id	text PRIMARY KEY,
dictionary_french	text,
dictionary_english	text,
permanent_coding	text,
links_to_commodity	text)


copy navigo.dictionnary_commodity from 'D:\CNRS\Travail_LIENSs\Projets\ANR_PORTIC\Data\cargo\dictionnaru commodities export.csv' WITH CSV HEADER DELIMITER ';'

 copy  navigo.dictionnary_commodity from '/home/plumegeo/navigo/ETL/data_07mai19/dictionnaru commodities export.csv' WITH CSV HEADER DELIMITER ';'

select commodity_purpose, commodity_standardized, commodity_id, commodity_permanent_coding 
from navigocheck.check_cargo c
where commodity_standardized not in (select distinct dictionary_english from  navigo.dictionnary_commodity )

select similarity('Oil (olive)', 'Olive oil') 
--1

select similarity('Huile (olive)', 'Olive oil') 
--0.4

select similarity('Wine (Bordeaux)', 'Bordeaux wine') 
select similarity('Wine (Ceres)', 'Bordeaux wine') 




-- toutes les entrées manquantes du dictionnaire
select commodity_standardized, max(similarity(commodity_standardized, dictionary_english))
from  navigocheck.check_cargo c, navigo.dictionnary_commodity d 
where c.commodity_standardized % dictionary_english
group by commodity_standardized
having max(similarity(commodity_standardized, dictionary_english)) < 1

-- select commodity_purpose, commodity_standardized, commodity_id, commodity_permanent_coding 
-- from navigocheck.check_cargo c, navigo.dictionnary_commodity d
-- where similarity(commodity_standardized, dictionary_english) < 0.7 
-- not in (select distinct dictionary_english from  navigo.dictionnary_commodity )

 select distinct flag1, flag2  from navigo_qualification
select count(distinct commodity_id) from navigocheck.check_cargo c where commodity_id is not null and commodity_permanent_coding is null
-- 186

```



