# -*- coding: utf-8 -*-
'''
Created on 22 february 2019
@author: cplumejeaud
'''

# Comprendre les imports en Python : http://sametmax.com/les-imports-en-python/
# print sys.path
# sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
## pour avoir le path d'un package
## print (psycopg2.__file__)
## C:\Users\cplume01\AppData\Local\Programs\Python\Python37-32\lib\site-packages\psycopg2\__init__.py

from __future__ import nested_scopes

import logging
import configparser

import xlsxwriter


import random
from xlrd import open_workbook, cellname, XL_CELL_TEXT, XL_CELL_DATE, XL_CELL_BLANK, XL_CELL_EMPTY, XL_CELL_NUMBER, \
    xldate_as_tuple

from LoadFilemaker import LoadFilemaker


class Controls(object):

    def __init__(self, config):
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('Controls')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

        self.loader = LoadFilemaker(config)

    def do_the_job(self, config):
        print("do checks")

    def getCompletedOKdata(self, config, relation):
        print("Build a data table for each field of the relation : nb filled, nb ok")

        query = """SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE
        TABLE_SCHEMA = 'navigo' and TABLE_NAME = '%s' order by ordinal_position""" % (relation)
        rows = self.loader.select_sql(query)
        cols = ", "
        columns = []
        for r in rows :
            columns.append(r[0])
        print (cols.join(columns))

        rows = self.loader.select_sql("select count(pkid) from "+relation)
        total = rows[0][0]
        print ('total: ', total)

        # Create a workbook and add a worksheet.
        print(config['outputs']['file_name'])
        workbook = xlsxwriter.Workbook(config['outputs']['file_name'])
        worksheet = workbook.add_worksheet(relation)

        filtre = config[relation]['filtre']
        print('filtre: ', filtre)
        # Start from the first cell. Rows and columns are zero indexed.
        row = 0
        col = 0
        for c in columns :
            #Nombre de lignes renseignées dans les sources G5 et Marseilles
            query = """ select count(*) from %s where %s 
            and %s is not null """ % (relation, filtre, c)
            rows = self.loader.select_sql(query)
            completed = rows[0][0]
            worksheet.write(row, col, c)
            worksheet.write(row, col + 1, completed)
            #Nombre d'erreurs null tant qu'on n'a pas fait les contrôles
            query = """ select * from navigo.navigo_qualification where navigo_table = '%s' and navigo_field = '%s' 
            and flag1<>9 limit 1 """ % (relation, c)
            erreurs = self.loader.select_sql(query)
            if len(erreurs)>0:
                query = """ select count(*) from navigo.navigo_qualification where navigo_table = '%s' and navigo_field = '%s' and flag1<>9""" % (relation, c)
                rows = self.loader.select_sql(query)
                errors = rows[0][0]
                worksheet.write(row, col + 2, errors)
            row += 1

        workbook.close()

if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_loadfilemaker.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))


    c = Controls(config)
    c.do_the_job(config)

    #c.getCompletedOKdata('geo_general')
    #c.getCompletedOKdata(config, 'pointcall')
    #c.getCompletedOKdata(config, 'cargo')
    c.getCompletedOKdata(config, 'taxes')

    c.loader.close_connection()

    ## pour le lancer sur le serveur
    # nohup python3 Controls.py > out.txt &
