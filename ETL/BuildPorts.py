# -*- coding: utf-8 -*-
'''
Created on 06 june 2020
@author: cplumejeaud
ANR PORTIC : used to build tables for ports : port_points

This requires :

1. to have loaded data (geo_general, port_points_old) into ports manually 
CREATE TABLE ports.geo_general (
	pointcall_name text NULL,
	pointcall_uhgs_id text NULL,
	latitude text NULL,
	longitude text NULL,
	shippingarea text NULL
); -- ok


2. You must restore the file portic_v4.ports.GIS-202006061653.sql ou portic_v5.ports.GIS-20200715.sql in flatdata/GIS to get extra information
* GIS data that are required to build portic gazetteer
- code_levels : list of admiralties with their head-quarters (siège amirauté) (by name). Only those cited in the Chardon report. The overseas provinces are missing (Martinique, Réunion, Guadeloupe)
- obliques : liste of harbors having a clerk and the number of congés (taking off) known from existing records per year
- matching_port : computed using geonames version : 2019 November 
- etats : comes from listings of Silvia Marzagalli : gives belonging states, substates (in fr, en) and range of dates between 1749 and 1815 for all ports
- world_borders : shp downloaded http://www.mappinghacks.com/data/ , last update 30 July 2008
- fermes_ports_csv : alignment of ports with the management and office of the Farm / La Ferme (fiscal entity), 1789, February 2, 2021
- labels_lang_csv : list of toponyms, flags and standardized products in fr and en - February 10, 2021
- generiques_inclusions_geo_csv : list of geographic inclusions between generic ports (* _sup) and ports. A port can belong to more than one generic port, depending on the grouping.
For example, a port in Sweden can also be part of the Baltic Sea. - February 11, 2021
All is packaged like postgres 11.6 tables, using postgis extension. 2.5

3. If you have new points and you want to add admiralty, province, belonging_states or shiparea information, you must open the GIS files and QGIS to set them manually
Many of data can be fetched from old versions of port_points
Manual updates of etats table are to be done also for new points
'''

# Comprendre les imports en Python : http://sametmax.com/les-imports-en-python/
# print sys.path
# sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
## pour avoir le path d'un package
## print (psycopg2.__file__)
## C:\Users\cplume01\AppData\Local\Programs\Python\Python37-32\lib\site-packages\psycopg2\__init__.py

from __future__ import nested_scopes

import logging
import configparser
import xlsxwriter
import os
import psycopg2
#from os import sys, path
import sys, traceback

import json


import random
from xlrd import open_workbook, cellname, XL_CELL_TEXT, XL_CELL_DATE, XL_CELL_BLANK, XL_CELL_EMPTY, XL_CELL_NUMBER, \
    xldate_as_tuple



class BuildPorts(object):

    def __init__(self, config):
        """
        Ouvre les fichiers de log et une connexion à la base de données (en fonction des paramètres de config)
        """
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('BuildPorts')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

        #self = LoadFilemaker(config)

        ## Open both a ssh connexion for copy/remove, and a tunnel for postgres connexion
        self.postgresconn = self.open_connection(config)


    def close_connection(self):
        '''
        Cleanly close DB connection
        :param postgresconn:
        :return:
        '''
        if self.postgresconn is not None:
            self.postgresconn.close()

    def open_connection(self, config):
        '''
        Open database connection with Postgres
        :param config:
        :return:
        '''
        # Acceder aux parametres de configuration
        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')
        # schema = config.get('base', 'schema')
        driverPostgres = 'host=' + host + ' port=' + port + ' user=' + user + ' dbname=' + dbname + ' password=' + password
        self.logger.debug(driverPostgres)

        conn = None
        try:
            conn = psycopg2.connect(driverPostgres)
        except Exception as e:
            self.logger.error("I am unable to connect to the database. " + str(e))
        # Test DB
        if conn is not None:
            cur = conn.cursor()
            cur.execute('select count(*) from pg_namespace')
            result = cur.fetchone()
            if result is None:
                print('open_connection Failed to get count / use of database failed')
            else:
                print('open_connection Got database connexion : ' + str(result[0]))
        else:
            print('open_connection Failed to get database connexion')

        return conn

    def setExtensionsFunctions(self, config):
        """
        Installe les extensions nécessaires à l'import et l'utilisation des données de navigocorpus dans portic, 
        crée les 3 schémas navigo, navigocheck et navigoviz
        et crée les 6 fonctions spécifiques à navigo pour importer les données
        Ces fonctions programmées en PLPython 3 permettent de retirer les crochets et parenthèses des données de navigo 
        pour les conserver épurées dans navigocheck, avec également un code associé à la présence de ces signes
        - 0 : pas de signe
        - -1 : parenthèses autour ()
        - -2 : crochets autour []
        - -4 : donnée manquante
        testé le 14 mai 2020

        encore un bug sur la dernière function frequency_topo, qu'il faut installer à la main avec un fichier SQL 
        """
        #Prerequis : en tant que postgres, 
        #CREATE ROLE dba WITH SUPERUSER NOINHERIT;
        #GRANT dba TO navigo;
        self.logger.info('### setExtensionsFunctions ### 1')
        self.execute_sql('SET ROLE dba')
        self.execute_sql("create extension if not exists postgis")
        self.execute_sql("create extension if not exists fuzzystrmatch")
        self.execute_sql("create extension if not exists pg_trgm")
        self.execute_sql("create extension if not exists postgis_topology")
        self.execute_sql("create extension if not exists plpython3u")
        self.execute_sql("create extension if not exists dblink")

        self.execute_sql("create schema if not exists ports")

        self.logger.info('### setExtensionsFunctions ### 2')

        self.execute_sql("DROP FUNCTION if exists ports.test_double_type(tested_value VARCHAR)") 

        query = """CREATE OR REPLACE FUNCTION ports.test_double_type (tested_value VARCHAR) RETURNS boolean AS 
        $BODY$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists  ports.test_int_type(tested_value VARCHAR)") 

        query = """CREATE OR REPLACE FUNCTION ports.test_int_type (tested_value VARCHAR) RETURNS boolean AS 
        $BODY$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::int'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.logger.info('### setExtensionsFunctions ### 3')

        self.execute_sql("drop type if exists  qual_value cascade")
        query = """CREATE TYPE qual_value AS (
            value   text,
            code  integer
            )"""
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists ports.rm_parentheses_crochets(tested_value text)") 

        #Commented 02 fev 2021 : or tested_value.strip().find(']') > 0
        query = """CREATE OR REPLACE FUNCTION ports.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -4
                        result = None
                else :
                    result = None
                    code = -4
                return [result, code]
            $$ LANGUAGE plpython3u;"""
        self.execute_sql(query)

        # BUG à cause des ''' :+item.replace('\'', '\'\''')+"'"  ##Bonne version : +item.replace('\'', '\'\'')+"'"
        query = """
                CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)
                RETURNS json
                LANGUAGE plpython3u
                AS $function$
                    import json
                    from operator import itemgetter
                    global temp
                    global result
                    global topos
                    global query
                    if toustopo is not None :
                        result = []
                        temp = toustopo.replace('{', '').replace('}', '').strip(' ')
                        temp = temp.replace('"', '').strip(' ')
                        topos = temp.split(',')
                        for item in topos:
                            #plpy.notice(item)
                            #query = "select count(*) as freq from ports.geo_general where pointcall_uhgs_id = '"+uhgs_id+"' and (ports.rm_parentheses_crochets(pointcall_name)).value='"+item.replace('\'', '\'\''')+"'"

                            query = "select count(*) as freq from navigocheck.check_pointcall  where pointcall_uhgs_id = '"+uhgs_id+"' and pointcall_name='"+item.replace('\'', '\'\''')+"'"
                            
                            #plpy.notice(query)
                            rv = plpy.execute(query)
                            #plpy.notice(rv[0]["freq"])
                            result.append(dict(topo=item, freq=rv[0]["freq"]))
                        result = sorted(result, key=itemgetter('freq'), reverse=True)

                    else :
                        result = None
                    #plpy.notice (result)
                    return json.dumps(result, ensure_ascii=False)
                $function$
        """
        #self.execute_sql(query)

        ## A garder ? les autres fonctions ne sont pas à dba ?
        #self.execute_sql("ALTER FUNCTION ports.frequency_topo(text, text) OWNER TO dba")

        self.execute_sql("DROP FUNCTION if exists ports.extract_state_fordate (tested_value text, dateparam int)") 

        query = """CREATE OR REPLACE FUNCTION ports.extract_state_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2) from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_states::json) as elt  
					from ports.port_points p
					where belonging_states = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;
    
                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return null;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists ports.extract_substate_fordate (tested_value text, dateparam int)") 
        query = """CREATE OR REPLACE FUNCTION ports.extract_substate_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates::json) as elt  
					from ports.port_points p
					where belonging_substates = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return null;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE; """
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists ports.extract_state_en_fordate (tested_value text, dateparam int)") 
        query = """CREATE OR REPLACE FUNCTION ports.extract_state_en_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2) from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_states_en::json) as elt  
					from ports.port_points p
					where belonging_states_en = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return null;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists ports.extract_substate_en_fordate (tested_value text, dateparam int)") 
        query = """CREATE OR REPLACE FUNCTION ports.extract_substate_en_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates_en::json) as elt  
					from ports.port_points p
					where belonging_substates_en = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return null;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.execute_sql('RESET ROLE')





    def buildPortTable(self, config):
        """
        A partir de la table geo_general (fabriquée à partir de l'import du codage des pointcall de navigo, table navigo.geo_general), 
        cette fonction fabrique une table port qui liste pour chaque pointcall_uhgs_id (identifiant du port)
        - le toponym le plus fréquent
        - les autres toponymes
        - l'appartenance aux nomenclatures (amirautés, provinces, fermes, pays actuels)
        - les coordonnées géographiques 
        de ce port. 
        
        drop table ports.port_points_old  cascade
        create table ports.port_points_old as (
            select * from ports.port_points pp 
        )-- 1144
        Testée le 03 juin 2020 / février 2021
        La fonction exploite les données stockées dans ports.port_points_old et dans etats, labels_lang_csv
        """

        self.execute_sql("drop table if exists ports.port_points cascade");

        query = """create table if not exists ports.port_points as (
            select pointcall_uhgs_id as uhgs_id, latitude, longitude, null as amiraute, null as province, 
            array_agg(distinct (ports.rm_parentheses_crochets(pointcall_name)).value) as toustopos, (array_agg(distinct shippingarea))[1] as shiparea
            from ports.geo_general
            group by pointcall_uhgs_id, latitude, longitude
        )"""
        self.execute_sql(query)
        
        # table qui va servir pour cartographier les ports issus de geo_general (extraction limitée à 1787 le G5 et Marseille) ; une ligne par code UHGS_id, la clé primaire, et les informations attenantes avec
        self.execute_sql("comment on table ports.port_points is 'Table used for mapping ports extracted from geo_general (1787 and 1789 years, and G5 and Marseille sources) ; one line per UHGS_id code, the primary key, with associated data'")

    

        query = """update ports.port_points set shiparea = gg.shippingarea
        from ports.geo_general gg where  shiparea is null and pointcall_uhgs_id = uhgs_id and shippingarea is not null """
        self.execute_sql(query)


        #Calcul du toponyme le plus fréquent
        self.execute_sql("alter table ports.port_points add column topofreq json")
        self.execute_sql("alter table ports.port_points add column toponyme text")
        query = """update ports.port_points set topofreq = ports.frequency_topo(uhgs_id, array_to_string(toustopos, ',')) where array_length(toustopos, 1) > 0 and ports.test_double_type(longitude) is true"""
        self.execute_sql(query)
        query = """update ports.port_points set toponyme = (topofreq::json->>0)::json->>'topo'"""
        self.execute_sql(query)

        #Récupérer les amirautés et province et shiparea déjà calcules
        query = """update ports.port_points p set amiraute = pp.amiraute , province = pp.province, shiparea=pp.shiparea 
                    from ports.port_points_old pp where pp.uhgs_id = p.uhgs_id """
        self.execute_sql(query)

        #ajouter un point (geometry)
        # update ports.port_points set latitude='36.987377' where uhgs_id = 'B0000959'
        self.execute_sql("alter table ports.port_points add column  geom geometry")
        # query = """
        #    update ports.port_points set geom = st_setsrid(st_makepoint(regexp_replace(longitude, ',', '.')::float, regexp_replace(latitude, ',', '.')::float), 4326)
        # """
        query = """
            update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326)
        """
        self.execute_sql(query)
        self.execute_sql("alter table ports.port_points add column  point3857 geometry")
        query = """
            update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857)
        """
        self.execute_sql(query)

        #Appartenance aux pays actuels

        #self.execute_sql("alter table ports.world_borders add column mpolygone3857 geometry");
        query = """update ports.world_borders set mpolygone3857 = st_setsrid(st_transform(geom, 3857), 3857) 
        where region in (150, 2, 19, 142, 9, 0) and iso2 <>'AQ' """
        '''
        -- 150 : Europe
        -- 2 : Afrique
        -- 19 : Amériques
        -- 142 : Orient / Asie (de la turquie Ã  l'inde)
        -- 9 : Océanie
        '''
        #self.execute_sql(query)

        
        self.execute_sql("alter table ports.port_points add column country2019_name text")
        self.execute_sql("alter table ports.port_points add column country2019_iso2code text")
        self.execute_sql("alter table ports.port_points add column country2019_region text")

        
        query = """ update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_contains(w.geom, p.geom)
        ) as k
        where ports.uhgs_id = k.uhgs_id """
        self.execute_sql(query)

        self.updateCountry(35000)
        self.updateCountry(50000)
        self.updateCountry(100000)
        self.updateCountry(150000)

        # Cas particulier de Lampeduse qui n'est pas dans le fichier world_borders
        query = """update ports.port_points set  country2019_name='Italy', country2019_iso2code ='IT', country2019_region ='150'
            where uhgs_id='A0249937'"""
        self.execute_sql(query)
    
        # Rajouter un identifiant entier unique pour l'export des features

        self.execute_sql("alter table ports.port_points add column ogc_fid serial")
        self.execute_sql("alter table ports.port_points add primary key (ogc_fid)")
        
        query = """select count(*) from ports.port_points where country2019_iso2code is null and point3857 is not null"""
        rows = self.select_sql(query);
        for rowk in rows:
            self.logger.error('Nombre de ports.port_points sans country: '+str(rowk[0]))
            print ('Nombre de ports.port_points sans country: ', str(rowk[0]))

    

    def buildPortTableSuite(self, config):
    
        #Appartenance aux états historiques
       
        # 1. attribuer la suite de relations d'appartenance et leurs date à  state
        self.execute_sql("alter table ports.port_points add column belonging_states text")
        self.execute_sql("alter table ports.port_points add column belonging_substates text")
        self.execute_sql("alter table ports.port_points add column belonging_states_en text")
        self.execute_sql("alter table ports.port_points add column belonging_substates_en text")

        query = """update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, etat, case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat_en) :: text as belonging_en
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)


        query = """update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
            from (
                select  uhgs_id, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances
                from ports.port_points 
                where belonging_states is null and country2019_name = 'France'
                union 
                (select 'A0146289' as uhgs_id, json_build_object(1749 ||'-'||1815, 'Iceland')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'Islande')::text as appartenances)
            ) as k 
        where pp.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)

        # Préciser le sous-etat (etat.subunit) dans belonging_substates

        query = """update ports.port_points pp set belonging_substates = '['||k.appartenances ||']', belonging_substates_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, subunit,  case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit_en) :: text as belonging_en
                from ports.etats 
                WHERE subunit IS NOT null
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)

        ## Extraire l'appartenance pour 1789 (identique aussi à 1787) en français, comme une chaine de charactère simple donnant le nom du pays ou sous-état . 
        self.execute_sql("alter table ports.port_points add column state_1789_fr text ")
        self.execute_sql("alter table ports.port_points add column substate_1789_fr text ")
        query = """update ports.port_points set substate_1789_fr = ports.extract_substate_fordate(belonging_substates, 1789) 
            where belonging_substates is not null"""
        self.execute_sql(query)
        query = """update ports.port_points set state_1789_fr = ports.extract_state_fordate(belonging_states, 1789) 
            where belonging_states is not null"""
        self.execute_sql(query)

        ## Extraire l'appartenance pour 1789 (identique aussi à 1787) en anglais, comme une chaine de charactère simple donnant le nom du pays ou sous-état. 
        self.execute_sql("alter table ports.port_points add column state_1789_en text ")
        self.execute_sql("alter table ports.port_points add column substate_1789_en text ")
        query = """update ports.port_points set substate_1789_en = ports.extract_substate_en_fordate(belonging_substates_en, 1789) 
                where belonging_substates_en is not null"""
        self.execute_sql(query)
        query = """update ports.port_points set state_1789_en = ports.extract_state_en_fordate(belonging_states_en, 1789) 
            where belonging_states_en is not null"""
        self.execute_sql(query)

        # Le port est oblique ou non (il a disposé d'un greffier)
        # Non on a récupéré de ports.port_points_old

        # self.execute_sql("alter table ports.port_points add column oblique boolean")
        # self.execute_sql("alter table ports.port_points add column has_a_clerk boolean")

        # self.execute_sql("CREATE INDEX if not Exists trgm_toponyme_idx ON ports.port_points USING GIST (toponyme gist_trgm_ops)")
        # self.execute_sql(" update ports.port_points pp set oblique = true, has_a_clerk=true from ports.obliques o where pp.uhgs_id =o.uhgs_id");
        # self.execute_sql(" update ports.port_points pp set oblique = true, has_a_clerk=true where pp.uhgs_id in ('B1965595', 'B2009606')");
        # # Saint Pierre et Miquelon/ B1965595
        # # Cayenne/B2009606

        # # On a le registre des congés de ce port en 1787 et en 1789
        # self.execute_sql("alter table ports.port_points add column source_1787_available boolean default false")
        # self.execute_sql("alter table ports.port_points add column source_1789_available boolean  default false")
        # self.execute_sql(" update ports.port_points pp set source_1787_available = true from ports.obliques o where pp.uhgs_id =o.uhgs_id and d1787 is not null");
        # self.execute_sql(" update ports.port_points pp set source_1789_available = true from ports.obliques o where pp.uhgs_id =o.uhgs_id and d1789 is not null");
        # self.execute_sql(" update ports.port_points pp set source_1787_available = true , source_1789_available = true where pp.uhgs_id in ('B1965595', 'B2009606')");


        # # Le status du port : siège d'amiraute / oblique / null
        # self.execute_sql("alter table ports.port_points add column status text default null")
        # self.execute_sql("update ports.port_points set status = 'oblique' where has_a_clerk is true")

        # query = """update ports.port_points n set status = 'siège amirauté' from
        # (SELECT o.amiraute, toponyme, pp.ogc_fid, similarity(toponyme, o.amiraute) AS sml
        # FROM ports.port_points pp, ports.codes_levels o
        # WHERE has_a_clerk is true  and o.amiraute % toponyme   
        # and similarity(o.amiraute, toponyme) > 0.8
        # order by o.amiraute_code 
        # ) as q
        # where q.ogc_fid = n.ogc_fid"""
        # self.execute_sql(query)

        # query = """update ports.port_points n set status = 'siège amirauté' where uhgs_id in 
        # ('A0152606', 'A0196771', 'A0212320', 'A0135548', 'A0187101', 'A0173748', 'A0189004', 'A0138383', 'A0170986')"""
        # self.execute_sql(query)


        # self.execute_sql("alter table ports.port_points add column toponyme_standard_fr text default null")
        # self.execute_sql("alter table ports.port_points add column toponyme_standard_en text default null")
        # # Mise à jour des toponymes standardisés par Silvia, mais sauvés dans une autre table que etat. La table labels. 
        # query = """update ports.port_points p set toponyme_standard_fr = trim(m.fr) , toponyme_standard_en= trim(m.en)
        # from ports.labels_lang_csv m where p.uhgs_id = m.key_id and label_type='toponyme'"""
        # self.execute_sql(query)

        self.execute_sql("alter table ports.port_points add column geonameid int default null")
        #Récupérer les geonameid déjà calculés
        query = """update ports.port_points p set geonameid = pp.geonameid  
                    from ports.port_points_old pp where pp.uhgs_id = p.uhgs_id """
        self.execute_sql(query)


        #Creation de relation_state pour le gazettier (require geonameid)
        self.execute_sql("alter table ports.port_points  add column relation_state text")

        query = """update ports.port_points pp set relation_state = k.appartenances
                from (
                select uhgs_id, json_agg(arelation) as appartenances
                    from
                    (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                    case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
                    'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
                    from (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is not null
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is null
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is not null 
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is  null 
                        )
                    ) as k 
                    group by uhgs_id, etat, geonameid, tgnid
                    ) as k
                    group by uhgs_id
                    order by uhgs_id
                ) as k
                where pp.uhgs_id = k.uhgs_id
        """
        self.execute_sql(query)

        #For French and 1 particulars cases, where set relation_state without reading ports.etats
        query = """
        update ports.port_points pp set relation_state = k.appartenances ::text
            from (
            select uhgs_id,  json_agg(arelation) as appartenances
                from
                (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                'http://www.geonames.org/'||geonameid ,
                'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
                from (
                    
                    select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    from ports.port_points 
                    where relation_state is null and country2019_name = 'France'
                    union all
                    (
                    select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    )
                ) as k 
                group by uhgs_id, etat, geonameid
                ) as k
                group by uhgs_id
                order by uhgs_id
            ) as k
            where pp.uhgs_id = k.uhgs_id
        """
        self.execute_sql(query)

        #remove fist [ and last ] from the array of json objects in relation_state 
        query = "update ports.port_points set relation_state = substring(relation_state from 2 for length(relation_state)-2) where substring (relation_state from 1 for 1)='[';"
        self.execute_sql(query)

        self.execute_sql("alter table ports.port_points add column ferme_direction text ")
        self.execute_sql("alter table ports.port_points add column ferme_bureau text ")
        self.execute_sql("alter table ports.port_points add column ferme_bureau_uncertainty int ")
        query = """update ports.port_points p set ferme_direction = f.ferme_direction , ferme_bureau = f.ferme_bureau , 
                ferme_bureau_uncertainty=f.ferme_bureau_uncertainty from ports.fermes_ports_csv f where f.uhgs_id = p.uhgs_id """
        self.execute_sql(query)
        
        self.execute_sql("alter table ports.port_points add column partner_balance_1789 text ")
        self.execute_sql("alter table ports.port_points add column partner_balance_supp_1789 text")
        self.execute_sql("alter table ports.port_points add column partner_balance_1789_uncertainty int")
        self.execute_sql("alter table ports.port_points add column partner_balance_supp_1789_uncertainty int")

        # Etranger pour partner_balance_supp_1789
        query = """update ports.port_points set partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where not (state_1789_fr  in ('France') or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Sénégal'))"""
        self.execute_sql(query)
        query = """update ports.port_points set partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  in ('France') and substate_1789_fr is null"""
        self.execute_sql(query)
        query = """update ports.port_points set partner_balance_supp_1789='colonies françaises', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  in ('France') and substate_1789_fr is not null"""
        self.execute_sql(query)
        #Doute : le Sénégal est à la fois Sénégal et Afrique ET colonies françaises ?
        query = """update ports.port_points set partner_balance_supp_1789='Sénégal et Guinée', partner_balance_supp_1789_uncertainty=-1
            where uhgs_id in (select uhgs_id from ports.port_points where country2019_region = '2'
            and country2019_name in ('Senegal', 'Equatorial Guinea' , 'Gambia', 'Nigeria')) or uhgs_id in ('C0000006')"""
        self.execute_sql(query)

        #Alignement pour partner_balance_1789

        #  Sénégal : Sénégal	dans Navigo pas de départ pour le Sénégal mais plusierus négrier qui vont vers autres côtes africaines. Faudra comprendre s'ils sont dedans
        query = """update ports.port_points set partner_balance_1789='Sénégal', partner_balance_1789_uncertainty=-1
        where uhgs_id in (select uhgs_id from ports.port_points where country2019_name = 'Senegal')"""

        # Autriche + Mecklembourg; Oldenburg ; Papenburg ; Flandre autrichienne
        query = """update ports.port_points set partner_balance_1789 = 'Etats de l''Empereur' ,  partner_balance_1789_uncertainty=-1
        where state_1789_fr in ('Autriche', 'Duché de Mecklenbourg', 'Duché d''Oldenbourg', 'Evêché de Münster')"""
        self.execute_sql(query)

        query = """update ports.port_points set partner_balance_1789 = 'Prusse' ,  partner_balance_1789_uncertainty=0
        where state_1789_fr in ('Prusse')"""
        self.execute_sql(query)

        # Quatre villes hanséatiques : 	Hambourg, Brême, Lübeck, Ville libre de Dantzig
        query = """update ports.port_points set partner_balance_1789 = 'Quatre villes hanséatiques' , partner_balance_1789_uncertainty=0
        where state_1789_fr in ('Hambourg', 'Lubeck', 'Brême') or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Dantzig')"""
        self.execute_sql(query)

        # Etats-Unis	Etats-Unis d'Amérique
        query = """update ports.port_points set partner_balance_1789 = 'Etats-Unis' ,  partner_balance_1789_uncertainty=0
        where state_1789_fr in ('Etats-Unis d''Amérique')"""
        self.execute_sql(query)

        # 	Angleterre : Grande-Bretagne (colonies comprises)
        query = """update ports.port_points set partner_balance_1789='Angleterre',  partner_balance_1789_uncertainty=0
        where state_1789_fr = 'Grande-Bretagne'"""
        self.execute_sql(query)

        # Danemark	Danemark (donc DK et Norvège)	
        query = """update ports.port_points set partner_balance_1789='Danemark', partner_balance_1789_uncertainty=0
        where state_1789_fr = 'Danemark'"""
        self.execute_sql(query)

        # Espagne	Espagne (colonies comprises) 
        query = """update ports.port_points set partner_balance_1789='Espagne',  partner_balance_1789_uncertainty=0
        where state_1789_fr = 'Espagne'"""
        self.execute_sql(query)

        # Hollande	Provinces-Unies 
        query = """update ports.port_points set partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0
        where state_1789_fr = 'Provinces-Unies'"""
        self.execute_sql(query)
        #	Iles françaises de l'Amérique : colonies françaises d'Amérique	
        query = """update ports.port_points set partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0
        where substate_1789_fr  = 'Colonies françaises d''Amérique'"""
        self.execute_sql(query)

        # Portugal :	Portugal (colonies comprises)
        query = """update ports.port_points set partner_balance_1789='Portugal',  partner_balance_1789_uncertainty=0
        where state_1789_fr  = 'Portugal'"""
        self.execute_sql(query)

        # Russie :	Russie
        query = """update ports.port_points set partner_balance_1789='Russie', partner_balance_1789_uncertainty=0
        where state_1789_fr  = 'Empire russe'"""
        self.execute_sql(query)

        # Suède	Suède (donc Suède, Poméranie suédoise, et Finlande)
        query = """update ports.port_points set partner_balance_1789='Suède', partner_balance_1789_uncertainty=0
        where state_1789_fr  = 'Suède'"""
        self.execute_sql(query)

        # Ports francs, petites iles et ports particuliers de France

        # Bayonne	Bayonne (port franc) + vérifier s'il faut ajouter Saint Jean de Luz parmi ports francs (3 navires)
        self.execute_sql("update ports.port_points set partner_balance_1789='Bayonne', partner_balance_1789_uncertainty=0 where uhgs_id = 'A0187995'")
        # Dunkerque	Dunkerque	port franc
        self.execute_sql("update ports.port_points set partner_balance_1789='Dunkerque',  partner_balance_1789_uncertainty=0 where uhgs_id = 'A0204180'")
        # Marseille : Marseille	port franc
        self.execute_sql("update ports.port_points set partner_balance_1789='Marseille',  partner_balance_1789_uncertainty=0 where uhgs_id = 'A0210797'")
        # Lorient	: Lorient	port franc
        self.execute_sql("update ports.port_points set partner_balance_1789='Lorient', partner_balance_1789_uncertainty=0 where uhgs_id = 'A0140266'")
        #  Petites Iles : île de Bouin A0165077 et île de Noirmoutier A0136403 (prendre tous les pointcall 	demander confimation à Thierry et/ou tester avec données Navigo
        self.execute_sql("update ports.port_points set partner_balance_1789='Petites Iles', partner_balance_1789_uncertainty=-1 where uhgs_id in ('A0165077', 'A0136403')")
        # Saint-Domingue	dans "colonies françaises d'Amérique" prendre uniquement les ports de Saint-Domingue (=Haïti actuel) et le pointcall "Saint-Domingue" générique
        self.execute_sql("update ports.port_points set partner_balance_1789='Saint-Domingue',  partner_balance_1789_uncertainty=0 where province % 'Domingue' ")
        # Saint-Jean de Luz	: Saint-Jean de Luz
        self.execute_sql("update ports.port_points set partner_balance_1789='Saint-Jean de Luz',  partner_balance_1789_uncertainty=-1 where uhgs_id = 'A0122594'")

    def updatePortStates(self, config):
        #Appartenance aux états historiques
       
        # 1. attribuer la suite de relations d'appartenance et leurs date à  state
        query = """update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, etat, case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat_en) :: text as belonging_en
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)


        query = """update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
            from (
                select  uhgs_id, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances
                from ports.port_points 
                where belonging_states is null and country2019_name = 'France'
                union 
                (select 'A0146289' as uhgs_id, json_build_object(1749 ||'-'||1815, 'Iceland')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'Islande')::text as appartenances)
            ) as k 
        where pp.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)

        # Préciser le sous-etat (etat.subunit) dans belonging_substates

        query = """update ports.port_points pp set belonging_substates = '['||k.appartenances ||']', belonging_substates_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, subunit,  case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit_en) :: text as belonging_en
                from ports.etats 
                WHERE subunit IS NOT null
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)

        ## Extraire l'appartenance pour 1789 (identique aussi à 1787) en français, comme une chaine de charactère simple donnant le nom du pays ou sous-état . 
        query = """update ports.port_points set substate_1789_fr = ports.extract_substate_fordate(belonging_substates, 1789) 
            where belonging_substates is not null"""
        self.execute_sql(query)
        query = """update ports.port_points set state_1789_fr = ports.extract_state_fordate(belonging_states, 1789) 
            where belonging_states is not null"""
        self.execute_sql(query)

        ## Extraire l'appartenance pour 1789 (identique aussi à 1787) en anglais, comme une chaine de charactère simple donnant le nom du pays ou sous-état. 
        query = """update ports.port_points set substate_1789_en = ports.extract_substate_en_fordate(belonging_substates_en, 1789) 
                where belonging_substates_en is not null"""
        self.execute_sql(query)
        query = """update ports.port_points set state_1789_en = ports.extract_state_en_fordate(belonging_states_en, 1789) 
            where belonging_states_en is not null"""
        self.execute_sql(query)


        #Update de relation_state pour le gazettier (require geonameid)
        query = """update ports.port_points pp set relation_state = k.appartenances
                from (
                select uhgs_id, json_agg(arelation) as appartenances
                    from
                    (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                    case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
                    'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
                    from (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is not null
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is null
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is not null 
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is  null 
                        )
                    ) as k 
                    group by uhgs_id, etat, geonameid, tgnid
                    ) as k
                    group by uhgs_id
                    order by uhgs_id
                ) as k
                where pp.uhgs_id = k.uhgs_id
        """
        self.execute_sql(query)

        #For French and 1 particulars cases, where set relation_state without reading ports.etats
        query = """
        update ports.port_points pp set relation_state = k.appartenances ::text
            from (
            select uhgs_id,  json_agg(arelation) as appartenances
                from
                (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                'http://www.geonames.org/'||geonameid ,
                'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
                from (
                    
                    select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    from ports.port_points 
                    where relation_state is null and country2019_name = 'France' 
                    and uhgs_id not in (select uhgs_id from ports.etats e) --enlever les ports français (ex: Menton Roquebrune) qui ont été étrangers
                    union all
                    (
                    select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    )
                ) as k 
                group by uhgs_id, etat, geonameid
                ) as k
                group by uhgs_id
                order by uhgs_id
            ) as k
            where pp.uhgs_id = k.uhgs_id
        """
        self.execute_sql(query)

        #remove fist [ and last ] from the array of json objects in relation_state 
        query = "update ports.port_points set relation_state = substring(relation_state from 2 for length(relation_state)-2) where substring (relation_state from 1 for 1)='[';"
        self.execute_sql(query)


    def useGeonames(self, config) : 
        self.execute_sql("drop  view IF EXISTS ports.myremote_geonamesplaces")
        query = """create materialized VIEW ports.myremote_geonamesplaces AS
            SELECT *
            FROM dblink('dbname=geonames user=postgres password=postgres options=-csearch_path=',
                        'select geonameid::int, name, (feature_class||''.''||feature_code) as feature_code, alternatenames, country_code,  latitude::float, longitude::float , point3857
                            from geonames.geonames_nov2019.allcountries a 
                            where feature_class||''.''||feature_code in (select code from geonames.geonames_nov2019.feature_code where keep = ''x'')')
            AS t1(geonameid int, name text, feature_code text, alternatenames text , country_code text, latitude float, longitude float, point3857 geometry)"""
        # --- 34 s, 5 012 322 lignes
        self.execute_sql(query)

        self.execute_sql("CREATE INDEX trgm_geoname_name_idx ON ports.myremote_geonamesplaces USING GIST (name gist_trgm_ops)");
        #-- 1 min 22s

        self.execute_sql("CREATE INDEX gist_geoname_point_idx ON ports.myremote_geonamesplaces USING GIST (point3857)");
        #-- 55 s


    def computeMatching(self, config) : 
        """
        Compute the geoname id of each geo_general point
        not tested yet
        """
        ## self.execute_sql("drop table IF EXISTS matching_port")
        query = """ create table if not exists ports.matching_port (
            source1 text,
            source2 text,
            id1 int, 
            id2 int, 
            uhgs_id text,
            topo1 text,
            topo2 text,
            simtext float,
            distgeo float,
            best boolean default false,
            certainity int default 1)"""
        self.execute_sql(query)


        query = """comment on table ports.matching_port is 'table de calcul des meilleurs appariements entre les ports de géogenéral - port_points, geonames, 
        et les ports saisis - saisie_13fev2020'"""
        self.execute_sql(query)

        query = """comment on column ports.matching_port.certainity is 'Certitude levels : 2 is less certain (multiple representation for instance) than 1'"""
        self.execute_sql(query)

        
        # Certitude levels : 2 is less certain (multiple representation for instance) than 1


        # Tous les lieux de geonames à moins de 5 km
        query = """insert into matching_port(source1, id1, uhgs_id, topo1, source2, id2, topo2, distgeo, simtext)
        select 'geo_general' as source1, pp.ogc_fid as id1, pp.uhgs_id, pp.toponyme,  'geonames' as source2, s.geonameid, s.toponyme , st_distance(s.point3857, pp.point3857 ), similarity(s.toponyme, pp.toponyme)
        from 
        (select ogc_fid, uhgs_id, unnest(toustopos) as toponyme, point3857 from ports.port_points where geonameid is null) as pp,
        (select geonameid, name as toponyme, point3857 from myremote_geonamesplaces ) as s
        where st_distance(s.point3857, pp.point3857 ) < 5000 
        """
        self.execute_sql(query)

        # 23023 lignes, 132 min ! 


        # 1) m.simtext = k.max and m.distgeo = k.min
        query = """update matching_port m set best = true , certainity = 1
        from (
            select distinct m.id1, m.id2, m.simtext, m.distgeo
            from matching_port m,
            (select id1, max(simtext), min(distgeo)  from matching_port where simtext > 0 and source1 = 'geo_general' and source2='geonames' group by id1 ) as k 
            where source1 = 'geo_general' and source2='geonames' and m.id1 = k.id1 and m.simtext = k.max and m.distgeo = k.min
        ) as k 
        where m.source1 = 'geo_general' and m.source2='geonames' and m.id1 = k.id1 and m.id2 = k.id2 and m.simtext = k.simtext and m.distgeo = k.distgeo"""
        self.execute_sql(query)

       
        query = """update ports.port_points p set geonameid = id2
        from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and geonameid is null"""
        self.execute_sql(query)

        # 2) m.simtext = k.max 
        query = """update matching_port m set best = true , certainity = 2
        from (
            select distinct m.topo1, m.topo2, m.id1, m.id2, m.simtext, m.distgeo, m.best
            from ports.port_points p, matching_port m, 
            (select id1, max(simtext), min(distgeo)  from matching_port where simtext > 0 and source1 = 'geo_general' and source2='geonames' group by id1 ) as k 
            where source1 = 'geo_general' and source2='geonames' and m.id1 = k.id1 and m.simtext = k.max 
            and p.ogc_fid = m.id1 and p.geonameid is null
        )
        as k 
        where m.source1 = 'geo_general' and m.source2='geonames' and m.id1 = k.id1 and m.id2 = k.id2 and m.simtext = k.simtext and m.distgeo = k.distgeo"""	
        self.execute_sql(query)

        query = """update ports.port_points p set geonameid = id2
        from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and geonameid is null"""
        self.execute_sql(query)
        # 867

        # 3) m.simtext = 0 and m.distgeo = k.min
        query = """update matching_port m set best = true, certainity = 3
        from (
            select distinct m.topo1, m.topo2, m.id1, m.id2, m.simtext, m.distgeo, m.best
            from ports.port_points p, matching_port m, 
            (select id1, max(simtext), min(distgeo)  from matching_port where  source1 = 'geo_general' and source2='geonames' group by id1 ) as k 
            where source1 = 'geo_general' and source2='geonames' and m.id1 = k.id1 and m.distgeo = k.min
            and p.ogc_fid = m.id1 and p.geonameid is null
        )
        as k 
        where m.source1 = 'geo_general' and m.source2='geonames' and m.id1 = k.id1 and m.id2 = k.id2 and m.simtext = k.simtext and m.distgeo = k.distgeo"""	
        self.execute_sql(query)
        # 71

        query = """update ports.port_points p set geonameid = id2
        from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and geonameid is null"""
        self.execute_sql(query)


    def updateStateGeonameid(self, config) : 
        """
        Not finished. SQL need only to be integrated with python
        All required SQL is here, except for the build of the geoname database (all is explained on geoname site)
        A lot of manual operations. This lead to etats table. Work with UTF_8 encoding.
        """
        '''
        create extension dblink;
        alter extension dblink set schema public;


        drop view myremote_geonames
        create or replace VIEW myremote_geonames AS
        SELECT *
            FROM dblink('dbname=geonames user=postgres password=postgres options=-csearch_path=',
                        'select geonameid, name, feature_code, country_code, admin1_code, latitude, longitude from geonames.geonames_nov2019.allcountries a 
                        where feature_class =''A'' and feature_code like ''P%'' or feature_code = ''TERR'' or feature_code like ''Z'' ')
            AS t1(geonameid int, name text, feature_code text, country_code text, admin1_code text, latitude float, longitude float);
        
        select * from myremote_geonames
        -- 466 etats

        alter table ports.etats add column geonameid int;
        alter table ports.etats add column name_en text;
        alter table ports.etats add column admin1_code text;
        alter table ports.etats add column latitude float;
        alter table ports.etats add column longitude float;
        alter table ports.etats add column tgnid int;
        alter table ports.etats add column wikipedia text;
        alter table ports.etats add column admin2_code text;
        alter table ports.etats add column admin3_code text;

        update ports.etats set geonameid = 3165361, wikipedia='https://fr.wikipedia.org/wiki/%C3%89trurie'  where etat = 'Royaume d''Étrurie';
        update ports.etats set geonameid = 2635167  where etat = 'Grande-Bretagne';
        update ports.etats set geonameid = 2510769  where etat = 'Espagne';
        update ports.etats set geonameid = 6252001  where etat = 'USA';
        update ports.etats set geonameid = 2750405  where etat = 'Provinces-Unies';
        update ports.etats set geonameid = 2750405  where etat = 'Royaume d''Hollande';
        update ports.etats set geonameid = 2750405  where etat = 'Royaume de Hollande';
        update ports.etats set etat = 'Royaume de Hollande'  where etat = 'Royaume d''Hollande';
        update ports.etats set geonameid = 798544  where etat = 'Pologne';
        update ports.etats set geonameid = 2215636  where etat = 'Régence de Tripoli';
        update ports.etats set geonameid = 2661886  where etat = 'Suède';
        update ports.etats set geonameid = 2264397  where etat = 'Portugal';
        update ports.etats set geonameid = 1814991  where etat = 'Chine';
        update ports.etats set geonameid = 298795  where etat = 'Empire ottoman';
        update ports.etats set geonameid = 3017382  where etat = 'Empire français';
        update ports.etats set geonameid = 3017382  where etat = 'France';
        update ports.etats set geonameid = 2542007  where etat = 'Empire du Maroc';
        update ports.etats set geonameid = 3175395  where etat = 'Royaume d''Italie';
        update ports.etats set geonameid = 2782113  where etat = 'Autriche';
        update ports.etats set geonameid = 2623032  where etat = 'Danemark';
        update ports.etats set geonameid = 2017370  where etat = 'Russie';
        update ports.etats set geonameid = 2993457  where etat = 'Monaco';
        update ports.etats set geonameid = 2562770  where etat = 'Malte';
        update ports.etats set geonameid = 2993457, tgnid=7005289  where etat = 'Hambourg';
        update ports.etats set geonameid = 2944387  where etat = 'Brême';
        update ports.etats set geonameid = 2872567  where etat = 'Duché de Mecklenbourg';
        update ports.etats set geonameid = 3165361  where etat = 'Toscane';
       update ports.etats set geonameid = 3164600 where etat = 'République de Venise';
        update ports.etats set geonameid = 3174725  where etat = 'République ligurienne'; 
       update ports.etats set geonameid = 3176217, tgnid=7008546  where etat = 'République de Gênes';
        update ports.etats set geonameid = 7577034, tgnid = 7015500, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_de_Raguse'  where etat = 'République de Raguse';
        update ports.etats set geonameid = 3164670, tgnid=7009981, wikipedia='https://fr.wikipedia.org/wiki/%C3%89tats_pontificaux'  where etat = 'Etats pontificaux';
        update ports.etats set geonameid = 3164670, tgnid=7009981, wikipedia='https://fr.wikipedia.org/wiki/%C3%89tats_pontificaux'  where etat = 'Etats Pontificaux';
       update ports.etats set etat = 'Etats Pontificaux' where etat = 'Etats pontificaux';
      
        update ports.etats set geonameid = 2523228, wikipedia='https://fr.wikipedia.org/wiki/Royaume_de_Sardaigne_(1720-1861)'  where etat = 'Royaume de Piémont-Sardaigne';
        update ports.etats set geonameid = 3174976, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_romaine_(1849)'  where etat = 'République romaine';
        update ports.etats set geonameid = 3174529, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_de_Lucques'  where etat = 'République de Lucques';
        update ports.etats set geonameid = 6541646, wikipedia='https://fr.wikipedia.org/wiki/%C3%89trurie'  where etat = 'Principauté de Piombino';
        update ports.etats set geonameid = 3173767, wikipedia='https://fr.wikipedia.org/wiki/Duch%C3%A9_de_Massa_et_Carrare'  where etat = 'Duché de Massa et Carrare';
        update ports.etats set geonameid = 3249071, wikipedia='https://fr.wikipedia.org/wiki/L%C3%BCbeck'  where etat = 'Lubeck';
        update ports.etats set geonameid = 3221095, wikipedia='https://fr.wikipedia.org/wiki/Duch%C3%A9_d%27Oldenbourg'  where etat = 'Duché d''Oldenbourg';
        update ports.etats set geonameid = 6697805, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_des_Sept-%C3%8Eles' where etat ='République de Sept-Îles';
        
       update ports.etats SET geonameid=3172394, tgnid=7003012, wikipedia='https://fr.wikipedia.org/wiki/Royaume_de_Naples' where etat = 'Royaume de Naples';
        update ports.etats set tgnid = 7016646, wikipedia='https://fr.wikipedia.org/wiki/Duch%C3%A9_de_Courlande'  where etat = 'Duché de Courlande';
        update ports.etats set tgnid = 7016786  where etat = 'Prusse';
       
       update ports.etats set geonameid=6285698, wikipedia='https://fr.wikipedia.org/wiki/Ville_libre_de_Dantzig' where etat = 'Ville libre de Dantzig';
       update ports.etats set geonameid=3723988, wikipedia='https://fr.wikipedia.org/wiki/Ha%C3%AFti' where etat = 'Haïti';
	   update ports.etats set geonameid=2750405, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_batave' where etat = 'République batave';
	   update ports.etats set geonameid=2750405, wikipedia='https://fr.wikipedia.org/wiki/Royaume_de_Hollande' where etat = 'Royaume des Pays-Bas';
	   update ports.etats set geonameid=6541107, tgnid=6005618, wikipedia='https://fr.wikipedia.org/wiki/%C3%89tat_des_Pr%C3%A9sides' where etat = 'Etat des Présides';
	   update ports.etats set geonameid=2324774, tgnid=1091915, wikipedia='https://fr.wikipedia.org/wiki/Bonny' where etat = 'Royaume de Bonny';

	   update ports.etats set geonameid=6285698, wikipedia='https://fr.wikipedia.org/wiki/Ville_libre_de_Dantzig' where uhgs_id = 'A1204354';
        update ports.etats set geonameid=2524459, wikipedia='https://fr.wikipedia.org/wiki/Lampedusa' where uhgs_id = 'A0249937';

	

       select distinct etat  from ports.etats where geonameid is null
        /*
        * zone maritime
        Duché de Courlande
        Prusse
        multi-Etat
        */

        update etats e set name_en=g.name, admin1_code=g.admin1_code, latitude=g.latitude, longitude=g.longitude 
        from myremote_geonames g
        where g.geonameid = e.geonameid


        update etats e set name_en=g.name, admin1_code=g.admin1_code, latitude=g.latitude, longitude=g.longitude 
         FROM dblink('dbname=geonames user=postgres password=postgres options=-csearch_path=',
                        'select geonameid::int, name, admin1_code,  latitude::float, longitude::float
                            from geonames.geonames_nov2019.allcountries a 
                             ')
            AS g(geonameid int, name text, admin1_code text, latitude float, longitude float)
        where g.geonameid = e.geonameid and name_en is null

        '''

    def buildPortDeprecated (self, config) : 
        # Mise à jour des états avec geoname (TODO)
        # self.updateStateGeonameid(config)

        """
        * 1. attribuer l'agrégat des etats, geonameids, name_ens à  state_labels, state_geonameids, state_labels_en
        * 2. attribuer  l'état de country2019_name à  state_labels, state_geonameids, state_labels_en si state_labels est vide
        * 3. attribuer la suite de relations d'appartenance et leurs date à  state
        """
        
        '''
        self.execute_sql("alter table ports.port_points add column state_labels text")
        self.execute_sql("alter table ports.port_points add column state_geonameids text")
        self.execute_sql("alter table ports.port_points add column state_labels_en text")

        # 1. attribuer l'agrégat des etats, geonameids, name_ens à  state_labels, state_geonameids, state_labels_en
        query = """update ports.port_points p set state_labels=k.state_labels, state_geonameids=k.state_geonameids, state_labels_en=k.state_labels_en
        from (
            select p.uhgs_id, p.toponyme, p.country2019_name, array_agg( e.dfrom ||'-'|| e.dto) as aperiod, array_agg( distinct e.etat) as state_labels, array_agg(distinct e.geonameid) as  state_geonameids, array_agg(distinct e.name_en) as state_labels_en
            from ports.port_points p, ports.etats e
            where p.uhgs_id =e.uhgs_id 
            -- and e.etat = 'Etats pontificaux'
            group by p.uhgs_id, p.toponyme, p.country2019_name
            order by aperiod
        ) as k
        where p.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)

        # 2. attribuer  l'état de country2019_name à  state_labels, state_geonameids, state_labels_en si state_labels est vide
        # Cas majoritaire de la France
        query = """update ports.port_points p set state_labels=k.state_labels, state_geonameids=k.state_geonameids, state_labels_en=k.state_labels_en
        from (
            select p.uhgs_id, p.toponyme, p.country2019_name, array_agg(country2019_name)  as state_labels,  array_agg(3017382) as state_geonameids, array_agg(country2019_name) as state_labels_en
            from ports.port_points p 
            where state_labels is  null and p.country2019_name = 'France'
            group by p.uhgs_id, p.toponyme, p.country2019_name
        ) as k
        where p.uhgs_id = k.uhgs_id"""
        self.execute_sql(query)

        #Iceland
        query = """ update ports.port_points p set state_labels=k.state_labels, state_geonameids=k.state_geonameids, state_labels_en=k.state_labels_en
        from (
            select p.uhgs_id, p.toponyme, p.country2019_name, array_agg(country2019_name)  as state_labels,  array_agg(2629691) as state_geonameids, array_agg(country2019_name) as state_labels_en
            from ports.port_points p 
            where state_labels is  null and p.country2019_name = 'Iceland'
            group by p.uhgs_id, p.toponyme, p.country2019_name
        ) as k
        where p.uhgs_id = k.uhgs_id and k.uhgs_id = 'A0146289'"""
        self.execute_sql(query)


        '''

    def exportJSONfeatures(self, config) : 
        """
        Export port_points en JSON
        Testée ok : 6 juin 2020
        """
        query = """SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         ogc_fid,
                'geometry',   ST_AsGeoJSON(geom)::jsonb,
                'properties', to_jsonb(row) - 'ogc_fid' 
            ) AS feature
            FROM (
            SELECT ogc_fid, uhgs_id, total, toponyme_standard_fr as  toponym, belonging_states, belonging_substates, status, geonameid, amiraute as admiralty, province, shiparea , state_1789_fr, substate_1789_fr, geom
            FROM ports.port_points p, 
                    (select pointcall_uhgs_id, count( *) as total
                    from navigocheck.check_pointcall gg group by pointcall_uhgs_id) as k
                    where p.uhgs_id = k.pointcall_uhgs_id
            ) row) features;"""
        rows = self.select_sql(query);

        #json_str = json.dumps(dataframe.to_json(orient='records'))
        #return json.loads(json_str)

        filename = config['ports']['geojson_output']
        print(filename)
        
        output = open(filename, "w")
        for rowk in rows:
            self.logger.info('Export json: \n'+str(rowk[0]))
            output.write( json.dumps(rowk[0]))
        output.close()


    def updateCountry(self, radius) : 
        query = """update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
            from (
            select q.*
            from 	(
                    select uhgs_id, min (d) from
                    (
                    select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
                    from ports.port_points p , ports.world_borders w
                    where st_intersects(w.mpolygone3857, st_buffer(p.point3857, %f)) and country2019_iso2code is null
                    order by uhgs_id
                    ) as k group by uhgs_id 
                ) as k,
                (
                select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
                from ports.port_points p , ports.world_borders w
                where st_intersects(w.mpolygone3857, st_buffer(p.point3857,  %f)) and country2019_iso2code is null
                order by uhgs_id
                ) as q
            where q.d = k.min and q.uhgs_id = k.uhgs_id
            ) as k
            where  country2019_name is null and ports.uhgs_id = k.uhgs_id """ % (radius,radius)
        self.execute_sql(query)

    def importShapefile(self, config) : 
        """
        import a shp  
        https://pypi.org/project/GDAL/#usage
        """
        import os.path  
        import psycopg2
  
        self.execute_sql("drop table if exists ports.world_borders cascade")

        query = """CREATE TABLE ports.world_borders (
            id serial NOT NULL,
            geom geometry(MULTIPOLYGON, 4326) NULL,
            fips varchar(2) NULL,
            iso2 varchar(2) NULL,
            iso3 varchar(3) NULL,
            un int4 NULL,
            "name" varchar(50) NULL,
            area int4 NULL,
            pop2005 int8 NULL,
            region int4 NULL,
            subregion int4 NULL,
            lon float8 NULL,
            lat float8 NULL,
            mpolygone3857 geometry NULL,
            CONSTRAINT world_borders_pkey PRIMARY KEY (id)
        ); """
        self.execute_sql(query)
        self.execute_sql("CREATE INDEX sidx_world_borders_geom ON ports.world_borders USING gist (geom);");
 
        
        filename = config['ports']['word_borders']
        print(filename)  

        '''
        import osgeo.ogr  

        shapefile = osgeo.ogr.Open(filename)    
        layer = shapefile.GetLayer(0)    
        for i in range(layer.GetFeatureCount()):  
            feature = layer.GetFeature(i)  
            name = feature.GetField("NAME").decode("Latin-1")
            iso2 = feature.GetField("iso2").decode("Latin-1")
            region = feature.GetField("region")   
            wkt = feature.GetGeometryRef().ExportToWkt()  
            self.execute_sql("INSERT INTO ports.world_borders (name,iso2,region,outline) " +"VALUES (%s, %s, %f, ST_GeometryFromText(%s, " +"4326))", 
                (name.encode("utf8"), iso2.encode("utf8"), region, wkt))  
        '''

        
    def createReadOnlyUsers(self, config, role,  password):
        """
        Must be run as postgres 

        tested 03 juin 2020
        """
        print('createReadOnlyUsers for :'+config['base']['dbname'])
        print('role :'+role)

        self.logger.debug('createReadOnlyUsers for :'+config['base']['dbname'])
        self.logger.debug('role :'+role)
        
        database = config['base']['dbname']
        
        try : 
            self.execute_sql("REVOKE ALL PRIVILEGES ON all tables IN schema  ports , public  FROM "+role+" cascade")
            self.execute_sql("REVOKE ALL PRIVILEGES ON all sequences IN schema ports , public  FROM "+role+" cascade")
            self.execute_sql("REVOKE ALL PRIVILEGES on schema ports, public  FROM "+role)
            self.execute_sql("REVOKE ALL PRIVILEGES ON database "+database+"  FROM "+role)
            self.execute_sql("DROP role if exists "+role)
        except {psycopg2.errors.InsufficientPrivilege, psycopg2.errors.DependentObjectsStillExist}: 
            print('REVOKE ALL FAILED :'+role)
            pass

        query = "CREATE ROLE "+role+" NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN"
        if password is not None : 
            self.logger.debug('password :'+password)
            query = query+" PASSWORD '%s'"% (password)
        self.execute_sql(query)
        

        self.execute_sql("GRANT USAGE ON SCHEMA public TO "+role)
        self.execute_sql("GRANT USAGE ON SCHEMA ports TO "+role)
        self.execute_sql("GRANT SELECT ON ALL TABLES IN SCHEMA  ports, public TO "+role)
        self.execute_sql("grant CONNECT on database "+database+" to "+role)
        self.execute_sql("grant SELECT on all sequences in schema ports, public to "+role)

    

    def debug(self, config, relation):
        """
        Exporte le contenu d'une table  dans un fichier excel
        Param relation : le nom de la table à exporter
        Param Config : contient le nom du fichier Excel à créer

        Testée le 11 mai 2020 / 05 fevrier 2021
        """
        print("Print table in a XLSX file")

        

        # Create a workbook and add a worksheet.
        print(config['outputs'][relation])
        workbook = xlsxwriter.Workbook(config['outputs'][relation])
        worksheet = workbook.add_worksheet(relation)

        ## Ecrire l'entête des colonnes
        columns = []
        cols = ", "
        liste_attributs = config['outputs'][relation+'_columns']
        print(liste_attributs)
        if liste_attributs == '':
            query = """SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE
            TABLE_SCHEMA = 'ports' and TABLE_NAME = '%s' order by ordinal_position""" % (relation)
            rows = self.select_sql(query)
            for r in rows :
                columns.append(r[0])
            print (cols.join(columns))
        else :
            columns = (config['outputs'][relation+'_columns']).split(',')

        excelcol = 0
        for c in columns :
            worksheet.write(0, excelcol, c)
            excelcol=excelcol+1
        
        toustopos_index = -1
        try:  
            toustopos_index = columns.index('toustopos'); 
        except ValueError: 
            pass
        print("Index de toustopos : "+str(toustopos_index))
        ## récupérer les données 
        query = """SELECT %s FROM ports.%s """ % (cols.join(columns), relation)
        rows = self.select_sql(query)
        excelrow = 1
        for r in rows :
            excelcol = 0
            #print(r)
            for c in r :
                #print(c) 
                data = str(c)
                if excelcol== toustopos_index :
                    data = data.replace("', '", ",")
                    data = data.replace("['", "")
                    data = data.replace("']", "")
                if c is not None : 
                    worksheet.write(excelrow, excelcol, data)
                excelcol=excelcol+1
            excelrow=excelrow+1

        workbook.close()

    def execute_sql(self, sql_query):
            cur = self.postgresconn.cursor()
            try:
                cur.execute(sql_query)
            except Exception as e:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print(e)
                print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
                self.logger.error(sql_query)

            cur.close()
            self.postgresconn.commit()

    def select_sql(self, sql_query):
        cur = self.postgresconn.cursor()
        try:
            cur.execute(sql_query)
            return cur.fetchall()
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print(e)
            print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            self.logger.error(sql_query)

        cur.close()
        self.postgresconn.commit()

if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_loadfilemaker.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))


    c = BuildPorts(config)
    #c.setExtensionsFunctions(config)

    #c.importShapefile(config) #bug avec import de osgeo
    
    #c.buildPortTable(config)
    #c.buildPortTableSuite(config)
    #c.updatePortStates(config)

    #c.useGeonames(config)
    ##c.computeMatching(config)

    #c.createReadOnlyUsers(config, 'porticapi', None)
    #c.createReadOnlyUsers(config, 'api_user',  'portic')

    c.exportJSONfeatures(config)

    c.debug(config, 'port_points')
    
    
    #Clôturer la connexion à la base de données
    c.close_connection()

    ## pour le lancer sur le serveur
    # nohup python3 BuildPorts.py > out.txt &
