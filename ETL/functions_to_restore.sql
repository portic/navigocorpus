-- le 08/12/2021 - Christine Plumejeaud - ANR PORTIC
-- Backup des functions développées en PL/PYTHON3 pour PORTIC en place sur la base portic (porticv6)
-- exécuter le fichier pour restaurer les fonctions.
-- attention, la fonction ports.frequency_topo risque de poser un souci, voir le commentaire qui précède la fonction dans le fihcier

-- functions dans le schéma ports


CREATE OR REPLACE FUNCTION ports.extract_state_en_fordate(tested_value character varying, dateparam integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2) from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_states_en::json) as elt  
					from ports.port_points p
					where belonging_states_en = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$;
        
CREATE OR REPLACE FUNCTION ports.extract_state_fordate(tested_value character varying, dateparam integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2) from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_states::json) as elt  
					from ports.port_points p
					where belonging_states = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$;
       
 CREATE OR REPLACE FUNCTION ports.extract_substate_en_fordate(tested_value character varying, dateparam integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates_en::json) as elt  
					from ports.port_points p
					where belonging_substates_en = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;

CREATE OR REPLACE FUNCTION ports.extract_substate_fordate(tested_value character varying, dateparam integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates::json) as elt  
					from ports.port_points p
					where belonging_substates = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;

-- Attention, cette function frequency_topo est "spéciale" à cause des quotes dans query = query+item.replace('\'', '\'\'')+"'""; 
-- il faut faire une petite manip pour s'en sortir : ecrire 
-- query = query+item.replace('\'', '\'\'')+"'"  donc dans le dernier guillemet

CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)
 RETURNS json
 LANGUAGE plpython3u
AS $function$
	import json
	from operator import itemgetter
	global temp
	global result
	global topos
	global query
	if toustopo is not None :
		result = []
		temp = toustopo.replace('{', '').replace('}', '').strip(' ')
		temp = temp.replace('"', '').strip(' ')
		topos = temp.split(',')
		for item in topos:
		    #plpy.notice(item)
		    
		    query = "select count(*) as freq from ports.geo_general where pointcall_uhgs_id = '"+uhgs_id+"' and (ports.rm_parentheses_crochets(pointcall_name)).value='"
		    query = query+item.replace('\'', '\'\'')+"'""
		    #plpy.notice(query)
		    rv = plpy.execute(query)
		    #plpy.notice(rv[0]["freq"])
		    result.append(dict(topo=item, freq=rv[0]["freq"]))
		result = sorted(result, key=itemgetter('freq'), reverse=True)

	else :
		result = None
	#plpy.notice (result)
	return json.dumps(result, ensure_ascii=False)
$function$
;


--- 

CREATE OR REPLACE FUNCTION ports.rm_parentheses_crochets(tested_value text)
 RETURNS qual_value
 LANGUAGE plpython3u
AS $function$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -3
                        result = None
                else :
                    result = None
                    code = -3
                return [result, code]
            $function$
;

CREATE OR REPLACE FUNCTION ports.test_double_type(tested_value character varying)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;

CREATE OR REPLACE FUNCTION ports.test_int_type(tested_value character varying)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::int'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;

CREATE OR REPLACE FUNCTION ports.update_ferme(p_amiraute character varying, direction character varying, bureau character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
        	nb_ports integer;
    begin
                -- EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
	            EXECUTE 'select count(*) from ports.port_points pp where amiraute = '||quote_literal(p_amiraute) into  nb_ports; 

                EXECUTE 'UPDATE ports.port_points 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where amiraute =  '||quote_literal(p_amiraute); --1
				EXECUTE 'UPDATE navigoviz.pointcall 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where pointcall_admiralty =  '||quote_literal(p_amiraute); --497
				EXECUTE 'UPDATE navigoviz.built_travels 
					set departure_ferme_direction='||quote_literal(direction)||',  departure_ferme_bureau='||quote_literal(bureau)||',
					departure_ferme_direction_uncertainty=0 , departure_ferme_bureau_uncertainty=0
					where departure_admiralty =  '||quote_literal(p_amiraute); --404
				EXECUTE 'UPDATE navigoviz.built_travels 
					set destination_ferme_direction='||quote_literal(direction)||',  destination_ferme_bureau='||quote_literal(bureau)||',
					destination_ferme_direction_uncertainty=0 , destination_ferme_bureau_uncertainty=0
					where destination_admiralty = '||quote_literal(p_amiraute); --415
                return nb_ports;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return -1;
            end;
$function$
;

CREATE OR REPLACE FUNCTION ports.update_ferme_unport(p_toponyme_standard_fr character varying, direction character varying, bureau character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
        	nb_ports integer;
    begin
                -- EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
	            EXECUTE 'select count(*) from ports.port_points pp where toponyme_standard_fr = '||quote_literal(p_toponyme_standard_fr) into  nb_ports; 

                EXECUTE 'UPDATE ports.port_points 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where toponyme_standard_fr =  '||quote_literal(p_toponyme_standard_fr); --1
				EXECUTE 'UPDATE navigoviz.pointcall 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where toponyme_fr =  '||quote_literal(p_toponyme_standard_fr); --497
				EXECUTE 'UPDATE navigoviz.built_travels 
					set departure_ferme_direction='||quote_literal(direction)||',  departure_ferme_bureau='||quote_literal(bureau)||',
					departure_ferme_direction_uncertainty=0 , departure_ferme_bureau_uncertainty=0
					where departure_fr =  '||quote_literal(p_toponyme_standard_fr); --404
				EXECUTE 'UPDATE navigoviz.built_travels 
					set destination_ferme_direction='||quote_literal(direction)||',  destination_ferme_bureau='||quote_literal(bureau)||',
					destination_ferme_direction_uncertainty=0 , destination_ferme_bureau_uncertainty=0
					where destination_fr = '||quote_literal(p_toponyme_standard_fr); --415
                return nb_ports;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return -1;
            end;
$function$
;

-- dans le schéma navigo

-- Oui, c'est la même que dans le schéma ports... désolée pour l'incohérence.

CREATE OR REPLACE FUNCTION navigo.extract_state_fordate(tested_value character varying, dateparam integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
        declare
        	state varchar;
        begin
	            
	            execute 'select states::text from (
				select  pointcall, pointcall_uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  pointcall, pointcall_uhgs_id, json_array_elements(pointcall_states::json) as elt  
					from navigoviz.pointcall p
					where pointcall_states = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;

-- Celle ci est utilisée par l'API : très importante
CREATE OR REPLACE FUNCTION navigo.pystrip(x text)
 RETURNS text
 LANGUAGE plpython3u
AS $function$
            global x
            x = x.strip()  # ok now
            return x
            $function$
;

-- pas la même que dans le schéma ports, et c'est celle-ci qui est la bonne version (-4 pour missing)

CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets(tested_value text)
 RETURNS qual_value
 LANGUAGE plpython3u
AS $function$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -4
                        result = None
                else :
                    result = None
                    code = -4
                return [result, code]
            $function$
;

-- meme que dans ports

CREATE OR REPLACE FUNCTION navigo.test_double_type(tested_value character varying)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;

-- meme que dans ports
CREATE OR REPLACE FUNCTION navigo.test_int_type(tested_value character varying)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::int'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $function$
;


