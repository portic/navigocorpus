# -*- coding: utf-8 -*-
'''
Created on 13 february 2019
@author: cplumejeaud
'''

# Comprendre les imports en Python : http://sametmax.com/les-imports-en-python/
# print sys.path
from __future__ import nested_scopes
from sqlite3 import OperationalError

import psycopg2
import logging
import configparser

from os import sys, path
import os, sys, traceback
import subprocess
import time
from stat import *

import random
from xlrd import open_workbook, cellname, XL_CELL_TEXT, XL_CELL_DATE, XL_CELL_BLANK, XL_CELL_EMPTY, XL_CELL_NUMBER, \
    xldate_as_tuple

#Note : xlrd in later version can't read xlsx anymore
#https://stackoverflow.com/questions/65254535/xlrd-biffh-xlrderror-excel-xlsx-file-not-supported
#use C:\Tools\Python\python37>Scripts\pip.exe install xlrd==1.2.0
#or :  pip install openpyxl 
# and read_excel("my.xlsx",engine='openpyxl') 

#pyopenxls à voir
#python -m pip install xlrd==1.2.0 avec python 3.9

## pour avoir le path d'un package
## print (psycopg2.__file__)
## C:\Users\cplume01\AppData\Local\Programs\Python\Python37-32\lib\site-packages\psycopg2\__init__.py

class LoadFilemaker(object):

    def __init__(self, config):
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('LoadFilemaker (v1.1)')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

        ## Open both a ssh connexion for copy/remove, and a tunnel for postgres connexion
        self.postgresconn = self.open_connection(config)


    def close_connection(self):
        '''
        Cleanly close DB connection
        :param postgresconn:
        :return:
        '''
        if self.postgresconn is not None:
            self.postgresconn.close()
            self.logger.info('close_connection CLOSED ? '+str(self.postgresconn.closed))

    def check_and_open_connection(self, config):
        '''
        If the connexion is down, it re-open it silenciously
        https://stackoverflow.com/questions/1281875/making-sure-that-psycopg2-database-connection-alive
        :param config: the config file
        :return:
        '''
        try:
            self.postgresconn.isolation_level
        except OperationalError as oe:
            self.logger.info('check_and_open_connection : GOT TO REOPEN')
            self.open_connection(config)
            

    def open_connection(self, config):
        '''
        Open database connection with Postgres
        :param config:
        :return:
        '''

        ''''''


        # Acceder aux parametres de configuration
        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')
        # schema = config.get('base', 'schema')
        driverPostgres = 'host=' + host + ' port=' + port + ' user=' + user + ' dbname=' + dbname + ' password=' + password
        self.logger.debug(driverPostgres)

        conn = None
        try:
            conn = psycopg2.connect(driverPostgres)
        except Exception as e:
            self.logger.error("I am unable to connect to the database. " + str(e))
        # Test DB
        if conn is not None:
            cur = conn.cursor()
            cur.execute('select count(*) from pg_namespace')
            result = cur.fetchone()
            if result is None:
                print('open_connection Failed to get count / use of database failed')
                self.logger.error('open_connection Failed to get count / use of database failed')
            else:
                print('open_connection Got database connexion : ' + str(result[0]))
                self.logger.info('open_connection Got database connexion : ' + str(result[0]))
        else:
            print('open_connection Failed to get database connexion')
            self.logger.error('open_connection Failed to get database connexion')

        self.postgresconn = conn
        return conn

    def do_the_job(self, config):


        self.logger.info('\n---------------------------- Processing Filemaker files ---------------------------- \n')
        try:
            
            self.setExtensionsFunctions(config)
                        
            #self.loadfile(config, 'geo_general')
            #self.create_uncertain_check_tables(config, 'geo_general')            
            #self.populate_uncertain_check_tables(config, 'geo_general')
            
            """
            self.loadfile(config, 'sources')

            
            self.loadfile(config, 'pointcall')
            self.create_uncertain_check_tables(config, 'pointcall')
            self.populate_uncertain_check_tables(config, 'pointcall')
            
            
            self.loadfile(config, 'cargo')
            self.create_uncertain_check_tables(config, 'cargo')
            self.populate_uncertain_check_tables(config, 'cargo')
            
            self.loadfile(config, 'taxes')
            self.create_uncertain_check_tables(config, 'taxes')
            self.populate_uncertain_check_tables(config, 'taxes')
            """   
            self.loadfile(config, 'stages')
            self.create_uncertain_check_tables(config, 'stages')
            self.populate_uncertain_check_tables(config, 'stages')
            
            
            
            #Attention, truncate all tables of this schema before. 


        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print(e)
            print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            self.logger.error(e)
            self.logger.error(e.message)
        self.logger.info('\n---------------------------- END ---------------------------- \n')
        self.close_connection()

    def setExtensionsFunctions(self, config):
        """
        Installe les extensions nécessaires à l'import et l'utilisation des données de navigocorpus dans portic, 
        crée les 3 schémas navigo, navigocheck et navigoviz
        et crée les 3 fonctions spécifiques à navigo pour importer les données
        Ces fonctions programmées en PLPython 3 permettent de retirer les crochets et parenthèses des données de navigo 
        pour les conserver épurées dans navigocheck, avec également un code associé à la présence de ces signes
        - 0 : pas de signe
        - -1 : parenthèses autour (
        - -2 : crochets autour []
        - -4 : donnée manquante
        testé le 14 mai 2020 et le 2 février 2021
        """
        #Prerequis : en tant que postgres, 
        #CREATE ROLE dba WITH SUPERUSER NOINHERIT;
        #GRANT dba TO navigo;

        self.execute_sql('SET ROLE dba')
        self.execute_sql("create extension if not exists postgis")
        self.execute_sql("create extension if not exists fuzzystrmatch")
        self.execute_sql("create extension if not exists pg_trgm")
        self.execute_sql("create extension if not exists postgis_topology")
        self.execute_sql("create extension if not exists plpython3u")
        self.execute_sql("create extension if not exists dblink")

        self.execute_sql("create schema if not exists navigo")
        self.execute_sql("create schema if not exists navigocheck")
        self.execute_sql("create schema if not exists navigoviz")

        self.execute_sql("create schema if not exists ports")

        #self.execute_sql('create role mydba with superuser noinherit') 
        self.execute_sql('GRANT mydba TO navigo')

        self.execute_sql("DROP FUNCTION if exists navigo.test_double_type(tested_value VARCHAR)") 

        query = """CREATE OR REPLACE FUNCTION navigo.test_double_type (tested_value VARCHAR) RETURNS boolean AS 
        $BODY$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists  navigo.test_int_type(tested_value VARCHAR)") 

        query = """CREATE OR REPLACE FUNCTION navigo.test_int_type (tested_value VARCHAR) RETURNS boolean AS 
        $BODY$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::int'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.execute_sql(query)

        self.execute_sql("drop type if exists qual_value cascade")
        query = """CREATE TYPE qual_value AS (
            value   text,
            code  integer
            )"""
        self.execute_sql(query)

        self.execute_sql("DROP FUNCTION if exists navigo.rm_parentheses_crochets(tested_value text)") 

        #Commented 02 fev 2021 : or tested_value.strip().find(']') > 0
        query = """CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -4
                        result = None
                else :
                    result = None
                    code = -4
                return [result, code]
            $$ LANGUAGE plpython3u;"""
        self.execute_sql(query)

        query = """CREATE or REPLACE FUNCTION navigo.pystrip(x text)
            RETURNS text
            AS $$
            global x
            x = x.strip()  # ok now
            return x
            $$ LANGUAGE plpython3u;"""
        self.execute_sql(query)
        
        self.execute_sql('RESET ROLE')

    def setExtensionsPortsFunctions(self, config):
        """
        a executer en tant que postgres
        """
        #Prerequis : en tant que postgres, 
        #CREATE ROLE dba WITH SUPERUSER NOINHERIT;
        #GRANT dba TO navigo;

        self.execute_sql('SET ROLE dba')

        self.execute_sql('create role porticapi')
        self.execute_sql('create role api_user')

        self.execute_sql('ALTER USER porticapi WITH PASSWORD ''portic')
        self.execute_sql('ALTER USER api_user WITH PASSWORD ''portic')

        

        ## Quand toutes les données seront loadées
        self.execute_sql('GRANT USAGE ON SCHEMA navigoviz TO porticapi, api_user')
        self.execute_sql('GRANT USAGE ON SCHEMA navigocheck to porticapi, api_user')
        self.execute_sql('GRANT USAGE ON SCHEMA navigo to porticapi, api_user')
        self.execute_sql('GRANT USAGE ON SCHEMA public to porticapi, api_user')
        self.execute_sql('GRANT USAGE ON SCHEMA ports to porticapi, api_user')
        self.execute_sql('GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO porticapi, api_user')
        self.execute_sql('grant CONNECT on database portic_v8 to porticapi, api_user')
        self.execute_sql('grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi, api_user')
        self.execute_sql('RESET ROLE')

    def create_uncertain_check_tables(self, config, relation):
        print('create_uncertain_check_tables ', relation)
        self.execute_sql('SET ROLE dba')
        sql_createtable = """SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='navigo' and TABLE_NAME = '%s' order by ordinal_position""" % (
            relation)
        self.logger.info(sql_createtable)
        columns = self.select_sql(sql_createtable)

        sql_createtable = """ CREATE TABLE navigocheck.uncertainity_""" + relation + """ ( """
        for c in columns:
            sql_createtable += c[0] + ' int,'
        sql_createtable = sql_createtable[:-1]
        sql_createtable += ");"
        self.logger.info(sql_createtable)
        sql_droptable = "drop table IF EXISTS navigocheck.uncertainity_" + relation + " cascade;"
        self.execute_sql(sql_droptable)
        self.execute_sql(sql_createtable)

        sql_createtable = """ CREATE TABLE navigocheck.check_""" + relation + """ ( """
        for c in columns:
            if (c[0] == 'pkid') :
                sql_createtable += c[0] + ' int,'
            else :
                sql_createtable += c[0] + ' text,'
        sql_createtable = sql_createtable[:-1]
        sql_createtable += ");"
        self.logger.info(sql_createtable)
        sql_droptable = "drop table IF EXISTS navigocheck.check_" + relation + " cascade;"
        self.execute_sql(sql_droptable);
        print(sql_droptable)
        self.execute_sql(sql_createtable)
        self.execute_sql('RESET ROLE')

    def populate_uncertain_check_tables(self, config, relation):
        self.execute_sql('SET ROLE dba')
        print('populate_uncertain_check_tables ', relation)

        query = """SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='navigo' and TABLE_NAME = '%s' order by ordinal_position""" % (relation)
        self.logger.info(query)
        columns = self.select_sql(query);

        sql_insert_head = "insert into navigocheck.uncertainity_" + relation + " ( select "
        for c in columns:
            if (c[0] == 'pkid'):
                sql_insert_head += c[0] + ','
            else:
                sql_insert_head += ' (navigo.rm_parentheses_crochets('+c[0]+')).code ,'
        sql_insert_head = sql_insert_head[:-1]
        sql_insert_head += " from navigo." + relation+");"
        self.logger.info("\n"+sql_insert_head+"\n")
        self.execute_sql(sql_insert_head)


        sql_insert_head = "insert into navigocheck.check_" + relation + " ( select "
        for c in columns:
            if (c[0] == 'pkid'):
                sql_insert_head += c[0] + ','
            else:
                sql_insert_head += ' (navigo.rm_parentheses_crochets('+c[0]+')).value ,'
        sql_insert_head = sql_insert_head[:-1]
        sql_insert_head += " from navigo." + relation+");"
        self.logger.info("\n"+sql_insert_head+"\n")
        self.execute_sql(sql_insert_head)
        self.execute_sql('RESET ROLE')

    def loadfile(self, config, relation):
        self.execute_sql('SET ROLE dba')
        print('Processing ' + relation)
        print('Data are in: ' + config.get(relation, 'file_name'))
        wb = open_workbook(config.get(relation, 'file_name'))
        sheet = wb.sheet_by_name(config.get(relation, 'sheet_name'))

        print("Lignes : ", sheet.nrows, "Colonnes : ", sheet.ncols)

        ## Traitement de la première ligne : creation de la relation
        row = 0
        sql_createtable = "create table navigo." + relation + " ( \n\t pkid serial primary key,"
        sql_insert_head = "insert into navigo." + relation + " ( "
        for col in range(0, sheet.ncols):
            if sheet.cell(row, col).ctype is not XL_CELL_BLANK and sheet.cell(row, col).ctype is not XL_CELL_EMPTY:
                self.logger.info(sheet.cell(row, col).value)
                column_name = sheet.cell(row, col).value
                if column_name.find("::") > -1:
                    column_name = column_name[:column_name.index("::")] + '__' + column_name[
                                                                                 column_name.index("::") + 2:]
                sql_createtable += "\n\t" + column_name + "     text,"
                sql_insert_head += column_name + ","
            else:
                print("l'entête du fichier excel ne peut contenir des colonnes vides")
                exit(0)
        sql_createtable = sql_createtable[:-1]
        sql_createtable += ");"
        self.logger.info(sql_createtable)

        sql_insert_head = sql_insert_head[:-1]
        sql_insert_head += ") values "
        self.logger.info(sql_insert_head)

        sql_droptable = "drop table IF EXISTS navigo." + relation + " cascade;"
        self.execute_sql(sql_droptable);
        self.execute_sql(sql_createtable);

        sql_insert = sql_insert_head
        for row in range(1, sheet.nrows):  # sheet.nrows
            # print (row)
            sql_insert += "\n\t("
            for col in range(0, sheet.ncols):
                if sheet.cell(row, col).ctype is not XL_CELL_BLANK and sheet.cell(row, col).ctype is not XL_CELL_EMPTY:
                    if sheet.cell(row, col).ctype is XL_CELL_TEXT:
                        sql_insert += "'" + sheet.cell(row, col).value.replace('\'', '\'\' ') + "',"
                    else:
                        sql_insert += "'" + str(sheet.cell(row, col).value) + "',"
                else:
                    sql_insert += "null,"
            sql_insert = sql_insert[:-1]
            sql_insert += "),"

            if row % 1000 == 0:
                self.logger.info(row)
                sql_insert = sql_insert[:-1]
                # self.logger.info(sql_insert.encode('UTF-8'))
                self.execute_sql(sql_insert.encode('UTF-8'))
                sql_insert = sql_insert_head
        sql_insert = sql_insert[:-1]
        # self.logger.info(sql_insert.encode('UTF-8'))
        self.execute_sql(sql_insert.encode('UTF-8'))
        self.execute_sql('RESET ROLE')

    def execute_sql(self, sql_query):
        cur = self.postgresconn.cursor()
        try:
            cur.execute(sql_query)
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print(e)
            print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            self.logger.error(sql_query)
            #if e isinstance psycopg2.errors.OutOfMemory
            return -1

        cur.close()
        self.postgresconn.commit()
        return 0

    def select_sql(self, sql_query):
        cur = self.postgresconn.cursor()
        try:
            cur.execute(sql_query)
            return cur.fetchall()
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print(e)
            print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            self.logger.error(sql_query)

        cur.close()
        self.postgresconn.commit()


if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    # configfile = sys.argv[1]
    configfile = 'config_loadfilemaker.txt'
    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))

    p = LoadFilemaker(config)
    p.do_the_job(config)

    ## pour le lancer sur le serveur
    # nohup python3 LoadFilemaker.py > out.txt &
