create extension postgis ;
create extension if not exists postgis ;
create extension if not exists fuzzystrmatch;
create extension if not exists pg_trgm;
create extension if not exists postgis_topology;
create extension if not exists plpython3u;
 
select * from pg_language;

SELECT lanpltrusted FROM pg_language WHERE lanname LIKE 'plpython3u';


create schema navigo;
create schema navigocheck;
create schema navigoviz;
create schema ports;
 
create role mydba with superuser noinherit; 
create role porticapi;
create role api_user;
ALTER USER porticapi WITH PASSWORD 'portic';
ALTER USER api_user WITH PASSWORD 'portic';

CREATE ROLE navigo WITH CREATEDB LOGIN PASSWORD 'navigocorpus2018';
GRANT mydba TO navigo;
GRANT ALL ON SCHEMA navigo TO navigo;
GRANT ALL ON SCHEMA navigocheck TO navigo;
GRANT ALL ON SCHEMA navigoviz TO navigo;
GRANT ALL ON SCHEMA ports TO navigo;
GRANT ALL ON SCHEMA public TO navigo;
GRANT ALL ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO navigo;
grant ALL on all sequences in schema navigoviz, navigo, navigocheck, ports, public to navigo;




-- à la toute fin

GRANT USAGE ON SCHEMA navigoviz TO porticapi;
GRANT USAGE ON SCHEMA navigocheck to porticapi;
GRANT USAGE ON SCHEMA navigo to porticapi;
GRANT USAGE ON SCHEMA public to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO porticapi;
grant CONNECT on database portic_v7 to porticapi;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi;


GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigocheck to api_user;
GRANT USAGE ON SCHEMA navigo to api_user;
GRANT USAGE ON SCHEMA public to api_user;
GRANT USAGE ON SCHEMA ports to api_user;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO api_user;
grant CONNECT on database portic_v7 to api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to api_user;

-- tester que python3u marche bien

CREATE FUNCTION navigo.pystrip(x text)
  RETURNS text
AS $$
  global x
  x = x.strip()  # ok now
  return x
$$ LANGUAGE plpython3u;

select navigo.pystrip('  dfsfsf sdsfs toto ')

-----------------------------------------------------------------------------------------------
-- la table port.port_points
-----------------------------------------------------------------------------------------------
       
-- 1. run BuildPorts
-- redo CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)

CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)
 RETURNS json
 LANGUAGE plpython3u
AS $function$
	import json
	from operator import itemgetter
	global temp
	global result
	global topos
	global query
	if toustopo is not None :
		result = []
		temp = toustopo.replace('{', '').replace('}', '').strip(' ')
		temp = temp.replace('"', '').strip(' ')
		topos = temp.split(',')
		for item in topos:
		    #plpy.notice(item)
		    
		    query = "select count(*) as freq from ports.geo_general where pointcall_uhgs_id = '"+uhgs_id+"' and (ports.rm_parentheses_crochets(pointcall_name)).value='"+item.replace('\'', '\'\'')+"'""
		    
		    #plpy.notice(query)
		    rv = plpy.execute(query)
		    #plpy.notice(rv[0]["freq"])
		    result.append(dict(topo=item, freq=rv[0]["freq"]))
		result = sorted(result, key=itemgetter('freq'), reverse=True)

	else :
		result = None
	#plpy.notice (result)
	return json.dumps(result, ensure_ascii=False)
$function$
;

-- 2.a run BuildPorts.setExtensionsFunctions
-- 2.b run BuildPorts.setExtensionsFunctions

-- 3. Importer le vieux fichier de ports.port_points et le renommer en backup
-- dans C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\obliques\ports_points_update05Janvier2022.sql
-- dans C:\Travail\Data\BDD-backups\portic\portic_port_points_V7_nov2022.sql
sudo -u postgres psql -U postgres -p 5432 -d portic_v8 -f /home/plumegeo/portic/portic_V8/portic_port_points_V7_nov2022.sql
-- 1434 entrées
alter table ports.port_points rename to port_points_old

alter table ports.port_points rename to port_points_v6

-- 4. Importer le fichier CSV de Geogeneral
-- Il est dans navigo.geo_general

-- pour la v6 et v8
CREATE TABLE navigo.geo_general_complet (
	country text NULL,
	class_name text NULL,
	pointcall_name text NULL,
	latitude text NULL,
	longitude text NULL,
	pointcall_uhgs_id text NULL,
	mgrs_id text NULL
);


-- pour la V7 inversion des 2 champs
CREATE TABLE navigo.geo_general_complet (
	country text NULL,
	pointcall_name text NULL,
	class_name text NULL,
	latitude text NULL,
	longitude text NULL,
	mgrs_id text null,
	pointcall_uhgs_id text NULL
);

select * from navigo.geo_general_complet;
truncate navigo.geo_general_complet;
-- tester l'import avec bulk load. 
select * from navigo.geo_general_complet_10jan22 ggcj 

sudo -U postgres psql -p 5432 -d portic_v8 -U postgres

\COPY navigo.geo_general_complet FROM /home/plumegeo/portic/portic_V8/geogeneral_28sept2022.txt
-- 'C:\Travail\Data\08-Navigo_28fevrier2022\geo_general_complet.txt' 
-- COPY 13911565 en v7
COPY  13912271 en v8


-- Permissions

ALTER TABLE navigo.geo_general_complet_10jan22 OWNER TO postgres;
GRANT ALL ON TABLE navigo.geo_general_complet_10jan22 TO postgres;
GRANT SELECT ON TABLE navigo.geo_general_complet_10jan22 TO porticapi;
GRANT SELECT ON TABLE navigo.geo_general_complet_10jan22 TO api_user;

-- a noter : mail  de Silvia du 7 février
-- "Ceci étant dit la base compte aujourd'hui 6349791 records (elle est commune à bcp de projets), donc je ne sais pas ce que Foliot t'a filé le 10 janvier....!
-- ma version de géogénéral du 28 fev est ok, pas cell du 1e0 janvier. 

-- mais il manque shipping_area
-- Importer le fichier pointcall_UHGS_coding_10jan2022_task(fr_87-89) exporté de la vue pointcall_UHGS_coding
-- Deux colonnes : UHGS_id, shiparea

create table if not exists ports.port_points as (
            select pointcall_uhgs_id as uhgs_id, latitude, longitude, null as amiraute, null as province, 
            array_agg(distinct (ports.rm_parentheses_crochets(pointcall_name)).value) as toustopos, (array_agg(distinct shippingarea))[1] as shiparea
            from ports.geo_general
            group by pointcall_uhgs_id, latitude, longitude)

create table if not exists ports.port_points as (
            select pointcall_uhgs_id as uhgs_id, latitude, longitude, null as amiraute, null as province, 
            array_agg(distinct (navigo.rm_parentheses_crochets(pointcall_name)).value) as toustopos
            from navigo.geo_general
            group by pointcall_uhgs_id, latitude, longitude)
            
alter table ports.port_points  add column shiparea text;

update ports.port_points pp set shiparea = ps.shiparea 
from ports."portUHGS_shippingarea" ps
where ps.uhgs_id = pp.uhgs_id 
-- 1141

select count(distinct uhgs_id ) from ports.port_points
-- 4092708

select count(distinct uhgs_id ) from ports."portUHGS_shippingarea"
-- 1151 (10 de plus de dans ports.port_points)

select count(*) from navigo.geo_general
-- 6 346 507

select count(*) from ports."portUHGS_shippingarea"
-- 108 000

create table ports.port_shiparea as 
(select distinct uhgs_id , shiparea from ports."portUHGS_shippingarea" )
-- 1152

select distinct sp.uhgs_id , sp.shiparea , pp.latitude , pp.longitude , pp.toustopos 
from ports.port_shiparea sp, ports.port_points pp
where sp.uhgs_id not in (select uhgs_id from ports.port_points_v6)
and sp.uhgs_id = pp.uhgs_id 

insert into ports.port_points_v6 (uhgs_id, latitude, longitude, toustopos)
select distinct sp.uhgs_id ,  pp.latitude , pp.longitude , pp.toustopos 
from ports.port_shiparea sp, ports.port_points pp
where sp.uhgs_id not in (select uhgs_id from ports.port_points_v6)
and sp.uhgs_id = pp.uhgs_id
--9 : ok : j'ai complété ports.port_points_V6 avec les points manquants

alter table navigo.geo_general rename  to geo_general_complet;

select distinct pointcall_uhgs_id, pointcall_name  , gg.shippingareamainptcall__e1_name , geo_general__lat, geo_general__long 
from navigo.geo_general gg 
where pointcall_uhgs_id not in (select uhgs_id from ports.port_points)
/*
 * 12 points à rajouter - le 28 janvier
 */

--A0079430	Arez en Galice	ACE-FINI	43.433333	-8.233333
--A1494718	Lapsaque	BSE-STRA	40.344167	26.685556
--A0081452	Villagancia en Galice	ACE-FINI	42.595493	 -8.765788
--A1415379	Hoggia Dei dans la Mer Noire et dans la partie ottomane	BSE-RUSS	46.483738	30.730705
--A0102450	Redondella en Galice	ACE-FINI	42.282225	 -8.609202
--A1825289	Tagano dans la mer d' Azof	BSE-RUSS	47.2273035475915	38.8908034936437
--A0333146	Ile de Zea	MED-AEGS	37.933333	23.65
--A0037077	Canal de Malte	MED-SIST	36.333333	15
--A0243974	Vasto dans le royaume de Naples		42.116667	14.7
--A1409167	Oczakou dans la Mer Noire	BSE-RUSS	46.616667	31.55
--A0339127	Golfe de Monte Santo dans l' archipel	MED-AEGN	40.2	23.933333
--A0348656	Schio [Chios]		38.367778	26.135833


insert into ports.port_points (uhgs_id, toponyme, shiparea, latitude, longitude, new_janvier2022)
select distinct pointcall_uhgs_id, pointcall_name  , gg.shippingareamainptcall__e1_name , 
geo_general__lat::float, geo_general__long::float, true
from navigo.geo_general gg 
where pointcall_uhgs_id not in (select uhgs_id from ports.port_points)

update ports.port_points p set toustopos = k.calcul 
from (
select uhgs_id , array_agg(toponyme) as calcul from ports.port_points p 
where new_janvier2022 is true and p.state_1789_fr is null and toustopos is null group by uhgs_id
) as k
where p.uhgs_id = k.uhgs_id

create table ports.port_points_backup02mars2022 as (select * from  ports.port_points);
-- les homeports qui sont codés mais pas dans ma table port
insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, new_janvier2022)
select cp.ship_homeport_uhgs_id, array_agg( distinct cp.ship_homeport) , gg.latitude , gg.longitude, true
from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
where cp.ship_homeport_uhgs_id not in (select uhgs_id from ports.port_points pp2) 
and gg.pointcall_uhgs_id = cp.ship_homeport_uhgs_id
group by cp.ship_homeport_uhgs_id, gg.latitude , gg.longitude
--223 nouveaux (le 31 janvier)
--229 nouveaux (le 02 mars)

select count(*) from ports.port_points  -- 1434

-- https://askcodez.com/comment-puis-je-supprimer-tous-les-caracteres-non-ascii-avec-la-regex-et-notepad.html

\COPY navigo.geo_general_complet FROM 'C:\Travail\Data\08-Navigo_28fevrier2022\geo_general_complet.txt' 
COPY 13911565

truncate navigo.geo_general_complet
select count(*) from navigo.geo_general_complet
select count(*) from navigoviz.test -- 86935
select count(*) from public.verifs  --73223 
select count(*) from navigoviz.pointcall  -- 109098

select uhgs_id from ports.port_points pp2 where uhgs_id='A0044949'
select * from navigocheck.check_pointcall where ship_homeport_uhgs_id='A0044949'
select count(*) from ports.port_points
select distinct cp.ship_homeport_uhgs_id
from navigocheck.check_pointcall cp 
where cp.ship_homeport_uhgs_id not in (select uhgs_id from ports.port_points pp2) 
order by ship_homeport_uhgs_id
-- 260
--A0824104	{Denmark}
select * from ports.port_points where new_janvier2022 is true
select * from ports.port_points where uhgs_id = 'A0824104'

----------------------
select distinct sp.uhgs_id ,  pp.latitude , pp.longitude , pp.toustopos 
from ports.port_shiparea sp, ports.port_points pp
where sp.uhgs_id not in (select uhgs_id from ports.port_points_v6)
and sp.uhgs_id = pp.uhgs_id

-- Puis rajouter les points du smogglage anglais
-- 7. Importer les ports du smogglage anglais
-- Fichier homeports_smogglage.csv du répertoire C:\Travail\Data\Navigo_10janvier2022
select  hsc.toponyme , ship_homeport_uhgs_id,  hsc.homeport_lat , hsc.homeport_long , hsc.province_gb, hsc.substate_1789_fr , hsc.state_1789_fr 
from   ports.homeports_smogglage_csv hsc 
where  ship_homeport_uhgs_id not in (select uhgs_id from port_points pp)
order by substate_1789_fr , province_gb 

insert into ports.port_points (toponyme, toponyme_standard_fr, toponyme_standard_en, uhgs_id, latitude, longitude, substate_1789_fr, substate_1789_en, state_1789_fr)
select  hsc.toponyme, hsc.toponyme, hsc.toponyme , ship_homeport_uhgs_id,  hsc.homeport_lat , hsc.homeport_long , hsc.substate_1789_fr , hsc.substate_1789_fr, hsc.state_1789_fr 
from   ports.homeports_smogglage_csv hsc 
where  ship_homeport_uhgs_id not in (select uhgs_id from port_points pp)
-- 27 lignes

-- A0386332 50.8 et 0.25 (mail du 5/11/2021)
select * from ports.homeports_smogglage_csv where ship_homeport_uhgs_id='A0386332' -- Eastbourn

select pp.uhgs_id, pp.toponyme_standard_fr, s.toponyme  , pp.substate_1789_fr, s.substate_1789_fr , pp.state_1789_fr , s.state_1789_fr 
from ports.homeports_smogglage_csv s, port_points pp 
where pp.uhgs_id = s.ship_homeport_uhgs_id
-- A0605259	Cork	Cork	Irlande
-- A0390202	Greenock	Greenock	Ecosse	
-- A0389696	Maldon	Maldon	Angleterre
-- A1963330	Jersey	Jersey	Iles anglo-normandes

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, p.toponyme_standard_fr , p.toponyme , p.uhgs_id , etat, subunit, dfrom, dto, e.geonameid, name_en, admin1_code, e.latitude, e.longitude, tgnid, wikipedia, etat_en, subunit_en
from etats e, ports.port_points p where e.uhgs_id = 'A0605259' and p.state_1789_en  is null and p.substate_1789_fr='Irlande';
-- 1

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, p.toponyme_standard_fr , p.toponyme , p.uhgs_id , etat, subunit, dfrom, dto, e.geonameid, name_en, admin1_code, e.latitude, e.longitude, tgnid, wikipedia, etat_en, subunit_en
from etats e, ports.port_points p where e.uhgs_id = 'A0390202' and p.state_1789_en  is null and p.substate_1789_fr='Ecosse';
-- 3

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, p.toponyme_standard_fr , p.toponyme , p.uhgs_id , etat, subunit, dfrom, dto, e.geonameid, name_en, admin1_code, e.latitude, e.longitude, tgnid, wikipedia, etat_en, subunit_en
from etats e, ports.port_points p where e.uhgs_id = 'A0389696' and p.state_1789_en  is null and p.substate_1789_fr='Angleterre';
-- 22


update ports.port_points pp set province = s.province_gb
from ports.homeports_smogglage_csv s
where pp.uhgs_id = s.ship_homeport_uhgs_id;

select uhgs_id , toponyme, province, latitude, longitude, substate_1789_fr from ports.port_points where geom is null and latitude is not null

--------------------------------------------------------------------------------
-- On part sur la table port_points, à completer avec nouvelles données (9 lignes)
-- 229 lignes le 2 mars
-- Objectif : compléter cette table à la main avant d'intégrer les données de navigo
--------------------------------------------------------------------------------

drop table ports.port_points;
alter table ports.port_points_v6 rename to port_points;


update ports.port_points set topofreq = ports.frequency_topo(uhgs_id, array_to_string(toustopos, ',')) 
where array_length(toustopos, 1) > 0 and ports.test_double_type(longitude) is true and topofreq is null
--9 / 222 / 228
update ports.port_points set toponyme = (topofreq::json->>0)::json->>'topo' where toponyme is null
-- 271 / 277

select * from ports.port_points where new_janvier2022 is true and geom is null --267 / 223 / 229
-- B0000960	34.816978	-76.199347
update ports.port_points set latitude='34.816978', longitude='-76.199347' where uhgs_id = 'B0000960'
update ports.port_points set longitude = trim(longitude), latitude = trim(latitude) where new_janvier2022 is true
update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where new_janvier2022 is true

select count(*) from ports.port_points pp where new_janvier2022 is true and pp.point3857 is null


select * from ports.port_points pp where new_janvier2022 is true and pp.point3857 is null
/*
A0152878	Saint Cyr : La Ciotat, Provence
A0233335	Rossano : Naples
A0242201	Maiori : Naples
A0234673	Marina di Monasterace : Naples Monasterace
A0234281	Campofelice : Sicile
A0235476	Coldirodi : république de Gènes / Coldirodi dans San Rémo
A0336866	Katarina : Empire Ottoman
A1907798	Es Sur : Moyen-Orient, reste Asie
D3674030	Bien Dong : china
*/

update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857) where new_janvier2022 is true

-- 5. Import de ports.world_borders
-- Fichier portic_v6_GIS_18janvier2022.sql dans C:\Travail\Data\Navigo_10janvier2022
-- psql -U postgres -D portic_v7 -p 5435 -f portic_v6_GIS_18janvier2022.sql
-- generiques_inclusions_geo_csv 
-- gshhs_f_l1_3857
-- lau_europe
-- limites_amirautes
-- rivers_f
-- world_1789
-- world_borders
-- wup2018_f13_capital_cities_csv

update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 50000)) and p.country2019_name is null and new_janvier2022 is true and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and new_janvier2022 is true
-- 223 (1000 m)+4 (7000 m) + 8 à 20 km + 2 à 50 km
        
select uhgs_id, count(distinct iso2) as c from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 50000)) and p.country2019_name is null and new_janvier2022 is true and point3857 is not null
   ) as k 
   group by uhgs_id
  having count(distinct iso2)>1
-- vérif ok. 
  
select uhgs_id, toponyme, shiparea from ports.port_points p where   country2019_name is null and new_janvier2022 is true  
select toponyme , * from ports.port_points where country2019_name is null and point3857 is not null
select count(*) from ports.port_points where country2019_name is null and point3857 is not null
-- 31 puis 19 puis 60 puis 11

-- récuperer la liste des ports à traiter pour toponyme_standard_fr, et etats d'appartenance

select uhgs_id , toponyme, country2019_name, state_1789_fr, substate_1789_fr from ports.port_points 
where new_janvier2022 is true and toponyme_standard_fr is null and geom is not null
-- and country2019_name='France'
order by country2019_name, uhgs_id
-- done le 01 févrirer 2022 pour Silvia

-- Récupération dans le fichier file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\Liste%20ports%20nouveaux%2030-01-2022%20Silvia%20en%20cours%2012-02-2022.csv
-- Mise à jour par Christine avec colone etat_like qui permet de faire la mise à jour pour les états. 
-- Excepté pour la France 
-- uhgs_id	toponyme	country2019_name	state_1789_fr	substate_1789_fr	toponyme_standardized_fr	toponyme_standardized_en	etat_like

truncate ports.liste_ports_nouveaux 

update ports.port_points p set toponyme_standard_fr = n.toponyme_standardized_fr , toponyme_standard_en = n.toponyme_standardized_en 
from ports.liste_ports_nouveaux n
where p.uhgs_id = n.uhgs_id  
-- 225

insert into labels_lang_csv (label_type, key_id, fr, en) 
select 'toponyme', uhgs_id, toponyme_standardized_fr, toponyme_standardized_en
from ports.liste_ports_nouveaux
-- 225

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, n.toponyme_standardized_fr , n.toponyme , n.uhgs_id , e.etat, e.subunit, e.dfrom, e.dto, e.geonameid, e.name_en, e.admin1_code, e.latitude, e.longitude, e.tgnid, e.wikipedia, e.etat_en, e.subunit_en
from etats e, liste_ports_nouveaux n 
where n.etat_like = e.uhgs_id and n.etat_like !='France';
-- 300


-- 6. Réutilisation de la nomenclature des Etats et des appariemments de geonames
-- Fichier portic_v6_nomenclatures_18janvier2022.sql dans C:\Travail\Data\Navigo_10janvier2022
-- psql -U postgres -D portic_v7 -p 5435 -f portic_v6_nomenclatures_18janvier2022.sql
-- labels_alng_csv
-- etats
-- matching_port

-- A0079430	Arez en Galice	ACE-FINI	43.433333	-8.233333 : https://fr.wikipedia.org/wiki/Ares_(La_Corogne), Espagne
-- A0081452	Villagancia en Galice	ACE-FINI	42.595493	 -8.765788 : Vilagarcía de Arousa https://fr.wikipedia.org/wiki/Vilagarc%C3%ADa_de_Arousa Espagne
-- A0102450	Redondella en Galice	ACE-FINI	42.282225	 -8.609202 : https://fr.wikipedia.org/wiki/Redondela Espagne
-- A0243974	Vasto dans le royaume de Naples		42.116667	14.7 : https://fr.wikipedia.org/wiki/Vasto naples
-- A0348656	Schio [Chios]		38.367778	26.135833 : Empire Ottoman https://fr.wikipedia.org/wiki/Chios Chios
-- A0333146	Ile de Zea	MED-AEGS	37.933333	23.65 : Empire Ottoman https://fr.wikipedia.org/wiki/K%C3%A9a_(%C3%AEle)
-- A1494718	Lapsaque	BSE-STRA	40.344167	26.685556 : Empire Ottoman https://fr.wikipedia.org/wiki/Lampsaque  Lampsaque / Lapseki
-- A1825289	Tagano dans la mer d'' Azof	BSE-RUSS	47.2273035475915	38.8908034936437 :  Russie Taganrog https://fr.wikipedia.org/wiki/Taganrog
-- A1415379	Hoggia Dei dans la Mer Noire et dans la partie ottomane	BSE-RUSS	46.483738	30.730705: Empire Ottoman Odessa https://en.wikipedia.org/wiki/Odessa 
-- reste ces 3 là à traduire
-- A0339127	Golfe de Monte Santo dans l'' archipel	MED-AEGN	40.2	23.933333 : Empire Ottoman
-- A0037077	Canal de Malte	MED-SIST	36.333333	15 : pas d'état ou malte A0036549
-- A1409167	Oczakou dans la Mer Noire	BSE-RUSS	46.616667	31.55 :  Empire ottoman A1416883


update ports.port_points set toponyme_standard_fr = 'Chios', toponyme_standard_en = 'Chios'
where uhgs_id = 'A0348656';
update ports.port_points set toponyme_standard_fr = 'Ares', toponyme_standard_en = 'Ares'
where uhgs_id = 'A0079430';
update ports.port_points set toponyme_standard_fr = 'Vilagarcia de Arousa', toponyme_standard_en = 'Vilagarcia de Arousa'
where uhgs_id = 'A0081452';
update ports.port_points set toponyme_standard_fr = 'Redondela', toponyme_standard_en = 'Redondela'
where uhgs_id = 'A0102450';
update ports.port_points set toponyme_standard_fr = 'Vasto', toponyme_standard_en = 'Vasto'
where uhgs_id = 'A0243974';
update ports.port_points set toponyme_standard_fr = 'Athènes', toponyme_standard_en = 'Athens'
where uhgs_id = 'A0333146';
update ports.port_points set toponyme_standard_fr = 'Lapseki', toponyme_standard_en = 'Lapseki'
where uhgs_id = 'A1494718';
update ports.port_points set toponyme_standard_fr = 'Taganrog', toponyme_standard_en = 'Taganrog'
where uhgs_id = 'A1825289';
update ports.port_points set toponyme_standard_fr = 'Odessa', toponyme_standard_en = 'Odessa'
where uhgs_id = 'A1415379';
update ports.port_points set toponyme_standard_fr = 'Ochakiv', toponyme_standard_en = 'Ochakov'
where uhgs_id = 'A1409167';
update ports.port_points set toponyme_standard_fr = 'golfe Singitique', toponyme_standard_en = 'Gulf of Sighitikos'
where uhgs_id = 'A0339127';
update ports.port_points set toponyme_standard_fr = 'Canal de Malte', toponyme_standard_en = 'Malta Channel'
where uhgs_id = 'A0037077';
	 	


insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0348656', 'Chios', 'Chios');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0079430', 'Ares', 'Ares');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0081452', 'Vilagarcia de Arousa', 'Vilagarcia de Arousa');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0102450', 'Redondela', 'Redondela');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0243974', 'Vasto', 'Vasto');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0333146', 'Athènes', 'Athens');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1494718', 'Lapseki', 'Lapseki');
--update labels_lang_csv set fr = 'Lapseki' where key_id='A1494718' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1825289', 'Taganrog', 'Taganrog');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1415379', 'Odessa', 'Odessa');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0037077', 'Canal de Malte', 'Malta Channel');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0339127', 'golfe Singitique', 'Gulf of Sighitikos');
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1415379', 'Ochakiv', 'Ochakov');

select * from etats where etat ilike 'Empire russe' order by subunit , toponyme
select * from etats where country2019_name  ilike '%China%'  order by subunit 

-- Costof en Crimée [Rostov-sur-le-Don] like A1825289 Tagano dans la mer d'' Azof / Russie - mer Noire
select * from etats where uhgs_id = 'A1763109' -- incorrect ; il faut regénérer belongings and state_xx
select toponyme_standard_fr, toponyme, uhgs_id from ports.port_points pp  where uhgs_id = 'A1763109' -- incorrect ; il faut regénérer belongings and state_xx

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Rostov-sur-le-Don', 'Costof en Crimée [Rostov-sur-le-Don]', 'A1763109', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1825289';

select uhgs_id , toponyme ,  state_1789_fr , substate_1789_fr , belonging_states , belonging_substates  , new_janvier2022 
from ports.port_points where state_1789_fr ilike '%Russie%' or state_1789_fr ilike 'Empire russe%'


-- A1825289	Tagano dans la mer d'' Azof	BSE-RUSS	47.2273035475915	38.8908034936437 :  Russie Taganrog https://fr.wikipedia.org/wiki/Taganrog
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Taganrog', 'Tagano dans la mer d'' Azof', 'A1825289', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1763109';
-- A1409167	Oczakou dans la Mer Noire	BSE-RUSS	46.616667	31.55 :  Russie A1416883
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Ochakiv', 'Oczakou dans la Mer Noire', 'A1415379', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0354220';
-- delete from etats where uhgs_id = 'A1763109';
-- A1415379	Hoggia Dei dans la Mer Noire et dans la partie ottomane	BSE-RUSS	46.483738	30.730705: Empire Ottoman Odessa https://en.wikipedia.org/wiki/Odessa 
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Odessa', 'Hoggia Dei dans la Mer Noire et dans la partie ottomane', 'A1415379', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1220285';
-- A0339127	Golfe de Monte Santo dans l'' archipel	MED-AEGN	40.2	23.933333 : Empire Ottoman
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Golfe de Monte Santo', 'Golfe de Monte Santo dans l'' archipel', 'A0339127', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1220285';
-- A0037077	Canal de Malte	MED-SIST	36.333333	15 : pas d'état ou malte A0036549
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Canal de Malte', 'Canal de Malte', 'A0037077', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0036549';
-- A0079430	Arez en Galice	ACE-FINI	43.433333	-8.233333 : https://fr.wikipedia.org/wiki/Ares_(La_Corogne), Espagne
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Ares', 'Arez en Galice', 'A0079430', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1964878';
-- A0081452	Villagancia en Galice	ACE-FINI	42.595493	 -8.765788 : Vilagarcía de Arousa https://fr.wikipedia.org/wiki/Vilagarc%C3%ADa_de_Arousa Espagne
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Vilagarcia de Arousa', 'Villagancia en Galice', 'A0081452', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1964878';
-- A0102450	Redondella en Galice	ACE-FINI	42.282225	 -8.609202 : https://fr.wikipedia.org/wiki/Redondela Espagne
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name,  'Redondela', 'Redondella en Galice', 'A0102450', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A1964878';

select * from etats where etat ilike '%Naples%' order by uhgs_id 
-- A0228170 Lipari avec Sicile
-- A0223015 Cap Stilo
select * from etats where uhgs_id in ('A0228170', 'A0223015') order by uhgs_id 

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Corigliano-Rossano', 'Rossano', 'A0233335', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0223015';
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Maiori', 'Maiori', 'A0242201', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0223015';
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Monasterace', 'Marina di Monasterace', 'A0234673', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0223015';
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Vasto', 'Vasto', 'A0243974', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0223015';

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Campofelice di Roccella', 'Campofelice', 'A0234281', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0228170';

-- A0235476	Coldirodi : république de Gènes / Coldirodi dans San Rémo A0250650
select * from etats where etat ilike '%nes%' order by uhgs_id 
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Coldirodi', 'Coldirodi', 'A0235476', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0250650';

select * from etats where etat ilike '%Ottoman%' order by uhgs_id 

-- A0336866	Katarina : Empire Ottoman / Grèce A0336877
select * from etats where etat ilike '%Ottoman%' order by uhgs_id 
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Katarina', 'Katarina', 'A0336866', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0336877';
-- A0348656	Schio [Chios]	
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Chios', 'Chios', 'A0348656', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0336877';
-- A0333146	Ile de Zea	MED-AEGS	37.933333	23.65 : Empire Ottoman https://fr.wikipedia.org/wiki/K%C3%A9a_(%C3%AEle)
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, null, 'Ile de Zea', 'A0333146', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0340214';
-- A1494718	Lapsaque	BSE-STRA	40.344167	26.685556 : Empire Ottoman https://fr.wikipedia.org/wiki/Lampsaque  Lampsaque / Lapseki
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Lampsaque', 'Lapsaque', 'A1494718', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0340214';

select * from etats where etat ilike '%Ottoman%' order by uhgs_id 
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Chios', 'Chios', 'A0348656', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0336877';




update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, etat, case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat_en) :: text as belonging_en
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id --and belonging_states is null;
-- 133/987/994
       
select * from port_points pp where pp.uhgs_id = 'A0228170'    

-- A0152878	Saint Cyr : La Ciotat, Provence  
-- tous les ports francais nouveau     
update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
            from (
                select  uhgs_id, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances
                from ports.port_points 
                where belonging_states is null and country2019_name = 'France'
                union 
                (select 'A0146289' as uhgs_id, json_build_object(1749 ||'-'||1815, 'Iceland')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'Islande')::text as appartenances)
            ) as k 
        where pp.uhgs_id = k.uhgs_id  and belonging_states is null;
--91 + 1 
       
select * from ports.port_points pp where pp.uhgs_id = 'A0152878'    
update ports.port_points pp set amiraute = 'La Ciotat', province = 'Provence', shiparea = 'MED-LIGS' where pp.uhgs_id = 'A0152878'   
 
update ports.port_points pp set belonging_substates = '['||k.appartenances ||']', belonging_substates_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, subunit,  case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit_en) :: text as belonging_en
                from ports.etats 
                WHERE subunit IS NOT null
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id --and belonging_substates is null;
-- 106 / 560 / 563 / 567
       
-- fonction qui a du être installée (absente, je ne sais pourquoi)
       CREATE OR REPLACE FUNCTION ports.extract_substate_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates::json) as elt  
					from ports.port_points p
					where belonging_substates = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE; 

update ports.port_points set state_1789_fr = ports.extract_state_fordate(belonging_states, 1789) 
            where belonging_states is not null  and state_1789_fr is null; 
           -- 1421 / 1434 / 1424 / 1428
update ports.port_points set state_1789_en = ports.extract_state_en_fordate(belonging_states_en, 1789) 
            where belonging_states_en is not null and state_1789_en is null;   
-- 224    / 1421   / 1434 / 1424 / 1428
       
-- select uhgs_id , belonging_substates, toponyme from port_points             
update ports.port_points set substate_1789_fr = ports.extract_substate_fordate(belonging_substates, 1789) 
where belonging_substates is not null and substate_1789_fr is null;
--  561  / 564  / 568
-- petit bug à corriger après ?!      
-- select * from   ports.port_points where substate_1789_en='false';
update ports.port_points set substate_1789_fr = null where substate_1789_fr='false';

update ports.port_points set substate_1789_en = ports.extract_substate_en_fordate(belonging_substates_en, 1789) 
where belonging_substates_en is not null  ;
-- 117 / 561  / 1434 / 564  / 568
update ports.port_points set substate_1789_en = null where substate_1789_en='false';--872

update ports.port_points set state_1789_en = null where state_1789_en='false';--5
update ports.port_points set state_1789_fr = null where state_1789_fr='false';--5


update ports.port_points pp set relation_state = k.appartenances
                from (
                select uhgs_id, json_agg(arelation) as appartenances
                    from
                    (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                    case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
                    'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
                    from (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is not null
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is null
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is not null 
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is  null 
                        )
                    ) as k 
                    group by uhgs_id, etat, geonameid, tgnid
                    ) as k
                    group by uhgs_id
                    order by uhgs_id
                ) as k
                where pp.uhgs_id = k.uhgs_id ;--and relation_state is null
                
                
update ports.port_points pp set relation_state = k.appartenances ::text
            from (
            select uhgs_id,  json_agg(arelation) as appartenances
                from
                (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                'http://www.geonames.org/'||geonameid ,
                'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
                from (
                    
                    select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    from ports.port_points 
                    where relation_state is null and country2019_name = 'France'
                    union all
                    (
                    select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    )
                ) as k 
                group by uhgs_id, etat, geonameid
                ) as k
                group by uhgs_id
                order by uhgs_id
            ) as k
            where pp.uhgs_id = k.uhgs_id ; --1
            
select state_1789_fr , substate_1789_fr, state_1789_en, substate_1789_en,  relation_state , 
toponyme, uhgs_id, ferme_direction , ferme_bureau , ferme_bureau_uncertainty, ferme_direction_uncertainty 
from port_points pp where pp.uhgs_id in ('A0152878', 'A0167567', 'A0235476', 'B0000714')


select relation_state , toponyme, uhgs_id from port_points pp where substring (relation_state from 1 for 1)='[' and uhgs_id = 'A0167567'

-- supprimer des [] sur relation_state
update ports.port_points set relation_state = substring(relation_state from 2 for length(relation_state)-2) where substring (relation_state from 1 for 1)='[';
-- 990

--- REPRISE ICI le 03 Mars

-- Reprendre l'import liste_ports_nouveaux_v2 pour la mise à jour des provinces amirauté etc. 
-- ferme	bureau	ferme_bureau_uncertainty

update ports.port_points p
set province=v.province , amiraute=v.amiraute , 
ferme_direction = v.ferme , ferme_bureau = v.bureau , 
ferme_bureau_uncertainty = v.ferme_bureau_uncertainty ,
ferme_direction_uncertainty = -1,
partner_balance_1789=v.partner_balance_1789 ,  
partner_balance_supp_1789=v.partner_balance_supp_1789 , 
partner_balance_supp_1789_uncertainty=v.partner_balance_supp_1789_uncertainty ,
partner_balance_1789_uncertainty=v.partner_balance_1789_uncertainty
from liste_ports_nouveaux_v2 v 
where p.new_janvier2022 is true and v.province is not null and v.province<>'0'  and p.uhgs_id = v.uhgs_id ;
--86

select toponyme_standard_fr,uhgs_id, province, amiraute,  ferme_direction, ferme_bureau, partner_balance_1789, partner_balance_supp_1789
from ports.port_points p
where toponyme in ('Séné', 'Larneur [Larmor]', 'Gavre', 'Milfort')

update ports.port_points p 
set partner_balance_supp_1789='colonies françaises', ferme_direction=null, ferme_bureau=null , 
ferme_bureau_uncertainty=null, ferme_direction_uncertainty=null
where toponyme in ('Milfort') and uhgs_id = 'B2046784';

update ports.port_points p 
set province='Bretagne', amiraute='Vannes',
partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
partner_balance_1789=null, partner_balance_1789_uncertainty=null,
ferme_direction='Lorient', ferme_bureau='Vannes' , ferme_bureau_uncertainty=-1, ferme_direction_uncertainty=0
where toponyme in ('Séné') and uhgs_id = 'A0135962';
	
update ports.port_points p 
set province='Bretagne', amiraute='Lorient',
partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
partner_balance_1789=null, partner_balance_1789_uncertainty=null,
ferme_direction='Lorient', ferme_bureau='Lorient' , ferme_bureau_uncertainty=0, ferme_direction_uncertainty=0
where toponyme in ('Larneur [Larmor]') and uhgs_id = 'A0149474';

update ports.port_points p 
set province='Bretagne', amiraute='Lorient',
partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
partner_balance_1789=null, partner_balance_1789_uncertainty=null,
ferme_direction='Lorient', ferme_bureau='Lorient' , ferme_bureau_uncertainty=0, ferme_direction_uncertainty=0
where toponyme in ('Gavre') and uhgs_id = 'A0178449';

update ports.port_points p 
set province='Saintonge', amiraute='Marennes',
partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
partner_balance_1789=null, partner_balance_1789_uncertainty=null,
ferme_direction='La Rochelle', ferme_bureau='Marennes' , ferme_bureau_uncertainty=-1, ferme_direction_uncertainty=0
where toponyme in ('Breuil') and uhgs_id = 'A1968855';

update ports.port_points p 
set province='Aunis', amiraute='La Rochelle',
partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
partner_balance_1789=null, partner_balance_1789_uncertainty=null,
ferme_direction='La Rochelle', ferme_bureau='Saint-Martin île de Ré' , ferme_bureau_uncertainty=-1, ferme_direction_uncertainty=0
where toponyme in ('Fort de la Prée') and uhgs_id = 'A1968856';

update ports.port_points p 
set province='Saintonge', amiraute='Marennes',
partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
partner_balance_1789=null, partner_balance_1789_uncertainty=null,
ferme_direction='La Rochelle', ferme_bureau='Marennes' , ferme_bureau_uncertainty=-1, ferme_direction_uncertainty=0
where toponyme in ('Marechalle') and uhgs_id = 'A1968904';

update ports.port_points p
set partner_balance_1789=null  
from liste_ports_nouveaux_v2 v 
where p.new_janvier2022 is true and v.province is not null and v.province<>'0'  and p.uhgs_id = v.uhgs_id ;
-- 86

select * from ports.port_points where partner_balance_supp_1789 is null and country2019_name = 'France';
select * from ports.port_points where  uhgs_id = 'A0310406';
/*
 * A1968855 La Rochelle	Marennes
A1968856 La Rochelle Saint-Martin île de Ré
A1968904
 */
update ports.port_points set partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0
where partner_balance_supp_1789 is null and country2019_name = 'France' and province is not null ;
-- 77 + 8 

select * from ports.port_points where uhgs_id = 'A0152878'
update port_points p set ferme_direction = 'Toulon' , ferme_bureau = 'Toulon', ferme_bureau_uncertainty = 0, ferme_direction_uncertainty = 0
where uhgs_id = 'A0152878'; -- Saint Cyr

update ports.port_points set partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0
        where uhgs_id = 'A0152878'; -- Saint Cyr

select toponyme, partner_balance_supp_1789 from ports.port_points where uhgs_id in ('A0233335', 'A0234673', 'A0234281', 'A0235476', 'A0336866', 'A0242201', 'A1907798', 'D3674030')
update ports.port_points set partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where uhgs_id in ('A0233335', 'A0234673', 'A0234281', 'A0235476', 'A0336866', 'A0242201', 'A1907798', 'D3674030')

        
select distinct partner_balance_1789 from ports.port_points
-- Russie, Espagne
select  * from ports.port_points where partner_balance_1789 = '0'

update ports.port_points set partner_balance_1789='Russie', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0, partner_balance_1789_uncertainty  = 0
        where uhgs_id in ('A1409167', 'A1825289');
update ports.port_points set partner_balance_1789='Espagne', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0, partner_balance_1789_uncertainty  = 0
        where uhgs_id in ('A0079430', 'A0081452', 'A0102450');

-- debug de etat et attributs dérivées 
       /*
select * from ports.port_points where state_1789_fr ilike '%Mecklenbourg%'
select * from etats where uhgs_id = 'A1763109' -- correct ; il faut regénérer belongings and state_xx
select * from ports.port_points where state_1789_fr ilike '%Oldenbourg%'
select uhgs_id , state_1789_fr , substate_1789_fr , belonging_states , belonging_substates  , new_janvier2022 
from ports.port_points where state_1789_fr ilike '%Russie%' or state_1789_fr ilike 'Empire russe%'
select distinct state_1789_fr from ports.port_points
select distinct etat, subunit from ports.etats order by etat

select partner_balance_1789, * from ports.port_points where country2019_name ilike '%Germany%'

*/
       


update ports.port_points set partner_balance_1789 = 'Etats de l''Empereur' ,  partner_balance_1789_uncertainty=-1,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Autriche', 'Duché de Mecklenbourg', 'Duché d''Oldenbourg', 'Evêché de Münster'); --16

update ports.port_points set partner_balance_1789 = 'Prusse' ,  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Prusse'); --20
        
update ports.port_points set partner_balance_1789 = 'Quatre villes hanséatiques' , partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Hambourg', 'Lubeck', 'Brême') or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Dantzig');
--4
update ports.port_points set partner_balance_1789 = 'Etats-Unis' ,  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Etats-Unis d''Amérique'); --43
update ports.port_points set partner_balance_1789='Angleterre',  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Grande-Bretagne' ;--240
update ports.port_points set partner_balance_1789='Danemark', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Danemark';--60
update ports.port_points set partner_balance_1789='Espagne',  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Espagne';--101
update ports.port_points set partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Provinces-Unies'; --72
update ports.port_points set partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0
        where substate_1789_fr  = 'Colonies françaises d''Amérique'; --30        
update ports.port_points set partner_balance_1789='Portugal',  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  = 'Portugal'; --14
update ports.port_points set partner_balance_1789='Russie', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  = 'Empire russe';--11
update ports.port_points set partner_balance_1789='Suède', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  = 'Suède';--37
update ports.port_points set partner_balance_1789='Bayonne', partner_balance_1789_uncertainty=0 where uhgs_id = 'A0187995';
update ports.port_points set partner_balance_1789='Dunkerque',  partner_balance_1789_uncertainty=0 where uhgs_id = 'A0204180';       
update ports.port_points set partner_balance_1789='Marseille',  partner_balance_1789_uncertainty=0 where uhgs_id = 'A0210797';      
update ports.port_points set partner_balance_1789='Lorient', partner_balance_1789_uncertainty=0 where uhgs_id = 'A0140266' ;
update ports.port_points set partner_balance_1789='Petites Iles', partner_balance_1789_uncertainty=-1 where uhgs_id in ('A0165077', 'A0136403');
update ports.port_points set partner_balance_1789='Saint-Domingue',  partner_balance_1789_uncertainty=0 where province % 'Domingue' ;        
update ports.port_points set partner_balance_1789='Saint-Jean de Luz',  partner_balance_1789_uncertainty=-1 where uhgs_id = 'A0122594';

        

        
select toponyme_standard_fr , uhgs_id , substate_1789_fr, state_1789_en , partner_balance_supp_1789 , partner_balance_supp_1789_uncertainty
from ports.port_points where state_1789_fr != 'France' and province is not null

update ports.port_points set partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
where state_1789_fr != 'France' and province is not null and partner_balance_supp_1789 is null
-- 27 ports anglais

-- mettre à jour les shipping area des 27 ports

update port_points set shiparea = 'ANE-MALI' where uhgs_id in ('A0398623', 'A0380284');
update port_points set shiparea = 'ANE-IRSE' where uhgs_id in ('A0410502', 'A0388793', 'A0399010');
update port_points set shiparea = 'ANE-LUND' where uhgs_id in ('A0399213');
update port_points set shiparea = 'ANE-PORT' where uhgs_id in ('A0404560');
update port_points set shiparea = 'NOR-HUMB' where uhgs_id in ('A0380965', 'A0384630', 'A0394459', 'A0381883');
update port_points set shiparea = 'NOR-THAM' where uhgs_id in ('A0406758', 'A0403623', 'A0635679');
update port_points set shiparea = 'NOR-DOVE' where uhgs_id in ('A0408933', 'A0056917');
update port_points set shiparea = 'ACE-IROI' where uhgs_id in ('A0192878', 'A0198704');
update port_points set shiparea = 'ACE-ROCH' where uhgs_id in ('A0214663');
update port_points set shiparea = 'MED-TYWS' where uhgs_id in ('A0229189');
update port_points set shiparea = 'NOR-CROM' where uhgs_id in ('A0380443', 'A0400354');
update port_points set shiparea = 'ACE-CANT' where uhgs_id in ('A0107371');
update port_points set shiparea = 'MED-ALBO' where uhgs_id in ('A0079492', 'A1965169');
update port_points set shiparea = 'ANE-FISL' where uhgs_id in ('A0380426');
update port_points set shiparea = 'NOR-GBIG' where uhgs_id in ('A0642340');

-- Avec QGIS, j'ai mis à jour presque tous les shipping area
select   * from port_points where shiparea is null and new_janvier2022 is false;
update port_points set shiparea = 'ACE-ROCH' where uhgs_id in ('A1968855', 'A1968904', 'A1968856');
-- 19 lignes // 31 avec les new.

-- reprendre le travail sauvegardé dans un fichier Excel
update port_points p set shiparea = v.shiparea
from liste_ports_nouveaux_v3 v 
where p.new_janvier2022 is true and v.shiparea<>'0'  and p.uhgs_id = v.uhgs_id ;
-- est-ce qu'il y a des doublons de uhgs_id
select uhgs_id, count(*)
from ports.port_points
group by uhgs_id
having count(*) > 1

select * from ports.port_points where uhgs_id = 'A0717276'
-- 1390 -- Waberg
-- 1389 -- Waberg
delete from ports.port_points where ogc_fid = 1390


-- pour les nouveaux ports janvier 2022
-- spécifier Etranger pour partner_balance_supp_1789
UPDATE ports.port_points SET partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 
WHERE state_1789_fr in ('Empire ottoman', 'Malte') AND partner_balance_supp_1789 IS null;
-- 5/7

UPDATE ports.port_points SET partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 
WHERE uhgs_id in ('A0243974') AND partner_balance_supp_1789 IS null
-- 1

UPDATE ports.port_points SET partner_balance_1789='Saint-Domingue', partner_balance_1789_uncertainty=0 
WHERE province = 'Saint-Domingue' 
-- 19 / 16

UPDATE ports.port_points SET partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0 
WHERE amiraute = 'Port-Louis (Tobago)' AND partner_balance_1789 IS NULL
-- 2/1



update ports.port_points set partner_balance_1789 = 'Etats de l''Empereur' ,  partner_balance_1789_uncertainty=-1
        where state_1789_fr in ('Autriche', 'Duché de Mecklenbourg', 'Duché d''Oldenbourg')

-----------------------------------------------------------------------------------------------
-- Les tables de navigo avec LoadFilemaker
-----------------------------------------------------------------------------------------------
         -- STOP ICI l'après-midi le 3 mars

ANF, G5-76/5854 et 5860 -- Dunkerque is missing



select distinct 'G5', source, substring(source from 1 for position('/' in source)-1) as component, substring(source from 1 for position('/' in source))
     from navigocheck.check_pointcall where 
     (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
      --and substring(source from 1 for position('/' in source)) not like 'G5-%'
      ) 
      and position('/' in source) > 0 
      and source = 'ANF, G5-76/5854 et 5860'
order by component
                
select pointcall_name, pointcall_action, trim(source), documentary_unit_id , data_block_local_id 
from navigo.pointcall p where trim(source) not in (
select "source" from navigoviz.source) 
order by  source

select * from  navigoviz.source 
where documentary_unit_id in (
	select documentary_unit_id 
	from navigo.pointcall p where trim(source) not in (
		select "source" from navigoviz.source
	) -- les doc missing
)
select documentary_unit_id , pointcall_name, pointcall_action, "source" 
	from navigo.pointcall p 
	where documentary_unit_id not in (
		select documentary_unit_id from navigoviz.source
	) -- les doc missing
select data_block_local_id , pointcall_name, pointcall_action, "source" 
	from navigo.pointcall p 
	where data_block_local_id not in (
		select data_block_local_id from navigoviz.source
	) -- les doc missing
	
select * from navigoviz.source where component like '%ANF, G5-76%'
select * from navigoviz.source where data_block_local_id = '00335413'
select distinct data_block_local_id, source, 'G5', 'ANF, G5-76', 'A0204180', 	'Dunkerque'
from navigocheck.check_pointcall where source like 'ANF, G5-76/5854 et 5860' 


select distinct suite from navigoviz."source" s 
--G5
--Expéditions coloniales Marseille (1789)
--la Santé registre de patentes de Marseille
--Registre du petit cabotage (1786-1787)

select source_suite, extract(year from outdate_fixed ), count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'G5' -- and p.pointcall_uhgs_id = 'A0210797'
 group by source_suite, extract(year from outdate_fixed )
 /*
  * G5	1787	31675
G5	1789	9830
G5		168
  */
 
 select source_suite, extract(year from coalesce(outdate_fixed, indate_fixed )), count( source_doc_id) as c 
from navigoviz.pointcall p 
--where p.pointcall_function = 'O' 
 group by source_suite, extract(year from coalesce(outdate_fixed, indate_fixed ))
 order by extract(year from coalesce(outdate_fixed, indate_fixed )), source_suite
 
select distinct source from navigo.pointcall p where source not in (select distinct s.source from navigoviz."source" s )

select * from navigoviz.pointcall p where p.source_suite is null

select * from navigo.pointcall p where source ilike 'ANF, G5-100/%'
-- Le Havre - Année 1787
select * from navigo.pointcall p where source ilike 'ANF, G5-118/%'
-- Congés, Marennes (1786-1790) - Année 1789
select * from navigo.pointcall p where source ilike 'ANF, G5-39B/%'
-- Congés, Aligre de Marans (1781-1789) - Année 1789
select * from navigo.pointcall p where source ilike 'ANF, G5-62/%'
-- Congés, Charente (Tonnay Charente) (1781-1790)  - Année 1789
select * from navigo.pointcall p where source ilike 'ANF, G5-76/%'
-- Congés, Dunkerque (1789)   - Année 1789
select * from navigo.pointcall p where source ilike 'ANF, G5-50/%'
-- Congés, Bordeaux (1787)  - Année 1787

select count(*) from navigo.pointcall -- 109104
select count(*) from navigoviz.pointcall -- 109098

select * from navigocheck.check_pointcall where  source='ANF, G5-62/rafraîchi' and  data_block_local_id = '00149228' 
-- IMPORTANT, à faire avant de construire les sources
update navigocheck.check_pointcall set source = 'ANF, G5-62/rafraîchi' where source='ANF, G5-62/rafraichi' and data_block_local_id = '00149228' and record_id = '00155198';       
update navigocheck.check_pointcall set source = 'ANF, G5-76/5854 et 5860' where source='ANF, G5-76/5854' and data_block_local_id = '00335413' and record_id = '00345314'   ;    
update navigocheck.check_pointcall set source = 'ADBdR, 200E, 545/0283' where source='ADBdR, 200E, 545/' and data_block_local_id = '00310213' and record_id in ('00310214', '00310215', '00310216') ;      
update navigocheck.check_pointcall set source = 'ADBdR, 200E, 545/0757' where source='ADBdR, 200E, 545/' and data_block_local_id = '00353709' and record_id in ('00353710')      ; 
-- done le 03 mars 2022 sur localhost 5435 portic_v7

select data_block_local_id , source, record_id,  *
from navigocheck.check_pointcall
where data_block_local_id = '00149228'


select * from navigo.pointcall p 
where pkid not in (select pkid from navigoviz.pointcall) 
order by p.component_description__component_short_title ,data_block_local_id
-- prend la vie des rats Z
source not in (select distinct s.source from navigoviz."source" s )
---------------------------------------------------------------

-- taxes : pointcall__data_block_local_id has disappeared

                select   documentary_unit_id , link_to_pointcall , c.tax_concept, null as payment_date , c.q01, c.q01_u, c.q02, c.q02_u, c.q03, c.q03_u  
                from navigocheck.check_taxes c  where     link_to_pointcall is null  
                -- 73464 / 88042
                -- 15583 lignes vides de link_to_pointcall
                
				select link_to_pointcall , *--, c.cargo__source , c.link_to_cargo_item , c.cargo__documentary_unit_id, *
                from navigocheck.check_cargo c   where     link_to_pointcall is null  
                -- 1005 lignes / 0 lignes
                
                
                select link_to_pointcall , c.tax_concept, null as payment_date , c.q01, c.q01_u, c.q02, c.q02_u, c.q03, c.q03_u  
                from navigocheck.check_taxes c
                -- 73464
                
-- cargo ok

                
                select c.pointcall__data_block_local_id as  link_to_pointcall, c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                 c.dictionary_commodities__permanent_coding as commodity_permanent_coding, c.cargo_item_action,
                labels.fr as commodity_standardized_fr,  labels.en as commodity_standardized_en  
                from navigocheck.check_cargo c, ports.labels_lang_csv labels
                where label_type= 'commodity' and key_id = c.commodity_id
                
-- pointcall
-- select pointcall_indate from navigo.pointcall cp where pointcall_indate = '1759<02<29' 
-- select pointcall_indate from navigocheck.check_pointcall cp  where record_id in ('00175018', '00175019')
-- Rejouer ces corrections avant BuildNavigoviz
update    navigo.pointcall set  pointcall_indate =   '1789>12>01'     where record_id in ('00345977', '00349763');
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>12>01'  where record_id in ('00345977', '00349763');
update    navigo.pointcall set  pointcall_indate =   '1787>04>17!'     where record_id in ('00154851');
update    navigocheck.check_pointcall set  pointcall_indate =   '1787>04>17!'  where record_id in ('00154851');
update    navigo.pointcall set  pointcall_indate =   '1789>05>01'     where record_id in ('00310621', '00310050');
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>05>01'  where record_id in ('00310621', '00310050');
update    navigo.pointcall set  pointcall_indate =   '1789>10>01'     where record_id in ('00364039');
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>10>01'  where record_id in ('00364039');
update    navigo.pointcall set  pointcall_indate =   '1787>03>01'     where record_id in ('00175018', '00175019', '00157431');
update    navigocheck.check_pointcall set  pointcall_indate =   '1787>03>01'  where record_id in ('00175018', '00175019', '00157431');
update    navigo.pointcall set  pointcall_outdate =   '1787=05=01'     where record_id in ('00185827');
update    navigocheck.check_pointcall set  pointcall_outdate =   '1787=05=01'  where record_id in ('00185827');
update    navigo.pointcall set  pointcall_outdate =   '1789=12=01'     where record_id in ('00336076', '00339862');
update    navigocheck.check_pointcall set  pointcall_outdate =   '1789=12=01'  where record_id in ('00336076', '00339862');

update    navigo.pointcall set  pointcall_indate =   '1788>12>01'     where  record_id in ('00306428', '00306976'); -- 1788>11>31 
update    navigocheck.check_pointcall set  pointcall_indate =   '1788>12>01'  where record_id in ('00336076', '00339862');
update    navigoviz.pointcall set  pointcall_in_date2 =   '1788=12=01'     where  record_id in ('00306428', '00306976'); -- 1788>11>31 

-- tout le block : 30 lignes
-- STOP ici le 03/03/2022
-- reprendre Mardi : 1. build  navigoviz : sources, pointcalls, raw_flows et travels.

select pointcall_indate, pointcall_indate_date,pointcall_indate_date is not null, record_id from navigo.pointcall where record_id in ('00306428', '00306976')
select pointcall_indate, pointcall_indate_date, record_id, * from navigo.pointcall where pointcall_indate = '1788>12>01'
pointcall_indate = '1788>11>31'
pointcall_indate_date
 update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null
select pointcall_in_date, record_id , to_date(pointcall_in_date2, 'YYYY=MM=DD')
from navigoviz.pointcall where record_id in ('00306428', '00306976')

comment on column navigoviz.source.suite is '[G5 (for congés)] or [Registre du petit cabotage (1786-1787)] or [Expéditions coloniales Marseille (1789)] [la Santé registre de patentes de Marseille]';

select record_id , data_block_local_id, pointcall_outdate as pointcall_out_date, 
            pointcall_action, 
            to_date(pointcall_outdate_date, 'DD/MM/YYYY') as outdate_fixed,
            pointcall_indate as pointcall_in_date, 
            to_date(pointcall_indate_date, 'DD/MM/YYYY') as indate_fixed
from navigocheck.check_pointcall

alter table navigoviz.pointcall add column  pointcall_in_date2 text;
alter table navigoviz.pointcall add column  pointcall_out_date2 text;
	        
update navigoviz.pointcall 
            set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
                                            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(pointcall_in_date, '!', ''), '>', '=')  else replace(pointcall_in_date, '>', '=')  end 
                                     end 
            where position('00' in pointcall_in_date ) = 0
update navigoviz.pointcall set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(replace(pointcall_in_date, '00', '01'), '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(replace(pointcall_in_date, '00', '01'), '!', ''), '>', '=')  else replace(replace(pointcall_in_date, '00', '01'), '>', '=')  end
            end  where position('00' in pointcall_in_date ) > 0
 
update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null   

update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(pointcall_out_date, '>', '=') end  
            where position('00' in pointcall_out_date ) = 0 and position('99' in pointcall_out_date ) = 0
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '00', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(replace(pointcall_out_date, '00', '01'), '>', '=') end  
                    where position('00' in pointcall_out_date ) > 0   
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '99', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(replace(pointcall_out_date, '99', '01'), '>', '=') end  
                    where position('99' in pointcall_out_date ) > 0
update navigoviz.pointcall set outdate_fixed= to_date(pointcall_out_date2, 'YYYY=MM=DD')  where pointcall_out_date is not null

select * from navigoviz.pointcall where pointcall_in_date2 = '1788=11=31'
00306428


select pointcall_indate , pointcall_indate_date from navigocheck.check_pointcall  
where record_id in ('00306428', '00306976')
1788>11>31 '1788>12>01'


select * from navigocheck.check_pointcall where pointcall_indate = '1788=11=31'
select * from navigo.pointcall  where pointcall_indate_date = '31/11/1788'
select to_date(pointcall_indate_date, 'DD/MM/YYYY') from navigocheck.check_pointcall
-- 1787=02=29 -- pointcall_in_date2
-- 1787=04=31 -- pointcall_out_date2
-- 1789=11=31 -- pointcall_out_date2
select record_id , data_block_local_id , pointcall_date , pointcall_indate , pointcall_indate_date 
from navigocheck.check_pointcall where pointcall_indate like '1787>02>29!' -- 00157431

select record_id , data_block_local_id , pointcall_date , pointcall_outdate , pointcall_outdate_date 
from navigocheck.check_pointcall where pointcall_outdate like '1787%04%31%' -- 00185827
select record_id , data_block_local_id , pointcall_date , pointcall_outdate , pointcall_outdate_date 
from navigocheck.check_pointcall where pointcall_outdate like '1789%11%31%' -- 00336076, 00339862

select * from navigoviz.pointcall where pointcall_in_date2 like '1787=02=29' -- 00157431
update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null
update navigoviz.pointcall set outdate_fixed= to_date(pointcall_out_date2, 'YYYY=MM=DD')  where pointcall_out_date is not null

-- Avant de construire source
update navigocheck.check_pointcall set source = 'ANF, G5-62/rafraîchi' where source='ANF, G5-62/rafraichi' and data_block_local_id = '00149228' and record_id = '00155198';       
update navigocheck.check_pointcall set source = 'ANF, G5-76/5854 et 5860' where source='ANF, G5-76/5854' and data_block_local_id = '00335413' and record_id = '00345314'   ;    
update navigocheck.check_pointcall set source = 'ADBdR, 200E, 545/0283' where source='ADBdR, 200E, 545/' and data_block_local_id = '00310213' and record_id in ('00310214', '00310215', '00310216') ;      
update navigocheck.check_pointcall set source = 'ADBdR, 200E, 545/0757' where source='ADBdR, 200E, 545/' and data_block_local_id = '00353709' and record_id in ('00353710')      ; 

-----------------------------------------------------------------------------------------
-- ZUT encore des trucs à compléter sur les ports
-----------------------------------------------------------------------------------------

-- terminer labels_lang_csv : à faire
SELECT pp.uhgs_id, pp.country2019_name  , toponyme , pp.toustopos, pp.toponyme_standard_fr, pp.toponyme_standard_en , pp.state_1789_fr, pp.substate_1789_fr  
FROM ports.port_points pp WHERE pp.new_janvier2022 IS true and toponyme_standard_fr is null
-- or uhgs_id in ('A0799311', 'A0235476', 'A0234281')
-- a intégrer dans labels_lang_csv
SELECT pp.toponyme , pp.toponyme_standard_fr, pp.toponyme_standard_en , nv.* 
FROM ports.port_points pp , ports.liste_ports_nouveaux_v3 nv 
WHERE pp.new_janvier2022 IS true and toponyme_standard_fr is null and nv.uhgs_id =pp.uhgs_id

select * from ports.etats where uhgs_id in ('A0799311', 'A0235476', 'A0234281', 'A1968907', 'A1968861', 'A1968898')




insert into etats (country2019_name, toponyme_standard_fr, toponyme , uhgs_id, etat, etat_en, wikipedia)
values ('Germany', 'Papenbourg', 'Papembourg', 'A0799311', 'Evêché de Münster', 'Prince-Bishopric of Münster', 'https://fr.wikipedia.org/wiki/Papenbourg');
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Arum', 'Anereun', 'A1968907', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0617176';
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'IJist', 'Eylet [IJist]', 'A1968861', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0617176';
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select country2019_name, 'Whitstable', 'Whistable', 'A1968898', etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en
from etats where uhgs_id = 'A0409139';

update ports.port_points set toponyme_standard_fr = 'Arum', toponyme_standard_en = 'Arum' where uhgs_id = 'A1968907';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1968907', 'Arum', 'Arum');
update ports.port_points set toponyme_standard_fr = 'IJist', toponyme_standard_en = 'IJist' where uhgs_id = 'A1968861';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1968861', 'IJist', 'IJist');
update ports.port_points set toponyme_standard_fr = 'Whitstable', toponyme_standard_en = 'Whitstable' where uhgs_id = 'A1968898';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1968898', 'Whitstable', 'Whitstable');
update ports.port_points set toponyme_standard_fr = 'La Flotte', toponyme_standard_en = 'La Flotte' where uhgs_id = 'A1968856';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1968856', 'La Flotte', 'La Flotte');
update ports.port_points set toponyme_standard_fr = 'Port-des-Barques', toponyme_standard_en = 'Port-des-Barques' where uhgs_id = 'A1968904';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1968904', 'Port-des-Barques', 'Port-des-Barques');
update ports.port_points set toponyme_standard_fr = 'Breuillet', toponyme_standard_en = 'Breuillet' where uhgs_id = 'A1968855';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A1968855', 'Breuillet', 'Breuillet');
update ports.port_points set toponyme_standard_fr = 'Papenbourg', toponyme_standard_en = 'Papenburg' where uhgs_id = 'A0799311';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0799311', 'Papenbourg', 'Papenburg');
update ports.port_points set toponyme_standard_fr = 'Coldirodi', toponyme_standard_en = 'Coldirodi' where uhgs_id = 'A0235476';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0235476', 'Coldirodi', 'Coldirodi');
update ports.port_points set toponyme_standard_fr = 'Campofelice', toponyme_standard_en = 'Campofelice' where uhgs_id = 'A0234281';
insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'A0234281', 'Campofelice', 'Campofelice');

update ports.port_points set province = 'Kent' where uhgs_id='A1968898' and toponyme_standard_fr ='Whitstable';


/*
 * A1968907 Anereun https://fr.wikipedia.org/wiki/Arum_(Frise) 
A1968861 Eylet [IJist] IJist IJist https://fr.wikipedia.org/wiki/IJlst Frise
A1968898 Whistable Whitstable est une ville côtière et station balnéaire du nord-est du Kent en Angleterr https://fr.wikipedia.org/wiki/Whitstable
A1968856 Fort de la Prée La Rochelle, Aunis, La Flotte https://fr.wikipedia.org/wiki/Fort_La_Pr%C3%A9e
A1968904 Marechalle Le Maréchat https://fr.wikipedia.org/wiki/Port-des-Barques Marennes, Saintonge
A1968855 Breuil https://fr.wikipedia.org/wiki/Breuillet_(Charente-Maritime) Marennes, Saintonge
A0799311 Papembourg Papenbourg	Papenburg  Evêché de Münster		Prince-Bishopric of Münster https://fr.wikipedia.org/wiki/Papenbourg

Campofelice Campofelice	Campofelice
Coldirodi Coldirodi	Coldirodi
Lapsaque
Ile de Zea
Canal de Malte
Oczakou dans la Mer Noire
Redondella en Galice
Tagano dans la mer d' Azof
Hoggia Dei dans la Mer Noire et dans la partie ottomane
Schio [Chios]
Arez en Galice
Golfe de Monte Santo dans l' archipel
Vasto dans le royaume de Naples
Villagancia en Galice
*/
 */
select data_block_local_id, record_id, source 
from navigocheck.check_pointcall 
where 
data_block_local_id in ('00149228', '00335413', '00310213', '00353709')
order by data_block_local_id;
-- extraction pour Silvia et Jean-Pierre le 09 mars 2022
------------------------------------------------------------------------------------
--- Analyse
------------------------------------------------------------------------------------

select source_doc_id, ship_id, pointcall_rankfull, p.pointcall_out_date, p.outdate_fixed , p.pointcall_in_date , p.indate_fixed , substring(p.pointcall_rank_dedieu from 1 for position('.' in p.pointcall_rank_dedieu) -1 ) ::int as dedieu_rank
from navigoviz.pointcall p
-- where substring(p.pointcall_rank_dedieu from 1 for position('.' in p.pointcall_rank_dedieu) -1 ) ::int <> p.pointcall_rankfull 
order by ship_id, pointcall_rankfull

-- pointcall_function
select  pointcall_function, navigo_status, count(*) 
from navigoviz.pointcall
group by pointcall_function, navigo_status 

select source_doc_id, source_suite, ship_id, pointcall_function, navigo_status, pointcall_rankfull, pointcall_out_date, outdate_fixed , pointcall_in_date , indate_fixed 
from navigoviz.pointcall 
where pointcall_function = 'T' and navigo_status='PC-RS'  and not position ('=' in coalesce (pointcall_out_date, pointcall_in_date ) ) > 0


T	PU-RS	4
T	PC-RS	3
	PU-RS	3
Z	PC-RS	1


select record_id, source_doc_id, source_suite, p.pointcall , ship_id, pointcall_function, navigo_status, pointcall_rankfull, pointcall_rank_dedieu, pointcall_out_date, pointcall_in_date, net_route_marker , data_block_leader_marker 
from navigoviz.pointcall p
where 
--source_doc_id in (select source_doc_id from navigoviz.pointcall where  navigo_status='PC_RS') and 
source_doc_id = '00356689'
order by ship_id, pointcall_rankfull--, pointcall_rankfull
-- erreur sur le statut FC_RS : PC-RS / 00186176 / 00184314 / 00186557 / 00186138
-- erreur sur le statut PC_RS : FC-RS / 00189834 
--  erreur sur le statut PC_RS : FC-RS / 00356689 / 00189667 / 00184484
update navigoviz.pointcall set navigo_status='PC-RS'
where pointcall_function is null and navigo_status='FC-RS'
and position ('=' in coalesce (pointcall_out_date, pointcall_in_date ) ) > 0
-- 4 
update navigoviz.pointcall set navigo_status='FC-RS'
where source_doc_id  =  '00189834' and pointcall_function = 'Z' and navigo_status='PC-RS' ;
update navigoviz.pointcall set navigo_status='FC-RS'
where source_doc_id  in ('00356689', '00189667', '00184484' ) and pointcall_function = 'T' and navigo_status='PC-RS' 

select source_doc_id, source_suite, p.pointcall , ship_id, pointcall_function, navigo_status, pointcall_rankfull, pointcall_out_date, pointcall_in_date, net_route_marker , data_block_leader_marker , pointcall_rank_dedieu 
from navigoviz.pointcall p
where source_doc_id  = '00184484'
-- where ship_id = '0014700N'
order by ship_id, pointcall_rankfull
--pointcall_rank_dedieu
    

/* Correction du pointcall_rankfull
update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), coalesce( pointcall_in_date, pointcall_out_date)) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                --where source_doc_id = '00189667'
                order by coalesce(ship_id, source_doc_id), date_dates, coalesce(pointcall_out_date, pointcall_in_date)
            ) as k where p.pkid = k.pkid 
*/
-- coalesce(p.outdate_fixed, p.indate_fixed),
-- , coalesce( pointcall_in_date, pointcall_out_date)
-- , coalesce( pointcall_out_date, pointcall_in_date)
-- coalesce(p.outdate_fixed, p.indate_fixed), source_doc_id , pointcall_function
select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id), 
			-- coalesce(p.outdate_fixed, p.indate_fixed), 
			case when source_suite = 'G5' then coalesce(p.outdate_fixed, p.indate_fixed) else  outdate_fixed end,
			source_doc_id, pointcall_rank_dedieu 
			) as pointcall_rankfull, 
			p.pkid, p.toponyme_fr , p.pointcall_action , p.pointcall_function , p.pointcall_rank_dedieu ,
                p.source_doc_id, p.source_text , p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                where 
                ship_id = '0014815N' -- '0014815N' '0000131N' -- '0000305N' '0000125N' '0007004N'
                --source_doc_id = '00189667' -- '00189667' '00189600' '00113390' '00114467' '00112650'
                order by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), source_doc_id , coalesce(pointcall_function, pointcall_rank_dedieu)
                -- case when source_suite = 'G5' then coalesce(p.outdate_fixed, p.indate_fixed) else  indate_fixed end,
                -- source_doc_id, pointcall_rank_dedieu 
                -- date_dates, coalesce(p.outdate_fixed, p.indate_fixed), source_doc_id, pointcall_rank_dedieu
                
                
select * from navigoviz.pointcall 
where source_doc_id  = '00184484'
order by ship_id, pointcall_rankfull
-- exemple de statut PC alors que date du genre FC et T sur PC. Et contradiction avec pointcall_rank_dedieu 

select distinct partner_balance_1789, pp3.partner_balance_supp_1789 
from ports.port_points pp3 
order by partner_balance_supp_1789, partner_balance_1789

select  partner_balance_1789, pp3.partner_balance_supp_1789 , *
from ports.port_points pp3 
where state_1789_fr = 'Grande-Bretagne' and partner_balance_1789 is null and new_janvier2022 

select id, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , destination_uhgs_id, pp2.amiraute,  duration , distance_dep_dest_miles , duration,
                (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
                pp.amiraute != pp2.amiraute as amirautes_differentes
                from navigoviz.uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
                where destination_navstatus like 'PC%' and destination_function = 'O' 
                and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
                and pp.uhgs_id = departure_uhgs_id
                and pp2.uhgs_id = destination_uhgs_id 
                and departure != destination 
                and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 

                
select source_suite , source_doc_id, record_id , p.toponyme_fr , p.pointcall_uhgs_id , p.pointcall_out_date , 
p.pointcall_action , p.pointcall_function , p.pointcall_in_date ,pointcall_rankfull, pointcall_rank_dedieu ,
p.ship_name , p.ship_id , p.ship_flag_standardized_fr , p.captain_id , p.captain_name , 
p.commodity_purpose
-- p.commodity_permanent_coding , p.cargo_item_action , p.quantity, p.quantity_u , p.tax_concept1 , p.taxe_amount01 
from navigoviz.pointcall p, 
             (
            
                select id, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , destination_uhgs_id, pp2.amiraute,  duration , distance_dep_dest_miles , duration,
                (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
                pp.amiraute != pp2.amiraute as amirautes_differentes
                from navigoviz.uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
                where destination_navstatus like 'PC%' and destination_function = 'O' 
                and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
                and pp.uhgs_id = departure_uhgs_id
                and pp2.uhgs_id = destination_uhgs_id 
                and departure != destination 
                and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
            
            ) as k
            
where k.doc_depart = p.source_doc_id and k.departure_uhgs_id = p.pointcall_uhgs_id 
and (k.departure_out_date = p.pointcall_out_date or k.departure_out_date is null)
order by source_suite, p.pointcall_rankfull

select pp.toponyme_standard_fr  , has_a_clerk, status from ports.port_points pp where pp.uhgs_id = 'A0140740'
select has_a_clerk from navigoviz.pointcall p where p.toponyme_fr = 'Meschers' and pointcall_function = 'O'

--- 

update navigoviz.pointcall set pointcall_uncertainity = -4 where (pointcall_uhgs_id = 'H4444444' or pointcall_uhgs_id = 'H9999999')
-- 355

update navigoviz.pointcall p set pointcall_uncertainity = -1 where pointcall_uncertainity<> -4;
-- ok : 108170

update navigoviz.pointcall p set pointcall_uncertainity = 0 
        where (((navigo_status like 'PC-RF' or navigo_status like 'PG-RF')   and pointcall_function like 'O') 
        or navigo_status like 'PC-RS' or navigo_status like 'PC-ML')
        and pointcall_uncertainity <> -4 and pointcall_uncertainity <> -2;
-- 67397

update navigoviz.pointcall p set pointcall_uncertainity = -3 where navigo_status like 'PU-%' 
        and pointcall_uncertainity <> -4
        
-- 8
       
update navigoviz.pointcall p set pointcall_uncertainity = -3 where navigo_status like 'PU-%' 
        and pointcall_uncertainity <> -4

update navigoviz.pointcall p set pointcall_uncertainity = 0 
where navigo_status like 'PC-RC' and pointcall_action = 'In-out'

-- Récupérer les cas louches de Z , pour les imprimer dans le fichier de debug
select STRING_AGG (quote_literal(ship_id), ','), STRING_AGG (quote_literal(doc_depart), ',')
from  (
		select id, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, 
                ship_id, ship_name, departure_out_date, destination_in_date, 
                pp.amiraute , destination , destination_uhgs_id, 
                pp2.amiraute,  duration , distance_dep_dest_miles , duration,
                (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
                pp.amiraute != pp2.amiraute as amirautes_differentes
                from navigoviz.uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
                where destination_navstatus like 'PC%' and destination_function = 'O' 
                and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
                and pp.uhgs_id = departure_uhgs_id
                and pp2.uhgs_id = destination_uhgs_id 
                and departure != destination 
                and pp.amiraute != pp2.amiraute
            	and 
            	-- departure_action = 'In' and destination_action = 'Out'
            	not (departure_action = 'Out' 
            	 	and destination_action in ('Out', 'In', 'In-out', 'In-Out', 'Sailing around', 'Transit') )
            	-- and destination_action != 'Transit'
            	and departure_action != 'Transit'
                and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
                ) as Zlouches 
 select tonnage_class from navigoviz.pointcall p2              
-- Décomptes des saisies
select count(*), source_suite, extract(year from coalesce(p.outdate_fixed, p.indate_fixed))
from pointcall p 
where p.data_block_leader_marker= 'A'
group by source_suite, extract(year from coalesce(p.outdate_fixed, p.indate_fixed))
-- analyse des calculs d'incertitude

-- Extraction d'un ensemble de cas de test intéressants. 

create table navigoviz.test as (

select pkid, ship_id , pointcall_rank_dedieu, p.pointcall_rankfull, record_id, source_suite, source_doc_id , pointcall_function,
pointcall,  p.pointcall_uhgs_id , p.pointcall_admiralty,  ship_name, tonnage, tonnage_unit, p.navigo_status , p.pointcall_out_date , p.pointcall_in_date , 
 pointcall_action ,  net_route_marker ,data_block_leader_marker, pointcall_uncertainity, 
 (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) as date_precise,
 list_sup_uhgs_id  ,
 (case when p.state_1789_fr = 'France' then 
 	(case when p.source_suite != 'G5' then 
 		true 
 	else 
 		(case when extract(year from coalesce(p.outdate_fixed, p.indate_fixed)) = 1787 then 
 			nb_conges_1787_inputdone is not null and p.nb_conges_1787_inputdone > 0 
 		else p.nb_conges_1789_inputdone is not null and p.nb_conges_1789_inputdone > 0 
 		end)
 	end)
 	else false
 	end) as has_input_done
from navigoviz.pointcall p left join 
(select ughs_id, array_agg(ughs_id_sup) as list_sup_uhgs_id 
from ports.generiques_inclusions_geo_csv gigc 
group by ughs_id) as k on k.ughs_id = p.pointcall_uhgs_id
where --source_doc_id = '00174446'
 -- p.ship_id = '0014650N' -- '0015709N' -- '0000447N' -- '0000305N' -- '0014815N' -- 0012800N
 -- p.source_doc_id in ('00354763', '00108710', '00332297', '00310958', '00189968', '00307963', '00352540', '00189667', '00189600', '00113390', '00114467', '00112650')
 -- or ship_id in ('0014650N', '0014815N', '0014911N', '0007352N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
      ship_id in ( '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
order by ship_id, pointcall_rankfull


)-- create tabke test
drop table navigoviz.test

select distinct source_doc_id from navigoviz.pointcall p
where source_doc_id in ( '00332297', '00310958', '00307963', '00352540')
and ship_id is null
and  ship_id not in ( '0014650N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')


-- Trouver les cas de Z différents de Silvia

select * from navigoviz.test t where date_precise is true and t.navigo_status not like 'PC%'

select  v.verif,  v.net_route_marker as algo_net_route_marker, t.net_route_marker as silvia_net_route_marker, v.certitude, v.certitude_reason , t.*
from navigoviz.test t left join public.verifs v on v.pkid = t.pkid
where t.ship_id = '0015074N'
order by ship_id, pointcall_rankfull 
-- inner join pour réduire la liste

drop table if exists navigoviz.test
select  tonnage::float, test_double_type(tonnage) from navigoviz.pointcall 
where tonnage is not null 
and test_double_type(tonnage) is false

select * from navigoviz.test where ship_id='0000389N'
select * from navigoviz.test where ship_id='0000386N'


select t.ship_id,t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,v.net_route_marker,t.net_route_marker as silvia_net_route_marker,t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.has_input_done,t.pointcall_admiralty,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
from navigoviz.test t left join public.verifs v on v.pkid = t.pkid
order by ship_id, pointcall_rankfull 


ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id 
create table navigoviz.log_uncertainity (
	id serial,
	pointcall_pkid int,
	source_doc_id text,
	record_id text,
	attributename text,
	code int,
	status text,
	reason text);

select distance_dep_dest_miles, distance_dep_dest_miles/ (case when duration=0 then 1 else duration end) as vitesse, * from uncertainity_travels ut where ship_id = '0021517N'
order by id 	


select distance_dep_dest_miles, distance_dep_dest_miles/ (case when duration=0 then 1 else duration end) as vitesse, * 
from uncertainity_travels ut where ut.doc_depart = '00340350'
order by id  


select source_doc_id, net_route_marker , ship_id from pointcall p 
where p.pointcall_function = 'O' and (position('=' in pointcall_out_date) > 0 or position('=' in pointcall_in_date) > 0)

select source_doc_id, net_route_marker , ship_id from pointcall p 
where p.net_route_marker != 'Z' and (navigo_status  like 'PU-%')

select source_doc_id, net_route_marker , ship_id from pointcall p 
where (navigo_status  like 'PU-%')

select * from built_travels ut where ship_id = '0021517N'
and source_entry != 'both-to'	



select duration, distance_dep_dest_miles / (case when duration=0 then 1 else duration end) as vitesse, * 
from uncertainity_travels ut 
where distance_dep_dest_miles / (case when duration=0 then 1 else duration end) > 250
and departure_uhgs_id not in ('A0390929', 'A0394917') 
and arrivee_net_route_marker != 'Z'
order by vitesse

select duration+1 as duration_jours, distance_dep_dest_miles , round(distance_dep_dest_miles / (duration+1)) as vitesse, 
p.tonnage , p.tonnage_unit , p.tonnage_class , ut.* 
from uncertainity_travels ut left join pointcall p on p.pkid = ut.departure_pkid 
where -- p.tonnage_class in ('[101-200]', '[201-500]')
  distance_dep_dest_miles / (duration+1) > 200
-- and departure_uhgs_id not in ('A0390929', 'A0394917') 
and arrivee_net_route_marker != 'Z' and depart_net_route_marker != 'Z' and 
position('=' in departure_out_date) > 0 and position('=' in destination_in_date) > 0 
and doc_depart = doc_destination 
order by vitesse




select source_suite, extract(year from coalesce(p.indate_fixed, p.outdate_fixed)), count(*) 
from navigoviz.pointcall p 
where p.source_suite != 'G5'
group by source_suite, extract(year from coalesce(p.indate_fixed, p.outdate_fixed))
order by source_suite, extract(year from coalesce(p.indate_fixed, p.outdate_fixed))



select pkid, ship_id , pointcall_rank_dedieu, p.pointcall_rankfull, record_id, source_suite, source_doc_id , pointcall_function,
            pointcall,  p.pointcall_uhgs_id , p.pointcall_admiralty,  ship_name, tonnage, tonnage_unit, p.navigo_status , p.pointcall_out_date , p.pointcall_in_date , 
            pointcall_action ,  net_route_marker ,data_block_leader_marker, pointcall_uncertainity, 
            (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) as date_precise,
            list_sup_uhgs_id  ,
            (case when p.state_1789_fr = 'France' then 
                (case when p.source_suite != 'G5' then 
                    true 
                else 
                    (case when extract(year from coalesce(p.outdate_fixed, p.indate_fixed)) = 1787 then 
                        nb_conges_1787_inputdone is not null and p.nb_conges_1787_inputdone > 0 
                    else p.nb_conges_1789_inputdone is not null and p.nb_conges_1789_inputdone > 0 
                    end)
                end)
                else false
                end) as has_input_done
            from navigoviz.pointcall p left join 

            (select ughs_id, array_agg(ughs_id_sup) as list_sup_uhgs_id 
            from ports.generiques_inclusions_geo_csv gigc 
            group by ughs_id) as k on k.ughs_id = p.pointcall_uhgs_id


            where p.ship_id = '0021616N' and net_route_marker != 'Q'-- p.source_doc_id = '00307963'
            -- p.ship_id = '0000068N' -- '0000305N' -- '0014815N' -- 0012800N
            -- p.source_doc_id in ('00108710', '00332297', '00310958', '00189968', '00307963', '00352540', '00189667', '00189600', '00113390', '00114467', '00112650')
            -- or ship_id in ('0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
            order by ship_id, pointcall_rankfull
            
select pointcall, pointcall_action, pointcall_out_date, pointcall_rank_dedieu , net_route_marker 
from navigoviz.pointcall p where source_doc_id in ('00310958', '00352540') order by pointcall_rankfull 

select * from ports.port_points pp where uhgs_id in ('A0203630', 'A0159051') or toponyme = 'Saint Germain' 

    select t.ship_id,t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as fixed_net_route_marker,t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.has_input_done,t.pointcall_admiralty,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from navigoviz.test t left join verifs v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull
    
select distinct ship_id from navigoviz.pointcall where pointcall_uhgs_id = 'A0204180' -- and pointcall='Dunkerque'


select distinct certitude from (
    select t.ship_id,t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as fixed_net_route_marker,t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.has_input_done,t.pointcall_admiralty,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from navigoviz.test t left join verifs v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull) as k
    
Observé
Infirmé
Confirmé
Déclaré

select  certitude, tonnage_uncertainity, count(*) 
from verifs v, navigoviz.pointcall p 
where p.pointcall = 'Dunkerque' and p.pkid = v.pkid 
group by certitude, tonnage_uncertainity 
order by   certitude, tonnage_uncertainity

select ship_homeport_uhgs_id , ship_homeport, ship_homeport_sort from navigo.pointcall p where p.record_id = '00110234'
-- Cootyes Plaat [Colijnsplaat]	Cootyes Plaat Colijnsplaat	A0635133

select * from port_points pp where pp.uhgs_id = 'A0635133'

select * from navigo.geo_general gg where gg.pointcall_uhgs_id = 'A0635133' 

select * from navigo.geo_general_complet gg where gg.pointcall_uhgs_id = 'A0635133' 
select * from navigo.geo_general_complet gg where gg.pointcall_name = 'Cootyes Plaat [Colijnsplaat]' 
select * from navigo.geo_general_complet gg where gg.pointcall_name = 'Cootyes Plaat Colijnsplaat' 
select * from navigo.geo_general_complet gg where gg.pointcall_name ilike '%Cootyes Plaat Colijnsplaat%' 
select * from navigo.geo_general_complet gg where gg.pointcall_name ilike '%Cootyes Plaat%' 


select count(*) from navigo.geo_general_complet
-- 6346507 points 223

select ship_id, pointcall, * from navigoviz.pointcall where ship_name = 'Marie Louise' order by ship_id, pointcall_rankfull  
-- 0004155N

select ship_id, ship_name, pointcall, * from navigoviz.pointcall where ship_name ilike '%Femme%' order by ship_id, pointcall_rankfull  
-- 0003951N

select ship_id, ship_name, pointcall, * from navigoviz.pointcall where ship_name ilike '%Jeune Nicolas%' order by ship_id, pointcall_rankfull  
-- 0002764N

select ship_id, ship_name, pointcall, * from navigoviz.pointcall where ship_name ilike '%Notre Dame des Carmes%' order by ship_id, pointcall_rankfull  
-- 0002764N
 

select ship_id, ship_name, pointcall, captain_name, tonnage, * from navigoviz.pointcall 
where ship_name ilike '%Sainte%Anne%'  and pointcall ilike 'Bordeaux'  order by ship_id, pointcall_rankfull  
-- and captain_name ilike '%Le%Cam%' 
-- 0008446N, 

select ship_id, ship_name, pointcall, captain_name, ship_tonnage, ship_homeport 
from navigo.pointcall where data_block_local_id = '00150364'



------------------------------------------
-- après la migration en postgres 13
------------------------------------------

-- test des fonctions plpython3u : ok


set search_path = 'ports', public;

create extension dblink ;

-- drop materialized view ports.myremote_geonamesplaces
create  materialized VIEW if not exists  ports.myremote_geonamesplaces AS
            SELECT *
            FROM dblink('dbname=geonames user=postgres password=mesange17 options=-csearch_path=',
                        'select geonameid::int, name, (feature_class||''.''||feature_code) as feature_code, alternatenames, country_code,  latitude::float, longitude::float , point3857 
                            from geonames.geonames_nov2019.allcountries a 
                            where feature_class||''.''||feature_code in (select code from geonames.geonames_nov2019.feature_code where keep = ''x'')  ')
            AS t1(geonameid int, name text, feature_code text, alternatenames text , country_code text, latitude float, longitude float, point3857 geometry) ; 

-- 5 milions de lignes, en 15 s

grant select on  ports.myremote_geonamesplaces to api_user
CREATE INDEX trgm_geoname_name_idx ON ports.myremote_geonamesplaces USING GIST (name gist_trgm_ops) --1 min 46
CREATE INDEX gist_geoname_point_idx ON ports.myremote_geonamesplaces USING GIST (point3857) -- 1 min 10

-- Que contient portic_v6_GIS_18janvier2022.sql (1 Go)

-- ports.generiques_inclusions_geo_csv : il faudra remplacer           
-- ports.gshhs_f_l1_3857           
-- ports.lau_europe
-- ports.limites_amirautes
-- ports.rivers_f
-- ports.world_1789 : : il faudra mettre aussi une autre version, qui est sur mon PC (juillet 2021)
-- ports.world_borders
-- ports.wup2018_f13_capital_cities_csv           


-- Que devra contenir portic_v7_GIS_22fevrier2022.sql (1 Go)
-- ports.generiques_inclusions_geo_csv
-- ports.port_points
-- ports.world_1789_mixte_30juillet2021_4326
-- ports.etats

----------------------------------------------------------------------------
-- Mise à jour des nouveaux homeports par import du fichier csv
-- 01-03-2022 Intégration fichier Excel des nouveaux homeports
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\Liste%20ports%20nouveaux%2030-01-2022%20Silvia%20en%20cours%2012-02-2022.xlsx
----------------------------------------------------------------------------
-- A0799311	Papembourg	Germany	Evêché de Münster		Papenbourg	Papenburg	Prince-Bishopric of Münster		je t'ai mis l'anglais car je ne pense pas qu'on ait cet Etat dans la base
-- A0839155	Nortbergen	Norway	Danemark	Norvège	Bergen	Bergen		douteux, à revoir
-- A0717276	Waberg							doublon ligne avant	
-- B0000960		United States	Etats-Unis	Caroline du Nord	Caroline du Nord	North Carolina		vide dans ton fichier
-- pas de nom


select count(*) from navigoviz.pointcall --109098
select count(*) from public.verifs --73223
select count(*) from navigoviz.test --86936


select count(*) from navigoviz.source -- 53110

----------------------------------------------------------------------------
-- Restauration des ports sur le server à partir d'une copie à jour de ma machine
----------------------------------------------------------------------------

create table ports.ports_points_copy10mars2022 as (select * from ports.port_points);
-- 1205
drop table ports.port_points_backup02mars2022 cascade;
drop table ports.port_points_backup21fev2022 cascade;
drop table ports.generiques_inclusions_geo_csv_backup  cascade;
--drop table ports.homeports_smogglage_csv  cascade;

drop table ports.port_points cascade;
drop table ports.etats cascade;
drop table ports.labels_lang_csv  cascade;
-- DROP cascade sur vue navigocheck.pointcall_cargo

select count(*) from ports.port_points -- 1433



--update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null
-- psycopg2.errors.DatetimeFieldOverflow: ERREUR:  valeur du champ date/time en dehors des limites : « 1787=17=04 »\n\n']

select * from navigoviz.pointcall where pointcall_in_date2='1787=17=04' 
select record_id , pointcall, pointcall_function, pointcall_in_date, pointcall_in_date2, indate_fixed, pointcall_out_date, pointcall_out_date2, outdate_fixed  
from navigoviz.pointcall where source_doc_id='00148881' and source_text='ANF, G5-62/9199'
select record_id ,pointcall_name, pointcall_function, pointcall_indate, pointcall_indate_date, pointcall_outdate , pointcall_outdate_date
from navigocheck.check_pointcall cp  where data_block_local_id='00148881' and source='ANF, G5-62/9199'


update    navigo.pointcall set  pointcall_indate =   '1787>04>17!'     where record_id in ('00154851');
update    navigocheck.check_pointcall set  pointcall_indate =   '1787>04>17!'  where record_id in ('00154851');
update    navigoviz.pointcall set  pointcall_in_date =   '1787>04>17!' , pointcall_in_date2='1787=04=17'    where record_id in ('00154851');

update navigoviz.pointcall set pointcall_in_date2='1787=04=17' where



---------------
-- Pb avec le code de vizsource sur le nom des suites
-- correction vite fait
---------------

select distinct s.suite from navigoviz.source s
/*
G5
Expéditions coloniales Marseille (1789)
la Santé registre de patentes de Marseille
Registre du petit cabotage (1786-1787)
*/

-- Retour de portic_v6 sur le serveur le 9 mars 2021
/*
Registre du petit cabotage (1786-1787)
Expéditions "coloniales" Marseille (1789)
Santé Marseille
G5
*/
update navigoviz.pointcall set source_suite='Expéditions "coloniales" Marseille (1789)' where source_suite='Expéditions coloniales Marseille (1789)';
update navigoviz.pointcall set source_suite='Santé Marseille' where source_suite='la Santé registre de patentes de Marseille';
-- fait sur le server, portic_v7 le 9 mars 2022

select distinct suite from navigoviz.source;

-------------------------------------------------------------
-- A/Z : reprise Analyse
-------------------------------------------------------------

select count(*) from public.verifs --73223
select * from navigoviz.test --86936

select * from public.verifs where record_id = '00351945'

select * from ports.generiques_inclusions_geo_csv
where ughs_id_sup = 'A0136930' or ughs_id = 'A0136930'
-- Correction suite aux retour de Silvia du 25 février sur l'algo sur Dunkerque.


update ports.generiques_inclusions_geo_csv set ughs_id='A0178037' , toponyme='Seudre'
where toponyme ='A0178037' and ughs_id='Seudre' and ughs_id_sup = 'A0136930';

select * from ports.generiques_inclusions_geo_csv where ughs_id not like 'A%' and ughs_id not like 'B%';
select * from ports.generiques_inclusions_geo_csv where ughs_id_sup not like 'A%' and ughs_id_sup not like 'B%';
-- rien d'autre à corriger


create table public.test2 as (
            select pkid, ship_id , pointcall_rank_dedieu, p.pointcall_rankfull, record_id, source_suite, source_doc_id , pointcall_function,
            pointcall,  p.pointcall_uhgs_id , p.pointcall_admiralty,  p.pointcall_province, ship_name, tonnage::float, tonnage_unit, p.navigo_status , p.pointcall_out_date , p.pointcall_in_date , 
            pointcall_action ,  net_route_marker ,data_block_leader_marker, pointcall_uncertainity, 
            (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) as date_precise,
            list_sup_uhgs_id  , list_inf_uhgs_id, date_fixed,
            (case when p.state_1789_fr = 'France' then 
                (case when p.source_suite != 'G5' then 
                    true 
                else 
                    (case when extract(year from coalesce(p.outdate_fixed, p.indate_fixed)) = 1787 then 
                        nb_conges_1787_inputdone is not null and p.nb_conges_1787_inputdone > 0 
                    else p.nb_conges_1789_inputdone is not null and p.nb_conges_1789_inputdone > 0 
                    end)
                end)
                else false
                end) as has_input_done
            from 
            navigoviz.pointcall p 
            left join 
            (select ughs_id, array_agg(ughs_id_sup) as list_sup_uhgs_id 
            from ports.generiques_inclusions_geo_csv gigc 
            group by ughs_id) as k on k.ughs_id = p.pointcall_uhgs_id
            left join 
            (select ughs_id_sup, array_agg(ughs_id) as list_inf_uhgs_id 
            from ports.generiques_inclusions_geo_csv gigc 
            group by ughs_id_sup) as k2 on k2.ughs_id_sup = p.pointcall_uhgs_id


            where 
            
            -- p.ship_id = '0014650N' --'0000447N' -- '0000305N' / -- '0014815N' -- 0012800N
            -- p.source_doc_id in ('00332297', '00310958', '00307963', '00352540') or
            -- ship_id in ( '0014650N', '0000011N', '0012406N', '0002344N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
            ship_id is not null and net_route_marker != 'Q' -- and ship_id >= '0000388N'
            and p.ship_id='0002537N'
            order by ship_id, pointcall_rankfull
            )

drop table test2
select * from test2
select * from verifs2

select pointcall_uhgs_id, pointcall_name, stages__length_miles, next_point__pointcall_name, next_point__pointcall_uhgs_id 
from navigocheck.check_stages
where ship_id='0002537N'
/* Manque Brouage	A0189364 --> Gravelines	A0189462 --> Bayonne	A0187995 */
select pointcall_uhgs_id, pointcall_name, stages__length_miles, next_point__pointcall_name, next_point__pointcall_uhgs_id 
from navigocheck.check_stages
where pointcall_uhgs_id='A0189462' and next_point__pointcall_uhgs_id='A0189364'
-- Absence dans les deux sens. 

select travel_id, outdate_fixed, departure_pkid, departure, departure_uhgs_id, distance_dep_dest_miles,
indate_fixed, destination_pkid , destination, destination_uhgs_id
from navigoviz.raw_flows where ship_id='0002537N'
order by travel_id

select * from ports.port_points where uhgs_id = 'A0187995' -- Bayonne n'a pas de source en 1789

------------------------------------------------------------------------------
-- travail perdu sur travels2
------------------------------------------------------------------------------

create table pointcall2 as (
        select t.ship_id,t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as fixed_net_route_marker,t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.has_input_done,t.pointcall_admiralty,t.pointcall_province,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from public.test2 t left join public.verifs2 v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull
    )

alter table pointcall2 add column pointcall_uncertainity int;
select distinct certitude, pointcall_uncertainity, verif from pointcall2
-- add uncertainty levels et -3 pour doute sur net_route_marker (verif)

update pointcall2 set pointcall_uncertainity = 0 where certitude='Observé';
update pointcall2 set pointcall_uncertainity = -1 where certitude='Confirmé';
update pointcall2 set pointcall_uncertainity = -2 where certitude='Déclaré';
update pointcall2 set pointcall_uncertainity = -3 where verif like '%#net_route_marker%';-- il y a débat entre algo et Silvia
update pointcall2 set pointcall_uncertainity = -4 where certitude is null;
update pointcall2 set pointcall_uncertainity = -5 where  certitude='Infirmé';

    -- 0 pour Observed
    -- -1 pour Confirmed
    -- -2 pour Declared
    -- -3 pour débat sur Net_Route_Marker
    -- -4 pour Missing / None / Null
    -- -5 pour Infirmed

select array_agg(distinct ''''||pointcall_uhgs_id||'''') from pointcall2    
    
create table public.travels2 as (
            select depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, 
            arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            
            depart.pointcall_action as departure_action, depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            arrivee.pointcall_out_date as arrivee_outdate,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id
            from navigoviz.pointcall d
            where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull) as depart, --ajout de d.source_doc_id à faire ou pas?
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, pointcall_out_date
            from navigoviz.pointcall d
            where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull) as arrivee -- suppression de d.source_doc_id entre ship_id et pointcall_rankfull
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id --supression de doc_depart_id <> doc_arrivee_id
            order by depart.ship_id, depart.id 
        )
        
-- create travels2 where net = A or uncertainity = -3
        
select * from public.travels2

select id, departure, destination, duration, 
departure_out_date, destination_in_date, departure_uncertainity, destination_uncertainity ,
st_makeline(pointpath) 
from public.travels2
where id = 6


-- ce qui a buggé : 5000 / 10000 (12 min dimanche et aujourd'hui + long et INACHEVES après correction du bug)
-- INACHEVE Test p1 SRID=3857;POINT(-492988.1506049512 6124779.7014998505) p2 SRID=3857;POINT(-454811.52998003527 6085626.1234356845) iter2 9
-- array_length(itineraire, 1) 718
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 10000, 50000)
where id = 7
-- OK : 10000 / 50000 en 9 secondes pour le 7 -- 51 pts / 11 pts après simplification
select array_length(pointpath, 1) from public.travels2 where id = 7

-- ce qui a bien marché : 20000 / 50000
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 20000, 50000)
where id = 6
-- array_length(itineraire, 1) 402 --> simpleifié à 20 pts : ok en 110 secondes
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 20000, 100000)
where id = 6
-- 332 pts INACHEVE --> 20 pts -- pas top
-- select array_length(pointpath, 1) from public.travels2 where id = 6
 
-- Pour tous dans un premier temps : bug sur 3, 11, et 21
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 20000, 50000)

update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 10000, 100000) where id = 3
-- test 20000 / 100000 : ok mais boucle
-- test 10000 / 100000 : parfait
-- test 15000 / 100 000 : inachevé
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 20000, 100000) where id = 11

update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 20000, 50000) where id = 21
-- 20 / 100 km : echec
-- 10 / 100 km : echec 
-- 20 / 150 km : echec
-- 5 / 100
-- 1 / 100
-- 0.5 / 100 
-- 10 / 50 km : echec, mais mieux. Croisic atteint


update public.travels2 set pointpath2 = ports.points_on_path(depart_pointcall_uhgs_id, arrivee_pointcall_uhgs_id, 20000, 50000) where id = 17

---- le 4 avril : pourquoi l'API sur le server ne retourne pas tous les ports ?

SELECT ogc_fid, uhgs_id, total, toponyme as  toponym, belonging_states, belonging_substates, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom, 900913)) as point
        FROM ports.port_points p, 
                (select pointcall_uhgs_id, count( *) as total
                from navigoviz.pointcall gg where true group by pointcall_uhgs_id) as k
                where p.toponyme is not null and p.uhgs_id = k.pointcall_uhgs_id
-- 1151 points
                
-- Correction
-- utilisation d'une jointure externe au lieu d'un inner join pour récupérer les homeports qui ne sont pas dans les pointcalls.
SELECT ogc_fid, uhgs_id, p.longitude as x, p.latitude as y, coalesce(total, 0) as total, toponyme as toponym,  p.toponyme_standard_fr, p.toponyme_standard_en , p.substate_1789_fr , p.state_1789_fr, p.substate_1789_en , p.state_1789_en, belonging_states, belonging_substates, 
	status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ferme_direction, ferme_bureau, partner_balance_1789, partner_balance_supp_1789 ,
	ST_AsGeoJSON(ST_Transform(geom, 900913)) as point
        FROM ports.port_points p left join 
                (select pointcall_uhgs_id, count( *) as total
                from navigoviz.pointcall gg where true group by pointcall_uhgs_id) as k on p.uhgs_id = k.pointcall_uhgs_id
                --where p.toponyme is not null 
                where p.geom is not null
-- 1384 si condition sur toponyme is not null / 1433 / 1426 avec les point non null
-- correction apportée à l'API le 4 avril. 
                
select '#'||toponyme_standard_fr||'#', * from ports.port_points where uhgs_id = 'A0404691'
update ports.port_points set toponyme_standard_fr = trim(toponyme_standard_fr), toponyme_standard_en = trim(toponyme_standard_en);
 
update ports.port_points set toponyme_standard_fr = 'Chatham', toponyme_standard_en = 'Chatham'  where uhgs_id = 'A0404691';

select '#'||fr||'#', '#'||en||'#' from ports.labels_lang_csv llc where llc.label_type = 'toponyme' and key_id = 'A0404691'
update ports.labels_lang_csv set  fr='Chatham', en='Chatham'    where label_type = 'toponyme' and key_id = 'A0404691' ;    

select '#'||toponyme_standard_fr||'#', * from ports.port_points where uhgs_id = 'A1945858';
update ports.port_points set toponyme_standard_fr = 'Aurigny', toponyme_standard_en = 'Alderney'  where uhgs_id = 'A1945858';
update ports.labels_lang_csv set  fr='Aurigny', en='Alderney'    where label_type = 'toponyme' and key_id = 'A1945858' ;    

select '#'||toponyme_standard_fr||'#', * from ports.port_points where uhgs_id = 'A0405054';
update ports.port_points set toponyme_standard_fr = 'Shoreham', toponyme_standard_en = 'Shoreham'  where uhgs_id = 'A0405054';
update ports.labels_lang_csv set  fr='Shoreham', en='Shoreham'    where label_type = 'toponyme' and key_id = 'A0405054' ;    

select '#'||toponyme_standard_fr||'#', * from ports.port_points where uhgs_id = 'A0599268';


------ la peche de Dunkerque
-- demain : filtrer les départs pour la pêche (terre-neuve, island) + commodity_purpose ~ pêche
-- A1964976 Ile feroe : non
-- A0146289 Islande : oui
-- B0000715 Grands Bancs de Terre-Neuve : oui

select rf.ship_id, ship_name, rf.destination_uhgs_id,destination_fr, rf.destination_substate_1789_fr , rf.destination_state_1789_fr, rf.tonnage , rf.outdate_fixed , 
rf.commodity_purpose, rf.commodity_purpose2 , rf.commodity_purpose3 , rf.commodity_purpose4 , 
rf.all_cargos , jsonb_array_length(rf.all_cargos)
from navigoviz.raw_flows rf 
where rf.departure_uhgs_id = 'A0204180' and extract(year from rf.outdate_fixed) = 1789
and rf.destination_partner_balance_supp_1789  in ('Etranger')  and rf.destination_state_1789_fr not in ('Grande-Bretagne') and rf.destination_uhgs_id not in ('C0000009', 'A0146289', 'B0000715')
and (commodity_purpose  not like '%Lège%' ) --and (commodity_purpose2  not like '%Lest%' )
and   (commodity_purpose is not null and commodity_purpose not like '%pêche%') 
--and   (commodity_purpose2 is not null and commodity_purpose2 not like '%pêche%') 
-- or commodity_purpose2  like '%pêche%' or commodity_purpose3  like '%pêche%' or commodity_purpose4 like '%pêche%')
--and  rf.destination_uhgs_id = 'B0000715'
-- 61 à la pêche


alter table navigoviz.raw_flows add column nbproduits int default 0;
update navigoviz.raw_flows set nbproduits  = jsonb_array_length(all_cargos);
alter table navigoviz.raw_flows rename column nbproduits to nb_cargo;


alter table navigoviz.built_travels  add column nbproduits int default 0;
update navigoviz.built_travels set nbproduits  = jsonb_array_length(all_cargos);
alter table navigoviz.built_travels rename column nbproduits to nb_cargo;

comment on column navigoviz.raw_flows.nb_cargo is 'Number of products that are carried by the ship';
comment on column navigoviz.raw_flows.nb_cargo is 'Number of products that are carried by the ship';


select * from navigoviz.built_travels

departure_uhgs_id,destination_uhgs_id,destination_fr,destination_substate_1789_fr,destination_state_1789_fr,tonnage,outdate_fixed,commodity_purpose,commodity_purpose2,commodity_purpose3,commodity_purpose3,commodity_purpose4,all_cargos,nbproduits
-- http://data.portic.fr/api/rawflows/?format=csv&date=1789&params=departure_uhgs_id,departure_fr,destination_uhgs_id,destination_fr,destination_partner_balance_supp_1789,destination_substate_1789_fr,destination_state_1789_fr,tonnage,outdate_fixed,commodity_purpose,commodity_purpose2,commodity_purpose3,commodity_purpose4,all_cargos,nb_cargo

select * from ports.labels_lang_csv where label_type = 'commodity' and fr like '%uf';
-- Peaux de bœuf

update ports.labels_lang_csv set  fr='Peaux de boeuf', en='Skin (oxen)'    where label_type = 'commodity' and key_id = '00001279' ;    
update navigoviz.pointcall set  commodity_purpose= REPLACE(commodity_purpose, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized_fr=REPLACE(commodity_standardized_fr, 'Peaux de bœuf', 'Peaux de boeuf' ), all_cargos=REPLACE(all_cargos::text, 'Peaux de bœuf', 'Peaux de boeuf' )::jsonb 
select all_cargos::text, REPLACE(all_cargos::text, 'Peaux de bœuf', 'Peaux de boeuf' )::jsonb from navigoviz.pointcall where commodity_purpose like '%uf';
-- 'Peaux de boeuf', en='Skin (oxen)'    where label_type = 'commodity' and key_id = '00001279' ;    
-- [{"quantity": null, "quantity_u": null, "commodity_id": "00001279", "cargo_item_action": "Out", "commodity_purpose": "peaux de boeuf", "link_to_pointcall": "00149956", "commodity_standardized_en": "Skin (oxen)", "commodity_standardized_fr": "Peaux de bœuf", "commodity_permanent_coding": null}]
-- [{"quantity": null, "quantity_u": null, "commodity_id": "00000287", "cargo_item_action": "Out", "commodity_purpose": "Boeuf", "link_to_pointcall": "00174061", "commodity_standardized_en": null, "commodity_standardized_fr": null, "commodity_permanent_coding": "ID-NBxxxx-JF-HBCxxx-FBxx"}, {"quantity": null, "quantity_u": null, "commodity_id": "00000013", "cargo_item_action": "Out", "commodity_purpose": "Autres", "link_to_pointcall": "00174061", "commodity_standardized_en": "Sundries", "commodity_standardized_fr": "Diverses marchandises", "commodity_permanent_coding": "xx-xxxxxx-JZ-MAxxxx-xxxx"}, {"quantity": null, "quantity_u": null, "commodity_id": "00000124", "cargo_item_action": "Out", "commodity_purpose": "Farine", "link_to_pointcall": "00174061", "commodity_standardized_en": "Flour", "commodity_standardized_fr": "Farine", "commodity_permanent_coding": "ID-BDxxxx-JF-AAAxxx-FBxx"}, {"quantity": null, "quantity_u": null, "commodity_id": "00000043", "cargo_item_action": "Out", "commodity_purpose": "Vin", "link_to_pointcall": "00174061", "commodity_standardized_en": "Wine", "commodity_standardized_fr": "Vin", "commodity_permanent_coding": "ID-FFBxxx-JF-FAAAxx-FBxx"}]
-- [{"quantity": "1.0", "quantity_u": "partie", "commodity_id": null, "cargo_item_action": "In", "commodity_purpose": "peaux de beuf", "link_to_pointcall": "00329242", "commodity_standardized_en": null, "commodity_standardized_fr": null, "commodity_permanent_coding": null}]
-- [{"quantity": "1.0", "quantity_u": "partie", "commodity_id": "00001279", "cargo_item_action": "In", "commodity_purpose": "peaux de boeuf", "link_to_pointcall": "00329244", "commodity_standardized_en": "Skin (oxen)", "commodity_standardized_fr": "Peaux de boeuf", "commodity_permanent_coding": null}, {"quantity": null, "quantity_u": null, "commodity_id": "00000109", "cargo_item_action": "In", "commodity_purpose": "fideaux", "link_to_pointcall": "00329244", "commodity_standardized_en": null, "commodity_standardized_fr": null, "commodity_permanent_coding": "ID-BDAxxx-JF-BDxxxx-FBxx"}]
update navigoviz.pointcall set  commodity_purpose2= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized2_fr=REPLACE(commodity_standardized2_fr, 'Peaux de bœuf', 'Peaux de boeuf' )
, commodity_purpose3= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized3_fr=REPLACE(commodity_standardized3_fr, 'Peaux de bœuf', 'Peaux de boeuf' )
, commodity_purpose4= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized4_fr=REPLACE(commodity_standardized4_fr, 'Peaux de bœuf', 'Peaux de boeuf' )


update navigoviz.built_travels set  commodity_purpose= REPLACE(commodity_purpose, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized_fr=REPLACE(commodity_standardized_fr, 'Peaux de bœuf', 'Peaux de boeuf' ), all_cargos=REPLACE(all_cargos::text, 'Peaux de bœuf', 'Peaux de boeuf' )::jsonb ;
update navigoviz.built_travels set  commodity_purpose2= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized2_fr=REPLACE(commodity_standardized2_fr, 'Peaux de bœuf', 'Peaux de boeuf' )
, commodity_purpose3= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized3_fr=REPLACE(commodity_standardized3_fr, 'Peaux de bœuf', 'Peaux de boeuf' )
, commodity_purpose4= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized4_fr=REPLACE(commodity_standardized4_fr, 'Peaux de bœuf', 'Peaux de boeuf' )

update navigoviz.raw_flows set  commodity_purpose= REPLACE(commodity_purpose, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized_fr=REPLACE(commodity_standardized_fr, 'Peaux de bœuf', 'Peaux de boeuf' ), all_cargos=REPLACE(all_cargos::text, 'Peaux de bœuf', 'Peaux de boeuf' )::jsonb ;
update navigoviz.raw_flows set  commodity_purpose2= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized2_fr=REPLACE(commodity_standardized2_fr, 'Peaux de bœuf', 'Peaux de boeuf' )
, commodity_purpose3= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized3_fr=REPLACE(commodity_standardized3_fr, 'Peaux de bœuf', 'Peaux de boeuf' )
, commodity_purpose4= REPLACE(commodity_purpose2, 'Peaux de bœuf', 'Peaux de boeuf' ), commodity_standardized4_fr=REPLACE(commodity_standardized4_fr, 'Peaux de bœuf', 'Peaux de boeuf' )

---------------------------------
-- Correction apportée sur le serveur le 7 avril
---------------------------------

select record_id, pointcall, cargo_item_action, cargo_item_action2, cargo_item_action3, cargo_item_action4, commodity_purpose, commodity_purpose2, commodity_purpose3, commodity_purpose4 from navigoviz.pointcall where source_doc_id= '00334176'
-- records 00344077 et 00334176 (de Dunkerque)

-- correction de pointcall sur le server le 7 avril 2022
update navigoviz.pointcall set commodity_purpose2=(all_cargos->>1)::json->>'commodity_purpose',
        commodity_id2=(all_cargos->>1)::json->>'commodity_id',
        quantity2=(all_cargos->>1)::json->>'quantity',
        quantity_u2=(all_cargos->>1)::json->>'quantity_u',
        commodity_standardized2=(all_cargos->>1)::json->>'commodity_standardized_en',
        commodity_standardized2_fr=(all_cargos->>1)::json->>'commodity_standardized_fr',
        commodity_permanent_coding2=(all_cargos->>1)::json->>'commodity_permanent_coding',
        cargo_item_action2=(all_cargos->>1)::json->>'cargo_item_action';
        
update navigoviz.pointcall set commodity_purpose3=(all_cargos->>2)::json->>'commodity_purpose',
        commodity_id3=(all_cargos->>2)::json->>'commodity_id',
        quantity3=(all_cargos->>2)::json->>'quantity',
        quantity_u3=(all_cargos->>2)::json->>'quantity_u',
        commodity_standardized3=(all_cargos->>2)::json->>'commodity_standardized_en',
        commodity_standardized3_fr=(all_cargos->>2)::json->>'commodity_standardized_fr',
        commodity_permanent_coding3=(all_cargos->>2)::json->>'commodity_permanent_coding',
        cargo_item_action3=(all_cargos->>2)::json->>'cargo_item_action';

update navigoviz.pointcall set commodity_purpose4=(all_cargos->>3)::json->>'commodity_purpose',
        commodity_id4=(all_cargos->>3)::json->>'commodity_id',
        quantity4=(all_cargos->>3)::json->>'quantity',
        quantity_u4=(all_cargos->>3)::json->>'quantity_u',
        commodity_standardized4=(all_cargos->>3)::json->>'commodity_standardized_en',
        commodity_standardized4_fr=(all_cargos->>3)::json->>'commodity_standardized_fr',
        commodity_permanent_coding4=(all_cargos->>3)::json->>'commodity_permanent_coding',
        cargo_item_action4=(all_cargos->>3)::json->>'cargo_item_action';

-- correction de raw_flows sur le server le 7 avril 2022
update navigoviz.raw_flows  set commodity_purpose2=(all_cargos->>1)::json->>'commodity_purpose',
        commodity_id2=(all_cargos->>1)::json->>'commodity_id',
        quantity2=(all_cargos->>1)::json->>'quantity',
        quantity_u2=(all_cargos->>1)::json->>'quantity_u',
        commodity_standardized2=(all_cargos->>1)::json->>'commodity_standardized_en',
        commodity_standardized2_fr=(all_cargos->>1)::json->>'commodity_standardized_fr',
        commodity_permanent_coding2=(all_cargos->>1)::json->>'commodity_permanent_coding',
        cargo_item_action2=(all_cargos->>1)::json->>'cargo_item_action';
        
update navigoviz.raw_flows set commodity_purpose3=(all_cargos->>2)::json->>'commodity_purpose',
        commodity_id3=(all_cargos->>2)::json->>'commodity_id',
        quantity3=(all_cargos->>2)::json->>'quantity',
        quantity_u3=(all_cargos->>2)::json->>'quantity_u',
        commodity_standardized3=(all_cargos->>2)::json->>'commodity_standardized_en',
        commodity_standardized3_fr=(all_cargos->>2)::json->>'commodity_standardized_fr',
        commodity_permanent_coding3=(all_cargos->>2)::json->>'commodity_permanent_coding',
        cargo_item_action3=(all_cargos->>2)::json->>'cargo_item_action';

update navigoviz.raw_flows set commodity_purpose4=(all_cargos->>3)::json->>'commodity_purpose',
        commodity_id4=(all_cargos->>3)::json->>'commodity_id',
        quantity4=(all_cargos->>3)::json->>'quantity',
        quantity_u4=(all_cargos->>3)::json->>'quantity_u',
        commodity_standardized4=(all_cargos->>3)::json->>'commodity_standardized_en',
        commodity_standardized4_fr=(all_cargos->>3)::json->>'commodity_standardized_fr',
        commodity_permanent_coding4=(all_cargos->>3)::json->>'commodity_permanent_coding',
        cargo_item_action4=(all_cargos->>3)::json->>'cargo_item_action';
 
-- correction de built_travels sur le server le 7 avril 2022
update navigoviz.built_travels  set commodity_purpose2=(all_cargos->>1)::json->>'commodity_purpose',
        commodity_id2=(all_cargos->>1)::json->>'commodity_id',
        quantity2=(all_cargos->>1)::json->>'quantity',
        quantity_u2=(all_cargos->>1)::json->>'quantity_u',
        commodity_standardized2=(all_cargos->>1)::json->>'commodity_standardized_en',
        commodity_standardized2_fr=(all_cargos->>1)::json->>'commodity_standardized_fr',
        commodity_permanent_coding2=(all_cargos->>1)::json->>'commodity_permanent_coding',
        cargo_item_action2=(all_cargos->>1)::json->>'cargo_item_action';
        
update navigoviz.built_travels set commodity_purpose3=(all_cargos->>2)::json->>'commodity_purpose',
        commodity_id3=(all_cargos->>2)::json->>'commodity_id',
        quantity3=(all_cargos->>2)::json->>'quantity',
        quantity_u3=(all_cargos->>2)::json->>'quantity_u',
        commodity_standardized3=(all_cargos->>2)::json->>'commodity_standardized_en',
        commodity_standardized3_fr=(all_cargos->>2)::json->>'commodity_standardized_fr',
        commodity_permanent_coding3=(all_cargos->>2)::json->>'commodity_permanent_coding',
        cargo_item_action3=(all_cargos->>2)::json->>'cargo_item_action';

update navigoviz.built_travels set commodity_purpose4=(all_cargos->>3)::json->>'commodity_purpose',
        commodity_id4=(all_cargos->>3)::json->>'commodity_id',
        quantity4=(all_cargos->>3)::json->>'quantity',
        quantity_u4=(all_cargos->>3)::json->>'quantity_u',
        commodity_standardized4=(all_cargos->>3)::json->>'commodity_standardized_en',
        commodity_standardized4_fr=(all_cargos->>3)::json->>'commodity_standardized_fr',
        commodity_permanent_coding4=(all_cargos->>3)::json->>'commodity_permanent_coding',
        cargo_item_action4=(all_cargos->>3)::json->>'cargo_item_action';
        
select * from navigocheck.pointcall_cargo         where link_to_pointcall = '00334176';
-------------------------------

select *  from ports.port_points where toponyme = 'Dunkerque'

select distinct source_suite, source_subset from navigoviz.raw_flows rf 
#Bureau de ferme de la Rochelle
select distinct source_suite, source_subset, departure_ferme_bureau, departure_action from navigoviz.raw_flows rf 
where departure_ferme_bureau='Marseille' and 

-- 237 lignes
select r.source_suite, r.destination_pkid , p.net_route_marker  ,destination_fr, destination_partner_balance_1789,  
destination_partner_balance_supp_1789, destination_substate_1789_fr, destination_state_1789_fr,
r.tonnage, r.outdate_fixed, extract(year from r.outdate_fixed) as year, r.nbproduits as nb_cargo
from navigoviz.raw_flows r, navigoviz.pointcall p  
where departure_ferme_bureau='Marseille' and destination_partner_balance_supp_1789='Etranger'
and p.pkid = r.destination_pkid 
order by r.source_suite, p.net_route_marker 
-- Frioul'departure_fr',
       'destination_uhgs_id', '',
       'destination_partner_balance_1789',
       'destination_partner_balance_supp_1789', 'destination_substate_1789_fr',
       'destination_state_1789_fr', 'tonnage', 'outdate_fixed', 'nb_cargo'
-- Marseille
---------------------------------------------------------------------------------------------

-------------------------------
-- les noms des sources à Marseille
-------------------------------

select distinct source_suite from navigoviz.pointcall
/*
 * static tabRealSourcesNames=[
        { abbrev: "g5", trueName: "G5" },
        { abbrev: "marseille", trueName: "Santé Marseille"},
        { abbrev: "cabotage", trueName: "Registre du petit cabotage (1786-1787)"},
        { abbrev: "expeditions", trueName: "Expéditions \"coloniales\" Marseille (1789)"}
    ];
 */

/*      OK sur ma machine 
Registre du petit cabotage (1786-1787)
Expéditions "coloniales" Marseille (1789)
Santé Marseille
G5
*/
select distinct source_suite from navigoviz.raw_flows

/* erreur sur le serveur
G5
Expéditions coloniales Marseille (1789)
la Santé registre de patentes de Marseille
Registre du petit cabotage (1786-1787)
*/

select distinct source_suite from navigoviz.built_travels

update navigoviz.pointcall set source_suite = 'Expéditions "coloniales" Marseille (1789)' where source_suite='Expéditions coloniales Marseille (1789)';-- 247
update navigoviz.raw_flows set source_suite = 'Expéditions "coloniales" Marseille (1789)' where source_suite='Expéditions coloniales Marseille (1789)' ;--128
update navigoviz.built_travels set source_suite = 'Expéditions "coloniales" Marseille (1789)' where source_suite='Expéditions coloniales Marseille (1789)'; --128

update navigoviz.pointcall set source_suite = 'Santé Marseille' where source_suite = 'la Santé registre de patentes de Marseille';-- 16592
update navigoviz.raw_flows set source_suite = 'Santé Marseille' where source_suite = 'la Santé registre de patentes de Marseille';--356
update navigoviz.built_travels set source_suite = 'Santé Marseille' where source_suite = 'la Santé registre de patentes de Marseille';--17407


-----------------------------------------------------------------
-- API pour Bernard Pradines
-----------------------------------------------------------------

set search_path = navigoviz, ports, public;

select count(*) from built_travels where source_entry != 'both-to';
-- 63204 segments

select distinct p.captain_id , p.captain_name , p.captain_uncertainity , 
p.birthplace , p.birthplace_uhgs_id , p.birthplace_uncertainity ,  p.birthplace_uhgs_id_uncertainity ,
p.citizenship , p.citizenship_uncertainity 
-- ATTENTION, à ajouter dans travels : captain_citizenship_id
--captain_last_name, captain_age, captain_status,   
--captain_citizenship, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id 
from  navigoviz.built_travels p
where source_entry != 'both-to'
order by captain_id
-- 21994

--- REFAIRE ICI 
drop table navigoviz.captain;

CREATE TABLE navigoviz.captain (
		captain_id text,
		occurences_names json,
		occurences_birthplaces json,
		occurences_citizenships json
	);

INSERT INTO navigoviz.captain(captain_id)
	SELECT DISTINCT captain_id FROM navigoviz.built_travels;
--- 12340 

delete from navigoviz.captain where captain_id is null;


update navigoviz.captain c set occurences_names = k.occurences_name
FROM
(select captain_local_id, array_to_json(array_agg(row_to_json(row(captain_name ,occurences)))) as occurences_name
	FROM
	(
	Select captain_id as captain_local_id, captain_name, count (captain_name) as occurences
	FROM navigoviz.built_travels
	where source_entry != 'both-to'
	GROUP BY captain_name,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 12285 BIZARRE il y a des capitain renseignés dans les destinations both-to

select * from navigoviz.captain where occurences_names is null;

update navigoviz.captain c set occurences_birthplaces = k.occurence_birthplace
FROM
(select captain_local_id, array_to_json(array_agg(row_to_json(row(birthplace ,occurences)))) as occurence_birthplace
FROM
(
Select captain_id as captain_local_id, birthplace, count (birthplace) as occurences
FROM navigoviz.built_travels
  where source_entry != 'both-to'
	GROUP BY birthplace,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 2316 / 2299  BIZARRE il y a des capitain rensignés dans les destinations both-to



update navigoviz.captain c set occurences_citizenships = k.occurences_citizenship
FROM
(select captain_local_id, array_to_json(array_agg(row_to_json(row(citizenship ,occurences)))) as occurences_citizenship
FROM
(
Select captain_id as captain_local_id, citizenship, count (citizenship) as occurences
FROM navigoviz.built_travels
   where source_entry != 'both-to'
	GROUP BY citizenship,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 1082 / 1081


select * from navigoviz.captain where captain_id in ('00002765', '00002763', '00002472', '00014569', '00011637') ;
-- 00002763	[{"f1":"Taglierani, Jean Antoine","f2":4}]		[{"f1":"Ragusois","f2":4}]
-- 00002472	[{"f1":"Perron, Joseph","f2":9},{"f1":"Person, Joseph","f2":7},{"f1":"Le Person, Joseph","f2":6},{"f1":"Le Perron, Joseph","f2":4},{"f1":"Porson, Joseph","f2":3}]	[{"f1":"Ile aux moines","f2":7}]	
-- 00002765	[{"f1":"Cazabon, Jean","f2":3},{"f1":"Casaubon, Jean","f2":1}]		
-- 00014569	[{"f1":"Accamo, Jean Baptiste","f2":42},{"f1":"Accame, Jean Baptiste","f2":6},{"f1":"Accamo,Jean Baptiste","f2":4},{"f1":"Accamo, Jean BAptiste","f2":2}]		[{"f1":"Génois","f2":41},{"f1":"Genois","f2":13}]
-- 00011637	[{"f1":"Rouilla, François","f2":7},{"f1":"Rouilla, Fancois","f2":4},{"f1":"Rolla, François","f2":3}]	[{"f1":"Tossa","f2":11},{"f1":"Tosse [Tossa]","f2":3}]	[{"f1":"Catalan","f2":10},{"f1":"Catalogne","f2":4}]

select captain_name, * from navigoviz.pointcall where captain_id  = '00002472'
order by captain_name
--- Pour les navires
drop table navigoviz.ship;

CREATE TABLE navigoviz.ship(
	ship_id text,
	occurences_names json,
	occurences_homeports_fr json,--use standardized homeports fr et en; should be only ONE ?
	occurences_homeports_en json,--use standardized homeports fr et en; should be only ONE ?
	occurences_flags_fr json,-- use standardized flag fr et en; should be only ONE ?
	occurences_flags_en json,-- use standardized flag fr et en; should be only ONE ?
	occurences_class json,
	occurences_tonnageclass json
);

INSERT INTO navigoviz.ship(ship_id,occurences_names)
select ship_id, array_to_json(array_agg(row_to_json(row(ship_name ,occurences)))) as occurences_name
FROM
(
Select ship_id, ship_name, count (ship_name) as occurences
FROM navigoviz.built_travels
	where source_entry != 'both-to'
	GROUP BY ship_name,ship_id
	ORDER BY ship_id, occurences DESC
) as k
GROUP BY ship_id;
-- 11618

update navigoviz.ship s set occurences_names = k.occurences_name
FROM
(select ship_id, array_to_json(array_agg(row_to_json(row(ship_name ,occurences)))) as occurences_name
	from (
		Select ship_id, ship_name, count (ship_name) as occurences
		FROM navigoviz.built_travels
			where source_entry != 'both-to'
			GROUP BY ship_name,ship_id
			ORDER BY ship_id, occurences DESC
		) as k
	GROUP BY ship_id
) as k
WHERE s.ship_id = k.ship_id;

select * from navigoviz.built_travels where ship_name is null ;

update navigoviz.ship s set occurences_flags_fr = k.flags
FROM
(SELECT t.ship_id, array_to_json(array_agg(row_to_json(row(t.ship_flag ,t.occurences)))) as flags
	FROM(
		Select ship_id,
		ship_flag_standardized_fr as ship_flag, count (ship_flag_standardized_fr) as occurences
		FROM navigoviz.built_travels t
		where source_entry != 'both-to'
		GROUP BY ship_id, t.ship_flag_standardized_fr
		ORDER BY ship_id, occurences DESC
		) t
	WHERE occurences > 0
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11104

update navigoviz.ship s set occurences_flags_en = k.flags
FROM
(SELECT t.ship_id, array_to_json(array_agg(row_to_json(row(t.ship_flag ,t.occurences)))) as flags
	FROM(
		Select ship_id,
		ship_flag_standardized_en as ship_flag, count (ship_flag_standardized_en) as occurences
		FROM navigoviz.built_travels t
		where source_entry != 'both-to'
		GROUP BY ship_id, t.ship_flag_standardized_en
		ORDER BY ship_id, occurences DESC
		) t
	WHERE occurences > 0
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11104

-- Requête JSON à faire : existe-t-il des cas où le flag n'est pas consistent pour le même navire ?
-- OUI : 26 cas sur 11104
-- taille du tableau ? 
select distinct json_array_length (occurences_flags_fr) from navigoviz.ship
select * from navigoviz.ship where  json_array_length (occurences_flags_fr) > 1
select * from navigoviz.ship where  json_array_length (occurences_flags_fr) > 1
select (occurences_flags_fr)::json->0->>'f1' from navigoviz.ship where occurences_flags_fr is not null
select (occurences_flags_fr)::json->0->>'f1' from navigoviz.ship where occurences_flags_fr is not null and (occurences_flags_fr)::json->0->>'f1' = ''


 

update navigoviz.ship s set occurences_homeports_fr = k.homeports
FROM
(SELECT t.ship_id, array_to_json(array_agg(row_to_json(row(t.ship_homeport ,t.occurences)))) as homeports
	FROM(
		Select ship_id,
		homeport_toponyme_fr as ship_homeport, count (homeport_toponyme_fr) as occurences
		FROM navigoviz.built_travels 
		where source_entry != 'both-to'
		GROUP BY ship_id, homeport_toponyme_fr
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
--- 7797

update navigoviz.ship s set occurences_homeports_en = k.homeports
FROM
(SELECT t.ship_id, array_to_json(array_agg(row_to_json(row(t.ship_homeport ,t.occurences)))) as homeports
	FROM(
		Select ship_id,
		homeport_toponyme_en as ship_homeport, count (homeport_toponyme_en) as occurences
		FROM navigoviz.built_travels 
		where source_entry != 'both-to'
		GROUP BY ship_id, homeport_toponyme_en
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
--- 7797

update navigoviz.ship s set occurences_class = null

update navigoviz.ship s set occurences_class = k.class
FROM
(SELECT t.ship_id, array_to_json(array_agg(row_to_json(row(t.ship_class ,t.occurences)))) as class
	FROM(
		Select ship_id,
		COALESCE(class,'') as ship_class, count (class) as occurences
		FROM navigoviz.built_travels 
		where source_entry != 'both-to'
		GROUP BY ship_id, class
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 4422

select * from navigoviz.ship s

update navigoviz.ship s set occurences_tonnageclass = k.tonnageclass
FROM
(SELECT t.ship_id, array_to_json(array_agg(row_to_json(row(t.ship_tonnage ,t.occurences)))) as tonnageclass
	FROM(
		Select ship_id,
		COALESCE(tonnage_class,'') as ship_tonnage, count (tonnage_class) as occurences
		FROM navigoviz.built_travels 
		where source_entry != 'both-to'
		GROUP BY ship_id, tonnage_class
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;	
-- 9047

select * from navigoviz.ship where  json_array_length (occurences_tonnageclass) > 1 --622

select * from navigoviz.ship where  json_array_length (occurences_tonnageclass) > 1 --622
select (occurences_tonnageclass)::json->0->>'f1' from navigoviz.ship where occurences_tonnageclass is not null

select travel_id, source_doc_id, outdate_fixed, indate_fixed, departure_uhgs_id, departure_fr, destination_uhgs_id, destination_fr,
ship_id, ship_name, tonnage_class, ship_flag_standardized_fr, homeport_toponyme_fr, 
captain_id , captain_name, birthplace, citizenship, travel_rank, nbproduits
from navigoviz.built_travels
where ship_id = '0013102N' and source_entry!='both-to'
order by travel_rank;
-- and captain_id=''

-- Renvoyer les variantes de noms de navires ou de capitaines
SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_tonnage ,t.occurences)))->0
-- , array_agg(row_to_json(row(t.ship_tonnage ,t.occurences))) as tonnageclass
	FROM(
		Select ship_id,
		COALESCE(tonnage_class,'') as ship_tonnage, count (tonnage_class) as occurences
		FROM navigoviz.built_travels 
		where source_entry != 'both-to'
		GROUP BY ship_id, tonnage_class
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0
	 GROUP BY t.ship_id

--------------------------------------------------------------------------
--------------------------------------------------------------------
-- Je refais avec ma variante
-- sauver le navigoviz.ship comme un backup
-- sauver le navigoviz.captain comme un backup
	 
SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.homeport_toponyme_fr ,t.occurences)))
-- ->0
-- , array_agg(row_to_json(row(t.ship_tonnage ,t.occurences))) as tonnageclass
	FROM(
		Select ship_id,
		COALESCE(homeport_toponyme_fr,'') as homeport_toponyme_fr, count (homeport_toponyme_fr) as occurences
		FROM navigoviz.built_travels 
		where source_entry != 'both-to'
		GROUP BY ship_id, homeport_toponyme_fr
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0 and ship_id = '0014902N'
	 GROUP BY t.ship_id

------------------------	 
	 select count(*) from public.test_pointcall -- 366 / 87307
	 select count(*) from navigoviz.pointcall p where ship_id is not null and net_route_marker != 'Q' --109096
	 
	 select count(*) from  public.verifs_pointcall --72929
	 select count(*) from public.pointcall_checked --366 / 87307
	 
	 select net_route_marker, fixed_net_route_marker, * from navigoviz.pointcall where net_route_marker != fixed_net_route_marker
	 -- 4450 points différents
	 select distinct certitude from navigoviz.pointcall
	 select array_agg(distinct certitude_reason) from navigoviz.pointcall

	 select travel_rank, style_dashed , 
	 travel_uncertainity , departure_uncertainity , destination_uncertainity , uncertainty_color ,
	 nb_cargo , 
	 departure_nb_conges_1787_inputdone , departure_nb_conges_1787_cr , departure_nb_conges_1789_inputdone , departure_nb_conges_1789_cr,
	 destination_nb_conges_1787_inputdone , destination_nb_conges_1787_cr , destination_nb_conges_1789_inputdone , destination_nb_conges_1789_cr
	 from navigoviz.built_travels bt where travel_id = '0008150N- 01'
	 
	 select pointcall_uncertainity, *  from pointcall p where pkid = 59796
select * from ports.port_points pp where uhgs_id = 'A0136930';
	  select count(*) from navigoviz.built_travels bt -- 128536
	 	
	  select count(*) from navigoviz.built_travels_backup17juin22 bt -- 126408

	  select count(*)  from navigoviz.uncertainity_travels --64268
	  
alter table navigoviz.built_travels add column nb_cargo int default 0;
update navigoviz.built_travels set nb_cargo  = jsonb_array_length(all_cargos);

select nb_longcours_marseille_1789, * from ports.port_points pp where nb_longcours_marseille_1789 is not null
select Nb_petitcabotage_1789, * from ports.port_points pp where Nb_petitcabotage_1789 is not null
select Nb_sante_1789, * from ports.port_points pp where Nb_sante_1789 is not null
select Nb_sante_1787, * from ports.port_points pp where Nb_sante_1787 is not null

drop table navigoviz.built_travels cascade;

-- Permissions

ALTER TABLE navigoviz.built_travels OWNER TO postgres;
GRANT ALL ON TABLE navigoviz.built_travels TO postgres;
GRANT select ON TABLE navigoviz.built_travels TO api_user;


update navigoviz.built_travels  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id and to_uhgs_id=destination_uhgs_id)
            or (from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id)
            and r.pointpath is not null;
-- 5858

           
select departure, destination, destination_state_1789_fr, destination_substate_1789_fr  ,
commodity_purpose, commodity_id, commodity_purpose2 , commodity_purpose3 , commodity_purpose4 ,
commodity_standardized_fr, commodity_standardized2_fr  commodity_standardized3_fr, commodity_standardized4_fr,
tonnage , tonnage_unit ,
ship_flag_standardized_fr ,
ship_name , ship_id , captain_name ,
departure_pkid ,
departure_out_date ,
source_text 
from navigoviz.raw_flows rf 
where rf.departure_uhgs_id = 'A0204180' and rf.destination_state_1789_fr  in ('Grande-Bretagne')
and extract(year from rf.outdate_fixed) = 1789 and tonnage != '12'
and commodity_purpose='lest'

----------------------------------
-- calcul d'un travel_rank pour les capitaines pour aider bernard
----------------------------------


select count(*) from navigoviz.uncertainity_travels ut 
-- 64268
-- 76792

select count(travel_id) from navigoviz.built_travels ut 
-- 128536
-- 128537
alter table navigoviz.uncertainity_travels add column captain_travel_rank int;
alter table navigoviz.uncertainity_travels add column shipcaptain_travel_rank int;


select depart_captainid, ship_id , outdate_fixed, id, travel_uncertainity, departure , destination , travel_rank,
row_number() over (PARTITION BY depart_captainid order by outdate_fixed asc, id asc, destination_uncertainity asc) as captain_travel_rank
from navigoviz.uncertainity_travels ut 

update navigoviz.uncertainity_travels ut  set captain_travel_rank = k.captain_travel_rank
from (
select departure_pkid, destination_pkid , depart_captainid, ship_id , outdate_fixed, id, travel_uncertainity, departure , destination , travel_rank,
row_number() over (PARTITION BY depart_captainid order by outdate_fixed asc, id asc, destination_uncertainity asc) as captain_travel_rank
from navigoviz.uncertainity_travels ut 
) as k
where ut.departure_pkid = k.departure_pkid and ut.destination_pkid = k.destination_pkid

update navigoviz.uncertainity_travels ut  set shipcaptain_travel_rank = k.shipcaptain_travel_rank
from (
select departure_pkid, destination_pkid , depart_captainid, ship_id , outdate_fixed, id, travel_uncertainity, departure , destination , travel_rank,
row_number() over (PARTITION BY ship_id, depart_captainid order by outdate_fixed asc, id asc, destination_uncertainity asc) as shipcaptain_travel_rank
from navigoviz.uncertainity_travels ut 
) as k
where ut.departure_pkid = k.departure_pkid and ut.destination_pkid = k.destination_pkid

-- sur le serveur uniquement
alter table navigoviz.built_travels add column captain_travel_rank int;
alter table navigoviz.built_travels add column shipcaptain_travel_rank int;

update navigoviz.built_travels b  set captain_travel_rank = k.captain_travel_rank
from (
select departure_pkid, destination_pkid ,captain_travel_rank
from navigoviz.uncertainity_travels  
) as k
where b.departure_pkid = k.departure_pkid and b.destination_pkid = k.destination_pkid
-- 128536

comment on column navigoviz.built_travels.captain_travel_rank is 'rank of this segment of travel for each captain (from 1 to n per captain_id)';
comment on column navigoviz.built_travels.shipcaptain_travel_rank is 'rank of this segment of travel for a combination of one captain_id and one ship_id';


update navigoviz.built_travels b  set captain_travel_rank = k.captain_travel_rank
from (
select departure_pkid, destination_pkid ,
captain_id, ship_id , outdate_fixed, travel_rank, travel_uncertainity, departure , destination , travel_rank,
row_number() over (PARTITION BY captain_id order by outdate_fixed asc, travel_rank asc, destination_uncertainity asc) as captain_travel_rank
from navigoviz.built_travels ut 
where source_entry != 'both-to' 
) as k
where b.departure_pkid = k.departure_pkid and b.destination_pkid = k.destination_pkid
-- 128536
-- Fait sur le serveur le 18 juillet 2022

update navigoviz.built_travels b  set shipcaptain_travel_rank = k.shipcaptain_travel_rank
from (
select departure_pkid, destination_pkid ,
captain_id, ship_id , outdate_fixed, travel_rank, travel_uncertainity, departure , destination , travel_rank,
row_number() over (PARTITION BY ship_id, captain_id order by outdate_fixed asc, travel_rank asc, destination_uncertainity asc) as shipcaptain_travel_rank
from navigoviz.built_travels ut 
where source_entry != 'both-to' 
) as k
where b.departure_pkid = k.departure_pkid and b.destination_pkid = k.destination_pkid
-- 128536
-- Fait sur le serveur le 20 juillet 2022


select departure_pkid, destination_pkid ,
captain_id, ship_id , outdate_fixed, travel_rank, travel_uncertainity, departure , destination , travel_rank, travel_id,
row_number() over (PARTITION BY ship_id, captain_id order by outdate_fixed asc, travel_rank asc, destination_uncertainity asc) as shipcaptain_travel_rank
from navigoviz.built_travels ut 
where source_entry != 'both-to' and captain_id = '00000143'

-- 0007200N- 50 : corriger la concaténation :  t.ship_id||'-'||to_char(t.travel_rank, '999')
-- select 'a'||'-'|| to_char(1, 'FM999')
-- select 'a'||'-'|| to_char(125, 'FM999')

select to_char(1, '9')


select departure_pkid, destination_pkid , captain_name ,
captain_id, ship_id , outdate_fixed, travel_rank, travel_uncertainity, departure , destination , travel_rank, travel_id,
row_number() over (PARTITION BY ship_id, captain_id order by outdate_fixed asc, travel_rank asc, destination_uncertainity asc) as shipcaptain_travel_rank
from navigoviz.built_travels ut 
where ship_name like 'Marie Louise' and ship_id = '0000110N'
-- and captain_name like 'Caplain%'

--http://data.portic.fr/api/travels/?format=json&params=shipcaptain_travel_rank,departure,destination,ship_name,captain_name,geom,pointpath&ship_id=0000110N
http://data.portic.fr/api/travels/?format=json&params=shipcaptain_travel_rank,departure,destination,ship_name,captain_name,geom,pointpath&ship_id=0002931N

select * from ship where  ship_id = '0000110N'
select * from portic_v7.navigoviz.pointcall p where p.ship_id = '0000110N'
00109024
00109172
00109568
00109718


Select ship_id, trim(ship_name) as ship_name, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
		WHERE p.ship_id in (select distinct ship_id from built_travels where ship_id = '0000110N') 
			GROUP BY trim(ship_name),ship_id
			ORDER BY ship_id, occurences desc
			
select shipcaptain_travel_rank,departure,destination,ship_name,captain_name,outdate_fixed,indate_fixed 
from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0002931N' )


select * from navigoviz.pointcall
where  ship_id = '0002931N'
order by pointcall_rankfull 
-- perdu la partie 1789


update navigoviz.built_travels  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id and to_uhgs_id=destination_uhgs_id)
            or (from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id)
            and r.pointpath is not null
-- 5858, fait sur le serveur
            
-------------------------------------------------------------------------------------------------
-- bug vizsources
select uhgs_id , nb_sante_1787, nb_sante_1789, nb_petitcabotage_1787, nb_petitcabotage_1789 , nb_longcours_marseille_1789 
from ports.port_points
where toponyme_standard_fr = 'Marseille';
-- c 3295	3075	2526	2416	119



select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    source_suite, pointcall_year,
    (case when pointcall_year = '1787' then nb_conges_1787_inputdone else nb_conges_1789_inputdone end) as nb_conges_inputdone,    
    (case when pointcall_year = '1787' then nb_conges_1787_cr else nb_conges_1789_cr end) as nb_conges_cr,
    (case when pointcall_year = '1787' then nb_sante_1787 else nb_sante_1789 end) as nb_conges_sante,
    (case when pointcall_year = '1787' then nb_petitcabotage_1787 else nb_petitcabotage_1789 end) as nb_petitcabotage,
    nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, nb_flag_filled, good_sum_tonnage,
    nb_tonnage_deduced, nb_homeport_deduced, nb_birthplace_deduced,nb_citizenship_deduced, nb_flag_deduced, good_sum_tonnage_deduced
from ports.port_points pp, -- left outer join
    (
     select 
        pointcall_uhgs_id, source_suite, pointcall_year, 
        count(k.tonnage) as nb_tonnage_filled,
        count(k.homeport) as nb_homeport_filled,
        count(commodity_purpose) as nb_product_filled,
        count(k.birthplace) as nb_birthplace_filled,
        count(k.citizenship) as nb_citizenship_filled,
        count(flag) as nb_flag_filled,
        sum(k.tonnage_numeric) as good_sum_tonnage,
        
        count(tonnage_deduced.tonnage) as nb_tonnage_deduced,
        count(homeport_deduced.homeport) as nb_homeport_deduced,
        count(birthplace_deduced.birthplace) as nb_birthplace_deduced,
        count(citizenship_deduced.citizenship) as nb_citizenship_deduced,
        count(flag_deduced.ship_flag) as nb_flag_deduced,
        sum(tonnage_deduced.tonnage_numeric) as good_sum_tonnage_deduced

        from (
            select pkid, pointcall_uhgs_id, source_suite,
            (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year, 
            nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr, 
            tonnage, homeport, commodity_purpose, birthplace, citizenship,  flag,
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric 
            from navigoviz.pointcall p 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' 
        ) as k 
        
        left join
        (
            select p.pkid, tonnage,
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
            from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_tonnage = -2
        ) as tonnage_deduced on tonnage_deduced.pkid = k.pkid 
        
        left join
        (
            select p.pkid, p.homeport
            from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_homeport  = -2
        ) as homeport_deduced
        on homeport_deduced.pkid = k.pkid 
        
        left join
        (
            select p.pkid, p.birthplace
            from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_birthplace  = -2
        ) as birthplace_deduced
        on birthplace_deduced.pkid = k.pkid 
        
         left join
        (
            select p.pkid, p.citizenship
            from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_citizenship  = -2
        ) as citizenship_deduced
        on citizenship_deduced.pkid = k.pkid 
        
        
        left join
        (
            select p.pkid, ship_flag
            from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_flag  = -2
        ) as flag_deduced
        on flag_deduced.pkid = k.pkid 
        
        
        
    group by  pointcall_uhgs_id, source_suite, pointcall_year
    ) as toto
 where  toto.pointcall_uhgs_id = pp.uhgs_id and pp.state_1789_fr = 'France' 
 --toto.pointcall_uhgs_id = 'A0210797' and
union
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,
    null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is not  null and nb_conges_1787_inputdone is null)
/*union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    null as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is  null and nb_conges_1787_inputdone is null and geom is not null)*/
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1789 as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,
    null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1789_cr is not  null and nb_conges_1789_inputdone is null )
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    null as source_suite, null as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled,  null as good_sum_tonnage,
    null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
from ports.port_points pp
where state_1789_fr = 'France' 
and nb_conges_1787_cr is null and nb_conges_1787_inputdone is null 
and nb_conges_1789_cr is null and nb_conges_1789_inputdone is null 
and geom is not null)
order by toponym, pointcall_year, source_suite

select p.state_1789_fr , p.substate_1789_fr, sum(tonnage::float) 
from portic_v7.navigoviz.pointcall p 
where  p.pointcall_uhgs_id = 'B2119892' and commodity_standardized in ('Salt', 'Fishing (cod)', 'Cod')
group by p.state_1789_fr , p.substate_1789_fr;

-- marseille test 
select count(pkid), pointcall_uhgs_id, source_suite,  (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year 
from portic_v7.navigoviz.pointcall p 
where state_1789_fr = 'France' and  pointcall_uhgs_id = 'A0210797' and pointcall_function = 'O' and source_suite='la Santé registre de patentes de Marseille'
group by pointcall_uhgs_id, source_suite, pointcall_year;

-- tonnage déduits

select p.pkid, tonnage,
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
            from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2 
            where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_tonnage = -2
-- 1382   

-- vérif des chiffres pour le cas de st tropez
select * from ports.port_points pp where uhgs_id = 'A0147257';
select source, ship_tonnage , ship_flag from navigo.pointcall p 
where p.pointcall_uhgs_id = 'A0147257' and p.pointcall_function = 'O';
-- ADVar, 7B10 A0147257	G5	1787	535 535

-- vérif pour marseille pointcall_uhgs_id = 'A0210797' and data_block_leader_marker = 'A'
select nb_sante_1787 , nb_sante_1789  from ports.port_points pp where uhgs_id = 'A0210797';

select source, ship_tonnage , ship_homeport , ship_flag, captain_birthplace , captain_citizenship  from navigo.pointcall p 
where p.pointcall_uhgs_id = 'A0210797' and source like '%200 E 60%'
--and ship_tonnage is not null and ship_tonnage not like '[%';
    and ship_homeport not like '[%';
   
select distinct p.commodity_id  , p.commodity_standardized 
from portic_v7.navigoviz.pointcall p 
where  p.pointcall_uhgs_id = 'B2119892' and  commodity_standardized in ('Salt', 'Fishing (cod)', 'Cod')


select p.state_1789_fr , p.substate_1789_fr,  p.pointcall_uhgs_id , p.pointcall , p.commodity_id  , p.commodity_standardized 
from portic_v7.navigoviz.pointcall p 
where  p.pointcall_uhgs_id = 'B2119892' and commodity_standardized in ('Salt', 'Fishing (cod)', 'Cod')
-- group by p.state_1789_fr , p.substate_1789_fr;

select * from port_points pp where pp.toponyme = 'Dunkerque'
-- A0204180

select * from navigoviz.raw_flows rf where departure_uhgs_id = 'A0204180';


select extract(year from rf.outdate_fixed) as annee, 
departure,travel_id,distance_dep_dest,distance_homeport_dep,departure_pkid,departure_fr,departure_en,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination,destination_pkid,destination_fr,destination_en,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_shiparea,destination_status,destination_status_uncertainity,destination_in_date,destination_action,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,tonnage_class,in_crew,flag,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_status,homeport_shiparea,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_number,source_other,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,status,citizenship,birthplace_uhgs_id,commodity_purpose,commodity_id,quantity,quantity_u,commodity_standardized,commodity_standardized_fr,commodity_permanent_coding,cargo_item_action,commodity_purpose2,commodity_id2,quantity2,quantity_u2,commodity_standardized2,commodity_standardized2_fr,commodity_permanent_coding2,cargo_item_action2,commodity_purpose3,commodity_id3,quantity3,quantity_u3,commodity_standardized3,commodity_standardized3_fr,commodity_permanent_coding3,cargo_item_action3,commodity_purpose4,commodity_id4,quantity4,quantity_u4,commodity_standardized4,commodity_standardized4_fr,commodity_permanent_coding4,cargo_item_action4,tax_concept1,payment_date,q01_1,q01_u,q02_1,q02_u,q03_1,q03_u,taxe_amount01,tax_concept2,q01_2,q02_2,q03_2,taxe_amount02,tax_concept3,q01_3,q02_3,q03_3,taxe_amount03,tax_concept4,q01_4,q02_4,q03_4,taxe_amount04,tax_concept5,q01_5,q02_5,q03_5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,shipclass_uncertainity,birthplace_uhgs_id_uncertainity,birthplace_uncertainity,citizenship_uncertainity,distance_dep_dest_miles,distance_homeport_dep_miles,nb_cargo
 from navigoviz.raw_flows rf 
where extract(year from rf.outdate_fixed) = 1787 and destination_state_1789_fr = 'Grande-Bretagne';

select sum(tonnage::float), tonnage_unit 
 from navigoviz.raw_flows rf 
 where extract(year from rf.outdate_fixed)= 1787 and destination_state_1789_fr = 'Grande-Bretagne'
 group by tonnage_unit;

select extract(year from rf.outdate_fixed) as annee, 
departure,travel_id,distance_dep_dest,distance_homeport_dep,departure_pkid,departure_fr,departure_en,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination,destination_pkid,destination_fr,destination_en,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_shiparea,destination_status,destination_status_uncertainity,destination_in_date,destination_action,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,tonnage_class,in_crew,flag,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_status,homeport_shiparea,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_number,source_other,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,status,citizenship,birthplace_uhgs_id,commodity_purpose,commodity_id,quantity,quantity_u,commodity_standardized,commodity_standardized_fr,commodity_permanent_coding,cargo_item_action,commodity_purpose2,commodity_id2,quantity2,quantity_u2,commodity_standardized2,commodity_standardized2_fr,commodity_permanent_coding2,cargo_item_action2,commodity_purpose3,commodity_id3,quantity3,quantity_u3,commodity_standardized3,commodity_standardized3_fr,commodity_permanent_coding3,cargo_item_action3,commodity_purpose4,commodity_id4,quantity4,quantity_u4,commodity_standardized4,commodity_standardized4_fr,commodity_permanent_coding4,cargo_item_action4,tax_concept1,payment_date,q01_1,q01_u,q02_1,q02_u,q03_1,q03_u,taxe_amount01,tax_concept2,q01_2,q02_2,q03_2,taxe_amount02,tax_concept3,q01_3,q02_3,q03_3,taxe_amount03,tax_concept4,q01_4,q02_4,q03_4,taxe_amount04,tax_concept5,q01_5,q02_5,q03_5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,shipclass_uncertainity,birthplace_uhgs_id_uncertainity,birthplace_uncertainity,citizenship_uncertainity,distance_dep_dest_miles,distance_homeport_dep_miles,nb_cargo
 from navigoviz.raw_flows rf ;
--where extract(year from rf.outdate_fixed) = 1787 and destination_state_1789_fr = 'Grande-Bretagne';

select pointcall, homeport, * from navigoviz.pointcall p where p.source_text  in ('ANF, G5-74/1775', 'ANF, G5-91/9825');
-- England (fake destination for)
select pointcall, captain_name , * from navigoviz.pointcall p where p.pkid = 15120;

-- homeport  : Waton
[Walton on the Naze]
------------------------------------------------------------------------------------
-- le 25 octobre 2022
--------------------------------------------------------------------------------------

select * from navigoviz.raw_flows where departure like '%Sables%';--Sables d' Olonne, A0137148
select * from navigoviz.raw_flows where departure like '%Marennes%';--Marennes, A0136930
select * from navigoviz.raw_flows where destination like '%Terre%';
--Terre Neuve, B2119892 
-- ou Nord [Petit Nord de Terre-Neuve] B0000946 
-- ou Saint Jean de Terre Neuve B2123022
-- ou Cap de Raye [Terre Neuve] B2126113
-- ou Les trois îles côte de Terre Neuve  B2118336
-- ou Banc de Terre Neuve B0000715
-- ou Ferol côte de Terre Neuve B2120324
-- ACA-NEWF

/*Terre Neuve	B2119892
Ferol côte de Terre Neuve	B2120324
Havre de la Sie en Terreneuve	B2138259
Baye du Griguet en Terreneuve	B0000971
Havre de Paquet (Terre neuve)	B2123277
Havre des Petites Oyes en Terreneuve	B2123732
Ficheau en Terre Neuve	B2129817
Baye de Saintfont en Terreneuve [Sans-Fonds]	B2130491
Baye des Cannaries en Terreneuve	B0000967
Belille en Terreneuve	B2134366
Baie de Saint Antoine en Terreneuve	B2136171
Kerpont en Terreneuve	B2118942
Havre du Four en Terreneuve	B0000953
Baye Forché en Terreneuve	B2129034
Les trois îles côte de Terre Neuve	B2118336
Cap de Raye [Terre Neuve]	B2126113
Saint Pierre et Miquelon	B1965595
Iles Magdelaine	B2125493
Banc de Terre Neuve	B0000715
Baye de Fleur de Lys en Terreneuve	B2123796
Havre de la Cramailliere en Terreneuve	B0000957
Cap Rouge en Terreneuve	B2128963
Baÿe de Hara en Terreneuve	B0000713
Baye de Crok en Terreneuve [Croque]	B2117763
Port au Choix	B2121832
Saint Jean de Terre Neuve	B2123022
Havre de Boutitout en Terreneuve	B0000944
Havre de Grandes Oyes en Terreneuve	B0000950
Baye Saint Jullien en Terreneuve	B2116953
Nord [Petit Nord de Terre-Neuve]	B0000946
Havre de Pins en Terreneuve	B0000941
Ingouraschar [Terre Neuve]	B2134969
Islets en Terreneuve	B0000968
Conce en Terreneuve	B2118220*/

select  toponyme, uhgs_id from portic_v7.ports.port_points pp where pp.shiparea = 'ACA-NEWF'
select * from navigoviz.raw_flows where destination like '%Terre%';


select * from navigoviz.raw_flows 
where departure_uhgs_id  = 'A0137148'
and destination_shiparea  = 'ACA-NEWF';
--and destination_uhgs_id in ('B2119892', 'B0000946', 'B2123022', 'B2126113', 'B2118336', 'B0000715', 'B2120324') ;
-- vers '%Terre Neuve%'
--Sables d' Olonne, A0137148


select * from navigoviz.raw_flows 
where departure_uhgs_id  = 'A0136930'
and destination_shiparea  = 'ACA-NEWF';
--and destination_uhgs_id in ('B2119892', 'B0000946', 'B2123022', 'B2126113', 'B2118336', 'B0000715', 'B2120324') ;
-- Marennes vers Terre Neuve

-- depuis l'amirauté
select * from navigoviz.raw_flows where departure_admiralty  like '%Sables%';--Sables-d’Olonne, A0137148

select source_text , departure_fr, departure_uhgs_id, departure_admiralty , tonnage, commodity_purpose , departure_out_date , destination_fr 
from navigoviz.raw_flows 
where departure_admiralty = 'Sables-d’Olonne'
and destination_shiparea  = 'ACA-NEWF';
-- commodity_purpose2 , commodity_purpose3 , 
-- 5



-- and destination_uhgs_id in ('B2119892', 'B0000946', 'B2123022', 'B2126113', 'B2118336', 'B0000715', 'B2120324') ;

select departure_fr, departure_uhgs_id, departure_admiralty , tonnage, commodity_purpose , commodity_purpose2 , commodity_purpose3  from navigoviz.raw_flows 
where departure_admiralty = 'Marennes'
and destination_shiparea  = 'ACA-NEWF';
-- 5


select captain_name , ship_name, citizenship , birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where captain_name like '%Mill%';
-- Millis, William	Sally	Dunkerque	Douvres	1789=04=06
-- Pierre Millié

select captain_name , ship_name, citizenship , birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where captain_name like '%Forgeri%';
-- capitaine Forgerit

select captain_name , ship_name, citizenship , birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Pérou%';
select captain_name , ship_name, citizenship , birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Protecteur%';
--Couture, Jean	Protecteur			Bordeaux	Bordeaux	Martinique	1787=03=14
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Deux soeurs%';
/*
Auger, Louis	Deux soeurs		Oléron	La Rochelle	île d'Oléron	1787=02=26
Dernier, Joseph	Deux soeurs		Redon	Rochefort	La Teste	1787=12=14
Fross, Hamming	Deux soeurs		Christiansand	Ars-en-Ré	Kristiansand 	1787=06=04
Verron, Pierre	Deux soeurs		Saint Savinien	Tonnay-Charente	Marennes	1787=04=02
Domain, Joseph	Deux soeurs		Redon	Rochefort	Tonnay-Charente	1787=09=15
Desheulles, George	Deux soeurs	Rouen		Malaga	Rouen	1787<01<24!
Barré, Jean	Deux soeurs		La Rochelle	La Rochelle	Rochefort	1787=08=16
Augé, Louis	Deux soeurs		Oléron	Le Château-d'Oléron	Rochefort	1787=10=17
Augé, Louix	Deux soeurs		Oléron	Le Château-d'Oléron	Rochefort	1787=12=11
Barré, Jean	Deux soeurs		La Rochelle	Rochefort	La Rochelle	1787<08<24!
Barré, Jean	Deux soeurs		La Rochelle	Soubise	La Rochelle	1787=08=24
Damien, Joseph	Deux soeurs		Redon	Tonnay-Charente	La Teste	1787=09=24
Desheulles, George	Deux soeurs	Rouen		Le Havre	Rouen	1787=01=24
Auger, Louis	Deux soeurs		Oléron	Rochefort	île d'Oléron	1787=03=07
*/
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Jeune Rose%' order by captain_name;
-- Dubernet, Leonard	Jeune Rose		Bordeaux	Bordeaux	Saint-Domingue	1787=11=10
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Comte de %' order by captain_name;
-- pas trouvé
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where captain_name like '%Boulineau%' order by captain_name;
-- -- pas trouvé
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where captain_name like '%Dubois%' order by captain_name;
-- Dubois, Guillaume	Comtede Forcalyner		La Rochelle	La Rochelle	Côte d'Angole	1787=04=02
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where captain_name like '%Rigolage%' order by captain_name;
-- Rigolage, Pierre mais pas de Clocheterie
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Clocheterie%' order by captain_name;
-- pas trouvé
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Aimable Lucile%' order by captain_name;
-- pas trouvé
select captain_name , ship_name, birthplace , homeport , departure_fr , destination_fr , departure_out_date 
from navigoviz.raw_flows where ship_name like '%Berceau%' order by captain_name;
-- pas trouvé

select * from navigoviz.raw_flows where tax_concept1 like '%p%che' 
/*Sables d'Olonne, je suis retourné voir les sources (congès G5 1787). Verdict, nous (moi et mes étudiants) avons saisi dans la base Navigocorpus plusieurs "congés de pêche" :
- campagnes dites "en prime" (retour durant l'été pour vente "primeure") février : 9 et mars : 9
- avril et mai : 0
- campagnes dites "en tard" (retour avant l'hiver) juin : 3 ; juillet : 12 et août : 2
- septembre, octobre, novembre, décembre : 0*/

select distinct tax_concept1 from navigoviz.pointcall p ;-- Congé de pêche

select source_doc_id , source_text , toponyme_fr , pointcall_uhgs_id , pointcall  , tonnage, commodity_purpose , commodity_purpose2 , pointcall_out_date 
from navigoviz.pointcall p 
where (tax_concept1 like '%Terre%' or tax_concept2 like '%Terre%') and pointcall_uhgs_id = 'A0137148'
order by pointcall_out_date;

select distinct tax_concept1 from navigoviz.pointcall p 
where  pointcall_uhgs_id = 'A0137148' ;
-- Congé pour la pêche de Terre Neuve
-- Droit de congé de pêche [morue à Terre-Neuve]
Congé français [pour la pêche au poisson frais]
Congé français [pour pêche la pêche au poisson frais]

select source_doc_id,  source_text , r.departure_out_date , destination_fr, destination_uhgs_id , ship_name , captain_name , tonnage, tax_concept1, tax_concept2, tax_concept3, commodity_purpose , commodity_purpose2 , commodity_purpose3 
from navigoviz.raw_flows r
where source_doc_id in 
(select source_doc_id 
from navigoviz.pointcall p 
where (tax_concept1 like '%Terre%' or tax_concept2 like '%Terre%') and pointcall_uhgs_id = 'A0137148'
order by pointcall_out_date)
order by departure_out_date, source_doc_id, r.travel_rank ;


-- 55975 lignes
create table navigoviz.raw_flows_fix as (
            select  
                ROW_NUMBER () OVER () as travel_id,
                round(((st_distance(d.pointcall_point , a.pointcall_point ))/1000) :: numeric, 3) as distance_dep_dest,
                round(((st_distance(d.pointcall_point , d.homeport_point::geometry ))/1000.0) :: numeric, 3) as distance_homeport_dep,
                    d.pointcall as departure,
                    d.pkid as departure_pkid,
                    substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int as departure_rank_dedieu,
                    d.toponyme_fr as departure_fr, 
                    d.toponyme_en as departure_en,
                    d.pointcall_uhgs_id  as departure_uhgs_id,
                    d.latitude as departure_latitude,
                    d.longitude as departure_longitude,
                    d.pointcall_admiralty as departure_admiralty,
                    d.pointcall_province as departure_province,
                    d.pointcall_states as departure_states,
                    d.pointcall_substates as departure_substates,
                    d.state_1789_fr as departure_state_1789_fr, 
                    d.substate_1789_fr as departure_substate_1789_fr, 
                    d.state_1789_en as departure_state_1789_en, 
                    d.substate_1789_en as departure_substate_1789_en,
                    d.ferme_direction as departure_ferme_direction, 
                    d.ferme_direction_uncertainty as departure_ferme_direction_uncertainty, 
                    d.ferme_bureau as departure_ferme_bureau, 
                    d.ferme_bureau_uncertainty as departure_ferme_bureau_uncertainty, 
                    d.partner_balance_1789 as departure_partner_balance_1789, 
                    d.partner_balance_supp_1789 as departure_partner_balance_supp_1789, 
                    d.partner_balance_1789_uncertainty as departure_partner_balance_1789_uncertainty, 
                    d.partner_balance_supp_1789_uncertainty as departure_partner_balance_supp_1789_uncertainty,
                    d.shiparea as departure_shiparea,
                    d.pointcall_status as departure_status,
                    -- 26
                    d.nb_conges_1787_inputdone as departure_nb_conges_1787_inputdone, 
                    d.Nb_conges_1787_CR as departure_Nb_conges_1787_CR, 
                    d.nb_conges_1789_inputdone as departure_nb_conges_1789_inputdone, 
                    d.Nb_conges_1789_CR as departure_Nb_conges_1789_CR, 
                    d.pointcall_status_uncertainity as departure_status_uncertainity,
                    d.pointcall_point as departure_point,
                    d.pointcall_out_date as departure_out_date,
                    d.pointcall_action as departure_action,
                    d.date_fixed as outdate_fixed,
                    d.navigo_status as departure_navstatus,
                    d.pointcall_function as departure_function,

                    a.pointcall as destination,
                    a.pkid as destination_pkid,
                    substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int as destination_rank_dedieu,
                    a.toponyme_fr as destination_fr, 
                    a.toponyme_en as destination_en,
                    a.pointcall_uhgs_id  as destination_uhgs_id,
                    a.latitude as destination_latitude,
                    a.longitude as destination_longitude,
                    a.pointcall_admiralty as destination_admiralty,
                    a.pointcall_province as destination_province,
                    a.pointcall_states as destination_states,
                    a.pointcall_substates as destination_substates,
                    a.state_1789_fr as destination_state_1789_fr, 
                    a.substate_1789_fr as destination_substate_1789_fr, 
                    a.state_1789_en as destination_state_1789_en, 
                    a.substate_1789_en as destination_substate_1789_en,
                    a.ferme_direction as destination_ferme_direction, 
                    a.ferme_direction_uncertainty as destination_ferme_direction_uncertainty, 
                    a.ferme_bureau as destination_ferme_bureau, 
                    a.ferme_bureau_uncertainty as destination_ferme_bureau_uncertainty, 
                    a.partner_balance_1789 as destination_partner_balance_1789, 
                    a.partner_balance_supp_1789 as destination_partner_balance_supp_1789, 
                    a.partner_balance_1789_uncertainty as destination_partner_balance_1789_uncertainty, 
                    a.partner_balance_supp_1789_uncertainty as destination_partner_balance_supp_1789_uncertainty,
                    a.shiparea as destination_shiparea,
                    a.pointcall_status as destination_status,
                    -- 34
                    a.nb_conges_1787_inputdone as destination_nb_conges_1787_inputdone, 
                    a.Nb_conges_1787_CR as destination_Nb_conges_1787_CR, 
                    a.nb_conges_1789_inputdone as destination_nb_conges_1789_inputdone, 
                    a.Nb_conges_1789_CR as destination_Nb_conges_1789_CR, 
                    a.pointcall_status_uncertainity as destination_status_uncertainity,
                    a.pointcall_point as destination_point,
                    a.pointcall_in_date as destination_in_date,
                    a.pointcall_action as destination_action,
                    a.date_fixed as indate_fixed,
                    a.navigo_status as destination_navstatus,
                    a.pointcall_function as destination_function,
                    d.ship_name,
                    d.ship_id,
                    d.tonnage,
                    d.tonnage_unit,
                    d.tonnage_class,
                    d.in_crew,
                    d.flag,
                    d.ship_flag_id,
                    d.ship_flag_standardized_fr, 
                    d.ship_flag_standardized_en, 
                    d.class,
                    d.homeport,
                    d.homeport_uhgs_id , 
                    d.homeport_latitude,
                    d.homeport_longitude,
                    d.homeport_admiralty,
                    d.homeport_province,
                    d.homeport_states,
                    d.homeport_substates,
                    d.homeport_status,
                    d.homeport_shiparea,
                    d.homeport_point,
                    d.homeport_state_1789_fr,
                    d.homeport_substate_1789_fr,
                    d.homeport_state_1789_en,
                    d.homeport_substate_1789_en,
                    d.homeport_toponyme_fr,
                    d.homeport_toponyme_en,
                    'from' as source_entry, 
                    d.source_doc_id,
                    d.source_text,
                    d.source_suite,
                    d.source_component,
                    d.source_number,
                    d.source_other,
                    d.source_main_port_uhgs_id, 
                    d.source_main_port_toponyme,
                    d.source_subset,
                    d.captain_id,
                    d.captain_name,
                    d.birthplace,
                    d.status,
                    d.citizenship,
                    d.birthplace_uhgs_id,
                    d.commodity_purpose,
                    d.commodity_id,
                    d.quantity,
                    d.quantity_u,
                    d.commodity_standardized,
                    d.commodity_standardized_fr,
                    d.commodity_permanent_coding,
                    d.cargo_item_action,
                    d.commodity_purpose2,
                    d.commodity_id2,
                    d.quantity2,
                    d.quantity_u2,
                    d.commodity_standardized2,
                    d.commodity_standardized2_fr,
                    d.commodity_permanent_coding2,
                    d.cargo_item_action2,
                    d.commodity_purpose3,
                    d.commodity_id3,
                    d.quantity3,
                    d.quantity_u3,
                    d.commodity_standardized3,
                    d.commodity_standardized3_fr,
                    d.commodity_permanent_coding3,
                    d.cargo_item_action3,
                    d.commodity_purpose4,
                    d.commodity_id4,
                    d.quantity4,
                    d.quantity_u4,
                    d.commodity_standardized4,
                    d.commodity_standardized4_fr,
                    d.commodity_permanent_coding4,
                    d.cargo_item_action4,
                    d.all_cargos,
                    -- 84
                    d.tax_concept1,
                    d.payment_date,
                    d.q01_1, d.q01_u , d.q02_1 , d.q02_u , d.q03_1 , d.q03_u , d.taxe_amount01,
                    d.all_taxes,
                    --10
                    d.tax_concept2, d.q01_2, d.q02_2 , d.q03_2 , d.taxe_amount02,
                    d.tax_concept3, d.q01_3, d.q02_3 , d.q03_3 , d.taxe_amount03,
                    d.tax_concept4, d.q01_4, d.q02_4 , d.q03_4 , d.taxe_amount04,
                    d.tax_concept5, d.q01_5, d.q02_5 , d.q03_5 , d.taxe_amount05,
                    --20
                    d.ship_uncertainity,
                    d.tonnage_uncertainity ,
                    d.flag_uncertainity,
                    d.homeport_uncertainity ,
                    d.pointcall_uncertainity as departure_uncertainity,
                    d.pointcall_uncertainity as destination_uncertainity,
                    d.captain_uncertainity,
                    null as travel_uncertainity,
                    d.cargo_uncertainity,
                    d.taxe_uncertainity, 
                    d.pointcall_outdate_uncertainity,
                    d.shipclass_uncertainity,
                    d.birthplace_uhgs_id_uncertainity,
                    d.birthplace_uncertainity,
                    d.citizenship_uncertainity    
            from navigoviz.pointcall d , navigoviz.pointcall a 
            where 
             	d.source_doc_id  = a.source_doc_id 
     			and substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int  + 1 = substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int
    			ORDER by source_doc_id,  d.pointcall_rank_dedieu
               
            ); -- 55975 lignes

           
           /* (d.pointcall_action in ('Out', 'In-Out', 'Loading', 'Transit', 'In-out', 'Sailing around')) 
		        and (a.pointcall_action in ('In', 'In-Out', 'Unloading', 'Transit', 'In-out', 'Sailing around')) 
		        AND a.data_block_leader_marker = 'T'
		        and d.source_doc_id  = a.source_doc_id
            ORDER by coalesce(d.ship_id, d.source_doc_id), d.pointcall_rankfull*/
           
            --Staying ;
            -- Partial-transit; Foundering ;  Transhipping-off ; In-emergency ; Fighting ; Captured
            select  pointcall_action, count(*) as n 
            from navigoviz.pointcall 
            group by pointcall_action
            order by n;
            
           /*Unloading	71
Loading	142
In-Out	311
Transit	878
Sailing around	2387
In-out	3574
In	50795
Out	50923*/
           /*Partial-transit	1
Foundering	1
Staying	1
Transhipping-off	1
In-emergency	2
Fighting	2
Captured	7*/
select  source_doc_id , source_text , pointcall_rank_dedieu, pointcall_out_date, toponyme_fr , pointcall_uhgs_id, pointcall_action,data_block_leader_marker , tonnage, commodity_purpose , commodity_purpose2 , tax_concept1 , tax_concept2  
from navigoviz.pointcall p 
where source_doc_id in (
	select source_doc_id from navigoviz.pointcall  where pointcall_action in ('Partial-transit', 'Foundering', 'Staying', 'Transhipping-off', 'In-emergency', 'Fighting', 'Captured') )
 order by source_doc_id,  p.pointcall_rank_dedieu
 
 -- tester une autre requete
 select  d.source_doc_id , d.source_text , d.pointcall_rank_dedieu, d.pointcall_out_date, a.pointcall_in_date, d.toponyme_fr , a.toponyme_fr, d.pointcall_action, d.data_block_leader_marker, a.pointcall_action, a.data_block_leader_marker , d.tonnage, d.commodity_purpose , d.commodity_purpose2 , d.tax_concept1 , d.tax_concept2 
 from 

 (select  source_doc_id , source_text , substring(pointcall_rank_dedieu , 0, strpos( pointcall_rank_dedieu, '.')) ::int as pointcall_rank_dedieu, pointcall_out_date, toponyme_fr , pointcall_uhgs_id, pointcall_action,data_block_leader_marker , tonnage, commodity_purpose , commodity_purpose2 , tax_concept1 , tax_concept2  
from navigoviz.pointcall p 
where source_doc_id in (
	select source_doc_id from navigoviz.pointcall  where pointcall_action in ('Partial-transit', 'Foundering', 'Staying', 'Transhipping-off', 'In-emergency', 'Fighting', 'Captured') )
 order by source_doc_id,  p.pointcall_rank_dedieu ) as d,
 
(select  source_doc_id , source_text , substring(pointcall_rank_dedieu , 0, strpos( pointcall_rank_dedieu, '.')) ::int as pointcall_rank_dedieu, pointcall_in_date, toponyme_fr , pointcall_uhgs_id, pointcall_action,data_block_leader_marker , tonnage, commodity_purpose , commodity_purpose2 , tax_concept1 , tax_concept2  
from navigoviz.pointcall p 
where source_doc_id in (
	select source_doc_id from navigoviz.pointcall  where pointcall_action in ('Partial-transit', 'Foundering', 'Staying', 'Transhipping-off', 'In-emergency', 'Fighting', 'Captured') )
 order by source_doc_id,  p.pointcall_rank_dedieu ) as a 

 where 
     d.source_doc_id  = a.source_doc_id 
     and d.pointcall_rank_dedieu + 1 = a.pointcall_rank_dedieu
     ORDER by source_doc_id,  d.pointcall_rank_dedieu;
           

    -- convertir le rank en entier
select substring('3.0' , 0, 2);
select pointcall_rank_dedieu, strpos( pointcall_rank_dedieu, '.'), 
substring(pointcall_rank_dedieu , 0, strpos( pointcall_rank_dedieu, '.')) ::int as test 
from navigoviz.pointcall p ;

-- décompte     et export       
-- select count(travel_id)
select departure,travel_id,distance_dep_dest,distance_homeport_dep,departure_pkid,departure_fr,departure_en,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination,destination_pkid,destination_fr,destination_en,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_shiparea,destination_status,destination_status_uncertainity,destination_in_date,destination_action,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,tonnage_class,in_crew,flag,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_status,homeport_shiparea,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_number,source_other,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,status,citizenship,birthplace_uhgs_id,commodity_purpose,commodity_id,quantity,quantity_u,commodity_standardized,commodity_standardized_fr,commodity_permanent_coding,cargo_item_action,commodity_purpose2,commodity_id2,quantity2,quantity_u2,commodity_standardized2,commodity_standardized2_fr,commodity_permanent_coding2,cargo_item_action2,commodity_purpose3,commodity_id3,quantity3,quantity_u3,commodity_standardized3,commodity_standardized3_fr,commodity_permanent_coding3,cargo_item_action3,commodity_purpose4,commodity_id4,quantity4,quantity_u4,commodity_standardized4,commodity_standardized4_fr,commodity_permanent_coding4,cargo_item_action4,tax_concept1,payment_date,q01_1,q01_u,q02_1,q02_u,q03_1,q03_u,taxe_amount01,tax_concept2,q01_2,q02_2,q03_2,taxe_amount02,tax_concept3,q01_3,q02_3,q03_3,taxe_amount03,tax_concept4,q01_4,q02_4,q03_4,taxe_amount04,tax_concept5,q01_5,q02_5,q03_5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,shipclass_uncertainity,birthplace_uhgs_id_uncertainity,birthplace_uncertainity,citizenship_uncertainity,distance_dep_dest_miles,distance_homeport_dep_miles,nb_cargo
from navigoviz.raw_flows_fix rf ;
-- 40803
-- 55975 après correctif

alter table navigoviz.raw_flows_fix add column distance_dep_dest_miles text;
alter table navigoviz.raw_flows_fix add column distance_homeport_dep_miles text;

update navigoviz.raw_flows_fix t set distance_dep_dest_miles = q.distance_dep_dest_miles
            from (
                    select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                        SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,   stages__length_miles
                        FROM navigoviz.raw_flows_fix t , navigo.stages s 
                        where stages__length_miles is not null and stages__length_miles != '0.0' and t.departure_uhgs_id = s.pointcall_uhgs_id and t.destination_uhgs_id = s.next_point__pointcall_uhgs_id 
                    ) as k
            ) as q
            where q.travel_id = t.travel_id;
-- 52182 
update navigoviz.raw_flows_fix t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,  stages__length_miles
                   FROM navigoviz.raw_flows_fix t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.destination_uhgs_id = s.pointcall_uhgs_id and t.departure_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
-- 22970
       
update navigoviz.raw_flows_fix t set distance_homeport_dep_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                    SELECT t.travel_id, t.homeport, t.homeport_uhgs_id, t.departure, t.departure_uhgs_id,   stages__length_miles
                    FROM navigoviz.raw_flows_fix t , navigo.stages s 
                    where stages__length_miles is not null and stages__length_miles != '0.0' and t.homeport_uhgs_id = s.pointcall_uhgs_id and t.departure_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
-- 6976
 
update navigoviz.raw_flows_fix t set distance_homeport_dep_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.homeport, t.homeport_uhgs_id, t.departure, t.departure_uhgs_id, stages__length_miles
                   FROM navigoviz.raw_flows_fix t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.departure_uhgs_id = s.pointcall_uhgs_id and t.homeport_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
-- 18134
       
alter table navigoviz.raw_flows_fix add column nb_cargo int default 0;
update navigoviz.raw_flows_fix set nb_cargo  = jsonb_array_length(all_cargos);
-- 55975

--- Correction reportée sur le serveur le 02 nov 2022

-- Fait aussi en local le 10 nov 2022
alter table  navigoviz.raw_flows rename to bug_raw_flows;
alter table  navigoviz.raw_flows_fix rename to raw_flows;

----------------------------------------------------------------------------
-- Pourquoi le style dashed est faux. 
----------------------------------------------------------------------------

update navigoviz.uncertainity_travels set direct = true where departure_uncertainity = 0; --si G5
-- not(t.direct) as style_dashed

select * from navigoviz.built_travels  where ship_id like '%14820N' and source_entry != 'both-to' order by travel_rank ;



select count(*), departure_uncertainity, ut.departure_navstatus  , ut.departure_function , ut.source_depart 
from navigoviz.uncertainity_travels ut
where ut.departure_navstatus like 'PC%' 
group by departure_uncertainity, ut.departure_navstatus  , ut.departure_function , ut.source_depart
order by source_depart ;

select * from navigoviz.uncertainity_travels where ship_id = '0004155N'
-- la plupart des PC-RS de Registre du petit cabotage (1786-1787) sont à 0
37	-4	PC-RS	A	Registre du petit cabotage (1786-1787)
-- la plupart des PC-RS de la Santé registre de patentes de Marseille sont à 0
11	-4	PC-RS		la Santé registre de patentes de Marseille
3	-4	PC-RS	A	la Santé registre de patentes de Marseille
-- certains cas PC-RF/RC de G5 ne sont pas à 0
795	-1	PC-RF	A	G5
1	-4	PC-RF	A	G5
175	-1	PC-RF		G5
1	-4	PC-RC	A	G5
28	-1	PC-RC	A	G5

select count(*), 
(case when position('=' in coalesce( ut.departure_out_date , ut.destination_in_date)) > 0 then true else false end) as date_precise,
departure_uncertainity, ut.departure_navstatus  , ut.departure_function , ut.source_suite 
from navigoviz.built_travels ut
where ut.departure_navstatus like 'PC%' and departure_uncertainity!= 0  and source_entry !='both-to' and departure_uncertainity=-2
group by (case when position('=' in coalesce( ut.departure_out_date , ut.destination_in_date)) > 0 then true else false end),
	departure_uncertainity, ut.departure_navstatus  , ut.departure_function , ut.source_suite
order by source_suite ;
-- FINIR L'ANALYSE ICI

select * from navigoviz.pointcall p 
where p.navigo_status = 'PC-RS' and p.source_suite = 'Registre du petit cabotage (1786-1787)'
and pointcall_uncertainity = -4 and pointcall_function = 'A' and pointcall_uhgs_id !='H4444444';--H9999999

select * from navigoviz.pointcall p 
where p.navigo_status = 'PC-RS' and p.source_suite = 'la Santé registre de patentes de Marseille'
and pointcall_uncertainity = -4  and pointcall_uhgs_id !='H4444444';--H9999999

select * from navigoviz.pointcall p 
where p.navigo_status = 'PC-RS' and p.source_suite = 'G5'
and pointcall_uncertainity = -4  and pointcall_uhgs_id !='H4444444';--H9999999


select * from navigoviz.pointcall p where p.source_doc_id = '00357427';

select distinct p2.pointcall_function, p2.navigo_status , p2.pointcall_action, p2.pointcall_in_date  
from navigoviz.pointcall p1 , navigoviz.pointcall p2 
where p1.source_suite = 'la Santé registre de patentes de Marseille' 
and p2.source_suite = 'la Santé registre de patentes de Marseille' 
and p1.source_doc_id = p2.source_doc_id 
and p1.pointcall_function = 'O'
and substring(p2.pointcall_rank_dedieu , 0, strpos( p2.pointcall_rank_dedieu, '.')) ::int  > substring(p1.pointcall_rank_dedieu , 0, strpos( p1.pointcall_rank_dedieu, '.'))::int ;
-- quasi tous de type T	FC-RS	In 1789>10>20!
-- SAUF PC-RS	In	1787=12=28 (mais function = null)

select distinct p2.pointcall_function, p2.navigo_status , p2.pointcall_action, p2.pointcall_in_date  
from navigoviz.pointcall p1 , navigoviz.pointcall p2 
where p1.source_suite = 'la Santé registre de patentes de Marseille' 
and p2.source_suite = 'la Santé registre de patentes de Marseille' 
and p1.source_doc_id = p2.source_doc_id 
and p1.pointcall_function = 'O'
and substring(p2.pointcall_rank_dedieu , 0, strpos( p2.pointcall_rank_dedieu, '.')) ::int  < substring(p1.pointcall_rank_dedieu , 0, strpos( p1.pointcall_rank_dedieu, '.'))::int ;


-- where p.source_suite = 'la Santé registre de patentes de Marseille' and pointcall_function = 'O';

-- tous les trajets arrivants à Marseille sont direct
update navigoviz.uncertainity_travels set direct = true 
where  source_depart in ('Registre du petit cabotage (1786-1787)', 'la Santé registre de patentes de Marseille')
and departure_navstatus = 'PC-RS';

-- en prod, sur travels, le 23 nov : 
-- attention, source_suite = Santé Marseille et pas la Santé registre de patentes de Marseille comme dans Pointcall
update navigoviz.built_travels  set style_dashed = false 
where  source_suite in ('Registre du petit cabotage (1786-1787)', 'Santé Marseille')
and departure_navstatus = 'PC-RS';
-- 11440 Registre du petit cabotage (1786-1787)

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels  set style_dashed = false 
where  source_suite in ('Santé Marseille')
and departure_navstatus = 'PC-RS';
-- 16755

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels set departure_uncertainity = 0
where departure_uncertainity = -2 and source_suite = 'Santé Marseille' and source_entry != 'both-to'
and departure_navstatus = 'PC-RS'
and position('=' in coalesce( departure_out_date , destination_in_date)) > 0;
-- 3654 / Il en restera 13 avec date pas sure à analyser/comprendre

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels set departure_uncertainity = 0
where departure_uncertainity = -2 and source_suite = 'Registre du petit cabotage (1786-1787)' and source_entry != 'both-to'
and departure_navstatus = 'PC-RS';
-- 3034

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels 
set uncertainty_color = 'green', travel_uncertainity = 0 
where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1);
-- -- en prod, sur travels, le 23 nov : 



-- certains les trajets du G5 disant "venir de" sont des directs 
/*update navigoviz.uncertainity_travels set direct = true 
where 
p.source_suite in ('G5')
and ut.departure_navstatus in ('PC-RF', 'PC-RC')*/

----------------------------------------------------------------------------
-- 
select * from navigoviz."source" s where s.subset like 'Expéditions%';
-- Expéditions "coloniales" Marseille (1789)
-- Amis Vieux Toulon, MA_11/108
select * from navigoviz."source" s where s.subset like 'Registre du petit cabotage (1786-1787)';
select data_block_local_id , documentary_unit_id , source, component_description__component_short_title  from navigo.pointcall p 
-- "Congé" registers of the French Admiralty - XVIIIth century

select
select * from navigo.pointcall p
where component_description__component_short_title like '%registers of the French Admiralty%';


select distinct source_entry from navigoviz.raw_flows rf ;

------------------------------------------------------------------------------
-- 10 /11/2022 : travail avec Alan pour préparer les cartes pour les Sables
------------------------------------------------------------------------------

-- Depuis les Sables, où vont les navires
-- bleu
select count(*) as c, destination_fr, destination_point 
from navigoviz.raw_flows
where departure_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
and source_entry != 'both-to'
group by destination_point, destination_fr
order by c desc;


-- Depuis les Sables, où vont les navires sablais (homeport= Amirauté des Sables)
-- rose
select count(*) as c, destination_fr, destination_point 
from navigoviz.raw_flows r
where departure_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
-- and r.homeport_uhgs_id = 'A0137148'
and r.homeport_admiralty= 'Sables-d’Olonne'
group by destination_point, destination_fr
order by c desc;

select destination_fr, homeport_admiralty, *
from navigoviz.raw_flows r
where departure_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
order by destination_fr

-- triangle : les navires qui vont aux sables

-- D'où viennent les navires qui vont au Sables
-- triangle
select count(*) as c, departure_fr , departure_point 
from navigoviz.raw_flows r
where destination_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
-- and r.homeport_uhgs_id = 'A0137148'
-- and r.homeport_admiralty= 'Sables-d’Olonne'
group by departure_point, departure_fr
order by c desc;

-- ceux qui vont aux sables, et qui sont des sablais
select count(*) as c, departure_fr , departure_point 
from navigoviz.raw_flows r
where destination_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
-- and r.homeport_uhgs_id = 'A0137148'
and r.homeport_admiralty= 'Sables-d’Olonne'
group by departure_point, departure_fr
order by c desc;

-- Cercle proportionnels aux nombres de départs, couleur proportionnelle à pourcent, la part des sablais
select total, coalesce(c*1.0/total * 100, 0) as pourcent, tout.departure_fr , tout.departure_point  from 
	(select count(*) as total, departure_fr , departure_point 
	from navigoviz.raw_flows r
	where destination_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
	-- and r.homeport_uhgs_id = 'A0137148'
	-- and r.homeport_admiralty= 'Sables-d’Olonne'
	group by departure_point, departure_fr
	) as tout left join 

	-- ceux qui vont aux sables, et qui sont des sables
	(select count(*) as c, departure_fr , departure_point 
	from navigoviz.raw_flows r
	where destination_uhgs_id= 'A0137148' and extract (year from outdate_fixed) = 1787
	-- and r.homeport_uhgs_id = 'A0137148'
	and r.homeport_admiralty= 'Sables-d’Olonne'
	group by departure_point, departure_fr
	) as sablais

on tout.departure_fr = sablais.departure_fr
order by pourcent;

--------------------------------------------------------------------------------------------------
-- jacques BOUCARD, Décembre 2022
------------------------------------------------------

select count(*), departure_fr, extract(year from rf.outdate_fixed) from navigoviz.raw_flows rf 
where 
departure_fr in ('Saint-Martin-de-Ré', 'Ars-en-Ré', 'La Flotte-en-Ré', 'île de Ré', 'La Couarde', 'La Flotte')
--or departure_fr = 'Loix'
and source_entry != 'both-to'
and (commodity_standardized = 'Salt' or commodity_standardized2='Salt' or commodity_standardized3='Salt' or commodity_standardized4='Salt')
group by departure_fr, extract(year from rf.outdate_fixed);

1787 24 passage du STRO (5488 tonneaux de jauge)
1789 26 passage du STRO de St Martin 

59	Saint-Martin-de-Ré	1787.0
49	Saint-Martin-de-Ré	1789.0


select count(*), departure_fr, extract(year from rf.outdate_fixed)
from navigoviz.raw_flows rf 
where 
departure_fr in ('Saint-Martin-de-Ré', 'Ars-en-Ré', 'La Flotte-en-Ré', 'île de Ré', 'La Couarde', 'La Flotte')
--or departure_fr = 'Loix'
and source_entry != 'both-to'
and (commodity_standardized = 'Salt' or commodity_standardized2='Salt' or commodity_standardized3='Salt' or commodity_standardized4='Salt')
and rf.destination_shiparea like 'BAL%'
group by departure_fr, extract(year from rf.outdate_fixed);

-- Saint-Martin-de-Ré en 1789

-- extraction pour Jacques Boucard le 24 nov 2022
select rf.departure_pkid , departure_rank_dedieu, ship_name, ship_id, outdate_fixed, extract(year from rf.outdate_fixed), departure_fr, destination_fr, captain_name, homeport_toponyme_fr , tonnage, commodity_standardized, commodity_standardized2, commodity_standardized3, commodity_standardized4
from navigoviz.raw_flows rf 
where 
departure_fr in ('Saint-Martin-de-Ré', 'Ars-en-Ré', 'La Flotte-en-Ré', 'île de Ré', 'La Couarde', 'La Flotte')
--or departure_fr = 'Loix'
and source_entry != 'both-to'
--and (commodity_standardized = 'Salt' or commodity_standardized2='Salt' or commodity_standardized3='Salt' or commodity_standardized4='Salt')
and rf.destination_shiparea like 'BAL%'
--and departure_fr = 'Saint-Martin-de-Ré' and 
-- and extract(year from rf.outdate_fixed) = 1789
-- and destination_fr not in ('Kristiansand', 'Tønsberg')
and rf.destination_shiparea not in ('BAL-SKAG')
order by extract(year from rf.outdate_fixed), destination_fr;
-- group by departure_fr, extract(year from rf.outdate_fixed);
-- , 'BAL-BELT'


select * from portic_v7.ports.port_points  p where p.toponyme ilike 'loix';
A0141676 ; Loix
select * from portic_v7.ports.port_points  p where p.toponyme ilike '%ré%';

select * from portic_v7.ports.port_points  p where p.toponyme ilike '%Riga%';
--BAL-RIGA
select * from portic_v7.ports.port_points  p where p.toponyme ilike '%Ribérou%';
select * from portic_v7.ports.port_points  p where p.toponyme ilike '%Oléron%';

-- extraction pour Jacques Boucard le 12 dec 2022
--La Rochelle
--Marennes
--Ribérou : rien
--Le Château d'Oléron : rien

select rf.departure_pkid , departure_rank_dedieu, ship_name, ship_id, outdate_fixed, 
extract(year from rf.outdate_fixed), departure_fr, destination_fr, captain_name, homeport_toponyme_fr , tonnage, 
commodity_standardized, commodity_standardized2, commodity_standardized3, commodity_standardized4,
commodity_purpose, commodity_purpose2, commodity_purpose3, commodity_purpose4
from navigoviz.raw_flows rf 
where 
departure_fr in ('La Rochelle', 'Marennes', 'Ribérou', 'Le Château-d''Oléron')
--or departure_fr = 'Loix'
and source_entry != 'both-to'
--and (commodity_standardized = 'Salt' or commodity_standardized2='Salt' or commodity_standardized3='Salt' or commodity_standardized4='Salt')
and rf.destination_shiparea like 'BAL%'
--and departure_fr = 'Saint-Martin-de-Ré' and 
-- and extract(year from rf.outdate_fixed) = 1789
-- and destination_fr not in ('Kristiansand', 'Tønsberg')
and rf.destination_shiparea not in ('BAL-SKAG')
order by departure_fr, extract(year from rf.outdate_fixed), destination_fr;

-- extraction pour Jacques Boucard le 23 dec 2022

select rf.departure_pkid , departure_rank_dedieu, ship_name, ship_id, outdate_fixed, 
extract(year from rf.outdate_fixed), departure_fr, destination_fr, captain_name, homeport_toponyme_fr , tonnage,
commodity_standardized, commodity_standardized2, commodity_standardized3, commodity_standardized4,
commodity_purpose, commodity_purpose2, commodity_purpose3, commodity_purpose4
from navigoviz.raw_flows rf 
where 
departure_province in ('Aunis', 'Saintonge')
-- departure_fr in ('La Rochelle', 'Marennes', 'Ribérou', 'Le Château-d''Oléron')
--or departure_fr = 'Loix'
and source_entry != 'both-to'
--and (commodity_standardized = 'Salt' or commodity_standardized2='Salt' or commodity_standardized3='Salt' or commodity_standardized4='Salt')
and (rf.destination_shiparea like 'BAL%' )
--or rf.destination_uhgs_id in ('A0973559', 'A0676229', 'A1017571', 'A0921408'))
--and departure_fr = 'Saint-Martin-de-Ré' and 
-- and extract(year from rf.outdate_fixed) = 1789
-- and destination_fr not in ('Kristiansand', 'Tønsberg')
and rf.destination_shiparea not in ('BAL-SKAG')
order by departure_fr, extract(year from rf.outdate_fixed), destination_fr;

-- en plus : Rochefort Tonnay-Charente
-- total : 69 entrées

select country2019_name , uhgs_id , toponyme_standard_fr , * 
from ports.port_points pp where pp.shiparea is null
order by country2019_name;
-- Denmark  A0821280 Halbourg --BAL-BELT mais passe pas le Sund
Estonia  A0973559 Rével -- BAL-RIGA
Finland	A0921408 Finlande, undeterminé -- BAL-BOTS
-- Germany	A0799311	Papenbourg -- NOR-GBIG
Germany	A0676229	Mecklenbourg --BAL-BASE
Latvia	A1017571	Courlande --BAL-BASE
-- Netherlands	A1968907	Arum -- NOR-GBIG
-- Netherlands	A1968861	IJist -- NOR-GBIG
-----------------------------------------------------------------------------------------
-- Mail du 30 /11/2022 de Silvia
-----------------------------------------------------------------------------------------

congés du G5 pour l'année 1787
tonnage agrégé (tout pavillon confondu) destiné pour l'étranger HORS colonies (prendre par sécurité si possible la seule destination finale, si jamais il y a deux destination indiquées), par pays de destination

select * from raw_flows rf 
where source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
-- 30854

select  destination_state_1789_fr, destination_substate_1789_fr, count(*), sum(tonnage::float) 
from raw_flows rf 
where source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
and destination_state_1789_fr != 'France'
group by destination_state_1789_fr, rf.destination_substate_1789_fr  ;

select source_doc_id , max(rf.departure_rank_dedieu)
from raw_flows rf 
where source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
group by source_doc_id
-- 29891

select source_doc_id , max(rf.departure_rank_dedieu) , max(rf.destination_rank_dedieu)
from raw_flows rf 
where source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
group by source_doc_id
having max(destination_rank_dedieu) > 2;

-- supprimer les entrées faisant double compte

select  destination_state_1789_fr, destination_substate_1789_fr, count(*), sum(tonnage::float) 
from 
	raw_flows rf , 
	(select source_doc_id , max(rf.destination_rank_dedieu) as dernier_rang
	from raw_flows rf 
	where source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
	group by source_doc_id) as k 
where source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
	and destination_state_1789_fr != 'France'
	and rf.source_doc_id = k.source_doc_id and rf.destination_rank_dedieu = k.dernier_rang
group by destination_state_1789_fr, rf.destination_substate_1789_fr  ;


select source_doc_id , departure , destination , destination_rank_dedieu, destination_navstatus 
from 
	raw_flows rf 
where 
source_suite = 'G5' and source_entry != 'both-to' and extract(year from rf.outdate_fixed) = 1787
and destination_state_1789_fr != 'France'
and
destination_state_1789_fr = 'Grande-Bretagne' and destination_substate_1789_fr = 'possessions anglaises en Méditerranée'
-- destination_state_1789_fr = 'Royaume de Naples' and destination_substate_1789_fr = 'Sicile';

00170077	Collioure	Gibraltar	2	FC-RF -- ok à supprimer car escale intermédiaire avant Italie
00170155	Italie	Sicile	3	FC-RF -- ok à supprimer car escale intermédiaire avant Cote d' Espagne 

select source_doc_id, departure_rank_dedieu, departure , destination, destination_navstatus, departure_out_date , destination_function , destination_action 
from raw_flows rf 
where source_doc_id in ('00170155', '00170077') and source_entry != 'both-to' 
order by source_doc_id, departure_rank_dedieu ;
-- ok


select count(*) from navigoviz.raw_flows r
where 
homeport_province = 'Poitou' and
-- departure_province = 'Poitou' and
 departure_state_1789_fr = 'France'
--and 
and not (departure_substate_1789_fr  is not null )
and extract(year from outdate_fixed ) = 1787
and source_suite  = 'G5';

-- 1549 / 40280
-- 2131 / 40280
select 1135 / 30513.0 *100
-- 3.7 %
select 923 / 30513.0 *100
-- 3.02

-- 30513 total de sorties hors colonies en 1787 (G5)
-- 1135 départs du Poitou
-- 923 départ des gens de Poitou

--------------------------------------------------------------------------
-- Classement des provinces pour Alan Chambaud
--------------------------------------------------------------------------


select count(*) as c, departure_province from navigoviz.raw_flows r
where 
-- homeport_province = 'Poitou' and
-- departure_province = 'Poitou' and
 departure_state_1789_fr = 'France'
--and 
and not (departure_substate_1789_fr  is not null )
and extract(year from outdate_fixed ) = 1787
and source_suite  = 'G5'
group by departure_province 
order by c;

/* 
377	Corse
570	Provence
1135	Poitou
2196	Flandre
2742	Saintonge
2948	Guyenne
3182	Picardie
3317	Aunis
4832	Bretagne
8960	Normandie
*/

-----------------------------------------------------------------------------------------------------------------
-- Discussion avec Silvia le 16/12 a propos des cargos
-----------------------------------------------------------------------------------------------------------------

select * from navigoviz.raw_flows rf  where departure_pkid =29693


select * from navigoviz.raw_flows rf  
where departure_fr = 'Messine' and destination_fr = 'Marseille' and source_suite != 'G5' and cargo_item_action = 'In'
00304793

select * from navigoviz.raw_flows rf 
where source_doc_id = '00304793'

select departure_fr, destination_fr, commodity_standardized_fr ,cargo_item_action  from navigoviz.raw_flows rf 
where source_doc_id = '00305305'
order by destination_rank_dedieu;

select departure_fr, destination_fr, commodity_standardized_fr ,cargo_item_action  from navigoviz.raw_flows rf 
where source_doc_id = '00141308'
order by destination_rank_dedieu;



select outdate_fixed , pointcall , all_cargos ,
commodity_purpose, cargo_item_action ,   
p.commodity_purpose2 , cargo_item_action2 , 
commodity_purpose3, cargo_item_action3 , 
commodity_purpose4, cargo_item_action4 , *
from navigoviz.pointcall p 
where source_doc_id = '00310196'

[{"quantity": null, "quantity_u": null, "commodity_id": "00000013", "cargo_item_action": "In", "commodity_purpose": "marchandises", "link_to_pointcall": "00310196", "commodity_standardized_en": "Sundries", "commodity_standardized_fr": "Diverses marchandises", "commodity_permanent_coding": "xx-xxxxxx-JZ-MAxxxx-xxxx"}, 
{"quantity": "1.0", "quantity_u": "partie", "commodity_id": "00000013", "cargo_item_action": "In", "commodity_purpose": "marchandises", "link_to_pointcall": "00310196", "commodity_standardized_en": "Sundries", "commodity_standardized_fr": "Diverses marchandises", "commodity_permanent_coding": "xx-xxxxxx-JZ-MAxxxx-xxxx"}, 
{"quantity": null, "quantity_u": null, "commodity_id": "00000098", "cargo_item_action": "In", "commodity_purpose": "chanvre", "link_to_pointcall": "00310196", "commodity_standardized_en": "Hemp", "commodity_standardized_fr": "Chanvre", "commodity_permanent_coding": "ID-GTHxxx-xx-xxxxxx-RTxx"}, 
{"quantity": "249.0", "quantity_u": "sacs", "commodity_id": "00000123", "cargo_item_action": "In", "commodity_purpose": "riz", "link_to_pointcall": "00310196", "commodity_standardized_en": null, "commodity_standardized_fr": null, "commodity_permanent_coding": "ID-BLxxxx-xx-xxxxxx-FBxx"}, 
{"quantity": "9.0", "quantity_u": null, "commodity_id": "00000002", "cargo_item_action": "In", "commodity_purpose": "passagers", "link_to_pointcall": "00310196", "commodity_standardized_en": "Passenger", "commodity_standardized_fr": "Passagers", "commodity_permanent_coding": "IY-AAxxxx-xx-xxxxxx-xxxx"}, 
{"quantity": null, "quantity_u": null, "commodity_id": "00000040", "cargo_item_action": "In", "commodity_purpose": "marchandises", "link_to_pointcall": "00310196", "commodity_standardized_en": "Goods", "commodity_standardized_fr": "Marchandises", "commodity_permanent_coding": null}]

select * from navigoviz.pointcall p where p.record_id = '00305308';
-- Toulon
select * from navigoviz.pointcall p where p.record_id = '00141308';
-- mesquer
select * from navigoviz.pointcall p where p.record_id = '00310199';
-- Genes

select cargo_item_action, cargo_item_sending_place, first_point__pointcall_name,	first_point__pointcall_uhgs_id
from 
navigo.cargo c 
where cargo_item_sending_place = first_point__pointcall_name; 
-- 748

select cargo_item_action, cargo_item_sending_place, first_point__pointcall_name,	first_point__pointcall_uhgs_id
from 
navigo.cargo c 
where cargo_item_sending_place is null ; 
-- 57325

select cargo_item_action, count(*)
from 
navigo.cargo c 
where cargo_item_sending_place = first_point__pointcall_name
group by cargo_item_action;

-- On peut s'appuyer sur link_to_pointcall
select  count(*)
from 
navigo.cargo c 
where c.link_to_pointcall is not null and length(link_to_pointcall) > 0; -- 58818

select  count(*)
from 
navigo.cargo c 
where c.pointcall__data_block_local_id  is not null; -- 58818

select  count(*)
from 
navigo.cargo c 
where c.pointcall__record_id  is not null and pointcall__record_id != ''; -- 58818

select  count(*)
from 
navigo.cargo c 
where c.pointcall__record_id  != link_to_pointcall; -- 0


select departure_fr , destination_fr , departure_action , departure_function , departure_navstatus  , departure_out_date ,
cargo_item_action , commodity_purpose 
from navigoviz.built_travels bt where ship_id = '0003593N' and source_entry != 'both-to';
-- pourquoi uncertainty = -4 sur départ et arrivée ?


select outdate_fixed , pointcall , all_cargos ,
commodity_purpose, cargo_item_action ,   
p.commodity_purpose2 , cargo_item_action2 , 
commodity_purpose3, cargo_item_action3 , 
commodity_purpose4, cargo_item_action4 , *
from navigoviz.pointcall p 
where source_doc_id = '00182772'
order by pointcall_rank_dedieu ;
-- Carthagènes --> Marseille

select outdate_fixed , pointcall , pointcall_function , pointcall_action , 
all_cargos ,
commodity_purpose, cargo_item_action ,   
p.commodity_purpose2 , cargo_item_action2 , 
commodity_purpose3, cargo_item_action3 , 
commodity_purpose4, cargo_item_action4 , *
from navigoviz.pointcall p 
where source_doc_id = '00149602'
order by pointcall_rank_dedieu ;
-- Vannes --> La Flotte en Ré --> dans le Pertuis

select * from navigoviz.pointcall p where p.record_id = '00149602';
-- La Flotte en Ré

select outdate_fixed , pointcall , pointcall_function , pointcall_action , 
all_cargos ,
commodity_purpose, cargo_item_action ,   
p.commodity_purpose2 , cargo_item_action2 , 
commodity_purpose3, cargo_item_action3 , 
commodity_purpose4, cargo_item_action4 , *
from navigoviz.pointcall p 
where source_doc_id = '00110062'
order by pointcall_rank_dedieu ;

select * from navigoviz.pointcall p where p.record_id = '00110062';
-- Dunkerque

select outdate_fixed , pointcall , pointcall_function , pointcall_action , 
all_cargos ,
commodity_purpose, cargo_item_action ,   
p.commodity_purpose2 , cargo_item_action2 , 
commodity_purpose3, cargo_item_action3 , 
commodity_purpose4, cargo_item_action4 , *
from navigoviz.pointcall p 
where source_doc_id = '00331406'
order by pointcall_rank_dedieu ;

-------------------------------------------------------------------------------------
-- Vérif pour l'article H&M ; mail de Thierry du 2 janvier 2023
-------------------------------------------------------------------------------------

select ship_id, outdate_fixed, destination_fr ,  homeport_toponyme_fr 
from navigoviz.raw_flows rf 
where 
rf.departure_fr = 'La Rochelle'
and rf.destination_province in ('Poitou', 'Aunis', 'Saintonge')
and extract(year from rf.outdate_fixed) = 1787
and rf.homeport_province = 'Poitou'
order by ship_id;
-- 32 navires dont 10 0000882N

select ship_id, outdate_fixed, destination_fr , homeport_toponyme_fr, homeport_province, homeport_state_1789_fr  from navigoviz.built_travels rf  
where source_entry !='both-to'
and rf.departure_fr = 'La Rochelle'
and rf.destination_province in ('Poitou', 'Aunis', 'Saintonge')
and extract(year from rf.outdate_fixed) = 1787
and rf.homeport_province = 'Poitou'  
order by ship_id;
-- 39 navires  dont 9 0000882N
-- 0004144N (3+); 0004149N(3=); 0004286N (3+)
-- 0004292N (2=)
-- 0007498N (2) ? reconstitution car out vers la rochelle dans les sources.
-- 0010464N (3++)
select * from navigo.pointcall p where p.ship_id = '0007498N' --Ursulle/Aimable Ursule et Longue Epée, Charles François

homeport;homeport_admiralty;homeport_province;homeport_states;homeport_uncertainity;
departure;departure_admiralty;departure_province;departure_states;
destination;destination_admiralty;destination_province;destination_states;
distance_dep_dest;distance_dep_dest_miles;flag;tonnage;tonnage_class;outdate_fixed

select homeport_toponyme_fr as homeport,	homeport_admiralty,	homeport_province,	homeport_states,	homeport_uncertainity,
departure_fr as departure,	departure_admiralty,	departure_province,	departure_states,
destination_fr as destination,	destination_admiralty,	destination_province,	destination_states,	
distance_dep_dest,	distance_dep_dest_miles,	flag,	tonnage,	tonnage_class,	outdate_fixed
from navigoviz.raw_flows rf 
where source_suite = 'G5' and extract(year from rf.outdate_fixed) = 1787;
-- extract g5data.csv sur portic_V7

select homeport_toponyme_fr as homeport,	homeport_admiralty,	homeport_province, pp.belonging_states as	homeport_states,	homeport_uncertainity,
departure_fr as departure,	departure_admiralty,	departure_province,	departure_states,
destination_fr as destination,	destination_admiralty,	destination_province,	destination_states,	
rf.orthodromic_distance_dep_dest_km as distance_dep_dest,	orthodromic_distance_dep_dest_km as distance_dep_dest,	flag,	tonnage,	tonnage_class,	outdate_fixed
from navigoviz.raw_flows rf left join ports.port_points pp on rf.homeport_uhgs_id = pp.uhgs_id 
where source_suite = 'G5' and extract(year from rf.outdate_fixed) = 1787;
-- extract g5data.csv sur portic_V8

id;departure;departure_admiralty;departure_province;departure_states;
indate_fixed;flag;class;birthplace;commodity_standardized;
distance_dep_dest_miles;in_crew;tonnage_class

select travel_id, departure_fr as departure,	departure_admiralty,	departure_province,	departure_states, 
indate_fixed, ship_flag_standardized_fr as flag, ship_class_standardized as class, birthplace, 
(all_cargos::json->>0)::json->>'commodity_standardized_fr' as commodity_standardized,
distance_dep_dest_miles, in_crew , tonnage_class 
from navigoviz.raw_flows rf
where source_suite != 'G5' and extract(year from rf.outdate_fixed) = 1787;
-- extract marseilledata.csv sur portic_V8

select travel_id, departure_fr as departure,	departure_admiralty,	departure_province,	departure_states, 
indate_fixed, ship_flag_standardized_fr as flag,  class, birthplace, 
(all_cargos::json->>0)::json->>'commodity_standardized_fr' as commodity_standardized,
distance_dep_dest_miles, in_crew , tonnage_class 
from navigoviz.raw_flows rf
where source_suite != 'G5' and extract(year from rf.outdate_fixed) = 1787;
-- extract marseilledata.csv sur portic_V7

select departure_fr , departure_admiralty , extract(year from rf.outdate_fixed), count(*)
from navigoviz.raw_flows rf 
where source_suite = 'G5' 
and rf.departure_province in ('Poitou', 'Aunis', 'Saintonge')
and rf.departure_function = 'O'
group by departure_admiralty, departure_fr , extract(year from rf.outdate_fixed) ;


select oblique, status , has_a_clerk , amiraute, toponyme_standard_fr 
from ports.port_points pp 
where pp.province in ('Poitou', 'Aunis', 'Saintonge')
and has_a_clerk is true
order by amiraute , toponyme_standard_fr;
-- Meschers n'est pas oblique

select p.toponyme_fr , pointcall_admiralty , extract(year from p.outdate_fixed), count(*)
from navigoviz.pointcall p
where source_suite = 'G5' 
and p.pointcall_province in ('Poitou', 'Aunis', 'Saintonge')
and p.pointcall_function = 'O'
and extract(year from p.outdate_fixed) = 1787
group by p.pointcall_admiralty, p.toponyme_fr , extract(year from p.outdate_fixed) ;

select * from navigoviz.pointcall p
where extract(year from p.outdate_fixed) = 1787
and toponyme_fr = 'Meschers';

-- source : 00153230 /ANF, G5-145/422
select * from navigoviz.pointcall p where source_doc_id = '00153230' order by outdate_fixed ;

---
-- correction de bureaux de ferme le 3 janvier 2023 : V7 local et server
update ports.port_points set ferme_bureau = 'Oléron' where uhgs_id = 'A1964982' and toponyme_standard_fr = 'La Perrotine';
update ports.port_points set ferme_bureau = 'Oléron' where uhgs_id = 'A0159385' and toponyme_standard_fr = 'Saint-Trojan';
update ports.port_points set ferme_bureau = 'Oléron' where uhgs_id = 'A0215022' and toponyme_standard_fr = 'Pointe d'' Ors';
update ports.port_points set ferme_bureau = 'La Rochelle' where uhgs_id = 'A0152629' and toponyme_standard_fr = 'L''Houmée';
update ports.port_points set ferme_bureau = 'La Rochelle' where uhgs_id = 'A0137297' and toponyme_standard_fr = 'Marselleq';
select uhgs_id , toponyme_standard_fr  from ports.port_points where ferme_bureau = 'Alligre' ;
--A0144350	Charron
--A0219226	pertuis Breton
--A0214663	Le Brault
--A1964694	Marans
update ports.port_points set ferme_bureau = 'Aligre' where ferme_bureau = 'Alligre' ;

-- correction coquille sur amirauté de La Chaume le 3 janvier 2023
update ports.port_points set amiraute = 'Sables-d’Olonne' where toponyme_standard_fr = 'La Chaume' and uhgs_id = 'A0166852';

-- modif annulée
/*
update ports.port_points set ferme_bureau = 'Marennes' where toponyme_standard_fr = 'Royan';
update ports.port_points set ferme_bureau = 'Marennes' where toponyme_standard_fr = 'Mortagne';
update ports.port_points set ferme_bureau = 'Marennes' where toponyme_standard_fr = 'Ribérou';
update ports.port_points set ferme_bureau = 'Alligre' where toponyme_standard_fr = 'Charron';
update ports.port_points set ferme_bureau = 'Charente' where toponyme_standard_fr = 'Soubise';
update ports.port_points set ferme_bureau = 'Marennes' where toponyme_standard_fr = 'Brouage' or uhgs_id = 'A0189364';
update ports.port_points set ferme_bureau = 'Charente' where toponyme_standard_fr = 'Tonnay-Charente';
*/

