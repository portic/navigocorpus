--------------------------------------------------------------------------------------------------
-- Traitement pour portic_v6
-- Christine PLUMEJEAUD-PERREAU, U.M.R. LIENSS 7266 
-- ANR PORTIC 
-- Le 04 février 2021
--------------------------------------------------------------------------------------------------

-- 1 . Les ports : reprendre processing_ports_v2.sql
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\archives_sql\processing_ports_v2.sql

-- 2. les sources

drop table navigoviz."source" 
select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1
data_block_local_id:  00149228
data_block_local_id:  00335413

data_block_local_id:  00353709
data_block_local_id:  00310213
data_block_local_id:  00149228
data_block_local_id:  00335413

Fixes : delete from navigoviz.source where data_block_local_id = '00149228' and source = 'ANF, G5-62/rafraichi'    
delete from navigoviz.source where data_block_local_id = '00335413' and source = 'ANF, G5-76/5854 et 5860'    
00335413

select documentary_unit_id, data_block_local_id, source from navigoviz."source" s  where data_block_local_id = '00335413'
ANF, G5-76/5854 et 5860 -- supprimer
ANF, G5-76/5854

select documentary_unit_id, data_block_local_id, source from navigoviz."source" s  where data_block_local_id = '00310213'
ADBdR, 200E, 545/
ADBdR, 200E, 545/0283 -- supprimer
select * from navigo.cargo p  where pointcall__data_block_local_id = '00310213'
select * from navigo.taxes t  where link_to_pointcall = '00310213'
select * from navigo.taxes t  where  source = 'ADBdR, 200E, 545/'
select * from navigo.taxes t  where  source = 'ADBdR, 200E, 545/0283'

select documentary_unit_id, data_block_local_id, source from navigoviz."source" s  where data_block_local_id = '00310213'


select * from navigo.taxes t  where  source = 'ANF, G5-76/5854'
select * from navigo.taxes t  where  source = 'ANF, G5-76/5854 et 5860'

select record_id, documentary_unit_id , * from navigo.pointcall p  where data_block_local_id = '00353709'
ADBdR, 200E, 545/0757
ADBdR, 200E, 545/
select * from navigo.taxes t  where  source = 'ADBdR, 200E, 545/'
select * from navigo.taxes t  where  source = 'ADBdR, 200E, 545/0757' -- supprimer
select * from navigo.cargo p  where pointcall__data_block_local_id = '00353709'
select * from navigoviz."source" s where data_block_local_id='00305161'

-- Faire le lien entre source et cargo
select * from navigo.cargo p  where pointcall__data_block_local_id = '00335413'
-- ok, par pointcall__data_block_local_id

select * from navigo.cargo t where pointcall__data_block_local_id not in (select data_block_local_id from navigoviz."source" s)
-- 0 (ok)
select count(*) from navigo.cargo
-- 58804

select * from navigo.taxes t where link_to_pointcall = '00335413'

SELECT * FROM crosstab(
select * from navigo.taxes t where link_to_pointcall = '00335413'
) AS t (user_id int, user_name text, email1 text, email2 text, email3 text);

/*
pointcall__source = 'ANF, G5-76/5854 et 5860'
cargo__source = 'ANF, G5-76/5854'
taxe record_id : 
				00069975
				00086339
*/

-- TOTAL de taxes : 74 251
-- Faire le lien entre source et taxes
select * from navigo.taxes t where pointcall__data_block_local_id not in (select data_block_local_id from navigoviz."source" s)
-- 0 ok
select count(*)  from navigo.taxes t
-- 70782

drop view navigocheck.taxes_complete

select * from navigo.taxes t where link_to_pointcall not in (select data_block_local_id from navigoviz."source" s)


select * from navigo.taxes t where link_to_pointcall not in (select data_block_local_id from navigoviz."source" s)
-- 2511
select * from navigo.taxes t where pointcall__documentary_unit_id is not null and pointcall__documentary_unit_id not in (select documentary_unit_id from navigoviz."source" s)
-- 0 : tous ceux non null sont alignés

select distinct * from 
(
select * from navigo.taxes t where pointcall__documentary_unit_id is not null and pointcall__documentary_unit_id  in (select documentary_unit_id from navigoviz."source" s)
-- 60980
union
select * from navigo.taxes t where pointcall__documentary_unit_id  is null and link_to_pointcall  in (select data_block_local_id from navigoviz."source" s)
) as k
-- 11004 / 9801


select 74251 - 70781, 60980+9801, 74251 - 70863
3470	70781 3388
70 863

-- a mettre dans le python
create or replace view navigocheck.taxes_complete as (
select  joinon, pkid, record_id , pointcall__source , pointcall__documentary_unit_id, link_to_pointcall, tax_concept, payment_date , q01, q02, q03, q01_u , q02_u , q03_u 
from (
select 'documentary_unit_id' as joinon, * from navigo.taxes t where pointcall__documentary_unit_id is not null and pointcall__documentary_unit_id  in (select documentary_unit_id from navigoviz."source" s)
-- 60980
union
select 'data_block_local_id'as joinon, * from navigo.taxes t where link_to_pointcall  in (select data_block_local_id from navigoviz."source" s)
and (pointcall__documentary_unit_id is  null or pointcall__documentary_unit_id  not in (select documentary_unit_id from navigoviz."source" s))
) as k
order by pkid
)

select count(*) from navigocheck.taxes_complete
select 60980 + 9801
select pkid
from navigocheck.taxes_complete 
group by pkid 
having count(pkid) = 1

select * from navigocheck.taxes_complete where pkid in (23849, 80)

select joinon, array_dims(array_agg(pkid)) from navigocheck.taxes_complete where pkid in 
(select pkid
from navigocheck.taxes_complete 
group by pkid having count(pkid) = 1)
group by joinon

--data_block_local_id	[1:9801]
--documentary_unit_id	[1:48]
drop view navigocheck.taxes_complete 

/*union 
select * from navigo.taxes t where pointcall__source  in (select source from navigoviz."source" s)
union 
select * from navigo.taxes t where link_to_cargo_item  in (
		select record_id from navigo.cargo where pointcall__data_block_local_id  in (select data_block_local_id from navigoviz."source" s)*/
	
) as k

select * from navigo.taxes t where pkid = 60901
union
select * from navigo.taxes t where link_to_pointcall = '00296040'
-- '00296046'
ANF, G5-145/4827

alter table navigoviz.source add column documentary_unit_id text
update navigoviz.source s set documentary_unit_id = cp.documentary_unit_id
from navigocheck.check_pointcall cp where s.data_block_local_id = cp.data_block_local_id 
-- 49912

select * from navigo.taxes t where pointcall__record_id not in (select record_id from navigo.pointcall p)
-- 2268

select * from navigo.taxes t where pointcall__source = 'ANF, G5-76/5854 et 5860'
-- ok

select * from navigo.taxes t 
where trim(pointcall__source) not in (select source from navigoviz."source" s ) 
and trim(pointcall__source) not like 'ANF, G5-154-%'
-- 2322 pui 2244 puis 1063 puis 13
 select * from navigoviz."source" where source like 'ANF, G5-39B/ 5673%'

 select * from navigo.taxes t where link_to_pointcall='00150465'
 
 ANF, G5-39B/ 5673
ANF, G5-62/4417
ANF, G5-39B/ 4678
ANF, G5-118/9446
ANF, G5-62/1449
Marcus/ - NA, RG 84, Consular register
Marcus/ - NA, RG 84, Consular register
Marcus/ - NA, RG 84, Consular register
Marcus/ - NA, RG 84, Consular register
ANF, G5, 154-1, 8209
ANF, G5-118/9446
ANF, G5-125-1/4977
ADBdR, 200E, 545/0018

 */
select * from navigo.taxes t where pointcall__source not in (select source from navigoviz."source" s )

select * from navigo.taxes t where cargo__record_id not in (
	select record_id from navigo.cargo where pointcall__data_block_local_id  in (select data_block_local_id from navigoviz."source" s)
)
-- 692

select * from navigo.taxes t where link_to_cargo_item not in (
		select record_id from navigo.cargo where pointcall__data_block_local_id  in (select data_block_local_id from navigoviz."source" s)
	)
-- -- 540	

select * from 
	(select * from navigo.taxes t where link_to_cargo_item not in (
		select record_id from navigo.cargo where pointcall__data_block_local_id  in (select data_block_local_id from navigoviz."source" s)
	)) as k -- 540
	where link_to_pointcall not in (select data_block_local_id from navigoviz."source" s) --487

select * from 
	(select * from navigo.taxes t where link_to_cargo_item not in (
		select record_id from navigo.cargo where pointcall__data_block_local_id  in (select data_block_local_id from navigoviz."source" s)
	)) as k -- 540
	where pointcall__source not in (select source from navigoviz."source" s) --474

select count(*) from navigoviz."source" s where source like 'ANF, G5-154-1/%' -- 399


select * from 
	(select * from navigo.taxes t where link_to_cargo_item not in (
		select record_id from navigo.cargo where pointcall__data_block_local_id  in (select data_block_local_id from navigoviz."source" s)
	)) as k -- 540
	where pointcall__source not in (select source from navigoviz."source" s) and pointcall__source not like 'ANF, G5-154-1/%' --474
-- OK

select pointcall__source, link_to_pointcall, link_to_cargo_item, cargo__documentary_unit_id , documentary_unit_id , pointcall__documentary_unit_id 
from navigo.taxes t 
where pointcall__source  like 'ANF, G5-154-1/  1514' 
--474
-- "ANF, G5-154-1/  1514"	00280301 00184474	00280301	00280301	00280301


select data_block_local_id, documentary_unit_id , record_id , source 
from navigo.pointcall 
where documentary_unit_id = '00281235'
--00281235	00281235	00281235	ANF, G5-154-1/ 1514
--00281235	00281235	00282705	ANF, G5-154-1/ 1514

select * from navigoviz.source where source like '%ANF, G5-154-1/  1514%' -- pointcall__source dans taxe
select * from navigoviz.source where source like '%ANF, G5-154-1/ 1514%' -- pointcall

select substring(pointcall__source from 1 for 14)|| substring(pointcall__source from 16) 
from navigocheck.check_taxes t where pointcall__source like '%ANF, G5-154-1/  1514%'
ANF, G5-154-1/ 1514
ANF, G5-154-1/ 1514
select substring(pointcall__source from 1 for 14)|| substring(pointcall__source from 16) 
from navigocheck.check_taxes t where pointcall__source like '%ANF, G5-154-1/%'

pointcall__documentary_unit_id = '00335413'
select * from navigo.taxes t  where pointcall__source like '%ANF, G5-76/5854 et 5860%'

select pointcall_uhgs_id, pointcall_name , record_id from navigocheck.check_pointcall cp 
where documentary_unit_id = '00108579'

-- a mettre dans le python
create or replace view navigocheck.taxes_complete as (
select  joinon, pkid, record_id , pointcall__source , pointcall__documentary_unit_id, link_to_pointcall, tax_concept, payment_date , q01, q02, q03, q01_u , q02_u , q03_u 
from (
select 'documentary_unit_id' as joinon, * from navigo.taxes t where pointcall__documentary_unit_id is not null and pointcall__documentary_unit_id  in (select documentary_unit_id from navigoviz."source" s)
-- 60980
union
select 'data_block_local_id'as joinon, * from navigo.taxes t where link_to_pointcall  in (select data_block_local_id from navigoviz."source" s)
and (pointcall__documentary_unit_id is  null or pointcall__documentary_unit_id  not in (select documentary_unit_id from navigoviz."source" s))
) as k
order by pkid
)

select * from navigocheck.taxes_complete where link_to_pointcall is not null and joinon <> 'data_block_local_id'
select * from navigocheck.taxes_complete where pointcall__documentary_unit_id is  null and joinon <> 'documentary_unit_id'
select * from navigocheck.check_taxes ct  where link_to_pointcall is  null

create or replace view public.pointcall_taxes as (select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'tax_concept' as tax_concept,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'payment_date' as payment_date,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q01' as q01,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q01_u' as q01_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q02' as q02,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q02_u' as q02_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q03' as q03,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q03_u' as q03_u,
            jsonb_agg(to_jsonb(r.*) ) as all_taxes
            from 
                (
                select joinon, pointcall__documentary_unit_id, link_to_pointcall , c.tax_concept, c.payment_date , c.q01, c.q01_u, c.q02, c.q02_u, c.q03, c.q03_u  
                from navigocheck.taxes_complete c
                ) as r	
            group by r.link_to_pointcall
            )     
---------------------

            create or replace view navigocheck.pointcall_cargo as (
            select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_purpose' as commodity_purpose,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_id' as commodity_id,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity' as quantity,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity_u' as quantity_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_standardized' as commodity_standardized,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_permanent_coding' as commodity_permanent_coding,
            jsonb_agg(to_jsonb(r.*) ) as all_cargos
            from 
                (
                select c.pointcall__data_block_local_id as  link_to_pointcall, c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                c.commodity_standardized , c.dictionary_commodities__permanent_coding as commodity_permanent_coding 
                from navigocheck.check_cargo c
                ) as r
            group by r.link_to_pointcall)
            
            select * from navigocheck.pointcall_cargo
            -- 39161
            
            drop view IF EXISTS navigocheck.pointcall_taxes cascade

            create or replace view navigocheck.pointcall_taxes as (select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'tax_concept' as tax_concept,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'payment_date' as payment_date,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q01' as q01,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q01_u' as q01_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q02' as q02,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q02_u' as q02_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q03' as q03,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q03_u' as q03_u,
            jsonb_agg(to_jsonb(r.*) ) as all_taxes
            from 
                (
                select pointcall__data_block_local_id as  link_to_pointcall , c.tax_concept, null as payment_date , c.q01, c.q01_u, c.q02, c.q02_u, c.q03, c.q03_u  
                from navigocheck.check_taxes c
                ) as r	
            group by r.link_to_pointcall
            )       
--- importer les traductions des produits
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\commodity_standardized.csv

-- corriger les Peaux de bœuf
select * from ports.labels_lang_csv llc where fr like 'b%f'   
select * from ports.labels_lang_csv llc where fr like '%Peaux%'   
select * from ports.labels_lang_csv llc where fr like '%uf'   
commodity	00001279	Peaux de buf	Skin (oxen)	
toponyme	A0160615	Paimbuf	Paimbuf	
toponyme	A0123256	Le Pas au Bœuf	Le Pas au Bœuf

-- taper oe : ALT 0156 
-- https://outils-javascript.aliasdmc.fr/encodage-caracteres-accentues/encode-caractere-0153-html-css-js-autre.html

update ports.labels_lang_csv llc set fr ='Peaux de bœuf'  where key_id = '00001279'
update ports.labels_lang_csv llc set fr ='Le Pas au Bœuf', en='Le Pas au Bœuf'  where key_id = 'A0123256' and label_type = 'toponyme'
update ports.labels_lang_csv llc set fr ='Paimbœuf', en='Paimbœuf'  where key_id = 'A0160615' and label_type = 'toponyme'

select * from ports.labels_lang_csv where key_id in ('00001279', 'A0123256', 'A0160615')

select uhgs_id , toponyme_standard_fr , toponyme_standard_en from ports.port_points pp where  uhgs_id in ('A0123256', 'A0160615')
update ports.port_points  set toponyme_standard_fr ='Paimbœuf', toponyme_standard_en='Paimbœuf'  where uhgs_id = 'A0160615'
update ports.port_points  set toponyme_standard_fr ='Le Pas au Bœuf', toponyme_standard_en='Le Pas au Bœuf'  where uhgs_id = 'A0123256'



drop table ports.port_points_bk04fev2021 cascade
drop table ports.port_points_old  cascade
create table ports.port_points_old as (
	select * from ports.port_points pp 
)-- 1144

drop table ports.geo_general_backup cascade



drop view IF EXISTS navigocheck.pointcall_cargo cascade

create or replace view navigocheck.pointcall_cargo as (
            select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_purpose' as commodity_purpose,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_id' as commodity_id,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity' as quantity,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity_u' as quantity_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_standardized_en' as commodity_standardized,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_permanent_coding' as commodity_permanent_coding,
            jsonb_agg(to_jsonb(r.*) ) as all_cargos
            from 
                (
                select c.pointcall__data_block_local_id as  link_to_pointcall, c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                 c.dictionary_commodities__permanent_coding as commodity_permanent_coding, 
                labels.fr as commodity_standardized_fr,  labels.en as commodity_standardized_en  
                from navigocheck.check_cargo c, ports.labels_lang_csv labels
                where label_type= 'commodity' and key_id = c.commodity_id
                ) as r
            group by r.link_to_pointcall
        );

select distinct commodity_id, commodity_standardized, commodity_standardized_en, commodity_standardized_fr 
from navigocheck.pointcall_cargo where commodity_standardized <> commodity_standardized_en
-- corrigé après retour de Silvia le 08/02/2021 même jour

update ports.labels_lang_csv llc set  en='Plank' where label_type = 'commodity' and  key_id = '00000068' ;
update ports.labels_lang_csv llc set  en='Barley' where label_type = 'commodity' and  key_id = '00000093' ;
update ports.labels_lang_csv llc set  en='Oats' where label_type = 'commodity' and  key_id = '00000095' ;
update ports.labels_lang_csv llc set  en='Cask (empty)' where label_type = 'commodity' and  key_id = '00000242' ;
update ports.labels_lang_csv llc set  en='Fishing implements' where label_type = 'commodity' and  key_id = '00000281' ;
update ports.labels_lang_csv llc set  en='Bran' where label_type = 'commodity' and  key_id = '00000491' ;
update ports.labels_lang_csv llc set  en='Bottle' where label_type = 'commodity' and  key_id = '00000589' ;
update ports.labels_lang_csv llc set  en='Cannon' where label_type = 'commodity' and  key_id = '00000590' ;
update ports.labels_lang_csv llc set  en='Stone' where label_type = 'commodity' and  key_id = '00000630' ;
update ports.labels_lang_csv llc set  en='Dry fish' where label_type = 'commodity' and  key_id = '00001281' ;
update ports.labels_lang_csv llc set  en='Faience' where label_type = 'commodity' and  key_id = '00001275' ;

select distinct commodity_id, commodity_standardized, commodity_standardized_fr 
from navigocheck.pointcall_cargo 
-- 121


-- définir un sous-ensemble Poitou
update navigoviz.source set subset = 'Poitou' where subset is null and suite = 'G5' and main_port_uhgs_id in ('A1964694', 'A0171758', 
        'A0136930', 'A0196496', 'A0198999', 'A0137148', 'A0127055', 'A0133403', 'A0213721', 'A0199508', 'A0148208', 'A0141325', 'A0138533', 'A1964982', 'A0186515', 
        'A0124809', 'A1964767', 'A0172590', 'A0181608',  'A0169240', 'A0165056', 'A1963997', 'A0136403', 'A0195938', 'A0122971',  'A0207992', 'A0165077')
-- 13434 
-- ile de Bouin      'A0165077' 
        
select distinct state_1789_fr, substate_1789_fr from ports.port_points pp      
order by state_1789_fr

drop table navigoviz.pointcall

create table navigoviz.pointcall as (
            select k.identifiant as pkid, 
            k.record_id,
            -- pointcall
            pp.toponyme as pointcall, pp.uhgs_id as pointcall_uhgs_id, 
            pp.toponyme_standard_fr as toponyme_fr, pp.toponyme_standard_en as toponyme_en,
            pp.latitude, pp.longitude, 
            pp.amiraute as pointcall_admiralty, pp.province as pointcall_province, 
            pp.belonging_states as pointcall_states, pp.belonging_substates as pointcall_substates,
            pp.belonging_states_en as pointcall_states_en, pp.belonging_substates_en as pointcall_substates_en,
            pp.state_1789_fr, pp.state_1789_en, pp.substate_1789_fr, pp.substate_1789_en,
            pp.source_1787_available, pp.source_1789_available, 
            pp.status as pointcall_status, pp.shiparea, st_asewkt(pp.point3857) as pointcall_point,  
            pp.ferme_direction, pp.ferme_bureau, pp.ferme_bureau_uncertainty,
            pp.partner_balance_1789, pp.partner_balance_supp_1789, pp.partner_balance_1789_uncertainty, pp.partner_balance_supp_1789_uncertainty,
            k.pointcall_outdate as pointcall_out_date, 
            k.pointcall_action, 
            to_date(k.pointcall_outdate_date, 'DD/MM/YYYY') as outdate_fixed,
            k.pointcall_indate as pointcall_in_date, 
            to_date(k.pointcall_indate_date, 'DD/MM/YYYY') as indate_fixed,
            k.net_route_marker,
            k.pointcall_function,
            k.pointcall_status as navigo_status,
            -- ship
            k.ship_name, k.ship_id, k.ship_tonnage as tonnage, k.ship_tonnage_u as tonnage_unit, k.ship_flag as flag, k.ship_class as class,
            k.ship_flag_id, k.ship_increw as in_crew,
            -- homeport
            k.ship_homeport as homeport, k.ship_homeport_uhgs_id as homeport_uhgs_id,
            k.toponyme_standard_fr as homeport_toponyme_fr, k.toponyme_standard_en as homeport_toponyme_en,
            k.latitude as homeport_latitude, k.longitude  as homeport_longitude, 
            k.amiraute as homeport_admiralty, k.province as homeport_province, 
            k.belonging_states as homeport_states, k.belonging_substates as homeport_substates, k.belonging_states_en as homeport_states_en, k.belonging_substates_en as homeport_substates_en,
            k.state_1789_fr as homeport_state_1789_fr, pp.state_1789_en as homeport_state_1789_en, pp.substate_1789_fr as homeport_substate_1789_fr, pp.substate_1789_en as homeport_substate_1789_en,
            k.source_1787_available as homeport_source_1787_available, k.source_1789_available as homeport_source_1789_available, 
            k.status as homeport_status, k.shiparea as homeport_shiparea, st_asewkt(k.point3857) as homeport_point,
            k.ferme_direction as homeport_ferme_direction, k.ferme_bureau as  homeport_ferme_bureau, k.ferme_bureau_uncertainty as homeport_ferme_bureau_uncertainty,
            k.partner_balance_1789 as homeport_partner_balance_1789, k.partner_balance_supp_1789 as homeport_partner_balance_supp_1789, k.partner_balance_1789_uncertainty as homeport_partner_balance_1789_uncertainty, 
            k.partner_balance_supp_1789_uncertainty as homeport_partner_balance_supp_1789_uncertainty,
            -- source
            k.doc_id as source_doc_id, k.cpsource as source_text, s.suite as source_suite, s.component as source_component, 
            s.conge_number as source_number, s.other as source_other, s.main_port_uhgs_id as source_main_port_uhgs_id, s.main_port_toponyme as source_main_port_toponyme,
            s.subset as source_subset,
            -- captain
            k.captain_local_id as captain_id, k.captain_name, k.captain_birthplace as birthplace, k.captain_status as status, k.captain_citizenship as citizenship,
            -- cargo
            cc.commodity_purpose, cc.commodity_id, cc.quantity, cc.quantity_u, cc.commodity_standardized, cc.commodity_standardized_fr, cc.commodity_permanent_coding, cc.all_cargos,
            -- taxes
            k.tax_concept, k.payment_date, k.q01, k.q01_u, k.q02, k.q02_u , k.q03 , k.q03_u, k.all_taxes,
            -- uncertainity
            up.ship_id as ship_uncertainity, up.ship_tonnage as tonnage_uncertainity, up.ship_flag as flag_uncertainity, up.ship_homeport as homeport_uncertainity,
            case when k.pointcall_function in ('A', 'O') then 0 else (case when k.pointcall_status like 'PC%' then 0 else (case when  k.pointcall_status like 'PU%' then -2 else -1 end) end) end as pointcall_uncertainity,
            up.captain_name as captain_uncertainity
            
            from 
            (
                (select cp.pkid as identifiant, cp.data_block_local_id as doc_id, cp."source" as cpsource, * 
                    from navigocheck.check_pointcall cp 
                    left outer join navigocheck.pointcall_taxes ct on cp.data_block_local_id = ct.link_to_pointcall
                ) as q  
                left outer join ports.port_points ph on q.ship_homeport_uhgs_id = ph.uhgs_id
            ) as k 
            left outer join navigocheck.pointcall_cargo cc on cc.link_to_pointcall = k.doc_id, 
            navigocheck.uncertainity_pointcall up, navigoviz."source" s, 	 ports.port_points pp  
            where  up.pkid = k.identifiant and   s.data_block_local_id = k.doc_id and s.source = k.cpsource
            and pp.uhgs_id = k.pointcall_uhgs_id
)
-- 108530

-- modif : 
-- ship_id au lieu de ship_loal_id
-- ship_increw au lieu de in_crew
select distinct pointcall_function from navigo.pointcall p 

select distinct pointcall_function from navigo.pointcall p 

---------------------
select count(*) from navigoviz.source where subset = 'Poitou'
-- 14200

select count(*) from navigoviz.pointcall where subset = 'Poitou' 
-- 26817
select outdate_fixed , pointcall_out_date, pointcall_in_date, indate_fixed 
from navigoviz.pointcall 
where source_subset = 'Poitou' 
and ( extract(year from outdate_fixed) = 1789 or extract(year from indate_fixed) = 1789)
-- 6904


select p.source_doc_id , p.*
from navigoviz.pointcall p, navigoviz."source" s
where subset = 'Poitou' and s.data_block_local_id  = p.source_doc_id 
and ( extract(year from outdate_fixed) = 1789 or extract(year from indate_fixed) = 1789)
-- 
00339898	ANF, G5-126-1/8195
00333141	ANF, G5-129/8164
00338736	ANF, G5-118/2386
00338525	ANF, G5-118/7775

select from navigoviz.pointcall p where source_text = 'ANF, G5-126-1/8195'
select * from navigoviz.pointcall p where source_doc_id = '00339898'

select distinct main_port_toponyme , main_port_uhgs_id, component 
from navigoviz.source where subset = 'Poitou' 
order by main_port_toponyme

select distinct component from navigoviz.source where main_port_uhgs_id = 'A0137148'

select distinct component  from navigoviz.source where main_port_uhgs_id is null
ANF, G5, 154-1, 1539 -- Saint Valery en Caux
ANF, G5-76 -- Dunkerque
ANF, G5-151B -- Saint Martin de Ré
ANF, G5-125 -- Noirmoutier ? ou Omonville ?


select distinct component  from navigoviz.source where main_port_uhgs_id is null
-- ANF, G5, 154-1, 1539

update navigoviz.source s set main_port_uhgs_id =k.uhgs_id, main_port_toponyme =k.toponyme
            from (
            select  q2.component, q2.uhgs_id, q2.toponyme, q1.max from (
            select component, max(c)  from (
                select distinct substring(source from 1 for position('/' in source)) as component, pp.uhgs_id , pp.toponyme , pp.amiraute , pp.province , pp.shiparea, count(*) as c
                from navigocheck.check_pointcall p, ports.port_points  pp
                where p.pointcall_uhgs_id = pp.uhgs_id 
                and (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
                    and substring(source from 1 for position('/' in source)) not like 'G5-%') 
                -- and ( pointcall_outdate like '%1787%' or pointcall_indate like '%1787%') and data_block_leader_marker='A'
                group by component, pp.uhgs_id, pp.toponyme , pp.amiraute , pp.province , pp.shiparea
                order by component) as k
            group by component
            ) as q1,
            (select  distinct substring(source from 1 for position('/' in source)) as component, pp.uhgs_id , pp.toponyme , pp.amiraute , pp.province , pp.shiparea, count(*) as c
                from navigocheck.check_pointcall p, ports.port_points   pp
                where p.pointcall_uhgs_id = pp.uhgs_id 
                and (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
                    and substring(source from 1 for position('/' in source)) not like 'G5-%') 
                -- and ( pointcall_outdate like '%1787%' or pointcall_indate like '%1787%') and data_block_leader_marker='A'
                group by component, pp.uhgs_id, pp.toponyme , pp.amiraute , pp.province , pp.shiparea
                order by component
            ) as q2
            where q1.component = q2.component and q2.c = q1.max
            ) as k
            where position('/' in k.component)>0 and s.component = substring(k.component from 1 for position('/' in k.component)-1)

            
  -- 39389
  select * from navigoviz."source" where   component is null  
    select * from navigoviz."source" where   position('/' in component) =0 

A0122218	Rouen
A0135548	Saint Valery en Caux
A0187101	Le Havre
A0187836	Honfleur

select * from navigoviz."source" where   component = 'ANF, G5, 154-1, 1539'
select * from navigoviz."source" where   component like 'ANF, G5-154%'

update navigoviz."source" set main_port_uhgs_id ='A0135548', main_port_toponyme = 'Saint Valery en Caux' where component = 'ANF, G5, 154-1, 1539'


update navigoviz.source set subset = 'Poitou' where subset is null and suite = 'G5' and main_port_uhgs_id in ('A1964694', 'A0171758', 
        'A0136930', 'A0196496', 'A0198999', 'A0137148', 'A0127055', 'A0133403', 'A0213721', 'A0199508', 'A0148208', 'A0141325', 'A0138533', 'A1964982', 'A0186515', 
        'A0124809', 'A1964767', 'A0172590', 'A0181608',  'A0169240', 'A0165056', 'A1963997', 'A0136403', 'A0195938', 'A0122971',  'A0207992', 'A0165077')
        
select distinct component  from navigoviz.source where main_port_uhgs_id is null
-- ok

update navigoviz.pointcall p set source_subset = 'Poitou_1789' where source_doc_id in (
	select source_doc_id from navigoviz.pointcall p where p.source_doc_id in (
	select p.source_doc_id from navigoviz.pointcall p where source_subset = 'Poitou' 
	and ( extract(year from outdate_fixed) = 1789 or extract(year from indate_fixed) = 1789)
	)
) -- 13829

select count(*) from navigoviz.pointcall where source_subset = 'Poitou_1789' 
drop table if exists navigoviz.pointcall cascade
select count(*) from navigocheck.check_pointcall where source_subset = 'Poitou_1789' 
select count(*) from navigocheck.check_cargo cc 

truncate navigocheck.check_cargo ;
truncate navigocheck.check_geo_general ;
truncate navigocheck.check_pointcall ;
truncate navigocheck.check_stages ;
truncate navigocheck.check_taxes ;
truncate navigocheck.uncertainity_cargo ;
truncate navigocheck.uncertainity_geo_general ;
truncate navigocheck.uncertainity_pointcall ;
truncate navigocheck.uncertainity_stages ;
truncate navigocheck.uncertainity_taxes ;

----------------------------------------------------------------------
-- Traitement incertitude
----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -4
                        result = None
                else :
                    result = None
                    code = -4
                return [result, code]
            $$ LANGUAGE plpython3u;

           
select navigo.rm_parentheses_crochets('dsdffd') -- 0
select navigo.rm_parentheses_crochets('(dsdffd)') -- -1        
select navigo.rm_parentheses_crochets('[(dsdffd)]') -- -2
select navigo.rm_parentheses_crochets('[fgd]') -- -2
select navigo.rm_parentheses_crochets(' ') ---4
  
 select source_doc_id, ship_id, tonnage::float, tonnage_unit from navigoviz.pointcall
 
 select * from navigoviz.pointcall where tonnage = '2.5'
 
 -- pré-traitement pour prendre en compte les tonnages déjà vérifiés pas Silvia
 -- requiert l'import de update_tonnage_uncertainty_csv qui donne la liste des valeurs contrôlées par Silvia
 select cp.ship_id, cp.pointcall_function, cp.ship_tonnage , cp.ship_tonnage_u , uc.ship_tonnage , uc.ship_tonnage_u , utuc.tonnage_uncertainty 
 from navigocheck.uncertainity_pointcall uc, navigocheck.check_pointcall cp , navigo.update_tonnage_uncertainty_csv utuc 
 where uc.pkid = cp.pkid and uc.ship_tonnage<>-4 and cp.ship_id = utuc.ship_id 
 order by uc.ship_tonnage asc
 -- 557 lignes
 
 update navigoviz.pointcall set tonnage_uncertainity = 0 where tonnage_uncertainity<> -4;
-- 83028

update navigoviz.pointcall p set tonnage_uncertainity = utuc.tonnage_uncertainty
from navigo.update_tonnage_uncertainty_csv utuc
where tonnage_uncertainity<> -4 and p.ship_id = utuc.ship_id ;
-- 557 

select p.ship_id, p.tonnage_uncertainity, utuc.tonnage_uncertainty 
from  navigoviz.pointcall p , navigo.update_tonnage_uncertainty_csv utuc
where tonnage_uncertainity<> -4 and p.ship_id = utuc.ship_id 
-- 557

select distinct  p.tonnage_uncertainity, utuc.tonnage_uncertainty 
from  navigoviz.pointcall p , navigo.update_tonnage_uncertainty_csv utuc
where tonnage_uncertainity<> -4 and p.ship_id = utuc.ship_id 

select  pointcall_function, navigo_status , count(*)
from navigoviz.pointcall
group by pointcall_function, navigo_status 
order by pointcall_function, navigo_status

--A	FC-RF	11
--A	FC-RS	12
--A	PA-RC	1
--A	PC-RC	28
--A	PC-RF	932
--A	PC-RS	10921
--O	FC-RF	28
--O	PC-ML	119
--O	PC-RC	2
--O	PC-RF	41588
--O	PC-RS	11315
--O	PG-RF	73
--t	FC-RF	1
--T	FA-RF	12
--T	FC-ML	115
--T	FC-RC	2
--T	FC-RF	39229
--T	FC-RS	186
--T	FC-RT	1
--T	FC-TT	2
--T	FM-RF	7
--T	PC-ML	3
--T	PC-RF	40
--T	PC-RS	3
--T	PU-RF	1
--T	PU-RS	4
--Z	FC-RF	4
--Z	PC-RS	1
--	FA-RF	5
--	FC-ML	12
--	FC-RF	193
--	FC-RS	44
--	FM-RF	6
--	PC-RF	181
--	PC-RS	3439
--	PU-RS	4
--		5


select source_subset, navigo_status, pointcall_rankfull, pointcall_action , * 
from navigoviz.pointcall p where pointcall_function is null
-- 1400

-- 1. tout ce qui est observé ou sûr
update navigoviz.pointcall p set pointcall_uncertainity = 0 where navigo_status like 'PC-%' 
and pointcall_uncertainity <> -4 and pointcall_uncertainity <> -2;
-- 68572

update navigoviz.pointcall p set pointcall_uncertainity = 0 where pointcall_function like 'O' 
and pointcall_uncertainity <> -4 and pointcall_uncertainity <> -2 and pointcall_uncertainity <> 0;
-- 28

-- select source_doc_id , ship_name , ship_id , pointcall, pointcall_in_date , pointcall_out_date, net_route_marker , data_block_leader_marker from navigoviz.pointcall p where pointcall_uncertainity = -2 
-- 9
--00189943	Anne Marguerite	0014911N	Ostende	1787>12>28!		A	T
--00310958	Vierge des Carmes et Saint Antoine		Barcelonne	1789>05>18			
--00354763	Sainte Anne		Martigues	1787>04>22			T
--00332297	Saint Jerome		Barcelonne	1789>08>10			
--00182826	Très Saint Crucifix	0015131N	Espagne [Mediterranean]	1787>01>27!		A	T
--00174446	Petite Fille	0007352N	Bergen	1787>05>04!		A	T
--00332294	Saint Michel Archange			1789>08>10!			T
--00182467	Union	0014650N	Espagne [Mediterranean]	1787>01>03!		A	T
--00189968	Ville d' yverdun	0014815N	Smirne	1787>12>31!		A	

-- select * from navigoviz.pointcall p where navigo_status like 'PU-%'



--2. Les points incertains
update navigoviz.pointcall p set pointcall_uncertainity = -1 where navigo_status like 'FC-%' 
and pointcall_uncertainity <> -4 and pointcall_uncertainity <> -2;
-- 39840

update navigoviz.pointcall p set pointcall_uncertainity = -3 where navigo_status like 'PU-%' 
        and pointcall_uncertainity <> -4 
-- 9

--3. tous les attributs sont à 0 sauf si manquants ou bien déduits []
update navigoviz.pointcall p set captain_uncertainity = 0 where navigo_status like 'FC-%' 
and captain_uncertainity <> -4 and captain_uncertainity <> -2;
-- 39824

update navigoviz.pointcall p set cargo_uncertainity = 0 where navigo_status like 'FC-%' 
and cargo_uncertainity <> -4 and cargo_uncertainity <> -2;
-- 25954

update navigoviz.pointcall p set flag_uncertainity = 0 where navigo_status like 'FC-%' 
and flag_uncertainity <> -4 and flag_uncertainity <> -2;
-- 13559

update navigoviz.pointcall p set homeport_uncertainity = 0 where navigo_status like 'FC-%' 
and homeport_uncertainity <> -4 and homeport_uncertainity <> -2;
-- 27220

update navigoviz.pointcall p set ship_uncertainity = 0 where navigo_status like 'FC-%' 
and ship_uncertainity <> -4 and ship_uncertainity <> -2;
-- 31453

update navigoviz.pointcall p set taxe_uncertainity = 0 where navigo_status like 'FC-%' 
and taxe_uncertainity <> -4 and taxe_uncertainity <> -2;
-- 38895

update navigoviz.pointcall p set tonnage_uncertainity = 0 where navigo_status like 'FC-%' 
and tonnage_uncertainity <> -4 and tonnage_uncertainity <> -2;
-- 39057



-- 4. Boucler sur les trajets
select  pointcall_rankfull, pointcall, date_fixed , pointcall_function, navigo_status, net_route_marker, pointcall_action , ship_id, ship_uncertainity ,record_id , source_doc_id, source_subset, * 
from navigoviz.pointcall
where ship_id is not null 
and ship_id = '0000018N'
-- navigo_status like 'FC-%' and net_route_marker
order by ship_id, pointcall_rankfull

select distinct ship_id 
from navigoviz.pointcall
where ship_id is not null 
group by ship_id
having count(*) > 1
-- 10943 ships

--00138632	00138632	Poitou	PC-RF	O	1	Out	0000018N	21813	00138632	La Rochelle
--00139630	00138632	Poitou	FC-RF	T	2	In	0000018N	22761	00139630	Dunkerque
--00108254	00108254		PC-RF	O	3	Out	0000018N	3866	00108254	Cherbourg
--00118606	00108254		FC-RF	T	4	In	0000018N	13882	00118606	Dunkerque
--00110460	00110460		PC-RF	O	5	Out	0000018N	6072	00110460	Dunkerque
--00120801	00110460		FC-RF	T	6	In	0000018N	15724	00120801	Brest
--00144905	00144905		PC-RF	O	7	Out	0000018N	27549	00144905	Brest
--00145435	00144905		FC-RF	T	8	In	0000018N	28079	00145435	La Rochelle
--00151180	00151180	Poitou	PC-RF	O	9	Out	0000018N	32527	00151180	Marennes
--00157150	00151180	Poitou	FC-RF	T	10	In	0000018N	37834	00157150	Honfleur
--00136856	00136856		PC-RF	O	11	Out	0000018N	20070	00136856	Honfleur
--00137593	00136856		FC-RF	T	12	In	0000018N	20777	00137593	Bayonne
--00145081	00145081		PC-RF	O	13	Out	0000018N	27725	00145081	Brest
--00145611	00145081		FC-RF	T	14	In	0000018N	28255	00145611	Blaye
select pointcall, ship_id, ship_name from navigo.pointcall where data_block_local_id = '00138632'
select pointcall_uhgs_id , ship_id, ship_name, captain_local_id , captain_name, pointcall_date 
from navigo.pointcall 
where  ship_id = '0000018N' order by pointcall_date 

select cp.ship_name , up.ship_name from navigocheck.uncertainity_pointcall up  , navigocheck.check_pointcall cp 
where cp.pkid = up.pkid 
and cp.record_id = '00139630'

select cp.record_id, cp.ship_name , up.ship_name from navigocheck.uncertainity_pointcall up  , navigocheck.check_pointcall cp 
where cp.pkid = up.pkid and up.ship_name = -2

select record_id , ship_name, captain_name from navigo.pointcall where ship_name like '[%]'

select record_id , ship_name, captain_name, ship_tonnage from navigo.pointcall where ship_tonnage like '[%]'



-----------------
-- retravailler sur les trajets


create table navigoviz.uncertainity_travels as (
            select depart.ship_id||'-'||to_char(depart.id, '09') as travel_id, depart.id, depart.ship_id, depart.ship_name, depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as departure_uhgs_id, arrivee.pointcall_uhgs_id as destination_uhgs_id, 
            
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            
            depart.pointcall_action as departure_action, depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, depart.net_route_marker as depart_net_route_marker,
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, arrivee.net_route_marker as arrivee_net_route_marker,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid ,
            
            arrivee.ship_name as arrivee_shipname, 
            depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            arrivee.pointcall_out_date as arrivee_outdate
            
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uhgs_id , d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id
            from navigoviz.pointcall d
            --where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull) as depart,
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uhgs_id , d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, d.pointcall_out_date,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id
            from navigoviz.pointcall d
            --where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull, d.source_doc_id) as arrivee
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id 
            order by depart.ship_id, depart.id 
        ) 
-- 62105
-- drop table navigoviz.uncertainity_travels

        
        alter table navigoviz.uncertainity_travels add column distance_dep_dest_miles  text;
		update navigoviz.uncertainity_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                    SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,   stages__length_miles
                    FROM navigoviz.uncertainity_travels t , navigo.stages s 
                    where stages__length_miles is not null and stages__length_miles != '0.0' and t.departure_uhgs_id = s.pointcall_uhgs_id and t.destination_uhgs_id = s.next_point__pointcall_uhgs_id 
--                    union 
--                    SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,  stages__length_miles
--                    FROM navigoviz.uncertainity_travels t ,navigo.stages s 
--                    where stages__length_miles is not null and t.destination_uhgs_id = s.pointcall_uhgs_id and t.departure_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
        -- 33s 
        -- 38040

        
       	update navigoviz.uncertainity_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,  stages__length_miles
                   FROM navigoviz.uncertainity_travels t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.destination_uhgs_id = s.pointcall_uhgs_id and t.departure_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id
		-- 27412
        --- Certains lignes restent vide
        
         alter table navigoviz.uncertainity_travels  alter column distance_dep_dest_miles type float using distance_dep_dest_miles::float

        set search_path = 'navigoviz', 'ports', public
        
        
       /* select * from uncertainity_travels where distance_dep_dest_miles is not null
        -- 0000001N- 02 Angleterre	Boulogne sur Mer	A0390929	A0152606
        --0000008N- 02	2	0000008N	Lyon	Lisbonne [mais: Angleterre]	Boulogne sur Mer	A0394917	A0152606
        select * from uncertainity_travels where distance_dep_dest_miles is  null
        select * from navigo.stages where pointcall_uhgs_id ='A0390929' or next_point__pointcall_uhgs_id='A0390929'

                select * from navigo.stages where pointcall_uhgs_id ='A0390929' and next_point__pointcall_uhgs_id='A0204180'
                select * from navigo.stages where pointcall_uhgs_id ='A0204180' and next_point__pointcall_uhgs_id='A0390929'

		update navigoviz.uncertainity_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,  stages__length_miles
                   FROM navigoviz.uncertainity_travels t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.destination_uhgs_id = 'A0390929' and 'A0394917' = s.next_point__pointcall_uhgs_id and t.departure_uhgs_id = s.pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id
        
        update navigoviz.uncertainity_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,  stages__length_miles
                   FROM navigoviz.uncertainity_travels t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.departure_uhgs_id = 'A0390929' and 'A0394917' = s.pointcall_uhgs_id and t.destination_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id
        

        A0181496	Barfleur	Angleterre	A0390929	66.0
A0152606	Boulogne sur Mer	Angleterre	A0390929	26.0
A0152606	Boulogne sur Mer	Angleterre	A0390929	26.0
A0152606	Boulogne sur Mer	Angleterre	A0390929	26.0

        select distinct departure from uncertainity_travels where distance_dep_dest_miles is  null
		select distinct departure , departure_uhgs_id , destination, destination_uhgs_id from uncertainity_travels where distance_dep_dest_miles is  null
		order by departure 
		
		Angleterre	A0390929	Calais	A0165080
Angleterre	A0390929	Saint Malo	A0170819
Angleterre	A0390929	Le Havre	A0187101
Angleterre	A0390929	Dunkerque	A0204180
Angleterre	A0390929	Détroit de Gibraltar	A0354220
Angleterre	A0390929	Lorient	A0140266
Angleterre	A0390929	Angleterre	A0390929
Angleterre	A0390929	Dieppe	A0130761
Angleterre	A0390929	Waterford	A0599268
Angleterre	A0390929	Nantes	A0124817
-- A0399213
-- Saint Malo	A0170819
		select * from navigo.stages s where s.pointcall_uhgs_id = 'A0170819' or s.next_point__pointcall_uhgs_id = 'A0170819'
Angleterre	Dunkerque	A0390929	A0204180


		select * from navigo.stages s where s.pointcall_uhgs_id = 'A0390929' or s.next_point__pointcall_uhgs_id = 'A0390929'

        -- 0000001N- 02
        -- 15191*/
        
        
        select departure , destination from uncertainity_travels ut 
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='Z' -- vient de silvia
        and arrivee_net_route_marker='A' 
        and departure != destination
        -- 761 lignes
        Ile de Ré	Ars en Ré
        
        select departure , pp.amiraute , destination , pp2.amiraute,  duration , distance_dep_dest_miles , distance_dep_dest_miles/duration as vitesse
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
        and pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
        and pp.amiraute != pp2.amiraute
        -- 348
        
        select duration, departure_out_date , outdate_fixed , destination_in_date , indate_fixed from navigoviz.uncertainity_travels ut 
        
        
        select count(*), min(vitesse), max(vitesse), avg(vitesse), stddev(vitesse) from (
        select departure , pp.amiraute , destination , pp2.amiraute,  duration , distance_dep_dest_miles , distance_dep_dest_miles/duration as vitesse
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where 
        -- destination_navstatus like 'PC%' and destination_function = 'O' 
        -- and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
        -- and 
        pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        -- and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
        -- and pp.amiraute != pp2.amiraute
        and duration is not null and duration > 0
        ) as k
        where vitesse > 25
        
        -- https://www.persee.fr/doc/geo_0003-4010_1942_num_51_288_12093
        -- Vitesse maximale : 8 noeud marin (1,852 km/h ) soit 96 noeud / jours
        -- 1679 segments sous 8 : 1679	0.03689064558629776	8.0	3.0406326563457875	2.337086692478929
        -- 5340	segments sous 100 : 0.03689064558629776	100.0	24.358895250039193	22.315579779058236 et AVG = 25
        -- 5902	segments au total : 0.03689064558629776	987.5	27.595472059470406	33.026895901475804 et AVG = 27.59
        -- Moyenne : 2 noeuds (par heure) donc 25 noeuds par jour de 12H.
        
        -- Export pour vérification
        select doc_depart, source_depart, doc_destination , source_destination, departure , ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
        and pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
        -- 562 lignes à vérifier. Pointcall_uncertainity de départ prend -3 et les autres attributs aussi. 
        -- and pp.amiraute != pp2.amiraute
        -- and duration is not null and duration > 0
        
        -- Hypothèse : Toulon- Marseille = 48 miles,et un bateau peut parcourir en une journée Toulon-Marseille (48 noeuds)
        -- En effet, en 8 heures, et en allant à 6 noeuds, il réalise la distance. 
        
        -- ici depart_net_route_marker ='A' serait remettre en question l'expertise de Silvia
        -- et supposer qu'elle s'est parfois gourrée : 8823 lignes à vérifier
        select doc_depart, source_depart, doc_destination , source_destination, departure , ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='A' and pp.state_1789_fr = 'France' and arrivee_net_route_marker='A' 
        and pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
        
--
	-- les dates de sortie des ports
	alter table navigoviz.pointcall add column pointcall_outdate_uncertainity int default 0;
	update navigoviz.pointcall set pointcall_outdate_uncertainity = -1 where source_suite = 'G5';
	-- 82356


	update navigoviz.pointcall p set pointcall_uncertainity = -3 
	from (
	
		select id, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , destination_uhgs_id, pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
        and pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
	
	) as k
	where k.doc_depart = p.source_doc_id and k.departure_uhgs_id = p.pointcall_uhgs_id 
	 and (k.departure_out_date = p.pointcall_out_date or k.departure_out_date is null)
	-- 563

	
	 
	select * from navigoviz.pointcall where pointcall_uncertainity = -3 and source_doc_id = '00141232' and pointcall_uhgs_id='A0127400'
	
	
	
	select doc_depart, departure_uhgs_id, count(*) as c from (
	select id, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , destination_uhgs_id, pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
        and pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
	
        --and departure_out_date is null
    ) as k 
    group by doc_depart, departure_uhgs_id
    having count(*) > 1
	-- ok, 0 lignes, donc le critère date n'est pas utile.

select pointcall_name , pointcall_date, ship_name, captain_name, p.ship_flag , p.ship_homeport , pointcall_function , net_route_marker, p.pointcall_status 
from navigo.pointcall p where p.pkid in (select pkid from navigoviz.pointcall p2 where p2.pointcall_uncertainity = -3)
order by pointcall_status
        
update navigoviz.pointcall p set captain_uncertainity = -3, cargo_uncertainity = -3, flag_uncertainity = -3, 
homeport_uncertainity = -3, ship_uncertainity = -3, taxe_uncertainity = -3, tonnage_uncertainity =-3
where p.pointcall_uncertainity = -3
-- 572


		select travel_id, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , destination_uhgs_id, pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        order by travel_id 
        
		select travel_id, ut.*, doc_depart, source_depart, doc_destination , source_destination, departure , departure_uhgs_id, ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , destination_uhgs_id, pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from uncertainity_travels ut , navigoviz.pointcall depar
        where pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        order by travel_id     

        select p.ship_name , p.flag, p.ship_flag_id , p.homeport , p.homeport_uhgs_id, p.captain_name, p.captain_id from navigoviz.pointcall p 
        
        create table navigoviz.debug_travels as (
            select depart.id, depart.ship_id, depart.ship_name, depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            
            depart.pointcall_action as departure_action, depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            arrivee.pointcall_out_date as arrivee_outdate,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id
            from navigoviz.pointcall d
            where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull) as depart,
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, pointcall_out_date
            from navigoviz.pointcall d
            where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull, d.source_doc_id) as arrivee
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id 
            order by depart.ship_id, depart.id 
        ) 
        -- 50755
        
        select ship_id, ship_name, depart_captainname, arrivee_captainname , depart_captainid, arrivee_captainid,
        departure_function,
        destination_function,
        position(',' in depart_captainname) ,
        substring(depart_captainname from 0 for position(',' in depart_captainname)),
        substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)),
        departure, destination, *
        from navigoviz.uncertainity_travels ut 
        where position(',' in depart_captainname) > 0 and position(',' in arrivee_captainname) > 0
        and not(depart_captainname % arrivee_captainname) 
        and not (substring(depart_captainname from 0 for position(',' in depart_captainname)) % substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)))
        -- levenshtein('depart_captainname', 'arrivee_captainname'),
        
        
        select ship_id, ship_name, depart_captainname, arrivee_captainname , depart_captainid, arrivee_captainid,
        departure_function,
        destination_function,
        position(',' in depart_captainname) ,
        substring(depart_captainname from 0 for position(',' in depart_captainname)),
        substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)),
        departure, destination, *
        from navigoviz.uncertainity_travels 
        where depart_captainid != arrivee_captainid and depart_net_route_marker = 'Z'
        and
        position(',' in depart_captainname) > 0 and position(',' in arrivee_captainname) > 0
        and (depart_captainname % arrivee_captainname) 
        and  (substring(depart_captainname from 0 for position(',' in depart_captainname)) % substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)))
        -- levenshtein('depart_captainname', 'arrivee_captainname')
        -- les captain_id sont différents, alors que les captain names se ressemblent : 596
        
        select ship_id, ship_name, depart_captainname, arrivee_captainname , depart_captainid, arrivee_captainid,
        departure_function,
        destination_function,
        position(',' in depart_captainname) ,
        substring(depart_captainname from 0 for position(',' in depart_captainname)),
        substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)),
        departure, destination, *
        from navigoviz.uncertainity_travels 
        where depart_captainid = arrivee_captainid
        and
        position(',' in depart_captainname) > 0 and position(',' in arrivee_captainname) > 0
        and not (depart_captainname % arrivee_captainname) 
        and not (substring(depart_captainname from 0 for position(',' in depart_captainname)) % substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)))
        -- les captain_id sont identiques, alors que les captain names sont différents : 232 cas où Sylvia a bien fait
        
        
        -- incertitude sur l'identification des capitaines
        update navigoviz.pointcall p set captain_uncertainity = -1 from 
        (
        select ship_id, ship_name, depart_captainname, arrivee_captainname , depart_captainid, arrivee_captainid,
        departure_function,
        destination_function,
        position(',' in depart_captainname) ,
        substring(depart_captainname from 0 for position(',' in depart_captainname)),
        substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)),
        departure, destination, id, departure_uhgs_id
        from navigoviz.uncertainity_travels 
        where depart_captainid != arrivee_captainid and depart_net_route_marker = 'Z'
        ) as k 
        where p.pointcall_rankfull = k.id and p.ship_id = k.ship_id and p.pointcall_uhgs_id = k.departure_uhgs_id and p.captain_uncertainity <> -2
        -- 601
        
        -- incertitude sur l'identification des pavillons
        update navigoviz.pointcall p set flag_uncertainity = -1 from 
        (
        select ship_id, ship_name, depart_flag, arrivee_flag , depart_flagid, arrivee_flagid,
        departure_function,
        destination_function,
        departure, destination, id, departure_uhgs_id
        from navigoviz.uncertainity_travels 
        where depart_flagid != arrivee_flagid and depart_net_route_marker = 'Z'
        ) as k 
        where p.pointcall_rankfull = k.id and p.ship_id = k.ship_id and p.pointcall_uhgs_id = k.departure_uhgs_id and p.flag_uncertainity <> -2
        -- 2 / 4
        
        -- incertitude sur l'identification des homeports
        update navigoviz.pointcall p set homeport_uncertainity = -1 from 
        (
        select ship_id, ship_name, depart_homeport, arrivee_homeport , depart_homeportid, arrivee_homeportid,
        departure_function,
        destination_function,
        departure, destination, 
        id, departure_uhgs_id
        from navigoviz.uncertainity_travels 
        where depart_homeportid != arrivee_homeportid and depart_net_route_marker = 'Z'
        ) as k 
        where p.pointcall_rankfull = k.id and p.ship_id = k.ship_id and p.pointcall_uhgs_id = k.departure_uhgs_id and p.homeport_uncertainity <> -2
        -- 274 / 321
        
        -- incertitude sur l'identification des navires (si nom de navire , nom de capitaines et port d'attache diffèrent)
        update navigoviz.pointcall p set ship_uncertainity = -1 from 
        (
        select ship_id, ship_name, arrivee_shipname, depart_captainname, arrivee_captainname, 
        departure_function,
        destination_function,
        departure, destination, 
        id, departure_uhgs_id,
        depart_homeport, arrivee_homeport
        from navigoviz.uncertainity_travels 
        where not (ship_name % arrivee_shipname) --and depart_net_route_marker = 'Z'
        and depart_homeportid != arrivee_homeportid 
        -- and depart_flagid != arrivee_flagid
        and not (depart_captainname % arrivee_captainname) 
        and position(',' in depart_captainname) > 0 and position(',' in arrivee_captainname) > 0
        and not (substring(depart_captainname from 0 for position(',' in depart_captainname)) % substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)))
        
        ) as k 
        where p.pointcall_rankfull = k.id and p.ship_id = k.ship_id and p.pointcall_uhgs_id = k.departure_uhgs_id and p.ship_uncertainity <> -2
        -- levenshtein(ship_name, arrivee_shipname),
        -- 23
        
        -- valeur de outdate qui sont contredites
        update navigoviz.pointcall p set pointcall_outdate_uncertainity = -3 from 
        (
        select ship_id, ship_name, departure_out_date, arrivee_outdate, outdate_fixed ,indate_fixed ,
        departure_function,
        destination_function,
        departure, destination, 
        id
        from navigoviz.debug_travels   
        where  
         --ship_id = '0000132N' 
        --ship_id = '0013541N' --order by id
        -- and arrivee_net_route_marker = 'Z'
         -- and depart_net_route_marker = 'Z'
        -- order by id
        -- depart_net_route_marker = 'Z' and arrivee_outdate is not null and departure_out_date is not null 
         outdate_fixed is not null and indate_fixed is not null and outdate_fixed<> indate_fixed and source_depart = 'G5' and source_destination <> 'G5'
        ) as k 
        where p.pointcall_rankfull = k.id and p.ship_id = k.ship_id and p.pointcall_outdate_uncertainity <> -2
        -- 148
        
alter table navigoviz.debug_travels add column travel_uncertainity int
update navigoviz.debug_travels set travel_uncertainity = 0 where departure_uncertainity=0 and destination_uncertainity=0 --16175
update navigoviz.debug_travels set travel_uncertainity = -1 where departure_uncertainity=-1 or destination_uncertainity=-1 --34580
update navigoviz.debug_travels set travel_uncertainity = -2 where departure_uncertainity=-2 or destination_uncertainity=-2 --0
update navigoviz.debug_travels set travel_uncertainity = -3 where departure_uncertainity=-3 or destination_uncertainity=-3 --0

select count(*) from navigoviz.debug_travels where travel_uncertainity is null
select travel_uncertainity, count(*)
from navigoviz.debug_travels
group by travel_uncertainity

select 16175+34580 --50755



create table navigoviz.built_travels as (
                    (
                    select t.ship_id||'-'||to_char(t.id, '09') as id, 
                    round(((st_distance(d.pointcall_point , a.pointcall_point ))/1000) :: numeric, 3) as distance_dep_dest,
                    round(((st_distance(d.pointcall_point , d.homeport_point::geometry ))/1000.0) :: numeric, 3) as distance_homeport_dep,
                    t.departure,
                    d.toponyme_fr as departure_fr, d.toponyme_en as departure_en,
                    d.pointcall_uhgs_id  as departure_uhgs_id,
                    d.latitude as departure_latitude,
                    d.longitude as departure_longitude,
                    d.pointcall_admiralty as departure_admiralty,
                    d.pointcall_province as departure_province,
                    d.pointcall_states as departure_states,
                    d.pointcall_substates as departure_substates,
                    d.state_1789_fr as departure_state_1789_fr, d.substate_1789_fr as departure_substate_1789_fr, d.state_1789_en as departure_state_1789_en, d.substate_1789_en as departure_substate_1789_en,
                    d.ferme_direction as departure_ferme_direction, d.ferme_bureau as departure_ferme_bureau, d.ferme_bureau_uncertainty as departure_ferme_bureau_uncertainty, 
                    d.partner_balance_1789 as departure_partner_balance_1789, d.partner_balance_supp_1789 as departure_partner_balance_supp_1789, 
                    d.partner_balance_1789_uncertainty as departure_partner_balance_1789_uncertainty, d.partner_balance_supp_1789_uncertainty as departure_partner_balance_supp_1789_uncertainty,
                    d.shiparea as departure_shiparea,
                    d.pointcall_status as departure_status,
                    d.pointcall_point as departure_point,
                    t.departure_out_date,
                    t.departure_action,
                    t.outdate_fixed,
                    t.departure_navstatus,
                    t.departure_function,
                    t.destination,
                    a.toponyme_fr as destination_fr, a.toponyme_en as destination_en,
                    a.pointcall_uhgs_id  as destination_uhgs_id,
                    a.latitude as destination_latitude,
                    a.longitude as destination_longitude,
                    a.pointcall_admiralty as destination_admiralty,
                    a.pointcall_province as destination_province,
                    a.pointcall_states as destination_states,
                    a.pointcall_substates as destination_substates,
                    a.state_1789_fr as destination_state_1789_fr, a.substate_1789_fr as destination_substate_1789_fr, a.state_1789_en as destination_state_1789_en, a.substate_1789_en as destination_substate_1789_en,
                    a.ferme_direction as destination_ferme_direction, a.ferme_bureau as destination_ferme_bureau, a.ferme_bureau_uncertainty as destination_ferme_bureau_uncertainty, 
                    a.partner_balance_1789 as destination_partner_balance_1789, a.partner_balance_supp_1789 as destination_partner_balance_supp_1789, 
                    a.partner_balance_1789_uncertainty as destination_partner_balance_1789_uncertainty, a.partner_balance_supp_1789_uncertainty as destination_partner_balance_supp_1789_uncertainty,
                    a.shiparea as destination_shiparea,
                    a.pointcall_status as destination_status,
                    a.pointcall_point as destination_point,
                    t.destination_in_date,
                    t.destination_action,
                    t.indate_fixed,
                    t.destination_navstatus,
                    t.destination_function,
                    d.ship_name,
                    t.ship_id,
                    d.tonnage,
                    d.tonnage_unit,
                    d.tonnage_class,
                    d.in_crew,
                    d.flag,
                    d.ship_flag_id,
                    d.ship_flag_standardized_fr, d.ship_flag_standardized_en, 
                    d.class,
                    d.homeport,
                    d.homeport_uhgs_id , 
                    d.homeport_latitude,
                    d.homeport_longitude,
                    d.homeport_admiralty,
                    d.homeport_province,
                    d.homeport_states,
                    d.homeport_substates,
                    d.homeport_status,
                    d.homeport_shiparea,
                    d.homeport_point,
                    d.homeport_state_1789_fr,
                    d.homeport_substate_1789_fr,
                    d.homeport_state_1789_en,
                    d.homeport_substate_1789_en,
                    d.homeport_toponyme_fr,
                    d.homeport_toponyme_en,
                    (case when t.doc_destination = doc_depart then 'from' else 'both-from' end) as source_entry, 
                    d.source_doc_id,
                    d.source_text,
                    d.source_suite,
                    d.source_component,
                    d.source_number,
                    d.source_other,
                    d.source_main_port_uhgs_id, 
                    d.source_main_port_toponyme,
                    d.source_subset,
                    d.captain_id,
                    d.captain_name,
                    d.birthplace,
                    d.status,
                    d.citizenship,
                    d.commodity_purpose,
                    d.commodity_id,
                    d.quantity,
                    d.quantity_u,
                    d.commodity_standardized,
                    d.commodity_standardized_fr,
                    d.commodity_permanent_coding,
                    d.commodity_purpose2,
                    d.commodity_id2,
                    d.quantity2,
                    d.quantity_u2,
                    d.commodity_standardized2,
                    d.commodity_standardized2_fr,
                    d.commodity_permanent_coding2,
                    d.commodity_purpose3,
                    d.commodity_id3,
                    d.quantity3,
                    d.quantity_u3,
                    d.commodity_standardized3,
                    d.commodity_standardized3_fr,
                    d.commodity_permanent_coding3,
                    d.commodity_purpose4,
                    d.commodity_id4,
                    d.quantity4,
                    d.quantity_u4,
                    d.commodity_standardized4,
                    d.commodity_standardized4_fr,
                    d.commodity_permanent_coding4,
                    d.all_cargos,
                    d.tax_concept,
                    d.payment_date,
                    d.q01, d.q01_u , d.q02 , d.q02_u , d.q03 , d.q03_u ,
                    d.all_taxes,
                    d.ship_uncertainity,
                    d.tonnage_uncertainity ,
                    d.flag_uncertainity,
                    d.homeport_uncertainity ,
                    t.departure_uncertainity,
                    t.destination_uncertainity,
                    d.captain_uncertainity,
                    t.travel_uncertainity,
                    d.cargo_uncertainity,
                    d.taxe_uncertainity, 
                    d.pointcall_outdate_uncertainity
                    from 
                    navigoviz.debug_travels t,
                    navigoviz.pointcall d,
                    navigoviz.pointcall a
                    where t.departure_pkid = d.pkid and t.destination_pkid = a.pkid
                    order by id,  coalesce (departure_out_date, destination_in_date)
                    )
                    
                    union 
                    (
                    select t.ship_id||'-'||to_char(t.id, '09') as id, 
                    round(((st_distance(d.pointcall_point , a.pointcall_point ))/1000) :: numeric, 3) as distance_dep_dest,
                    round(((st_distance(d.pointcall_point , d.homeport_point::geometry ))/1000) :: numeric, 3) as distance_homeport_dep,
                    t.departure,
                    d.toponyme_fr as departure_fr, d.toponyme_en as departure_en,
                    d.pointcall_uhgs_id  as departure_uhgs_id,
                    d.latitude as departure_latitude,
                    d.longitude as departure_longitude,
                    d.pointcall_admiralty as departure_admiralty,
                    d.pointcall_province as departure_province,
                    d.pointcall_states as departure_states,
                    d.pointcall_substates as departure_substates,
                    d.state_1789_fr as departure_state_1789_fr, d.substate_1789_fr as departure_substate_1789_fr, d.state_1789_en as departure_state_1789_en, d.substate_1789_en as departure_substate_1789_en,
                    d.ferme_direction as departure_ferme_direction, d.ferme_bureau as departure_ferme_bureau, d.ferme_bureau_uncertainty as departure_ferme_bureau_uncertainty, 
                    d.partner_balance_1789 as departure_partner_balance_1789, d.partner_balance_supp_1789 as departure_partner_balance_supp_1789, 
                    d.partner_balance_1789_uncertainty as departure_partner_balance_1789_uncertainty, d.partner_balance_supp_1789_uncertainty as departure_partner_balance_supp_1789_uncertainty,
                    d.pointcall_status as departure_status,
                    d.shiparea as departure_shiparea,
                    d.pointcall_point as departure_point,
                    t.departure_out_date,
                    t.departure_action,
                    t.outdate_fixed,
                    t.departure_navstatus,
                    t.departure_function,
                    t.destination,
                    a.toponyme_fr as destination_fr, a.toponyme_en as destination_en,
                    a.pointcall_uhgs_id  as destination_uhgs_id,
                    a.latitude as destination_latitude,
                    a.longitude as destination_longitude,
                    a.pointcall_admiralty as destination_admiralty,
                    a.pointcall_province as destination_province,
                    a.pointcall_states as destination_states,
                    a.pointcall_substates as destination_substates,
                    a.state_1789_fr as destination_state_1789_fr, a.substate_1789_fr as destination_substate_1789_fr, a.state_1789_en as destination_state_1789_en, a.substate_1789_en as destination_substate_1789_en,
                    a.ferme_direction as destination_ferme_direction, a.ferme_bureau as destination_ferme_bureau, a.ferme_bureau_uncertainty as destination_ferme_bureau_uncertainty, 
                    a.partner_balance_1789 as destination_partner_balance_1789, a.partner_balance_supp_1789 as destination_partner_balance_supp_1789, 
                    a.partner_balance_1789_uncertainty as destination_partner_balance_1789_uncertainty, a.partner_balance_supp_1789_uncertainty as destination_partner_balance_supp_1789_uncertainty,
                    a.shiparea as destination_shiparea,
                    a.pointcall_status as destination_status,
                    a.pointcall_point as destination_point,
                    t.destination_in_date,
                    t.destination_action,
                    t.indate_fixed,
                    t.destination_navstatus,
                    t.destination_function,
                    d.ship_name,
                    t.ship_id,
                    a.tonnage,
                    a.tonnage_unit,
                    a.tonnage_class,
                    a.in_crew,
                    a.flag,
                    a.ship_flag_id,
                    a.ship_flag_standardized_fr, a.ship_flag_standardized_en, 
                    a.class,
                    a.homeport,
                    a.homeport_uhgs_id , -- manque la suite
                    a.homeport_latitude,
                    a.homeport_longitude,
                    a.homeport_admiralty,
                    a.homeport_province,
                    a.homeport_states,
                    a.homeport_substates,
                    a.homeport_status,
                    a.homeport_shiparea,
                    a.homeport_point,
                    a.homeport_state_1789_fr,
                    a.homeport_substate_1789_fr,
                    a.homeport_state_1789_en,
                    a.homeport_substate_1789_en,
                    a.homeport_toponyme_fr,
                    a.homeport_toponyme_en,
                    'both-to' as source_entry, 
                    a.source_doc_id,
                    a.source_text,
                    a.source_suite,
                    a.source_component,
                    a.source_number,
                    a.source_other,
                    a.source_main_port_uhgs_id, 
                    a.source_main_port_toponyme,
                    a.source_subset,
                    a.captain_id,
                    a.captain_name,
                    a.birthplace,
                    a.status,
                    a.citizenship,
                    a.commodity_purpose,
                    a.commodity_id,
                    a.quantity,
                    a.quantity_u,
                    a.commodity_standardized,
                    a.commodity_standardized_fr,
                    a.commodity_permanent_coding,
                    a.commodity_purpose2,
                    a.commodity_id2,
                    a.quantity2,
                    a.quantity_u2,
                    a.commodity_standardized2,
                    a.commodity_standardized2_fr,
                    a.commodity_permanent_coding2,
                    a.commodity_purpose3,
                    a.commodity_id3,
                    a.quantity3,
                    a.quantity_u3,
                    a.commodity_standardized3,
                    a.commodity_standardized3_fr,
                    a.commodity_permanent_coding3,
                    a.commodity_purpose4,
                    a.commodity_id4,
                    a.quantity4,
                    a.quantity_u4,
                    a.commodity_standardized4,
                    a.commodity_standardized4_fr,
                    a.commodity_permanent_coding4,
                    a.all_cargos,
                    d.tax_concept,
                    a.payment_date,
                    a.q01, a.q01_u , a.q02 , a.q02_u , a.q03 , a.q03_u ,
                    a.all_taxes,
                    a.ship_uncertainity,
                    a.tonnage_uncertainity ,
                    a.flag_uncertainity,
                    a.homeport_uncertainity ,
                    t.departure_uncertainity,
                    t.destination_uncertainity,
                    a.captain_uncertainity,
                    t.travel_uncertainity, 
                    a.cargo_uncertainity,
                    a.taxe_uncertainity,
                    a.pointcall_outdate_uncertainity
                    from 
                    navigoviz.debug_travels t,
                    navigoviz.pointcall d,
                    navigoviz.pointcall a
                    where t.departure_pkid = d.pkid and t.destination_pkid = a.pkid and t.doc_destination != t.doc_depart
                    order by id,  coalesce (departure_out_date, destination_in_date)
                    )
	
        )
-- 74609

select distinct id from navigoviz.built_travels 
where source_entry ='from'
        
 ---------------
  alter table navigoviz.built_travels rename column id  to travel_id  

 alter table navigoviz.built_travels add column distance_dep_dest_miles text
 alter table navigoviz.built_travels add column distance_homeport_dep_miles text
 
 
 update navigoviz.built_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                    SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,   stages__length_miles
                    FROM navigoviz.built_travels t , navigo.stages s 
                    where stages__length_miles is not null and stages__length_miles != '0.0' and t.departure_uhgs_id = s.pointcall_uhgs_id and t.destination_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
        
update navigoviz.built_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,  stages__length_miles
                   FROM navigoviz.built_travels t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.destination_uhgs_id = s.pointcall_uhgs_id and t.departure_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
        
       
       
update navigoviz.built_travels t set distance_homeport_dep_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                    SELECT t.travel_id, t.homeport, t.homeport_uhgs_id, t.departure, t.departure_uhgs_id,   stages__length_miles
                    FROM navigoviz.built_travels t , navigo.stages s 
                    where stages__length_miles is not null and stages__length_miles != '0.0' and t.homeport_uhgs_id = s.pointcall_uhgs_id and t.departure_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id;
 -- 14488       
update navigoviz.built_travels t set distance_homeport_dep_miles = q.distance_dep_dest_miles
        from (
                select distinct k.travel_id, stages__length_miles::float as  distance_dep_dest_miles from (
                   SELECT t.travel_id, t.homeport, t.homeport_uhgs_id, t.departure, t.departure_uhgs_id, stages__length_miles
                   FROM navigoviz.built_travels t ,navigo.stages s 
                   where stages__length_miles is not null and stages__length_miles != '0.0' and t.departure_uhgs_id = s.pointcall_uhgs_id and t.homeport_uhgs_id = s.next_point__pointcall_uhgs_id 
                ) as k
        ) as q
        where q.travel_id = t.travel_id
 -- 21387
        
select count(*) from built_travels bt where distance_homeport_dep_miles is null -- 48900 
select count(*) from built_travels bt where distance_dep_dest_miles is null -- 11180 


select count(*) from built_travels bt where source_entry != 'both-to'

select distinct tonnage_unit, count(*) 
from built_travels bt 
where tonnage is not null
group by tonnage_unit
-- where source_entry != 'both-to'
--quintaux	371
--quintaux]	1
--Ton	2
--tx	66749
--Tx	64

select distinct tonnage_unit from pointcall p2  where source_entry != 'both-to'
select case tonnage_unittonnage_unit from pointcall p2  where source_entry != 'both-to'

select ship_id, min(tonnage), max(tonnage), avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
from 
(
select ship_id, case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
from built_travels bt
where tonnage is not null
) as k 
group by ship_id
having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 50 and max(tonnage) > 20

-- update pointcall set tonnage_uncertainity = -1
select ship_id, min(tonnage), max(tonnage), avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
from 
(
select ship_id, case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
from pointcall 
where tonnage is not null and (source_main_port_toponyme ='Marennes' or source_main_port_toponyme ='Nantes')
) as k 
group by ship_id
having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20

-- mettre dans le code
update pointcall p set tonnage_uncertainity = -1 from 
(

	select ship_id, min(tonnage), max(tonnage), avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
	from 
	(
	select ship_id, case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
	from pointcall 
	where tonnage is not null 
	-- and (source_main_port_toponyme ='Marennes' or source_main_port_toponyme ='Nantes')
	) as k 
	group by ship_id
	having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20


) as k 
where p.ship_id = k.ship_id and tonnage_uncertainity<> -2
-- 3124 lignes

(source_main_port_toponyme !='Marennes' or source_main_port_toponyme !='Nantes')

select up.pkid, up.ship_tonnage , p.tonnage_uncertainity , p.tonnage 
from navigocheck.uncertainity_pointcall up , navigoviz.pointcall p
where up.pkid = p.pkid
and up.ship_tonnage = -2 and p.tonnage_uncertainity = -1

update navigoviz.pointcall set tonnage_uncertainity = 0 where tonnage_uncertainity = -1
-- 3205

update navigoviz.pointcall p set tonnage_uncertainity = -2
from navigocheck.uncertainity_pointcall up 
where up.pkid = p.pkid and up.ship_tonnage = -2 and p.tonnage_uncertainity = -1
-- 12
update navigoviz.pointcall p set tonnage_uncertainity = -4
from navigocheck.uncertainity_pointcall up 
where up.pkid = p.pkid and up.ship_tonnage = -4 and p.tonnage_uncertainity = -1
-- 4

-- exclure les congés pris à Marennes ou à Nantes (166 cas de navires)
update pointcall p set tonnage_uncertainity = -1 from 
(
	select ship_id, min(tonnage), max(tonnage), avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
	from 
	(
	select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
	from navigoviz.pointcall p
	where tonnage is not null 
	--and (pointcall ='Marennes' or pointcall ='Nantes')
	-- and (pointcall_uhgs_id != 'A0136930' and pointcall_uhgs_id != 'A0124817')
	and (source_main_port_toponyme !='Marennes' and source_main_port_toponyme !='Nantes')
	) as k 
	group by ship_id
	having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20
) as k 
where p.ship_id = k.ship_id and tonnage_uncertainity<> -2
-- 2281

-- Comparer les congés de Nantes et Marennes avec les autres
-- Si Exceptionnels (165 % plus élevés) alors marquer ceux là aussi comme incertains (13 cas de navires)
update pointcall p set tonnage_uncertainity = -1 from 
(

	select marennes.ship_id
	from 
		(select ship_id,  avg(tonnage) as moyenneMarennesNantes, max(tonnage) as maxMarennesNantes
			from 
			(
			select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
			from navigoviz.pointcall p
			where tonnage is not null 
			--and (pointcall ='Marennes' or pointcall ='Nantes')
			-- and (pointcall_uhgs_id = 'A0136930' or pointcall_uhgs_id = 'A0124817')
			and (source_main_port_toponyme ='Marennes' or source_main_port_toponyme ='Nantes')
			) as k 
			group by ship_id
		) as marennes,
	-- having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20

		(select ship_id, min(tonnage), max(tonnage) as maxautres, avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
			from 
			(
			select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
			from navigoviz.pointcall p
			where tonnage is not null 
			--and (pointcall ='Marennes' or pointcall ='Nantes')
			-- and (pointcall_uhgs_id != 'A0136930' and pointcall_uhgs_id != 'A0124817')
			and (source_main_port_toponyme !='Marennes' and source_main_port_toponyme !='Nantes')
			) as k 
			group by ship_id
		) as autres
			-- having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20
	where autres.ship_id = marennes.ship_id and moyenneMarennesNantes > moyenne*1.30 and maxautres > 20
) as k 
where p.ship_id = k.ship_id and tonnage_uncertainity<> -2

-- Dunkerque

select avg(ratio) from (
	select marennes.ship_id, moyenneMarennesNantes, moyenne, moyenneMarennesNantes/moyenne as ratio
	from 
		(select ship_id,  avg(tonnage) as moyenneMarennesNantes, max(tonnage) as maxMarennesNantes
			from 
			(
			select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
			from navigoviz.pointcall p
			where tonnage is not null and ship_id is not null
			--and (pointcall ='Marennes' or pointcall ='Nantes')
			-- and (pointcall_uhgs_id = 'A0136930' or pointcall_uhgs_id = 'A0124817')
			and (source_main_port_toponyme ='Nantes' )
			) as k 
			group by ship_id
		) as marennes,
	-- having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20

		(select ship_id, min(tonnage), max(tonnage) as maxautres, avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
			from 
			(
			select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
			from navigoviz.pointcall p
			where tonnage is not null and ship_id is not null
			--and (pointcall ='Marennes' or pointcall ='Nantes')
			-- and (pointcall_uhgs_id != 'A0136930' and pointcall_uhgs_id != 'A0124817')
			and (source_main_port_toponyme !='Nantes' )
			) as k 
			group by ship_id
		) as autres
			-- having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20
	where autres.ship_id = marennes.ship_id 
	-- and moyenneMarennesNantes > moyenne and maxautres > 20
) as k


select distinct ship_id from navigoviz.pointcall p 
where ship_id in ('0000599N', '0001524N', '0002537N', '0003754N', '0007523N', '0009618N', '0009651N')
and tonnage_uncertainity <> -1



select ship_id, ship_name, arrivee_shipname, depart_captainname, arrivee_captainname, 
        departure_function,
        destination_function,
        departure, destination, 
        id, departure_uhgs_id,
        depart_homeport, arrivee_homeport
        from navigoviz.uncertainity_travels 
        where not (ship_name % arrivee_shipname) --and depart_net_route_marker = 'Z'
        and depart_homeportid != arrivee_homeportid 
        and not (depart_captainname % arrivee_captainname) 
        and position(',' in depart_captainname) > 0 and position(',' in arrivee_captainname) > 0
        and not (substring(depart_captainname from 0 for position(',' in depart_captainname)) % substring(arrivee_captainname from 0 for position(',' in arrivee_captainname)))
        
        ) as k 
        where p.pointcall_rankfull = k.id and p.ship_id = k.ship_id and p.pointcall_uhgs_id = k.departure_uhgs_id and p.ship_uncertainity <> -2


select  pointcall_uncertainity, count(*)
from navigoviz.pointcall p
group by pointcall_uncertainity order by pointcall_uncertainity
-3	9
-1	39875
0	68646

select  tonnage_uncertainity , count(*)
from navigoviz.pointcall p
group by tonnage_uncertainity order by tonnage_uncertainity

-4	25491
-3	9
-2	1114
-1	2416
0	79500

select  ship_uncertainity , count(*)
from navigoviz.pointcall p
group by ship_uncertainity order by ship_uncertainity
-4	34334
-3	9
-1	12
0	74175

select  captain_uncertainity , count(*)
from navigoviz.pointcall p
group by captain_uncertainity order by captain_uncertainity

-4	8
-3	9
-2	29
-1	601
0	107883

select  flag_uncertainity , count(*)
from navigoviz.pointcall p
group by flag_uncertainity order by flag_uncertainity
-4	10382
-3	9
-2	67787
-1	2
0	30350

select  homeport_uncertainity , count(*)
from navigoviz.pointcall p
group by homeport_uncertainity order by homeport_uncertainity

-4	34519
-3	9
-2	17375
-1	263
0	56364

select  pointcall_outdate_uncertainity , count(*)
from navigoviz.pointcall p
group by pointcall_outdate_uncertainity order by pointcall_outdate_uncertainity

-3	147
-1	82213
0	26170

select  cargo_uncertainity , count(*)
from navigoviz.pointcall p
group by cargo_uncertainity order by cargo_uncertainity
-4	28051
-3	9
-2	1913
0	78557

select  taxe_uncertainity , count(*)
from navigoviz.pointcall p
group by taxe_uncertainity order by taxe_uncertainity
-4	27319
-3	9
0	81202

select ship_id, tonnage_uncertainity from navigoviz.pointcall p where ship_id in ('0006487N', '0009209N', '0015899N', '0000116N', '0001064N', '0001066N')


select p.* from 
navigocheck.uncertainity_pointcall up, navigo.pointcall p 
where up.pointcall_name = -2 and p.pkid = up.pkid

select pointcall, pointcall_uncertainity from navigoviz.pointcall where pointcall_uhgs_id = 'H9999999' and pointcall_uncertainity = -1 and pointcall is null
update navigoviz.pointcall set pointcall_uncertainity = -4 where (pointcall_uhgs_id = 'H4444444' or pointcall_uhgs_id = 'H9999999') 
and pointcall_uncertainity = 0

select source_suite , * from navigoviz.pointcall where navigo_status like 'FC%' and pointcall_function = 'O'

select  record_id , * from navigo.pointcall where pointcall_status  like 'FC%' and pointcall_function = 'O' and captain_name not  like '(%'
and data_block_leader_marker = 'A'

select * from navigo.pointcall where data_block_local_id in (
select  data_block_local_id from navigo.pointcall where pointcall_status  like 'FC%' and pointcall_function = 'O' and data_block_leader_marker != 'A')
order by data_block_local_id, pointcall_rank 

select distinct source_subset  from navigoviz.pointcall p 

-- exemple de travel pour doc API
select travel_id,distance_homeport_dep,distance_dep_dest,travel_rank,distance_dep_dest_miles,distance_homeport_dep_miles,departure,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_states,departure_substates,departure_status,departure_shiparea,departure_state_1789_fr,departure_substate_1789_fr,departure_state_1789_en,departure_substate_1789_en,departure_ferme_direction,departure_ferme_bureau,departure_ferme_bureau_uncertainty,departure_partner_balance_1789,departure_partner_balance_supp_1789,departure_partner_balance_1789_uncertainty,departure_partner_balance_supp_1789_uncertainty,departure_source_1787_available,departure_source_1789_available,departure_fr,departure_en,departure_point,departure_pkid,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_states,destination_substates,destination_status,destination_shiparea,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_source_1787_available,destination_source_1789_available,destination_fr,destination_en,destination_point,destination_pkid,destination_action,destination_in_date,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_states,homeport_substates,homeport_status,homeport_shiparea,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_number,source_other,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,status,citizenship,commodity_purpose,commodity_id,quantity,quantity_u,commodity_standardized,commodity_standardized_fr,commodity_permanent_coding,all_cargos,commodity_purpose2,commodity_id2,quantity2,quantity_u2,commodity_standardized2,commodity_standardized2_fr,commodity_permanent_coding2,commodity_purpose3,commodity_id3,quantity3,quantity_u3,commodity_standardized3,commodity_standardized3_fr,commodity_permanent_coding3,commodity_purpose4,commodity_id4,quantity4,quantity_u4,commodity_standardized4,commodity_standardized4_fr,commodity_permanent_coding4,tax_concept,payment_date,q01,q01_u,q02,q02_u,q03,q03_u,all_taxes,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity
from navigoviz.built_travels  
where travel_id = '0008150N- 01'


select * from ports.port_points pp where uhgs_id = 'A0630182'

select homeport , homeport_state_1789_en , homeport_state_1789_fr , homeport_substate_1789_en , homeport_substate_1789_fr from navigoviz.pointcall p where pkid = 55869

-- exemple de pointcall pour doc API
select pkid,record_id,pointcall,pointcall_uhgs_id,latitude,longitude,pointcall_admiralty,pointcall_province,pointcall_states,pointcall_substates,pointcall_status,shiparea,pointcall_point,pointcall_out_date,pointcall_action,outdate_fixed,pointcall_in_date,indate_fixed,net_route_marker,pointcall_rankfull,date_fixed,navigo_status,pointcall_function,data_block_leader_marker,source_1787_available,source_1789_available,toponyme_fr,toponyme_en,ferme_direction,ferme_bureau,ferme_bureau_uncertainty,partner_balance_1789,partner_balance_supp_1789,partner_balance_1789_uncertainty,partner_balance_supp_1789_uncertainty,state_1789_fr,substate_1789_fr,state_1789_en,substate_1789_en,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_states,homeport_substates,homeport_status,homeport_shiparea,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,homeport_states_en,homeport_substates_en,homeport_source_1787_available,homeport_source_1789_available,homeport_ferme_direction,homeport_ferme_bureau,homeport_ferme_bureau_uncertainty,homeport_partner_balance_1789,homeport_partner_balance_supp_1789,homeport_partner_balance_1789_uncertainty,homeport_partner_balance_supp_1789_uncertainty,source_doc_id,source_text,source_suite,source_component,source_number,source_other,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,status,citizenship,commodity_purpose,commodity_id,quantity,quantity_u,commodity_standardized,commodity_standardized_fr,commodity_permanent_coding,all_cargos,commodity_purpose2,commodity_id2,quantity2,quantity_u2,commodity_standardized2,commodity_standardized2_fr,commodity_permanent_coding2,commodity_purpose3,commodity_id3,quantity3,quantity_u3,commodity_standardized3,commodity_standardized3_fr,commodity_permanent_coding3,commodity_purpose4,commodity_id4,quantity4,quantity_u4,commodity_standardized4,commodity_standardized4_fr,commodity_permanent_coding4,tax_concept,payment_date,q01,q01_u,q02,q02_u,q03,q03_u,all_taxes,ship_uncertainity,tonnage_uncertainity,homeport_uncertainity,pointcall_uncertainity,captain_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity
from navigoviz.pointcall p  
where 
-- source_text = 'ADMorbihan, 10B-19/ fol.122'
source_doc_id = '00180090'

-- exemple de port pour doc API

select ogc_fid,uhgs_id,toponyme,belonging_states,belonging_substates,geonameid,amiraute,province,shiparea,status,toustopos,topofreq,geom,point3857,country2019_name,country2019_iso2code,country2019_region,oblique,relation_state,has_a_clerk,source_1787_available,source_1789_available,belonging_states_en,belonging_substates_en,toponyme_standard_fr,toponyme_standard_en,ferme_direction,ferme_bureau,ferme_bureau_uncertainty,partner_balance_1789,partner_balance_supp_1789,partner_balance_1789_uncertainty,partner_balance_supp_1789_uncertainty,state_1789_fr,substate_1789_fr,state_1789_en,substate_1789_en
from ports.port_points pp   
where toponyme_standard_fr = 'Marennes'

GRANT USAGE ON SCHEMA navigoviz TO porticapi;
GRANT USAGE ON SCHEMA navigocheck to porticapi;
GRANT USAGE ON SCHEMA navigo to porticapi;
GRANT USAGE ON SCHEMA public to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO porticapi;
grant CONNECT on database portic_v6 to porticapi;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi;

ALTER USER porticapi WITH PASSWORD 'portic';
ALTER USER api_user WITH PASSWORD 'portic';


GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigocheck to api_user;
GRANT USAGE ON SCHEMA navigo to api_user;
GRANT USAGE ON SCHEMA public to api_user;
GRANT USAGE ON SCHEMA ports to api_user;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO api_user;
grant CONNECT on database portic_v6 to api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to api_user;

select postgis_version() 

drop extension pgRouting

ALTER ROLE api_user NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;

ALTER ROLE porticapi NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;

GRANT create, USAGE ON SCHEMA navigoviz TO navigo;
GRANT create,USAGE ON SCHEMA navigocheck to navigo;
GRANT create,USAGE ON SCHEMA navigo to navigo;
GRANT create,USAGE ON SCHEMA public to navigo;
GRANT create,USAGE ON SCHEMA ports to navigo;
GRANT ALL ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO navigo;
grant CONNECT on database portic_v6 to navigo;
grant ALL on all sequences in schema navigoviz, navigo, navigocheck, ports, public to navigo;

GRANT create, USAGE ON SCHEMA navigoviz TO dba;
GRANT create,USAGE ON SCHEMA navigocheck to dba;
GRANT create,USAGE ON SCHEMA navigo to dba;
GRANT create,USAGE ON SCHEMA public to dba;
GRANT create,USAGE ON SCHEMA ports to dba;
GRANT ALL ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO dba;
grant CONNECT on database portic_v6 to dba;
grant ALL on all sequences in schema navigoviz, navigo, navigocheck, ports, public to dba;

select count(*) from navigoviz.pointcall
-- 108530

select count(*), source_entry from navigoviz.built_travels bt 
group by source_entry

select 23855 + 26719+ 23855
http://data.portic.fr/api/travels/?params=departure_state_1789_fr,departure_admiralty,departure_province,departure_states,outdate_fixed,destination,destination_admiralty,destination_province,destination_states,tonnage,tonnage_class,flag,homeport,homeport_admiralty,homeport_province,homeport_states,homeport_uncertainity,source_suite,departure_action,destination_action&format=csv

select count(*), substring(departure_out_date for 4) from navigoviz.built_travels bt 
where 
-- destination like 'Santo Do%'
destination_fr  like 'Saint-Domingue' and source_entry <> 'both-to'
-- and substring(departure_out_date from 0 for 5) = '1787'
group by substring(departure_out_date for 4)

-- 132

select count(*) from navigoviz.built_travels bt 
where source_entry <> 'both-to'and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')

set role porticapi

select count(*) from navigoviz.built_travels bt 
where source_entry <> 'both-to'and (substring(departure_out_date for 4) = '1789' or substring(destination_in_date for 4) = '1789')

select * from built_travels bt 
where bt.outdate_fixed = '1789-10-26'

0021542N- 30

select travel_id, distance_dep_dest,distance_dep_dest_miles,departure_admiralty,departure_province,departure_states,outdate_fixed,destination,destination_admiralty,destination_province,destination_states,tonnage,tonnage_class,flag,homeport,homeport_admiralty,homeport_province,homeport_states,homeport_uncertainity,source_suite,departure_action,destination_action from navigoviz.built_travels where source_entry <> 'both-to' and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')
-- and travel_id = '0021542N- 30'
select count(*) from built_travels bt 
where destination_fr  like 'Saint-Domingue' and substring(departure_out_date for 4) = '1787'
group by substring(departure_out_date from 0 for 5)



select * from navigocheck.check_pointcall cp where ship_tonnage <> '196,5' and ship_tonnage::float < 0

select * from navigoviz.pointcall cp where tonnage::float < 0


select distinct source_component, source_main_port_toponyme , source_main_port_uhgs_id , pointcall, pointcall_function 
from navigoviz.pointcall p where source_component like 'ANF, G5, 154-1%'
-- ok : plus de problème

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------

-- préparer l'analyse sous R

----------------------------------------
-- extract data for CSV files
-- 16 nov 2020 (from file R Z.poitou_portic.R)
----------------------------------------

DROP TABLE public.csv_extract;

 create table public.csv_extract as (
 select 1789 as annee, pkid, record_id,
 source_doc_id, source_suite, source_component, source_main_port_uhgs_id, source_main_port_toponyme,
 pointcall_rankfull, data_block_leader_marker, pointcall_action, pointcall_function, pointcall_out_date, pointcall_in_date, outdate_fixed, indate_fixed, net_route_marker, navigo_status,
 pointcall, pointcall_uhgs_id, latitude, longitude, pointcall_admiralty, pointcall_province, shiparea,
 -- pointcall_states, pointcall_substates, 
 pointcall_status, p.source_1789_available as source_available, 
 homeport_uhgs_id , homeport,  homeport_latitude, homeport_longitude, homeport_admiralty, homeport_province, homeport_states, homeport_substates, homeport_status, homeport_shiparea,
 ship_id , flag, ship_flag_id , ship_flag_standardized_fr, ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit ,
 captain_id, captain_name, birthplace, status, citizenship, in_crew,
 json_array_length(all_cargos::json) as nb_products, 
 commodity_permanent_coding, commodity_purpose, commodity_standardized, commodity_standardized_fr, quantity , 
 commodity_permanent_coding2, commodity_purpose2, commodity_standardized2, commodity_standardized2_fr, quantity2, 
 commodity_permanent_coding3, commodity_purpose3, commodity_standardized3, commodity_standardized3_fr, quantity3, 
 commodity_permanent_coding4, commodity_purpose4, commodity_standardized4, commodity_standardized4_fr, quantity4,
 json_array_length(all_taxes::json) as nb_taxes,
 tax_concept , q01, q02, q03, ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe,
 
 (all_taxes->>2)::json->>'tax_concept' as tax_concept2, (all_taxes->>2)::json->>'q01' as t2q01, (all_taxes->>2)::json->>'q02' as t2q02, (all_taxes->>2)::json->>'q03' as t2q03,
 ((case when (all_taxes->>2)::json->>'q01' is null then 0 else ((all_taxes->>2)::json->>'q01')::float*240 end) + (case when (all_taxes->>2)::json->>'q02' is null then 0 else ((all_taxes->>2)::json->>'q02')::float*12 end) + (case when (all_taxes->>2)::json->>'q03' is null then 0 else ((all_taxes->>2)::json->>'q03')::float end)) as taxe2,
 
 (all_taxes->>3)::json->>'tax_concept' as tax_concept3,
 ((case when (all_taxes->>3)::json->>'q01' is null then 0 else ((all_taxes->>3)::json->>'q01')::float*240 end) + (case when (all_taxes->>3)::json->>'q02' is null then 0 else ((all_taxes->>3)::json->>'q02')::float*12 end) + (case when (all_taxes->>3)::json->>'q03' is null then 0 else ((all_taxes->>3)::json->>'q03')::float end)) as taxe3,

 (all_taxes->>4)::json->>'tax_concept' as tax_concept4,
  ((case when (all_taxes->>4)::json->>'q01' is null then 0 else ((all_taxes->>4)::json->>'q01')::float*240 end) + (case when (all_taxes->>4)::json->>'q02' is null then 0 else ((all_taxes->>4)::json->>'q02')::float*12 end) + (case when (all_taxes->>4)::json->>'q03' is null then 0 else ((all_taxes->>4)::json->>'q03')::float end)) as taxe4,

 (all_taxes->>5)::json->>'tax_concept' as tax_concept5,
  ((case when (all_taxes->>5)::json->>'q01' is null then 0 else ((all_taxes->>5)::json->>'q01')::float*240 end) + (case when (all_taxes->>5)::json->>'q02' is null then 0 else ((all_taxes->>5)::json->>'q02')::float*12 end) + (case when (all_taxes->>5)::json->>'q03' is null then 0 else ((all_taxes->>5)::json->>'q03')::float end)) as taxe5,

 ship_uncertainity, tonnage_uncertainity, flag_uncertainity, homeport_uncertainity, pointcall_uncertainity, captain_uncertainity, cargo_uncertainity, taxe_uncertainity,
 state_1789_fr as state_1789, homeport_state_1789_fr as homeport_state_1789
 --, state_1789, homeport_state_1787, homeport_state_1789
 , substate_1789_fr, homeport_substate_1789_fr , 
 ferme_bureau , ferme_bureau_uncertainty , ferme_direction , 
 partner_balance_1789 , partner_balance_1789_uncertainty , partner_balance_supp_1789 , partner_balance_supp_1789_uncertainty ,
 homeport_ferme_bureau , homeport_ferme_bureau_uncertainty , homeport_ferme_direction ,
 homeport_partner_balance_1789 , homeport_partner_balance_1789_uncertainty , homeport_partner_balance_supp_1789 , homeport_partner_balance_supp_1789_uncertainty,  
 toponyme_fr , homeport_toponyme_fr ,  source_subset 
 from navigoviz.pointcall p 
 where  (substring(pointcall_out_date for 4)::int = 1789 OR substring(pointcall_in_date for 4)::int= 1789)
 )
 -- 31278 en v5 --> 32589 en V6
 
insert into public.csv_extract  (
 select 1787 as annee, pkid, record_id,
 source_doc_id, source_suite, source_component, source_main_port_uhgs_id, source_main_port_toponyme,
 pointcall_rankfull, data_block_leader_marker, pointcall_action, pointcall_function, pointcall_out_date, pointcall_in_date, outdate_fixed, indate_fixed, net_route_marker, navigo_status,
 pointcall, pointcall_uhgs_id, latitude, longitude, pointcall_admiralty, pointcall_province, shiparea,
 -- pointcall_states, pointcall_substates, 
 pointcall_status, p.source_1789_available as source_available, 
 homeport_uhgs_id , homeport,  homeport_latitude, homeport_longitude, homeport_admiralty, homeport_province, homeport_states, homeport_substates, homeport_status, homeport_shiparea,
 ship_id , flag, ship_flag_id , ship_flag_standardized_fr, ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit ,
 captain_id, captain_name, birthplace, status, citizenship, in_crew,
 json_array_length(all_cargos::json) as nb_products, 
 commodity_permanent_coding, commodity_purpose, commodity_standardized, commodity_standardized_fr, quantity , 
 commodity_permanent_coding2, commodity_purpose2, commodity_standardized2, commodity_standardized2_fr, quantity2, 
 commodity_permanent_coding3, commodity_purpose3, commodity_standardized3, commodity_standardized3_fr, quantity3, 
 commodity_permanent_coding4, commodity_purpose4, commodity_standardized4, commodity_standardized4_fr, quantity4,
 json_array_length(all_taxes::json) as nb_taxes,
 tax_concept , q01, q02, q03, ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe,
 (all_taxes->>2)::json->>'tax_concept' as tax_concept2, (all_taxes->>2)::json->>'q01' as t2q01, (all_taxes->>2)::json->>'q02' as t2q02, (all_taxes->>2)::json->>'q03' as t2q03,
 ((case when (all_taxes->>2)::json->>'q01' is null then 0 else ((all_taxes->>2)::json->>'q01')::float*240 end) + (case when (all_taxes->>2)::json->>'q02' is null then 0 else ((all_taxes->>2)::json->>'q02')::float*12 end) + (case when (all_taxes->>2)::json->>'q03' is null then 0 else ((all_taxes->>2)::json->>'q03')::float end)) as taxe2,
  
 (all_taxes->>3)::json->>'tax_concept' as tax_concept3,
 ((case when (all_taxes->>3)::json->>'q01' is null then 0 else ((all_taxes->>3)::json->>'q01')::float*240 end) + (case when (all_taxes->>3)::json->>'q02' is null then 0 else ((all_taxes->>3)::json->>'q02')::float*12 end) + (case when (all_taxes->>3)::json->>'q03' is null then 0 else ((all_taxes->>3)::json->>'q03')::float end)) as taxe3,

 (all_taxes->>4)::json->>'tax_concept' as tax_concept4,
  ((case when (all_taxes->>4)::json->>'q01' is null then 0 else ((all_taxes->>4)::json->>'q01')::float*240 end) + (case when (all_taxes->>4)::json->>'q02' is null then 0 else ((all_taxes->>4)::json->>'q02')::float*12 end) + (case when (all_taxes->>4)::json->>'q03' is null then 0 else ((all_taxes->>4)::json->>'q03')::float end)) as taxe4,

 (all_taxes->>5)::json->>'tax_concept' as tax_concept5,
  ((case when (all_taxes->>5)::json->>'q01' is null then 0 else ((all_taxes->>5)::json->>'q01')::float*240 end) + (case when (all_taxes->>5)::json->>'q02' is null then 0 else ((all_taxes->>5)::json->>'q02')::float*12 end) + (case when (all_taxes->>5)::json->>'q03' is null then 0 else ((all_taxes->>5)::json->>'q03')::float end)) as taxe5,

 ship_uncertainity, tonnage_uncertainity, flag_uncertainity, homeport_uncertainity, pointcall_uncertainity, captain_uncertainity, cargo_uncertainity, taxe_uncertainity,
 state_1789_fr as state_1789, homeport_state_1789_fr as homeport_state_1789
 --, state_1789, homeport_state_1787, homeport_state_1789
 , substate_1789_fr, homeport_substate_1789_fr , 
 ferme_bureau , ferme_bureau_uncertainty , ferme_direction , 
 partner_balance_1789 , partner_balance_1789_uncertainty , partner_balance_supp_1789 , partner_balance_supp_1789_uncertainty ,
 homeport_ferme_bureau , homeport_ferme_bureau_uncertainty , homeport_ferme_direction ,
 homeport_partner_balance_1789 , homeport_partner_balance_1789_uncertainty , homeport_partner_balance_supp_1789 , homeport_partner_balance_supp_1789_uncertainty,  
 toponyme_fr , homeport_toponyme_fr ,  source_subset 
 from navigoviz.pointcall p 
 where  (substring(pointcall_out_date for 4)::int = 1787 OR substring(pointcall_in_date for 4)::int= 1787)
 )
 -- 75815 en v5 --> 75941 en v6
 
 grant select on public.csv_extract to api_user;
  grant select on public.csv_extract to porticapi;
 
 select * from navigoviz.pointcall where tonnage_unit  = 'quintaux]'
 select tonnage, tonnage_unit from navigoviz.pointcall where pkid  = 61868

 select count(*) from navigoviz.pointcall where homeport_uncertainity = -1
 
 select toponyme_standard_fr, uhgs_id, province, amiraute, status, has_a_clerk, source_1787_available , source_1789_available,  state_1789_fr, substate_1789_fr 
from ports.port_points pp 
where pp.state_1789_fr like 'France'
order by state_1789_fr, substate_1789_fr , province, amiraute

alter table public.csv_extract add column if not exists ship_class_codage  text;

select pkid, ship_class,pscc.ship_class_codage  from public."public.ship_class_codage" pscc where ship_class is not null 
update  public.csv_extract p set ship_class_codage = pscc.ship_class_codage from public."public.ship_class_codage" pscc where p.pkid = pscc.pkid

 select pkid, record_id, source_doc_id, source_suite, ship_id, tonnage::float, tonnage_unit, tonnage_uncertainity, source_main_port_toponyme, ship_class, in_crew 
            from public.csv_extract
            
 SELECT pointcall_function, pointcall, state_1789, flag, ship_flag_id, homeport, homeport_uhgs_id , homeport_state_1789
 FROM public.csv_extract 
 WHERE annee = 1787 and pointcall_function<>'O' and flag <> 'French'
 
 -- state_1787 is really different from homeport_state_1787
  SELECT record_id, ship_id, pointcall_function, pointcall, state_1789, flag, ship_flag_id, homeport, homeport_uhgs_id , homeport_state_1789
 FROM public.csv_extract 
 WHERE annee = 1787 and pointcall_function<>'O' and flag <> 'French' and state_1789<>homeport_state_1789 and pointcall='Flessingue'
 -- A	Siculiana	Royaume de Naples	Genoese	A0219524	Genoa	A0232521	République de Gênes
 -- T	Flessingue	Provinces-Unies	Prussian	A1156318	Emden	A0752869	Prusse

 
 SELECT record_id, ship_id, pointcall_function, pointcall, state_1789, flag, ship_flag_id, homeport, homeport_uhgs_id , homeport_state_1789
 FROM public.csv_extract 
 WHERE annee = 1789 and pointcall_function<>'O' and flag <> 'French' and state_1789<>homeport_state_1789 
 -- 78 --> 211
 
 select count(ship_flag_id) 
 from public.csv_extract  WHERE annee = 1789
 and ship_flag_id is not null and flag is not null
 -- 18486 , 18486 --> 27155
 
 select count(ship_flag_id) 
 from public.csv_extract  WHERE annee = 1787
 and ship_flag_id is not null and flag is not null
 -- 69980, 69980 --> 70193
 
 -- 1789 
  select count(record_id) 
 from public.csv_extract  WHERE annee = 1789
 and homeport is  null and homeport_uhgs_id is  null 
 -- 6587 null (homeport_uhgs_id pas codés) +  11374 non null =  17961 homeport not null
 -- 13486 (homeport null) + 19098 non null 
 
 
 
 select count(record_id) 
 from public.csv_extract  WHERE annee = 1787
 and homeport is not null and homeport_uhgs_id is not  null 
 -- 12 null (homeport_uhgs_id pas codés) +  54754 non null =  54766 homeport not null
 -- 0 null + 54908 non null
 
   select distinct homeport, homeport_uhgs_id
   select count(homeport)
 from public.csv_extract  
 where homeport is not null and homeport_uhgs_id is null 
 -- 0
 
 homeport_state_1789
 select count(homeport)
 from public.csv_extract  
 where homeport is not null and homeport_uhgs_id is not null and homeport_state_1789 is null
 -- 4589
 
  select distinct homeport, homeport_uhgs_id
 from public.csv_extract  
 where homeport is not null and homeport_uhgs_id is not null and homeport_state_1789 is null
 --- 297
 
 select * from navigo.geo_general gg where gg.pointcall_uhgs_id in (
 select distinct homeport_uhgs_id
 from navigoviz.pointcall p  
 where homeport is not null and homeport_uhgs_id is not null 
 and homeport_uhgs_id <> 'H4444444' and homeport_uhgs_id <> 'H9999999' and homeport_uhgs_id <> 'H8888888'
 and p.homeport_latitude is null
 order by homeport_uhgs_id
 ) -- 295 distinct uhgs_id hors H4444444, H8888888 / 573 avec homeport
 
 p.homeport_state_1789_fr is null
 -- 635
 
 select distinct name, latitude, longitude, uhgs_id from ports.new_geo_general gg where gg.uhgs_id in (
	 select distinct homeport_uhgs_id
	 from navigoviz.pointcall p  
	 where homeport is not null and homeport_uhgs_id is not null 
	 and homeport_uhgs_id <> 'H4444444' and homeport_uhgs_id <> 'H9999999' and homeport_uhgs_id <> 'H8888888'
	 and p.homeport_latitude is null
	 order by homeport_uhgs_id
 ) 
 -- and uhgs_id = 'A0212540' -- Bayeux, absent
 -- and uhgs_id = 'B1969630' -- Le carénage
 -- and uhgs_id = 'B2044556' -- Le Môle Saint-Nicolas (Saint-Domingue) 
 -- and uhgs_id = 'B2048242' -- Fort-Dauphin (B2048242)
 -- and uhgs_id = 'B2049573' -- Petit-Goave (B2049573)
 -- and  name = 'Port-de-Paix'
 and  name like 'Port%de%Paix%'

 -- Bayeux
  select distinct name, latitude, longitude, mgrs_id, uhgs_id from ports.new_geo_general gg where uhgs_id = 'A0212540'
-- Bayeux	49.266667	-0.7	30UXV6732159646P A0212540

  -- Le carénage
  select distinct name, latitude, longitude, mgrs_id, uhgs_id from ports.new_geo_general gg where uhgs_id = 'B1969630'
--Castries	14.0	-61.0	20PQA1602148639P B1969630
--Port Castries	14.0	-61.0	20PQA1602148639P B1969630

  -- Le Môle Saint-Nicolas (Saint-Domingue) 
  select distinct name, latitude, longitude, mgrs_id, uhgs_id from ports.new_geo_general gg where uhgs_id = 'B2044556'
--Mole (Le)	19.8	-73.383333	B2044556
--Mole Saint-Nicolas	19.8	-73.383333	18QXG6934290159P B2044556
--Mole Saint Nicolas (Le)	19.8	-73.383333	18QXG6934290159P B2044556

  -- Fort-Dauphin (B2048242)
  select distinct name, latitude, longitude, mgrs_id, uhgs_id from ports.new_geo_general gg where uhgs_id = 'B2048242'
--Fort-Dauphin	19.667778	-71.839722	19QBB0223777203P B2048242
--Fort Liberte	19.667778	-71.839722	19QBB0223777203P B2048242
--Ville de Fort-Liberte	19.667778	-71.839722	19QBB0223777203P B2048242

  -- Petit-Goave (B2049573)
    select distinct name, latitude, longitude, mgrs_id, uhgs_id from ports.new_geo_general gg where uhgs_id = 'B2049573'
--Petit Goave	18.431389	-72.866944	18QYF2529739240P	B2049573
--Ville de Petit Goave	18.431389	-72.866944	18QYF2529739240P	B2049573
 
select distinct name, latitude, longitude, mgrs_id, uhgs_id from ports.new_geo_general gg where name like 'Port%de%Paix%'
--Port-de-Paix	19.833333	-72.916667	18QYG1819394384A	B2059844
--Port-de-Paix	19.95	-72.833333	18QYH2675807412P	B2049724
--Port de Paix	19.933333	-72.85	18QYH2503705544B	B2045254
    
  
 select distinct ship_name , count(ship_name) as c
 from public.csv_extract  WHERE annee = 1789
 group by ship_name
 order by c desc
 -- "Aimable" Indienne
 -- ship_prename <- Aimable ou Marie ou Saint ou Anne ou Baron ou Sainte
 -- ship_sufname <- Indienne ou Lanthicy
 -- Baron de Lanthicy : "Baron" "Lanthicy" 
 
 select distinct tax_concept , count(tax_concept) as c, 'pêche'
 from public.csv_extract  
 group by tax_concept
 -- 268
 
  select distinct in_crew::int , count(in_crew) as c
 from public.csv_extract
 where  in_crew != 'tx'
 -- in_crew <> '(10)'
 group by in_crew::int
 order by in_crew::int
 -- 2:3 / 4,5,6 / 7,8,9 / 10:15 / 15:20/ > 20
 -- histogramme and look after the distribtion
 -- correlation betweeen in_crew and tonnage (a scatter plot, compute the correlation value)
 -- we will need a cut for this variable
 
  select distinct homeport_state_1789 , count(homeport_state_1789) as c
 from public.csv_extract
  where source_subset = 'Poitou_1789'
 group by homeport_state_1789
 order by c desc
 
   select distinct homeport_state_1789 , count(homeport_state_1789) as c
 from public.csv_extract
 where annee = 1789
 group by homeport_state_1789
 order by homeport_state_1789
 
 select homeport, homeport_state_1789
 from public.csv_extract
 where annee = 1789
 and homeport is not null and homeport_state_1789 is not null 
 -- 11 184  / 17961 
 
  select homeport, homeport_state_1789, *
 from public.csv_extract
 -- where annee = 1787
 where homeport is not null and homeport_state_1789 is null 
 
 select homeport, homeport_uhgs_id from navigoviz.pointcall p where pkid = 68642
 -- 1789 and 1787 sont ressemblants
 -- Folkstone 
select * from ports.port_points pp where uhgs_id = 'A1963922'
select * from navigo.geo_general gg  where pointcall_uhgs_id = 'A1963922'
 
   select homeport,homeport_state_1787, homeport_state_1789
 from public.csv_extract
 where 
  homeport is not null  and homeport in (select homeport from public.csv_extract where annee = 1787 and homeport_state_1787 is not null)
  
  
     select homeport,homeport_state_1787, homeport_state_1789
 from public.csv_extract
 where 
  homeport is not null and homeport_state_1789 is null and homeport in (select homeport from public.csv_extract where annee = 1787 and homeport_state_1787 is not null)
  
  select distinct etat, subunit, name_en from ports.etats e 
  order by etat
   select distinct name_en from ports.etats e where name_en is not null

SELECT tax_concept, commodity_purpose , count(*) FROM public.csv_extract 
WHERE tax_concept % 'pêche' or commodity_purpose % 'pêche'
group by tax_concept, commodity_purpose

select postgis_full_version() 

--------------------------
-- 4 mars 2021 : alignement états des homeports et pavillons (flags)
--------------------------

  select distinct homeport_state_1789 , homeport_uhgs_id , count(homeport_state_1789), ship_flag_id, ship_flag_standardized_fr  as c
 from public.csv_extract
 where homeport_state_1789 is not null
 group by homeport_state_1789, homeport_uhgs_id, ship_flag_id, ship_flag_standardized_fr
 order by c desc
 
 insert into ports.generiques_inclusions_geo_csv (toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria, done)
 select distinct  ship_flag_standardized_fr, ship_flag_id, homeport_uhgs_id, homeport_state_1789, 'alignement  flags - homeports (états correspondant) ', count(homeport_state_1789) as c
 from public.csv_extract
 where homeport_state_1789 is not null 
 and ship_flag_standardized_fr = 'français' and homeport_state_1789 = 'France'
 group by homeport_state_1789, homeport_uhgs_id, ship_flag_id, ship_flag_standardized_fr;
 
  insert into ports.generiques_inclusions_geo_csv (toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria, done)
  select distinct  ship_flag_standardized_fr, ship_flag_id, homeport_uhgs_id, homeport_state_1789, 'alignement  flags - homeports (états correspondant) ', count(homeport_state_1789) as c
 from public.csv_extract
 where homeport_state_1789 is not null 
 and (ship_flag_standardized_fr != 'français' and homeport_state_1789 != 'France')
 group by homeport_state_1789, homeport_uhgs_id, ship_flag_id, ship_flag_standardized_fr;
 
 select * from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='A0617755'
 
 delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and toponyme_sup='Pays-Bas autrichien' and toponyme in ('Lubeck', 'Hambourg')
 
 delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='A0642340' and toponyme_sup = 'hollandais'
 
 delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='B0000050' and toponyme_sup = 'britannique'
 
  delete  from ports.generiques_inclusions_geo_csv
 where criteria like 'alignement%' and ughs_id ='A0617755' and toponyme_sup = 'britannique'
 
 
 update ports.generiques_inclusions_geo_csv set toponyme_sup = 'Piombino', ughs_id_sup = null 
 where ughs_id_sup = 'A1964382' and ughs_id = 'A0251559' and toponyme = 'Principauté de Piombino' and criteria like 'alignement%'
-- Pays-Bas autrichien	A0049520	A0686629	Lubeck : lubeckois
-- Pays-Bas autrichien	A0049520	A0743522	Hambourg : hambourgeois
-- napolitain	A1964382	A0251559	Principauté de Piombino
-- hollandais	A0617755	A0642340	Prusse : prussien
-- britannique	A0395415	B0000050	Etats-Unis d'Amérique : etatsunien
-- britannique	A0395415	A0617755	Provinces-Unies : hollandais


select distinct toponyme_sup as ship_flag_standardized_fr, toponyme as homeport_state_fr 
from ports.generiques_inclusions_geo_csv 
 where criteria like 'alignement%'
 
 SELECT count(*), homeport_uncertainity 
 FROM navigoviz.pointcall p 
 group by homeport_uncertainity
 
 SELECT * FROM navigoviz.pointcall WHERE tonnage IS NULL 
 AND ship_id IN (SELECT DISTINCT ship_id FROM navigoviz.pointcall WHERE tonnage IS NOT null and ship_id is not null)
 
 select count(pointcall_uncertainity), pointcall_uncertainity
 FROM navigoviz.pointcall 
 group by pointcall_uncertainity
 
  select count(p."class")
 FROM navigoviz.pointcall p, navigocheck.uncertainity_pointcall cp 
 where p.pkid = cp.pkid and cp.ship_class = 0 and p."class" is null and p.ship_id is not null
 
 
   select count(record_id)
 FROM navigoviz.pointcall p
 where  p."class" is null and p.ship_id is not null and p.pointcall_uncertainity != 0
 -- 28519 si pointcall_uncertainity == 0
 -- 26285 si pointcall_uncertainity != 0
 
 group by pointcall_uncertainity
 
 SELECT tonnage, tonnage_unit 
 FROM navigoviz.pointcall p WHERE p.source_main_port_toponyme LIKE '%Trop%'
 
 select max(tonnage::float) FROM navigoviz.pointcall p WHERE p.source_main_port_toponyme LIKE '%Trop%'
 -- 190
 
 select record_id,  ship_id, ship_name, tonnage::float, tonnage::float/24, tonnage_unit , source_main_port_toponyme 
 from navigoviz.pointcall p where ship_id= '0014429N'
 '0014396N'
 
 '0014429N'
 '0014428N'
 '0014418N' 
select record_id,  ship_id, ship_name, tonnage::float, tonnage_unit , source_main_port_toponyme, captain_name , flag, homeport , p.citizenship , p.birthplace , p."class" 
 from navigoviz.pointcall p where ship_id= '0014429N'
 
 select record_id,  ship_id, ship_name, p.ship_homeport , p.ship_homeport_uhgs_id ,
 captain_name ,  p.captain_citizenship, p.captain_citizenship_id , p.captain_birthplace , p.captain_birthplace_id, p.ship_flag, p.ship_flag_id 
 from navigo.pointcall p 
 where captain_birthplace is not null and captain_birthplace_id is not null
 -- 912 / 19011 lignes
 
 ship_id= '0014429N'
 
 select * from ports.port_points pp where uhgs_id = 'A0199508'
 
 select record_id,  ship_id, ship_name, p.ship_homeport , p.ship_homeport_uhgs_id ,
 captain_name ,  p.captain_citizenship, p.captain_citizenship_id , p.captain_birthplace , p.captain_birthplace_id, p.ship_flag, p.ship_flag_id 
 from navigo.pointcall p 
 where captain_citizenship is not null 
 -- and captain_citizenship_id is not null
 -- 6235 
select * from ports.port_points pp where uhgs_id = 'A0199508'


select distinct e.etat, e.etat_en , e.subunit , e.subunit_en  from ports.etats e 
order by etat, subunit


-- Danemark	Danemark	colonies danoises	Danish colonies
Grande-Bretagne	Great Britain	Irlande	Ireland

select * from ports.etats where etat='Danemark' and etat_en='Danemark' -- Islande
update ports.etats set etat_en='Denmark'  where etat='Danemark' and etat_en='Danemark' 
 
select * from ports.etats where etat='Grande-Bretagne' and etat_en='Great Britain' and subunit='Irlande' and subunit_en='Irland' 
update ports.etats set subunit_en = 'Ireland' where etat='Grande-Bretagne' and etat_en='Great Britain' and subunit='Irlande' and subunit_en='Irland' 
-- and subunit='Irlande' 

-------------------------------------------------
-- Nouveaux attributs
-------------------------------------------------

alter table navigoviz.pointcall add column shipclass_uncertainity int;
alter table navigoviz.pointcall add column birthplace_uhgs_id_uncertainity int ;
alter table navigoviz.pointcall add column birthplace_uncertainity int;
alter table navigoviz.pointcall add column citizenship_uncertainity int;
alter table navigoviz.pointcall add column birthplace_uhgs_id text ;

alter table navigoviz.pointcall add column tax_concept1 text ;
alter table navigoviz.pointcall add column tax_concept2 text ;
alter table navigoviz.pointcall add column tax_concept3 text ;
alter table navigoviz.pointcall add column tax_concept4 text ;
alter table navigoviz.pointcall add column tax_concept5 text ;
alter table navigoviz.pointcall add column taxe_amount01 float ;
alter table navigoviz.pointcall add column taxe_amount02 float ;
alter table navigoviz.pointcall add column taxe_amount03 float ;
alter table navigoviz.pointcall add column taxe_amount04 float ;
alter table navigoviz.pointcall add column taxe_amount05 float ;

update navigoviz.pointcall p set birthplace_uhgs_id=cp.captain_birthplace_id, 
shipclass_uncertainity=up.ship_class,
birthplace_uhgs_id_uncertainity = up.captain_birthplace_id, 
birthplace_uncertainity = up.captain_birthplace,
citizenship_uncertainity = up.captain_citizenship
from navigocheck.uncertainity_pointcall up , navigocheck.check_pointcall cp 
where p.pkid = up.pkid and cp.pkid = p.pkid

select p.pkid, cp.captain_birthplace_id as birthplace_uhgs_id, up.ship_class as shipclass_uncertainity, 
up.captain_birthplace_id as birthplace_uhgs_id_uncertainity, 
up.captain_birthplace as birthplace_uncertainity, up.captain_citizenship as citizenship_uncertainity 
from navigoviz.pointcall p, navigocheck.uncertainity_pointcall up , navigocheck.check_pointcall cp 
where p.pkid = up.pkid and cp.pkid = p.pkid


alter table public.csv_extract add column if not exists shipclass_uncertainity  int;
alter table public.csv_extract add column if not exists birthplace_uhgs_id_uncertainity  int;
alter table public.csv_extract add column if not exists birthplace_uncertainity  int;
alter table public.csv_extract add column if not exists citizenship_uncertainity  int;
alter table public.csv_extract add column if not exists birthplace_uhgs_id  text;

update public.csv_extract c set shipclass_uncertainity=p.shipclass_uncertainity,
birthplace_uhgs_id_uncertainity = p.birthplace_uhgs_id_uncertainity,
birthplace_uncertainity = p.birthplace_uncertainity,
citizenship_uncertainity = p.citizenship_uncertainity,
birthplace_uhgs_id = p.birthplace_uhgs_id
from navigoviz.pointcall p
where c.pkid = p.pkid

create table navigoviz.pointcall_backup as (select * from navigoviz.pointcall)
create table navigoviz.built_travels_backup as (select * from navigoviz.built_travels)
create table navigoviz.uncertainity_travels_backup as (select * from navigoviz.uncertainity_travels ut)
create table navigoviz.debug_travels_backup as (select * from navigoviz.debug_travels ut)

-- fix du pb de commodity_standardized3_fr et commodity_standardized4_fr
-- Le jeudi 8 avril, sur ma machine, puis sur le serveur plume
update navigoviz.pointcall set 
        commodity_standardized3_fr=(all_cargos->>2)::json->>'commodity_standardized_fr',
		commodity_standardized4_fr=(all_cargos->>3)::json->>'commodity_standardized_fr';
		
select all_cargos, commodity_standardized3_fr, commodity_standardized4_fr 
from navigoviz.built_travels bt where all_cargos is not null limit 10 

update navigoviz.built_travels set 
        commodity_standardized3_fr=(all_cargos->>2)::json->>'commodity_standardized_fr',
		commodity_standardized4_fr=(all_cargos->>3)::json->>'commodity_standardized_fr';
		
-------------------------------------------------
-- import de données CSV permettant d'associer port et fermes sur toute la France
-- Fichier C:\Travail\ULR_owncloud\ANR_PORTIC\Reunions\2021-05-27-28_postdatasprint\liste direction bureaux 1789.csv
-- le 28 mai 2021
-------------------------------------------------

select * from ports.liste_direction_bureaux ldb 

select ldb.customs_region , ldb.customs_office , pp.toponyme, levenshtein(ldb.customs_office, pp.toponyme) as dtopo, 
pp.toponyme_standard_fr, pp.province
from ports.liste_direction_bureaux ldb , ports.port_points pp 
where pp.state_1789_fr = 'France' and pp.toponyme % ldb.customs_office 
order by ldb.customs_region, ldb.customs_office, dtopo

alter table ports.liste_direction_bureaux add column is_maritime boolean default true
update ports.liste_direction_bureaux set is_maritime = false 
where customs_region in ('Grenoble', 'Lyon', 'Directions de terre', 'Chalons', 'Besancon', 'Charleville', 'Langres', 'Auch', 'Valenciennes', 'Saint-Quentin', 'Soisson')


select count(tonnage) , p.toponyme_fr 
from navigoviz.pointcall p 
where p.source_subset = 'Poitou_1789' and p.pointcall_function != 'O'
group by p.toponyme_fr 


----------------------------------------------------------------------------
-- Entre le 7 et le 12 juillet 2021 
-- Calculs des flux bruts : trajets départ-arrivée sans tenir compte des ship_id ni des A ou Z
-- Correction des in_date qui étaient nulles
----------------------------------------------------------------------------



---------------------------------------
-- Correction des dates qui conditionnent l'ordonnancement des pointcalls et la reconstitution des trajets
---------------------------------------

select count(*) from navigoviz.pointcall p 
where date_fixed is null
-- 46533 / 108529

select count(*)  from navigoviz.pointcall_backup p where pointcall_out_date2 is null
-- 52606
select pointcall_out_date, pointcall_out_date2  from navigoviz.pointcall_backup p where pointcall_out_date2 is null

select pointcall_out_date, pointcall_out_date2, outdate_fixed , 
pointcall_in_date, pointcall_in_date2, indate_fixed, to_date(pointcall_in_date2, 'YYYY=MM=DD')   
from navigoviz.pointcall p 
where pointcall_out_date2 is null and date_fixed is null


-- Correction de soucis sur les formats saisis dans pointcall_indate
-- manuelle

select pkid, source_doc_id, record_id,pointcall_in_date, pointcall_in_date2 from navigoviz.pointcall 
where  
--pointcall_in_date = '1787>02>29!'-- record_id : 00175018 00175019
-- pointcall_in_date = '1789>09>31' -- record_id :00364039
-- pointcall_in_date = '1789>04>31' -- record_id : 00310621 00310050
-- pointcall_in_date = '1787>17>04!'--  00154851
-- pointcall_in_date = '1789>11>31'

update    navigo.pointcall set  pointcall_indate =   '1789>12>01'     where record_id in ('00345977', '00349763');
update    navigoviz.pointcall set  pointcall_in_date =   '1789>12>01'     where record_id in ('00345977', '00349763');
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>12>01'  where record_id in ('00345977', '00349763');

update    navigo.pointcall set  pointcall_indate =   '1787>04>17!'     where record_id in ('00154851');
update    navigoviz.pointcall set  pointcall_in_date =   '1787>04>17!'     where record_id in ('00154851');
update    navigocheck.check_pointcall set  pointcall_indate =   '1787>04>17!'  where record_id in ('00154851');

update    navigo.pointcall set  pointcall_indate =   '1789>05>01'     where record_id in ('00310621', '00310050');
update    navigoviz.pointcall set  pointcall_in_date =   '1789>05>01'     where record_id in ('00310621', '00310050');
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>05>01'  where record_id in ('00310621', '00310050');

update    navigo.pointcall set  pointcall_indate =   '1789>10>01'     where record_id in ('00364039');
update    navigoviz.pointcall set  pointcall_in_date =   '1789>10>01'     where record_id in ('00364039');
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>10>01'  where record_id in ('00364039');

update    navigo.pointcall set  pointcall_indate =   '1787>03>01'     where record_id in ('00175018', '00175019');
update    navigoviz.pointcall set  pointcall_in_date =   '1787>03>01'     where record_id in ('00175018', '00175019');
update    navigocheck.check_pointcall set  pointcall_indate =   '1787>03>01'  where record_id in ('00175018', '00175019');

select pkid, source_doc_id, record_id,pointcall_out_date, pointcall_out_date2 from navigoviz.pointcall 
where pointcall_out_date2 = '1787=04=31'
-- pointcall_out_date = '1789=11=31' -- record_id : 00336076  00339862
-- pointcall_out_date = '1787=04=31' -- record_id : 00185827

update    navigo.pointcall set  pointcall_outdate =   '1789=11=30'     where record_id in ('00339862', '00336076');
update    navigocheck.check_pointcall set  pointcall_outdate =   '1789=11=30'  where record_id in ('00339862', '00336076');
update    navigoviz.pointcall set  pointcall_out_date =   '1789=11=30'     where record_id in ('00339862', '00336076');

update    navigo.pointcall set  pointcall_outdate =   '1787=04=30'     where record_id in ('00185827');
update    navigocheck.check_pointcall set  pointcall_outdate =   '1787=04=30'  where record_id in ('00185827');
update    navigoviz.pointcall set  pointcall_out_date =   '1787=04=30'     where record_id in ('00185827');

select pkid, source_doc_id, record_id, pointcall_out_date, pointcall_out_date2 from navigoviz.pointcall 
where position('00' in pointcall_out_date ) > 0 or position('99' in pointcall_out_date ) > 0
order by pointcall_out_date
/*
 * 70844	00186568	00295084	1787<03<00	
32720	00151408	00151408	1787<03<00	
25691	00140410	00142720	1787<07<00!	
10651	00114418	00115071	1787<09<99!	
10659	00114426	00115079	1787<10<99!	
40300	00154080	00160050	1787<12<00!	
39245	00152774	00158744	1787<12<00!	
3869	00108257	00108257	1787=03=00	
63043	00183829	00183830	1787=03=00	
65894	00186754	00186755	1787=05=00	
9078	00113471	00113471	1787=05=00	
9077	00113470	00113470	1787=05=00	
9088	00113481	00113481	1787=07=00	
31743	00150382	00150382	1787=10=00	
31742	00150381	00150381	1787=10=00	
31737	00150376	00150376	1787=10=00	
31738	00150377	00150377	1787=10=00	
31744	00150383	00150383	1787=10=00	
31736	00150375	00150375	1787=10=00	
31740	00150379	00150379	1787=10=00	
31745	00150384	00150384	1787=10=00	
31739	00150378	00150378	1787=10=00	
31741	00150380	00150380	1787=10=00	
58247	00178298	00178299	1787>02>00	
72877	00309200	00309201	1789<01<00!	
71598	00306868	00306870	1789<01<00!	
71577	00306791	00306793	1789<01<00!	
71702	00307063	00307065	1789<01<00!	
71488	00306372	00306373	1789<01<00!	
72189	00308386	00308388	1789<02<00	
72931	00309336	00309338	1789<03<00!	
72507	00308815	00308816	1789<03<00!	
72328	00308629	00308630	1789<03<00!	
72613	00308924	00308925	1789<03<00!	
74481	00311298	00311299	1789<04<00!	
73791	00310243	00310244	1789<04<00!	
74667	00311606	00311607	1789<05<00!	
74362	00311128	00311130	1789<05<00!	
107750	00364344	00364345	1789=08=00	
 */
-- Re-calculer pointcall_in_date2
update navigoviz.pointcall set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
else case when position('!' in pointcall_in_date ) > 0 then replace(replace(pointcall_in_date, '!', ''), '>', '=')  else replace(pointcall_in_date, '>', '=')  end
end  where position('00' in pointcall_in_date ) = 0
-- replace 00 et 99 par 01
update navigoviz.pointcall set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(replace(pointcall_in_date, '00', '01'), '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
else case when position('!' in pointcall_in_date ) > 0 then replace(replace(replace(pointcall_in_date, '00', '01'), '!', ''), '>', '=')  else replace(replace(pointcall_in_date, '00', '01'), '>', '=')  end
end  where position('00' in pointcall_in_date ) > 0
-- 21

-- Re-calculer pointcall_out_date2
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(pointcall_out_date, '>', '=') end  
            where position('00' in pointcall_out_date ) = 0 and position('99' in pointcall_out_date ) = 0
-- replace 00 et 99 par 01
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '00', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(replace(pointcall_out_date, '00', '01'), '>', '=') end  
            where position('00' in pointcall_out_date ) > 0 
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '99', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(replace(pointcall_out_date, '99', '01'), '>', '=') end  
            where position('99' in pointcall_out_date ) > 0 
            
-- vérif que la mise à jour peut marcher pour indate_fixed
select pointcall_out_date, pointcall_out_date2, outdate_fixed , 
pointcall_in_date, pointcall_in_date2, indate_fixed, to_date(pointcall_in_date2, 'YYYY=MM=DD')   
from navigoviz.pointcall p 
where pointcall_out_date2 is null and date_fixed is null

-- vérif que la mise à jour peut marcher pour outdate_fixed
select pointcall_out_date, pointcall_out_date2, outdate_fixed , 
pointcall_in_date, pointcall_in_date2, indate_fixed, to_date(pointcall_out_date2, 'YYYY=MM=DD')   
from navigoviz.pointcall p 
where pointcall_out_date is not null and pointcall_out_date2 is  null and outdate_fixed is null
order by pointcall_out_date

-- mise à jour de indate_fixed
update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD')  where pointcall_in_date is not null
-- mise à jour de outdate_fixed
update navigoviz.pointcall set outdate_fixed= to_date(pointcall_out_date2, 'YYYY=MM=DD')  where pointcall_out_date is not null
            
-- Remise à jour de date_fixed et rankfull (attention, pointcall_out_date2 et pointcall_in_date2 marchent pas aussi bien, exemple avec source_doc_id = '00340010' )

update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by COALESCE(pointcall_out_date, pointcall_in_date )) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                order by p.ship_id,  date_dates
            ) as k where p.pkid = k.pkid

select count(*) from navigoviz.pointcall p 
where date_fixed is null
-- 0

-- correction le 20 juillet 2021
-- order by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), pointcall_out_date

-- premier code du 19 juillet
update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by source_doc_id, COALESCE(pointcall_out_date, pointcall_in_date )) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                order by p.ship_id
            ) as k where p.pkid = k.pkid

-- code correction du 20 juillet
 update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), pointcall_out_date) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                order by coalesce(ship_id, source_doc_id), date_dates, pointcall_out_date
            ) as k where p.pkid = k.pkid
            
select pointcall_out_date, pointcall_out_date2, outdate_fixed , 
pointcall_in_date, pointcall_in_date2, indate_fixed
from navigoviz.pointcall p 
where date_fixed is NULL

-- Vérifier

-- Silvia prend
-- ship_id	pointcall_rankfull	pointcall_action	pointcall_out_date	pointcall	net_route_marker	ship_name	captain_name	tonnage	tx	homeport_toponyme_fr	class	flag

select coalesce(outdate_fixed , indate_fixed) as pointcall_date, source_doc_id, data_block_leader_marker, ship_id, pointcall_rankfull, pointcall_action, pointcall_out_date, pointcall,  net_route_marker,  ship_name,	captain_name,	tonnage,	homeport_toponyme_fr,	"class" , flag, 
pointcall_uncertainity, pointcall_in_date , outdate_fixed, indate_fixed,  pointcall_out_date2, pointcall_in_date2 
from navigoviz.pointcall p where 
-- ship_id in ('0002637N', '0002454N', '0002933N') --
source_doc_id in ('00340010', '00333737', '00333698', '00333621', '00109860', '00184695', '00109878', '00185192', '00108261', '00333558', '00341294','00340757', '00337212' )
order by coalesce(ship_id, source_doc_id), pointcall_rankfull
-- coalesce(ship_id, source_doc_id), pointcall_date, pointcall_out_date
-- pointcall_rankfull
-- coalesce(ship_id, source_doc_id), coalesce(pointcall_out_date, pointcall_in_date)

select coalesce(outdate_fixed , indate_fixed) as pointcall_date, source_doc_id, data_block_leader_marker, ship_id, pointcall_rankfull, pointcall_action, pointcall_out_date, pointcall,  net_route_marker,  ship_name,	captain_name,	tonnage,	homeport_toponyme_fr,	"class" , flag, 
pointcall_uncertainity, pointcall_in_date , outdate_fixed, indate_fixed,  pointcall_out_date2, pointcall_in_date2 
from navigoviz.pointcall p where 
 ship_id in ('0002637N', '0002454N', '0002933N') --
--source_doc_id in ('00340010', '00333737', '00333698', '00333621', '00109860', '00184695', '00109878', '00185192', '00108261', '00333558', '00341294','00340757', '00337212' )
order by coalesce(ship_id, source_doc_id), pointcall_rankfull


select source_doc_id, travel_id, departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.built_travels bt  
where source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192') and source_entry != 'both-to'
order by travel_id

-- pour debug Silvia

select bt.source_doc_id, p.data_block_leader_marker, bt.ship_id, p.pointcall_rankfull, bt.departure_action, bt.departure_out_date , departure_fr ,  p.net_route_marker,  
bt.destination_fr ,
bt.ship_name,	bt.captain_name,	bt.tonnage,	bt.homeport_toponyme_fr,	bt."class" , bt.flag,
destination_action, destination_in_date 
from navigoviz.built_travels bt  , navigoviz.pointcall p 
where bt.source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192') and source_entry != 'both-to'
and p.pkid = bt.departure_pkid 
order by travel_id


select source_doc_id, travel_id, departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.raw_flows bt  
where source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192') and source_entry != 'both-to'
order by travel_id
-----------------------------------------------------------------------------------------------
-- Calculs des flux bruts : trajets départ-arrivée sans tenir compte des ship_id ni des A ou Z
-----------------------------------------------------------------------------------------------

--- au départ : des tests sur la Direction de la Ferme de la Rochelle, qui justifiait le besoin de flux bruts pour le datasprint
drop table navigoviz.test_fermeLR2

-- Bonne solution pour créer l'API pour 1789
create table navigoviz.test_fermeLR2 as (
	select p1.source_doc_id, p1.pkid, p1.pointcall as depart, p1.pointcall_uhgs_id, p1.ferme_bureau, p1.ferme_direction, p1.tonnage::float, 
	p1.homeport, p1.homeport_province,  p1.homeport_substate_1789_fr, p1.homeport_state_1789_fr, 
	p2.pointcall, p2.pointcall_province, p2.substate_1789_fr, p2.state_1789_fr, p2.partner_balance_supp_1789
	from navigoviz.pointcall p1 , navigoviz.pointcall p2 
	where extract(year from p1.outdate_fixed) = 1789
		and (p1.pointcall_action in ('Out', 'In-Out', 'Loading', 'Transit', 'In-out', 'Sailing around')) 
		and (p2.pointcall_action in ('In', 'In-Out', 'Unloading', 'Transit', 'In-out')) -- manque transit, sailing, etc. Sailing around
		AND p2.data_block_leader_marker = 'T'
		and p1.source_doc_id  = p2.source_doc_id
		and p1.ferme_direction = 'La Rochelle'
)
select * from navigoviz.test_fermeLR2
select distinct pointcall_action, count(*) from navigoviz.pointcall group by pointcall_action order by count
-- 7062 en 1787 // versus 7020 dans built_travels en 1787 
-- 6832 en 1787 // si p2.data_block_leader_marker = 'T'
-- 6773 en 1789 // 6535 en 1789 si p2.data_block_leader_marker = 'T' (évite de compter des doubles comptes de départ de la Rochelle, mais on perd les escales)
select (6773-6535) / 6773.0 * 100.0 -- 3.51 % en 1789
select (7062 - 7020) / 7020.0 * 100.0 -- 0.60 % en 1787 avérés (built_travels avec ship_id versus autojointure sur source_doc_id)

select source_doc_id , count(record_id)
from navigoviz.pointcall p
where source_doc_id = '00296034' and extract(year from p.outdate_fixed) = 1789  and p.ferme_direction = 'La Rochelle'  
group by source_doc_id
having count(distinct pkid) = 1

select pkid, source_doc_id, * from navigoviz.pointcall where source_doc_id = '00296034'
70988	00296034
70987	00296034
select ferme_direction, pointcall_uhgs_id, source_doc_id, * from navigoviz.test_fermeLR2 where source_doc_id not in (
	select source_doc_id from navigoviz.built_travels bt 
	where   extract(year from bt.outdate_fixed) = 1787 
	and source_entry != 'both-to' 
	and bt.ferme_direction = 'La Rochelle')
A0196496	00152874
A0171758	00149034
A0171758	00148729

select departure_ferme_bureau

select count(*), data_block_leader_marker from navigoviz.built_travels bt , pointcall p 
where  p.pkid = bt.destination_pkid and extract(year from bt.outdate_fixed) = 1787 
and source_entry != 'both-to' 
--and bt.departure_province in ('Aunis', 'Saintonge', 'Poitou')
and bt.departure_ferme_direction  = 'La Rochelle'
group by data_block_leader_marker
-- and p.data_block_leader_marker = 'T'
--and (bt.departure_action =  'Out' or departure_action = 'In-Out' ) -- 1147 au lieu de 1155
-- 7020 en 1787, 515 en 1789

select count(*) from navigoviz.built_travels bt  
where   extract(year from bt.outdate_fixed) = 1787 
and source_entry != 'both-to' 
and bt.departure_ferme_direction = 'La Rochelle'

--both-from
--both-to
--from


select  count(*)
from navigoviz.pointcall p1 , navigoviz.pointcall p2 
where extract(year from p1.outdate_fixed) = 1789
	and (p1.pointcall_action = 'Out' or p1.pointcall_action = 'In-Out' ) 
	AND p2.data_block_leader_marker = 'T'
	and p1.source_doc_id  = p2.source_doc_id
	and p1.ferme_direction = 'La Rochelle'
	and p1.ship_id is null
-- 5470 de ship id null en 1789
	
	
-- compter les départs de la Rochelle
	select  p1.pointcall, p2.pointcall, p1.ship_id, p1.outdate_fixed, 
	p1.pointcall_action, p2.indate_fixed , p2.pointcall_action 
from navigoviz.pointcall p1, navigoviz.pointcall p2
where extract(year from p1.outdate_fixed) = 1787
	and (p1.pointcall_action = 'Out' or p1.pointcall_action = 'In-Out' ) 
	and (p2.pointcall_action = 'In' or p2.pointcall_action = 'In-Out' ) 
	--AND p1.data_block_leader_marker = 'T'
	and p1.source_doc_id  = p2.source_doc_id
	and p1.ferme_direction = 'La Rochelle'
	-- 6663 départs, 2 arrivées avec T, 6629 arrivée / départs

select pp.toponyme_standard_fr, pp.uhgs_id, pp.ferme_direction , pp.ferme_bureau , pp.amiraute, pp.province 
from ports.port_points pp 
where pp.state_1789_fr = 'France' -- and pp.toponyme like '%Lorient%'
order by  pp.ferme_direction , pp.ferme_bureau ,  pp.province , pp.amiraute
-- Le Havre	A0187101	Rouen	Le Havre
-- Lorient	A0140266	Lorient	Lorient

select distinct c.cargo_item_action from navigo.cargo c 
select distinct cc.cargo_item_action, count(*) from navigocheck.check_cargo cc group by cargo_item_action order by count

drop table navigoviz.test_fermeLR
create table navigoviz.test_fermeLR as (
 select depart.id, depart.ship_id, depart.ship_name, depart.pointcall as departure, arrivee.pointcall as destination, 
   depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, depart.pointcall_action as depart_action,  
   arrivee.pointcall_action, 
   depart.ferme_direction,
   depart.source_doc_id, 
   depart.pkid
from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, d.ferme_direction 
            from navigoviz.pointcall d
            where   
            net_route_marker = 'A' and pointcall_uncertainity > -2
            and d.ferme_direction = 'La Rochelle'
            and extract(year from d.outdate_fixed) = 1787
            order by d.ship_id, d.pointcall_rankfull) as depart,
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker, d.data_block_leader_marker, d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, pointcall_out_date
            from navigoviz.pointcall d
            where   net_route_marker = 'A' and pointcall_uncertainity > -2
            -- pointcall_action= 'In'
            -- and 
            -- data_block_leader_marker = 'T'
             and extract(year from d.outdate_fixed) = 1787
            order by d.ship_id, d.pointcall_rankfull, d.source_doc_id) as arrivee
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id 
            order by depart.ship_id, depart.id )
            -- 6038 avec net_route_marker / 6041 sans 

select count(*) from navigoviz.test_fermeLR -- 6038
-- pourquoi data_block_leader_marker = 'T' bug ?
            -- ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id,
            select  d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker, d.data_block_leader_marker, d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, pointcall_out_date
            from navigoviz.pointcall d
            where   
            -- net_route_marker = 'A' and 
            -- pointcall_action= 'In'
            -- and pointcall_uncertainity > -2
            data_block_leader_marker = 'T'
            and extract(year from d.outdate_fixed) = 1787
            order by d.ship_id, d.pointcall_rankfull, d.source_doc_id
            

-- Nouvelle table pour l'API sans tenir compte des A et Z
drop table navigoviz.debug_travels2
create table navigoviz.debug_travels2 as (
            select  
                ROW_NUMBER () OVER (PARTITION BY d.source_doc_id order by d.pointcall_rankfull) as id,
                round(((st_distance(d.pointcall_point , a.pointcall_point ))/1000) :: numeric, 3) as distance_dep_dest,
                round(((st_distance(d.pointcall_point , d.homeport_point::geometry ))/1000.0) :: numeric, 3) as distance_homeport_dep,
                    d.pointcall as departure,
                    d.toponyme_fr as departure_fr, 
                    d.toponyme_en as departure_en,
                    d.pointcall_uhgs_id  as departure_uhgs_id,
                    d.latitude as departure_latitude,
                    d.longitude as departure_longitude,
                    d.pointcall_admiralty as departure_admiralty,
                    d.pointcall_province as departure_province,
                    d.pointcall_states as departure_states,
                    d.pointcall_substates as departure_substates,
                    d.state_1789_fr as departure_state_1789_fr, 
                    d.substate_1789_fr as departure_substate_1789_fr, 
                    d.state_1789_en as departure_state_1789_en, 
                    d.substate_1789_en as departure_substate_1789_en,
                    d.ferme_direction as departure_ferme_direction, 
                    d.ferme_direction_uncertainty as departure_ferme_direction_uncertainty, 
                    d.ferme_bureau as departure_ferme_bureau, 
                    d.ferme_bureau_uncertainty as departure_ferme_bureau_uncertainty, 
                    d.partner_balance_1789 as departure_partner_balance_1789, 
                    d.partner_balance_supp_1789 as departure_partner_balance_supp_1789, 
                    d.partner_balance_1789_uncertainty as departure_partner_balance_1789_uncertainty, 
                    d.partner_balance_supp_1789_uncertainty as departure_partner_balance_supp_1789_uncertainty,
                    d.shiparea as departure_shiparea,
                    d.pointcall_status as departure_status,
                    -- 26
                    --d.source_1787_available as departure_source_1787_available,
                    --d.source_1789_available as departure_source_1789_available,
                    d.nb_conges_1787_inputdone as departure_nb_conges_1787_inputdone, 
                    d.Nb_conges_1787_CR as departure_Nb_conges_1787_CR, 
                    d.nb_conges_1789_inputdone as departure_nb_conges_1789_inputdone, 
                    d.Nb_conges_1789_CR as departure_Nb_conges_1789_CR, 
                    d.pointcall_status_uncertainity as departure_status_uncertainity,
                    d.pointcall_point as departure_point,
                    d.pointcall_out_date as departure_out_date,
                    d.pointcall_action as departure_action,
                    d.date_fixed as outdate_fixed,
                    d.navigo_status as departure_navstatus,
                    d.pointcall_function as departure_function,
                    a.pointcall as destination,
                    a.toponyme_fr as destination_fr, 
                    a.toponyme_en as destination_en,
                    a.pointcall_uhgs_id  as destination_uhgs_id,
                    a.latitude as destination_latitude,
                    a.longitude as destination_longitude,
                    a.pointcall_admiralty as destination_admiralty,
                    a.pointcall_province as destination_province,
                    a.pointcall_states as destination_states,
                    a.pointcall_substates as destination_substates,
                    a.state_1789_fr as destination_state_1789_fr, 
                    a.substate_1789_fr as destination_substate_1789_fr, 
                    a.state_1789_en as destination_state_1789_en, 
                    a.substate_1789_en as destination_substate_1789_en,
                    a.ferme_direction as destination_ferme_direction, 
                    a.ferme_direction_uncertainty as destination_ferme_direction_uncertainty, 
                    a.ferme_bureau as destination_ferme_bureau, 
                    a.ferme_bureau_uncertainty as destination_ferme_bureau_uncertainty, 
                    a.partner_balance_1789 as destination_partner_balance_1789, 
                    a.partner_balance_supp_1789 as destination_partner_balance_supp_1789, 
                    a.partner_balance_1789_uncertainty as destination_partner_balance_1789_uncertainty, 
                    a.partner_balance_supp_1789_uncertainty as destination_partner_balance_supp_1789_uncertainty,
                    a.shiparea as destination_shiparea,
                    a.pointcall_status as destination_status,
                    -- 34
                    --a.source_1787_available as destination_source_1787_available,
                    --a.source_1789_available as destination_source_1789_available,
                    a.nb_conges_1787_inputdone as destination_nb_conges_1787_inputdone, 
                    a.Nb_conges_1787_CR as destination_Nb_conges_1787_CR, 
                    a.nb_conges_1789_inputdone as destination_nb_conges_1789_inputdone, 
                    a.Nb_conges_1789_CR as destination_Nb_conges_1789_CR, 
                    a.pointcall_status_uncertainity as destination_status_uncertainity,
                    a.pointcall_point as destination_point,
                    a.pointcall_in_date as destination_in_date,
                    a.pointcall_action as destination_action,
                    a.date_fixed as indate_fixed,
                    a.navigo_status as destination_navstatus,
                    a.pointcall_function as destination_function,
                    d.ship_name,
                    d.ship_id,
                    d.tonnage,
                    d.tonnage_unit,
                    d.tonnage_class,
                    d.in_crew,
                    d.flag,
                    d.ship_flag_id,
                    d.ship_flag_standardized_fr, 
                    d.ship_flag_standardized_en, 
                    d.class,
                    d.homeport,
                    d.homeport_uhgs_id , 
                    d.homeport_latitude,
                    d.homeport_longitude,
                    d.homeport_admiralty,
                    d.homeport_province,
                    d.homeport_states,
                    d.homeport_substates,
                    d.homeport_status,
                    d.homeport_shiparea,
                    d.homeport_point,
                    d.homeport_state_1789_fr,
                    d.homeport_substate_1789_fr,
                    d.homeport_state_1789_en,
                    d.homeport_substate_1789_en,
                    d.homeport_toponyme_fr,
                    d.homeport_toponyme_en,
                    'from' as source_entry, 
                    d.source_doc_id,
                    d.source_text,
                    d.source_suite,
                    d.source_component,
                    d.source_number,
                    d.source_other,
                    d.source_main_port_uhgs_id, 
                    d.source_main_port_toponyme,
                    d.source_subset,
                    d.captain_id,
                    d.captain_name,
                    d.birthplace,
                    d.status,
                    d.citizenship,
                    d.birthplace_uhgs_id,
                    d.commodity_purpose,
                    d.commodity_id,
                    d.quantity,
                    d.quantity_u,
                    d.commodity_standardized,
                    d.commodity_standardized_fr,
                    d.commodity_permanent_coding,
                    --d.cargo_item_action,
                    d.commodity_purpose2,
                    d.commodity_id2,
                    d.quantity2,
                    d.quantity_u2,
                    d.commodity_standardized2,
                    d.commodity_standardized2_fr,
                    d.commodity_permanent_coding2,
                    --d.cargo_item_action2,
                    d.commodity_purpose3,
                    d.commodity_id3,
                    d.quantity3,
                    d.quantity_u3,
                    d.commodity_standardized3,
                    d.commodity_standardized3_fr,
                    d.commodity_permanent_coding3,
                    --d.cargo_item_action3,
                    d.commodity_purpose4,
                    d.commodity_id4,
                    d.quantity4,
                    d.quantity_u4,
                    d.commodity_standardized4,
                    d.commodity_standardized4_fr,
                    d.commodity_permanent_coding4,
                    --d.cargo_item_action4,
                    d.all_cargos,
                    -- 84
                    d.tax_concept1,
                    d.payment_date,
                    d.q01_1, d.q01_u , d.q02_1 , d.q02_u , d.q03_1 , d.q03_u , d.taxe_amount01,
                    d.all_taxes,
                    --10
                    d.tax_concept2, d.q01_2, d.q02_2 , d.q03_2 , d.taxe_amount02,
                    d.tax_concept3, d.q01_3, d.q02_3 , d.q03_3 , d.taxe_amount03,
                    d.tax_concept4, d.q01_4, d.q02_4 , d.q03_4 , d.taxe_amount04,
                    d.tax_concept5, d.q01_5, d.q02_5 , d.q03_5 , d.taxe_amount05,
                    --20
                    d.ship_uncertainity,
                    d.tonnage_uncertainity ,
                    d.flag_uncertainity,
                    d.homeport_uncertainity ,
                    d.pointcall_uncertainity as departure_uncertainity,
                    d.pointcall_uncertainity as destination_uncertainity,
                    d.captain_uncertainity,
                    null as travel_uncertainity,
                    d.cargo_uncertainity,
                    d.taxe_uncertainity, 
                    d.pointcall_outdate_uncertainity,
                    d.shipclass_uncertainity,
                    d.birthplace_uhgs_id_uncertainity,
                    d.birthplace_uncertainity,
                    d.citizenship_uncertainity

            
            from navigoviz.pointcall d , navigoviz.pointcall a 
            where 
                (d.pointcall_action in ('Out', 'In-Out', 'Loading', 'Transit', 'In-out', 'Sailing around')) 
		        and (a.pointcall_action in ('In', 'In-Out', 'Unloading', 'Transit', 'In-out')) 
		        AND a.data_block_leader_marker = 'T'
		        and a.source_doc_id  = d.source_doc_id
            ) -- 40788
            
select id, source_doc_id, departure, destination, outdate_fixed, bt.ship_name, bt.departure_ferme_direction
from navigoviz.debug_travels2 bt
where extract(year from bt.outdate_fixed) = 1787  and bt.departure_ferme_direction = 'La Rochelle'
order by id

select count(*) from navigoviz.built_travels bt  
where   
and source_entry != 'both-to' 
and bt.departure_ferme_direction = 'La Rochelle'

------
drop table navigoviz.built_travels_backup ;
create table navigoviz.built_travels_backup as (select * from navigoviz.built_travels);

drop table navigoviz.debug_travels2 ;
drop table navigoviz.debug_travels_backup ;
create table navigoviz.debug_travels_backup as (select * from navigoviz.debug_travels);

drop table navigoviz.pointcall_backup 
-- drop table navigoviz.test_fermelr 
drop table navigoviz.uncertainity_travels_backup ;
create table navigoviz.uncertainity_travels_backup as (select * from navigoviz.uncertainity_travels);

drop table navigoviz.test_fermelr ;
drop table navigoviz.test_fermelr2 ;
------





select count(distinct source_doc_id)  from navigoviz.pointcall p 
-- 53109
select count(distinct source_doc_id)  from navigoviz.pointcall_backup p 
-- 53109

select departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action 
from navigoviz.raw_flows rf where ship_id = '0002637N' order by outdate_fixed

select source_doc_id, departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action 
from navigoviz.raw_flows rf where ship_id is null
order by coalesce(ship_id, source_doc_id), outdate_fixed

--00331412	Arles	Livourne		Notre Dame du Jardin	1789-07-31	Out	In
--00331412	Marseille	Livourne		Notre Dame du Jardin	1789-08-01	In-out	in
--
--00331419	Toulon	Sette		Deux Freres	1789-07-25	Out	In
--00331419	Bouc	Sette		Deux Freres	1789-08-01	In-out	In
--00331419	Marseille	Sette		Deux Freres	1789-08-01	In-out	In

select source_doc_id, departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action 
from navigoviz.raw_flows rf where ship_id is null and source_suite = 'G5'
order by rf.travel_id

select source_doc_id, count(*)
from navigoviz.raw_flows rf 
where ship_id is null --and source_suite = 'G5'
group by rf.source_doc_id
--having count(*) > 1
order by rf.source_doc_id

-- 286 source_doc_id avec plus d'un depart-destination / 7801 si source_suite = 'G5' , soit 3.6 %
-- 371 source_doc_id avec plus d'un depart-destination / 7912 toutes sources soit 4.6 %

select source_doc_id, count(*)
from navigoviz.raw_flows rf 
where extract(year from outdate_fixed) = 1789 AND rf.departure_ferme_direction = 'La Rochelle' 
group by rf.source_doc_id
--having count(*) > 1
order by rf.source_doc_id

select  count(*)
from navigoviz.raw_flows rf 
where extract(year from outdate_fixed) = 1789 AND rf.departure_ferme_direction = 'La Rochelle'
-- 165 source_doc_id avec plus d'un depart-destination / 6753 departs de la Rochelle en 1789
-- 165 source_doc_id avec plus d'un depart-destination / 6585 source_doc_id de la Rochelle en 1789

--
00333621	4
00333698	2
00333737	2
00340010	3

-- Silvia prend
-- ship_id	pointcall_rankfull	pointcall_action	pointcall_out_date	pointcall	net_route_marker	ship_name	captain_name	tonnage	tx	homeport_toponyme_fr	class	flag

select coalesce(outdate_fixed , indate_fixed) as pointcall_date, source_doc_id, data_block_leader_marker, ship_id, pointcall_rankfull, pointcall_action, pointcall_out_date, pointcall,  net_route_marker,  ship_name,	captain_name,	tonnage,	homeport_toponyme_fr,	"class" , flag, 
pointcall_uncertainity, pointcall_in_date , outdate_fixed, indate_fixed,  pointcall_out_date2, pointcall_in_date2 
from navigoviz.pointcall p where 
ship_id = '0002933N' --
--source_doc_id in ('00340010', '00333737', '00333698', '00333621', '00109860', '00184695', '00109878')
order by coalesce(ship_id, source_doc_id), pointcall_date, pointcall_out_date
-- pointcall_rankfull
-- coalesce(ship_id, source_doc_id), coalesce(pointcall_out_date, pointcall_in_date)
1789=08=01
1789=07=22

select count(*) from navigoviz.pointcall where data_block_leader_marker is null and ship_id is null

select source_doc_id, departure_ferme_direction, travel_id, departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action,
rf.departure_out_date, rf.destination_in_date 
from navigoviz.raw_flows rf where source_doc_id = '00340010'
order by rf.travel_id

select source_doc_id, departure_ferme_direction, travel_id, departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action,
rf.departure_out_date, rf.destination_in_date 
from navigoviz.built_travels rf  where ship_id = '0002637N'  and source_entry != 'both-to'
order by rf.travel_id 

from navigoviz.pointcall d , navigoviz.pointcall a 
            where 
                (d.pointcall_action in ('Out', 'In-Out', 'Loading', 'Transit', 'In-out', 'Sailing around')) 
		        and (a.pointcall_action in ('In', 'In-Out', 'Unloading', 'Transit', 'In-out')) 
		        AND a.data_block_leader_marker = 'T'
		        and d.source_doc_id  = a.source_doc_id
            ORDER by coalesce(d.ship_id, d.source_doc_id), d.pointcall_rankfull

00109860	Dunkerque	0002454N	1	A
00109860	Ostende	0002454N	2	Z
00109860	Marseille	0002454N	3	Z
00184695	Ostende	0002454N	4	A
00184695	Détroit de Gibraltar	0002454N	5	A
00184695	Marseille	0002454N	6	A

            update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by source_doc_id, COALESCE(pointcall_out_date, pointcall_in_date )) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                order by p.ship_id
            ) as k where p.pkid = k.pkid
                
 1	19571	00109878	0002933N	Ostende	A
2	5490	00109878	0002933N	Dunkerque	Z
3	19570	00109878	0002933N	Malaga	A
4	15158	00109878	0002933N	Barcelonne	Z
5	64365	00185192	0002933N	Barcelonne	A
6	64366	00185192	0002933N	Toulon	A
7	64364	00185192	0002933N	Marseille	A

select source_doc_id, travel_id, departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action,
rf.departure_out_date, rf.destination_in_date 
from navigoviz.raw_flows rf where source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192')
order by coalesce(ship_id, source_doc_id), coalesce (rf.outdate_fixed , rf.indate_fixed )
           

select source_doc_id, travel_id, departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.built_travels bt  where source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192') and source_entry != 'both-to'
order by travel_id

select  id, doc_destination, departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.debug_travels bt  where  ship_id = '0001730N' -- doc_destination = doc_depart
order by id

select source_doc_id, travel_id, departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.built_travels bt  where ship_id = '0002454N' and source_entry != 'both-to'
order by travel_id

select source_doc_id, travel_id, departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.built_travels_backup bt  where source_doc_id = '00109878'
order by travel_id

           
select departure, destination, ship_id, ship_name, outdate_fixed, departure_action, destination_action 
from navigoviz.debug_travels bt  where ship_id = '0002933N'
order by id

select source_doc_id, count(*)
from navigoviz.raw_flows rf 
where ship_id is not null and source_suite = 'G5'
group by rf.source_doc_id
having count(*) > 1
order by rf.source_doc_id

00109860	2
00109878	3

select 

select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by COALESCE(pointcall_out_date, pointcall_in_date )) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p where source_doc_id = '00340010'
                order by p.ship_id,  pointcall_rankfull

update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by COALESCE(pointcall_out_date, pointcall_in_date )) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                order by p.ship_id,  date_dates
            ) as k where p.pkid = k.pkid
            

GRANT USAGE ON SCHEMA navigoviz TO porticapi;
GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigo TO porticapi;--pour les fonctions
GRANT USAGE ON SCHEMA navigo TO api_user;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO porticapi;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to api_user;


SELECT 'travels' as API,
        c.column_name as name,
        case when c.table_name= 'built_travels' then 't' else (case when c.table_name= 'port_points' then 'pp' else 'p' end) end||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('built_travels')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid;
       
       


SELECT 'rawflows' as API,
        c.column_name as name,
        case when c.table_name= 'built_travels' then 't' else (case when c.table_name= 'port_points' then 'pp' else 'p' end) end||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('raw_flows')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid;
        
select travel_id,distance_homeport_dep,distance_dep_dest,travel_rank,distance_dep_dest_miles,distance_homeport_dep_miles,departure,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_states,departure_substates,departure_status,departure_shiparea,departure_state_1789_fr,departure_substate_1789_fr,departure_state_1789_en,departure_substate_1789_en,departure_ferme_direction,departure_ferme_direction_uncertainty,departure_ferme_bureau,departure_ferme_bureau_uncertainty,departure_partner_balance_1789,departure_partner_balance_supp_1789,departure_partner_balance_1789_uncertainty,departure_partner_balance_supp_1789_uncertainty,departure_status_uncertainity,departure_nb_conges_1787_inputdone,departure_Nb_conges_1787_CR,departure_nb_conges_1789_inputdone,departure_Nb_conges_1789_CR,departure_fr,departure_en,departure_point,departure_pkid,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_states,destination_substates,destination_status,destination_shiparea,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_status_uncertainity,destination_nb_conges_1787_inputdone,destination_Nb_conges_1787_CR,destination_nb_conges_1789_inputdone,destination_Nb_conges_1789_CR,destination_fr,destination_en,destination_point,destination_pkid,destination_action,destination_in_date,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_states,homeport_substates,homeport_status,homeport_shiparea,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_number,source_other,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,birthplace_uhgs_id,status,citizenship,commodity_purpose,commodity_id,quantity,quantity_u,commodity_standardized,commodity_standardized_fr,commodity_permanent_coding,cargo_item_action,all_cargos,commodity_purpose2,commodity_id2,quantity2,quantity_u2,commodity_standardized2,commodity_standardized2_fr,commodity_permanent_coding2,cargo_item_action2,commodity_purpose3,commodity_id3,quantity3,quantity_u3,commodity_standardized3,commodity_standardized3_fr,commodity_permanent_coding3,cargo_item_action3,commodity_purpose4,commodity_id4,quantity4,quantity_u4,commodity_standardized4,commodity_standardized4_fr,commodity_permanent_coding4,cargo_item_action4,tax_concept1,payment_date,q01_1,q01_u,q02_1,q02_u,q03_1,q03_u,all_taxes,taxe_amount01,tax_concept2,q01_2,q02_2,q03_2,taxe_amount02,tax_concept3,q01_3,q02_3,q03_3,taxe_amount03,tax_concept4,q01_4,q02_4,q03_4,taxe_amount04,tax_concept2,q01_2,q02_2,q03_2,taxe_amount02,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,birthplace_uncertainity,birthplace_uhgs_id_uncertainity,citizenship_uncertainity,shipclass_uncertainity
from navigoviz.built_travels bt 
where bt.travel_id = '0008150N- 01'


select source_doc_id, travel_id, departure, destination, ship_id, ship_name, rf.outdate_fixed, rf.departure_action, rf.destination_action,
rf.departure_out_date, rf.destination_in_date 
from navigoviz.raw_flows rf where extract(year from outdate_fixed) = 1789 AND rf.departure_ferme_direction = 'La Rochelle' 
order by rf.travel_id
--6753

------------------------------------------
-- Préparation du datasprint @ Dunkerque
-- extraction données pour les étudiants et le sujet de micro-projet de Silvia
------------------------------------------


alter table navigoviz.dunkerque_smugglers add column found_in_api boolean default false;

update navigoviz.dunkerque_smugglers ds set found_in_api = true from
(
select pointcall_out_date, source_doc_id , source_text, ship_id, ship_name, captain_id , captain_name ,  tonnage, 
homeport, homeport_uhgs_id, homeport_state_1789_fr, homeport_latitude, homeport_longitude 
from navigoviz.pointcall p
where pointcall_uhgs_id = 'A0204180' and homeport_state_1789_fr='Grande-Bretagne' and extract(year from p.outdate_fixed) = 1787
order by pointcall_out_date) as p
where ds."Datablock_leader::Ship_id" =p.ship_id and ds.pointcall_outdate = p.pointcall_out_date 
-- 1135

select ship_id, captain_name , ship_name, pointcall, * from pointcall where ship_name = 'Betzy' and pointcall_out_date = '1787=01=03'
-- 0000715N
select distinct ds."Datablock_leader::Ship_homeport"  from navigoviz.dunkerque_smugglers ds where found_in_api = false
Pegelsham
Salwats

select * from ports.port_points pp where pp.toponyme = 'Lee'
select * from ports.port_points pp where pp.toponyme = 'Salwats'

select * from ports.port_points pp where uhgs_id = 'A0204180'

------------------------------------------
-- Travail sur le server le 20 juillet 2021
------------------------------------------

drop table navigoviz.built_travels_backup;
drop table navigoviz.debug_travels_backup ;
drop table navigoviz.pointcall_backup ;
drop table navigoviz.uncertainity_travels_backup  ;

CREATE TABLE navigoviz.built_travels_backup AS (SELECT * FROM navigoviz.built_travels);
CREATE TABLE navigoviz.debug_travels_backup AS (SELECT * FROM navigoviz.debug_travels);
CREATE TABLE navigoviz.pointcall_backup AS (SELECT * FROM navigoviz.pointcall);
CREATE TABLE navigoviz.uncertainity_travels_backup AS (SELECT * FROM navigoviz.uncertainity_travels);


-------------------------------------------
select pointcall, pointcall_rankfull, ship_id, ship_name, pointcall_action , pointcall_function , pointcall_in_date , pointcall_out_date , net_route_marker ,p.record_id
 from navigoviz.pointcall p,
(
select debug.id, bt.source_doc_id, dep.record_id , arr.record_id , bt.departure, bt.destination, bt.distance_dep_dest, bt.travel_uncertainity , bt.departure_action
from (navigoviz.built_travels bt left outer join navigoviz.debug_travels debug on  (debug.ship_id ||'-'|| to_char(debug.id::int, '09')) = bt.travel_id ),
navigo.pointcall dep,  navigo.pointcall arr
where bt.source_entry = 'both-from' and bt.travel_uncertainity = -1 and bt.departure_action <> 'In-Out' and bt.departure_action <> 'In-out' and bt.distance_dep_dest = 0
and arr.pkid = debug.destination_pkid and dep.pkid = debug.departure_pkid
UNION
select debug.id, bt.source_doc_id, dep.record_id , arr.record_id , bt.departure, bt.destination, bt.distance_dep_dest, bt.travel_uncertainity , bt.departure_action
from (navigoviz.built_travels bt left outer join navigoviz.debug_travels debug on  (debug.ship_id ||'-'|| to_char(debug.id::int, '09')) = bt.travel_id ),
navigo.pointcall dep,  navigo.pointcall arr
where bt.source_entry = 'both-to' and bt.travel_uncertainity = -1 and bt.departure_action <> 'In-Out' and bt.departure_action <> 'In-out' and bt.distance_dep_dest = 0
and arr.pkid = debug.destination_pkid and dep.pkid = debug.departure_pkid
) as k
 where  p.source_doc_id = k.source_doc_id and p.pointcall_rankfull = k.id
 order by ship_id, pointcall_rankfull

 select count(*), travel_uncertainity from navigoviz.built_travels where  source_entry != 'both-to' group by travel_uncertainity
 34436	-1
16137	0

----------------------------------------------------------------------------------------------------
-- Faire un fichier de debug référence avec des cas connus
-- lister par ordre chrono les pointcalls et les trajets reconstruits pour êtr sûr
-- pour debug Silvia
/*Voir les documents ici 
C:\Travail\ULR_owncloud\ANR_PORTIC\Reunions\2021-01-07_ROCHELLE\incertitude.docx
C:\Travail\ULR_owncloud\ANR_PORTIC\IDEX\BDD_navigo\Travail_preparation_ANR_PORTIC-v6_01février2021.docx
*/
--------------------------------------------------------------------------------------------------------

-- Les pointcalls

select coalesce(outdate_fixed , indate_fixed) as pointcall_date, source_doc_id, data_block_leader_marker, 
ship_id, pointcall_rankfull, pointcall_action, pointcall_out_date, pointcall,  net_route_marker,  
ship_name,	captain_name,	tonnage,	homeport_toponyme_fr,	"class" , flag, 
pointcall_uncertainity, pointcall_in_date , outdate_fixed, indate_fixed,  pointcall_out_date2, pointcall_in_date2 
from navigoviz.pointcall p where 
-- ship_id in ('0002637N', '0002454N', '0002933N') --
source_doc_id in ('00340010', '00333737', '00333698', '00333621', '00109860', '00184695', '00109878', '00185192', '00108261', '00333558', '00341294','00340757', '00337212', 
'00185820', '00170196', '00186746', '00110740', '00109073', '00176680', '00110063', '00174211', '00138209', '00176574', '00062231', '00109344', '00152409', '00152490', '00336306', '00336441', '00336616' )
order by coalesce(ship_id, source_doc_id), pointcall_rankfull


-- Les travels

select bt.source_doc_id, p.data_block_leader_marker as depart_data_block_leader_marker, bt.ship_id, bt.travel_id , bt.departure_action, bt.departure_out_date , departure_fr ,  p.net_route_marker as depart_net_route_marker,  
bt.destination_fr ,
bt.ship_name,	bt.captain_name,	bt.tonnage,	bt.homeport_toponyme_fr,	bt."class" , bt.flag,
destination_action, destination_in_date 
from navigoviz.built_travels bt  , navigoviz.pointcall p 
where source_entry != 'both-to' and
--bt.source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192') 
bt.source_doc_id in  ('00340010', '00333737', '00333698', '00333621', '00109860', '00184695', '00109878', '00185192', '00108261', '00333558', '00341294','00340757', '00337212',
'00185820', '00170196', '00186746', '00110740', '00109073', '00176680', '00110063', '00174211', '00138209', '00176574', '00062231', '00109344', '00152409', '00152490', '00336306', '00336441', '00336616' ) 
and p.pkid = bt.departure_pkid 
order by travel_id

-- les raw flows

select bt.source_doc_id, p.data_block_leader_marker as depart_data_block_leader_marker, bt.ship_id, bt.travel_id, bt.departure_action, bt.departure_out_date, departure_fr, p.net_route_marker as depart_net_route_marker, 
destination_fr, bt.ship_name,	bt.captain_name,	bt.tonnage,	bt.homeport_toponyme_fr,	bt."class" , bt.flag, 
destination_action, destination_in_date  
from navigoviz.raw_flows bt, navigoviz.pointcall p   
where bt.source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192',
'00185820', '00170196', '00186746', '00110740', '00109073', '00176680', '00110063', '00174211', '00138209', '00176574', '00062231', '00109344', '00152409', '00152490', '00336306', '00336441', '00336616' )  
and p.pkid = bt.departure_pkid 
order by coalesce(bt.ship_id, bt.source_doc_id), departure_out_date

SELECT source_doc_id, ship_id, ship_name, captain_name, class, pointcall_out_date , pointcall 
FROM navigoviz.pointcall 
WHERE 
-- captain_name ilike '%Le Cam, François%' and
-- ship_name ilike '%Marechalle de Mailly%' -- ship_id 0007392N, source_doc_id : 00185820, 00170196
-- ship_name ilike '%Nouvelle Union%' -- ship_id 0000242N, source_doc_id : 00186746, 00110740, 00109073
-- ship_name ilike '%Femme et %' -- ship_id 0003951N, source_doc_id : 00176680, 00110063, 00174211
-- ship_name ilike '%Saint hilaire%' -- ship_id 0004167N, source_doc_id : 00138209, 00176574, 00062231
-- ship_name ilike '%Jeune Nicolas%' -- ship_id 0008090N, source_doc_id : 00109344
-- ship_name ilike '%Sainte Anne%'   -- ship_id 0008446N, source_doc_id : 00152409, 00152490, 00336306, 00336441, 00336616
-- 0002095N -- , le Henry (0002095N), capitaine Jacques Caillot (Cailleau, Cailleaud, Cailoud, Caillaud), que nous retrouvons dans 7 sources différentes, avec un itinéraire est parfaitement cohérent, jauge 90, 95 ou 118 tonneaux selon les lieux. 
-- navire Cornelis et Maria, 0012669N
-- navires 0013706N et 0008751N
-- navire le Naif, 0010857N
-- navire 0000488N, les Trois frères « de Dunkerque » 
-- navire 0000132N Paix
-- navire 0015678N Saint Esprit , source_doc_id : 00188100
-- navire 0014815N Ville d' yverdun , source_doc_id : 00189968
-- navire 0014748N Saint Louis, source_doc_id : 00182372
-- la Fidèle Marianne (ou Fidèle Marie Anne) de Bayonne, 70 tonneaux, commandée par Léon Naudin (ou Nandin), 
-- stop 2.1 Absence des sources dans la base pour confirmer le trajet

SELECT source_doc_id, ship_id, ship_name, captain_name, class, pointcall_out_date , pointcall 
FROM navigoviz.pointcall 
WHERE source_doc_id = '00182372'

SELECT distinct source_doc_id, ship_id, ship_name, captain_name, class  
FROM navigoviz.pointcall 
WHERE source_doc_id in ('00340010', '00333621', '00333698', '00333737', '00109860', '00184695', '00109878', '00185192',
'00185820', '00170196', '00186746', '00110740', '00109073', '00176680', '00110063', '00174211', '00138209', '00176574', '00062231', '00109344', '00152409', '00152490', '00336306', '00336441', '00336616' ) 
order by ship_id


SELECT documentary_unit_id , ship_id, ship_name, captain_name, ship_class  FROM navigo.pointcall 
WHERE 
captain_name ilike '%raymond%'
00185820	0007392N	(Marechalle de Mailly)	(Frere, Raymond)	(Brigantin)
00364407

--- 
-- travail sur source_suite pour l'interface sur la visu des sources le 05 janvier 2021
-- besoin de distinguer le petit cabotage de la santé dans navigoviz.pointcall pour faire les décomptes des renseignés
-- Code reporté dans l'ETL
---

select distinct source_suite , source_subset from navigoviz.pointcall where source_suite = '%Marseille%'
 select distinct  source_subset from navigoviz.pointcall where source_suite = '%Marseille%'
-- Registre du petit cabotage (1786-1787)
-- Expéditions "coloniales" Marseille (1789)
-- Santé Marseille
 
 
