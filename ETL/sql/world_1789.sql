-- Extrait de processing_v2

-------------------------------------------------------------------------
-- 10 mars 2021
-- Import des communes d'Europe
--Télécharger les communes sur toute l'europe
--https://ec.europa.eu/eurostat/fr/web/gisco/geodata/reference-data/administrative-units-statistical-units/communes#communes16
--version 2016 (EPSG 4326 ou 3857 au choix) - UTF-8
--
--Le fond Euro Boundary map coute une blinde
--https://eurogeographics.org/maps-for-europe/licensing/
--
--Chez Eurostat, les LAU2, avec Grece, Turquie et Irlande seulement
--https://ec.europa.eu/eurostat/fr/web/nuts/local-administrative-units
-- Turquie : latin 1 , EPSG 4230
-- 
--Les données des USA
--https://www.census.gov/programs-surveys/geography/guidance/tiger-data-products-guide.html
--https://www.census.gov/geographies/mapping-files/time-series/geo/cartographic-boundary.html
--https://www2.census.gov/geo/tiger/GENZ2019/shp/cb_2019_us_state_500k.zip
--1 : 500,000 (national)  projection 4269

-- achat des limites historiques de 1700 : 192 euros
-- https://www.euratlas.net/shop/maps_gis/fr_gis_1700.html
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\gis_1700\data\1700\utf8\2nd_level_divisions.shp
 
--l'UKRAINE : 4326
--http://worldmap.harvard.edu/data/geonode:ukraine_may_2014_administrative_units_9ob
--
--La Crimée : 4326
--- http://worldmap.harvard.edu/data/geonode:crimea_9sr

-- Importer le fond NON ESPON space (3857)
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\cartes\NUTS_2003\GEOM\map_template_1_(eurogeographics_20M)\non_espon_space_2003.shp
-- 
-------------------------------------------------------------------------
export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\COMM_RG_01M_2016_3857.shp\COMM_RG_01M_2016_3857.shp -a_srs EPSG:3857 -nln LAU_europe -nlt MULTIPOLYGON

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\gis_1700\data\1700\utf8\2nd_level_divisions.shp -a_srs EPSG:3395 -nln gis_1700_division2 -nlt MULTIPOLYGON

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\gis_1700\data\1700\utf8\sovereign_states.shp -a_srs EPSG:3395 -nln gis_1700_sovereign_states -nlt MULTIPOLYGON

-- ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\cartes\NUTS_2003\GEOM\map_template_1_(eurogeographics_20M)\non_espon_space_2003.shp -a_srs EPSG:4258 -nln around_europe -nlt MULTIPOLYGON
drop table around_europe 
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\non_espon_space_3857.shp -a_srs EPSG:3857 -nln around_europe -nlt MULTIPOLYGON

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\cb_2019_us_state_20m\cb_2019_us_state_20m.shp -a_srs EPSG:4269 -nln usa -nlt MULTIPOLYGON

-- Rajouter la Turquie ((latin 1)
set PGCLIENTENCODING=latin1
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\Geodata\TR_2011_LAU1.shp -a_srs EPSG:4230 -nln LAU_turquie -nlt MULTIPOLYGON

-- Rajouter l'Ukraine 4326 au niveau des régions administratives
export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\ukraine_may_2014_administrative_units_9ob\ukraine_may_2014_administrative_units_9ob.shp -a_srs EPSG:4326 -nln Ukraine -nlt MULTIPOLYGON

alter table ukraine add geom3857 geometry;
update ukraine set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857);

insert INTO LAU_europe(COMM_NAME, COMM_ID, FID, CNTR_CODE, wkb_geometry)
(SELECT ADI AS COMM_NAME , ICC_LAU_CO as COMM_ID, ICC_LAU_CO as FID, ICC as CNTR_CODE, st_setsrid(st_transform(wkb_geometry, 3857), 3857) as wkb_geometry 
FROM LAU_turquie)

alter table gis_1700_division2 add geom3857 geometry;
update gis_1700_division2 set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857);

alter table gis_1700_sovereign_states add geom3857 geometry;
update gis_1700_sovereign_states set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857);

alter table LAU_europe add methodg int;
-- 1 pour inclusion totale
-- 2 pour intersection avec des polygones mais de la meme entite sup
-- 2 pour intersection avec des polygones d'entités distinctes
-- null quand il faut étendre

alter table LAU_europe add id_hgis int; -- jointure avec num de gis_1700_division2
alter table LAU_europe add id_sup int; -- jointure avec owner_id de gis_1700_division2
alter table LAU_europe add id_sup_holder int; -- jointure avec holder_id de gis_1700_division2

select count(distinct num) from gis_1700_division2
-- 302 / 836
select count(distinct holder_id) from gis_1700_division2
-- 92

select count(*) from LAU_europe
-- 122750


select e.comm_name , d.short_name , 1 as methodg, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where st_contains(d.geom3857, e.wkb_geometry)


update LAU_europe set methodg = null, id_hgis=null, id_sup = null, id_sup_holder=null, province_code = null, province_name = null , methodnum = null

update LAU_europe e
set methodg = 1, id_hgis = d.num, id_sup=d.owner_id, id_sup_holder=d.holder_id
from gis_1700_division2 d
where methodg is null and st_contains(d.geom3857, e.wkb_geometry)  
-- 97008

select e.ogc_fid, e.comm_name , d.short_name , 2 as methodg, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
order by e.ogc_fid 
-- Andorra par exemple

select e.ogc_fid , count(distinct owner_id) as c , 2 as methodg
--, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
group by e.ogc_fid 
having count(distinct owner_id)= 1
order by e.ogc_fid 

update LAU_europe e
set methodg = 2,  id_sup=d.owner_id
from gis_1700_division2 d,
	(select e.ogc_fid , count(distinct owner_id) as c , 2 as methodg
	--, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
	from LAU_europe e, gis_1700_division2 d
	where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
	group by e.ogc_fid 
	having count(distinct owner_id)= 1 ) as k
where e.methodg is null and k.ogc_fid=e.ogc_fid 
and st_intersects(d.geom3857, e.wkb_geometry)  
-- 16404

select count(*) from LAU_europe e
where methodg is null
-- 9337

select e.ogc_fid, e.comm_name , d.short_name , 3 as methodg, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
order by e.ogc_fid 

update LAU_europe e
set methodg = 3, id_hgis = d.num, id_sup=d.owner_id, id_sup_holder=d.holder_id
from gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
-- 7813 (3 : il faut retraiter pour attribuer aux plus grandes unités qui intersectent)

select count(*) from LAU_europe e
where methodg is null
-- 1538

-- régler le cas de Donets'ka en Ukraine
-- c'est une partie qui devrait faire partie de la subdivision DON COSSACKS (voir histoire de la ville de Donets), qui appartient à la Russie

select * from ukraine where adm1_name = 'Donets''ka'
select e.* from LAU_europe e, ukraine u
where st_contains(u.geom3857, e.wkb_geometry) and u.adm1_name = 'Donets''ka' and methodg is null

update LAU_europe e set methodg=4, id_hgis=2, id_sup = 2344, id_sup_holder=-1
from ukraine u 
where st_contains(u.geom3857, e.wkb_geometry) and u.adm1_name = 'Donets''ka' and methodg is null
-- 6 ok

-- régler le cas la Finlande, la Suède et la Norvège

select e.* from LAU_europe e, world_borders wb 
where st_contains(wb.mpolygone3857, e.wkb_geometry) and wb."name" = 'Norway' and methodg is null

select e.* from LAU_europe e where cntr_code = 'NO' and methodg is null
update LAU_europe set methodg=4, id_hgis=71, id_sup = 892, id_sup_holder=-1 where cntr_code = 'NO' and methodg is null;
update LAU_europe set province_code ='NO', province_name = 'Norvège' where cntr_code = 'NO' ;

-- 426

select e.* from LAU_europe e, world_borders wb 
where st_contains(wb.mpolygone3857, e.wkb_geometry) and wb."name" = 'Sweden' and methodg is null


select e.* from LAU_europe e where cntr_code = 'SE' and methodg is null
update LAU_europe set methodg=4, id_hgis=158, id_sup = 8246, id_sup_holder=-1 where cntr_code = 'SE' and methodg is null;
-- 63

-- La Finlande est Russe à partir de 1809
-- https://fr.wikipedia.org/wiki/Finlande#Domination_su%C3%A9doise_puis_russe
select e.* from LAU_europe e where cntr_code = 'FI' and methodg is null
update LAU_europe set methodg=4, id_hgis=192, id_sup = 8246, id_sup_holder=-1 where cntr_code = 'FI' and methodg is null;
update LAU_europe set province_code ='FI', province_name = 'Finlande'  where cntr_code = 'FI' ;
-- 311

-- la Poméranie Suedoise (num 163 ) e Suède (8246) 
update LAU_europe set province_code ='POMS', province_name = 'Poméranie suédoise'  where id_sup = 8246 
and id_hgis=163 and nuts_code in ('DE80N', 'DE80L', 'DE80J', 'PL424' , 'PL428')
-- 270

-- régler le cas des iles SHetlands (appartiennent à GB, SHETLAND)
update LAU_europe set methodg=4, id_hgis=192, id_sup = 2165, id_sup_holder=-1 where 
comm_id in ('GBS13002772', 'GBS13002773', 'GBS13002777') 
and methodg is null;


-- régler le cas de l'Islande (IS), des iles Feroe (FO), du Groenland (GL) qui appartiennent au royaume Denmark-Norvège (892)
update LAU_europe set methodg=4, id_hgis=0, id_sup = 892, id_sup_holder=-1 where 
cntr_code  in ('FO', 'IS', 'GL') 
and methodg is null;
-- 103 + 5

-- régler le cas des canaries 
update LAU_europe set methodg=4, id_hgis=85, id_sup = 1122, id_sup_holder=-1 where 
nuts_code  in ('ES703', 'ES704', 'ES705', 'ES706', 'ES707', 'ES708', 'ES709') 
and methodg is null;
update LAU_europe set province_code='ES7XX', province_name = 'Canaries' where nuts_code  in ('ES703', 'ES704', 'ES705', 'ES706', 'ES707', 'ES708', 'ES709') 

-- 68

-- régler le cas des acores (PT200) 
update LAU_europe set methodg=4,  id_sup = 3852, id_sup_holder=-1 where 
nuts_code  in ('PT200') 
and methodg is null;
-- 156

-- régler le cas de la Martinique, Guadeloupe, Guyanne , Mayotte, Réunion

alter table LAU_europe add column province_code text;
alter table LAU_europe add column province_name text;

update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY30', province_name='Guyanne' where 
nuts_code  in ('FRY30') and methodg is null; --22
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY20', province_name='Martinique' where 
nuts_code  in ('FRY20') and methodg is null; --34
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY10', province_name='Gualeloupe' where 
nuts_code  in ('FRY10') and methodg is null; --33
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY50', province_name='Mayotte' where 
nuts_code = 'FRY50' and methodg is null; --17
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY40', province_name='Réunion' where 
nuts_code = 'FRY40' and methodg is null; --24

--  le cas de BL97701 - Saint-Barthélemy
-- cédé par la France à la Suède en 1784
-- https://en.wikipedia.org/wiki/Saint_Barth%C3%A9lemy#17th_century
update LAU_europe set methodg=4,  id_sup = 8246, id_sup_holder=-1, province_code='BL97701', province_name='Saint-Barthélemy' where 
comm_id = 'BL97701' and methodg is null;

select count(*) from LAU_europe e
where methodg is null
-- 496

-- 1122	Espagne	Spain	FK	colonies espagnoles d'Amérique	Spanish colonies in America	les malouines (Falkland Islands)

update LAU_europe set methodg=4,  id_sup = 1122, id_sup_holder=-1, province_code='FK', province_name='Falkland Islands' where 
comm_id = 'FK' and methodg is null; --1



-- En Grèce

-- Paxon (Venise 2490, num = 124, STATO DEL MAR)
-- https://fr.wikipedia.org/wiki/Paxos
update LAU_europe set methodg=4,  id_sup = 2490, id_sup_holder=-1, id_hgis=124 where 
ogc_fid = 84363  and methodg is null;

-- Agkistriou
update LAU_europe set methodg=4,  id_sup = 2490, id_sup_holder=-1, id_hgis=124 where 
ogc_fid = 84729 and methodg is null;

-- Sikinou
update LAU_europe set methodg=4,  id_sup = 6084, id_sup_holder=-1, id_hgis=146 where 
ogc_fid = 84805  and methodg is null;

-- les iles européennes
update LAU_europe set methodg=4,  id_sup = 1122, id_sup_holder=-1, id_hgis=61 where 
ogc_fid = 37897  and methodg is null;

-- iles de Bretagne
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, id_hgis=14 where 
nuts_code in ('FRH01', 'FRH02', 'FRH04')  and methodg is null; --17

update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, id_hgis=11 where 
nuts_code in ('FRG05')  and methodg is null;
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, id_hgis=12 where 
nuts_code in ('FRI32')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 1122, id_sup_holder=-1, id_hgis=73 where 
ogc_fid=96482  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 2750, id_sup_holder=-1, id_hgis=36 where 
ogc_fid=106713  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 2490, id_sup_holder=-1, id_hgis=124 where 
ogc_fid= 83701 and methodg is null;

update LAU_europe set methodg=4,  id_sup = 1423, id_sup_holder=-1, id_hgis=136 where 
nuts_code in ('UKK30')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 4117, id_sup_holder=1281, id_hgis=5 where 
nuts_code in ('DE947', 'DE94H')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 6319, id_sup_holder=-1, id_hgis=3 where 
nuts_code in ('DE94A')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=110 where 
nuts_code in ('DEF07', 'DEF09')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 1423, id_sup_holder=-1, id_hgis=77 where 
nuts_code in ('IE042')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=119 where 
nuts_code in ('DK014')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=79 where 
nuts_code in ('DK022')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=54 where 
nuts_code in ('DK031')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=118 where 
nuts_code in ('DK041', 'DK042')  and methodg is null;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

select st_buffer(st_union(e.wkb_geometry), 0)
from LAU_europe e
where cntr_code = 'SE'

drop  table ports.world_1789 
create table ports.world_1789 
(
id serial,
unit text,
unit_id int,
unit_code text,
unitlevel int, --0 pour etat, 1 pour sous-unité
unit_sup_id int,
unit_sup_code text,
sourcegeom text,
sourcecode text,
geom3857 geometry
)


insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e
group by id_sup
-- 92

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e WHERE id_hgis IS NOT null
group by id_hgis
-- 275, 2min 21

select * from LAU_europe e where id_hgis is null

update LAU_europe e set id_hgis = null, province_name = 'Island', province_code = 'IS' where cntr_id = 'IS' and id_hgis = 0;
update LAU_europe e set id_hgis = null, province_name = 'Faroe Islands', province_code = 'FO' where cntr_id = 'FO' and id_hgis = 0;
update LAU_europe e set id_hgis = null, province_name = 'Greenland', province_code = 'GL' where cntr_id = 'GL' and id_hgis = 0;

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_collect(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null
group by province_code, province_name

drop table around_europe 

-- Suède
insert into ports.world_1789 (unit, unit_id, subunit, subunit_id,sourcegeom, sourcecode,  geom3857)
(select 'Denmark', 8246, 'Sweden', 158, 'LAU_europe', 'SE', st_union(e.wkb_geometry) 
from LAU_europe e
where cntr_code = 'SE')
-- correction
update ports.world_1789 set unit='Sweden', unit_id=8246, subunit_id=null, subunit=null   where sourcecode ='SE'
select * from ports.world_1789

insert into ports.world_1789 (unit, unit_id, subunit, subunit_id,sourcegeom, sourcecode,  geom3857)
(select 'Sweden', 8246, 'Finland', 192, 'LAU_europe', 'FI', ST_union(e.wkb_geometry) 
from LAU_europe e
where cntr_code = 'FI')



insert into ports.world_1789 (unit, unit_id, subunit, subunit_id,sourcegeom, sourcecode,  geom3857)
(select 'Denmark', 892, 'Norway', 71, 'LAU_europe', 'NO', ST_union(e.wkb_geometry) 
from LAU_europe e
where cntr_code = 'NO')




-- plus tard
update ports.world_1789 set geom3857 = ST_UnaryUnion (geom3857) where subunit = 'Sweden'
select * from ports.world_1789;

--------------------------------------------------------------------------------

--------------------
select count(*) from LAU_europe e
where methodg is null
-- 810

-- Traiter les cas d'intersection 3 (à cheval sur deux états) 
-- calculer l'intersection entre la division owner_id et la commune LAU
-- attribuer la commune à la partie la plus grande 

select count(*) from LAU_europe e
where methodg = 3
-- 7813

select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
-- set methodg = 3, id_hgis = d.num, id_sup=d.owner_id, id_sup_holder=d.holder_id
from gis_1700_division2 d, LAU_europe e
where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) --e.id_sup = d.owner_id and
order by e.ogc_fid, surface 
-- 54 secondes

-- and  e.comm_name = 'Andorra' 
select * from LAU_europe e where e.comm_name = 'Andorra' -- 2 communes 'Andorra' en Espagne

select k.ogc_fid, max(surface)
from 
	(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
	from gis_1700_division2 d, LAU_europe e
	where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and e.ogc_fid = 17949
	) as k
group by ogc_fid


select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  
from gis_1700_division2 d, 
	LAU_europe e,
	(select k.ogc_fid, max(surface) as surfacemax
		from 
		(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
		from gis_1700_division2 d, LAU_europe e
		where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and e.ogc_fid = 17949
		) as k
	group by ogc_fid) as qmax -- 7813
where e.ogc_fid = 17949 and methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)



select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , d.num, d.holder_id 
from gis_1700_division2 d, 
	LAU_europe e,
	(select k.ogc_fid, max(surface) as surfacemax
		from 
		(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
		from gis_1700_division2 d, LAU_europe e
		where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) 
		) as k
	group by ogc_fid) as qmax -- 7813
where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid 
and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)
-- ok

update LAU_europe e set id_hgis = s.num, id_sup=s.owner_id, id_sup_holder=s.holder_id
from (
	select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , d.num, d.holder_id 
	from gis_1700_division2 d, 
		LAU_europe e,
		(select k.ogc_fid, max(surface) as surfacemax
			from 
			(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
			from gis_1700_division2 d, LAU_europe e
			where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) 
			) as k
		group by ogc_fid) as qmax -- 7813
	where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid 
	and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)
) as s 
where s.ogc_fid = e.ogc_fid 
-- 7813

-- C'est bon, avec id_sup, toutes les LAU ont leur ETAT de renseigné

select count(*) from LAU_europe e
where methodg is null
-- 332

select black_ogc_fid, max(longueurcommune) 
from 
(
	select black_ogc_fid, id_sup, sum(commun) as longueurcommune
	from (
		select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_sup, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
		from LAU_europe e1,LAU_europe e2
		where e2.methodg is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true
		order by e2.ogc_fid
	) as k
	group by black_ogc_fid, id_sup
	-- order by black_ogc_fid
) as q
group by black_ogc_fid


update LAU_europe e set methodg=5, id_sup=s.id_sup
from (
	select lmax.black_ogc_fid, id_sup
	from 
	(
		select black_ogc_fid, max(longueurcommune) 
		from 
		(
			select black_ogc_fid, id_sup, sum(commun) as longueurcommune
			from (
				select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_sup, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
				from LAU_europe e1,LAU_europe e2
				where e2.methodg is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_sup is not null
				order by e2.ogc_fid
			) as k
			group by black_ogc_fid, id_sup
			-- order by black_ogc_fid
		) as q
		group by black_ogc_fid
	) as lmax,
	(select black_ogc_fid, id_sup, sum(commun) as longueurcommune
		from (
			select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_sup, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
			from LAU_europe e1,LAU_europe e2
			where e2.methodg is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_sup is not null
			order by e2.ogc_fid
		) as k
		group by black_ogc_fid, id_sup
		order by black_ogc_fid
	) as k
	where k.black_ogc_fid = lmax.black_ogc_fid 
) as s 
where s.black_ogc_fid = e.ogc_fid and methodg is null
-- 365
-- 27 au second passage
-- 6 au troisième passage
-- 3 au 4eme passage
-- 0 ensuite

	



select count(*) from LAU_europe e
where methodg is null
-- 0

select  id_hgis, count(*) 
from LAU_europe e
group by id_hgis

select * from LAU_europe where id_hgis is null and methodg != 2 and methodg != 5

-- Mettre hgis à jour pour les cas 2 puis 5 par extension

alter table LAU_europe add column methodnum int;

update LAU_europe e
set methodnum = 1, id_hgis = d.num
from gis_1700_division2 d
where methodnum is null and st_contains(d.geom3857, e.wkb_geometry) 
-- 97262

select e.ogc_fid , count(distinct num) as c , 2 as methodnum
from LAU_europe e, gis_1700_division2 d
where methodnum is null and st_intersects(d.geom3857, e.wkb_geometry)
group by e.ogc_fid 
having count(distinct num)= 1
order by e.ogc_fid 

update LAU_europe e
set methodnum = 2,  id_hgis=d.num
from gis_1700_division2 d,
	(select e.ogc_fid , count(distinct num) as c , 2 as methodnum
	from LAU_europe e, gis_1700_division2 d
	where methodnum is null and st_intersects(d.geom3857, e.wkb_geometry)
	group by e.ogc_fid 
	having count(distinct num)= 1) as k
where e.methodnum is null and k.ogc_fid=e.ogc_fid 
and st_intersects(d.geom3857, e.wkb_geometry)  
-- 4623

update LAU_europe e
set methodnum = 3, id_hgis = d.num
from gis_1700_division2 d 
where methodnum is null and st_intersects(d.geom3857, e.wkb_geometry)
-- 20284 (3 : il faut retraiter pour attribuer aux plus grandes unités qui intersectent)

update LAU_europe e set id_hgis = s.num
from (
	select e.ogc_fid, e.comm_name, d.num , d.short_name  
	from gis_1700_division2 d, 
		LAU_europe e,
		(select k.ogc_fid, max(surface) as surfacemax
			from 
			(select e.ogc_fid, e.comm_name, d.num , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
			from gis_1700_division2 d, LAU_europe e
			where methodnum = 3 and st_intersects(d.geom3857, e.wkb_geometry) 
			) as k
		group by ogc_fid) as qmax -- 7813
	where methodnum = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid 
	and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)
) as s 
where s.ogc_fid = e.ogc_fid 
-- 20284, 3min 18 s 

select count(*) from LAU_europe e
where methodnum is null
--------------

update LAU_europe e set methodnum=5, id_hgis=s.id_hgis
from (
	select lmax.black_ogc_fid, id_hgis
	from 
	(
		select black_ogc_fid, max(longueurcommune) 
		from 
		(
			select black_ogc_fid, id_hgis, sum(commun) as longueurcommune
			from (
				select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_hgis, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
				from LAU_europe e1,LAU_europe e2
				where e2.methodnum is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_hgis is not null
				order by e2.ogc_fid
			) as k
			group by black_ogc_fid, id_hgis
			-- order by black_ogc_fid
		) as q
		group by black_ogc_fid
	) as lmax,
	(select black_ogc_fid, id_hgis, sum(commun) as longueurcommune
		from (
			select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_hgis, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
			from LAU_europe e1,LAU_europe e2
			where e2.methodnum is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_hgis is not null
			order by e2.ogc_fid
		) as k
		group by black_ogc_fid, id_hgis
		order by black_ogc_fid
	) as k
	where k.black_ogc_fid = lmax.black_ogc_fid 
) as s 
where s.black_ogc_fid = e.ogc_fid and methodnum is null;
-- 1131
-- 28
-- 6
-- 3


select  id_hgis, count(*) 
from LAU_europe e
group by id_hgis


/*
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\shp\Pologne_1789.shp -a_srs EPSG:3857 -nln Pologne_1789 -nlt MULTIPOLYGON
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\shp\Prussia_1789.shp -a_srs EPSG:3857 -nln Prussia_1789 -nlt MULTIPOLYGON

select * from ports.pologne_1789 p2 
select st_buffer(st_boundary(ST_UnaryUnion(st_collect(p.wkb_geometry)) ),0.1)
from ports.pologne_1789 p 
group by id
-- select * from ports.pologne_1789 p 
alter table  pologne_1789 add unaryunion geometry;

update pologne_1789 set unaryunion = k.geom
from (select ST_UnaryUnion(st_collect(wkb_geometry)) as geom from ports.pologne_1789 ) as k
where ogc_fid = 1

-- update ports.pologne_1789 set id = 29 where id is null
alter table  prussia_1789 add unaryunion geometry;
select * from ports.prussia_1789 p2 
update prussia_1789 set unaryunion = k.geom
from (select ST_UnaryUnion(st_collect(wkb_geometry)) as geom from ports.prussia_1789 ) as k
where ogc_fid = 1

-- ok
update world_1789 w set geom3857 = unaryunion, sourcegeom = w.sourcegeom || ', around_europe'
from ports.prussia_1789 p2 
where p2.ogc_fid = 1 and w.id  = 66

update world_1789 w set geom3857 = unaryunion, sourcegeom = w.sourcegeom || ', around_europe'
from ports.pologne_1789 p2 
where p2.ogc_fid = 1 and w.id  = 29



select st_unaryunion(st_collect(geom3857)) from (
select geom3857 from ports.world_borders_dump where name like 'Russia' and st_contains(geom3857, capitale)
-- union 
-- select geom3857 from ports.world_borders_dump where name like 'Kazakhstan' 
union 
select geom3857 from ports.world_1789 w where unit_id = 2344 and unitlevel = 0
UNION
select geom3857 from ports.world_borders_dump where name like 'Russia' and NOT st_contains(geom3857, capitale) 
AND st_isvalid(geom3857) AND pkid != 1609
union 
select geom3857 from ports.world_borders_dump where name like 'Georgia' 
) as k
-- Rajouter la Georgia
pkid = 1741

http://worldmap.harvard.edu/data/geonode:_boundaries_5c5
*/
-- 228

---------------------------------------------------------------
-- Piemont (unit_id = 4705)
--------------------------------------------------------------

-- import des circonscriptions 1861 d'Italie (en ligne)
-- + Montferra (2310)
-- + Masserano (7184)
-- Retirer de Milan (1122) les circonscriptions suivantes
-- Ossola, Valsesia, Biella, Lomallina, Alessandria, Tortona, 
-- Novi (partie qui intersecte Milan - 188), Bobbio (partie qui intersecte Milan - 39),
-- Voghera (321)
-- Ajouter la Sardaigne : num = 65 à 68

-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\Limiti_1861\Circondari_1861

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports"  C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\Limiti_1861\Circondari_1861\Circondari_1861.shp -a_srs EPSG:32632 -nln Italie_1861 -nlt MULTIPOLYGON

alter table Italie_1861 add geom3857 geometry
update Italie_1861 set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857)




SELECT * FROM ports.world_1789 WHERE unit_id in (4705, 2310, 7184)
delete from ports.world_1789 WHERE unit_id in (4705, 2310, 7184) 

update LAU_europe e set id_sup = 4705 where id_sup in (2310, 7184);
update LAU_europe e set id_sup = 4705, province_code = 'SA02', province_name='Sardaigne' where id_hgis in (65,66,67,68) and id_sup=1122 -- Sardaigne
update LAU_europe e set id_sup = 4705 where ogc_fid in (92731) -- commune d'Alagna
update LAU_europe e set id_sup = 4705 where ogc_fid = 93639

SELECT * FROM ports.world_1789 WHERE unit_id in (1122) -- Milan

DROP TABLE test_piemont
create table test_piemont as (
select st_unaryunion(st_collect(e.wkb_geometry)) 
from LAU_europe e, italie_1861 i 
where e.id_sup = 1122 and i.den_circ in ('Ossola', 'Valsesia', 'Biella', 'Lomellina', 'Alessandria', 'Tortona', 'Voghera', 'Vercelli', 'Casale Monferrato', 'Pavia', 'Novi', 'Bobbio')
and st_intersects(i.geom3857 , e.wkb_geometry ) AND st_area(st_intersection(i.geom3857 , e.wkb_geometry))/st_area(e.wkb_geometry) > 0.80
)  

update LAU_europe e set id_sup = 4705
from italie_1861 i 
where e.id_sup = 1122 and i.den_circ in ('Ossola', 'Valsesia', 'Biella', 'Lomellina', 'Alessandria', 'Tortona', 'Voghera', 'Vercelli', 'Casale Monferrato', 'Pavia', 'Novi', 'Bobbio')
and st_intersects(i.geom3857 , e.wkb_geometry ) AND st_area(st_intersection(i.geom3857 , e.wkb_geometry))/st_area(e.wkb_geometry) > 0.80

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e WHERE id_sup = 4705
group by id_sup
--------------------------------------





--- Corse
-- --Corse (nuts_code = 'FRM01', 'FRM02') e France (1225)

SELECT id_sup, id_hgis, province_code , province_name FROM lau_europe le where le.nuts_code in ('FRM02', 'FRM01')
update lau_europe set id_hgis = null, province_code = 'FRM', province_name = 'Isles de Corse', id_sup='1225'
where nuts_code in ('FRM02', 'FRM01');
-- 360

-- Lorraine
-- --Lorraine [owner_id = 2514 ] ?? e France (1225)
-- la Lorraine : duché acquis par la France en 1766
SELECT id_sup, id_hgis, province_code , province_name FROM lau_europe le where id_sup = 2514
update lau_europe set id_hgis = null, province_code = 'FRL', province_name = 'Lorraine', id_sup='1225'
where id_sup = 2514;
--1015

SELECT max(id_sup) FROM lau_europe
-- 15204

-- 100000

-- --Naples (num 77 et 82) e Royaume de Naples et plus à l'Espagne
-- Naples (69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80) = NA01 (province_code), et NA 
update lau_europe set  province_code = 'NA01', province_name = 'Naples', id_sup=100000
where id_hgis in (69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80) and id_sup = 1122;

update lau_europe set  province_code = 'NA02', province_name = 'Sicile', id_sup=100000
where id_hgis in (81, 82, 83) and id_sup = 1122;

update lau_europe set  province_code = 'NA03', province_name = 'Etat des Présides', id_sup=100000
where id_hgis in (34) and id_sup = 1122;
select * from lau_europe where province_code = 'NA03'
update lau_europe set province_code = null, province_name = null, id_sup = '62' where province_code = 'NA03'


update lau_europe set  province_code = 'NA03', province_name = 'Etat des Présides', id_sup=100000, id_hgis=NULL where ogc_fid in ( 97064, 96987, 96338)
-- id_sup = 62, id_hgis = 34

-- Pays-bas (actuelle Belgique) et Milanias: mettre dans la même couleur que l'Autriche et pas dans la même couleur que l'Espagne (changement de domination en 1713)

-- Milanais (id_hgis = 33, id_sup = 1122) appartient aux habsbourgs (Autriche), owner_id = 5689
update lau_europe set   id_sup=5689  where id_hgis = 33 and id_sup = 1122

-- Pays-bas (actuelle Belgique) autrichiens
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (2, 26, 92, 91, 13, 94) and id_sup = 1122 and cntr_code in ('BE', 'LU')
--- reste à finir
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (2, 92) and id_sup = 1122 and cntr_code in ('FR');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (94) and id_sup = 1122 and cntr_code in ('FR');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (26,13) and id_sup = 1122 and cntr_code in ('NL');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (5) and id_sup = 1122 and cntr_code in ('NL');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (5) and id_sup = 1122 and cntr_code in ('DE');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (94,13) and id_sup = 1122 and cntr_code in ('DE');

/*
--- Ravennes, id_hgis = 13 se retrouve dans l'Autriche, alors qu'elle appartient aux Papal States
update lau_europe set province_code = null, province_name = null, id_sup = 2140 where province_code = 'AU03' and id_sup =5689 and id_hgis=13  and nuts_code like 'IT%'
select * from lau_europe where province_code = 'AU03' and id_sup =5689 and id_hgis in (2, 26, 92, 91, 13, 94) order by nuts_code

-- Corriger sur la Suisse
update lau_europe set province_code = null, province_name = null, id_sup = 5666 where province_code = 'AU03' and id_sup =5689 and id_hgis in (2, 26, 92, 91, 13, 94) and nuts_code like 'CH%'
-- Corriger sur la France
update lau_europe set province_code = null, province_name = null, id_sup = 1225 where province_code = 'AU03' and id_sup =5689 and id_hgis in (2, 26, 92, 91, 13, 94) and nuts_code like 'FR%'
-- Corriger en allemagne De : 9642 Passau
update lau_europe set province_code = null, province_name = null, id_sup = 9642 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and (nuts_code = 'DE225' or nuts_code = 'DE228')
update lau_europe set province_code = null, province_name = null, id_sup = 9642 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and (ogc_fid = 25026)
update lau_europe set province_code = null, province_name = null where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and (ogc_fid = 1263)

-- Corriger en allemagne De : Ratzeburg
update lau_europe set province_code = null, province_name = null, id_sup = 8445 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DE80M'
update lau_europe set province_code = null, province_name = null, id_sup = 8445 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DEF06'

-- Corriger Saxe-Hildburghausen 4289 DE24C, DEG%, DE247
update lau_europe set province_code = null, province_name = null, id_sup = 4289 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code like 'DEG%' OR nuts_code='DE247' OR nuts_code='DE24C'
-- 8583 Hechingen : DE143 (pas DE142 qui finalement aurait été Autrichienne
update lau_europe set province_code = null, province_name = null, id_sup = 8583 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DE143'
-- 8583 Hechingen :  Mössigen commune, DE142
update lau_europe set province_code = null, province_name = null, id_sup = 8583 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and ogc_fid = 20214
--MD, RO, UA e Ottoman : ERREUR sur UA et MD
update lau_europe set province_code = null, province_name = null, id_sup =  6084
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code like 'RO%' OR cntr_code like 'UA%' OR cntr_code like 'MD%'
-- correction de UA
update lau_europe set  id_sup =  5689
where   id_sup =6084 and id_hgis in (83, 55) AND cntr_code like 'UA%'
-- correction de MD
update lau_europe set  id_sup =  2750
where   id_sup =6084 and id_hgis in (33) AND cntr_code like 'MD%'


-- 1160  Bernburg : DEE09 , DEE0C  :: ERREUR sur DEE0C
update lau_europe set province_code = null, province_name = null, id_sup = 1160 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DEE09' or nuts_code = 'DEE0C'
-- correction de DEE0C
update lau_europe set province_code = null, province_name = null, id_sup = 5141
-- select * from lau_europe
where   id_sup =1160 and id_hgis =23 and nuts_code = 'DEE0C'



-- Corriger Sweden
update lau_europe set province_code = null, province_name = null, id_sup = 8246 
where province_code = 'AU03' and id_sup =5689 and id_hgis =94 and nuts_code = 'SE213' 

-- Corriger le Portugal
update lau_europe set province_code = null, province_name = null, id_sup = 3852 
where province_code = 'AU03' and id_sup =5689 and cntr_code  = 'PT'
update lau_europe set province_code = null, province_name = null, id_sup = 3852 
where province_code = 'AU03' and id_sup =5689 and comm_id  in ('ES7006095', 'ES7006037')
-- Corriger l'Irlande (IE) e 1423
update lau_europe set province_code = null, province_name = null, id_sup = 1423 
where province_code = 'AU03' and id_sup =5689 and cntr_code  = 'IE'

-- Corriger la Grèce/Albanie
update lau_europe set province_code = null, province_name = null, id_sup = 6084 
where province_code = 'AU03' and id_sup =5689 and id_hgis = 26 and (cntr_code  = 'AL' or cntr_code  = 'EL')


-- correction de UA : encore beaucoup à restituer à la Pologne et la Russie
-- corriger en Pologne PL
update lau_europe set province_code = null, province_name = null, id_sup = 2750 
where comm_id = 'PL2002073' and id_hgis = 13
*/


-- En 1773 a lieu la première partition de la Pologne, bouffée en partie par la Russie, la Prusse et l'Autriche. 
-- Comme il y a deux autres partitions dans les années 1790, la carte de 1800 ne t'aidera pas. 
-- Je te joins une carte de cette région, il faut prendre celle de gauche pour tracer les frontières de la Pologne en 1789  

-- Pologne
-- l'unité 10 est deux fois en Pologne (Belz et )
update lau_europe set id_sup = 2750 
where  id_hgis = 10 and id_sup = 5141  and nuts_code = 'PL812';
update lau_europe set id_sup = 2750 
where  id_hgis = 10 and id_sup = 5141  and nuts_code = 'PL822';

-- La Galicia en Pologne : gros bazard à cheval avec l'Ukraine, qui va appartenir à l'Autriche (5689)
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('PL225', 'PL219', 'PL218', 'PL217');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('SK041');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('SK042');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('SK031');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('PL821', 'PL824');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('PL823');

update lau_europe set id_sup = 5689 
where  id_sup = 2750  and comm_id in ('PL1213023', 'PL1213062', 'PL1213043', 'PL1213052', 'PL1213072', 'PL1213082', 'PL1213011', 'PL1218102', 'PL1218013', 'PL1218093', 
'PL1218082', 'PL1213093', 'PL1218062', 'PL1218082', 'PL1218093', 'PL1218052', 'PL1218072', 'PL1218042', 'PL1218033', 'PL2410032', 'PL1218022');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  
and comm_id in ('PL1206113', 'PL1209073', 'PL1209042', 'PL1209082', 'PL1209022', 
'PL1209092', 'PL1209033', 'PL1206092', 'PL1206143', 'PL1209062', 'PL1209013', 'PL1209052',
'PL1219053', 'PL1219012', 'PL1219022', 'PL1201052', 'PL1201082', 'PL1201092',
'PL1219043', 'PL1219032', 'PL1201022', 'PL1201011', 'PL1201063', 'PL1201042', 'PL1201032', 'PL1201072')

update lau_europe set id_sup = 5689 
where  id_sup = 2750 and comm_id in ('UA120338','UA120320', 'UA120328', 'UA120325', 'UA120317')
-- doute vers Brody, Busk, Lwow
-- frontières avec Krakow ok, le long du fleuve.

-- La Russie gagne aussi du terrain sur la mer Noire en 1774, après une guerre contre l'Empire ottoman et en 1783:
-- la Géorgie est annexée en 1783 ainsi que la Crimée donc tout ce qui ici sur cette carte est indiqué acquis en 1774 + "crimée indépendante" et Kouban, annexés en 1783 doit être indiqué dans ta carte comme "Russie"
--Khersons'ka 3155
--Zaporiz'ka 3171
--Donets'ka 3152
--Kharkivs'ka 3154
-- Gain sur l'Empire Ottoman (6084)
select * from LAU_europe e, ukraine u 
where u.adm1_code in (3155, 3171, 3152, 3154) and st_intersects(u.geom3857, e.wkb_geometry)
and e.id_sup = 6084

update LAU_europe e set id_sup = 2344
from ukraine u 
where u.adm1_code in (3155, 3171, 3152, 3154) and st_intersects(u.geom3857, e.wkb_geometry)
and e.id_sup = 6084
-- 46

-- Importer la crimée indépendante d'aujourd'hui
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\crimea_9sr\crimea_9sr.shp
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\crimea_9sr\crimea_9sr.shp -a_srs EPSG:4326 -nln Crimee_2020 -nlt MULTIPOLYGON
alter table crimee_2020 add geom3857 geometry
update crimee_2020 set geom3857 = st_setsrid(st_transform(wkb_geometry , 3857), 3857)

-- elle appartient à la Russie (1783) - Gain sur l'Empire Ottoman (6084)
update LAU_europe e set id_sup = 2344
from crimee_2020 c
where  st_intersects(c.geom3857, e.wkb_geometry)
and e.id_sup = 6084

 -- Une petite partie de Mykolayivs'ka (3162)
 update LAU_europe e set id_sup = 2344 
 where comm_id = 'UA130630' and e.id_sup = 6084

 -- UA130687 sera à découper par le Fleuve (TODO)
  update LAU_europe e set id_sup = 2344 
 where comm_id = 'UA130687' and e.id_sup = 6084
 
 

-- Rajouter Ruthenia (28) 
update lau_europe set id_sup = 5689 
where  id_sup = 2750 and id_hgis = 28 and cntr_code in ('PL', 'UA')
update lau_europe set id_sup = 5689 
where  id_sup = 2750 and comm_id in ('UA180404', 'UA180406')




-- -- du coup, La Prusse ca donne un truc uni sur la Baltique et pas deux morceaux '
-- Prusse : 5141
/*
update lau_europe set province_code = null, province_name = null, id_sup = 5141 
where  id_hgis = 94 and province_code = 'AU03' and id_sup =5689 and cntr_code = 'PL'
*/

update lau_europe set province_code = null, province_name = null, id_sup = 5141 
where  id_hgis = 94 and id_sup = 2750  and cntr_code = 'PL'


-- Silesia : 61 (e 5689 avant)
-- la Silésie (Leipzig) passe à la Prusse (était autrichienne) en 1740

update lau_europe set id_sup = 5141 
where  id_hgis = 61  and id_sup =5689 and cntr_code = 'PL'
-- and cntr_code = 'PL'

-- En Pomeralia (36), la commune Gdansk reste polonaise (2750), mais le reste de la Pomeralia devient Prusse
update lau_europe set  id_sup = 5141 
where  id_hgis = 36 and id_sup = 2750 and cntr_code = 'PL' and comm_id != 'PL2261011'

-- finalement Gdansk reste une ville libre
update lau_europe set  id_sup = 100004 
where   comm_id = 'PL2261011'

-- Podnan (35), Butow, Pomerania , Warmia, Malbork : 35, 10, 5, 35, 94, 14 
update lau_europe set  id_sup = 5141 
where  id_hgis in (35, 10, 5, 35, 94, 14) and id_sup = 2750 and cntr_code = 'PL' 


-- Venice
update lau_europe set  id_sup = 6084 
where  nuts_code in ('EL621', 'EL623', 'EL624', 'EL632', 'EL633', 'EL651', 'EL652', 'EL653', 'EL307') and id_sup = 2490 and cntr_code = 'EL' 
-- commune de Tinou 
update lau_europe set  id_sup = 6084 
where  comm_id = 'GR06127001' and id_sup = 2490 and cntr_code = 'EL' 

-- San Marino en Italie devrait être dans les Papal States (TODO)
-- IT311041060
update lau_europe set  id_sup = 6084 
where  comm_id = 'GR06127001' and id_sup = 2490 and cntr_code = 'EL' 

-- En mer Baltique, Ingria, Livonie et Estonie et les îles en face deviennent russes en 1721 (ce qui est en vert clair ici)
-- Estonie
update lau_europe set  id_sup = 2344
where id_sup=8246 and id_hgis in (86, 71, 207) and cntr_code = 'EE'

-- Livonie (Latvia - LV)
update lau_europe set  id_sup = 2344
where id_sup=8246 and id_hgis = 207 and cntr_code = 'LV'

-- L'espace germanique entre 1700 et 1789 ne bouge pas tant que ça: c'est une entité composée en gros par 350 Etats différents et ça reste extrêmement morcelé jusqu'en 1803:

-- Ecosse: acte d'union en 1707 (était déjà guvernée par le roi d'Angleterre mais après 1707 il n'y a plus deux royaumes distincts avec un seul roi, mais un seul royaume
update lau_europe set  province_code = 'UK02', province_name = 'Ecosse', id_sup=1423
where id_sup = 2165

update LAU_europe e set province_code = 'IE', province_name = 'Irlande'
where id_sup = 1423 and (cntr_code = 'IE' or nuts_code  in ('UKN0A','UKN0B','UKN0C','UKN0D','UKN0E','UKN0F', 'UKN0G','UKN06','UKN07','UKN08','UKN09')) 

update LAU_europe e set province_code = 'WL', province_name = 'Pays de Galles'
where id_sup = 1423 and (cntr_code = 'UK' and id_hgis  in (150,247,147,183,145,250,152,138,139,137,141,140,142)) 

update LAU_europe e set province_code = 'WL', province_name = 'Pays de Galles' where comm_id in ('GBW05000991', 'GBW05000984')
update LAU_europe e set province_code = 'UK02', province_name = 'Ecosse' where comm_id in ('GBS13002533', 'GBS13002532', 'GBS13002534')


-- https://fr.wikipedia.org/wiki/%C3%8Ele_de_Man#P%C3%A9riode_britannique
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_multi(geom3857), iso2, 'île de Man' from world_borders_dump where pkid = 306 

-- Jersey, Guernesey  :  Iles anglo-normandes
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(name_latn, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select 'Iles anglo-normandes', 'JE-GG',  6, 6, 1423, st_union(geom3857), 'JE-GG', 'Iles anglo-normandes' 
from world_borders_dump where id in (244, 243)

-- select sourcegeom from LAU_europe where province_code in ('JE-GG', 'IM')

----------------------------------------------------------------------------
--- Importer dans LAU les parties qui me manquent pour faire le monde
----------------------------------------------------------------------------

create table ports.world_borders_dump as (
SELECT wb.id, wb.name, wb.iso2, wb.region, wb.subregion, 
	st_setsrid(st_transform(st_setsrid(st_makepoint(lon, lat), 4326) , 3857), 3857) as capitale,
      (ST_Dump(wb.mpolygone3857)).geom AS geom3857
FROM ports.world_borders wb )

alter table ports.world_borders_dump add pkid serial;



delete from lau_europe e where methodg = 6


-- Belarus (appartient à la Pologne 2750) 
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'BY' as cntr_id,  name as name_latn, iso2 as cntr_code, 'BY'||id::text , 6, 2750, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 2738

-- Russsie dans la Prusse
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'RU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'RU'||id::text , 6, 5141, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 1609

-- Georgia (appartient à la Russie 2344)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'GE' as cntr_id,  name as name_latn, iso2 as cntr_code, 'GE'||id::text , 6, 2344, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 2292

-- La Russie de Moscou
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'RU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'RU'||id::text , 6, 2344, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 1741

select * from LAU_europe, port_points pp where cntr_id = 'RU' and pp.toponyme_standard_fr = 'Russie' and st_contains(wkb_geometry, pp.point3857)
update LAU_europe set wkb_geometry = st_multi(st_buffer(wkb_geometry, 2000)) where ogc_fid = 123716
-- select * from LAU_europe where ogc_fid = 123716

insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, fid, methodg, id_sup, wkb_geometry)
select 'RU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'RU'||id::text , 'RU'||pkid::text , 6, 2344, ST_Multi(geom3857)  
from world_borders_dump w where pkid NOT in (1741, 1609)   AND name like 'Russia' AND st_isvalid(geom3857)
-- 225


-- Bosnia and Herzegovina e Ottoman (6084)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'BA' as cntr_id,  name as name_latn, iso2 as cntr_code, 'BA'||id::text , 6, 6084, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 312

-- Montenegro e Ottoman (6084)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'ME' as cntr_id,  name as name_latn, iso2 as cntr_code, 'ME'||id::text , 6, 6084, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 3247

update LAU_europe set sourcegeom = 'world borders' where cntr_id in ('BY', 'RU', 'GE', 'RU', 'BA', 'ME')
-- select cntr_id, sourcegeom, * from LAU_europe where cntr_id in ('BY', 'RU', 'GE', 'RU', 'BA', 'ME')


-- Haiti (saint-Domingue) e France (1225)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, methodnum , province_code , province_name , wkb_geometry)
select 'HT' as cntr_id,  name as name_latn, iso2 as cntr_code, 'HT'||id::text , 6, 1225, 6, 'HT', 'Saint-Domingue', ST_Multi(geom3857)  from world_borders_dump w where name = 'Haiti'

--- Sainte-Lucie : en France
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry)
select 'LC', 'Sainte-Lucie', iso2, 'LC'||id::text,  6, 6, 1225, ST_Multi(geom3857) from world_borders_dump where pkid = 305
update LAU_europe set province_code='LC' , province_name='Sainte-Lucie' where cntr_id = 'LC' and id_sup=1225;

-- Tobago : en France
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1225, ST_Multi(geom3857), iso2, 'Tobago' from world_borders_dump where pkid = 887
update LAU_europe set province_code = 'TO' where cntr_id='TT' and province_name='Tobago';

-- Trinité (TT) : en Espagne
-- https://fr.wikipedia.org/wiki/Trinit%C3%A9_(%C3%AEle)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1122, ST_Multi(geom3857), iso2, 'Trinité' from world_borders_dump where pkid = 886

-- République dominicaine (DO) : en Espagne
-- https://fr.wikipedia.org/wiki/R%C3%A9publique_dominicaine
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1122, ST_Union(geom3857), iso2, 'République dominicaine' from world_borders_dump where id = 48 group by iso2, name, id

-- Puerto-Rico (PR) : en Espagne
-- https://fr.wikipedia.org/wiki/Porto_Rico
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1122, ST_Union(geom3857), iso2, 'Porto-Rico' from world_borders_dump where id = 174 group by iso2, name, id

-- Sainte-Croix 731 , Saint-Thomas 733 (iles vierges américaines depuis 1917) et une sans nom (732)
-- dans le Danemark en 1789 : 892
-- https://fr.wikipedia.org/wiki/Saint-Thomas_(%C3%AEles_Vierges_des_%C3%89tats-Unis)
-- https://fr.wikipedia.org/wiki/Sainte-Croix_(%C3%AEles_Vierges_des_%C3%89tats-Unis)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, 'Sainte-Croix', iso2, iso2||id::text,  6, 6, 892, ST_Multi(geom3857), iso2, 'îles vierges' from world_borders_dump where pkid = 731

insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, 'Saint-Thomas', iso2, iso2||id::text,  6, 6, 892, ST_Multi(geom3857), iso2, 'îles vierges' from world_borders_dump where pkid = 733

insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, 'Saint-Jean', iso2, iso2||id::text,  6, 6, 892, ST_Multi(geom3857), iso2, 'îles vierges' from world_borders_dump where pkid = 732


-- https://fr.wikipedia.org/wiki/Antigua-et-Barbuda
--  Antigua-et-Barbuda e Anglais (1423) et font partie des colonies anglaises en Amérique
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, name from world_borders_dump where id = 1 group by iso2, name, id

-- https://fr.wikipedia.org/wiki/Saint-Christophe-et-Ni%C3%A9v%C3%A8s
-- Appartient à la Grande Bretagne depuis 1713
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, 'îles du Vent' from world_borders_dump where id = 178 group by iso2, name, id

-- https://fr.wikipedia.org/wiki/Saint-Vincent-et-les-Grenadines
-- Appartient à la Grande Bretagne depuis 1783
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, 'Saint-Vincent-et-les-Grenadines' from world_borders_dump where id = 213 group by iso2, name, id

-- https://en.wikipedia.org/wiki/Grenada#British_colonial_period
-- https://fr.wikipedia.org/wiki/Grenade_(pays)
-- Appartient à la Grande Bretagne depuis 1783 grenada, 70
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, 'La Grenade' from world_borders_dump where id = 70 group by iso2, name, id

-- https://fr.wikipedia.org/wiki/Barbade
-- https://fr.wikipedia.org/wiki/Histoire_de_la_Barbade
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_multi(geom3857), iso2, 'La Barbade' from world_borders_dump where id = 11 

-- https://fr.wikipedia.org/wiki/%C3%8Ele_de_Man#P%C3%A9riode_britannique
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_multi(geom3857), iso2, 'île de Man' from world_borders_dump where pkid = 306 

-- Jersey, Guernesey  :  Iles anglo-normandes
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(name_latn, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select 'Iles anglo-normandes', 'JE-GG',  6, 6, 1423, st_union(geom3857), 'JE-GG', 'Iles anglo-normandes' 
from world_borders_dump where id in (244, 243)


-- Terre-Neuve e GB en grande partie (1423) sauf St pierre et Miquelon (france, 1225) 
-- Mais à refaire manuellement
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), 'TN', 'Terre-Neuve' from world_borders_dump 
where pkid in (986, 937,941,944,940,939,951,954, 956, 957, 958, 978, 983, 984, 988) 
group by iso2, name, id

-- Saint pierre et miquelon
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1225, st_union(geom3857), 'PM', 'Saint-Pierre-et-Miquelon' from world_borders_dump 
where id = 233 and iso2 = 'PM' group by iso2, name, id

--https://fr.wikipedia.org/wiki/Canada#Colonies_britanniques_des_Maritimes
-- Reste du Canada est anglais --> 1789 au moins
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), 'CA', 'Canada' from world_borders_dump 
where id = 24 and pkid not in (986, 937,941,944,940,939,951,954, 956, 957, 958, 978, 983, 984, 988)  group by iso2, name, id


-- Ile Maurice
-- https://fr.wikipedia.org/wiki/%C3%8Ele_Maurice#Histoire
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'MU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'MU'||id::text , 6, 1225, ST_Multi(geom3857)  from world_borders_dump w where pkid = 814
update LAU_europe set sourcegeom = 'world borders', province_code = 'MU', province_name = 'Île Maurice' where cntr_id = 'MU' and name_latn = 'Mauritius'

-- Malte
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry, sourcegeom)
select 'MT' as cntr_id,  name as name_latn, iso2 as cntr_code, 'MU'||id::text , 6, 100009, ST_union(geom3857), 'world borders'  
from world_borders_dump w where id = 117 group by iso2, name, id

alter table LAU_europe add sourcegeom text default 'LAU_europe';

select * from LAU_europe where methodg = 6 and province_code is not null
update LAU_europe set sourcegeom = 'world borders' where methodg = 6 and province_code is not null

-----------
-- Corrections Italie : Oneglia et Loano sont des enclaves
-- pas au piemont 4705 mais à Genes 1345
UPDATE LAU_europe SET id_sup = 1345 WHERE comm_id IN (
'IT107008037', 'IT107008034', 'IT107008023', 'IT107008046', 'IT107008004', 'IT107008042',
'IT107008066', 'IT107008018', 'IT107008012', 'IT107008005', 'IT107008049', 'IT107008010',
'IT107008019', 'IT107008020', 'IT107008033', 'IT107008010', 'IT107008006', 'IT107008054', 'IT107008024', 'IT107008021') AND id_sup = 4705

-- au piemont 4705 en plus et pas à genes 1345 + Loano
UPDATE LAU_europe SET id_sup = 4705 WHERE comm_id IN (
'IT107008010', 'IT107008041', 'IT107008067', 'IT107008017', 'IT107008013', 'IT107009034') AND id_sup = 1345


-- Corrections Italie : Lucca
--IT309046015 Gallicano
--IT309046010 Castiglione di Garfagnana
--IT309046019 Minucciano
--IT309045011 Montignoso
UPDATE LAU_europe SET id_sup = 4974, id_hgis = 1 WHERE comm_id IN (
'IT309046015', 'IT309046010', 'IT309046019', 'IT309045011') -- AND id_sup = 5262 (Modena)

-- Corrections Italie : Modene (5562) et pas Toscane(4973)
UPDATE LAU_europe SET id_sup = 5562, id_hgis = 1 WHERE comm_id IN (
'IT309045009')
-- IT309045009 Licciana Nardi pour Mallapina-Villafranca

-- Sardinia (Piemont 4705)
--IT309045015 Tresana
--IT309045013 Podenzana
--IT309045001 Aulla
--IT309045008 Fosdinovo
UPDATE LAU_europe SET id_sup = 4705, id_hgis = 4 WHERE comm_id IN (
'IT309045015', 'IT309045013', 'IT309045001', 'IT309045008') 

-- Toscane
--IT309045002 Bagnone
--IT107011008 Calice al Cornoviglio
--IT309045016 Villafranca in Lunigiana pour Bagnone
-- IT309046024 Pietrasanta
-- IT309046030 Stazzema
-- IT309046013 Forte dei Marmi
-- IT309046028 Seravezza
-- IT309046003 barga
UPDATE LAU_europe SET id_sup = 4973, id_hgis = 3 WHERE comm_id IN (
'IT309045002', 'IT107011008', 'IT309045016', 'IT309046024', 'IT309046030', 'IT309046013', 'IT309046028', 'IT309046003') 

-- Genes
--IT107011011 Castelnuovo Magra
--IT107011020 Ortonovo
UPDATE LAU_europe SET id_sup = 1345, id_hgis = 4 WHERE comm_id IN (
'IT107011011', 'IT107011020') 

--- Monaco
--FR93062150 La Turbie
--FR93062067 Gorbio
--IT107008065 Ventimiglia
--FR93062012 Beausoleil
--Sainte-Agnès : ok
--Roquebrune : ok
--Monti : pas vu (dans Ventimiglia ?)
--Garavan : pas vu (dans Ventimiglia ?)
--Castellar : ok
UPDATE LAU_europe SET id_sup = 9347, id_hgis = 1, methodg = 6, methodnum=6 WHERE comm_id IN (
'FR93062150', 'FR93062067', 'IT107008065', 'FR93062012') 


-- Mantou/venice (2490) /papal states (2140)
--IT208038002 Berra
--IT205029039 Porto Tole
--IT205029046 Taglio di po
--IT205029002 Ariano nel Polesine
--IT205029017 Corbola
UPDATE LAU_europe SET id_sup = 2490, id_hgis = 120, methodg = 6, methodnum=6 WHERE comm_id IN (
'IT208038002', 'IT205029039', 'IT205029046', 'IT205029002', 'IT205029017') 

-- Venice (2490) / Autriche (5689)
UPDATE LAU_europe SET id_sup = 2490, id_hgis = 120, methodg = 6, methodnum=6 WHERE comm_id IN (
'IT206031009', 'IT206031012', 'IT206031023', 'IT206030134', 'IT206030056') 
--IT206031009 Grado e Venice (2490)
--IT206031012 Monfalcone e Venice (2490)
--IT206031023 Staranzano e Venice (2490)
--IT206030134 Villa Vicentina e Venice (2490)
--IT206030056 Marano Lagunare e Venice (2490)

UPDATE LAU_europe SET id_sup = 5689, id_hgis = 77, methodg = 6, methodnum=6 WHERE comm_id IN (
'IT206030004', 'IT206030120', 'IT206030018',  'IT206030082') 
--IT206030004 Aquileia e Autriche (5689)
--IT206030120 Terzo d'Aquileia e Autriche (5689)
--IT206030018 Carlino e Autriche (5689)
---- IT206030069 Palazzolo dello Stella
--IT206030082 Precenicco e Autriche (5689)


-- Oldenburg num 111 n'appartient pas au Danemark (892), il est indépendant 
UPDATE LAU_europe SET id_sup = 111, methodg = 6, methodnum=6  where id_sup = 892 and id_hgis = 111 and cntr_code  = 'DE'
select * from LAU_europe where id_sup = 892 and id_hgis = 111 and cntr_code  = 'DE'

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 111
group by id_sup


-- Swhleswig-Holstein e Danemark
UPDATE LAU_europe SET id_sup = 892, methodg = 6, methodnum=6  where id_sup = 6788 and id_hgis in (17, 18) 
-- and cntr_code  = 'DE'
-- 299
UPDATE LAU_europe SET id_sup = 7708, methodg = 6, methodnum=6  where id_sup = 8246 and id_hgis = 165 and cntr_code  = 'DE'

select * from lau_europe le where id_hgis = 6788
select * from lau_europe le where id_sup = 6788

select * from world_1789 w2 where id = 43
delete from world_1789 w2 where id = 43

-- bengale : 1765 e Angleterre(1423) depuis 1765
-- https://fr.wikipedia.org/wiki/Inde#P%C3%A9riode_coloniale

-- Vietnam 
-- https://fr.wikipedia.org/wiki/Vi%C3%AAt_Nam#Histoire

-- Pondichéry : francais (1225) au XVIII
-- https://fr.wikipedia.org/wiki/Pondich%C3%A9ry#Colonisation_fran%C3%A7aise_en_Inde

-- Amérique du sud
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)

-- delete from ports.world_1789 where unit_code = 'Amerique-du-Sud'
-- delete from ports.world_1789 where unit_code ='Sud des USA (Mexique et Texas)'

-- rajouter les etats du sud des états unis (Californie, Nevada, Utah, Arizona, ) au Mexique (100007) 
-- Nouveau mexique et Texax)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 1 as unitlevel, 100007, name, 'usa', 'usa',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'CA', 'NV', 'UT', 'AZ') 

--insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
--select 100007, 0 as unitlevel, -1, name, 'world borders', 'world borders', st_union(geom3857 )
--from world_borders_dump e where id= 120 group by name

--- Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 1 as unitlevel, 100007, name, 'world borders', 'world borders', st_union(geom3857 )
from world_borders_dump e where id= 120 group by name

-- Etendre les limites du MExique
insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 'MX', 0, -1, 'Mexique', 'world borders + usa', 'world borders + usa', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100007 and unitlevel=1
-- delete from ports.world_1789  where id = 1312

-- Texas indépendant avec Nouveau Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 1 as unitlevel, 100008, name, 'usa', 'usa',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'TX', 'NM') 

insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100008, 'TX', 0, -1, 'Texas', 'usa', 'usa', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100008 and unitlevel=1




/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1122, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('LA');
*/
-- Rajouter Hawai
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Hawai', 0 as unitlevel, -1, 'usa', 'usa', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where pkid in (3414, 3412, 3408, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396)

update ports.world_1789 set unit_id = 100010, shortname='Hawaï', sourcegeom='world borders', sourcecode='pkid' 
where  unit_code = 'Hawai';

-- Brésil e Portugal
-- https://fr.wikipedia.org/wiki/Br%C3%A9sil#Ind%C3%A9pendance_%C3%A0_l'%C3%A9gard_du_Portugal_et_empire
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'BR', 1 as unitlevel, 3852, 'Brésil - world borders', ' Brésil - world borders', st_union(geom3857)
from world_borders_dump e where id = 21*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'BR', 'BR'||id::text , 6, 6, 3852, st_union(geom3857), 'world borders', 'BR', 'Brésil'  
from world_borders_dump e where id = 21 group by name, id

-- Etendre le Portugal avec le Portugal
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 3852 or (unit_code in ('BR') and unit_sup_id = 3852) 
) as k where unit_id = 3852


--- Afrique

-- Distinguer Maroc (indépendant), Algérie, Tunisie, Egypte du reste e Ottoman (6084)
-- 141 Réunion, 170 Mayotte, ile Maurice
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814
-- delete from ports.world_1789 where unit_code = 'Afrique'

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'MA', 0 as unitlevel, -1, 'Marocco - world borders', 'Marocco - world borders',  st_union(geom3857)
from world_borders_dump e where id = 114

update ports.world_1789 set unit_id = 100006 where unit_code = 'MA' and unitlevel = 0

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'DZ', 'DZ'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'DZ', 'Algérie'  
from world_borders_dump e where id = 2 group by name, id

/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'DZ', 1 as unitlevel, 6084, 'Algérie - world borders', 'Algérie - world borders',  st_union(geom3857)
from world_borders_dump e where id = 2*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'TN', 'TN'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'TN', 'Tunisie'  
from world_borders_dump e where id = 201 group by name, id

/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'TN', 1 as unitlevel, 6084, 'Tunisie - world borders', 'Tunisie - world borders',  st_union(geom3857)
from world_borders_dump e where id = 201
*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'LY', 'LY'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'LY', 'Tripoli'  
from world_borders_dump e where id = 107 group by name, id

/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'LY', 1 as unitlevel, 6084, 'Lybie - world borders', 'Libyan Arab Jamahiriya - world borders',  st_union(geom3857)
from world_borders_dump e where id = 107
*/
/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'EG', 1 as unitlevel, 6084, 'Egypte - world borders', 'Egypt - world borders',  st_union(geom3857)
from world_borders_dump e where id = 50
*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'EG', 'EG'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'EG', 'Egypte'  
from world_borders_dump e where id = 50 group by name, id

-- Etendre l'empire Ottoman avec le maghreb
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 6084 or (unit_code in ('DZ', 'TN', 'LY', 'EG') and unit_sup_id = 6084) 
) as k where unit_id = 6084

-- Le reste Asie + Moyen-Orient 145 + Océanie 54, 53 + Taiwan (0) / retirer la Turquie (202)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 0 as unitlevel, -1, 'Asie + Moyen-Orient + Océanie - world borders', 'Asie + Moyen-Orient + Océanie  - world borders',  st_unaryunion(st_collect(geom3857))
from world_borders_dump e where subregion in (145, 54, 53, 30, 34, 35, 143, 0) and id<>202

-- delete from ports.world_1789  where unit_code = 'WWW'


-----------------------------
-- Fabriquer les frontières Européennes et du monde
-----------------------------



-- drop  table ports.world_1789 
create table ports.world_1789 
(
id serial,
unit text,
unit_id int,
unit_code text,
unitlevel int, --0 pour etat, 1 pour sous-unité
unit_sup_id int,
unit_sup_code text,
sourcegeom text,
sourcecode text,
geom3857 geometry
)


insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e
group by id_sup
-- 89 (pas 92)

-- mettre à jour en Danemark (892), GB (1423), ES (1122), Piemont(4705) , genes 1345

update world_1789 set geom3857 = k.geom
from (select ST_union(e.wkb_geometry) as geom
from LAU_europe e where id_sup = 1225
group by id_sup) as k where unit_id = 1225

delete from ports.world_1789  where unitlevel = 1
-- 296

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null
group by id_sup, id_hgis
-- 667 (pas 275), 2min 21


-- Gerer id_hgis si null
select * from LAU_europe e where id_hgis =0
-- une commune en Islande
update LAU_europe set id_hgis = 0 where ogc_fid = 89011
update LAU_europe set province_name =  'Acores', province_code = 'PT200' where nuts_code = 'PT200';
update LAU_europe set province_name =  'Madere', province_code = 'PT300' where nuts_code = 'PT300';

--update LAU_europe e set id_hgis = null, province_name = 'Island', province_code = 'IS' where cntr_id = 'IS' and id_hgis = 0;
--update LAU_europe e set id_hgis = null, province_name = 'Faroe Islands', province_code = 'FO' where cntr_id = 'FO' and id_hgis = 0;
--update LAU_europe e set id_hgis = null, province_name = 'Greenland', province_code = 'GL' where cntr_id = 'GL' and id_hgis = 0;

select * from LAU_europe e where id_hgis is null
select * from LAU_europe where id_hgis is null and methodnum is null and nuts_code not in ('PT200', 'FRY10', 'FRY20', 'FRY30', 'FRY40', 'FRY50')

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e where province_code is not null
group by id_sup, province_code, province_name
-- 21, 53 s

select * from ports.world_1789 where unitlevel = 1 and unit_code is not null 
delete from ports.world_1789 where unitlevel = 1 and unit_code is not null 
update LAU_europe set province_name = 'Pays-Bas autrichiens' where province_code = 'AU03'
-- 834
-- https://fr.wikipedia.org/wiki/Saint-Marin#Histoire

-- Brême
update LAU_europe SET id_sup = 100003, id_hgis = NULL, province_code='BR', province_name='Brême'
WHERE comm_id = 'DE040011000001' 

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'commune de Brême - group by province_code', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and comm_id = 'DE040011000001'
group by province_code, province_name

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 0 as unitlevel, 'LAU_europe', 'commune de Brême', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and comm_id = 'DE040011000001'
group by province_code, province_name

select * from LAU_europe where comm_id = 'DE040011000001'

delete from ports.world_1789 where unit_id = 8246
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 8246
group by id_sup

delete from ports.world_1789 where unit_id = 165 AND unitlevel = 1 AND id= 281

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Hambourg - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_hgis = 165 and id_sup = 8246
group by id_hgis


insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_collect(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and comm_id = 'DE040011000001'
group by province_code, province_name

select * from ports.world_1789 where unit_code = 'BR'
delete from ports.world_1789 where id = 384
delete from ports.world_1789 where unit_code = 'BR'

-- Hamburg
update LAU_europe SET id_sup = 15024
WHERE comm_id = 'DE020001000001' 


delete from ports.world_1789 where unit_id = 15024
delete from ports.world_1789 where unit_id = 892

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'commune de Hambourg - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 15024
group by id_sup

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Hambourg - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 15024
group by id_sup


insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 892
group by id_sup

-- Gdansk
delete from ports.world_1789  where unit_id = 2750 and unitlevel = 0
delete from ports.world_1789  where unit_sup_id = 2750 and unitlevel = 1

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup = 2750
group by id_sup
-- 1

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'commune de Gdansk - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup = 100004
group by id_sup

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.id_sup = 2750
group by id_sup, id_hgis

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Gdansk - group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.id_sup = 100004
group by id_sup, id_hgis

-- lampedouse pas dans naples (100000)
-- IT519084020 - Lampedusa e Linosa
-- 100005
update LAU_europe e set id_hgis = 100005, id_sup = 100005 where id_sup = 100000 and comm_id = 'IT519084020';
delete from ports.world_1789 where unitlevel = 0 and unit_id = 100000;

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (100000, 100005)
group by id_sup;

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Lampedusa - group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.id_sup = 100005
group by id_sup, id_hgis;
insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_collect(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and e.id_sup = 100000
group by province_code, province_name;

-- Ost Friesland pas dans les Provinces Unies 4117
update LAU_europe e set id_hgis = 100009, id_sup = 100009 where id_sup = 4117 and id_hgis = 5 and cntr_code = 'DE';
delete from ports.world_1789 where unitlevel = 0 and unit_id = 4117;
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (100009)
group by id_sup;
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (4117)
group by id_sup;

-- Ost Friesland appartient à la Prusse ? OUI
-- https://fr.wikipedia.org/wiki/Frise_orientale, oui depuis 1744
update LAU_europe e set id_sup = 5141, province_name='Frise orientale', province_code='FRO' where id_hgis = 100009
delete from ports.world_1789 where unitlevel = 0 and unit_id in (5141, 100009);
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (5141)
group by id_sup;

--- USA
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select 100001 as unit_id, 0 as unitlevel, 'usa', 'usa', st_setsrid(st_transform(ST_unaryunion(st_collect(e.wkb_geometry)), 3857), 3857) as geom3857
from usa e where  stusps in ('NC', 'SC', 'GA',  'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT')

-- not in ('PR', 'LA')

select 100001 as unit_id, 0 as unitlevel, 'usa', 'usa', st_setsrid(st_transform(st_union(e.wkb_geometry), 3857), 3857) as geom3857
from usa e where  stusps not in ('PR', 'LA')

delete from ports.world_1789 where unit_id = 100001
delete from ports.world_1789 where unit_code = 'others states in usa'
delete from ports.world_1789 where unit_sup_id = 100001

--insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id, sourcegeom, sourcecode, geom3857)
--select 'others states in usa' , 1 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(ST_unaryunion(st_collect(e.wkb_geometry)), 3857), 3857) as geom3857
--from usa e where stusps not in ('PR', 'LA', 'NC', 'SC', 'GA', 'LA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA')

-- Les états de la côte Est et les autres fondateurs
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('NC', 'SC', 'GA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT')

-- La Louisiane et la Floride sont espagnoles
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1122, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('LA');

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1122, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('FL');*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 1122, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', 'LA', name  
from usa e where stusps  in ('LA');
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 1122, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', 'FL', name  
from usa e where stusps  in ('FL');

-- L'Alaska est Russe
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 2344, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('AK');*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 2344, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', 'AK', name  
from usa e where stusps  in ('AK');

-- à L'ouest, beaucoup d'états anglais
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1423, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR');
*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 1423, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', stusps, name  
from usa e where stusps  in ('WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR');

update LAU_europe set wkb_geometry = st_multi(st_buffer(wkb_geometry, 3000)) where province_code in ('WA', 'ID', 'MT', 'ND') and id_sup = 1423

-- Cuba e espagne
-- https://fr.wikipedia.org/wiki/Cuba#Histoire_coloniale
/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'CU', 1 as unitlevel, 1122, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=41 and name = 'Cuba'  ;*/


insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'CU', 'CU'||id::text , 6, 6, 1122, st_union(geom3857), 'world borders', 'CU', 'Cuba'  
from world_borders_dump e where id = 41 and name = 'Cuba' group by id, name


-- Etendre l'Espagne avec la Louisiane, Cuba, La Floride
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1122 or (unit_code in ('LA', 'CU', 'FL') and unit_sup_id = 1122) 
) as k where unit_id = 1122

-- Etendre la Russie avec l'Alaska
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 2344 or (unit_code in ('AK') and unit_sup_id = 2344) 
) as k where unit_id = 2344

-- Etendre l'Angleterre avec les états de l'Ouest
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1423 or (unit_sup_id = 1423 and 
unit_code in ('WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR')) 
) as k where unit_id = 1423

update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1423 or (unit_sup_id = 1423 and 
unit_code in ('IA')) 
) as k where unit_id = 1423

-- Jamaïque e angleterre
-- https://fr.wikipedia.org/wiki/Jama%C3%AFque#Histoire
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'JM', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=90 and name = 'Jamaica'  */

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'JM', 'JM'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'JM', 'Jamaïque'  
from world_borders_dump e where id = 90 and name = 'Jamaica' group by id, name


-- https://fr.wikipedia.org/wiki/Bahamas
-- Bahamas e angleterre
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'BS', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=13 and name = 'Bahamas'  */

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'BS', 'BS'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'BS', 'Bahamas'  
from world_borders_dump e where id = 13 and name = 'Bahamas' group by id, name


-- Ile Caiman & Turks e angleterre
-- https://fr.wikipedia.org/wiki/%C3%8Eles_Turques-et-Ca%C3%AFques
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'TC', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=236 and name = 'Turks and Caicos Islands'  */

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'TC', 'TC'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'TC', 'Turks and Caicos Islands'  
from world_borders_dump e where id = 236 and name = 'Turks and Caicos Islands' group by id, name


-- Cayman Islands e angleterre depuis 1670
-- https://fr.wikipedia.org/wiki/%C3%8Eles_Ca%C3%AFmans
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'KY', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=34 and name = 'Cayman Islands'*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'KY', 'KY'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'KY', 'Îles Caïman'  
from world_borders_dump e where id = 34 and name = 'Cayman Islands' group by id, name


-- Etendre l'angleterre avec ces provinces 
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1423 or (unit_code in ('JM', 'BS', 'TC', 'KY') and unit_sup_id = 1423) 
) as k where unit_id = 1423

-- https://fr.wikipedia.org/wiki/Louisiane#Colonisation_europ%C3%A9enne
select distinct etat, dfrom, dto, subunit, subunit_en from etats e2 where etat = 'Etats-Unis d''Amérique'
-- pas la Louisiane LA
select *, etat, dfrom, dto, subunit from etats e2 where subunit like 'Lousiana'
update  etats set subunit = 'Louisiane', subunit_en = 'Louisiana'  where subunit like 'Lousiana'

-- Cap vert e Portugal
-- https://fr.wikipedia.org/wiki/Cap-Vert#Du_XVe_%C3%A0_la_fin_du_XVIIIe_si%C3%A8cle
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'CV', 'CV'||id::text , 6, 6, 3852, st_multi(st_union(geom3857)), 'world borders', 'CV', 'Cap Vert'  
from world_borders_dump e where id = 42 and name = 'Cape Verde' group by id, name


-- Dominica e France
-- https://fr.wikipedia.org/wiki/Dominique_(pays)#Histoire
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'DM', 'DM'||id::text , 6, 6, 1225, st_multi(st_union(geom3857)), 'world borders', 'DM', 'Dominique'  
from world_borders_dump e where id = 47 and name = 'Dominica' group by id, name

-- Montserrat e Angleterre
-- https://fr.wikipedia.org/wiki/Montserrat_(Antilles)#Colonisation_europ%C3%A9enne
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'MS', 'MS'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'MS', 'Montserrat'  
from world_borders_dump e where id = 111 and name = 'Montserrat' group by id, name


insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'VG', 'VG'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'VG', 'British Virgin Islands'  
from world_borders_dump e where id = 215 and name = 'British Virgin Islands' group by id, name

-- Anguilla e Angleterre
-- https://fr.wikipedia.org/wiki/Anguilla#Colonisation_britannique
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'AI', 'AI'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'AI', 'Anguilla'  
from world_borders_dump e where id = 128 and name = 'Anguilla' group by id, name


-----------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------

-- créer les noms des provinces, et des pays
alter table ports.world_1789 add shortname text;
alter table ports.world_1789 add longname text;

update ports.world_1789 w set shortname = g.short_name , longname = g.long_name 
from gis_1700_division2 g
where g.num = w.unit_id and g.owner_id = w.unit_sup_id and w.unitlevel = 2 and shortname is null
-- 526

update ports.world_1789 w set shortname = e.province_name , longname = e.province_name 
from lau_europe e where w.unit_code = e.province_code and w.unitlevel = 2 and shortname is null
-- 22
UPDATE ports.world_1789 w set unit_id = 100003 where w.unit_code = 'BR' and w.unitlevel = 0;

UPDATE ports.world_1789 w set shortname = 'Autriche' where w.unit_id=5689 ;
UPDATE ports.world_1789 w set shortname = 'Brême' where w.unit_id = 100003 ;
UPDATE ports.world_1789 w set shortname = 'Dantzig' where w.unit_id = 100004 ;
UPDATE ports.world_1789 w set shortname = 'Hambourg' where w.unit_id=15024 ;
UPDATE ports.world_1789 w set shortname = 'Lubeck' where w.unit_id=15011 ;
UPDATE ports.world_1789 w set shortname = 'Grande-Bretagne' where w.unit_id=1423 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'France' where w.unit_id=1225 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Espagne' where w.unit_id=1122 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Empire ottoman' where w.unit_id=6084 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Duché de Massa et Carrare' where w.unit_id=15058 ;
UPDATE ports.world_1789 w set shortname = 'Duché de Mecklenbourg' where w.unit_id=4918 ;
UPDATE ports.world_1789 w set shortname = 'Danemark' where w.unit_id=892 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Monaco' where w.unit_id=9347 ;
UPDATE ports.world_1789 w set shortname = 'Pologne' where w.unit_id=2750 ;
UPDATE ports.world_1789 w set shortname = 'Portugal' where w.unit_id=3852 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Prusse' where w.unit_id=5141 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Principauté de Piombino' where w.unit_id=8797 ;
UPDATE ports.world_1789 w set shortname = 'République de Gènes' where w.unit_id=1345 ;
UPDATE ports.world_1789 w set shortname = 'République de Raguse' where w.unit_id=3437 ;
UPDATE ports.world_1789 w set shortname = 'Etats pontificaux' where w.unit_id=2140  ;
UPDATE ports.world_1789 w set shortname = 'République de Venise' where w.unit_id=2490 ;
UPDATE ports.world_1789 w set shortname = 'Royaume de Naples' where w.unit_id=100000 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Royaume de Piémont-Sardaigne' where w.unit_id=4705 ;
UPDATE ports.world_1789 w set shortname = 'Russie' where w.unit_id=2344 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Suède' where w.unit_id=8246 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Toscane' where w.unit_id=4973 ;
UPDATE ports.world_1789 w set shortname = 'République de Lucques' where w.unit_id=4974;
UPDATE ports.world_1789 w set shortname = 'principauté de Lampédouse' where w.unit_id=100005 ;
UPDATE ports.world_1789 w set shortname = 'Provinces-Unies' where w.unit_id=4117 ;
UPDATE ports.world_1789 w set shortname = 'Etats-Unis d''Amérique' where w.unit_id=100001 ;
UPDATE ports.world_1789 w set shortname =  'Duché d''Oldenbourg'	where w.unit_id=111 ;
UPDATE ports.world_1789 w set shortname =  'Danemark'	where w.unit_id=892 and shortname is  null;
UPDATE ports.world_1789 w set shortname =  'Empire du Maroc'	where w.unit_id=100006 ;

UPDATE ports.world_1789 w set shortname = 'Malte' where w.unit_id=100009 ;
update ports.world_1789 w set shortname = 'reste Afrique' where unit_code='Afrique' ;
update ports.world_1789 w set shortname = 'reste Amérique-du-Sud' where unit_code='Amerique-du-Sud' ;
update ports.world_1789 w set shortname = 'reste Asie, Moyen-Orient et Océanie' where unit_code='WWW' ;
update ports.world_1789 w set shortname = 'Mexique' where w.unit_id=100007 ;
update ports.world_1789 w set shortname = 'Texas indépendant' where w.unit_id=100008 ;
update ports.world_1789 w set shortname = 'Hawaï' where w.unit_id=100010 ;
update ports.world_1789 w set shortname = 'Svalbard' where w.unit_id=100011 ;

select unit_id, g.short_name , g.long_name 
from ports.world_1789 w, gis_1700_sovereign_states g 
where w.unitlevel = 0 and g.owner_id = w.unit_id and shortname is null; 
-- 38

UPDATE ports.world_1789 w set shortname = g.short_name
from gis_1700_sovereign_states g 
where w.unitlevel = 0 and g.owner_id = w.unit_id and shortname is null; 
-- 62




UPDATE ports.world_1789 w set shortname = g.short_name
from gis_1700_sovereign_states g 
where w.unitlevel = 1 and g.owner_id = w.unit_id and shortname is null; 
-- 62

select * from ports.world_1789 w where shortname is not null and unitlevel = 1;
100007	Mexique

--------------------------------------------------------------------------------------------------------
SELECT * FROM etats WHERE etat = 'Royaume uni des Pays-Bas'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume de Hollande'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République de Sept-Îles'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume de Bonny'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République romaine'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République ligurienne'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume d''Étrurie'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume d''Italie'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République batave'

SELECT country2019_name , etat, subunit, dfrom, dto FROM etats WHERE etat = 'Duché d''Oldenbourg' -- dfrom 1773
SELECT country2019_name , etat, subunit, dfrom, dto FROM etats WHERE etat = 'Duché de Courlande' -- dto : 1795


------------------------------------------------------------------------------------------------------------

create table LAU_europe_backup as (select * from LAU_europe);
create table world_1789_backup as (select * from world_1789);

drop table world_1789_backup ;
drop table lau_europe_backup ;


drop table world_1789 

CREATE TABLE ports.world_1789 (
	id serial NOT NULL,
	unit_id int4 NULL, -- either this one is filled
	unit_code text NULL, -- either this one is filled
	unitlevel int4 NULL, -- 0 for state,  1 for substate, 2 for province
	unit_sup_id int4 NULL, -- -1 if state, else the unit_id of the belonging state
	sourcegeom text NULL,
	sourcecode text NULL,
	geom3857 geometry NULL,
	shortname text NULL, -- name of the state, substate or province, as given by Silvia, in French, or from other sources in English
	longname text null
);

select id, shortname, unit, unit_id, unit_code, unitlevel, unit_sup_id, unit_sup_code, sourcegeom,sourcecode 
from world_1789 w2 where unitlevel = 0 order by unit_id 
 
/*
select id, shortname, unit, unit_id, unit_code, unitlevel, unit_sup_id, unit_sup_code, sourcegeom,sourcecode 
from world_1789 w2 where unitlevel = 1 order by unit_sup_id 
*/



/*update world_1789 set geom3857 = k.geom
from (select ST_union(e.wkb_geometry) as geom
from LAU_europe e where id_sup = 1225
group by id_sup) as k where unit_id = 1225*/


select count(*) from LAU_europe where  province_code is not null
select count(*) from LAU_europe where  province_code is  null and id_hgis is null
select * from LAU_europe where  province_code is  null and id_hgis is null order by name_latn 



insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 2 as unitlevel, 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.province_code is null
group by id_sup, id_hgis;
-- 560


insert into ports.world_1789 (unit_code, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select province_code as unit_code, 2 as unitlevel, 'group by province_code', st_union(wkb_geometry), e.id_sup, province_name
from LAU_europe e WHERE  e.province_code is not null
group by id_sup, province_name, province_code 
-- 79

insert into ports.world_1789 (unit_code, unitlevel, sourcecode, geom3857, unit_sup_id)
select id_sup as unit_id, 2 as unitlevel, 'group by id_sup', st_union(wkb_geometry), e.id_sup
from LAU_europe e WHERE  province_code is  null and id_hgis is null
group by id_sup
-- 5

-- select * from ports.world_1789 where unit_sup_id = 5689

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id)
select e.unit_sup_id as unit_id, 1 as unitlevel, 'pays sans sous-etats, group by id_sup', ST_union(e.geom3857) as geom3857, e.unit_sup_id 
from ports.world_1789 e 
WHERE unit_sup_id in (100003, 100004, 111, 15058, 4918,2140, 15024, 15011, 4918, 9347, 2750, 
100005, 8797, 4974, 3437, 2490, 5708, 4973)
group by unit_sup_id;
-- 17

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id)
select e.unit_sup_id as unit_id, 1 as unitlevel, 'group by id_sup and unit_code is null', ST_union(e.geom3857) as geom3857, e.unit_sup_id 
from ports.world_1789 e 
WHERE  unit_code is null
and unit_sup_id not in (100003, 100004, 111, 15058, 4918,2140, 15024, 15011, 4918, 9347, 2750, 100005, 8797, 4974, 3437, 2490, 5708, 4973)
group by unit_sup_id
-- 74

-- cas particuliers
insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200000, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , e.shortname 
from ports.world_1789 e 
WHERE  unit_code = 'AU03' and unit_sup_id = 5689
group by unit_sup_id, unit_code, shortname;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200001, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'colonies danoises' 
from ports.world_1789 e 
WHERE  unit_code in ('IS', 'FO', 'GL', 'VI') and unit_sup_id = 892
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200002, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Norvège' 
from ports.world_1789 e 
WHERE  unit_code in ('NO') and unit_sup_id = 892
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200003, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Régence d''Alger' 
from ports.world_1789 e 
WHERE  unit_code in ('DZ') and unit_sup_id = 6084
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200004, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Régence de Tripoli' 
from ports.world_1789 e 
WHERE  unit_code in ('LY', 'EG') and unit_sup_id = 6084
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200005, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Régence de Tunis' 
from ports.world_1789 e 
WHERE  unit_code in ('TN') and unit_sup_id = 6084
group by unit_sup_id;

-- étendre l'empire ottoman avec la Bosnie et le monténégro
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 6084 or (unit_code =  '6084' and unit_sup_id = 6084) 
) as k where unit_id = 6084 and unitlevel = 1

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200006, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Canaries' 
from ports.world_1789 e 
WHERE  unit_code in ('ES7XX') and unit_sup_id = 1122
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200007, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies espagnoles d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('CU', 'LA', 'FL', 'FK', 'DO', 'PR', 'TT') and unit_sup_id = 1122
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200008, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies françaises d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('PM', 'FRY10', 'FRY20', 'FRY30', 'LC', 'TO', 'DM', 'HT') and unit_sup_id = 1225
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200009, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies françaises en Asie' 
from ports.world_1789 e 
WHERE  unit_code in ('MU', 'FRY40', 'FRY50') and unit_sup_id = 1225
group by unit_sup_id;

-- Etendre la France
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 1225 or (unit_code in ('FRL') and unit_sup_id = 1225) 
) as k where unit_id = 1225 and unitlevel = 1 


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200010, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies britanniques d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('TN', 'CA',  'GD', 'AG', 'KN', 'BB', 'VC', 'JM', 'BS', 'TC', 'KY', 'MS', 'AI', 'VG',
'WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200011, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Ecosse' 
from ports.world_1789 e 
WHERE  unit_code in ('UK02')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200012, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Iles anglo-normandes' 
from ports.world_1789 e 
WHERE  unit_code in ('JE-GG')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200013, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Irlande' 
from ports.world_1789 e 
WHERE  unit_code in ('IE')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200014, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Pays de Galles' 
from ports.world_1789 e 
WHERE  unit_code in ('WL')
and unit_sup_id = 1423
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200015, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies portugaises d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('BR')
and unit_sup_id = 3852
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200016, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'îles atlantiques portugaises' 
from ports.world_1789 e 
WHERE  unit_code in ('PT200', 'CV', 'PT300')
and unit_sup_id = 3852
group by unit_sup_id;

delete from ports.world_1789 where unit_id = 4117 and unitlevel = 1
-- select * from ports.world_1789 where unit_id = 4117 and unitlevel = 1

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200017, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Frise' 
from ports.world_1789 e 
WHERE  unit_id = 18 and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200018, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Groningue' 
from ports.world_1789 e 
WHERE  unit_id = 8 and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200019, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Gueldre' 
from ports.world_1789 e 
WHERE  unit_id in (56) and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200032, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Overijssel' 
from ports.world_1789 e 
WHERE  unit_id in (79) and unit_sup_id = 4117
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200020, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Hollande' 
from ports.world_1789 e 
WHERE  unit_id in (1,60) and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200021, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Pays de la Généralité' 
from ports.world_1789 e 
WHERE  unit_id in (72) and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200022, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Zélande' 
from ports.world_1789 e 
WHERE  unit_id in (75) and unit_sup_id = 4117
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200023, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Frise orientale' 
from ports.world_1789 e 
WHERE  unit_code in ('FRO') and unit_sup_id = 5141
group by unit_sup_id;


-- étendre la prusse
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 5141 or (unit_code =  '5141' and unit_sup_id = 5141) 
) as k where unit_id = 5141 and unitlevel = 1


-- étendre la Russie
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 2344 or (unit_code =  '2344' and unit_sup_id = 2344) 
) as k where unit_id = 2344 and unitlevel = 1

update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 2344 or (unit_code =  'AK' and unit_sup_id = 2344) 
) as k where unit_id = 2344 and unitlevel = 1


select * from ports.world_1789 where unit_code = 'FRM';

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200024, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Corse' 
from ports.world_1789 e 
WHERE  unit_code in ('FRM') and unit_sup_id = 1225
group by unit_sup_id;


select *, st_area(geom3857) from ports.world_1789 where unit_code like 'NA%'
select * from lau_europe le where province_code = 'NA02' and id_sup = 100005
update lau_europe set province_code = null, province_name = null where province_code = 'NA02' and id_sup = 100005
--delete from ports.world_1789 where id = 636
select *, st_area(geom3857) from ports.world_1789 where unit_id = 100005

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200025, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Etat des Présides' 
from ports.world_1789 e 
WHERE  unit_code in ('NA03') and unit_sup_id = 100000
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200026, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Sicile' 
from ports.world_1789 e 
WHERE  unit_code in ('NA02') and unit_sup_id = 100000
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200027, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Naples' 
from ports.world_1789 e 
WHERE  unit_code in ('NA01') and unit_sup_id = 100000
group by unit_sup_id;



insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200028, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Sardaigne' 
from ports.world_1789 e 
WHERE  unit_code in ('SA02') and unit_sup_id = 4705
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200029, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Finlande' 
from ports.world_1789 e 
WHERE  unit_code in ('FI') and unit_sup_id = 8246
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200030, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Poméranie suédoise' 
from ports.world_1789 e 
WHERE  unit_code in ('POMS') and unit_sup_id = 8246
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200031, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonie suèdoise en Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('BL97701') and unit_sup_id = 8246
group by unit_sup_id;

-------------------------
-- rajouter les pays restant dans le monde


-- delete from ports.world_1789 where unit_code = 'Amerique-du-Sud'
-- delete from ports.world_1789 where unit_code ='Sud des USA (Mexique et Texas)'

-- rajouter les etats du sud des états unis (Californie, Nevada, Utah, Arizona, ) au Mexique (100007) 
-- Nouveau mexique et Texax)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 2 as unitlevel, 100007, name, 'usa', '(Californie, Nevada, Utah, Arizona)',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'CA', 'NV', 'UT', 'AZ') 

--insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
--select 100007, 0 as unitlevel, -1, name, 'world borders', 'world borders', st_union(geom3857 )
--from world_borders_dump e where id= 120 group by name

--- Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 2 as unitlevel, 100007, name, 'world borders', 'id = 120', st_union(geom3857 )
from world_borders_dump e where id= 120 group by name

-- Etendre les limites du MExique
insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 'MX', 1, 100007, 'Mexique', 'world borders + usa', 'Mexique + (Californie, Nevada, Utah, Arizona)', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100007 and unitlevel=2
-- delete from ports.world_1789  where id = 1312

-- Texas indépendant avec Nouveau Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 2 as unitlevel, 100008, name, 'usa', 'usa',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'TX', 'NM') 

insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100008, 'TX', 1, 100008, 'Texas', 'usa', 'usa', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100008 and unitlevel=2


-- Rajouter Hawai
insert into ports.world_1789 (shortname, unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Hawaï', 100010, 'Hawai', 2 as unitlevel, 100010, 'world borders', 'pkid', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where pkid in (3414, 3412, 3408, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396)

insert into ports.world_1789 (shortname, unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Hawaï', 100010, 'Hawai', 1 as unitlevel, 100010, 'world borders', 'pkid', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where pkid in (3414, 3412, 3408, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396)

-- Maroc
insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 100006, 'MA', 2 as unitlevel, 100006, 'world borders', 'Marocco, id 114',  st_union(geom3857)
from world_borders_dump e where id = 114

insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 100006, 'MA', 1 as unitlevel, 100006, 'world borders', 'Marocco, id 114',  st_union(geom3857)
from world_borders_dump e where id = 114


-- Les états de la côte Est et les autres fondateurs
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('NC', 'SC', 'GA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT');

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 2 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('NC', 'SC', 'GA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT');


-- Amérique du sud
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 1 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 2 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)


--- Afrique

-- Distinguer Maroc (indépendant), Algérie, Tunisie, Egypte du reste e Ottoman (6084)
-- 141 Réunion, 170 Mayotte, ile Maurice
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 1 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 2 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814

-- delete from ports.world_1789 where unit_code = 'Afrique'

-- Le reste Asie + Moyen-Orient 145 + Océanie 54, 53 + Taiwan (0) / retirer la Turquie (202)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 0 as unitlevel, -1, 'Asie + Moyen-Orient + Océanie - world borders', 'Asie + Moyen-Orient + Océanie  - world borders',  st_unaryunion(st_collect(geom3857))
from world_borders_dump e where subregion in (145, 54, 53, 30, 34, 35, 143, 0) and id<>202

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 1 , -1, sourcegeom, sourcecode, geom3857 from ports.world_1789 where unit_code='WWW' and unitlevel=0

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 2 , -1, sourcegeom, sourcecode, geom3857 from ports.world_1789 where unit_code='WWW' and unitlevel=0

-- Svalbard (SJ) ?
insert into ports.world_1789 (unit_id, unit_sup_id, shortname, unit_code, unitlevel,  sourcegeom, sourcecode, geom3857)
select 100011, 100011, 'Svalbard', 'SJ', 2 as unitlevel, 'world borders', 'world borders', st_unaryunion(st_collect(geom3857))
from world_borders_dump e where  id = 240

insert into ports.world_1789 (unit_id, unit_sup_id, shortname, unit_code, unitlevel,  sourcegeom, sourcecode, geom3857)
select 100011, 100011, 'Svalbard', 'SJ', 1 as unitlevel, 'world borders', 'world borders', st_unaryunion(st_collect(geom3857))
from world_borders_dump e where  id = 240


-- level 0 : les états
insert into ports.world_1789 (unit_id, unit_sup_id, unitlevel,  sourcecode, geom3857)
select unit_sup_id as unit_id, -1, 0 as unitlevel, 'group by unit_sup_id', st_multi(ST_union(e.geom3857)) as geom3857 
from ports.world_1789 e 
WHERE unitlevel = 1 and unit_sup_id<> -1
-- and  (unit_code  in ('Afrique', 'WWW', 'Amerique-du-Sud')) 
group by unit_sup_id;


delete from ports.world_1789 where unit_id = 1225

insert into ports.world_1789 (unit_id, unit_sup_id, unitlevel,  sourcecode, geom3857)
select unit_sup_id as unit_id, 1225, 1 as unitlevel, 'group by unit_sup_id', st_multi(ST_union(e.geom3857)) as geom3857 
from ports.world_1789 e 
WHERE unitlevel = 2 and unit_sup_id= 1225 and ( unit_id is not null or unit_code in ('FRL', 'FRM'))
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unit_sup_id, unitlevel,  sourcecode, geom3857)
select unit_sup_id as unit_id, -1, 0 as unitlevel, 'group by unit_sup_id', st_multi(ST_union(e.geom3857)) as geom3857 
from ports.world_1789 e 
WHERE unitlevel = 1 and unit_sup_id= 1225 
group by unit_sup_id;


select * from ports.world_1789 WHERE  unitlevel = 2 and shortname = 'North Carolina'


SELECT  comm_id, comm_name, name_asci ,cntr_id , nuts_code , id_hgis as unit_id, id_sup, province_code , province_name, sourcegeom 
FROM lau_europe
order by id_sup, id_hgis, province_code

alter table lau_europe add column unit_id int ;
update lau_europe set unit_id = id_hgis 

-------------------------------------------------------------------------
-- 24 mars 2021
-------------------------------------------------------------------------
-- 24 mars 2021
-- Fin, et envoie des métadonnées et fichiers utiles 
-- à Robin De Mourat,  Loic Charles, Guillaume Daudin, Silvia Marzagalli, Cécile Asselin.
-------------------------------------------------------------------------

------------------
-- id / id_sup
------------------


ALTER TABLE ports.world_1789 ADD COLUMN id_sup int default -1;

update ports.world_1789 w1 set id_sup = w.id 
from ports.world_1789 w 
where w1.unitlevel = 1 and w.unitlevel = 0 and w.unit_id = w1.unit_sup_id 

update ports.world_1789 w set id_sup = -1 where w.unitlevel = 2 
update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w.unitlevel = 1 and w2.unit_sup_id = w.unit_id and w2.unit_code is null

select unitlevel, id, id_sup, shortname from ports.world_1789 where shortname = 'France'

select unitlevel, id, id_sup, shortname from ports.world_1789 where id_sup = 945
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 944
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 761
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 745
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 743

select unitlevel,id, id_sup, shortname from ports.world_1789 where unit_sup_id = 1225
select unitlevel,id, id_sup, shortname, * from ports.world_1789 where unit_sup_id = 1225 and unitlevel = 2

select shortname, unitlevel, id, id_sup, unit_code , unit_id, unit_sup_id from ports.world_1789 
where    id_sup=-1

select shortname, unitlevel, id, id_sup, unit_code , unit_id, unit_sup_id from ports.world_1789 
where  unit_code = '100009'
unitlevel = 1 and unit_sup_id = 100001

select * from ports.world_1789
where  unitlevel = 0 and unit_id = 100003
select * from ports.world_1789
where  unitlevel = 0 and id = 870

update ports.world_1789 w set id_sup = 671 where  id = 620 ; -- Alaska
update ports.world_1789 w set id_sup = 944 where  id in (579 ) ; -- Lorraine
update ports.world_1789 w set id_sup = 670 where  id in (596 ) ; -- Ile de Man
update ports.world_1789 w set id_sup = 833 where  id in (831, 832 ) ; -- Texas et Nouveau Mexique
update ports.world_1789 w set id_sup = 761 where  id in (578 ) ; -- Corse
update ports.world_1789 w set id_sup = 839 where  id in (834 ) ; -- Mexico
update ports.world_1789 w set id_sup = 825 where  id in (824 ) ; -- Maroc
update ports.world_1789 w set id_sup = 830 where  id in (829 ) ; -- Hawaï
update ports.world_1789 w set id_sup = 844 where  id in (843 ) ; -- Svalbard
-- Sans noms
update ports.world_1789 w set id_sup = 712 where  id in (642 ) ; -- Prusse
update ports.world_1789 w set id_sup = 657 where  id in (641 ) ; -- Pologne
update ports.world_1789 w set id_sup = 709 where  id in (643 ) ; -- Ottoman
update ports.world_1789 w set id_sup = 671 where  id in (640) ; -- Russie
update ports.world_1789 w set id_sup = 664, shortname = 'Malte' where  id in (644) ; -- Russie


update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code is not null
and w.unitlevel = 1 and w2.shortname = w.shortname and w.unit_sup_id = 100001;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code is not null
and w.unitlevel = 1 and w2.unit_code in ('AU03') and w.unit_id = 200000;

update ports.world_1789 w2 set unit_code = 'BRM' where shortname = 'Brême' and unit_code = 'BR'
update ports.lau_europe w2 set province_code = 'BRM' where province_name = 'Brême' and unit_code = 'BR'

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('BRM')
and w.unitlevel = 1 and w.unit_id = 100003 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('IS', 'FO', 'GL', 'VI')
and w.unitlevel = 1 and w.unit_id = 200001 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NO')
and w.unitlevel = 1 and w.unit_id = 200002 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('DZ')
and w.unitlevel = 1 and w.unit_id = 200003 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('LY', 'EG')
and w.unitlevel = 1 and w.unit_id = 200004 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('TN')
and w.unitlevel = 1 and w.unit_id = 200005 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('ES7XX')
and w.unitlevel = 1 and w.unit_id = 200006 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('CU', 'LA', 'FL', 'FK', 'DO', 'PR', 'TT')
and w.unitlevel = 1 and w.unit_id = 200007 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('PM', 'FRY10', 'FRY20', 'FRY30', 'LC', 'TO', 'DM', 'HT')
and w.unitlevel = 1 and w.unit_id = 200008 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('MU', 'FRY40', 'FRY50')
and w.unitlevel = 1 and w.unit_id = 200009 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code 
in ('TN', 'CA',  'GD', 'AG', 'KN', 'BB', 'VC', 'JM', 'BS', 'TC', 'KY', 'MS', 'AI', 'VG',
'WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR')
and w.unitlevel = 1 and w.unit_id = 200010 ;

-- attention, CAnada et CAlifornie
select * from ports.world_1789 w2  where w2.shortname = 'California'
select id, * from ports.world_1789  where unit_id = 100007
update ports.world_1789 set id_sup = 839 where shortname = 'California'

update ports.world_1789 set id_sup = 839 where shortname in ('Nevada', 'Utah', 'Arizona')

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('UK02')
and w.unitlevel = 1 and w.unit_id = 200011 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('JE-GG')
and w.unitlevel = 1 and w.unit_id = 200012 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('IE')
and w.unitlevel = 1 and w.unit_id = 200013 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('WL')
and w.unitlevel = 1 and w.unit_id = 200014 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('BR')
and w.unitlevel = 1 and w.unit_id = 200015 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('PT200', 'PT300', 'CV')
and w.unitlevel = 1 and w.unit_id = 200016 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('FRI')
and w.unitlevel = 1 and w.unit_id = 200017 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('GRO')
and w.unitlevel = 1 and w.unit_id = 200018 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('GUE')
and w.unitlevel = 1 and w.unit_id = 200019 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('HOL')
and w.unitlevel = 1 and w.unit_id = 200020 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('GEN')
and w.unitlevel = 1 and w.unit_id = 200021 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('ZEL')
and w.unitlevel = 1 and w.unit_id = 200022 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('OVR')
and w.unitlevel = 1 and w.unit_id = 200032 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('FRO')
and w.unitlevel = 1 and w.unit_id = 200023 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NA03')
and w.unitlevel = 1 and w.unit_id = 200025 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NA02')
and w.unitlevel = 1 and w.unit_id = 200026 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NA01')
and w.unitlevel = 1 and w.unit_id = 200027 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('SA02')
and w.unitlevel = 1 and w.unit_id = 200028 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('FI')
and w.unitlevel = 1 and w.unit_id = 200029 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('POMS')
and w.unitlevel = 1 and w.unit_id = 200030 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('BL97701')
and w.unitlevel = 1 and w.unit_id = 200031

select unitlevel, id, id_sup, shortname from ports.world_1789 where shortname = 'Grande-Bretagne'
select unitlevel, id, id_sup, shortname from ports.world_1789 where id_sup = 859
select unitlevel, id, id_sup, unit_id, unit_sup_id , shortname, * from ports.world_1789 where id_sup = 670

update lau_europe set province_code ='FRI', province_name = 'Frise' where id_hgis=18 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='GRO', province_name = 'Groningue' where id_hgis=8 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='GUE', province_name = 'Gueldre' where id_hgis=56 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='HOL', province_name = 'Hollande' where id_hgis in (1,60) and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='GEN', province_name = 'Pays de la Généralité' where id_hgis=72 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='ZEL', province_name = 'Zélande' where id_hgis=75 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='OVR', province_name = 'Overijssel' where id_hgis=79 and id_sup =4117 and cntr_code in ('NL', 'DE');

select * from ports.world_1789 where shortname = 'Groningue'
select * from ports.world_1789 where shortname = 'Maryland'

update ports.world_1789 set unit_code = 'FRI' where shortname in ('Frise', 'Friesland') ;
update ports.world_1789 set unit_code = 'GRO' where shortname in ('Groningue', 'Groningen');
update ports.world_1789 set unit_code = 'GUE' where shortname in ('Gueldre', 'Guelders');
update ports.world_1789 set unit_code = 'HOL' where shortname in ('Hollande', 'Utrecht', 'Holland');
update ports.world_1789 set unit_code = 'GEN' where shortname in ('Pays de la Généralité', 'Generality');
update ports.world_1789 set unit_code = 'ZEL' where shortname in ('Zélande', 'Zeeland');
update ports.world_1789 set unit_code = 'OVR' where shortname = 'Overijssel'

select * from ports.world_1789 where shortname = 'Overijssel'

select id, id_sup, unit_code, unit_id, unit_sup_id , unitlevel from ports.world_1789 where unitlevel = 2 and id_sup = -1


select * from lau_europe where id_hgis=18 and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis=8 and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis=56 and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (1, 60) and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (72) and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (75) and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (79) and id_sup =4117 and cntr_code in ('NL', 'DE')

select * from ports.world_1789 where unitlevel = 0 


select * from ports.world_1789 where unit_sup_id = 100009
select * from ports.world_1789 where id = 644
update ports.world_1789 set shortname = 'Malte' where id = 850 -- au lieu de Saint John
update ports.world_1789 set shortname = 'Malte' where id = 664 -- au lieu de Saint John

update ports.world_1789_23mars2021 set shortname = 'Malte' where id = 850


SELECT  comm_id, comm_name, name_asci ,cntr_id , nuts_code , unit_id as id_sup, province_code , province_name, sourcegeom 
FROM lau_europe
order by id_sup, unit_id, province_code

alter table lau_europe add column unit_id int ; -- id de l'unité d'appartenance dans la table world_1789

update lau_europe l set unit_id = w.id
from ports.world_1789 w where unitlevel = 2 and w.unit_code = province_code and w.unit_sup_id = l.id_sup 
and  province_code is not null
-- 12 020
update lau_europe l set unit_id = w.id
from ports.world_1789 w where unitlevel = 2 and w.unit_id = id_hgis and w.unit_sup_id = l.id_sup 
and  province_code is  null
-- 111 743


select * from world_1789 w2 where unitlevel = 2 and unit_code = '2344'
update lau_europe l set unit_id = 640 where unit_id is null and id_sup = 2344


update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '2750') where unit_id is null and id_sup = 2750;
update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '6084') where unit_id is null and id_sup = 6084;
update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '5141') where unit_id is null and id_sup = 5141;
update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '100009') where unit_id is null and id_sup = 100009;

select * from lau_europe where unit_id is null

select distinct w.id, w.shortname , w1.id, w1.shortname, w2.id, w2.shortname 
from world_1789 w , world_1789 w1, world_1789 w2
where w1.id_sup = w.id and w.unitlevel = 0 and w1.unitlevel = 1 and w2.unitlevel = 2 and w2.id_sup = w1.id
order by w.shortname, w1.shortname 

create table ports.world_1789_23mars2021 as (select * from ports.world_1789 w)
create table ports.lau_europe_23mars2021 as (select * from ports.lau_europe )
drop table ports.lau_europe_23mars2021
drop table ports.world_1789_23mars2021

-- erreur sur une petite partie du Piemont en Italie qui appartient à l'Autriche
select * from ports.world_1789 where id = 693
select * from ports.world_1789 where id_sup = 693

update ports.world_1789 set unit_sup_id = 5689, unit_id = 5689, id_sup = 870, shortname= 'Autriche'
where id = 693 and unit_id = 1 -- pas fait


------------------------------------------------------------------------------------------------------------------
-- le 26 mars 2021 : suite
------------------------------------------------------------------------------------------------------------------

comment on table lau_europe is 'import des géométries des communes d''Europe, source : https://ec.europa.eu/eurostat/fr/web/gisco/geodata/reference-data/administrative-units-statistical-units/communes#communes16 ; Enrichi avec les attributs province_code, province_name, sourcegeom, unit_id';
comment on column lau_europe.methodg is 'comment a été déterminée l''appartenance à un état (level 0): 1 (inclusion totale) - 2 (intersection, mais avec toujours le même état) -3 (intersection et report de l''état partageant la plus grande surface avec la commune) ; 4 (manuelle), 5 (par extension avec voisins les plus contigus), 6 (insertion de géométries d''autres sources)';
comment on column lau_europe.methodnum is 'comment a été déterminée l''appartenance à une province (level 2): 1 (inclusion totale) - 2 (intersection, mais avec toujours la même province) -3 (intersection et report de la province partageant la plus grande surface avec la commune) ; 4 (manuelle), 5 (par extension avec voisins les plus contigus), 6 (insertion de géométries d''autres sources)';
comment on column lau_europe.id_hgis is 'identifiant interne de la province, level 2';
comment on column lau_europe.id_sup is 'identifiant de l''état d''appartenance, level 0';
comment on column lau_europe.province_code is 'code de la province, level 2 - pas toujours renseigné - a servi à construire le level 1 manuellement';
comment on column lau_europe.province_name is 'nom de la province, level 2 - pas toujours renseigné - a servi à construire le level 1 manuellement';
comment on column lau_europe.sourcegeom is 'source de la géométrie, si externe à la couche originelle : world_borders https://thematicmapping.org/downloads/world_borders.php ou usa https://www2.census.gov/geo/tiger/GENZ2019/shp/cb_2019_us_state_500k.zip';
comment on column lau_europe.unit_id is 'FK vers la table world_borders, donnant l''identifiant de la province (level 2) dans la table world_borders : FK vers id';

create table ports.lau_europe_31mars2021 as (select * from ports.lau_europe )
comment on table lau_europe_31mars2021 is 'Backup du 31 mars - import des géométries des communes d''Europe, source : https://ec.europa.eu/eurostat/fr/web/gisco/geodata/reference-data/administrative-units-statistical-units/communes#communes16 ; Enrichi avec les attributs province_code, province_name, sourcegeom, unit_id';
comment on column lau_europe_31mars2021.methodg is 'comment a été déterminée l''appartenance à un état (level 0): 1 (inclusion totale) - 2 (intersection, mais avec toujours le même état) -3 (intersection et report de l''état partageant la plus grande surface avec la commune) ; 4 (manuelle), 5 (par extension avec voisins les plus contigus), 6 (insertion de géométries d''autres sources)';
comment on column lau_europe_31mars2021.methodnum is 'comment a été déterminée l''appartenance à une province (level 2): 1 (inclusion totale) - 2 (intersection, mais avec toujours la même province) -3 (intersection et report de la province partageant la plus grande surface avec la commune) ; 4 (manuelle), 5 (par extension avec voisins les plus contigus), 6 (insertion de géométries d''autres sources)';
comment on column lau_europe_31mars2021.id_hgis is 'identifiant interne de la province, level 2';
comment on column lau_europe_31mars2021.id_sup is 'identifiant de l''état d''appartenance, level 0';
comment on column lau_europe_31mars2021.province_code is 'code de la province, level 2 - pas toujours renseigné - a servi à construire le level 1 manuellement';
comment on column lau_europe_31mars2021.province_name is 'nom de la province, level 2 - pas toujours renseigné - a servi à construire le level 1 manuellement';
comment on column lau_europe_31mars2021.sourcegeom is 'source de la géométrie, si externe à la couche originelle : world_borders https://thematicmapping.org/downloads/world_borders.php ou usa https://www2.census.gov/geo/tiger/GENZ2019/shp/cb_2019_us_state_500k.zip';
comment on column lau_europe_31mars2021.unit_id is 'FK vers la table world_borders, donnant l''identifiant de la province (level 2) dans la table world_borders : FK vers id';

create table ports.world_1789_31mars2021 as (select * from ports.world_1789 w)
comment on table world_1789 is 'Fabriqué par union des géométries des communes, d''abord au niveau 2 pour faire les provinces, puis au niveau 1 pour faire les sous-états, puis au niveau 0 pour faire les états. Imports de morceaux de monde non distingués depuis world borders identiques aux 3 niveaux : Afrique, Amérique du Sud, Asie/Océanie';
comment on column world_1789.id is 'identifiant unique de l''entité, quelque soit son niveau';
comment on column world_1789.unit_id is 'identifiant de l''état si level = 0, de la province si level = 2, parfois manquant';
comment on column world_1789.unit_code is 'code de l''entité, parfois renseigné';
comment on column world_1789.unitlevel is '0 si c''est un état, 1 un sous-état, 2 une province';
comment on column world_1789.unit_sup_id is 'identifiant de l''unité supérieure d''appartenance (FK vers unit_id)';
comment on column world_1789.sourcegeom is 'source des géometries';
comment on column world_1789.sourcecode is 'mode d''agrégation des unités pour constituer les frontières';
comment on column world_1789.geom3857 is 'Géométries en projection EPSG:3857 - multipolygones ou polygones';
comment on column world_1789.shortname is 'Nom de l''entité en français';
comment on column world_1789.id_sup is 'identifiant de l''entité d''appartenance de niveau supérieur (FK sur id), ou -1 si unitlevel = 0';


alter table ports.lau_europe_31mars2021 drop column id_hgis;
alter table ports.lau_europe_31mars2021 drop column id_sup;
drop table lau_europe_23mars2021

alter table ports.world_1789_31mars2021 drop column unit_id;
alter table ports.world_1789_31mars2021 drop column unit_sup_id;
alter table ports.world_1789_31mars2021 add column geom4326 geometry;
update ports.world_1789_31mars2021 set geom4326 = st_setsrid(st_transform(geom3857, 4326), 4326);

comment on table world_1789_31mars2021 is 'Backup du 31 mars 2021 - Fabriqué par union des géométries des communes, d''abord au niveau 2 pour faire les provinces, puis au niveau 1 pour faire les sous-états, puis au niveau 0 pour faire les états. Imports de morceaux de monde non distingués depuis world borders identiques aux 3 niveaux : Afrique, Amérique du Sud, Asie/Océanie';
comment on column world_1789_31mars2021.id is 'identifiant unique de l''entité, quelque soit son niveau';
comment on column world_1789_31mars2021.unit_code is 'code de l''entité, parfois renseigné';
comment on column world_1789_31mars2021.unitlevel is '0 si c''est un état, 1 un sous-état, 2 une province';
comment on column world_1789_31mars2021.sourcegeom is 'source des géometries';
comment on column world_1789_31mars2021.sourcecode is 'mode d''agrégation des unités pour constituer les frontières';
comment on column world_1789_31mars2021.geom3857 is 'Géométries en projection EPSG:3857 - multipolygones ou polygones';
comment on column world_1789_31mars2021.shortname is 'Nom de l''entité en français';
comment on column world_1789_31mars2021.id_sup is 'identifiant de l''entité d''appartenance de niveau supérieur (FK sur id), ou -1 si unitlevel = 0';
comment on column world_1789_31mars2021.geom4326 is 'Géométries en projection EPSG:4326 - multipolygones ou polygones';

drop table world_1789_23mars2021 
drop table prussia_1789 
drop table pologne_1789 

drop table world_1789_backup 
create table world_1789_backup as (select * from world_1789)

drop table lau_europe_backup 
create table lau_europe_backup as (select * from lau_europe)

------------------------------------------------------
-- travail du vendredi matin perdu
------------------------------------------------------

-- Requete QGIS qui exporte des fonds mixte
("unitlevel" =0 and shortname = 'Etats-Unis d''Amérique') or (unitlevel = 2 and id_sup = 944 ) 
or (unitlevel = 1 and id != 944 and id_sup != 854)

-- Creation de cette table ports.world_1789_mixte
-- DROP TABLE ports.world_1789_mixte;

CREATE TABLE ports.world_1789_mixte (
	unitlevel int4 NULL,
	shortname text NULL,
	id int4 NULL,
	id_sup int4 NULL,
	geom3857 geometry NULL,
	state_shortname text NULL,
	geom4326 geometry NULL,
	centroid3857 geometry NULL,
	centroid4326 geometry NULL
);

-------------------------------------------------------------------------------------------------------------------
-- Reprise des travaux sur world_1789 avec les frontières dessinées par Charlotte
-- 21 juillet 2021
-------------------------------------------------------------------------------------------------------------------

set search_path = 'ports', public

-- shp de Charlotte : 3 attributs utiles, id et id_sup, mais par contre le shortname is mal encodé en UTF_8. Récupérer les limites des entités.

export PGCLIENTENCODING=latin1
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\Charlotte_30juin2021\World_1789_level2_final.shp -a_srs EPSG:3857 -nln world_1789_level2_charlotte -nlt MULTIPOLYGON

-- importer le fichier CSV créé par Charlotte et annoté par Christine 



drop table world_1789_backup;
create table world_1789_backup as (select * from world_1789);

select max(id) from world_1789
-- 945
select max(unit_id) from world_1789
-- 200032
select max(unit_sup_id) from world_1789
-- 100011

--insert into world_1789 (unitlevel, unit_id, unit_sup_id, sourcegeom, sourcecode, geom3857, shortname)
--(select 2, 200033, 200033, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry, shortname 
--from world_1789_level2_charlotte where shortname='Kamtchatka')
--
--select 2, 200033, 200033, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry, shortname 
--from world_1789_level2_charlotte where shortname like '%rehange'


select 2, w.id, im.shortname2, w.shortname, new_unit_id, new_unit_sup_id, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry
from world_1789_level2_charlotte w, id_modificationsentites im 
where todo_christine = 'create' and im.id_new = w.id and trim(im.shortname2) = trim(w.shortname) 
order by new_unit_id

select 2, w.ogc_fid, w.id, im.id_new, im.shortname2, w.shortname, new_unit_id, new_unit_sup_id, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry
from world_1789_level2_charlotte w, id_modificationsentites im 
where todo_christine = 'create' and  im.new_unit_id = 200044 and '805' = w.id

select * from world_1789_level2_charlotte where id is null
update world_1789_level2_charlotte w set shortname='Comté de Créhange', id = '?' where ogc_fid =6
update world_1789_level2_charlotte w set shortname='Disputed between West Florida and USA' where ogc_fid =649
update world_1789_level2_charlotte w set shortname='Disputed between Rupert’s land (UK) and USA' where ogc_fid =641
update world_1789_level2_charlotte w set shortname='Rupert’s land (UK)' where ogc_fid =644

select * from world_1789_level2_charlotte where id is null

--- Do create : 15
insert into world_1789 (unitlevel, shortname, unit_id, unit_sup_id, id_sup, sourcegeom, sourcecode, geom3857)
select 2, im.shortname2, new_unit_id, new_unit_sup_id, new_unit_sup_id, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry
from world_1789_level2_charlotte w, id_modificationsentites im 
where todo_christine = 'create' and im.id_new = w.id and trim(im.shortname2) = trim(w.shortname) 
order by new_unit_id

select * from world_1789 where unit_id > 200032 and unit_sup_id > 200032
update world_1789 set id_sup = id where unit_id > 200032 and unit_sup_id > 200032

-- Rajouter Oneglia comme appartenant au piedmont
insert into world_1789 (unitlevel, unit_id, unit_sup_id, id_sup, sourcegeom, sourcecode, geom3857, shortname)
(select 2, 200047, 693, 693, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry, shortname 
from world_1789_level2_charlotte where shortname='Oneglia')

-- update 200039	743 / west Florida
UPDATE world_1789 SET unit_sup_id = 743, id_sup=743 WHERE unit_id = 200039 AND shortname = 'West Florida'
select st_area(geom3857) ,* from world_1789 where unit_id = 200039 AND shortname = 'West Florida'
select * from world_1789 where sourcegeom ='Prestation saisie manuelle, 29 juin 2021' order by shortname
delete from world_1789 where id = 953

-- update 200039	743 / East Florida
select * from id_modificationsentites where new_unit_id = 569	--743
update id_modificationsentites set new_unit_sup_id = 743 where new_unit_id = 569

-- Do union 

-- Ces cas seront traités à la main
select count(*), id 
from world_1789_level2_charlotte
group by id
having count(*) > 1

select * from id_modificationsentites where todo_christine = 'UNION' -- 92, 823
select * from world_1789_level2_charlotte where id in ('92', '823')
select * from world_1789_level2_charlotte where id = '823'
select * from world_1789_level2_charlotte where id = '503'

select * from world_1789 where id = 92
select * from world_1789 where id = 823
select * from world_1789 where id = 503

-- 2 morceaux de Nassau
update world_1789 w set geom3857 = k.geom
from (
	select st_union(wkb_geometry) as geom
	from world_1789_level2_charlotte c 
	where ogc_fid in (240, 638)
) as k
where w.id = 503

-- 2 morceaux de Russie
select * from world_1789_level2_charlotte where id = '640' and shortname is null

update world_1789 w set geom3857 = k.geom
from (
	select st_union(wkb_geometry) as geom
	from world_1789_level2_charlotte c 
	where ogc_fid in (657, 658)
) as k
where w.id = 640  

select st_area(wkb_geometry), ogc_fid, shortname 
	from world_1789_level2_charlotte c 
	where ogc_fid in (636, 639, 640)
	
-- 3 morceaux d'Alsace
update world_1789 w set geom3857 = k.geom
from (
	select st_union(wkb_geometry) as geom
	from world_1789_level2_charlotte c 
	where ogc_fid in (636, 639, 640)
) as k
where w.id = 92  
-- Il y a des pbs de topologie sur cette nouvelle géométrie

-- none 
-- 9450307712,061 m² , ogc_fid 636 cette partie est nouvelle, indépendante de la France, elle s'appelle Alsace
-- 10228538245,146 m², ogc_fid 639 cette partie est l'Alsace doit être réunie avec la petite
-- 14094455.586 m², ogc_fid 640

-- annulé ensuite
update world_1789 w set geom3857 = k.geom3857
from (
	select  *
	from world_1789_backup c
	where id = 92 
) as k
where w.id = 92  

select  *
	from world_1789 c
	where id = 92 


-- reste du monde avec Kamtchatka
update world_1789 w set geom3857 = k.geom
from (
	select st_union(wkb_geometry) as geom
	from world_1789_level2_charlotte c 
	where ogc_fid in (1, 431)
) as k
where w.id = 823  

-- Maine : 3 morceaux mais celui du milieu à mettre comme disputed area
select st_area(wkb_geometry),* from world_1789_level2_charlotte where id in ('819') order by st_area(wkb_geometry)
-- 98844083.26115505	656 (ile)
-- 44179079389.403564	655 (disputed area)
-- 132153285846.43036	255 (Maine)
select * from world_1789 where id = 819

update world_1789 w set geom3857 = k.geom, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021', sourcecode='656, 255'
from (
	select st_union(wkb_geometry) as geom
	from world_1789_level2_charlotte c 
	where ogc_fid in (656, 255)
) as k
where w.id = 819  

-- create 200048 as Disputed between Massachusetts & Colony of new Brunswick (UK)
 insert into world_1789 (unitlevel, unit_id, unit_sup_id, id_sup, sourcegeom, sourcecode, geom3857, shortname)
(select 2, 200048, 200048, 200048, 'Prestation saisie manuelle, 29 juin 2021', ogc_fid, wkb_geometry, 'Disputed between Massachusetts and Colony of new Brunswick (UK)' 
from world_1789_level2_charlotte where id in ('819')  and st_area(wkb_geometry)>98844084 and  st_area(wkb_geometry)<132153285846 )

-- Pologne et Livonie avec le même id

select st_area(wkb_geometry), * from world_1789_level2_charlotte where id = '166'
410109090196.0048	4 --  Pologne (id 641  / appartient à la Pologne 657)
118820846507.24118	58 -- Livonie  id 166 / appartient à la Russie (671)
-- https://fr.wikipedia.org/wiki/Gouvernement_de_Livonie
-- la Livonie (capitale Riga) appartient à la Russie (671) depuis 1710
-- https://commons.wikimedia.org/wiki/File:Polish-Lithuanian_Commonwealth_in_1772.PNG?uselang=fr
-- Morceau de Pologne concerné : id 641 pourrait s'appeler MINSKIE-WILENSKIE-etc
-- NON Polockie-Witebskie-Mscislawskie (il n'a pas de shorname)
-- mettre à jou r
SELECT 118820846507 < 410109090196 : oui
update world_1789_level2_charlotte set shortname = 'Livonie' where id = '166' and st_area(wkb_geometry) < 118820846508
update world_1789_level2_charlotte set id = '641', shortname = 'Miskie-Nowogrodskie-Brzesco-Litewskie' where id = '166' and st_area(wkb_geometry) > 118820846508

-- Ici : reste update et delete
select * from id_modificationsentites where id_new = '62' and shortname ='Saintonge' 
update id_modificationsentites set shortname2 ='Saintonge' where id_new = '62' and shortname ='Saintonge' and shortname2 is null 

update id_modificationsentites set todo_christine = 'update', shortname2='Lorraine' where  shortname ='Lorraine' and shortname2 is null and id_old = '579' 


-- Do update
select shortname2, id_new from id_modificationsentites where todo_christine = 'update'

update id_modificationsentites set todo_christine = 'create', new_unit_id = 200047, new_unit_sup_id = 693 where shortname2 = 'Oneglia' and id_new = '225'

-- fait : 13 lignes
update  world_1789 w set shortname=k.shortname2,  sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from (
select  w.id, im.shortname2, ogc_fid, wkb_geometry
from world_1789_level2_charlotte w, id_modificationsentites im 
where todo_christine = 'update' and im.id_new = w.id and trim(im.shortname2) = trim(w.shortname) 
order by w.id
)as k
where k.id::integer = w.id


-- manquent 95, Flanders and Conquest + 83 Île-de-France
select  *
from world_1789_level2_charlotte w
where id = '83'


select * from world_1789 w where id=166

update  world_1789 w set shortname='Flandres',  sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '95' and w.id=95

update  world_1789 w set shortname='Île-de-France',  sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '83' and w.id=83

-- update Pologne 641 & Livonie 166 pour shortname et geom en priorité car pas listés dans le fichier Excel
update  world_1789 w set shortname=k.shortname,  sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '641' and w.id=641

update  world_1789 w set shortname=k.shortname,  sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '166' and w.id=166

-- Mise à jour nécessaires
-- pays bas autrichiens
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '627' and w.id=627
-- Bouillon
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '55' and w.id=55
-- la Marche
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '77' and w.id=77
-- Russie
update  world_1789 w set   sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
where w.id=640
-- Berry
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '79' and w.id=79
-- Orléanais
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '80' and w.id=80
-- Navarre
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '58' and w.id=58
-- Gênes
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '98' and w.id=98
-- Gênes
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '98' and w.id=98
-- Lucca
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '235' and w.id=235 
-- Lunigiana
update  world_1789 w set sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from world_1789_level2_charlotte k
where k.id = '232' and w.id=232 
select * from world_1789 w where w.shortname = 'East Florida'

-- Ces cas peuvent être utilisés pour faire les mises à jour, mais les noms sont pourris (UTF8)
select id , array_agg(shortname)
from world_1789_level2_charlotte
group by id
having count(*) = 1
-- 634 cas

update id_modificationsentites set todo_christine = 'UNION' where id_new = '503' and shortname = 'Nassau'

-- to delete : 15
select id, shortname from world_1789 w where unitlevel = 2 and id not in (select id::integer from world_1789_level2_charlotte where id <> '?') and unit_id < 200033
select id, shortname from world_1789_level2_charlotte where id = '?'
alter table world_1789 add column to_delete boolean default false 
update world_1789 set to_delete = true where id not in (select id::integer from world_1789_level2_charlotte where id <> '?') and unitlevel = 2 and unit_id < 200033
-- 15
select distinct id_old, shortname from id_modificationsentites im where todo_christine = 'delete'
order by id_old 
-- 42

update  world_1789 w set to_delete = true
-- ,  sourcecode=k.ogc_fid, geom3857=k.wkb_geometry, sourcegeom = 'Prestation saisie manuelle, 29 juin 2021'
from (
select  distinct w.id, im.shortname, w.shortname, w. to_delete
from world_1789 w, id_modificationsentites im 
where todo_christine = 'delete' and im.id_old = w.id and trim(im.shortname) = trim(w.shortname) and to_delete = false
order by w.id
)as k
where k.id::integer = w.id
-- 27 etats des USA

-- Californie, Arizona, Washington, Georgia
update  world_1789 w set to_delete = true where id in (835, 838, 618, 798) and unitlevel = 2
-- "unitlevel" = 2 and to_delete is false
-- and "unit_sup_id" = 1225 and unit_code is null

-- TODO
select shortname, sourcegeom from world_1789 w where id in (641, 166)


select * from world_1789 where id_sup = 902

select id, shortname, id_sup , unitlevel from world_1789 where id = 657


select id, shortname, id_sup , unitlevel from world_1789 where id_sup = 746
select id, shortname, id_sup , unitlevel from world_1789 where id = 746 -- colonies britanniques d'Amériques

select id, shortname, id_sup , unitlevel from world_1789 where id = 854 -- Etats-Unis d'Amérique
select id, shortname, id_sup , unitlevel from world_1789 where id_sup = 854 -- Etats-Unis d'Amérique

-- cas de Georgie Russe (id 68 dans world_borders) : à retirer de l'état russe 640 (appartient à empire russe 860) et du reste du monde (822 e 823)
select * from world_borders wb  where id = 68
select * from world_1789 wb  where id = 640
update world_1789 set shortname = 'Russie' where id = 640;

select st_area(r.geom3857), st_area(g.mpolygone3857),  st_isvalid(st_difference(r.geom3857, g.mpolygone3857)) , st_area(st_difference(r.geom3857, g.mpolygone3857))
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 640

update world_1789 w set geom3857 = k.geom  
from (
select st_difference(r.geom3857, st_buffer(g.mpolygone3857, 2500)) as geom
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 640
) as k 
where w.id = 640

-- 81830251196923.75	127467517152.33359 81702783679771.42
select st_area(r.geom3857), st_area(g.mpolygone3857),  st_area(st_difference(r.geom3857, g.mpolygone3857)),  st_isvalid(st_difference(r.geom3857, g.mpolygone3857))
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 822
-- 56439593813498.234	127467517152.33359 56312126296345.89
-- select * from world_1789 r  where r.id = 640

select id, shortname, unitlevel from world_1789 where shortname = 'reste Asie, Moyen-Orient et Océanie';


update world_1789 w set geom3857 = k.geom  
from (
select st_difference(r.geom3857, st_buffer(g.mpolygone3857, 2500)) as geom, st_area(st_difference(r.geom3857, g.mpolygone3857))
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 822
) as k 
where w.id = 822;

update world_1789 w set geom3857 = k.geom  
from (
select st_difference(r.geom3857, st_buffer(g.mpolygone3857, 2500)) as geom, st_area(st_difference(r.geom3857, g.mpolygone3857))
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 821
) as k 
where w.id = 821;


update world_1789 w set geom3857 = k.geom  
from (
select st_difference(r.geom3857, st_buffer(g.mpolygone3857, 2500)) as geom, st_area(st_difference(r.geom3857, g.mpolygone3857))
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 823
) as k 
where w.id = 823;


update world_1789 w set geom3857 = k.geom  
from (
select st_difference(r.geom3857, st_buffer(g.mpolygone3857, 2500)) as geom, st_area(st_difference(r.geom3857, g.mpolygone3857))
from world_borders g  , world_1789 r 
where g.id = 68 and r.id = 823
) as k 
where w.id = 823
select max(unit_id) from world_1789
select distinct sourcegeom from world_1789

-- create 200049 as Georgia. At unitlevel 1, il faudra mettre id_sup = 860 (empire Russe)
 insert into world_1789 (unitlevel, unit_id, unit_sup_id, id_sup, sourcegeom, sourcecode, geom3857, shortname)
(select 2, 200049, 200049, 200049, 'word_borders', iso2, mpolygone3857, name 
from world_borders where id = 68   )
SELECT * FROM world_1789 WHERE  unit_id = 200049

---- Supprimer ceux qui sont à supprimer : 46
select id from world_1789 w where to_delete is true and unitlevel = 2
alter table world_1789_backup add column to_delete boolean default false
update world_1789_backup wb set to_delete = true 
from world_1789 w where w.to_delete is true and w.id = wb.id  and w.unitlevel = 2


select id, to_delete, shortname, sourcegeom from world_1789_backup where id in (799, 801)
update world_1789_backup wb set to_delete = true  where id in (799, 801)
update world_1789 wb set to_delete = true  where id in (799, 801)
update world_1789 wb set to_delete = true  where id in (799, 801)

delete from world_1789 where to_delete is true and unitlevel = 2; --46 + 1


-- mettre à jour tout sauf les nouveaux et ceux en doublons 

select w.id, w.shortname , w.sourcegeom, w.unit_id from 
world_1789 w, world_1789_level2_charlotte c
where w.unitlevel = 2 and c.id = w.id::text and w.id not in (819, 805,640,62,84,92,225,823,569,166,503) and unit_id < 200033 
AND st_equals(c.wkb_geometry,w.geom3857) IS false;
--542 / 526 + 16 déjà mise à jour
--and c.id = '569'

select count(distinct id) from world_1789 where unitlevel = 2 --654
select count(distinct id) from world_1789_level2_charlotte --644

-- Avant mise à jour des géométries avec celles de Charlotte
create table world_1789_24072021 as (select * from world_1789);
update world_1789_24072021 wb set to_delete = true  where id in (799, 801)

--Prestation saisie manuelle, 29 juin 2021
-- update geom and sourcegeom si la geometrie diffère

update world_1789 w set geom3857 = c.wkb_geometry
-- w.id, w.shortname , w.sourcegeom, w.unit_id from 
from  world_1789_level2_charlotte c
where w.unitlevel = 2 and c.id = w.id::text and w.id not in (819, 805,640,62,84,92,225,823,569,166,503) and unit_id < 200033 
AND st_equals(c.wkb_geometry,w.geom3857) IS false;
-- 526

-- export pour corriger la topologie avec pprepair
SELECT DISTINCT id , shortname FROM world_1789 w WHERE w.unitlevel = 2

select w.id, w.shortname 
from world_1789 w, world_1789 w2 
WHERE w2.id = 963 and w.unitlevel = 2 and w.unitlevel = w2.unitlevel and w.id <> w2.id  and st_intersects(w.geom3857, w2.geom3857)  

-- 3 entités ont été corrigées pour self-intersection : Canada, crimée et Allemagne

set PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\topologie\allemagne_247.shp -a_srs EPSG:3857 -nln topo_fixed -nlt MULTIPOLYGON
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\topologie\crimee_170.shp -append -a_srs EPSG:3857 -nln topo_fixed -nlt MULTIPOLYGON
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\topologie_ok\canada_592.shp -overwrite -a_srs EPSG:3857 -nln topo_fixed -nlt MULTIPOLYGON
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\topologie_ok\TerreNeuve_616.shp -overwrite -a_srs EPSG:3857 -nln topo_fixed -nlt MULTIPOLYGON

drop table topo_fixed
select * from topo_fixed where st_isvalid(wkb_geometry) is false
select st_isvalid(st_union(wkb_geometry)) from topo_fixed

update world_1789 w set geom3857 = c.wkb_geometry
from  topo_fixed c
where w.unitlevel = 2 and w.id = 247-- allemagne

update world_1789 w set geom3857 = c.wkb_geometry
from  topo_fixed c
where w.unitlevel = 2 and w.id = 170 -- crimée
-- attention, en fait elle est fausse (vérif du shp après)
update world_1789 w set geom3857 = c.wkb_geometry, shortname = 'Crimea'
from  world_1789_level2_charlotte c
where w.unitlevel = 2 and w.id = 170 and c.id = '170'
select * from world_1789 where id = 170
select st_isvalid(geom3857) from world_1789 where id = 170
select st_makevalid(geom3857) from world_1789 where id = 170
update world_1789 w set geom3857 = st_makevalid(geom3857) where id = 170

update world_1789 w set geom3857 = k.geom
from  (select (st_union(wkb_geometry)) as geom from topo_fixed ) as k
where w.unitlevel = 2 and w.id = 592 -- canada

update world_1789 w set geom3857 = k.geom
from  (select (st_union(wkb_geometry)) as geom from topo_fixed ) as k
where w.unitlevel = 2 and w.id = 616 -- Terre-Neuve

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\topologie_ok\out.shp -overwrite -a_srs EPSG:3857 -nln topo_fixed -nlt MULTIPOLYGON

select id, shortname from world_1789 where unitlevel = 2 and st_isvalid(geom3857) is false
--480	Vize
--481	Kocaeli
--374	Gallipoli
--368	Sugla
--485	Kostantiniyye
--379	Tekke
--385	Alanya
--Self-intersection at or near point 3132144.7100000009 5022292.9899999993
--Self-intersection at or near point 3259257.8900000006 4986580.9899999993
--Self-intersection at or near point 3160688.0100000016 5019472.3299999991
--Self-intersection at or near point 3008603.0600000024 4645315.9300000006
--Self-intersection at or near point 3132144.7100000009 5022292.9899999993
--Self-intersection at or near point 3259257.8900000006 4986580.9899999993
--Self-intersection at or near point 3160688.0100000016 5019472.3299999991
--Self-intersection at or near point 3008603.0600000024 4645315.9300000006
--Self-intersection at or near point 3233986.5 4999653.6100000003
--Self-intersection at or near point 3417798.2300000004 4422942.71
--Self-intersection at or near point 3556284.5700000003 4377844.1000000006


update world_1789 w set geom3857 = k.geom
from  (select (st_union(wkb_geometry)) as geom from topo_fixed ) as k
where w.unitlevel = 2 and w.id = 385 -- Alanya

update world_1789 w set geom3857 = k.geom
from  (select (st_union(wkb_geometry)) as geom from topo_fixed ) as k
where w.unitlevel = 2 and w.id = 368 -- Sugla

update world_1789 w set geom3857 = k.geom
from  (select (st_union(wkb_geometry)) as geom from topo_fixed ) as k
where w.unitlevel = 2 and w.id = 480 -- Vize

update world_1789 w set geom3857 = k.geom
from  (select (st_union(wkb_geometry)) as geom from topo_fixed ) as k
where w.unitlevel = 2 and w.id = 481 -- Kocaeli

select id, shortname, st_isvalid(st_makevalid(geom3857)) 
from world_1789 where unitlevel = 2 and st_isvalid(geom3857) is false

alter table world_1789 add column valid_geom3857 geometry; 
update world_1789 set valid_geom3857 = st_makevalid(geom3857)
where unitlevel = 2 and id in (374, 485, 379);
update world_1789 set valid_geom3857 = st_buffer(valid_geom3857,0)
where unitlevel = 2 and id in (374, 485, 379);

update world_1789 set valid_geom3857 = geom3857
where unitlevel = 2 and id not in (374, 485, 379);

update world_1789 set  geom3857 = valid_geom3857
where unitlevel = 2 and id  in (374, 485, 379);

-- Tekke, Gallipoli et Kostantiniyye : reglé

/*select id, shortname, st_area(geom3857), st_perimeter(geom3857) from world_1789
where unitlevel = 2 and id in (374, 485, 379)
-- bug bizarre

-- retour en arrière
update world_1789 w set geom3857 = wb.geom3857
from world_1789_24072021 wb
where  wb.id in (374, 485, 379) and w.id = wb.id and w.unitlevel = 2

update world_1789 w set geom3857 = wb.wkb_geometry 
from world_1789_level2_charlotte wb
where  wb.id in ('374', '485', '379') and w.id::text = wb.id and w.unitlevel = 2
*/

--------------------------------------------------------
-- 27 juillet 2021 - calcul des hiérarchies supérieures
--------------------------------------------------------
 
select id, shortname, id_sup from world_1789 WHERE id IN (962, 293, 223)
select id, shortname, id_sup from world_1789 where unit_id= 200049
select id, shortname, id_sup, unit_id, unit_sup_id from world_1789 where unit_id>= 200033 
update world_1789 set id_sup = 962 where  id =962;
select id, shortname, id_sup from world_1789 WHERE id IN (693, 679)

update world_1789 set shortname = 'Milano' where id = 293;
update world_1789 set shortname = 'Mantua', id_sup=679 where id = 223



-- where and w.id not in (819, 805,640,62,84,92,225,823,569,166,503) and unit_id < 200033 

-- traiter les 17 nouvelles unités
select id, shortname, id_sup, unit_id, unit_code, unit_sup_id from world_1789 
where unit_id>= 200033 and unitlevel =2

-- traiter les 6 nouvelles unités indépendantes
select id, shortname, id_sup, unit_id, unit_code, unit_sup_id from world_1789 
where unit_id>= 200033 and unit_id=unit_sup_id and unitlevel =2

insert into world_1789 (unitlevel,  unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857)
select 1, unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857  from world_1789 where unit_id>= 200033 and unit_id=unit_sup_id 

-- rajouter la Georgia USA (id = 954 )
insert into world_1789 (unitlevel,  unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857)
select 1, unit_id, unit_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857  from world_1789 where unit_id= 200040 and shortname = 'Georgia'

select * from world_1789 where unitlevel = 1 and unit_id= 200040 and shortname = 'Georgia'
update world_1789 set id_sup = 975 where unit_id= 200040 and shortname = 'Georgia' and unitlevel = 2
update world_1789 set id_sup = 854 where unit_id= 200040 and shortname = 'Georgia' and unitlevel = 1

-- rajouter Unclaimed territory USA (id = 950 )
insert into world_1789 (unitlevel,  unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857)
select 1, unit_id, unit_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857  from world_1789 where unit_id= 200037 and shortname = 'Unclaimed territory'

select * from world_1789 where unitlevel = 1 and unit_id= 200037 and shortname = 'Unclaimed territory'
update world_1789 set id_sup = 976 where unit_id= 200037 and shortname = 'Unclaimed territory' and unitlevel = 2

insert into world_1789 (id_sup, unitlevel,  unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857)
select -1, 0, unit_id, unit_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857  from world_1789 where unit_id= 200037 and shortname = 'Unclaimed territory' and unitlevel = 1

-- 
select id, shortname, id_sup, unit_id, unit_code, unit_sup_id from world_1789 
where unit_id>= 200033 and unit_id=unit_sup_id and unitlevel =1

update world_1789 w set id_sup = level1.id
from world_1789 level1 
where w.unitlevel =2 and w.unit_id>= 200033 and w.unit_id=w.unit_sup_id and w.shortname = level1.shortname  

select id, shortname, id_sup, unit_id, unit_code, unit_sup_id from world_1789 
where unit_id>= 200033 and unit_id=unit_sup_id and unitlevel =1

select id, shortname, id_sup from world_1789  where unitlevel = 0 and shortname = 'Russie'
update world_1789 w set id_sup = 860 where id = 964 and unitlevel =1 and shortname = 'Georgia'

update world_1789 w set geom3857 = k.g
from
	(select id_sup, st_union(geom3857) as g
	from world_1789 level2
	where unitlevel = 2 and id_sup <> -1
	group by id_sup
	) as k
where k.id_sup = w.id and w.unitlevel = 1
-- 145 



select count(*) from world_1789 where unitlevel = 1
-- 163 

select *  from world_1789 where unitlevel = 1 and id_sup = -1
-- 8 

select shortname, id, id_sup, unit_id, unit_sup_id 
from world_1789 where unitlevel = 1 and id_sup <> -1
and id not in (select id_sup from world_1789  where unitlevel = 2)

-- supprimer ces 15 entités
/*
Minnesota	779
Indiana	783
Salm	714
Kentucky	771
Georgia	772
Wisconsin	773
Tennessee	775
West Virginia	782
Michigan	777
Illinois	778
Ohio	769
Alabama	770
Mississippi	786
Mulhouse	734
Texas indépendant	833
*/

delete from world_1789 where unitlevel = 1 and id_sup <> -1
and id not in (select id_sup from world_1789  where unitlevel = 2)

-- update la geom de  822  reste Asie, Moyen-Orient et Océanie
select * from world_1789 where id  = 821 and unitlevel = 0
select * from world_1789 where id  = 822 and unitlevel = 1

select * from world_1789 where id  = 823 and unitlevel = 2
update world_1789 w set geom3857 = level2.geom3857
from world_1789 level2 
where level2.id  = 823 and level2.unitlevel = 2 and w.id in (821, 822);

-- level 0

-- traiter les 5 nouvelles entités indépendantes
insert into world_1789 (unitlevel, id_sup, unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857)
select 0, id_sup, unit_id, unit_sup_id, unit_code, sourcegeom, sourcecode, shortname, longname, geom3857  
from world_1789 where unit_id>= 200033 and unit_id=unit_sup_id and unitlevel = 1 and id_sup = -1


select id, shortname, id_sup, unit_id, unit_code, unit_sup_id from world_1789 
where unit_id>= 200033 and unit_id=unit_sup_id and unitlevel =1 and w.shortname <> 'Georgia'

select id, shortname, id_sup, unit_id, unit_code, unit_sup_id from world_1789 
where unit_id>= 200033 and unit_id=unit_sup_id and unitlevel =0

update world_1789 w set id_sup = level0.id
from world_1789 level0 
where level0.unitlevel = 0 and w.unitlevel =1 and w.unit_id>= 200033 and w.unit_id=w.unit_sup_id and w.shortname = level0.shortname  and w.shortname <> 'Georgia'
-- 5 unités : 966	Northwest Territory / 968	Disputed between West Florida and USA / 965	Kamtchatka / 967	Disputed between Massachusetts and Colony of new Brunswick (UK)
-- 969	Disputed between Rupert’s land (UK) and USA

-- faire l'union
update world_1789 w set geom3857 = k.g
from
	(select id_sup, st_union(geom3857) as g
	from world_1789 level1
	where unitlevel = 1 and id_sup <> -1
	group by id_sup
	) as k
where k.id_sup = w.id and w.unitlevel = 0
-- 100 lignes en 2min 18

update world_1789 w set geom3857 = k.g
from
	(select id_sup, st_union(geom3857) as g
	from world_1789 level1
	where unitlevel = 1 and id_sup = 854
	group by id_sup
	) as k
where k.id_sup = w.id and w.unitlevel = 0



select count(*) from world_1789 where unitlevel = 0
--  106

select *  from world_1789 where unitlevel = 0 and id_sup = -1
--  

select shortname, id, id_sup, unit_id, unit_sup_id 
from world_1789 where unitlevel = 0 
and id not in (select id_sup from world_1789  where unitlevel = 1)

-- supprimer les entités de niveau 0 qui n'ont plus lieu d'être
--Texas indépendant	942
--Salm	918
--Mulhouse	941
delete from world_1789 where id in (942, 941, 918 ) and unitlevel = 0

-- Unclaimed territory
-- Georgia
select *  from world_1789 where unitlevel = 1 and id = 854

----------------
-- regler les pb de topologie

SELECT topology.CreateTopology('world_topo',3857, 100);
-- id = 1

-- SELECT topology.DropTopology('world_topo');


--SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789', 'topo', 'POLYGON');
-- 1
select DropTopoGeometryColumn('ports', 'world_1789', 'topo');
-- Layer 1 (ports.world_1789.topo) dropped
SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789', 'topo_layer3', 'MULTIPOLYGON');
-- layer 3
-- No layer registered on world_topo.world_1789.topo

select * from world_topo.node

select * from topology.layer

create table world_topo.world_1789_poly (id integer);

select topology.addTopoGeometryColumn('world_topo', 'world_topo', 'world_1789_poly', 'topogeom', 'MULTIPOLYGON')
-- 2 = layer_id

-- Insérer des géométries, avec un snapping à 100 m automatique (pour voir)
-- https://postgis.net/docs/manual-2.2/toTopoGeom.html
insert into world_topo.world_1789_poly (id, topogeom) 
select id, topology.toTopoGeom(geom3857, 'world_topo', 2, 100) from ports.world_1789 w where unitlevel=2 and id_sup = 944; -- France pour tester
-- 35 unités de la France



select * from world_topo.relation
-- topogeo_id  = 1, layer_id = 2, 63 élements de type 3
-- si topogeo_id = 32, j'ai 5 éléments_id : 62:66
-- Dans la layer_id=2, (donc ici world_topo.world_1789_poly),
-- topogeo_id (32) correspond à topogeom.id et à id (66), et id = 66 est l’identifiant de l'entité dans ports.world_1789.  
-- element_id correspond à la face (gauche ou droite), et fait la jointure avec righ_face/left_face de la table edge_data

select * from world_topo.relation r, world_topo.world_1789_poly w
where (w.topogeom).id = 32 and r.topogeo_id = 32
62
63
64
65
66


select * from world_topo.relation r
delete from world_topo.relation where topogeo_id = 36
select * from world_topo.edge_data r 

select * from world_topo.edge_data where 
left_face not in (select element_id from world_topo.relation) 
and right_face not in (select element_id from world_topo.relation)

select topology.TopoElementArray_Agg(ARRAY[r.element_id,3]) from world_topo.relation r where r.topogeo_id = 32
-- https://postgis.net/docs/manual-2.2/CreateTopoGeom.html
-- https://trac.osgeo.org/postgis/wiki/UsersWikiPostgisTopology
select * from world_topo.edge_data
-- 70 face


select * from world_topo.world_1789_poly
-- map topogeom.id de l'élément à l'id de la couche source (de world_1789)


-- 66 = Bretagne
select w.id, element_id , mbr  
from world_topo.relation r, 
world_topo.world_1789_poly w, world_topo.face f
where  w.id = 66 and (w.topogeom).id = r.topogeo_id and r.element_id = f.face_id 
 

-- liste des arcs qui font la Bretagne
select w.id, e.geom, start_node, end_node, abs_next_left_edge, abs_next_right_edge
from world_topo.relation r, world_topo.world_1789_poly w, 
world_topo.edge_data e
where  w.id = 66 and (w.topogeom).id = r.topogeo_id 
and (r.element_id = e.left_face or r.element_id = e.right_face) 


select topology.CreateTopoGeom('world_topo',3,2, topology.TopoElementArray_Agg(ARRAY[r.element_id,3])::topology.topoelementarray)
from world_topo.relation r where r.topogeo_id = 32
-- 36 : créé un duplicat de la Bretagne, bof

UPDATE ports.world_1789 set topo_layer3 = k.mytopogeom
from 
(select  topology.CreateTopoGeom('world_topo',3,3, topology.TopoElementArray_Agg(ARRAY[r.element_id,3])::topology.topoelementarray ) as mytopogeom
from world_topo.relation r where r.topogeo_id = 32
) as k
where id = 66 and unitlevel = 2

alter table  ports.world_1789 add column new_geom geometry;

UPDATE ports.world_1789 SET new_geom = topo_layer3::geometry where id = 66 and unitlevel = 2;
select st_area(new_geom) from ports.world_1789 where id = 66 and unitlevel = 2;
--- marche pour la Bretagne

-- supprimer de l'Alsace les edge : 104, 105 et 106 et le  node 72
delete from edge_data where edge_id  in (104, 105, 106);
delete from node where node_id  in (72);
update edge_data set next_left_edge = 99, abs_next_left_edge = 99 where edge_id = 100
-- ok

-- FRANCE

-- Pour toute la France
UPDATE ports.world_1789 set topo_layer3 = null

UPDATE ports.world_1789 p set topo_layer3 = k.mytopogeom
from 
(select  topogeo_id, topology.CreateTopoGeom('world_topo',3,3, topology.TopoElementArray_Agg(ARRAY[r.element_id,3])::topology.topoelementarray ) as mytopogeom
from world_topo.relation r 
group by topogeo_id
) as k, world_topo.world_1789_poly w
where (w.topogeom).id = k.topogeo_id 
and unitlevel = 2 and p.id = w.id
-- 35
-- ATTENTION : mélange des id ici probablement : il aurait fallu and p.id = (w.topogeom).id

UPDATE ports.world_1789 SET new_geom = topo_layer3::geometry ;
update ports.world_1789 set geom3857 = new_geom where unitlevel = 2 and id_sup = 944
-- 35



UPDATE ports.world_1789 SET new_geom = topo_layer3::geometry ;
update ports.world_1789 set geom3857 = new_geom where unitlevel = 2 and id_sup = 944

-- TROP LONG ET DIFFICILE
--delete from edge_data where edge_id  in (1023, 1025, 1027, 1029, 1031);
--select * from edge_data 
--where abs_next_left_edge in (1023, 1025, 1027, 1029, 1031)
--or abs_next_right_edge in (1023, 1025, 1027, 1029, 1031) and edge_id  not in (1023, 1025, 1027, 1029, 1031) order by edge_id
--update edge_data set next_left_edge = 99, abs_next_left_edge = 99 where edge_id = 100

-- SOLUTION SUPER pour Ecosse / Pays de Galles

-- Insérer des géométries, avec un snapping à 100 m automatique (pour voir)
-- https://postgis.net/docs/manual-2.2/toTopoGeom.html
insert into world_topo.world_1789_poly (id, topogeom) 
select w2.id, topology.toTopoGeom(w2.geom3857, 'world_topo', 2, 100) 
from ports.world_1789 w , ports.world_1789 w2 
where w.unitlevel=2 and w.id in (613, 594) AND w2.unitlevel=2  AND st_intersects(w.geom3857, w2.geom3857) ;
-- Ecosse 594 et pays de Galles 613 pour tester

select w2.id, w2.geom3857
from ports.world_1789 w , ports.world_1789 w2 
where w.unitlevel=2 and w.id in (613, 594) AND w2.unitlevel=2  AND st_intersects(w.geom3857, w2.geom3857) ;
-- liste des id impliqués : ( 594,133,128,127,108,126,139,613)

--UPDATE ports.world_1789 AS p
--	SET topo_layer3 = topology.CreateTopoGeom('world_topo'
--        ,3,3
--        , foo.bfaces)
--FROM (SELECT b.id,  topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
--		FROM ports.world_1789 As b
--            INNER JOIN world_topo.face As f ON b.geom3857 && f.mbr
--        WHERE 
--        	b.unitlevel=2 and b.id_sup <> 944 and  b.id in ( 594,133,128,127,108,126,139,613)
--        	AND
--        	ST_Covers(b.geom3857, topology.ST_GetFaceGeometry('world_topo', f.face_id))
--        	OR
-- 			( ST_Intersects(b.geom3857, topology.ST_GetFaceGeometry('world_topo', f.face_id))
--            	AND ST_Area(ST_Intersection(b.geom3857, topology.ST_GetFaceGeometry('world_topo', f.face_id) ) ) > 
--                ST_Area(topology.ST_GetFaceGeometry('world_topo', f.face_id))*0.5
--                )  
--            GROUP BY b.id
--     ) As foo
--WHERE foo.id = p.id; 
-- 7 min

select face_id from world_topo.face
-- 1157
alter table  world_topo.face add column face_geom geometry;
update world_topo.face set face_geom = topology.ST_GetFaceGeometry('world_topo', face_id) where face_id <> 0
CREATE INDEX faceloc ON world_topo.face USING gist (face_geom);
CREATE INDEX world_1789_gist_index ON ports.world_1789 USING gist (geom3857);
vacuum analyze  ports.world_1789
vacuum analyze  world_topo.face

--UPDATE ports.world_1789 AS p
--	SET topo_layer3 = topology.CreateTopoGeom('world_topo'
--        ,3,3
--        , foo.bfaces)
--FROM (SELECT b.id,  topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
--		FROM ports.world_1789 As b, world_topo.face As f 
--		-- INNER JOIN world_topo.face As f ON b.geom3857 && f.mbr
--        WHERE 
--        	b.id in ( 594,133,128,127,108,126,139,613) 
--        	--and b.geom3857 && f.mbr
--        	and (
--        	ST_Covers(b.geom3857, f.face_geom)
--        	OR
-- 			( ST_Intersects(b.geom3857, f.face_geom)
--            	AND ST_Area(ST_Intersection(b.geom3857, f.face_geom ) ) > 
--                ST_Area(f.face_geom)*0.5
--                )  )
--            GROUP BY b.id
--     ) As foo
--WHERE foo.id = p.id; 
-- 7 min, 58 rows
-- 8 lignes en 51,8s
-- ne marche pas car la clause : b.geom recouvre à plus de 50% la face du trou n'est pas toujours vraie, quelque soit la geom adjacente.

-- si on se trompe, on reprend les géométries de Charlotte
update world_1789 w set geom3857 = c.wkb_geometry 
-- w.id, w.shortname , w.sourcegeom, w.unit_id from 
from  world_1789_level2_charlotte c
where w.unitlevel = 2 and c.id = w.id::text and w.id  in (594,133,128,127,108,126,139,613) 
-- qui sont bonnes (pas compris)
--
--SELECT f.face_id, max(ST_Area(ST_Intersection(b.geom3857, f.face_geom )))
--FROM ports.world_1789 as b, world_topo.face as f 
--WHERE b.id in ( 594,133,128,127,108,126,139,613) AND ST_Intersects(b.geom3857, f.face_geom) 
--GROUP BY f.face_id
--
--alter table world_topo.face add column max_area float ;
--update world_topo.face f set max_area = k.m
--from (
--SELECT f.face_id, max(ST_Area(ST_Intersection(b.geom3857, f.face_geom ))) as m
--FROM ports.world_1789 as b, world_topo.face as f 
--WHERE b.id in ( 594,133,128,127,108,126,139,613) AND ST_Intersects(b.geom3857, f.face_geom) 
--GROUP BY f.face_id
--) as k 
--where k.face_id = f.face_id
--
--
--
--UPDATE ports.world_1789 AS p
--	SET topo_layer3 = topology.CreateTopoGeom('world_topo'
--        ,3,3
--        , foo.bfaces)
--FROM (SELECT b.id,  topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
--		FROM ports.world_1789 As b, world_topo.face As f 
--		-- INNER JOIN world_topo.face As f ON b.geom3857 && f.mbr
--        WHERE 
--        	b.id in ( 594,133,128,127,108,126,139,613) 
--        	--and b.geom3857 && f.mbr
--        	and (
--        	--ST_Covers(b.geom3857, f.face_geom)
--        	--OR
-- 			( ST_Intersects(b.geom3857, f.face_geom)
--            	AND ST_Area(ST_Intersection(b.geom3857, f.face_geom ) )+1 >  f.max_area
--                )  )
--            GROUP BY b.id
--     ) As foo
--WHERE foo.id = p.id; 
---- 8 lignes en 51s
--
--UPDATE ports.world_1789 SET new_geom = topo_layer3::geometry ;

--update ports.world_1789 set geom3857 = new_geom where unitlevel = 2 and id in ( 594,133,128,127,108,126,139,613) 


-- SOLUTION pour Russie

-- Insérer des géométries, avec un snapping à 100 m automatique (pour voir)
-- https://postgis.net/docs/manual-2.2/toTopoGeom.html
insert into world_topo.world_1789_poly (id, topogeom) 
select w2.id, topology.toTopoGeom(w2.geom3857, 'world_topo', 2, 100) 
from ports.world_1789 w , ports.world_1789 w2 
where w.unitlevel=2 and w.id in (640) AND w2.unitlevel=2  AND st_intersects(w.geom3857, w2.geom3857) ;
-- erreur
-- SQL Error [XX000]: ERREUR: Could not get geometry of face 1412
-- Où : fonction PL/pgsql totopogeom(geometry,topogeometry,double precision), ligne 112 à FOR sur des lignes de SELECT
-- fonction PL/pgsql totopogeom(geometry,character varying,integer,double precision), ligne 88 à affectation

insert into world_topo.world_1789_poly (id, topogeom) 
select w.id, topology.toTopoGeom(w.geom3857, 'world_topo', 2, 100) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (640) AND w.unitlevel=2  
-- Russie
insert into world_topo.world_1789_poly (id, topogeom) 
select w.id, topology.toTopoGeom(w.geom3857, 'world_topo', 2, 100) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (963) AND w.unitlevel=2  
-- Georgia

select w2.id, w2.shortname 
from ports.world_1789 w , ports.world_1789 w2 
where w.unitlevel=2 and w.id in (640) AND w2.unitlevel=2  AND st_intersects(w.geom3857, w2.geom3857) ;
-- liste des id impliqués : (160,162,469,963,472,473,471,642,956,641,640,565,632,164,199,202,197,470,823,159)

select ed.edge_id, st_touches(w.geom3857, ed.geom) 
from ports.world_1789 w , world_topo.edge_data ed 
where w.id in (963) and st_touches(w.geom3857, ed.geom) is true

update world_topo.face set face_geom = topology.ST_GetFaceGeometry('world_topo', face_id) where face_id <> 0

update world_topo.face f set max_area = k.m
from (
SELECT f.face_id, max(ST_Area(ST_Intersection(b.geom3857, f.face_geom ))) as m
FROM ports.world_1789 as b, world_topo.face as f 
WHERE b.id in ( 594,133,128,127,108,126,139,613) AND ST_Intersects(b.geom3857, f.face_geom) 
GROUP BY f.face_id
) as k 
where k.face_id = f.face_id


UPDATE ports.world_1789 AS p
	SET topo_layer3 = topology.CreateTopoGeom('world_topo'
        ,3,3
        , foo.bfaces)
FROM (SELECT b.id,  topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
		FROM ports.world_1789 As b, world_topo.face As f 
		-- INNER JOIN world_topo.face As f ON b.geom3857 && f.mbr
        WHERE 
        	b.id in ( 594,133,128,127,108,126,139,613) 
        	--and b.geom3857 && f.mbr
        	and (
        	ST_Covers(b.geom3857, f.face_geom)
        	OR
 			( ST_Intersects(b.geom3857, f.face_geom)
            	AND ST_Area(ST_Intersection(b.geom3857, f.face_geom ) )+1 >  f.max_area
                )  )
            GROUP BY b.id
     ) As foo
WHERE foo.id = p.id; 


---
-- TODO
CREATE OR REPLACE FUNCTION ports.fix_topology_for_unit(shortname character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
        	id_unit integer;
    begin
                -- EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
	            EXECUTE 'select id from ports.world_1789 pp where shortname = '||quote_literal(shortname) into  id_unit; 

                EXECUTE 'UPDATE ports.port_points 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where toponyme_standard_fr =  '||quote_literal(p_toponyme_standard_fr); --1
				
                return nb_ports;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return -1;
            end;
$function$;



-- Nettoyer la layer 2

select DropTopoGeometryColumn('world_topo', 'world_1789_poly', 'topogeom');
-- Layer 2 (world_topo.world_1789_poly.topogeom) dropped
-- Layer 4 (world_topo.world_1789_poly.topogeom) dropped


-- Nettoyer la layer 3
select DropTopoGeometryColumn('ports', 'world_1789', 'topo_layer3');
-- Layer 3 (ports.world_1789.topo_layer3) dropped
-- Layer 5 (ports.world_1789.topo_layer3) dropped

SELECT topology.CreateTopology('world_topo',3857, 100);


-- création d'une colonne world_topo.world_1789_poly.topogeom de type topogeometrie associée à une layer (4)
create table world_topo.world_1789_poly (id integer);
select topology.addTopoGeometryColumn('world_topo', 'world_topo', 'world_1789_poly', 'topogeom', 'MULTIPOLYGON')
-- layer_id = 1

select * from world_topo.relation r 
select * from world_topo.face f  
select * from world_topo.edge_data f  
select * from world_topo.node f  

-- il y a toujours face_id = 0 au départ : l'univers

insert into world_topo.world_1789_poly (id, topogeom) 
select w.id, topology.toTopoGeom(w.geom3857, 'world_topo', 1, 100) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (640) AND w.unitlevel=2  
-- Russie
insert into world_topo.world_1789_poly (id, topogeom) 
select w.id, topology.toTopoGeom(w.geom3857, 'world_topo', 1, 100) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (963) AND w.unitlevel=2  
-- Georgia


-- alter table  world_topo.face add column face_geom geometry;
update world_topo.face set face_geom = topology.ST_GetFaceGeometry('world_topo', face_id) where face_id <> 0
-- 1415
CREATE INDEX faceloc ON world_topo.face USING gist (face_geom);
CREATE INDEX world_1789_gist_index ON ports.world_1789 USING gist (geom3857);
vacuum analyze  ports.world_1789
vacuum analyze  world_topo.face

SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789', 'topo_layer2', 'MULTIPOLYGON')
-- 2

-- https://postgis.net/docs/CreateTopoGeom.html
UPDATE ports.world_1789 p SET topo_layer2 = topology.CreateTopoGeom('world_topo',3,2, k.bfaces)
FROM (SELECT topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
		FROM  world_topo.face As f 
		where face_id not in (356, 0, 182, 183, 184, 185, 186, 187, 188, 189)      
     ) As k
WHERE p.id = 640 -- Russie


UPDATE ports.world_1789 SET new_geom = topo_layer2::geometry where id = 640 and unitlevel = 2;
-- édition manuelle de new_geom dans un shapefile russie_640 
export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\world_1789\topologie_ok\russie_640.shp -a_srs EPSG:3857 -nln russie_640 -nlt MULTIPOLYGON
select id, shortname, wkb_geometry from ports.russie_640 where id = 640 
UPDATE ports.world_1789 w SET geom3857 = wkb_geometry
from ports.russie_640 r where w.id = 640 and w.unitlevel = 2 and r.id = 640 ;
UPDATE ports.world_1789 w SET new_geom = wkb_geometry
from ports.russie_640 r where w.id = 640 and w.unitlevel = 2 and r.id = 640 ;


UPDATE ports.world_1789 p SET topo_layer2 = topology.CreateTopoGeom('world_topo',3,2, k.bfaces)
FROM (SELECT topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
		FROM  world_topo.face As f 
		where face_id  in (356)      
     ) As k
WHERE p.id = 963 -- Georgia

UPDATE ports.world_1789 SET new_geom = topo_layer2::geometry where id = 963 and unitlevel = 2;
UPDATE ports.world_1789 SET geom3857 = new_geom where id = 963 and unitlevel = 2;

----
-- Traiter le cas de Chypre (357) en doublon avec 823 (reste Asie...) et les frontières avec la Turquie
----

select DropTopology('world_topo');

SELECT topology.CreateTopology('world_topo',3857, 100);


-- création d'une colonne world_topo.world_1789_poly.topogeom de type topogeometrie associée à une layer (4)
create table world_topo.world_1789_poly (id integer);
select topology.addTopoGeometryColumn('world_topo', 'world_topo', 'world_1789_poly', 'topogeom', 'MULTIPOLYGON')
-- layer_id = 1

select * from world_topo.relation r 
select * from world_topo.face f  
select * from world_topo.edge_data f  
select * from world_topo.node f  

select w2.id, w2.shortname 
from ports.world_1789 w , ports.world_1789 w2 
where w.unitlevel=2 and w.id in (823) AND w2.unitlevel=2  AND st_intersects(w.geom3857, w2.geom3857) ;
-- liste des id impliqués :
--( 350,629,357,356,353,460,493,442,495,429,4,427,443,348,444,448,355,461,439,433,349,494,470,823,640)

--350	Jableh
--629	Egypte
--357	Cyprus
--356	Aleppo
--353	Edessa
--460	Kecvan
--493	Manbij
--442	Bergri
--495	Birecik
--429	Hakkari
--4	Chukhursaad
--427	Amadya
--443	Beyazıt
--348	Mardin
--444	Bashkala
--448	Qotur
--355	Surüç
--461	Zarsat
--439	Van
--433	Cizre
--349	Ras al-Ayn
--494	Kilis
--470	Çildir
--823	reste Asie, Moyen-Orient et Océanie
--640	Russie

-- Insérer des géométries, avec un snapping à 100 m automatique 
-- https://postgis.net/docs/manual-2.2/toTopoGeom.html
insert into world_topo.world_1789_poly (id, topogeom) 
select w2.id, topology.toTopoGeom(w2.geom3857, 'world_topo', 1, 100) 
from ports.world_1789 w , ports.world_1789 w2 
where w.unitlevel=2 and w.id in (823) AND w2.unitlevel=2  AND st_intersects(w.geom3857, w2.geom3857) ;


SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789', 'topo_layer2', 'MULTIPOLYGON');


alter table  world_topo.face add column face_geom geometry;
update world_topo.face set face_geom = topology.ST_GetFaceGeometry('world_topo', face_id) where face_id <> 0
CREATE INDEX faceloc ON world_topo.face USING gist (face_geom);

SELECT f.face_id, ST_Area(ST_Intersection(b.geom3857, f.face_geom )) as m, b.id
FROM ports.world_1789 as b, world_topo.face as f 
WHERE b.id in ( 357) AND ST_Intersects(b.geom3857, f.face_geom) 
-- 55 toutes les faces de Chypre 


select w.id, element_id , face_geom  
from world_topo.relation r, 
world_topo.world_1789_poly w, world_topo.face f
where  w.id = 357 and (w.topogeom).id = r.topogeo_id and r.element_id = f.face_id 
-- 28 (que les faces marquées appartenant à 357 et pas celles de 823)

--- requete pour affecter une face à la géométrie adjacente de plus grande surface

-- 1. calculer la plus grande surface adjacente et la plus grande distance surfacique adjacente
SELECT f.face_id, max(ST_Area(b.geom3857)) as m
FROM ports.world_1789 as b, world_topo.face as f 
WHERE b.id in ( 442, 823) AND ST_Intersects(b.geom3857, f.face_geom) and face_id = 1250
GROUP BY f.face_id

SELECT f.face_id, max(ST_Area(b.geom3857)) as m
FROM ports.world_1789 as b, world_topo.face as f 
WHERE b.id in ( 640, 823) AND ST_Intersects(b.geom3857, f.face_geom) and face_id = 1252
GROUP BY f.face_id

select face_id from world_topo.face , ports.world_1789 where ST_Intersects(geom3857, face_geom) and id in (640, 823)
select ST_Intersects(face_geom, geom3857), ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))  from world_topo.face , ports.world_1789 
where  id = 823 and face_id  = 1396 --0.74 
select ST_Intersects(face_geom, geom3857), ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))  from world_topo.face , ports.world_1789 
where  id = 640 and face_id  = 1396 -- 0.0000213285625107
-- ca marche : 1396 serait affectée à l'Asie / 823 (c'est sa plus grosse facette)

select ST_Intersects(face_geom, geom3857), ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))  from world_topo.face , ports.world_1789 
where  id = 823 and face_id  = 1237 -- 0.0000308526562635
select ST_Intersects(face_geom, geom3857), ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))  from world_topo.face , ports.world_1789 
where  id = 640 and face_id  = 1237 -- 0.0000213285625107
-- ca marche : 1237 serait affectée à l'Asie / 823

select ST_Intersects(face_geom, geom3857), ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))  from world_topo.face , ports.world_1789 
where  id = 823 and face_id  = 1250 -- 0
select ST_Intersects(face_geom, geom3857), ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))  from world_topo.face , ports.world_1789 
where  id = 442 and face_id  = 1250 -- 0
-- si c'est un trou, affecter au plus grand des deux car la distance surfacique est nulle

SELECT f.face_id, p.id
FROM 
	ports.world_1789 as p, 
	world_topo.face as f ,
	(SELECT f.face_id, max(ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))) as m, max(ST_Area(geom3857)) as surfaceG
	FROM ports.world_1789 as b, world_topo.face as f 
	WHERE  b.unitlevel=2 and (ST_Intersects(b.geom3857, f.face_geom) ) and face_id = 1396
	GROUP BY f.face_id) as k
WHERE  f.face_id = 1396 and p.unitlevel=2 and ST_Intersects(p.geom3857, f.face_geom)  and k.face_id = f.face_id 
and case when m = 0 then ST_Area(geom3857) = surfaceG else m = ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom )) end
-- requete ok sur les cas test

-- 2. récupérer l'identifiant du polygone qui a la plus grande surface adjacente



UPDATE ports.world_1789 p 
	SET topo_layer2 = topology.CreateTopoGeom('world_topo',3,2, foo.bfaces)
FROM (
	SELECT p.id, topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
	FROM 
		ports.world_1789 as p, 
		world_topo.face as f ,
		(SELECT f.face_id, max(ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))) as m, max(ST_Area(geom3857)) as surfaceG
		FROM ports.world_1789 as b, world_topo.face as f 
		WHERE  b.unitlevel=2 and (ST_Intersects(b.geom3857, f.face_geom) ) 
		GROUP BY f.face_id) as k
	WHERE  p.id = 640 and p.unitlevel=2 and ST_Intersects(p.geom3857, f.face_geom)  and k.face_id = f.face_id
			and f.face_id not in (1395, 3,4,5) -- exclure des lacs et la mer noire
		and case when m = 0 then ST_Area(geom3857) = surfaceG else m = ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom )) end
	group by p.id
	) as foo
WHERE foo.id = p.id and p.id = 640; 
-- 26 lignes en 9 min  33
-- Pas mal (le mieux jusqu'à présent)
-- 1 ligne avec la Russie p.id = 640, 5 min 13 et and f.face_id not in (1395, 3,4,5) -- exclure des lacs et la mer noire

UPDATE ports.world_1789 SET new_geom = topo_layer2::geometry 
where id = 640 and topo_layer2 is not null and unitlevel = 2;

select id, shortname from ports.world_1789  where topo_layer2 is not null
		
-- Comment Refaire 823 en retirant les facettes de Chypre ?
-- D'abord refaire Chypre 357


SELECT f.face_id, ST_Area(ST_Intersection(b.geom3857, f.face_geom )) as m, b.id
FROM ports.world_1789 as b, world_topo.face as f 
WHERE b.id in ( 357) AND ST_Intersects(b.geom3857, f.face_geom) 
-- 55 toutes les faces de Chypre 

UPDATE ports.world_1789 p 
	SET topo_layer2 = topology.CreateTopoGeom('world_topo',3,2, foo.bfaces)
FROM (
	SELECT p.id, topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
	FROM 
		ports.world_1789 as p, 
		world_topo.face as f 
	WHERE  p.id = 357 and p.unitlevel=2 and ST_Intersects(p.geom3857, f.face_geom)  
	group by p.id
	) as foo
WHERE foo.id = p.id and p.id = 357; 

UPDATE ports.world_1789 SET new_geom = topo_layer2::geometry 
where id = 357 and topo_layer2 is not null and unitlevel = 2;

-- Puis retirer de  823 la new_geom de Chypre avec un petit buffer de 5 km pour être sûrs
UPDATE ports.world_1789 p SET new_geom = st_difference(p.new_geom, st_buffer(chypre.new_geom, 5000))
from ports.world_1789 chypre  
where p.id = 823 and p.unitlevel = 2 and chypre.id = 357


--- Autre pb : Edessa en turquie (353) est associée avec une autre entité sud de la Grèce (ex stato da mar, des Vénitiens)
-- Retirer de Edessa cette entité (à créer)
-- La nouvelle entité s'appellera Stato da Mar, et aura les facettes 9:12 + 14:27 et pour entité sup 709 (l'empire Ottoman)

-- Créer la nouvelle entité  Stato da Mar
insert into ports.world_1789 (unitlevel, shortname, id_sup, sourcegeom , sourcecode, topo_layer2) 
select 2, 'Stato da Mar', 709, 'manip Christine en retirant des sous parties de Edessa 29/07/2021', '353', topology.CreateTopoGeom('world_topo',3,2, foo.bfaces)
FROM (
	SELECT topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
	FROM world_topo.face as f 
	WHERE  f.face_id in (9, 10, 11, 12) or f.face_id in (14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27)
	) as foo
-- 
select id, shortname, topo_layer2::geometry from ports.world_1789 where shortname = 'Stato da Mar' and id_sup = 709 and unitlevel =2
-- id = 978
update ports.world_1789 set geom3857 = topo_layer2::geometry, new_geom = topo_layer2::geometry
where shortname = 'Stato da Mar' and id_sup = 709 and unitlevel =2;


-- Puis retirer de  353 la new_geom de cette entite 978 avec un petit buffer de 5 km pour être sûrs
UPDATE ports.world_1789 p SET new_geom = st_difference(p.new_geom, st_buffer(stato.new_geom, 5000))
from ports.world_1789 stato  
where p.id = 353 and p.unitlevel = 2 and stato.id = 978

-- Ok, c'est assez bon pour mettre à jour
UPDATE ports.world_1789 SET geom3857 = new_geom 
where  unitlevel = 2 and new_geom is not null;


----
-- Traiter le cas de Turquie intérieure (479,476, 478, 374)
----

select DropTopology('world_topo');

SELECT topology.CreateTopology('world_topo',3857, 100);


-- création d'une colonne world_topo.world_1789_poly.topogeom de type topogeometrie associée à une layer (4)
create table world_topo.world_1789_poly (id integer);
select topology.addTopoGeometryColumn('world_topo', 'world_topo', 'world_1789_poly', 'topogeom', 'MULTIPOLYGON');
-- layer_id = 1

select * from world_topo.relation r 
select * from world_topo.face f  
select * from world_topo.edge_data f  
select * from world_topo.node f  


-- turquie : comméncé et pas fait
insert into world_topo.world_1789_poly (id, topogeom) 
select id, topology.toTopoGeom(geom3857, 'world_topo', 1, 1000) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (479,476, 478, 374) ;

-- cas de la Virginia / Maryland : ok
insert into world_topo.world_1789_poly (id, topogeom) 
select id, topology.toTopoGeom(geom3857, 'world_topo', 1, 100) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (800, 806) ;

-- cas de la South Carolina 816 / Georgia 954 / North carolina 817
insert into world_topo.world_1789_poly (id, topogeom) 
select id, topology.toTopoGeom(geom3857, 'world_topo', 1, 100) 
from ports.world_1789 w 
where w.unitlevel=2 and w.id in (816, 817, 954) ;

alter table  world_topo.face add column face_geom geometry;
update world_topo.face set face_geom = topology.ST_GetFaceGeometry('world_topo', face_id) where face_id <> 0;
CREATE INDEX faceloc ON world_topo.face USING gist (face_geom);

SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789', 'topo_layer2', 'MULTIPOLYGON');

UPDATE ports.world_1789 p 
	SET topo_layer2 = topology.CreateTopoGeom('world_topo',3,2, foo.bfaces)
FROM (
	SELECT p.id, topology.TopoElementArray_Agg(ARRAY[f.face_id,3]) As bfaces
	FROM 
		ports.world_1789 as p, 
		world_topo.face as f ,
		(SELECT f.face_id, max(ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom ))) as m, max(ST_Area(geom3857)) as surfaceG
		FROM ports.world_1789 as b, world_topo.face as f 
		WHERE  b.unitlevel=2 and (ST_Intersects(b.geom3857, f.face_geom) ) 
		GROUP BY f.face_id) as k
	WHERE  p.unitlevel=2 and ST_Intersects(p.geom3857, f.face_geom)  and k.face_id = f.face_id
		and case when m = 0 then ST_Area(geom3857) = surfaceG else m = ST_Area(ST_Intersection(geom3857, face_geom ))/ST_Area(ST_union(geom3857, face_geom )) end
	group by p.id
	) as foo
WHERE foo.id = p.id ; 
-- 3 entité

UPDATE ports.world_1789 SET new_geom = topo_layer2::geometry where id in (816, 817, 954) and unitlevel = 2;

UPDATE ports.world_1789 set geom3857 = new_geom where id in (816, 817, 954, 800, 806)

--- Refaire vite fait la hiérarchie et voila

-- level 1
update world_1789 w set geom3857 = k.g
from
	(select id_sup, st_union(geom3857) as g
	from world_1789 level2
	where unitlevel = 2 and id_sup <> -1
	group by id_sup
	) as k
where k.id_sup = w.id and w.unitlevel = 1
-- 147 : 1 min 26

-- faire l'union level 0
update world_1789 w set geom3857 = k.g
from
	(select id_sup, st_union(geom3857) as g
	from world_1789 level1
	where unitlevel = 1 and id_sup <> -1
	group by id_sup
	) as k
where k.id_sup = w.id and w.unitlevel = 0
-- 100 en 2 min 11

-- Recopier les entité de niveau id_sup = -1
select id, shortname from world_1789 where id_sup = -1;

update world_1789 w set geom3857 = k.geom3857
from world_1789 k where k.unitlevel = 2 and k.id = 823 and w.id = 822 and w.unitlevel = 1

update world_1789 w set geom3857 = k.geom3857
from world_1789 k where k.unitlevel = 2 and k.id = 823 and w.id = 821 and w.unitlevel = 0


-----------------------------------------------------------------------------------------------------
-- le 30 juillet
-- faire la carto en 4326  (WGS84)
-----------------------------------------------------------------------------------------------


select distinct w.id, w.shortname, w.id_sup, w0.shortname 
from ports.world_1789 w, ports.world_1789 w0
where w0.unitlevel = 0 and w0.id = w.id_sup and 
-- Requete QGIS qui exporte des fonds mixte
-- (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
-- or 
(w.unitlevel = 1 and w.id != 944 and w.id_sup != 854)

select shortname from ports.world_1789 w0
where w0.id = 944 and w0.unitlevel = 0
select * from ports.world_1789 where id = 845

create table ports.world_1789_mixte_30juillet2021 as (
	select id, unitlevel, shortname, id_sup, geom3857 
	from ports.world_1789 w
	where (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
	or (w.unitlevel = 1 and w.id != 944 and w.id_sup != 854)
)
-- Table pourrie à supprimer
drop table ports.world_1789_mixte_30juillet2021

SELECT topology.DropTopology('world_topo')
SELECT topology.CreateTopology('world_topo',4326, 100);
-- SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789', 'topogeom', 'MULTIPOLYGON');

update ports.world_1789 w set topogeom = topology.toTopoGeom(geom3857, 'world_topo', 1, 100) 
where (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
or (w.unitlevel = 1 and w.id != 944 and w.id_sup != 854)




SELECT topology.AsGML(topogeom) FROM countries WHERE name='Nulland'; 

drop table ports.world_1789_mixte_30juillet2021_4326

create table ports.world_1789_mixte_30juillet2021_4326 as (
	select id, unitlevel, shortname, id_sup, st_setsrid(st_transform(geom3857 , 4326), 4326) AS geom4326
	from ports.world_1789 w
	where (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
	or (w.unitlevel = 1 and w.id != 944 and w.id_sup != 854)
)

--update  ports.world_1789_mixte_30juillet2021_4326 w set geom4326 = st_makevalid(geom4326)
--where w.unitlevel = 1 and id = 671
---- st_setsrid(st_transform(geom3857 , 4326), 4326)
---- Russie
--
--select st_buffer(st_makevalid(st_setsrid(st_transform(geom3857 , 4326), 4326)), 0)
--from ports.world_1789 w 
--where w.unitlevel = 1 and id = 671
--
--update  ports.world_1789_mixte_30juillet2021_4326 w set geom4326 = st_makevalid(st_setsrid(st_transform(st_buffer(o.geom3857, -10000) , 4326), 4326))
--from ports.world_1789 o
--where w.unitlevel = 1 and w.id = 671 and o.id =671

update  ports.world_1789_mixte_30juillet2021_4326 w set geom4326 = st_makevalid(st_setsrid(st_transform(st_buffer(o.geom3857, -5000) , 4326), 4326))
from ports.world_1789 o
where w.unitlevel = 1 and w.id = 671 and o.id =671
-- Règle le pb de la Russie en 4326. En dessous, (buffer -4000 m), non

/*
SELECT topology.DropTopology('world_topo')
SELECT topology.CreateTopology('world_topo',4326);
SELECT topology.AddTopoGeometryColumn('world_topo', 'ports', 'world_1789_mixte_30juillet2021_4326', 'topogeom', 'MULTIPOLYGON');

update ports.world_1789_mixte_30juillet2021_4326 w 
set topogeom = topology.toTopoGeom(geom4326, 'world_topo', 1)
where w.unitlevel = 1 and id = 671

select * from ports.world_1789_mixte_30juillet2021_4326 w where w.unitlevel = 1 and id = 671
alter table  world_topo.face add column face_geom geometry;
update world_topo.face set face_geom = topology.ST_GetFaceGeometry('world_topo', face_id) where face_id <> 0

select * from world_topo.face
select * from world_topo.edge_data
select * from world_topo.relation
select * from world_topo.node
*/


---------------------------------
-- GROS BAZARD EN FRANCE : melange des geom, des shortname 
-- tout à refaire (corrections de topologies)
---------------------------------
select shortname from world_1789_mixte_30juillet2021_4326 where id = 92
select shortname from world_1789 where id = 92
select shortname from world_1789_backup wb where id = 92

select distinct shortname, id, wlc.ogc_fid from ports.world_1789_level2_charlotte wlc where id_sup = 944 
-- Bretagne	66
-- Alsace	92
select distinct shortname, id_old, id_new, shortname2 from ports.id_modificationsentites im 

select id, shortname from world_1789 wb where id_sup = 944 and unitlevel = 2
select id, shortname from world_1789_backup wb where id_sup = 944 and unitlevel = 2
select id, shortname from world_1789_level2_charlotte  wb where id_sup = 944 
update world_1789_level2_charlotte set shortname='Navarre-Béarn' where id = '58';
update world_1789_level2_charlotte set shortname='Dauphiné' where id = '96';
update world_1789_level2_charlotte set shortname='Franche-Comté' where id = '87';
update world_1789_level2_charlotte set shortname='Orléanais' where id = '80';
update world_1789_level2_charlotte set shortname='Normandie' where id = '61';
update world_1789_level2_charlotte set shortname='Picardie' where id = '84';
update world_1789_level2_charlotte set shortname='Île-de-France' where id = '83';
update world_1789_level2_charlotte set shortname='Flandre' where id = '95';

SELECT * FROM world_1789_backup WHERE shortname LIKE 'Flandre'
Flanders and Conquests

update world_1789 set shortname = '' where id=84
update world_1789 set shortname = 'Bretagne' where id=84

update world_1789 w set shortname = c.shortname
from world_1789_level2_charlotte c
where st_contains(c.wkb_geometry, st_centroid(w.geom3857)) and w.id_sup = 944 and w.unitlevel = 2

update world_1789 w set shortname = c.shortname, geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and w.unitlevel = 2 and w.id::text = c.id and c.id not in ('62', '84', '92')

update  world_1789 w set shortname = 'Saintonge', geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and w.unitlevel = 2 and w.id::text = c.id and c.id = '62';

update  world_1789 w set shortname = 'Alsace', geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and w.unitlevel = 2 and w.id::text = c.id and c.id = '92';

update  world_1789 w set shortname = 'Alsace', geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and w.unitlevel = 2 and w.id::text = c.id and c.id = '84';

update  world_1789 w set shortname = 'Artois' where w.id = 947
select * from world_1789 w where  w.id_sup = 944 and  w.unitlevel = 2 and w.shortname = 'Artois'
select * from world_1789_level2_charlotte w where   id = '84'
w.shortname like '%Artois%'

update  world_1789 w set   geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and  w.unitlevel = 2 and w.shortname = 'Artois'  and c.ogc_fid = 633;


update  world_1789 w set shortname = 'Picardie' where id = '84';


update  world_1789 w set  geom3857 = k.geom
from (
select shortname, st_buffer(st_union(c.wkb_geometry), 0) as geom
from world_1789_level2_charlotte c
where c.shortname = 'Alsace'
group by c.shortname
) as k
where  w.id_sup = 944 and w.unitlevel = 2 and w.shortname = k.shortname;

update world_1789 w set shortname = 'Angoumois' where id = '946';
update world_1789 w set geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and  w.unitlevel = 2 and w.shortname = 'Angoumois'  and c.ogc_fid = 630;


update world_1789 w set shortname = 'Angoumois' where id = '946';
update world_1789 w set shortname = 'Comté de Créhange', geom3857 = c.wkb_geometry
from world_1789_level2_charlotte c
where  w.id_sup = 944 and  w.unitlevel = 2 and w.id=948  and c.ogc_fid = 6;
-------------------------------------
-- encore une erreur avec un polygone manquant en Prusse

select max(unit_id) from world_1789
-- 200049

-- create 200050 as cote de Prusse
 insert into world_1789 (unitlevel, unit_id, unit_sup_id, id_sup, sourcegeom, sourcecode, geom3857)
(select 2, 200050, 712, 712, 'perdu ?!', ogc_fid, wkb_geometry 
from world_1789_level2_charlotte where id in ('642')  and ogc_fid = 278)
-- Mise à jour des hiérarchies après (le 01 aout 2021) dans world_1789



update world_1789_mixte_30juillet2021_4326 wgs set geom4326 = st_setsrid(st_transform(geom3857 , 4326), 4326)
from world_1789 w 
where  w.id_sup = 944 and  w.unitlevel = 2 and wgs.id_sup = 944 and  wgs.unitlevel = 2 and wgs.id = w.id


select id_sup, st_union(geom3857) 
from world_1789 w where unitlevel  = 2 and id_sup = 712
group by id_sup

update world_1789_mixte_30juillet2021_4326 wgs set geom4326 = st_setsrid(st_transform(ST_UnaryUnion(k.geom) , 4326), 4326)
from (select id_sup, st_union(st_buffer(geom3857, 500)) as geom
from world_1789 w where unitlevel  = 2 and id_sup = 712
group by id_sup) as k
where  wgs.id = 712 
-- Prusse (petit truc pour éviter geom pourrie)


ALTER table ports.world_1789_mixte_30juillet2021_4326 ADD COLUMN centroid4326 geometry
update  ports.world_1789_mixte_30juillet2021_4326 w set centroid4326 = st_centroid(geom4326)



update ports.world_1789 set id_sup = 746 where id=616 and shortname = 'Terre-Neuve'
-- Mise à jour des hiérarchies après (le 01 aout 2021) dans world_1789

-- colonies britanniques d'Amériques
update world_1789_mixte_30juillet2021_4326 wgs set geom4326 = st_setsrid(st_transform(ST_UnaryUnion(k.geom) , 4326), 4326)
from (select id_sup, st_union(geom3857) as geom
from world_1789 w where unitlevel  = 2 and id_sup = 746
group by id_sup) as k
where  wgs.id = 746 

-- Regence de Tunis
update world_1789_mixte_30juillet2021_4326 wgs set geom4326 = st_setsrid(st_transform(ST_UnaryUnion(k.geom) , 4326), 4326)
from (select id_sup, st_union(geom3857) as geom
from world_1789 w where unitlevel  = 2 and id_sup = 741
group by id_sup) as k
where  wgs.id = 741 

-- refaire le dico mixte

select id, unitlevel, shortname, id_sup, st_x(centroid4326), st_y(centroid4326)  
from ports.world_1789_mixte_30juillet2021_4326 

select id, unitlevel, shortname, id_sup , st_x(st_setsrid(st_transform(st_centroid(geom3857) , 4326), 4326)) as longitude, st_y(st_setsrid(st_transform(geom3857 , 4326), 4326)) as latitude
	from ports.world_1789 w
	where (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
	or (w.unitlevel = 1 and w.id != 944 and w.id_sup != 854)
	
	
------------------------------------------------------------------------------
-- Premier Aout 2021
-- Rajout du nom de l'entité dominante
------------------------------------------------------------------------------

ALTER table ports.world_1789_mixte_30juillet2021_4326 ADD COLUMN manual_centroid4326 geometry;

-- Récupérer ceux qu'on a édités à la main. 

update ports.world_1789_mixte_30juillet2021_4326 wgs set manual_centroid4326 = edit.geom 
from cartoweb_world_1789_29juillet2021_mixte4326_centres edit
where edit.id = wgs.id 
-- 170

select * from ports.world_1789_mixte_30juillet2021_4326 wgs where wgs.manual_centroid4326 is null




-- Rajouter dominant
ALTER table ports.world_1789_mixte_30juillet2021_4326 ADD COLUMN dominant text;

update ports.world_1789_mixte_30juillet2021_4326 w set dominant = k.dominant
from (
select distinct w.id, w.shortname, w.id_sup, w0.shortname as dominant
from ports.world_1789 w, ports.world_1789 w0
where w0.unitlevel = 0 and w0.id = w.id_sup and 
-- Requete QGIS qui exporte des fonds mixte
-- (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
-- or 
(w.unitlevel = 1 and w.id != 944 and w.id_sup != 854) AND w.shortname<>w0.shortname
) as k 
where k.id = w.id

update ports.world_1789_mixte_30juillet2021_4326 w set dominant = k.dominant
from (
select distinct w.id, w.shortname, w.id_sup, w0.shortname as dominant
from ports.world_1789 w, ports.world_1789 w0
where w0.unitlevel = 1 and w0.id = w.id_sup and 
-- Requete QGIS qui exporte des fonds mixte
--(w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or 
(w.unitlevel = 2 and w.id_sup = 944 ) 
-- or 
-- (w.unitlevel = 1 and w.id != 944 and w.id_sup != 854) AND w.shortname<>w0.shortname
) as k 
where k.id = w.id



select random(255)
-- Fabriquer une couleur pour chaque entité dominante
ALTER table ports.world_1789_mixte_30juillet2021_4326 ADD COLUMN red int;
ALTER table ports.world_1789_mixte_30juillet2021_4326 ADD COLUMN green int;
ALTER table ports.world_1789_mixte_30juillet2021_4326 ADD COLUMN blue int;

select floor(random()* (255-0 + 1) + 0);

update world_1789_mixte_30juillet2021_4326 w set red = k2.red, green=k2.g, blue=k2.b
from (
	select name, floor(random()* (255-0 + 1) + 0) as red, floor(random()* (255-0 + 1) + 0) as g, floor(random()* (255-0 + 1) + 0) as b
	from (
	select distinct dominant as name from ports.world_1789_mixte_30juillet2021_4326 w
	union 
	select distinct shortname as name from ports.world_1789_mixte_30juillet2021_4326 w where dominant is null
	) as k
) as k2
where k2.name = w.dominant or k2.name = w.shortname

select distinct id_sup, shortname, dominant from  ports.world_1789_mixte_30juillet2021_4326 w
where w.unitlevel = 1 and w.id != 944 and w.id_sup != 854

select * from ports.world_1789_mixte_30juillet2021_4326
select distinct dominant as name from ports.world_1789_mixte_30juillet2021_4326 w
union 
select distinct shortname as name from ports.world_1789_mixte_30juillet2021_4326 w where dominant is null

-- générer une couche geojson avec cela

SELECT id, id_sup, unitlevel, shortname, dominant, red, green, blue, 
st_x(manual_centroid4326) AS longitude, st_y(manual_centroid4326) AS longitude , manual_centroid4326 AS g
FROM ports.world_1789_mixte_30juillet2021_4326



SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(g)::jsonb,
                'properties', to_jsonb(row) - 'id' 
            ) AS feature
            FROM (
            SELECT id, id_sup, unitlevel, shortname, dominant, red, green, blue, 
				st_x(manual_centroid4326) AS longitude, st_y(manual_centroid4326) AS latitude , manual_centroid4326 AS g
				FROM ports.world_1789_mixte_30juillet2021_4326
            ) row) features;
           -- JSON valide. 
-- enregistrer dans un fichier C:\Travail\Dev\portic_humanum\gazetteer\test.json
           

SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(geom4326, 2)::jsonb,
                'properties', to_jsonb(row) - 'geom4326'
            ) AS feature
            FROM (
            SELECT id, id_sup, unitlevel, shortname, dominant, red, green, blue, 
				round((st_x(manual_centroid4326))::numeric, 2) AS long, round(st_y(manual_centroid4326)::numeric, 2) AS lat, geom4326 
				FROM ports.world_1789_mixte_30juillet2021_4326
            ) row) features;
-- enregistré dans un fichier C:\Travail\Dev\portic_humanum\gazetteer\cartoweb_world_1789_29juillet2021_mixte4326_geojson.geojson
           
 SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         ogc_fid,
                'geometry',   ST_AsGeoJSON(geom4326, 2)::jsonb,
                'properties', to_jsonb(row) - 'geom4326' - 'ogc_fid'
            ) AS feature
            FROM (
            SELECT ogc_fid, uhgs_id, toponyme_standard_fr, toponyme_standard_en, state_1789_fr, state_1789_en, 
            	amiraute, province, status, 
				round(st_x(geom)::numeric, 2) AS long, 
				round(st_y(geom)::numeric, 2) AS lat, 
				geom as geom4326 
				FROM ports.port_points pp 
            ) row) features;       
-- enregistré dans un fichier C:\Travail\Dev\portic_humanum\gazetteer\ports_1789_4326.json
           
select REPLACE(latitude , ',', '.' ) from ports.wup2018_f13_capital_cities_csv wfccc   
update ports.wup2018_f13_capital_cities_csv set latitude= REPLACE(latitude , ',', '.' );
update ports.wup2018_f13_capital_cities_csv set longitude= REPLACE(longitude , ',', '.' );

SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         index,
                'geometry',   ST_AsGeoJSON(geom4326, 2)::jsonb,
                'properties', to_jsonb(row) - 'geom4326' - 'index'
            ) AS feature
            FROM (
            SELECT index, country, capital_city, city_code, "Population2018_(thousands)", 
            	st_makepoint(longitude::numeric, latitude::numeric) as geom4326, 
				round(longitude::numeric, 2) AS long, 
				round(latitude::numeric, 2) AS lat 
				FROM ports.wup2018_f13_capital_cities_csv cities 
            ) row) features;  
-- enregistré dans un fichier C:\Travail\Dev\portic_humanum\gazetteer\cities_2018_4326.json

           
SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         identifier,
                'geometry',   ST_AsGeoJSON(geom4326, 2)::jsonb,
                'properties', to_jsonb(row) - 'geom4326' - 'identifier'
            ) AS feature
            FROM (
            SELECT identifier, standardized_name1 as admiralty1, standardized_name2 as  admiralty2, province1, province2, 
				 st_transform(geom, 4326 ) as geom4326
				FROM ports.limites_amirautes la 
            ) row) features;
            
select * from ports.limites_amirautes la  where (nom1 != standardized_name1) or (nom2 != standardized_name2)
/*
- Le Havre  -> Havre
- Les Sables d'Olonnes -> Sables-d''Olonne
- Portbail et Carteret -> Port-Bail
- Eu --> Eu et Tréport
- Le Rochelle --> La Rochelle
*/
/*
update ports.limites_amirautes la  set standardized_name1 = 'Havre' where nom1 = 'Le Havre';
update ports.limites_amirautes la  set standardized_name2 = 'Havre' where nom2 = 'Le Havre';
update ports.limites_amirautes la  set standardized_name1 = 'Sables-d''Olonne' where nom1 = 'Les Sables d''Olonnes';
update ports.limites_amirautes la  set standardized_name2 = 'Sables-d''Olonne' where nom2 = 'Les Sables d''Olonnes';
update ports.limites_amirautes la  set standardized_name1 = 'Port-Bail' where nom1 = 'Portbail et Carteret';
update ports.limites_amirautes la  set standardized_name2 = 'Port-Bail' where nom2 = 'Portbail et Carteret';
update ports.limites_amirautes la  set standardized_name1 = 'Eu et Tréport' where nom1 = 'Eu';
update ports.limites_amirautes la  set standardized_name2 = 'Eu et Tréport' where nom2 = 'Eu';
*/
update ports.limites_amirautes la  set standardized_name1 = 'La Rochelle' where nom1 = 'Le Rochelle';
update ports.limites_amirautes la  set standardized_name2 = 'La Rochelle' where nom2 = 'Le Rochelle';

--- le 3 février 2022 : rajout des traductions des noms de pays et province 

select * from world_1789_mixte_30juillet2021_4326 wmj where unitlevel = 1
select distinct shortname from world_1789_mixte_30juillet2021_4326 wmj where unitlevel = 1
select distinct etat, subunit, etat_en, subunit_en from ports.etats e 

select id, unitlevel , shortname , id_sup, dominant , e.etat , e.subunit , e.etat_en , e.subunit_en 
from world_1789_mixte_30juillet2021_4326 w,
(select distinct etat, subunit, etat_en, subunit_en from ports.etats) as e
where unitlevel = 1 and w.shortname = e.subunit 

alter table world_1789_mixte_30juillet2021_4326 add column shortname_en text;
alter table world_1789_mixte_30juillet2021_4326 add column dominant_en text;

update world_1789_mixte_30juillet2021_4326 w set shortname_en = k.subunit_en, dominant_en=k.etat_en
from 
(
select id, unitlevel , shortname , id_sup, dominant , e.etat , e.subunit , e.etat_en , e.subunit_en 
from world_1789_mixte_30juillet2021_4326 w,
(select distinct etat, subunit, etat_en, subunit_en from ports.etats) as e
where unitlevel = 1 and w.shortname = e.subunit 
) as k
where w.id = k.id
-- 29

update world_1789_mixte_30juillet2021_4326 w set shortname_en = k.etat_en
from 
(select id, unitlevel , shortname , id_sup, dominant , e.etat , e.subunit , e.etat_en , e.subunit_en 
from world_1789_mixte_30juillet2021_4326 w, 
(select distinct etat, subunit, etat_en, subunit_en from ports.etats) as e
where unitlevel = 0 and w.shortname = e.etat) as k
where w.id = k.id -- Etats-Unis d'Amérique / United States


select id, unitlevel , shortname , id_sup, dominant , shortname_en, dominant_en
from world_1789_mixte_30juillet2021_4326 where dominant = 'France' and unitlevel = 2

-- 35 provinces françaises (non traduites en anglais)
update world_1789_mixte_30juillet2021_4326 w set shortname_en = shortname, dominant_en=dominant
where dominant = 'France' and unitlevel = 2 and id_sup = 944

update world_1789_mixte_30juillet2021_4326 w set shortname_en='County of Crehange' where id=948;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Flanders' where id=95;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Brittany' where id=66;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Burgundy' where id=82;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Dauphine' where id=96;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Orleans' where id=80;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Normandy' where id=61;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Ile-de-France' where id=83;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Picardy' where id=84;


update world_1789_mixte_30juillet2021_4326 w set shortname_en = k.etat_en
from 
(select id, unitlevel , shortname , id_sup, dominant , e.etat , e.subunit , e.etat_en , e.subunit_en 
from world_1789_mixte_30juillet2021_4326 w, 
(select distinct etat, subunit, etat_en, subunit_en from ports.etats where subunit is null) as e
where unitlevel = 1 and w.shortname = e.etat and dominant is null) as k
where w.id = k.id -- Maroc, Piemont, etc.
-- 27

select distinct etat_en from etats where etat ilike '%Mecklenbourg%';
update etats set etat_en = trim(etat_en);
update etats set etat_en = 'Mecklenburg' where etat ilike '%Mecklenbourg%';

	
select id, unitlevel , shortname , id_sup, dominant , shortname_en, dominant_en
from world_1789_mixte_30juillet2021_4326
where shortname_en is null and dominant is not null
/*
768	1	colonie suèdoise en Amérique	892	Suède
964	1	Georgia	860	Russie
756	1	Overijssel	925	Provinces-Unies
761	1	Isles de Corse	945	France
764	1	Naples	861	Royaume de Naples
*/
select * from etats where etat like '%Naples%'
select * from etats where etat like '%Piémon%'

update world_1789_mixte_30juillet2021_4326 w set dominant_en = 'Sweden', shortname_en='Swedish colonies in America' where id=768 ;
update world_1789_mixte_30juillet2021_4326 w set dominant_en = 'Russia', shortname_en='Georgia' where id=964;
update world_1789_mixte_30juillet2021_4326 w set dominant_en = 'United Provinces', shortname_en='Overijssel' where id=756;
update world_1789_mixte_30juillet2021_4326 w set dominant_en = 'France', shortname_en='Corsica island' where id=761;
update world_1789_mixte_30juillet2021_4326 w set dominant_en = 'Kingdom of Naples', shortname_en='Naples' where id=764;

select id, unitlevel , shortname , id_sup, dominant , shortname_en, dominant_en
from world_1789_mixte_30juillet2021_4326
where shortname_en is null and id_sup = -1

update world_1789_mixte_30juillet2021_4326 w set shortname_en='part of South America' where id=841;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='part of Asia, Middle-East and Oceania' where id=822;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='part of Africa' where id=827;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Unclaimed territory', shortname='Territoires vierges' where id=976;

select id, unitlevel , shortname , id_sup, dominant , shortname_en, dominant_en
from world_1789_mixte_30juillet2021_4326
where shortname_en is null

update world_1789_mixte_30juillet2021_4326 w set shortname_en='Republic of Genoa' where id=678;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Mexico' where id=839;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Austria' where id=679;

update world_1789_mixte_30juillet2021_4326 w set shortname_en='Schweinfurt', shortname='Schweinfurt' where id=668;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Wittelsbach', shortname='Wittelsbach' where id=724;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Memmingen', shortname='Memmingen' where id=723;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Northwest Territory', shortname='Territoire du Nord-Ouest' where id=966;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Bernburg', shortname='Bernbourg' where id=687;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Hechingen', shortname='Hechingen' where id=728;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Hesse-Darmstadt', shortname='Hesse-Darmstadt' where id=695;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Nassau', shortname='Nassau' where id=697;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Zerbst', shortname='Zerbst' where id=716;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Mühlhausen', shortname='Mühlhausen' where id=686;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Cologne', shortname='Eau de Cologne' where id=718;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Andorra', shortname='Andorre' where id=669;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Trier', shortname='Trèves' where id=711;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Brunswick-Wolfenbuttel', shortname='Brunswick-Wolfenbuttel' where id=708;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Waldeck', shortname='Waldeck' where id=690;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Orange', shortname='Orange' where id=707;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Liege', shortname='Liège' where id=683;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Zweibrücken', shortname='Zweibrücken' where id=699;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Passau', shortname='Passau' where id=684;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Hawaï', shortname='Hawaï' where id=830;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Schwabisches Hall', shortname='Schwäbisches Hall' where id=704;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Lindau', shortname='Lindau' where id=706;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Disputed between West Florida and USA', shortname='Disputé entre la Floride occidentale et les États-Unis' where id=968;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Brandenburg-Ansbach', shortname='Brandebourg-Ansbach' where id=722;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Coburg', shortname='Cobourg' where id=674;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Weimar', shortname='Weimar' where id=701;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Small States', shortname='Petits États' where id=682;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Sigmaringen', shortname='Sigmaringen' where id=673;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Reutlingen', shortname='Reutlingen' where id=705;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Modena', shortname='Modène' where id=729;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Eisenach', shortname='Eisenach' where id=689;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Nuremberg', shortname='Nuremberg' where id=733;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Parma Piacenza', shortname='Parme Plaisance' where id=732;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Swiss Cantons', shortname='Cantons suisses' where id=720;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Hesse-Kassel', shortname='Hesse-Cassel' where id=663;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Hanover', shortname='Hanovre' where id=685;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Sulzbach', shortname='Sulzbach' where id=719;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Ulm', shortname='Ulm' where id=715;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Disputed between Rupert’s land (UK) and USA', shortname='Disputé entre la terre de Rupert (Royaume-Uni) et les États-Unis' where id=969;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Mainz', shortname='Mayence' where id=731;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Republic of Genoa', shortname='République de Gènes' where id=678;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Disputed between Massachusetts and Colony of new Brunswick (UK)', shortname='Différend entre le Massachusetts et la colonie du Nouveau-Brunswick (Royaume-Uni)' where id=967;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Württemberg', shortname='Wurtemberg' where id=688;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Meiningen', shortname='Meiningen' where id=666;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Altenburg', shortname='Altenbourg' where id=662;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Mexico', shortname='Mexique' where id=839;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Brandenburg-Bayreuth', shortname='Brandebourg-Bayreuth' where id=717;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Überlingen', shortname='Überlingen' where id=672;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Kaufbeuren', shortname='Kaufbeuren' where id=727;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Saxe-Hildburghausen', shortname='Saxe-Hildburghausen' where id=710;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Persia', shortname='Perse' where id=735;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Saxony-Poland-Lithuania', shortname='Saxe-Pologne-Lituanie' where id=665;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='San Marino', shortname='Saint Marin' where id=656;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Baden', shortname='Bade' where id=700;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Ratzeburg', shortname='Ratzebourg' where id=677;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Biberach', shortname='Biberach' where id=730;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Dortmund', shortname='Dortmund' where id=680;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Frankfurt', shortname='Francfort' where id=681;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Kamtchatka', shortname='Kamtchatka' where id=965;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Rothenburg', shortname='Rothenburg' where id=726;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Rottweil', shortname='Rottweil' where id=696;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Dessau', shortname='Dessau' where id=691;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Porrentruy', shortname='Porrentruy' where id=725;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Svalbard', shortname='Svalbard' where id=844;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Salzburg', shortname='Salzbourg' where id=692;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Austria', shortname='Autriche' where id=679;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Bavaria', shortname='Bavière' where id=676;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Köthen', shortname='Köthen' where id=675;
update world_1789_mixte_30juillet2021_4326 w set shortname_en='Lippe', shortname='Lippé' where id=667;


SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(geom4326, 2)::jsonb,
                'properties', to_jsonb(row) - 'geom4326'
            ) AS feature
            FROM (
            SELECT id, id_sup, unitlevel, shortname, dominant, shortname_en, dominant_en, 
				round((st_x(manual_centroid4326))::numeric, 2) AS long, round(st_y(manual_centroid4326)::numeric, 2) AS lat, geom4326 
				FROM ports.world_1789_mixte_30juillet2021_4326
            ) row) features;

-- enregistré dans un fichier C:\Travail\Dev\portic_humanum\gazetteer\cartoweb_world_1789_03fevrier2022_mixte4326_geojson.geojson
pgsql2shp -f "C:\Travail\ULR_owncloud\ANR_PORTIC\Data\travels_Z_net\Paths\cartoweb_world_1789_03fevrier2022_mixte4326.shp" -u postgres -P postgres portic_v6 "SELECT id, id_sup, unitlevel, shortname, dominant, shortname_en, dominant_en, round((st_x(manual_centroid4326))::numeric, 2) AS long, round(st_y(manual_centroid4326)::numeric, 2) AS lat, geom4326 FROM ports.world_1789_mixte_30juillet2021_4326;"

-----------------------------------------------------------
-- mise à jour suite à corrections Silvia
-- le 28 fev 2022
-- fichier file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\traduction_nom_de_pays_monde1789%20(1)%20avec%20corrections%20Silvia.xlsx
-----------------------------------------------------------

-- fichier importé dans portic_v7
-- sur lequel on continue les corrections;
SELECT * FROM world_1789_mixte_30juillet2021_4326 WHERE id = 731;
SELECT * FROM world_1789_mixte_30juillet2021_4326 w WHERE id = 737;

update world_1789_mixte_30juillet2021_4326 w set  shortname='Colonies danoises' where id=737;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Colonies espagnoles d''Amérique' where id=743;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Colonies françaises d''Amérique' where id=943;
update world_1789_mixte_30juillet2021_4326 w set  shortname='colonies françaises en Asie' where id=745;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Guyenne' where id=73;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Corse' where id=761;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Colonies britanniques d''Amérique' where id=746;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Colonies portugaises d''Amérique' where id=751;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Georgie' where id=964;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Colonie suèdoise en Amérique' where id=768;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Confédération suisse [ou: Suisse]' where id=720;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Cologne' where id=718;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Maroc' where id=825;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Parme et Plaisance' where id=732;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Principauté de Lampédouse' where id=661;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Empire russe' where id=671;
update world_1789_mixte_30juillet2021_4326 w set  shortname='Saint-Marin' where id=656;

update world_1789_mixte_30juillet2021_4326 w set  dominant='Empire russe', dominant_en='Russian Empire' where id=964;

update world_1789_mixte_30juillet2021_4326 w set  shortname_en='Guyenne' where id=73;
update world_1789_mixte_30juillet2021_4326 w set  shortname_en='Corsica' where id=761;
update world_1789_mixte_30juillet2021_4326 w set  shortname_en='Disputed between Massachusetts and Colony of New Brunswick (UK)' where id=967;


           

