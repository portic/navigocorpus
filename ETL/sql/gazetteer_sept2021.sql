------------------------------------
-- le 20 septembre 2021 
-- Mise à jour de la table ports.gazetteer sur le server
------------------------------------

-- bug du programme car relationType pour un port est mal enregistré (nType) 
-- Il faut refaire la colonne ports.ports_point.relation_state
-- Code SQL récupéré de C:\Travail\Dev\portic_humanum\gazetteer\BuildPorts.py
 
insert into ports.gazetteer (pkid, linked_place_desc) values ('A0172590', '{"@id": "http://gaz.portic.fr/places/?A0172590",
    "type": "Feature",
    "properties":{
      "title": "Royan",
      "ccodes": ["FR"]
    },
    "when": {
      "timespans": [{"start": {"in":"1749"},"end": {"in":"1815"}}]
    },
    "names": [
         
        { "toponym":"Royan",
        "lang":"fr",
        "citations": [
          {"label": "Portic Gazetteer (1787)",
            "@id":"http://anr.portic.fr/"}],
            "when": { "timespans":[{"start":{"in":"1749"}, "end":{"in":"1815"}}]}
        }
    ],
    "types": [
      { "identifier": "aat:300120599", "label": "ports (settlements)"} 
    ],
    "geometry": {
      "type": "GeometryCollection",
      "geometries": [
          { "type": "Point",
              "coordinates": [-1.033333,45.633333],
              "when": {"timespans":[
                  {"start":{"in":"1749"},"end":{"in":"1815"}}]},
              "certainty": "uncertain"
            }
    , { "type": "Point",
              "coordinates": [-1.03153,45.62811],
              "when": {"timespans":[
                {"start":{"in":"1750"},"end":{"in":"2020"}}]},
              "citations": [
                {"label": "Geonames (retrieved november 2019)",
                 "@id":"geoname:2982343"}],
              "certainty": "uncertain"
            }
      ]
    },
    "links": [
      {"type": "exactMatch", "identifier": "http://www.geonames.org/2982343/"}
    ],
    "relations": [
        nType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" :,
        { "relationType": "gvp:broaderPartitive",
          "relationTo": "http://gaz.portic.fr/places/?Marennes",
          "label": "Admiralty Marennes",
          "when":{"timespans":[
            {"start":{"in":"1781"}, "end":{"in":"1785"}}]},
          "citations": [
            {"label": "Archives Nationales, C4 174 à 176. Procès-verbaux d''inspections des ports et amirautés de France par le commissaire Chardon. 1781-1785",
             "@id": "doi:10.4000/books.pur.115293"}],
          "certainty": "less-certain"
        },
        { "relationType": "gvp:broaderPartitive",
          "relationTo": "http://gaz.portic.fr/places/?Saintonge",
          "label": "Province Saintonge",
          "when":{"timespans":[
            {"start":{"in":"1781"}, "end":{"in":"1785"}}]},
          "citations": [
            {"label": "Archives Nationales, C4 174 à 176. Procès-verbaux d''inspections des ports et amirautés de France par le commissaire Chardon. 1781-1785",
             "@id": "doi:10.4000/books.pur.115293"}],
          "certainty": "less-certain"
        }
    ]
}'::jsonb)

select uhgs_id, relation_state, province, amiraute from ports.port_points where uhgs_id = 'A0172590'

{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" :


select uhgs_id, json_agg(arelation) as appartenances
            from
            (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
            case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
            'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
            from (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                from ports.etats 
                where dfrom is not null and dto is not null
                union all
                (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                from ports.etats 
                where dfrom is not null and dto is null
                )
                union all
                (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                from ports.etats 
                where dfrom is null and dto is not null 
                )
                union all
                (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                from ports.etats 
                where dfrom is null and dto is  null 
                )
            ) as k 
            group by uhgs_id, etat, geonameid, tgnid
            ) as k
                    where 'A0172590' = uhgs_id
            group by uhgs_id
            order by uhgs_id
  
select uhgs_id,  json_agg(arelation) as appartenances
            from
            (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
            'http://www.geonames.org/'||geonameid ,
            'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
            from (
                
                select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                from ports.port_points 
                where country2019_name = 'France'
                
                union all
                (
                select  'A0339882' as uhgs_id, 'Hellenic Republic' as etat, 390903 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                )
                union all
                (
                select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                )
            ) as k 
            group by uhgs_id, etat, geonameid
            ) as k
            where 'A0172590' = uhgs_id
            group by uhgs_id
            order by uhgs_id

        select * from myremote_geonames
create or replace VIEW myremote_geonames AS
        SELECT *
            FROM dblink('dbname=geonames user=postgres password=postgres options=-csearch_path=',
                        'select geonameid, name, feature_code, country_code, admin1_code, latitude, longitude from geonames.geonames_nov2019.allcountries a 
                        where feature_class =''A'' and feature_code like ''P%'' or feature_code = ''TERR'' or feature_code like ''Z'' ')
            AS t1(geonameid int, name text, feature_code text, country_code text, admin1_code text, latitude float, longitude float);
       
                   select uhgs_id, etat, geonameid  from etats where etat='Grande-Bretagne'

                   update ports.etats set geonameid = 2635167  where etat = 'Grande-Bretagne';
        update ports.etats set geonameid = 2510769  where etat = 'Espagne';
        update ports.etats set geonameid = 6252001  where etat = 'USA';
        update ports.etats set geonameid = 2750405  where etat = 'Provinces-Unies';
        update ports.etats set geonameid = 2750405  where etat = 'Royaume d''Hollande';
        update ports.etats set geonameid = 798544  where etat = 'Pologne';
        update ports.etats set geonameid = 2215636  where etat = 'Régence de Tripoli';
        update ports.etats set geonameid = 2661886  where etat = 'Suède';
        update ports.etats set geonameid = 2264397  where etat = 'Portugal';
        update ports.etats set geonameid = 1814991  where etat = 'Chine';
        update ports.etats set geonameid = 298795  where etat = 'Empire ottoman';
        update ports.etats set geonameid = 3017382  where etat = 'Empire français';
        update ports.etats set geonameid = 3017382  where etat = 'France';
        update ports.etats set geonameid = 2542007  where etat = 'Empire du Maroc';
        update ports.etats set geonameid = 3175395  where etat = 'Royaume d''Italie';
        update ports.etats set geonameid = 2782113  where etat = 'Autriche';
        update ports.etats set geonameid = 2623032  where etat = 'Danemark';
        update ports.etats set geonameid = 2017370  where etat = 'Russie';
        update ports.etats set geonameid = 2993457  where etat = 'Monaco';
        update ports.etats set geonameid = 2562770  where etat = 'Malte';
        update ports.etats set geonameid = 2993457, tgnid=7005289  where etat = 'Hambourg';
        update ports.etats set geonameid = 2944387  where etat = 'Brême';
        update ports.etats set geonameid = 2872567  where etat = 'Duché de Mecklenbourg';
        update ports.etats set geonameid = 3165361  where etat = 'Toscane';

        update ports.etats set etat = 'Royaume d''Étrurie' where etat= 'Royaume d''Etrurie';
        update ports.etats set geonameid = 3165361, wikipedia='https://fr.wikipedia.org/wiki/%C3%89trurie'  where etat = 'Royaume d''Étrurie';


        update ports.etats set geonameid = 3164600 where etat = 'République de Venise';
        update ports.etats set etat = 'République ligurienne' where etat= 'Republique Ligure';
        update ports.etats set geonameid = 3174725  where etat = 'République ligurienne';

        update ports.etats set geonameid = 3176217, tgnid=7008546  where etat = 'République de Gênes';
        update ports.etats set geonameid = 7577034, tgnid = 7015500, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_de_Raguse'  where etat = 'République de Raguse';
        update ports.etats set geonameid = 3164670, tgnid=7009981, wikipedia='https://fr.wikipedia.org/wiki/%C3%89tats_pontificaux'  where etat = 'Etats pontificaux';
        update ports.etats set geonameid = 2523228, wikipedia='https://fr.wikipedia.org/wiki/Royaume_de_Sardaigne_(1720-1861)'  where etat = 'Royaume de Piémont-Sardaigne';
        update ports.etats set geonameid = 3174976, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_romaine_(1849)'  where etat = 'République romaine';
        update ports.etats set geonameid = 3174529, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_de_Lucques'  where etat = 'République de Lucques';
        update ports.etats set geonameid = 6541646, wikipedia='https://fr.wikipedia.org/wiki/%C3%89trurie'  where etat = 'Principauté de Piombino';
        update ports.etats set geonameid = 3173767, wikipedia='https://fr.wikipedia.org/wiki/Duch%C3%A9_de_Massa_et_Carrare'  where etat = 'Duché de Massa et Carrare';
        update ports.etats set geonameid = 3249071, wikipedia='https://fr.wikipedia.org/wiki/L%C3%BCbeck'  where etat = 'Lubeck';
        update ports.etats set geonameid = 3221095, wikipedia='https://fr.wikipedia.org/wiki/Duch%C3%A9_d%27Oldenbourg'  where etat = 'Duché d''Oldenbourg';
        update ports.etats set geonameid = 6697805, wikipedia='https://fr.wikipedia.org/wiki/R%C3%A9publique_des_Sept-%C3%8Eles' where etat ='Sept Iles';

        update ports.etats set tgnid=7003012, wikipedia='https://fr.wikipedia.org/wiki/Royaume_de_Naples' where etat = 'Royaume de Naples';
        update ports.etats set tgnid = 7016646, wikipedia='https://fr.wikipedia.org/wiki/Duch%C3%A9_de_Courlande'  where etat = 'Duché de Courlande';
        update ports.etats set tgnid = 7016786  where etat = 'Prusse';

select distinct etat  from etats where geonameid is null
         /*
Mecklenbourg
zone maritime
Royaume de Naples
Duché de Courlande
Prusse
multi-Etat
*/

update ports.etats set etat = trim(etat)

update ports.etats set etat = 'Duché de Mecklenbourg' where etat = 'Mecklenbourg'
update ports.etats set etat = 'USA' where etat = 'Etats-Unis d''Amérique' --38
update ports.etats set etat = 'République de Gênes' where etat = 'République de Gènes' --6

Royaume de Naples
        update ports.etats set geonameid = 2872567  where etat = 'Duché de Mecklenbourg';

               update etats e set name_en=g.name, admin1_code=g.admin1_code, latitude=g.latitude, longitude=g.longitude 
        from myremote_geonames g
        where g.geonameid = e.geonameid
        
        
update ports.port_points set relation_state = null


update port_points pp set relation_state = k.appartenances
        from (
        select uhgs_id, json_agg(arelation) as appartenances
            from
            (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
            case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
            'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
            from (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                from ports.etats 
                where dfrom is not null and dto is not null
                union all
                (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                from ports.etats 
                where dfrom is not null and dto is null
                )
                union all
                (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                from ports.etats 
                where dfrom is null and dto is not null 
                )
                union all
                (
                select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                from ports.etats 
                where dfrom is null and dto is  null 
                )
            ) as k 
            group by uhgs_id, etat, geonameid, tgnid
            ) as k
            group by uhgs_id
            order by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id
        
        
        update port_points pp set relation_state = k.appartenances ::text
        from (
        select uhgs_id,  json_agg(arelation) as appartenances
            from
            (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
            'http://www.geonames.org/'||geonameid ,
            'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
            from (
                
                select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                from ports.port_points 
                where relation_state is null and country2019_name = 'France'
                
                union all
                (
                select  'A0339882' as uhgs_id, 'Hellenic Republic' as etat, 390903 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                )
                union all
                (
                select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                )
            ) as k 
            group by uhgs_id, etat, geonameid
            ) as k
            group by uhgs_id
            order by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id
        
        
update port_points set relation_state = substring(relation_state from 2 for length(relation_state)-2)
        
        
select relation_state from ports.port_points pp2  where uhgs_id = 'A0172590'


select uhgs_id, toponyme from ports.port_points  where uhgs_id is not null and latitude is null