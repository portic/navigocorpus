        update    navigo.pointcall set  pointcall_indate =   '1788>11>30'     where   record_id in ('00306428', '00306976');
        update    navigocheck.check_pointcall set  pointcall_indate =   '1788>11>30'     where record_id  in ('00306428', '00306976');


        -- 00297588	1759<02<29	76555 car 1759 n'est pas bissextiel : 1759<02<29 est impossible   
        update    navigo.pointcall set  pointcall_indate =   '1759<02<28'     where record_id  = '00297588';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1759<02<28'     where record_id  = '00297588';

        -- 00302494	1748=31=12	81393
        update    navigo.pointcall set  pointcall_indate =   '1748=12=31'     where record_id  = '00302494';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1748=12=31'     where record_id  = '00302494';

        -- 00302493		1748<31<12!
        update    navigo.pointcall set  pointcall_outdate =   '1748<12<31!'     where record_id  = '00302493';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1748<12<31!'     where record_id  = '00302493';

        -- 00313825	1759=11=31	92616	1759=11=31
        update    navigo.pointcall set  pointcall_indate =   '1759=11=30'     where record_id  = '00313825';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1759=11=30'     where record_id  = '00313825';

        -- 00365122	1789>16>07!	139226	1789=16=07
        update    navigo.pointcall set  pointcall_indate =   '1789>07>16!'      where record_id  = '00365122';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1789>07>16!'      where record_id  = '00365122';

        -- 00312165 1749=30=29 
        update    navigo.pointcall set  pointcall_indate =   '1749=06=29'      where record_id  = '00312165';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1749=06=29'      where record_id  = '00312165';

        -- 00154851	1787>17>04	35727
        update    navigo.pointcall set  pointcall_indate =   '1787>04>17'      where record_id  = '00154851';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1787>04>17'      where record_id  = '00154851';

        -- 00327941	1799=11=[$]	31/10/1799
        update    navigo.pointcall set  pointcall_outdate =   '1799=10=31'     where record_id  = '00327941';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1799=10=31'     where record_id  = '00327941';

        -- 00301528	1748=22=10	10/10/1749
        update    navigo.pointcall set  pointcall_outdate =   '1748=10=22'     where record_id  = '00301528';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1748=10=22'     where record_id  = '00301528';

        -- 00331147	1779=11=31	01/12/1779
        update    navigo.pointcall set  pointcall_outdate =   '1779=12=01'     where record_id  = '00331147';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1779=12=01'     where record_id  = '00331147';

        -- 00297588	1759=02=29	01/03/1759
        update    navigo.pointcall set  pointcall_outdate =   '1759=03=01'     where record_id  = '00297588';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1759=03=01'     where record_id  = '00297588';

		update navigo.pointcall set pointcall_outdate = '1787<02<25!' where record_id = '00143961' and pointcall_outdate like '1878%';
		update navigocheck.check_pointcall set pointcall_outdate = '1787<02<25!' where record_id = '00143961' and pointcall_outdate like '1878%';
	
		update navigo.pointcall set pointcall_outdate = '1749=10=04' where record_id = '00320922' and pointcall_outdate = '1479=10=04';
		update navigocheck.check_pointcall set pointcall_outdate = '1749=10=04' where record_id = '00320922' and pointcall_outdate = '1479=10=04';
       
		update navigo.pointcall set net_route_marker  = 'A' where net_route_marker = 'Y';
		update navigocheck.check_pointcall set net_route_marker  = 'A' where net_route_marker = 'Y';

       
        update navigo.pointcall set pointcall_status = 'PC-RS'
        where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
        '00364778', '00364786', '00364788', '00364787', '00364794', '00364796'); 

        update navigocheck.check_pointcall set pointcall_status = 'PC-RS'
        where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
        '00364778', '00364786', '00364788', '00364787', '00364794', '00364796');

        update navigocheck.check_pointcall set pointcall_status = 'FC-RS', pointcall_rank = '4.0' where record_id = '00364213';
        update navigo.pointcall set pointcall_status = 'FC-RS', pointcall_rank = '4.0' where record_id = '00364213';


-- déportées dans le code
/*       
	delete from navigoviz."source" s 
	where data_block_local_id in (
		select data_block_local_id from navigoviz."source" s 
	            group by data_block_local_id
	            having count(data_block_local_id) > 1
	    )
	and s.documentary_unit_id is null;
	
	delete from navigoviz."source" s 
	where data_block_local_id = '00104863' and component = 'ANF, G5-115A/La Hougue';
	
	delete from navigoviz."source" s 
	where data_block_local_id = '00149228' and source = 'ANF, G5-62/rafraichi';
*/

		update navigo.pointcall p set ship_tonnage = null where data_block_local_id = '00283632';
		update navigocheck.check_pointcall  p set ship_tonnage = null where data_block_local_id = '00283632';
		
		update navigo.pointcall p set ship_tonnage = 196.5 where data_block_local_id = '00148691';
		update navigocheck.check_pointcall  p set ship_tonnage = 196.5 where data_block_local_id = '00148691';
