﻿--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
-- Christine Plumejeaud-Perreau, U.M.R 7266 LIENSS 
-- date de création : 14 février 2019
-- Date de dernière mise à jour : 07 mars 2019
-- Licence : GPL v3
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------


drop table navigo.navigo_qualification cascade
create table navigo.navigo_qualification (
navigo_table text,
navigo_field text,
refpkid int,
documentary_unit_id int,
record_id int,
pointcall_id int,
original_value text,
final_value text,
flag1 int,
flag2 text,
link json,
method text,
run_date date,
validation_date date,
comment text)

Alter TABLE navigo.navigo_qualification  add CONSTRAINT unique_pk PRIMARY KEY (navigo_table, navigo_field, refpkid)

drop table      navigo.join_qualif_viz

create table navigo.join_qualif_viz (
navigo_table text, 
navigo_field text, 
navigo_refpkid int,
navigoviz_table text,
navigoviz_field text, 
navigoviz_refpkid int)

Alter TABLE navigo.join_qualif_viz  add CONSTRAINT join_qualif_viz_fk FOREIGN KEY (navigo_table, navigo_field, navigo_refpkid)
      REFERENCES navigo.navigo_qualification (navigo_table, navigo_field, refpkid) MATCH SIMPLE
     ON UPDATE NO ACTION ON DELETE CASCADE;


-- DROP FUNCTION navigo.test_double_type(tested_value VARCHAR) ;
CREATE OR REPLACE FUNCTION navigo.test_double_type (tested_value VARCHAR) RETURNS boolean AS 
$BODY$
	begin
		EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
		return true;

	exception when others then 
	    raise notice '% %', SQLERRM, SQLSTATE;
	    return false;
	end;
$BODY$ LANGUAGE plpgsql VOLATILE;


-- DROP FUNCTION navigo.test_double_type(tested_value VARCHAR) ;
CREATE OR REPLACE FUNCTION navigo.test_int_type (tested_value VARCHAR) RETURNS boolean AS 
$BODY$
	begin
		EXECUTE 'select '||quote_literal(tested_value)||'::int'; 	
		return true;

	exception when others then 
	    raise notice '% %', SQLERRM, SQLSTATE;
	    return false;
	end;
$BODY$ LANGUAGE plpgsql VOLATILE;

select regexp_replace(regexp_replace('[du bon jambon]', '\[', ''), '\]', '')
select navigo.rm_parentheses_crochets('[(du bon jambon)]')


select regexp_replace(regexp_replace('[(du bon jambon)]', '\[', ''), '\]', '')




------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- EXPORT des controles
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- Une ligne pour les erreurs sur une champs d'une ligne du fichier (donc si 2 champs foirent sur la meme ligne, on a en sorti 2 lignes)
select * from navigo.navigo_qualification


-- Une ligne regroupant les erreurs sur la même ligne du fichier
select navigo_table, refpkid,  array_agg(navigo_field) as fields,  array_agg(flag1) as flag1s, array_agg(flag2) as flag2s, array_agg(original_value) as original_values, array_agg(final_value) as final_values
from navigo.navigo_qualification
group by navigo_table, refpkid
--having count(distinct navigo_field) > 1
order by navigo_table, refpkid

-- Une ligne regroupant les erreurs>=2 sur la même ligne du fichier
select navigo_table, refpkid,  array_agg(navigo_field) as fields,  array_agg(flag1) as flag1s, array_agg(flag2) as flag2s, array_agg(original_value) as original_values, array_agg(final_value) as final_values
from navigo.navigo_qualification
group by navigo_table, refpkid
having count(distinct navigo_field) > 1
order by navigo_table, refpkid

-- Le fichier source avec son numéro
select * from navigo.geo_general

--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
-- geo_general
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------

/*
create table geo_general (
	pkid serial primary key,
	Documentary_unit_id     text,
	Pointcall_name     text,
	Pointcall_UHGS_id     text,
	geo_general__lat     text,
	geo_general__long     text,
	shippingarea__e1_name     text);

drop table if exists geo_general cascade;

insert into geo_general(Documentary_unit_id, Pointcall_name, Pointcall_UHGS_id, LAT, LONG, E1_name) values 
('00000001',	'Goletta',	'A0012229',	'36.818056',	'10.305',	'MED-SIST'),
('00000001',	'Malta',	'A0036549',	'35.899722',	'14.514722',	'MED-SIST')

insert into geo_general(Documentary_unit_id, Pointcall_name, Pointcall_UHGS_id, LAT, LONG, E1_name) values 
('00000001',	'Goletta',	'A0012229',	'36.818056',	'10.305',	'MED-SIST'),
('00000001',	null,	'A0036549',	'35.899722',	'14.514722',	'MED-SIST')

select count(*) from navigo.geo_general
select max(pkid) from navigo.geo_general

select * from navigo.geo_general where pkid > 184900
alter table navigo.geo_general alter column lat type float using lat::float
alter table navigo.geo_general alter column long type float using long::float
select * from navigo.geo_general  where geo_general__long = 'A0151321-0.683333'
-- "Saint Savinien";"A0147182";45.883333;"A0151321-0.683333";"ACE-ROCH"

*/


-- cette partie risque de changer suite aux résultats des controles.
create schema navigocheck;

-- alter table navigoviz.geo_general set schema navigocheck; 
create table navigocheck.geo_general 
(	pkid serial primary key,
	Pointcall_name     text,
	Pointcall_UHGS_id     text,
	latitude     float,
	longitude     float,
	shippingarea__e1_name     text,
	point geometry);

insert into navigocheck.geo_general  (Pointcall_name, Pointcall_UHGS_id, latitude, longitude, shippingarea__e1_name, point) 
(select distinct Pointcall_name, Pointcall_UHGS_id, regexp_replace(geo_general__lat, ',', '.')::float as latitude,
regexp_replace(geo_general__long, ',', '.')::float as longitude, shippingarea__e1_name,
st_setsrid(st_makepoint(regexp_replace(geo_general__long, ',', '.')::float, regexp_replace(geo_general__lat, ',', '.')::float), 4326)
from navigo.geo_general 
where geo_general__long <> 'A0151321-0.683333')

-- Query returned successfully: 3811 rows affected, 1.0 secs execution time.
select count(*) from navigocheck.geo_general
-- 3811

select count(*) from navigo.geo_general
-- 185646

select count(*) from navigoviz.geo_general
-- 3012

-- Trouver toutes les appellations identiques d'un meme point_call
select  Pointcall_UHGS_id, array_agg(Pointcall_name),  latitude, longitude
from navigocheck.geo_general
group by Pointcall_UHGS_id, latitude, longitude
--having count(Pointcall_name) > 2 
order by Pointcall_UHGS_id




--------------
-- controle 1 : le type de geo_general__long est de type chaine de character et pas réel
--------------



select test_double_type('456.23')
select test_double_type('456,23')
select test_double_type('A0151321-0.683333')

select * from navigo.geo_general  where geo_general__long is not null and navigo.test_double_type(geo_general__long) is false 
select * from navigo.geo_general  where geo_general__lat is not null and navigo.test_double_type(geo_general__lat) is false 

select * from navigo.geo_general  where geo_general__long is not null and navigo.test_double_type(geo_general__long) is false 
insert into navigo.navigo_qualification 
(select 'geo_general', 'geo_general__long', pkid, documentary_unit_id::int, null, null, geo_general__long, regexp_replace(geo_general__long, ',', '.') as final_value, 4, '4.01 Bad type - real number is expected', null, '001 Vérification de type', now(), null, null
from navigo.geo_general  where geo_general__long is not null and navigo.test_double_type(geo_general__long) is false 
)

--------------
-- controle 2 : geo_general__lat AND geo_general__long are missing
--------------
select * from navigo.geo_general  where geo_general__long is  null and geo_general__lat is not null 
select * from navigo.geo_general  where geo_general__lat is  null and geo_general__long is not null 


insert into navigo.navigo_qualification 
(select 'geo_general', 'geo_general__long | geo_general__lat', pkid, documentary_unit_id::int, null, null, null as original_value, null as final_value, 9, '9.00 Missing field - real number is expected', null, '000 Vérification de nullité', now(), null, null
from navigo.geo_general  where geo_general__long is null  and geo_general__lat is null 
)

--------------
-- controle 3 : shippingarea__e1_name is missing
--------------
select * from navigo.geo_general  where shippingarea__e1_name is  null and geo_general__lat is not null and geo_general__long is not null
insert into navigo.navigo_qualification 
(select 'geo_general', 'shippingarea__e1_name', pkid, documentary_unit_id::int, null, null, null as original_value, null as final_value, 9, '9.00 Missing field - string is expected', null, '000 Vérification de nullité', now(), null, null
from navigo.geo_general  where shippingarea__e1_name is  null and geo_general__lat is not null and geo_general__long is not null
)


--------------
-- controle 4 : shippingarea__e1_name est incohérent - aucun
--------------
select * 
from navigo.geo_general  g,
(select Pointcall_UHGS_id,  array_agg(Pointcall_name),  array_agg(shippingarea__e1_name)
from navigoviz.geo_general
group by Pointcall_UHGS_id 
-- shippingarea__e1_name 
having count(distinct(shippingarea__e1_name)) > 1
order by Pointcall_UHGS_id) as s
where g.Pointcall_UHGS_id=s.Pointcall_UHGS_id 


--------------
-- controle 5: 2 ou plus Pointcall_UHGS_id pour un même Pointcall, longitude, latitude (doublon)
--------------

select Pointcall_name, array_agg(Pointcall_UHGS_id),  latitude, longitude
from navigocheck.geo_general
group by Pointcall_name, latitude, longitude
having count(distinct Pointcall_UHGS_id) > 1 
order by Pointcall_name

select * from navigoviz.geo_general where Pointcall_name='Gibraltar'

"Gibraltar";"{A0354005,A0354005,A0354078}";36.133333;-5.35
"Guernesey";"{A1945598,A1945598,A1963974}";49.45;-2.6

select * 
from navigo.geo_general  g,
(select Pointcall_name, array_agg(Pointcall_UHGS_id),  latitude, longitude, array_agg(shippingarea__e1_name)
from navigocheck.geo_general
group by Pointcall_name, latitude, longitude
having count(distinct Pointcall_UHGS_id) > 1 
order by Pointcall_name ) as s
where g.pointcall_name=s.Pointcall_name and g.geo_general__lat=s.latitude::text and g.geo_general__long=s.longitude::text

insert into navigo.navigo_qualification 
(select 'geo_general', 'Pointcall_UHGS_id', g.pkid, documentary_unit_id::int, null, null, Pointcall_UHGS_id as original_value, candidats as final_value, 5, '5.01 Doublon - unique Pointcall_UHGS_id is expected for this Pointcall_name, latitude, longitude', 
to_json(candidats), '003 Vérification d''unicité des Pointcall_UHGS_id', now(), null, null
from navigo.geo_general  g,
(select Pointcall_name, array_agg(Pointcall_UHGS_id) as candidats,  latitude, longitude, array_agg(shippingarea__e1_name)
from navigocheck.geo_general
group by Pointcall_name, latitude, longitude
having count(distinct Pointcall_UHGS_id) > 1 
order by Pointcall_name ) as s
where g.pointcall_name=s.Pointcall_name and g.geo_general__lat=s.latitude::text and g.geo_general__long=s.longitude::text
)



-- cette partie exploite les résultats des controles.
create table navigoviz.geo_general 
(	pkid serial primary key,
	Pointcall_name     text,
	Pointcall_UHGS_id     text,
	latitude     float,
	longitude     float,
	shippingarea__e1_name     text,
	point geometry);

insert into navigoviz.geo_general  (Pointcall_name, Pointcall_UHGS_id, latitude, longitude, shippingarea__e1_name, point) 
(select distinct Pointcall_name, Pointcall_UHGS_id, geo_general__lat::float as latitude, geo_general__long::float as longitude, shippingarea__e1_name, st_setsrid(st_makepoint(geo_general__long::float, geo_general__lat::float), 4326)
from navigo.geo_general 
where pkid not in (select refpkid from  navigo_qualification where navigo_table = 'geo_general'))
-- 3012 lignes


-- export le 26 fev 2019 pour shapefile pour intégrer les amirautés et provinces
select pointcall_uhgs_id, latitude, longitude, null as amiraute, null as province, (array_agg(distinct pointcall_name))[1] as topo1,
(array_agg(distinct pointcall_name))[2] as topo2, (array_agg(distinct pointcall_name))[3] as topo3, (array_agg(distinct pointcall_name))[4] as topo4,
(array_agg(distinct pointcall_name))[5] as topo5, (array_agg(distinct pointcall_name))[6] as topo6, (array_agg(distinct pointcall_name))[7] as topo7,
array_agg(distinct pointcall_name) as toustopos, (array_agg(distinct shippingarea__e1_name))[1] as shippingarea1,
(array_agg(distinct shippingarea__e1_name))[1] as shippingarea2, (array_agg(distinct shippingarea__e1_name))[1] as shippingarea3,
array_agg(distinct shippingarea__e1_name) as tousshippingarea
from navigocheck.geo_general
group by pointcall_uhgs_id, latitude, longitude

--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
-- pointcall
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------

SELECT column_name
   FROM   INFORMATION_SCHEMA.COLUMNS
   WHERE  TABLE_SCHEMA='navigo' and TABLE_NAME = 'pointcall'
   order by ordinal_position
   
	 pkid serial primary key,
	Ship_name     text,
	Datablock_leader__Captain_name     text,
	Datablock_leader__Ship_tonnage     text,
	Datablock_leader__Ship_homeport     text,
	Last_point__Pointcall_name     text,
	Last_point__Pointcall_UHGS_id     text,
	Datablock_leader__Pointcall_outdate     text,
	Documentary_unit_id     text,
	Captain_age     text,
	Captain_alternate_id     text,
	Captain_birthplace     text,
	Captain_birthplace_id     text,
	Captain_citizenship     text,
	Captain_citizenship_id     text,
	Captain_last_name     text,
	Captain_local_id     text,
	Captain_name     text,
	Captain_name_sort     text,
	Captain_origin     text,
	Captain_origin_id     text,
	Captain_status     text,
	Convoi_entrant_id     text,
	Convoi_id_calcul�     text,
	Data_block_leader_marker     text,
	Data_block_local_id     text,
	Documentary_unit_id_merging     text,
	Documentary_unit_id_original     text,
	Documentary_unit_marker     text,
	Guns     text,
	Health_data     text,
	Net_route_marker     text,
	Next_point_rank     text,
	Observation_date     text,
	Observation_pointcall_outdate     text,
	Pointcall_action     text,
	Pointcall_calculated_year     text,
	Pointcall_date     text,
	Pointcall_date_sort     text,
	Pointcall_function     text,
	Pointcall_indate     text,
	Pointcall_indate_date     text,
	Pointcall_indate_relativity     text,
	Pointcall_marker     text,
	Pointcall_name     text,
	Pointcall_outdate     text,
	Pointcall_outdate_date     text,
	Pointcall_outdate_relativity     text,
	Pointcall_rank     text,
	Pointcall_status     text,
	Pointcall_stay_duration     text,
	Pointcall_stay_duration_calculated     text,
	Pointcall_stay_duration_declared     text,
	Pointcall_UHGS_id     text,
	Previous_pointcall_rank     text,
	Query_route     text,
	Record_creation_date     text,
	Record_creation_signature     text,
	Record_id     text,
	Record_id_merging     text,
	Record_last_change_date     text,
	Record_last_change_signature     text,
	Remarks     text,
	Route_calculated_duration     text,
	Route_declared_duration     text,
	Route_duration     text,
	Route_points_number     text,
	Selected_record_rank     text,
	Selected_records_number     text,
	Ship_class     text,
	Ship_flag     text,
	Ship_flag_id     text,
	Ship_homeport     text,
	Ship_homeport_id     text,
	Ship_homeport_sort     text,
	Ship_increw     text,
	Ship_local_id     text,
	Ship_name_sort     text,
	Ship_outcrew     text,
	Ship_tonnage     text,
	Ship_tonnage_sort     text,
	Ship_tonnage_U     text,
	Source     text,
	Stage_calculated_duration_next     text,
	Stage_calculated_duration_previous     text,
	Stage_declared_duration_previous     text,
	Stage_duration_previous     text,
	Stage_id     text,
	Stage_id_link     text,
	Stage_id_link_reversed     text,
	Stage_identifier_length     text,
	Stage_relooked     text,
	Stage_relooked_consistency     text,
	Stage_speed     text,
	Stage_speed_relooked     text,
	Task     text,
	Component_description__Component_id     text,
	Component_description__Component_short_title     text,
	Component_description__Component_source     text,
	Component_description__Suite_id     text,
	Component_description__Task     text);

insert into navigo.pointcall ( Ship_name,Datablock_leader__Captain_name,Datablock_leader__Ship_tonnage,Datablock_leader__Ship_homeport,Last_point__Pointcall_name,Last_point__Pointcall_UHGS_id,Datablock_leader__Pointcall_outdate,Documentary_unit_id,Captain_age,Captain_alternate_id,Captain_birthplace,Captain_birthplace_id,Captain_citizenship,Captain_citizenship_id,Captain_last_name,Captain_local_id,Captain_name,Captain_name_sort,Captain_origin,Captain_origin_id,Captain_status,Convoi_entrant_id,Convoi_id_calcul�,Data_block_leader_marker,Data_block_local_id,Documentary_unit_id_merging,Documentary_unit_id_original,Documentary_unit_marker,Guns,Health_data,Net_route_marker,Next_point_rank,Observation_date,Observation_pointcall_outdate,Pointcall_action,Pointcall_calculated_year,Pointcall_date,Pointcall_date_sort,Pointcall_function,Pointcall_indate,Pointcall_indate_date,Pointcall_indate_relativity,Pointcall_marker,Pointcall_name,Pointcall_outdate,Pointcall_outdate_date,Pointcall_outdate_relativity,Pointcall_rank,Pointcall_status,Pointcall_stay_duration,Pointcall_stay_duration_calculated,Pointcall_stay_duration_declared,Pointcall_UHGS_id,Previous_pointcall_rank,Query_route,Record_creation_date,Record_creation_signature,Record_id,Record_id_merging,Record_last_change_date,Record_last_change_signature,Remarks,Route_calculated_duration,Route_declared_duration,Route_duration,Route_points_number,Selected_record_rank,Selected_records_number,Ship_class,Ship_flag,Ship_flag_id,Ship_homeport,Ship_homeport_id,Ship_homeport_sort,Ship_increw,Ship_local_id,Ship_name_sort,Ship_outcrew,Ship_tonnage,Ship_tonnage_sort,Ship_tonnage_U,Source,Stage_calculated_duration_next,Stage_calculated_duration_previous,Stage_declared_duration_previous,Stage_duration_previous,Stage_id,Stage_id_link,Stage_id_link_reversed,Stage_identifier_length,Stage_relooked,Stage_relooked_consistency,Stage_speed,Stage_speed_relooked,Task,Component_description__Component_id,Component_description__Component_short_title,Component_description__Component_source,Component_description__Suite_id,Component_description__Task) values 
	('Hornet','Dent, John H.',null,null,'Cagliari','A0250475','1806=12=12','00000001',null,'00004205',null,null,null,null,' John H','00010392','Dent, John H.','Dent, John H',null,null,'Commander',null,null,'A','00000001',null,'00000001',null,'18',null,'A','3.0','1806=12=08',null,'In-out','1806','1806=12=08','18061208b','O','1806=12=08','08/12/1806',null,null,'Goletta','1806=12=12','12/12/1806',null,'2.0','PC-RC','4','4',null,'A0012229','1',null,'39994.0','jpdedieu','00000001',null,'43347.0','Parpaillou',null,null,null,null,'3.0','11.0','2.0','Brig of the USA','USA',null,null,null,null,'160','0002994N','Hornet',null,null,null,null,'NARA, RG84, Tunis consulate, vol. 51/',null,null,null,null,'A0012229A0250475','A0012229A0250475','A0012229A0250475
A0250475A0012229','16.0','A0012229A0250475','1',null,null,null,null,'US ships, Tunisia (1806-1867)','NARA, RG84, Tunis consulate, vol. 51/','00000001',null);

select Datablock_leader__Ship_homeport from  navigo.pointcall limit 10


select * from navigo.pointcall where captain_name like '%Blair, Victor%' order by pointcall_rank

--------------------------------------------------------------------------------------------
-- capitaine + documentary_unit_id
--------------------------------------------------------------------------------------------

select 
captain_name, captain_last_name, captain_age, captain_status, captain_local_id, captain_alternate_id, 
captain_citizenship, captain_citizenship_id, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id,
datablock_leader__captain_name
from navigo.pointcall

-- "00010392";"00004205"
select captain_name, captain_last_name, captain_age, captain_status, captain_local_id, captain_alternate_id, 
captain_citizenship, captain_citizenship_id, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id,
datablock_leader__captain_name from  navigo.pointcall
where 
-- captain_local_id = '00010392'
captain_alternate_id = '00004205'

select regexp_replace(regexp_replace(captain_local_id, '\(', ''), '\)', '')::int from  navigo.pointcall
-- entrée invalide pour l'entier : « 0019079N »
select regexp_replace(regexp_replace(captain_alternate_id, '\(', ''), '\)', '')::int from  navigo.pointcall
-- entrée invalide pour l'entier : « 0004382N »

select distinct captain_local_id 
from  navigo.pointcall where captain_local_id is not null 
and test_int_type(regexp_replace(regexp_replace(captain_local_id, '\(', ''), '\)', '')) is false 
-- 40 lignes

select count(*)
from  navigo.pointcall where captain_local_id is not null 
and test_int_type(regexp_replace(regexp_replace(captain_local_id, '\(', ''), '\)', '')) is true 
-- 110561 lignes

select count(*)
from  navigo.pointcall where captain_local_id is not  null 
-- 75282 lignes nulles

select distinct captain_alternate_id 
from  navigo.pointcall where captain_alternate_id is not null 
and test_int_type(regexp_replace(regexp_replace(captain_local_id, '\(', ''), '\)', '')) is false 
-- 3 lignes
/*
"00002581"
"00005348"
"00001574"
*/

select count(*)
from  navigo.pointcall where captain_alternate_id is not null 
and test_int_type(regexp_replace(regexp_replace(captain_alternate_id, '\(', ''), '\)', '')) is true 
-- 13598 lignes

select count(*)
from  navigo.pointcall where captain_alternate_id is  null 
-- 172294 lignes nulles

select count(*) from  navigo.pointcall
where 
datablock_leader__captain_name is not null
captain_name, captain_last_name, captain_age, captain_status, captain_local_id, captain_alternate_id, 
captain_citizenship, captain_citizenship_id, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id,
datablock_leader__captain_name
-- captain_local_id = '00010392'
captain_alternate_id = '00004205'

select count(*) from  navigo.pointcall where captain_birthplace is not null 
select distinct captain_birthplace from  navigocheck.check_pointcall limit 20

-- plus d'info sur captain_origin... "(Honfleur) Danois" ou "Ydra grec" : compile les info sur birthplace + citizenship
select captain_name, captain_last_name, captain_age, captain_status, captain_local_id, captain_alternate_id, 
captain_citizenship, captain_citizenship_id, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id,
datablock_leader__captain_name from  navigo.pointcall
where
captain_birthplace <> captain_origin

/*
select regexp_replace('(Josso, François)', '\(.*\)', '')
-- https://stackoverflow.com/questions/35986472/postgresql-regexp-replace-to-remove-bracketsexample
SELECT regexp_replace('420 CONSUMER SQUARE (PET SMART PARKING LOT)', '\(.*\)', '');
SELECT regexp_replace('420 CONSUMER SQUARE (PET SMART PARKING LOT)', '\\(.*?\\)$', '');
SELECT regexp_replace('(Josso, François)', '\(.*?\)', '');
*/
SELECT regexp_replace(regexp_replace('(Josso, François)', '\(', ''), '\)', '');


-- captain_name parfois différent de datablock_leader__captain_name
-- "Audaine, François";"(Andaine, François)"
-- "L' Hyoudre, Thomas";"(Lehyoudre, Thomas)"
select datablock_leader__captain_name, captain_name, captain_last_name, captain_age, captain_status, captain_local_id, captain_alternate_id, 
captain_citizenship, captain_citizenship_id, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id,
datablock_leader__captain_name from  navigo.pointcall
where
regexp_replace(regexp_replace(captain_name, '\(', ''), '\)', '')  <> datablock_leader__captain_name


-- test de nullité sur Captain_birthplace_id et captain_origin_id
select datablock_leader__captain_name, captain_name, captain_last_name, captain_age, captain_status, captain_local_id, captain_alternate_id, 
captain_citizenship, captain_citizenship_id, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id
from  navigo.pointcall
where Captain_birthplace_id is not null

select captain_status,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by captain_status order by d desc

select captain_citizenship,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by captain_citizenship order by d desc


--------------------------------------------------------------------------------------------
-- trajectoire, séjour et ordres de passage dans les ports + documentary_unit_id
--------------------------------------------------------------------------------------------

select
record_id,  
stage_id, 
pointcall_stay_duration, pointcall_stay_duration_calculated, Pointcall_stay_duration_declared, 
stage_declared_duration_previous, stage_duration_previous, 
query_route, route_points_number, selected_record_rank, Previous_pointcall_rank, pointcall_rank, next_point_rank ,
Route_calculated_duration, Route_duration, Route_declared_duration,
pointcall_name, pointcall_uhgs_id, data_block_leader_marker, net_route_marker,
pointcall_status, 
pointcall_outdate, pointcall_outdate_date, datablock_leader__pointcall_outdate,
pointcall_indate, pointcall_indate_date, Pointcall_indate_relativity, 
observation_date, Observation_pointcall_outdate, Pointcall_calculated_year, Pointcall_date_sort,Pointcall_date,
pointcall_action, health_data
from navigo.pointcall

select count(*) from navigo.pointcall where query_route is not null
-- vide
select count(*) from navigo.pointcall where Route_calculated_duration is not null
31633
select count(*) from navigo.pointcall where Route_duration is not null
30062
select count(*) from navigo.pointcall where Route_declared_duration is not null
274

select count(*) from navigo.pointcall where Route_points_number  is not null



select distinct Route_duration from navigocheck.check_pointcall limit 20
select distinct Route_calculated_duration from navigocheck.check_pointcall limit 20

select count(*) from navigo.pointcall where Pointcall_indate_relativity is not null
-- 9
select count(*) from navigo.pointcall where Pointcall_outdate_relativity is not null
-- 20

select count(*) from navigo.pointcall where Route_declared_duration is not null
-- 274
select count(*) from navigo.pointcall where Route_points_number is not null
-- 185646

select count(*) from navigo.pointcall where datablock_leader__pointcall_outdate = pointcall_outdate
-- 56432
select count(*) from navigo.pointcall where datablock_leader__pointcall_outdate is not null and datablock_leader__pointcall_outdate<> pointcall_outdate
-- 12076
select * from navigo.pointcall where datablock_leader__pointcall_outdate is not null and datablock_leader__pointcall_outdate<> pointcall_outdate limit 20

select count(*) from navigo.pointcall where Selected_records_number is not null
-- 185646

select count(*) from navigo.pointcall where stage_speed is not null
select distinct stage_speed from navigo.pointcall where stage_speed is not null

-- 11967
select count(*) from navigo.pointcall where Stage_speed_relooked is not null
-- 717

select count(*) from navigo.pointcall where Stage_relooked is not null
-- 48920
select count(*) from navigo.pointcall where Stage_relooked_consistency is not null
-- 183567

select count(*) from navigo.pointcall where stage_id is not null
select distinct stage_id, pointcall_Uhgs_id from navigo.pointcall where stage_id is not null

Stage_speed


select count(*) from  navigo.pointcall where health_data is not null 
select distinct health_data from  navigocheck.check_pointcall limit 20



select stage_id, stage_id_link, stage_id_link_reversed, Stage_relooked, Stage_relooked_consistency, stage_speed, Stage_speed_relooked from navigo.pointcall 
where Stage_relooked is not null limit 20


select record_id, stage_id, stage_id_link, stage_id_link_reversed, Stage_relooked, Stage_relooked_consistency, stage_speed, Stage_speed_relooked,
Stage_calculated_duration_next, Stage_calculated_duration_previous, Stage_declared_duration_previous, Stage_duration_previous
 from navigo.pointcall 
where  Stage_calculated_duration_next is not null
limit 20  

select distinct Stage_identifier_length from navigo.pointcall where  Stage_identifier_length is not null
/*
"16.0"
"11.0"
"8.0"
"24.0"
"3.0"
*/
select count(*) from navigo.pointcall where  Stage_identifier_length is not null
-- 183552
select count(*) from navigo.pointcall where  Stage_identifier_length = ' '

select count(*) from navigo.pointcall where  Stage_calculated_duration_next is not null
select count(*) from navigo.pointcall where  Stage_calculated_duration_previous is not null
select count(*) from navigo.pointcall where  Stage_declared_duration_previous is not null

select count(*) from navigo.pointcall where  Stage_duration_previous is not null

select count(*) from navigo.pointcall where  Observation_pointcall_outdate is not null

select count(*) from navigo.pointcall where  Observation_date is not null
43027
select count(*) from navigo.pointcall where Observation_date is not null and 
(Observation_date=pointcall_indate or Observation_date=pointcall_outdate)
43027
select count(*) from navigo.pointcall where Observation_date is not null and Observation_date=pointcall_indate
25568 
select count(*) from navigo.pointcall where Observation_date is not null and Observation_date=pointcall_outdate
17557
select 17557+25568 = 43125 

select distinct pointcall_action from navigo.pointcall

select count(*) from navigo.pointcall where pointcall_action is not null
select pointcall_action, count(*) as d from navigo.pointcall group by pointcall_action 
order by d desc

select pointcall_function, count(*) as d from navigocheck.check_pointcall group by pointcall_function 
order by d desc

Selected_records_number

select distinct upper(pointcall_status) from navigo.pointcall
-- 63 lignes
select count(*) from navigo.pointcall where pointcall_status is not null
185646
select count(*) from navigo.pointcall where record_id is not null
185646

select count(*) from navigo.pointcall where Net_route_marker is not null
select Net_route_marker, count(*) from navigo.pointcall group by Net_route_marker 


select Net_route_marker,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by Net_route_marker order by d desc

select Pointcall_function,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by Pointcall_function order by d desc

select Pointcall_action,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by Pointcall_action order by d desc

select Pointcall_status,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by Pointcall_status order by d desc

select distinct Next_point_rank from navigo.pointcall
select count(*) from navigo.pointcall where Next_point_rank is not null
select count(*) from navigo.pointcall where Pointcall_function is not null
select distinct Pointcall_function from navigo.pointcall

select  distinct Pointcall_marker from navigo.pointcall
select count(*) from navigo.pointcall where Pointcall_marker is not null
select count(*) from navigo.pointcall where Pointcall_name is not null
select  distinct Pointcall_name from navigo.pointcall limit 10
select   Previous_pointcall_rank, pointcall_rank, next_point_rank from navigo.pointcall limit 10

select  distinct data_block_leader_marker from navigo.pointcall
""
"T"
"A"

select data_block_leader_marker,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by data_block_leader_marker order by d desc

select count(*) from navigo.pointcall where data_block_leader_marker is not null
134368
select count(*) from navigo.pointcall where net_route_marker is not null
111317

select count(*) from navigo.pointcall where data_block_leader_marker <> net_route_marker
34630
select count(*) from navigo.pointcall where data_block_leader_marker = net_route_marker
50509
select count(*) from navigo.pointcall where data_block_leader_marker is null and net_route_marker is not null
26178
select count(*) from navigo.pointcall where data_block_leader_marker is not null and net_route_marker is  null
49229

select count(*) from navigo.pointcall where datablock_leader__pointcall_outdate is not null
132469 
select count(*) from navigo.pointcall where datablock_leader__pointcall_outdate = pointcall_outdate
56432
and net_route_marker is  null


select 50509+34630+26178 = 111317

SELECT column_name, *
   FROM   INFORMATION_SCHEMA.COLUMNS
   WHERE  TABLE_SCHEMA='navigo' and TABLE_NAME = 'pointcall'
   order by ordinal_position
--------------------------------------------------------------------------------------------
-- navire + documentary_unit_id
--------------------------------------------------------------------------------------------
select 
ship_name, ship_class, ship_flag, ship_homeport, ship_homeport_id, ship_increw, ship_outcrew, ship_tonnage, ship_tonnage_u, guns, 
convoi_id, convoi_entrant_id, datablock_leader__ship_homeport, datablock_leader__ship_tonnage
from navigo.pointcall

select ship_name, ship_class, ship_flag, datablock_leader__ship_homeport, ship_homeport, ship_homeport_id, datablock_leader__ship_tonnage, ship_tonnage, ship_tonnage_u, guns,  
ship_increw, ship_outcrew, convoi_entrant_id, Convoi_id_calculé
from navigo.pointcall

select distinct ship_class from navigo.pointcall
select count(*) from navigo.pointcall where ship_class is not null
select count(*) from navigo.pointcall where ship_flag is not null
select distinct ship_flag from navigo.pointcall

select ship_flag,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by ship_flag order by d desc

select ship_class,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by ship_class order by d desc

select ship_tonnage_U,  count(*) as d  from navigocheck.check_pointcall where component_description__suite_id in ('00000003', '00000074')
group by ship_tonnage_U order by d desc

select count(*) from navigo.pointcall where ship_flag_id is not null
select distinct ship_flag_id from navigo.pointcall 
select count(*) from navigo.pointcall where ship_homeport is not null
select distinct ship_homeport from navigo.pointcall 
select count(*) from navigo.pointcall where ship_homeport_id is not null and component_description__suite_id = '00000003'
select distinct ship_homeport_id from navigo.pointcall limit 10
select count(*) from navigo.pointcall where ship_local_id is not null 
select count(distinct ship_local_id) from navigo.pointcall limit 10
select count(*) from navigo.pointcall where ship_increw is not null 
select count(distinct ship_increw) from navigo.pointcall 
select distinct ship_increw from navigo.pointcall limit 10

select count(*) from navigo.pointcall where ship_name is not null 
select count(distinct ship_name) from navigo.pointcall 
select distinct ship_name from navigo.pointcall limit 10

select count(*) from navigo.pointcall where ship_tonnage is not null 
select count(distinct ship_tonnage) from navigo.pointcall 
select distinct ship_tonnage from navigo.pointcall limit 10


select count(*) from navigo.pointcall where ship_tonnage_u is not null 
select count(distinct ship_tonnage_u) from navigo.pointcall 
select distinct ship_tonnage_u from navigo.pointcall limit 10

select count(*) from navigo.pointcall where ship_tonnage_sort is not null 
select count(distinct ship_tonnage_sort) from navigo.pointcall 
select distinct ship_tonnage_sort from navigo.pointcall limit 10

select count(*) from navigo.pointcall where ship_outcrew is not null 
select count(distinct ship_outcrew) from navigo.pointcall 
select distinct ship_outcrew from navigo.pointcall limit 10

select count(*) from navigo.pointcall where guns is not null 
select count(distinct guns) from navigo.pointcall 
select distinct guns from navigo.pointcall limit 10


select count(*) from navigo.pointcall where convoi_entrant_id is not null 
select count(distinct convoi_entrant_id) from navigo.pointcall 
select distinct convoi_entrant_id from navigo.pointcall limit 10

select count(*) from navigo.pointcall where Convoi_id_calculé is not null 
select count(distinct Convoi_id_calculé) from navigo.pointcall 
select distinct Convoi_id_calculé from navigo.pointcall limit 10


--------------------------------------------------------------------------------------------
-- sources 
--------------------------------------------------------------------------------------------
select
 component_description__component_id, component_description__component_short_title, component_description__component_source, component_description__suite_id, 
remarks, record_last_change_signature,record_last_change_date, component_description__task, 
record_id, data_block_local_id, documentary_unit_id, Selected_records_number
from navigo.pointcall

select count(*) from navigo.pointcall where component_description__component_id is not null
select count(*) from navigo.pointcall where component_description__component_short_title is not null
select count(*) from navigo.pointcall where component_description__component_source is not null
select count(*) from navigo.pointcall where component_description__suite_id is not null
select Remarks from navigo.pointcall where Remarks is not null

select distinct component_description__component_id, component_description__component_short_title, component_description__suite_id, component_description__component_source from  navigo.pointcall 

select distinct record_id, data_block_local_id, documentary_unit_id, Documentary_unit_id_merging,  Documentary_unit_id_original, Documentary_unit_marker from  navigo.pointcall 
select count (distinct data_block_local_id) from  navigo.pointcall 
select count (distinct documentary_unit_id) from  navigo.pointcall 
select count (distinct documentary_unit_id)  from  navigo.pointcall  where documentary_unit_id=data_block_local_id
select count(*) from navigo.pointcall where component_description__task is not null
select count(distinct Selected_records_number) from navigo.pointcall  
-- 1 : vaut toujourq 2.0
select count(distinct task) from navigo.pointcall  
-- 0
select count(*) from navigo.pointcall where task is not null

select component_description__component_source, source from navigo.pointcall  where source<>component_description__component_source
Documentary_unit_id_merging

select distinct captain_status from navigo.pointcall
select distinct captain_local_id from navigo.pointcall
select distinct captain_citizenship from navigo.pointcall
select distinct captain_origin_id from navigo.pointcall
select distinct captain_origin from navigo.pointcall

select *
from navigo.pointcall p, navigo.geo_general g 
where g.pointcall_uhgs_id = p.captain_birthplace_id

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

-- en tant que postgres : https://stackoverrun.com/fr/q/1778157
-- create role dba with superuser noinherit; 
-- grant dba to navigo; 
set role dba
-- plus tard, quand j'ai fini de créer les fonction
reset role;

-- GRANT USAGE ON LANGUAGE plpython3u to navigo

SELECT lanpltrusted FROM pg_language WHERE lanname LIKE 'plpython3u';
-- f

CREATE FUNCTION navigo.pystrip(x text)
  RETURNS text
AS $$
  global x
  x = x.strip()  # ok now
  return x
$$ LANGUAGE plpython3u;

select navigo.pystrip('  dfsfsf sdsfs toto ')

CREATE TYPE qual_value AS (
  value   text,
  code  integer
);

CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
$$
	 
	global result
	global code
	if tested_value is not None : 
		result = tested_value.strip()
		code = 0
		if (tested_value.strip().find('(') == 0):
			result = result.replace('(', '').replace(')', '') 
			code = 1
		if (tested_value.strip().find('[') == 0):
			result = result.replace('[', '').replace(']', '') 
			code = 2
		if (result.strip().find('(') == 0):
			result = result.replace('(', '').replace(')', '') 
			code = code + 1
		if (result.strip().find('[') == 0):
			result = result.replace('[', '').replace(']', '')
			code = code + 1 
	else :
		result = None
		code = 5
	return [result, code]
$$ LANGUAGE plpython3u;

select (navigo.rm_parentheses_crochets('[(du bon jambon)]')).value, (navigo.rm_parentheses_crochets('[(du bon jambon)]')).code
select (navigo.rm_parentheses_crochets('[du bon jambon]')).value, (navigo.rm_parentheses_crochets('[du bon jambon]')).code
select (navigo.rm_parentheses_crochets('(du bon jambon)')).value, (navigo.rm_parentheses_crochets('(du bon jambon)')).code
select (navigo.rm_parentheses_crochets('du bon jambon')).value, (navigo.rm_parentheses_crochets('du bon jambon')).code
select (navigo.rm_parentheses_crochets('(du bon jambon]')).value, (navigo.rm_parentheses_crochets('(du bon jambon]')).code
-- "du bon jambon]";1
select (navigo.rm_parentheses_crochets(null)).value is null, (navigo.rm_parentheses_crochets(null)).code

insert into navigo.uncertainity_pointcall ( select pkid, (navigo.rm_parentheses_crochets(ship_name)).code , (navigo.rm_parentheses_crochets(datablock_leader__captain_name)).code , (navigo.rm_parentheses_crochets(datablock_leader__ship_tonnage)).code , (navigo.rm_parentheses_crochets(datablock_leader__ship_homeport)).code , (navigo.rm_parentheses_crochets(last_point__pointcall_name)).code , (navigo.rm_parentheses_crochets(last_point__pointcall_uhgs_id)).code , (navigo.rm_parentheses_crochets(datablock_leader__pointcall_outdate)).code , (navigo.rm_parentheses_crochets(documentary_unit_id)).code , (navigo.rm_parentheses_crochets(captain_age)).code , (navigo.rm_parentheses_crochets(captain_alternate_id)).code , (navigo.rm_parentheses_crochets(captain_birthplace)).code , (navigo.rm_parentheses_crochets(captain_birthplace_id)).code , (navigo.rm_parentheses_crochets(captain_citizenship)).code , (navigo.rm_parentheses_crochets(captain_citizenship_id)).code , (navigo.rm_parentheses_crochets(captain_last_name)).code , (navigo.rm_parentheses_crochets(captain_local_id)).code , (navigo.rm_parentheses_crochets(captain_name)).code , (navigo.rm_parentheses_crochets(captain_name_sort)).code , (navigo.rm_parentheses_crochets(captain_origin)).code , (navigo.rm_parentheses_crochets(captain_origin_id)).code , (navigo.rm_parentheses_crochets(captain_status)).code , (navigo.rm_parentheses_crochets(convoi_entrant_id)).code , (navigo.rm_parentheses_crochets(convoi_id_calculé)).code , (navigo.rm_parentheses_crochets(data_block_leader_marker)).code , (navigo.rm_parentheses_crochets(data_block_local_id)).code , (navigo.rm_parentheses_crochets(documentary_unit_id_merging)).code , (navigo.rm_parentheses_crochets(documentary_unit_id_original)).code , (navigo.rm_parentheses_crochets(documentary_unit_marker)).code , (navigo.rm_parentheses_crochets(guns)).code , (navigo.rm_parentheses_crochets(health_data)).code , (navigo.rm_parentheses_crochets(net_route_marker)).code , (navigo.rm_parentheses_crochets(next_point_rank)).code , (navigo.rm_parentheses_crochets(observation_date)).code , (navigo.rm_parentheses_crochets(observation_pointcall_outdate)).code , (navigo.rm_parentheses_crochets(pointcall_action)).code , (navigo.rm_parentheses_crochets(pointcall_calculated_year)).code , (navigo.rm_parentheses_crochets(pointcall_date)).code , (navigo.rm_parentheses_crochets(pointcall_date_sort)).code , (navigo.rm_parentheses_crochets(pointcall_function)).code , (navigo.rm_parentheses_crochets(pointcall_indate)).code , (navigo.rm_parentheses_crochets(pointcall_indate_date)).code , (navigo.rm_parentheses_crochets(pointcall_indate_relativity)).code , (navigo.rm_parentheses_crochets(pointcall_marker)).code , (navigo.rm_parentheses_crochets(pointcall_name)).code , (navigo.rm_parentheses_crochets(pointcall_outdate)).code , (navigo.rm_parentheses_crochets(pointcall_outdate_date)).code , (navigo.rm_parentheses_crochets(pointcall_outdate_relativity)).code , (navigo.rm_parentheses_crochets(pointcall_rank)).code , (navigo.rm_parentheses_crochets(pointcall_status)).code , (navigo.rm_parentheses_crochets(pointcall_stay_duration)).code , (navigo.rm_parentheses_crochets(pointcall_stay_duration_calculated)).code , (navigo.rm_parentheses_crochets(pointcall_stay_duration_declared)).code , (navigo.rm_parentheses_crochets(pointcall_uhgs_id)).code , (navigo.rm_parentheses_crochets(previous_pointcall_rank)).code , (navigo.rm_parentheses_crochets(query_route)).code , (navigo.rm_parentheses_crochets(record_creation_date)).code , (navigo.rm_parentheses_crochets(record_creation_signature)).code , (navigo.rm_parentheses_crochets(record_id)).code , (navigo.rm_parentheses_crochets(record_id_merging)).code , (navigo.rm_parentheses_crochets(record_last_change_date)).code , (navigo.rm_parentheses_crochets(record_last_change_signature)).code , (navigo.rm_parentheses_crochets(remarks)).code , (navigo.rm_parentheses_crochets(route_calculated_duration)).code , (navigo.rm_parentheses_crochets(route_declared_duration)).code , (navigo.rm_parentheses_crochets(route_duration)).code , (navigo.rm_parentheses_crochets(route_points_number)).code , (navigo.rm_parentheses_crochets(selected_record_rank)).code , (navigo.rm_parentheses_crochets(selected_records_number)).code , (navigo.rm_parentheses_crochets(ship_class)).code , (navigo.rm_parentheses_crochets(ship_flag)).code , (navigo.rm_parentheses_crochets(ship_flag_id)).code , (navigo.rm_parentheses_crochets(ship_homeport)).code , (navigo.rm_parentheses_crochets(ship_homeport_id)).code , (navigo.rm_parentheses_crochets(ship_homeport_sort)).code , (navigo.rm_parentheses_crochets(ship_increw)).code , (navigo.rm_parentheses_crochets(ship_local_id)).code , (navigo.rm_parentheses_crochets(ship_name_sort)).code , (navigo.rm_parentheses_crochets(ship_outcrew)).code , (navigo.rm_parentheses_crochets(ship_tonnage)).code , (navigo.rm_parentheses_crochets(ship_tonnage_sort)).code , (navigo.rm_parentheses_crochets(ship_tonnage_u)).code , (navigo.rm_parentheses_crochets(source)).code , (navigo.rm_parentheses_crochets(stage_calculated_duration_next)).code , (navigo.rm_parentheses_crochets(stage_calculated_duration_previous)).code , (navigo.rm_parentheses_crochets(stage_declared_duration_previous)).code , (navigo.rm_parentheses_crochets(stage_duration_previous)).code , (navigo.rm_parentheses_crochets(stage_id)).code , (navigo.rm_parentheses_crochets(stage_id_link)).code , (navigo.rm_parentheses_crochets(stage_id_link_reversed)).code , (navigo.rm_parentheses_crochets(stage_identifier_length)).code , (navigo.rm_parentheses_crochets(stage_relooked)).code , (navigo.rm_parentheses_crochets(stage_relooked_consistency)).code , (navigo.rm_parentheses_crochets(stage_speed)).code , (navigo.rm_parentheses_crochets(stage_speed_relooked)).code , (navigo.rm_parentheses_crochets(task)).code , (navigo.rm_parentheses_crochets(component_description__component_id)).code , (navigo.rm_parentheses_crochets(component_description__component_short_title)).code , (navigo.rm_parentheses_crochets(component_description__component_source)).code , (navigo.rm_parentheses_crochets(component_description__suite_id)).code , (navigo.rm_parentheses_crochets(component_description__task)).code  from navigo.pointcall);

select * from navigo.uncertainity_pointcall limit 20
select * from navigocheck.check_pointcall limit 20


insert into navigo.uncertainity_acting_parties ( select pkid, (navigo.rm_parentheses_crochets(acting_party_final_date)).code , (navigo.rm_parentheses_crochets(acting_party_initial_date)).code , (navigo.rm_parentheses_crochets(acting_party_local_id)).code , (navigo.rm_parentheses_crochets(acting_party_location)).code , (navigo.rm_parentheses_crochets(acting_party_location_class)).code , (navigo.rm_parentheses_crochets(acting_party_location_uhgs_id)).code , (navigo.rm_parentheses_crochets(acting_party_name)).code , (navigo.rm_parentheses_crochets(acting_party_role)).code , (navigo.rm_parentheses_crochets(acting_party_social_id)).code , (navigo.rm_parentheses_crochets(calculation_01)).code , (navigo.rm_parentheses_crochets(calculation_02)).code , (navigo.rm_parentheses_crochets(calculation_03)).code , (navigo.rm_parentheses_crochets(datablock_local_id)).code , (navigo.rm_parentheses_crochets(documentary_unit_id_integration)).code , (navigo.rm_parentheses_crochets(global_01)).code , (navigo.rm_parentheses_crochets(global_02)).code , (navigo.rm_parentheses_crochets(global_03)).code , (navigo.rm_parentheses_crochets(link_to_all)).code , (navigo.rm_parentheses_crochets(link_to_observation)).code , (navigo.rm_parentheses_crochets(point_id_integration)).code , (navigo.rm_parentheses_crochets(pointcall_id)).code , (navigo.rm_parentheses_crochets(record_creation_date)).code , (navigo.rm_parentheses_crochets(record_creation_signature)).code , (navigo.rm_parentheses_crochets(record_id)).code , (navigo.rm_parentheses_crochets(record_last_change_date)).code , (navigo.rm_parentheses_crochets(record_last_change_signature)).code , (navigo.rm_parentheses_crochets(remarks)).code , (navigo.rm_parentheses_crochets(selected_record_rank)).code , (navigo.rm_parentheses_crochets(selected_records_number)).code , (navigo.rm_parentheses_crochets(source)).code , (navigo.rm_parentheses_crochets(tasks)).code , (navigo.rm_parentheses_crochets(taxes__action_id_integration)).code , (navigo.rm_parentheses_crochets(taxes__cargo_item_id_integration)).code  from navigo.acting_parties);
insert into navigocheck.check_acting_parties ( select pkid, (navigo.rm_parentheses_crochets(acting_party_final_date)).value , (navigo.rm_parentheses_crochets(acting_party_initial_date)).value , (navigo.rm_parentheses_crochets(acting_party_local_id)).value , (navigo.rm_parentheses_crochets(acting_party_location)).value , (navigo.rm_parentheses_crochets(acting_party_location_class)).value , (navigo.rm_parentheses_crochets(acting_party_location_uhgs_id)).value , (navigo.rm_parentheses_crochets(acting_party_name)).value , (navigo.rm_parentheses_crochets(acting_party_role)).value , (navigo.rm_parentheses_crochets(acting_party_social_id)).value , (navigo.rm_parentheses_crochets(calculation_01)).value , (navigo.rm_parentheses_crochets(calculation_02)).value , (navigo.rm_parentheses_crochets(calculation_03)).value , (navigo.rm_parentheses_crochets(datablock_local_id)).value , (navigo.rm_parentheses_crochets(documentary_unit_id_integration)).value , (navigo.rm_parentheses_crochets(global_01)).value , (navigo.rm_parentheses_crochets(global_02)).value , (navigo.rm_parentheses_crochets(global_03)).value , (navigo.rm_parentheses_crochets(link_to_all)).value , (navigo.rm_parentheses_crochets(link_to_observation)).value , (navigo.rm_parentheses_crochets(point_id_integration)).value , (navigo.rm_parentheses_crochets(pointcall_id)).value , (navigo.rm_parentheses_crochets(record_creation_date)).value , (navigo.rm_parentheses_crochets(record_creation_signature)).value , (navigo.rm_parentheses_crochets(record_id)).value , (navigo.rm_parentheses_crochets(record_last_change_date)).value , (navigo.rm_parentheses_crochets(record_last_change_signature)).value , (navigo.rm_parentheses_crochets(remarks)).value , (navigo.rm_parentheses_crochets(selected_record_rank)).value , (navigo.rm_parentheses_crochets(selected_records_number)).value , (navigo.rm_parentheses_crochets(source)).value , (navigo.rm_parentheses_crochets(tasks)).value , (navigo.rm_parentheses_crochets(taxes__action_id_integration)).value , (navigo.rm_parentheses_crochets(taxes__cargo_item_id_integration)).value  from navigo.acting_parties);

insert into navigo.uncertainity_actions ( select pkid, (navigo.rm_parentheses_crochets(action_duration)).code , (navigo.rm_parentheses_crochets(action_final_date)).code , (navigo.rm_parentheses_crochets(action_final_date_non_gregorian)).code , (navigo.rm_parentheses_crochets(action_final_date_relativity)).code , (navigo.rm_parentheses_crochets(action_final_date_sort)).code , (navigo.rm_parentheses_crochets(action_initial_date)).code , (navigo.rm_parentheses_crochets(action_initial_date_non_gregorian)).code , (navigo.rm_parentheses_crochets(action_initial_date_relativity)).code , (navigo.rm_parentheses_crochets(action_initial_date_sort)).code , (navigo.rm_parentheses_crochets(action_permanent_coding)).code , (navigo.rm_parentheses_crochets(action_place)).code , (navigo.rm_parentheses_crochets(action_place_uhgs_id)).code , (navigo.rm_parentheses_crochets(action_point_id)).code , (navigo.rm_parentheses_crochets(action_point_id_appending)).code , (navigo.rm_parentheses_crochets(action_text)).code , (navigo.rm_parentheses_crochets(birth_year)).code , (navigo.rm_parentheses_crochets(birthdate_marker)).code , (navigo.rm_parentheses_crochets(busqueda_c_mt)).code , (navigo.rm_parentheses_crochets(calculation_01)).code , (navigo.rm_parentheses_crochets(calculation_02)).code , (navigo.rm_parentheses_crochets(calculation_03)).code , (navigo.rm_parentheses_crochets(calculation_import_01)).code , (navigo.rm_parentheses_crochets(calculation_import_02)).code , (navigo.rm_parentheses_crochets(cargo_item_id)).code , (navigo.rm_parentheses_crochets(cargo_item_id_appending)).code , (navigo.rm_parentheses_crochets(counter)).code , (navigo.rm_parentheses_crochets(cultural_object_id)).code , (navigo.rm_parentheses_crochets(dependent_variable_marker)).code , (navigo.rm_parentheses_crochets(documentary_unit_id)).code , (navigo.rm_parentheses_crochets(documentary_unit_id_appending)).code , (navigo.rm_parentheses_crochets(dur_routine_marker)).code , (navigo.rm_parentheses_crochets(edition_id)).code , (navigo.rm_parentheses_crochets(global_01)).code , (navigo.rm_parentheses_crochets(global_02)).code , (navigo.rm_parentheses_crochets(global_03)).code , (navigo.rm_parentheses_crochets(global_04)).code , (navigo.rm_parentheses_crochets(global_05)).code , (navigo.rm_parentheses_crochets(global_06)).code , (navigo.rm_parentheses_crochets(global_07)).code , (navigo.rm_parentheses_crochets(global_08)).code , (navigo.rm_parentheses_crochets(global_09)).code , (navigo.rm_parentheses_crochets(global_10)).code , (navigo.rm_parentheses_crochets(global_11)).code , (navigo.rm_parentheses_crochets(global_12)).code , (navigo.rm_parentheses_crochets(global_13)).code , (navigo.rm_parentheses_crochets(global_14)).code , (navigo.rm_parentheses_crochets(global_15)).code , (navigo.rm_parentheses_crochets(global_16)).code , (navigo.rm_parentheses_crochets(global_17)).code , (navigo.rm_parentheses_crochets(global_18)).code , (navigo.rm_parentheses_crochets(global_19)).code , (navigo.rm_parentheses_crochets(global_20)).code , (navigo.rm_parentheses_crochets(global_21)).code , (navigo.rm_parentheses_crochets(global_22)).code , (navigo.rm_parentheses_crochets(global_23)).code , (navigo.rm_parentheses_crochets(global_24)).code , (navigo.rm_parentheses_crochets(global_25)).code , (navigo.rm_parentheses_crochets(grouping_unit_class)).code , (navigo.rm_parentheses_crochets(grouping_unit_id_appending)).code , (navigo.rm_parentheses_crochets(hidden_data_marker)).code , (navigo.rm_parentheses_crochets(highlighted_string)).code , (navigo.rm_parentheses_crochets(incompleteness_marker)).code , (navigo.rm_parentheses_crochets(link_to_all)).code , (navigo.rm_parentheses_crochets(link_to_chosen)).code , (navigo.rm_parentheses_crochets(link_to_documents)).code , (navigo.rm_parentheses_crochets(link_to_geneal_birth_record)).code , (navigo.rm_parentheses_crochets(link_to_observation)).code , (navigo.rm_parentheses_crochets(link_to_research_operation)).code , (navigo.rm_parentheses_crochets(link_to_source_01)).code , (navigo.rm_parentheses_crochets(link_to_source_02)).code , (navigo.rm_parentheses_crochets(link_to_source_03)).code , (navigo.rm_parentheses_crochets(link_to_sources)).code , (navigo.rm_parentheses_crochets(links_to_diem)).code , (navigo.rm_parentheses_crochets(links_to_grouping_units)).code , (navigo.rm_parentheses_crochets(main_variable_marker)).code , (navigo.rm_parentheses_crochets(neat_data_marker)).code , (navigo.rm_parentheses_crochets(objects_id)).code , (navigo.rm_parentheses_crochets(p1_calculated_age)).code , (navigo.rm_parentheses_crochets(p1_cultural_object_indicator)).code , (navigo.rm_parentheses_crochets(p1_declared_address)).code , (navigo.rm_parentheses_crochets(p1_declared_age)).code , (navigo.rm_parentheses_crochets(p1_declared_birthdate)).code , (navigo.rm_parentheses_crochets(p1_declared_birthplace)).code , (navigo.rm_parentheses_crochets(p1_declared_citizenship)).code , (navigo.rm_parentheses_crochets(p1_declared_confession)).code , (navigo.rm_parentheses_crochets(p1_declared_father)).code , (navigo.rm_parentheses_crochets(p1_declared_gender)).code , (navigo.rm_parentheses_crochets(p1_declared_legal_residence)).code , (navigo.rm_parentheses_crochets(p1_declared_mother)).code , (navigo.rm_parentheses_crochets(p1_declared_office)).code , (navigo.rm_parentheses_crochets(p1_id)).code , (navigo.rm_parentheses_crochets(p1_link_to_birthdate)).code , (navigo.rm_parentheses_crochets(p1_name)).code , (navigo.rm_parentheses_crochets(p1_p2_p3_p4_id)).code , (navigo.rm_parentheses_crochets(p1_p2_previous_relationship)).code , (navigo.rm_parentheses_crochets(p1_query_name)).code , (navigo.rm_parentheses_crochets(p1_signing_hability)).code , (navigo.rm_parentheses_crochets(p2_calculated_age)).code , (navigo.rm_parentheses_crochets(p2_cultural_object_indicator)).code , (navigo.rm_parentheses_crochets(p2_declared_address)).code , (navigo.rm_parentheses_crochets(p2_declared_age)).code , (navigo.rm_parentheses_crochets(p2_declared_birthdate)).code , (navigo.rm_parentheses_crochets(p2_declared_birthplace)).code , (navigo.rm_parentheses_crochets(p2_declared_citizenship)).code , (navigo.rm_parentheses_crochets(p2_declared_confession)).code , (navigo.rm_parentheses_crochets(p2_declared_dwelling_place)).code , (navigo.rm_parentheses_crochets(p2_declared_father)).code , (navigo.rm_parentheses_crochets(p2_declared_gender)).code , (navigo.rm_parentheses_crochets(p2_declared_legal_residence)).code , (navigo.rm_parentheses_crochets(p2_declared_mother)).code , (navigo.rm_parentheses_crochets(p2_declared_office)).code , (navigo.rm_parentheses_crochets(p2_id)).code , (navigo.rm_parentheses_crochets(p2_link_to_birthdate)).code , (navigo.rm_parentheses_crochets(p2_name)).code , (navigo.rm_parentheses_crochets(p2_p1_previous_relationship)).code , (navigo.rm_parentheses_crochets(p2_query_name)).code , (navigo.rm_parentheses_crochets(p2_signing_hability)).code , (navigo.rm_parentheses_crochets(p3_id)).code , (navigo.rm_parentheses_crochets(p4_id)).code , (navigo.rm_parentheses_crochets(permanent_coding_marker)).code , (navigo.rm_parentheses_crochets(record_class)).code , (navigo.rm_parentheses_crochets(record_creation_date)).code , (navigo.rm_parentheses_crochets(record_creation_signature)).code , (navigo.rm_parentheses_crochets(record_id)).code , (navigo.rm_parentheses_crochets(record_id_appending)).code , (navigo.rm_parentheses_crochets(record_last_change_date)).code , (navigo.rm_parentheses_crochets(record_last_change_signature)).code , (navigo.rm_parentheses_crochets(relationship)).code , (navigo.rm_parentheses_crochets(remarks)).code , (navigo.rm_parentheses_crochets(research_operation)).code , (navigo.rm_parentheses_crochets(screen_density)).code , (navigo.rm_parentheses_crochets(selected_record_rank)).code , (navigo.rm_parentheses_crochets(selected_records_number)).code , (navigo.rm_parentheses_crochets(source)).code , (navigo.rm_parentheses_crochets(use_marker)).code , (navigo.rm_parentheses_crochets(v2_marker)).code , (navigo.rm_parentheses_crochets(v3_marker)).code , (navigo.rm_parentheses_crochets(v4_marker)).code , (navigo.rm_parentheses_crochets(actions_diem__global_01)).code , (navigo.rm_parentheses_crochets(actions_diem__screen_density)).code , (navigo.rm_parentheses_crochets(actions_grouping__global_01)).code , (navigo.rm_parentheses_crochets(actions_grouping__record_id)).code , (navigo.rm_parentheses_crochets(actions_grouping__record_number)).code , (navigo.rm_parentheses_crochets(actions_grouping__records_found)).code , (navigo.rm_parentheses_crochets(actions_grouping__screen_density)).code , (navigo.rm_parentheses_crochets(actions_sources__global_01)).code , (navigo.rm_parentheses_crochets(actions_sources__screen_density)).code , (navigo.rm_parentheses_crochets(dictionary_actors_p1__marker_for_extraction)).code , (navigo.rm_parentheses_crochets(dictionary_actors_p2__marker_for_extraction)).code , (navigo.rm_parentheses_crochets(dictionary_actors_p3__marker_for_extraction)).code , (navigo.rm_parentheses_crochets(dictionary_actors_p4__marker_for_extraction)).code  from navigo.actions);
insert into navigocheck.check_actions ( select pkid, (navigo.rm_parentheses_crochets(action_duration)).value , (navigo.rm_parentheses_crochets(action_final_date)).value , (navigo.rm_parentheses_crochets(action_final_date_non_gregorian)).value , (navigo.rm_parentheses_crochets(action_final_date_relativity)).value , (navigo.rm_parentheses_crochets(action_final_date_sort)).value , (navigo.rm_parentheses_crochets(action_initial_date)).value , (navigo.rm_parentheses_crochets(action_initial_date_non_gregorian)).value , (navigo.rm_parentheses_crochets(action_initial_date_relativity)).value , (navigo.rm_parentheses_crochets(action_initial_date_sort)).value , (navigo.rm_parentheses_crochets(action_permanent_coding)).value , (navigo.rm_parentheses_crochets(action_place)).value , (navigo.rm_parentheses_crochets(action_place_uhgs_id)).value , (navigo.rm_parentheses_crochets(action_point_id)).value , (navigo.rm_parentheses_crochets(action_point_id_appending)).value , (navigo.rm_parentheses_crochets(action_text)).value , (navigo.rm_parentheses_crochets(birth_year)).value , (navigo.rm_parentheses_crochets(birthdate_marker)).value , (navigo.rm_parentheses_crochets(busqueda_c_mt)).value , (navigo.rm_parentheses_crochets(calculation_01)).value , (navigo.rm_parentheses_crochets(calculation_02)).value , (navigo.rm_parentheses_crochets(calculation_03)).value , (navigo.rm_parentheses_crochets(calculation_import_01)).value , (navigo.rm_parentheses_crochets(calculation_import_02)).value , (navigo.rm_parentheses_crochets(cargo_item_id)).value , (navigo.rm_parentheses_crochets(cargo_item_id_appending)).value , (navigo.rm_parentheses_crochets(counter)).value , (navigo.rm_parentheses_crochets(cultural_object_id)).value , (navigo.rm_parentheses_crochets(dependent_variable_marker)).value , (navigo.rm_parentheses_crochets(documentary_unit_id)).value , (navigo.rm_parentheses_crochets(documentary_unit_id_appending)).value , (navigo.rm_parentheses_crochets(dur_routine_marker)).value , (navigo.rm_parentheses_crochets(edition_id)).value , (navigo.rm_parentheses_crochets(global_01)).value , (navigo.rm_parentheses_crochets(global_02)).value , (navigo.rm_parentheses_crochets(global_03)).value , (navigo.rm_parentheses_crochets(global_04)).value , (navigo.rm_parentheses_crochets(global_05)).value , (navigo.rm_parentheses_crochets(global_06)).value , (navigo.rm_parentheses_crochets(global_07)).value , (navigo.rm_parentheses_crochets(global_08)).value , (navigo.rm_parentheses_crochets(global_09)).value , (navigo.rm_parentheses_crochets(global_10)).value , (navigo.rm_parentheses_crochets(global_11)).value , (navigo.rm_parentheses_crochets(global_12)).value , (navigo.rm_parentheses_crochets(global_13)).value , (navigo.rm_parentheses_crochets(global_14)).value , (navigo.rm_parentheses_crochets(global_15)).value , (navigo.rm_parentheses_crochets(global_16)).value , (navigo.rm_parentheses_crochets(global_17)).value , (navigo.rm_parentheses_crochets(global_18)).value , (navigo.rm_parentheses_crochets(global_19)).value , (navigo.rm_parentheses_crochets(global_20)).value , (navigo.rm_parentheses_crochets(global_21)).value , (navigo.rm_parentheses_crochets(global_22)).value , (navigo.rm_parentheses_crochets(global_23)).value , (navigo.rm_parentheses_crochets(global_24)).value , (navigo.rm_parentheses_crochets(global_25)).value , (navigo.rm_parentheses_crochets(grouping_unit_class)).value , (navigo.rm_parentheses_crochets(grouping_unit_id_appending)).value , (navigo.rm_parentheses_crochets(hidden_data_marker)).value , (navigo.rm_parentheses_crochets(highlighted_string)).value , (navigo.rm_parentheses_crochets(incompleteness_marker)).value , (navigo.rm_parentheses_crochets(link_to_all)).value , (navigo.rm_parentheses_crochets(link_to_chosen)).value , (navigo.rm_parentheses_crochets(link_to_documents)).value , (navigo.rm_parentheses_crochets(link_to_geneal_birth_record)).value , (navigo.rm_parentheses_crochets(link_to_observation)).value , (navigo.rm_parentheses_crochets(link_to_research_operation)).value , (navigo.rm_parentheses_crochets(link_to_source_01)).value , (navigo.rm_parentheses_crochets(link_to_source_02)).value , (navigo.rm_parentheses_crochets(link_to_source_03)).value , (navigo.rm_parentheses_crochets(link_to_sources)).value , (navigo.rm_parentheses_crochets(links_to_diem)).value , (navigo.rm_parentheses_crochets(links_to_grouping_units)).value , (navigo.rm_parentheses_crochets(main_variable_marker)).value , (navigo.rm_parentheses_crochets(neat_data_marker)).value , (navigo.rm_parentheses_crochets(objects_id)).value , (navigo.rm_parentheses_crochets(p1_calculated_age)).value , (navigo.rm_parentheses_crochets(p1_cultural_object_indicator)).value , (navigo.rm_parentheses_crochets(p1_declared_address)).value , (navigo.rm_parentheses_crochets(p1_declared_age)).value , (navigo.rm_parentheses_crochets(p1_declared_birthdate)).value , (navigo.rm_parentheses_crochets(p1_declared_birthplace)).value , (navigo.rm_parentheses_crochets(p1_declared_citizenship)).value , (navigo.rm_parentheses_crochets(p1_declared_confession)).value , (navigo.rm_parentheses_crochets(p1_declared_father)).value , (navigo.rm_parentheses_crochets(p1_declared_gender)).value , (navigo.rm_parentheses_crochets(p1_declared_legal_residence)).value , (navigo.rm_parentheses_crochets(p1_declared_mother)).value , (navigo.rm_parentheses_crochets(p1_declared_office)).value , (navigo.rm_parentheses_crochets(p1_id)).value , (navigo.rm_parentheses_crochets(p1_link_to_birthdate)).value , (navigo.rm_parentheses_crochets(p1_name)).value , (navigo.rm_parentheses_crochets(p1_p2_p3_p4_id)).value , (navigo.rm_parentheses_crochets(p1_p2_previous_relationship)).value , (navigo.rm_parentheses_crochets(p1_query_name)).value , (navigo.rm_parentheses_crochets(p1_signing_hability)).value , (navigo.rm_parentheses_crochets(p2_calculated_age)).value , (navigo.rm_parentheses_crochets(p2_cultural_object_indicator)).value , (navigo.rm_parentheses_crochets(p2_declared_address)).value , (navigo.rm_parentheses_crochets(p2_declared_age)).value , (navigo.rm_parentheses_crochets(p2_declared_birthdate)).value , (navigo.rm_parentheses_crochets(p2_declared_birthplace)).value , (navigo.rm_parentheses_crochets(p2_declared_citizenship)).value , (navigo.rm_parentheses_crochets(p2_declared_confession)).value , (navigo.rm_parentheses_crochets(p2_declared_dwelling_place)).value , (navigo.rm_parentheses_crochets(p2_declared_father)).value , (navigo.rm_parentheses_crochets(p2_declared_gender)).value , (navigo.rm_parentheses_crochets(p2_declared_legal_residence)).value , (navigo.rm_parentheses_crochets(p2_declared_mother)).value , (navigo.rm_parentheses_crochets(p2_declared_office)).value , (navigo.rm_parentheses_crochets(p2_id)).value , (navigo.rm_parentheses_crochets(p2_link_to_birthdate)).value , (navigo.rm_parentheses_crochets(p2_name)).value , (navigo.rm_parentheses_crochets(p2_p1_previous_relationship)).value , (navigo.rm_parentheses_crochets(p2_query_name)).value , (navigo.rm_parentheses_crochets(p2_signing_hability)).value , (navigo.rm_parentheses_crochets(p3_id)).value , (navigo.rm_parentheses_crochets(p4_id)).value , (navigo.rm_parentheses_crochets(permanent_coding_marker)).value , (navigo.rm_parentheses_crochets(record_class)).value , (navigo.rm_parentheses_crochets(record_creation_date)).value , (navigo.rm_parentheses_crochets(record_creation_signature)).value , (navigo.rm_parentheses_crochets(record_id)).value , (navigo.rm_parentheses_crochets(record_id_appending)).value , (navigo.rm_parentheses_crochets(record_last_change_date)).value , (navigo.rm_parentheses_crochets(record_last_change_signature)).value , (navigo.rm_parentheses_crochets(relationship)).value , (navigo.rm_parentheses_crochets(remarks)).value , (navigo.rm_parentheses_crochets(research_operation)).value , (navigo.rm_parentheses_crochets(screen_density)).value , (navigo.rm_parentheses_crochets(selected_record_rank)).value , (navigo.rm_parentheses_crochets(selected_records_number)).value , (navigo.rm_parentheses_crochets(source)).value , (navigo.rm_parentheses_crochets(use_marker)).value , (navigo.rm_parentheses_crochets(v2_marker)).value , (navigo.rm_parentheses_crochets(v3_marker)).value , (navigo.rm_parentheses_crochets(v4_marker)).value , (navigo.rm_parentheses_crochets(actions_diem__global_01)).value , (navigo.rm_parentheses_crochets(actions_diem__screen_density)).value , (navigo.rm_parentheses_crochets(actions_grouping__global_01)).value , (navigo.rm_parentheses_crochets(actions_grouping__record_id)).value , (navigo.rm_parentheses_crochets(actions_grouping__record_number)).value , (navigo.rm_parentheses_crochets(actions_grouping__records_found)).value , (navigo.rm_parentheses_crochets(actions_grouping__screen_density)).value , (navigo.rm_parentheses_crochets(actions_sources__global_01)).value , (navigo.rm_parentheses_crochets(actions_sources__screen_density)).value , (navigo.rm_parentheses_crochets(dictionary_actors_p1__marker_for_extraction)).value , (navigo.rm_parentheses_crochets(dictionary_actors_p2__marker_for_extraction)).value , (navigo.rm_parentheses_crochets(dictionary_actors_p3__marker_for_extraction)).value , (navigo.rm_parentheses_crochets(dictionary_actors_p4__marker_for_extraction)).value  from navigo.actions);

insert into navigo.uncertainity_taxes ( select pkid, (navigo.rm_parentheses_crochets(action_id)).code , (navigo.rm_parentheses_crochets(action_id_integration)).code , (navigo.rm_parentheses_crochets(calculation_01)).code , (navigo.rm_parentheses_crochets(calculation_02)).code , (navigo.rm_parentheses_crochets(calculation_03)).code , (navigo.rm_parentheses_crochets(cargo_item_id)).code , (navigo.rm_parentheses_crochets(cargo_item_id_integration)).code , (navigo.rm_parentheses_crochets(documentary_unit_id)).code , (navigo.rm_parentheses_crochets(documentary_unit_id_integration)).code , (navigo.rm_parentheses_crochets(global_01)).code , (navigo.rm_parentheses_crochets(global_02)).code , (navigo.rm_parentheses_crochets(global_03)).code , (navigo.rm_parentheses_crochets(global_04)).code , (navigo.rm_parentheses_crochets(global_05)).code , (navigo.rm_parentheses_crochets(global_06)).code , (navigo.rm_parentheses_crochets(global_07)).code , (navigo.rm_parentheses_crochets(global_08)).code , (navigo.rm_parentheses_crochets(global_09)).code , (navigo.rm_parentheses_crochets(global_10)).code , (navigo.rm_parentheses_crochets(global_11)).code , (navigo.rm_parentheses_crochets(global_12)).code , (navigo.rm_parentheses_crochets(global_13)).code , (navigo.rm_parentheses_crochets(global_14)).code , (navigo.rm_parentheses_crochets(link_to_all)).code , (navigo.rm_parentheses_crochets(link_to_observation)).code , (navigo.rm_parentheses_crochets(link_to_point)).code , (navigo.rm_parentheses_crochets(payment_date)).code , (navigo.rm_parentheses_crochets(point_id_integration)).code , (navigo.rm_parentheses_crochets(q01)).code , (navigo.rm_parentheses_crochets(q01_u)).code , (navigo.rm_parentheses_crochets(q02)).code , (navigo.rm_parentheses_crochets(q02_u)).code , (navigo.rm_parentheses_crochets(q03)).code , (navigo.rm_parentheses_crochets(q03_u)).code , (navigo.rm_parentheses_crochets(record_creation_date)).code , (navigo.rm_parentheses_crochets(record_creation_signature)).code , (navigo.rm_parentheses_crochets(record_id)).code , (navigo.rm_parentheses_crochets(record_id_integration)).code , (navigo.rm_parentheses_crochets(record_last_change_date)).code , (navigo.rm_parentheses_crochets(record_last_change_signature)).code , (navigo.rm_parentheses_crochets(remarks)).code , (navigo.rm_parentheses_crochets(research_operation)).code , (navigo.rm_parentheses_crochets(selected_record_rank)).code , (navigo.rm_parentheses_crochets(selected_records_number)).code , (navigo.rm_parentheses_crochets(sequence_id)).code , (navigo.rm_parentheses_crochets(source)).code , (navigo.rm_parentheses_crochets(tax_concept)).code  from navigo.taxes);
insert into navigocheck.check_taxes ( select pkid, (navigo.rm_parentheses_crochets(action_id)).value , (navigo.rm_parentheses_crochets(action_id_integration)).value , (navigo.rm_parentheses_crochets(calculation_01)).value , (navigo.rm_parentheses_crochets(calculation_02)).value , (navigo.rm_parentheses_crochets(calculation_03)).value , (navigo.rm_parentheses_crochets(cargo_item_id)).value , (navigo.rm_parentheses_crochets(cargo_item_id_integration)).value , (navigo.rm_parentheses_crochets(documentary_unit_id)).value , (navigo.rm_parentheses_crochets(documentary_unit_id_integration)).value , (navigo.rm_parentheses_crochets(global_01)).value , (navigo.rm_parentheses_crochets(global_02)).value , (navigo.rm_parentheses_crochets(global_03)).value , (navigo.rm_parentheses_crochets(global_04)).value , (navigo.rm_parentheses_crochets(global_05)).value , (navigo.rm_parentheses_crochets(global_06)).value , (navigo.rm_parentheses_crochets(global_07)).value , (navigo.rm_parentheses_crochets(global_08)).value , (navigo.rm_parentheses_crochets(global_09)).value , (navigo.rm_parentheses_crochets(global_10)).value , (navigo.rm_parentheses_crochets(global_11)).value , (navigo.rm_parentheses_crochets(global_12)).value , (navigo.rm_parentheses_crochets(global_13)).value , (navigo.rm_parentheses_crochets(global_14)).value , (navigo.rm_parentheses_crochets(link_to_all)).value , (navigo.rm_parentheses_crochets(link_to_observation)).value , (navigo.rm_parentheses_crochets(link_to_point)).value , (navigo.rm_parentheses_crochets(payment_date)).value , (navigo.rm_parentheses_crochets(point_id_integration)).value , (navigo.rm_parentheses_crochets(q01)).value , (navigo.rm_parentheses_crochets(q01_u)).value , (navigo.rm_parentheses_crochets(q02)).value , (navigo.rm_parentheses_crochets(q02_u)).value , (navigo.rm_parentheses_crochets(q03)).value , (navigo.rm_parentheses_crochets(q03_u)).value , (navigo.rm_parentheses_crochets(record_creation_date)).value , (navigo.rm_parentheses_crochets(record_creation_signature)).value , (navigo.rm_parentheses_crochets(record_id)).value , (navigo.rm_parentheses_crochets(record_id_integration)).value , (navigo.rm_parentheses_crochets(record_last_change_date)).value , (navigo.rm_parentheses_crochets(record_last_change_signature)).value , (navigo.rm_parentheses_crochets(remarks)).value , (navigo.rm_parentheses_crochets(research_operation)).value , (navigo.rm_parentheses_crochets(selected_record_rank)).value , (navigo.rm_parentheses_crochets(selected_records_number)).value , (navigo.rm_parentheses_crochets(sequence_id)).value , (navigo.rm_parentheses_crochets(source)).value , (navigo.rm_parentheses_crochets(tax_concept)).value  from navigo.taxes);


insert into navigo.uncertainity_cargo ( select pkid, (navigo.rm_parentheses_crochets(actor_id)).code , (navigo.rm_parentheses_crochets(actor_id_integration)).code , (navigo.rm_parentheses_crochets(calculation_01)).code , (navigo.rm_parentheses_crochets(calculation_02)).code , (navigo.rm_parentheses_crochets(calculation_03)).code , (navigo.rm_parentheses_crochets(cargo_item_action)).code , (navigo.rm_parentheses_crochets(cargo_item_date_declared)).code , (navigo.rm_parentheses_crochets(cargo_item_id)).code , (navigo.rm_parentheses_crochets(cargo_item_indate)).code , (navigo.rm_parentheses_crochets(cargo_item_indate_relativity)).code , (navigo.rm_parentheses_crochets(cargo_item_outdate)).code , (navigo.rm_parentheses_crochets(cargo_item_outdate_relativity)).code , (navigo.rm_parentheses_crochets(cargo_item_quantity)).code , (navigo.rm_parentheses_crochets(cargo_item_quantity_equivalent)).code , (navigo.rm_parentheses_crochets(cargo_item_quantity_equivalent_u)).code , (navigo.rm_parentheses_crochets(cargo_item_quantity_metrical)).code , (navigo.rm_parentheses_crochets(cargo_item_quantity_metrical_u)).code , (navigo.rm_parentheses_crochets(cargo_item_quantity_u)).code , (navigo.rm_parentheses_crochets(cargo_owner_class)).code , (navigo.rm_parentheses_crochets(cargo_owner_location)).code , (navigo.rm_parentheses_crochets(cargo_owner_name)).code , (navigo.rm_parentheses_crochets(commodity_first_word)).code , (navigo.rm_parentheses_crochets(commodity_id)).code , (navigo.rm_parentheses_crochets(commodity_number_cases)).code , (navigo.rm_parentheses_crochets(commodity_number_words)).code , (navigo.rm_parentheses_crochets(commodity_permanent_coding)).code , (navigo.rm_parentheses_crochets(commodity_purpose)).code , (navigo.rm_parentheses_crochets(commodity_second_word)).code , (navigo.rm_parentheses_crochets(commodity_standardisation_transfer)).code , (navigo.rm_parentheses_crochets(commodity_standardized)).code , (navigo.rm_parentheses_crochets(commodity_third_word)).code , (navigo.rm_parentheses_crochets(composite_commostand_commodity)).code , (navigo.rm_parentheses_crochets(composite_point_action_quantity_unit)).code , (navigo.rm_parentheses_crochets(desambiguation_marker_01)).code , (navigo.rm_parentheses_crochets(desambiguation_marker_02)).code , (navigo.rm_parentheses_crochets(desambiguation_marker_03)).code , (navigo.rm_parentheses_crochets(desambiguation_marker_global)).code , (navigo.rm_parentheses_crochets(documentary_unit_id)).code , (navigo.rm_parentheses_crochets(documentary_unit_id_integration)).code , (navigo.rm_parentheses_crochets(final_destination)).code , (navigo.rm_parentheses_crochets(global_01)).code , (navigo.rm_parentheses_crochets(global_02)).code , (navigo.rm_parentheses_crochets(global_03)).code , (navigo.rm_parentheses_crochets(global_04)).code , (navigo.rm_parentheses_crochets(global_05)).code , (navigo.rm_parentheses_crochets(inconsistency_marker)).code , (navigo.rm_parentheses_crochets(link_to_all)).code , (navigo.rm_parentheses_crochets(link_to_first_point)).code , (navigo.rm_parentheses_crochets(link_to_observation)).code , (navigo.rm_parentheses_crochets(link_to_signature)).code , (navigo.rm_parentheses_crochets(point_id_integration)).code , (navigo.rm_parentheses_crochets(point_name)).code , (navigo.rm_parentheses_crochets(point_uhgs_id)).code , (navigo.rm_parentheses_crochets(pointcall_id)).code , (navigo.rm_parentheses_crochets(q01)).code , (navigo.rm_parentheses_crochets(q01_u)).code , (navigo.rm_parentheses_crochets(q02)).code , (navigo.rm_parentheses_crochets(q02_u)).code , (navigo.rm_parentheses_crochets(q03)).code , (navigo.rm_parentheses_crochets(q03_u)).code , (navigo.rm_parentheses_crochets(record_creation_date)).code , (navigo.rm_parentheses_crochets(record_creation_signature)).code , (navigo.rm_parentheses_crochets(record_id)).code , (navigo.rm_parentheses_crochets(record_id_merging)).code , (navigo.rm_parentheses_crochets(record_last_change_date)).code , (navigo.rm_parentheses_crochets(record_last_change_signature)).code , (navigo.rm_parentheses_crochets(remarks)).code , (navigo.rm_parentheses_crochets(selected_record_rank)).code , (navigo.rm_parentheses_crochets(selected_records_number)).code , (navigo.rm_parentheses_crochets(sending_place)).code , (navigo.rm_parentheses_crochets(sending_place_original)).code , (navigo.rm_parentheses_crochets(source)).code , (navigo.rm_parentheses_crochets(sum_cases)).code , (navigo.rm_parentheses_crochets(sum_quantity)).code , (navigo.rm_parentheses_crochets(sum_quantity_metric)).code , (navigo.rm_parentheses_crochets(tasks)).code , (navigo.rm_parentheses_crochets(translation_variants_01)).code , (navigo.rm_parentheses_crochets(translation_variants_02)).code , (navigo.rm_parentheses_crochets(translation_variants_03)).code , (navigo.rm_parentheses_crochets(value_marker)).code  from navigo.cargo);
insert into navigocheck.check_cargo ( select pkid, (navigo.rm_parentheses_crochets(actor_id)).value , (navigo.rm_parentheses_crochets(actor_id_integration)).value , (navigo.rm_parentheses_crochets(calculation_01)).value , (navigo.rm_parentheses_crochets(calculation_02)).value , (navigo.rm_parentheses_crochets(calculation_03)).value , (navigo.rm_parentheses_crochets(cargo_item_action)).value , (navigo.rm_parentheses_crochets(cargo_item_date_declared)).value , (navigo.rm_parentheses_crochets(cargo_item_id)).value , (navigo.rm_parentheses_crochets(cargo_item_indate)).value , (navigo.rm_parentheses_crochets(cargo_item_indate_relativity)).value , (navigo.rm_parentheses_crochets(cargo_item_outdate)).value , (navigo.rm_parentheses_crochets(cargo_item_outdate_relativity)).value , (navigo.rm_parentheses_crochets(cargo_item_quantity)).value , (navigo.rm_parentheses_crochets(cargo_item_quantity_equivalent)).value , (navigo.rm_parentheses_crochets(cargo_item_quantity_equivalent_u)).value , (navigo.rm_parentheses_crochets(cargo_item_quantity_metrical)).value , (navigo.rm_parentheses_crochets(cargo_item_quantity_metrical_u)).value , (navigo.rm_parentheses_crochets(cargo_item_quantity_u)).value , (navigo.rm_parentheses_crochets(cargo_owner_class)).value , (navigo.rm_parentheses_crochets(cargo_owner_location)).value , (navigo.rm_parentheses_crochets(cargo_owner_name)).value , (navigo.rm_parentheses_crochets(commodity_first_word)).value , (navigo.rm_parentheses_crochets(commodity_id)).value , (navigo.rm_parentheses_crochets(commodity_number_cases)).value , (navigo.rm_parentheses_crochets(commodity_number_words)).value , (navigo.rm_parentheses_crochets(commodity_permanent_coding)).value , (navigo.rm_parentheses_crochets(commodity_purpose)).value , (navigo.rm_parentheses_crochets(commodity_second_word)).value , (navigo.rm_parentheses_crochets(commodity_standardisation_transfer)).value , (navigo.rm_parentheses_crochets(commodity_standardized)).value , (navigo.rm_parentheses_crochets(commodity_third_word)).value , (navigo.rm_parentheses_crochets(composite_commostand_commodity)).value , (navigo.rm_parentheses_crochets(composite_point_action_quantity_unit)).value , (navigo.rm_parentheses_crochets(desambiguation_marker_01)).value , (navigo.rm_parentheses_crochets(desambiguation_marker_02)).value , (navigo.rm_parentheses_crochets(desambiguation_marker_03)).value , (navigo.rm_parentheses_crochets(desambiguation_marker_global)).value , (navigo.rm_parentheses_crochets(documentary_unit_id)).value , (navigo.rm_parentheses_crochets(documentary_unit_id_integration)).value , (navigo.rm_parentheses_crochets(final_destination)).value , (navigo.rm_parentheses_crochets(global_01)).value , (navigo.rm_parentheses_crochets(global_02)).value , (navigo.rm_parentheses_crochets(global_03)).value , (navigo.rm_parentheses_crochets(global_04)).value , (navigo.rm_parentheses_crochets(global_05)).value , (navigo.rm_parentheses_crochets(inconsistency_marker)).value , (navigo.rm_parentheses_crochets(link_to_all)).value , (navigo.rm_parentheses_crochets(link_to_first_point)).value , (navigo.rm_parentheses_crochets(link_to_observation)).value , (navigo.rm_parentheses_crochets(link_to_signature)).value , (navigo.rm_parentheses_crochets(point_id_integration)).value , (navigo.rm_parentheses_crochets(point_name)).value , (navigo.rm_parentheses_crochets(point_uhgs_id)).value , (navigo.rm_parentheses_crochets(pointcall_id)).value , (navigo.rm_parentheses_crochets(q01)).value , (navigo.rm_parentheses_crochets(q01_u)).value , (navigo.rm_parentheses_crochets(q02)).value , (navigo.rm_parentheses_crochets(q02_u)).value , (navigo.rm_parentheses_crochets(q03)).value , (navigo.rm_parentheses_crochets(q03_u)).value , (navigo.rm_parentheses_crochets(record_creation_date)).value , (navigo.rm_parentheses_crochets(record_creation_signature)).value , (navigo.rm_parentheses_crochets(record_id)).value , (navigo.rm_parentheses_crochets(record_id_merging)).value , (navigo.rm_parentheses_crochets(record_last_change_date)).value , (navigo.rm_parentheses_crochets(record_last_change_signature)).value , (navigo.rm_parentheses_crochets(remarks)).value , (navigo.rm_parentheses_crochets(selected_record_rank)).value , (navigo.rm_parentheses_crochets(selected_records_number)).value , (navigo.rm_parentheses_crochets(sending_place)).value , (navigo.rm_parentheses_crochets(sending_place_original)).value , (navigo.rm_parentheses_crochets(source)).value , (navigo.rm_parentheses_crochets(sum_cases)).value , (navigo.rm_parentheses_crochets(sum_quantity)).value , (navigo.rm_parentheses_crochets(sum_quantity_metric)).value , (navigo.rm_parentheses_crochets(tasks)).value , (navigo.rm_parentheses_crochets(translation_variants_01)).value , (navigo.rm_parentheses_crochets(translation_variants_02)).value , (navigo.rm_parentheses_crochets(translation_variants_03)).value , (navigo.rm_parentheses_crochets(value_marker)).value  from navigo.cargo);

insert into navigo.uncertainity_component_description ( select pkid, (navigo.rm_parentheses_crochets(calculation_01)).code , (navigo.rm_parentheses_crochets(calculation_02)).code , (navigo.rm_parentheses_crochets(calculation_03)).code , (navigo.rm_parentheses_crochets(component_content)).code , (navigo.rm_parentheses_crochets(component_date_final)).code , (navigo.rm_parentheses_crochets(component_date_initial)).code , (navigo.rm_parentheses_crochets(component_id)).code , (navigo.rm_parentheses_crochets(component_layout)).code , (navigo.rm_parentheses_crochets(component_remarks)).code , (navigo.rm_parentheses_crochets(component_short_title)).code , (navigo.rm_parentheses_crochets(component_source)).code , (navigo.rm_parentheses_crochets(component_source_full)).code , (navigo.rm_parentheses_crochets(component_source_link)).code , (navigo.rm_parentheses_crochets(component_title)).code , (navigo.rm_parentheses_crochets(components_linked)).code , (navigo.rm_parentheses_crochets(global_01)).code , (navigo.rm_parentheses_crochets(global_02)).code , (navigo.rm_parentheses_crochets(link_to_grouping)).code , (navigo.rm_parentheses_crochets(link_to_source)).code , (navigo.rm_parentheses_crochets(link_to_title)).code , (navigo.rm_parentheses_crochets(linked_array_id)).code , (navigo.rm_parentheses_crochets(record_creation_date)).code , (navigo.rm_parentheses_crochets(record_creation_signature)).code , (navigo.rm_parentheses_crochets(record_id)).code , (navigo.rm_parentheses_crochets(record_last_change_date)).code , (navigo.rm_parentheses_crochets(record_last_change_signature)).code , (navigo.rm_parentheses_crochets(selected_record_rank)).code , (navigo.rm_parentheses_crochets(selected_records_number)).code , (navigo.rm_parentheses_crochets(suite_class)).code , (navigo.rm_parentheses_crochets(suite_content)).code , (navigo.rm_parentheses_crochets(suite_date_final)).code , (navigo.rm_parentheses_crochets(suite_date_initial)).code , (navigo.rm_parentheses_crochets(suite_id)).code , (navigo.rm_parentheses_crochets(suite_remarks)).code , (navigo.rm_parentheses_crochets(suite_title)).code , (navigo.rm_parentheses_crochets(task)).code  from navigo.component_description);
insert into navigocheck.check_component_description ( select pkid, (navigo.rm_parentheses_crochets(calculation_01)).value , (navigo.rm_parentheses_crochets(calculation_02)).value , (navigo.rm_parentheses_crochets(calculation_03)).value , (navigo.rm_parentheses_crochets(component_content)).value , (navigo.rm_parentheses_crochets(component_date_final)).value , (navigo.rm_parentheses_crochets(component_date_initial)).value , (navigo.rm_parentheses_crochets(component_id)).value , (navigo.rm_parentheses_crochets(component_layout)).value , (navigo.rm_parentheses_crochets(component_remarks)).value , (navigo.rm_parentheses_crochets(component_short_title)).value , (navigo.rm_parentheses_crochets(component_source)).value , (navigo.rm_parentheses_crochets(component_source_full)).value , (navigo.rm_parentheses_crochets(component_source_link)).value , (navigo.rm_parentheses_crochets(component_title)).value , (navigo.rm_parentheses_crochets(components_linked)).value , (navigo.rm_parentheses_crochets(global_01)).value , (navigo.rm_parentheses_crochets(global_02)).value , (navigo.rm_parentheses_crochets(link_to_grouping)).value , (navigo.rm_parentheses_crochets(link_to_source)).value , (navigo.rm_parentheses_crochets(link_to_title)).value , (navigo.rm_parentheses_crochets(linked_array_id)).value , (navigo.rm_parentheses_crochets(record_creation_date)).value , (navigo.rm_parentheses_crochets(record_creation_signature)).value , (navigo.rm_parentheses_crochets(record_id)).value , (navigo.rm_parentheses_crochets(record_last_change_date)).value , (navigo.rm_parentheses_crochets(record_last_change_signature)).value , (navigo.rm_parentheses_crochets(selected_record_rank)).value , (navigo.rm_parentheses_crochets(selected_records_number)).value , (navigo.rm_parentheses_crochets(suite_class)).value , (navigo.rm_parentheses_crochets(suite_content)).value , (navigo.rm_parentheses_crochets(suite_date_final)).value , (navigo.rm_parentheses_crochets(suite_date_initial)).value , (navigo.rm_parentheses_crochets(suite_id)).value , (navigo.rm_parentheses_crochets(suite_remarks)).value , (navigo.rm_parentheses_crochets(suite_title)).value , (navigo.rm_parentheses_crochets(task)).value  from navigo.component_description);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

--------------
-- controle 0  : il y a plus qu'un datablock_leader__captain_name par documentary_unit_id
--------------

select documentary_unit_id, count(distinct datablock_leader__captain_name ) 
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
group by documentary_unit_id
having count(distinct datablock_leader__captain_name)  > 1
-- 0 lignes

--------------
-- controle 1 : il y a plus qu'un capitaine_name par documentary_unit_id
--------------

select documentary_unit_id, count(distinct captain_name ) 
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
group by documentary_unit_id
having count(distinct captain_name)  > 1
-- 4830 lignes
-- "00011065"

"00283585"
select * from navigo.pointcall where documentary_unit_id='00149787'
"00011065";"Sourissau, Jean"
"00011065";"(Sourisseau, Jean)"
select * from navigocheck.check_pointcall where documentary_unit_id='00283585'

select * from navigocheck.check_pointcall where documentary_unit_id='00062297'
"Sauze Louis"
"Sauze Louis "


""

select documentary_unit_id, count(distinct captain_local_id) 
from navigo.pointcall p
group by documentary_unit_id
having count(distinct captain_local_id)  > 1
-- 397 lignes

select captain_local_id, captain_name , * from navigo.pointcall 
where documentary_unit_id='00000001'
/*
"00010392";"Dent, John H."
"(00010392)";"(Dent, John H.)"
"(00010392)";"(Dent, John H.)"
*/


select captain_local_id, captain_name , * from navigocheck.check_pointcall
where documentary_unit_id in ('00061805', '00105181')
/*
"0019079N";"Griffing, Joseph"
"00010944";"Griffing, Joseph"
"00000706";"Cornu, Robert"
"00001056";"Cornu, Robert"
*/


--------------
-- controle 2 : il y a plus qu'un captain_local_id par  datablock_leader__captain_name
--------------

select datablock_leader__captain_name, count(distinct captain_local_id ) , array_agg(documentary_unit_id), array_agg(record_id)
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
group by datablock_leader__captain_name
having count(distinct captain_local_id)  > 1
-- 1070 lignes

select *  from navigocheck.check_pointcall p
where documentary_unit_id in ('00126222','00130968','00130968','00126222')


insert into navigo.navigo_qualification 
(
select 'pointcall', 'datablock_leader__captain_name', p.pkid, p.documentary_unit_id::int, p.record_id::int, null, p.datablock_leader__captain_name as original_value, null as final_value, 
3, '3.01 datablock_leader__captain_name est associé à plusieurs captain_local_id', to_json('{"captain_local_id" : '||k.link|| '}') , '300 Décompte du nombre de captain_local_id par datablock_leader__captain_name', now(), null, null
from navigocheck.check_pointcall p,
	(select datablock_leader__captain_name, count(distinct captain_local_id ), array_to_json(array_agg(distinct captain_local_id)) as link , array_agg(documentary_unit_id), array_agg(record_id) as records
	from navigocheck.check_pointcall p
	where component_description__suite_id in ('00000003' , '00000074')
	group by datablock_leader__captain_name
	having count(distinct captain_local_id)  > 1) as k
-- where p.record_id in array_to_string (k.records, ',') -- unnest(k.records)
where p.datablock_leader__captain_name = k.datablock_leader__captain_name
) -- 12154 ligne
-- Query returned successfully: 12154 rows affected, 703 msec execution time.

--------------
-- controle 3 : il y a plus qu'un datablock_leader__captain_name par captain_local_id
--------------

select captain_local_id, count(distinct datablock_leader__captain_name), array_agg(documentary_unit_id), array_agg(record_id)
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
group by captain_local_id
having count(distinct datablock_leader__captain_name)  > 1
-- 4403 lignes


select *  from navigocheck.check_pointcall p
where documentary_unit_id in ('00105474','00105474','00105619','00105619','00106727','00106727') 
order by pointcall_date

insert into navigo.navigo_qualification 
(
select 'pointcall', 'captain_local_id', p.pkid, p.documentary_unit_id::int, p.record_id::int, null, p.datablock_leader__captain_name as original_value, null as final_value, 
3, '3.01   captain_local_id est associé à plusieurs datablock_leader__captain_name', to_json('{"datablock_leader__captain_name" : '||k.link|| '}') , '301 Décompte du nombre de datablock_leader__captain_name par captain_local_id', now(), null, null
from navigocheck.check_pointcall p,
	(select captain_local_id, count(distinct datablock_leader__captain_name), array_to_json(array_agg(distinct datablock_leader__captain_name)) as link
	from navigocheck.check_pointcall p
	where component_description__suite_id in ('00000003' , '00000074')
	group by captain_local_id
	having count(distinct datablock_leader__captain_name)  > 1) as k
where p.captain_local_id = k.captain_local_id
)
--- pas inséré (49717 lignes...)

--------------
-- controle 4 : Captain_local_id n'est pas un entier
--------------

select * from navigocheck.check_pointcall   
where Captain_local_id is not null and component_description__suite_id in ('00000003' , '00000074')
and navigo.test_int_type(Captain_local_id) is false
-- 14 lignes

insert into navigo.navigo_qualification 
(select 'pointcall', 'Captain_local_id', pkid, documentary_unit_id::int, record_id::int, null, Captain_local_id as original_value, null as final_value, 4, '4.01 Bad type of field - integer is expected', null, '000 Vérification de type', now(), null, null
from navigocheck.check_pointcall   
where Captain_local_id is not null and component_description__suite_id in ('00000003' , '00000074')
and navigo.test_int_type(Captain_local_id) is false
)
-- Query returned successfully: 14 rows affected, 703 msec execution time.



--------------
-- controle 5a : ship_tonnage n'est un réel
--------------

select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
ship_tonnage is not null and navigo.test_double_type(ship_tonnage) is false  
-- 98 lignes


insert into navigo.navigo_qualification 
(select 'pointcall', 'ship_tonnage', pkid, documentary_unit_id::int, record_id::int, null, ship_tonnage as original_value, null as final_value, 
4, '4.01 Bad type of field - float is expected', null, '000 Vérification de type', now(), null, null
from navigocheck.check_pointcall   
where component_description__suite_id in ('00000003' , '00000074') and
ship_tonnage is not null and navigo.test_double_type(ship_tonnage) is false  
)
-- Query returned successfully: 98 rows affected, 2.5 secs execution time.

--------------
-- controle 5b : guns n'est pas un entier
--------------

select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
guns is not null and navigo.test_int_type(guns) is false  
-- 0 lignes

--------------
-- controle 5b : Ship_increw n'est pas un entier
--------------
select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
Ship_increw is not null and navigo.test_int_type(Ship_increw) is false  
-- 5 lignes


insert into navigo.navigo_qualification 
(select 'pointcall', 'ship_increw', pkid, documentary_unit_id::int, record_id::int, null, ship_increw as original_value, null as final_value, 
4, '4.01 Bad type of field - int is expected', null, '000 Vérification de type', now(), null, null
from navigocheck.check_pointcall   
where component_description__suite_id in ('00000003' , '00000074') and
Ship_increw is not null and navigo.test_int_type(Ship_increw) is false  
)
-- Query returned successfully: 98 rows affected, 2.5 secs execution time.

--------------
-- controle 5b : Ship_outcrew n'est pas un entier
--------------
select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
Ship_outcrew is not null and navigo.test_int_type(Ship_outcrew) is false  
-- 12 lignes


insert into navigo.navigo_qualification 
(select 'pointcall', 'Ship_outcrew', pkid, documentary_unit_id::int, record_id::int, null, Ship_outcrew as original_value, null as final_value, 
4, '4.01 Bad type of field - int is expected', null, '000 Vérification de type', now(), null, null
from navigocheck.check_pointcall   
where component_description__suite_id in ('00000003' , '00000074') and
Ship_outcrew is not null and navigo.test_int_type(Ship_outcrew) is false  
)
-- Query returned successfully: 12 rows affected, 2.5 secs execution time.

--------------
-- controle 5c : record_id n'est un int
--------------

select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
record_id is not null and navigo.test_int_type(record_id) is false  
-- 98 lignes

--------------
-- controle 5d : Stage_calculated_duration_next n'est un réel
--------------

select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
Stage_calculated_duration_next is not null and navigo.test_double_type(Stage_calculated_duration_next) is false  
--  9397 lignes

insert into navigo.navigo_qualification 
(select 'pointcall', 'Stage_calculated_duration_next', pkid, documentary_unit_id::int, record_id::int, null, Stage_calculated_duration_next as original_value, null as final_value, 
4, '4.01 Bad type of field - float is expected', null, '000 Vérification de type', now(), null, null
from navigocheck.check_pointcall   
where component_description__suite_id in ('00000003' , '00000074') and
Stage_calculated_duration_next is not null and navigo.test_double_type(regexp_replace(Stage_calculated_duration_next, ',', '.')) is false  
)
-- Query returned successfully: 0 rows affected, 2.5 secs execution time.



--------------
-- controle 5f : route_duration et route_calculated_duration n'est pas un réel
--------------

select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
route_duration is not null and navigo.test_double_type(route_duration) is false  
--  0 lignes

select * from navigocheck.check_pointcall   
where  component_description__suite_id in ('00000003' , '00000074') and
route_calculated_duration is not null and navigo.test_double_type(route_calculated_duration) is false  
--  0 lignes





--------------
-- controle 6 : datablock_leader__ship_tonnage <> ship_tonnage
--------------
select * from navigocheck.check_pointcall   
where datablock_leader__ship_tonnage <> ship_tonnage and component_description__suite_id in ('00000003' , '00000074')
-- 467 lignes

insert into navigo.navigo_qualification 
(select 'pointcall', 'datablock_leader__ship_tonnage', pkid, documentary_unit_id::int, record_id::int, null, datablock_leader__ship_tonnage as original_value, null as final_value, 
3, '3.01 datablock_leader__ship_tonnage <> ship_tonnage', to_json('{"ship_tonnage" : "'||ship_tonnage|| '"}'), 'Egalité entre les 2 attributs', now(), null, null
from navigocheck.check_pointcall   
where component_description__suite_id in ('00000003' , '00000074') and
datablock_leader__ship_tonnage <> ship_tonnage 
)

--------------
-- controle 7 : datablock_leader__ship_homeport <> ship_homeport
--------------
select * from navigocheck.check_pointcall   
where datablock_leader__ship_homeport <> ship_homeport and component_description__suite_id in ('00000003' , '00000074')
-- 498 lignes

insert into navigo.navigo_qualification 
(select 'pointcall', 'datablock_leader__ship_homeport', pkid, documentary_unit_id::int, record_id::int, null, datablock_leader__ship_tonnage as original_value, null as final_value, 
3, '3.01 datablock_leader__ship_homeport <> ship_homeport', to_json('{"ship_homeport" : "'||ship_homeport|| '"}'), 'Egalité entre les 2 attributs', now(), null, null
from navigocheck.check_pointcall   
where component_description__suite_id in ('00000003' , '00000074') and
datablock_leader__ship_homeport <> ship_homeport 
)
-- Query returned successfully: 498 rows affected, 172 msec execution time.

--------------
-- controle 8 : vérifier  geo_general.pointcall_name = ship_homeport via Ship_homeport_id = geo_general.pointcall_uhgs_id
--------------

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.ship_homeport_id  is not null and p.ship_homeport is not null and
p.pkid not in (
select p.pkid 
from navigocheck.check_pointcall p,    navigocheck.geo_general g
where component_description__suite_id in ('00000003' , '00000074') and
p.ship_homeport_id = g.pointcall_uhgs_id and upper(trim(p.ship_homeport)) = upper(trim(g.pointcall_name)) 
)

"Saint Savinien";"A0147182"
"L' Isle au Moine";"A0185539"
"La Berlduque";"A0140339"
"L' Isle au Moine";"A0185539"
"Boursefranc";"A0145182"
A0137148
select * from navigocheck.geo_general g where pointcall_uhgs_id = 'A0147182' -- rien
select * from navigocheck.geo_general g where pointcall_uhgs_id = 'A0185539' -- "Ile aux Moines";"A0185539"
select * from navigocheck.geo_general g where pointcall_uhgs_id = 'A0140339' -- "Aberildut" ou "Aber Ildut"
select * from navigocheck.geo_general g where pointcall_uhgs_id = 'A0145182' -- "Bourcefranc"
select array_agg(pointcall_name) from navigocheck.geo_general g where pointcall_uhgs_id = 'A0137148' -- "{"Les Sables d' Olonne","Sables d' Olones","Sables [d' Olonne]","Sables d' Olonne"}"


alter table navigocheck.check_pointcall add constraint check_pointcall_pk primary key (pkid);

insert into navigo.navigo_qualification 
(select 'pointcall', 'ship_homeport_id', pkid, documentary_unit_id::int, record_id::int, null, ship_homeport_id as original_value, null as final_value, 
4, '4.10 ship_homeport n'' pas d''entree dans geogeneral ou pas avec cette orthographe', to_json('{"ship_homeport" : "'||ship_homeport|| '"}'), 'Cohérence entre geo_general et les ship_homeport_id', now(), null, null
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.ship_homeport_id  is not null and p.ship_homeport is not null and
	p.pkid not in (
	select p.pkid 
	from navigocheck.check_pointcall p,    navigocheck.geo_general g
	where component_description__suite_id in ('00000003' , '00000074') and
	p.ship_homeport_id = g.pointcall_uhgs_id and upper(trim(p.ship_homeport)) = upper(trim(g.pointcall_name)) 
	)
)

--------------
-- controle 9 : net_route_marker <> A, Z ou T
--------------

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.net_route_marker  is not null and net_route_marker not in ('A', 'Z', 'T')

insert into navigo.navigo_qualification 
(select 'pointcall', 'net_route_marker', pkid, documentary_unit_id::int, record_id::int, null, net_route_marker as original_value, null as final_value, 
4, '4.09 net_route_marker doit valoir A, Z, ou T', null, 'Contrôle de type énuméré', now(), null, null
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
and  p.net_route_marker  is not null and net_route_marker not in ('A', 'Z', 'T')
)

insert into navigo.navigo_qualification 
(select 'pointcall', 'net_route_marker', pkid, documentary_unit_id::int, record_id::int, null, net_route_marker as original_value, null as final_value, 
4, '4.09 net_route_marker doit valoir A ou Z', null, 'Contrôle de type énuméré', now(), null, null
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
and  p.net_route_marker  is not null and net_route_marker ='T'
)
select count(*) from  navigo.navigo_qualification where navigo_field='net_route_marker'
update navigo.navigo_qualification set flag2='4.09 net_route_marker doit valoir A ou Z' where navigo_field='net_route_marker' 

--------------
-- controle 10 : pointcall_indate < observation_date < pointcall_outdate
--------------

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.observation_date  is not null and observation_date > pointcall_outdate 
"1787>01>28!"  et "1787=01=28"

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.observation_date  is not null and pointcall_indate > pointcall_outdate 


insert into navigo.navigo_qualification 
(select 'pointcall', 'pointcall_indate', pkid, documentary_unit_id::int, record_id::int, null, pointcall_indate as original_value, pointcall_outdate as final_value, 
3, '3.10 pointcall_indate > pointcall_outdate ', null, 'cohérence de date', now(), null, null
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
and  p.pointcall_indate  is not null and pointcall_indate > pointcall_outdate 
)

--------------
-- controle 11 : pointcall_function cohérent avec data_block_marker
--------------

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and   pointcall_function = 'O' and data_block_leader_marker!='A'
-- 0 lignes

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and   pointcall_function = 'T' and data_block_leader_marker!='T'
-- 0 ligne


--------------
-- controle 12 : vérifier  geo_general.pointcall_name = pointcall_name via pointcall_UGHS_id = geo_general.pointcall_uhgs_id
--------------
select count(*) from navigocheck.check_pointcall p where pointcall_UHGS_id is not null

select * from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.pointcall_UHGS_id  is not null and p.pointcall_name is not null and
p.pkid not in (
select p.pkid 
from navigocheck.check_pointcall p,    navigocheck.geo_general g
where component_description__suite_id in ('00000003' , '00000074') and
p.pointcall_UHGS_id = g.pointcall_uhgs_id and upper(trim(p.pointcall_name)) = upper(trim(g.pointcall_name)) 
)

"Saint Savinien"; "A0147182"
"Anse de Mourjerol"; "H4444444"
"Mourjerol (Anse de)"; "H4444444"
"Divers"; "A9999997"
"Divers ports"; "A9999997"


select * from navigocheck.geo_general g where pointcall_uhgs_id = 'A0147182' -- rien
select * from navigocheck.geo_general g where pointcall_uhgs_id = 'H4444444' -- rien
select * from navigocheck.geo_general g where pointcall_uhgs_id = 'A9999997' -- rien


select array_agg(pointcall_name) from navigocheck.geo_general g where pointcall_uhgs_id = 'A0137148' -- "{"Les Sables d' Olonne","Sables d' Olones","Sables [d' Olonne]","Sables d' Olonne"}"

insert into navigo.navigo_qualification 
(select 'pointcall', 'pointcall_UHGS_id', pkid, documentary_unit_id::int, record_id::int, null, pointcall_UHGS_id as original_value, null as final_value, 
4, '4.12 pointcall_name n'' pas d''entree dans geogeneral ou pas avec cette orthographe', to_json('{"pointcall_name" : "'||pointcall_name|| '"}'), 'Cohérence entre geo_general et les pointcall_UHGS_id', now(), null, null
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and  p.pointcall_UHGS_id  is not null and p.pointcall_name is not null and
	p.pkid not in (
	select p.pkid 
	from navigocheck.check_pointcall p,    navigocheck.geo_general g
	where component_description__suite_id in ('00000003' , '00000074') and
	p.pointcall_UHGS_id = g.pointcall_uhgs_id and upper(trim(p.pointcall_name)) = upper(trim(g.pointcall_name)) 
	)
)
-- 418 lignes


--------------
-- controle 13 : vérifier  stage_id = pointcall_UHGS_id|| next_pointcall_UHGS_id 
--------------
select count(*) from navigocheck.check_pointcall p where pointcall_UHGS_id is not null

select p.source, p.data_block_local_id, 
previous.record_id as record_1, previous.pointcall_name as name_1, previous.pointcall_function as function_1, previous.pointcall_indate as indate_1, previous.pointcall_outdate as outdate_1,  
p.record_id as record_2, p.pointcall_name as name_2, p.pointcall_function as function_2, p.pointcall_indate as indate_2, p.pointcall_outdate as outdate_2, 
p.record_id, previous.pointcall_rank as rang_1, previous.net_route_marker as netmarker_1, p.pointcall_rank as rang2,  p.net_route_marker as netmarker_2, 
previous.pointcall_UHGS_id as UHGSid_1, p.pointcall_UHGS_id as UHGSid_2, previous.stage_id 
from navigocheck.check_pointcall p, 
	(select data_block_local_id, record_id, pointcall_function, pointcall_UHGS_id, pointcall_rank, net_route_marker, stage_id, pointcall_indate, pointcall_outdate, pointcall_name 
	from navigocheck.check_pointcall p
	where component_description__suite_id in ('00000003' , '00000074')
	and pointcall_function<> 'T') as previous
where previous.data_block_local_id = p.data_block_local_id 
and previous.pointcall_rank::float+1 = p.pointcall_rank::float
and previous.pointcall_UHGS_id||p.pointcall_UHGS_id <> previous.stage_id 
-- 23 lignes


select p.source, p.data_block_local_id, 
previous.record_id as record_1, previous.pointcall_name as name_1, previous.pointcall_function as function_1, previous.pointcall_indate as indate_1, previous.pointcall_outdate as outdate_1,  
p.record_id as record_2, p.pointcall_name as name_2, p.pointcall_function as function_2, p.pointcall_indate as indate_2, p.pointcall_outdate as outdate_2, 
p.record_id, previous.pointcall_rank as rang_1, previous.net_route_marker as netmarker_1, p.pointcall_rank as rang2,  p.net_route_marker as netmarker_2, 
previous.pointcall_UHGS_id as UHGSid_1, p.pointcall_UHGS_id as UHGSid_2, previous.stage_relooked 
from navigocheck.check_pointcall p, 
	(select data_block_local_id, record_id, pointcall_function, pointcall_UHGS_id, pointcall_rank, net_route_marker, stage_relooked, pointcall_indate, pointcall_outdate, pointcall_name 
	from navigocheck.check_pointcall p
	where component_description__suite_id in ('00000003' , '00000074')
	and pointcall_function<> 'T') as previous
where previous.data_block_local_id = p.data_block_local_id 
and previous.pointcall_rank::float+1 = p.pointcall_rank::float
and previous.pointcall_UHGS_id||p.pointcall_UHGS_id <> previous.stage_relooked 
-- 2312

select distinct navigo_field, flag2 from navigo.navigo_qualification where navigo_field = 'stage_relooked'
delete from navigo.navigo_qualification where navigo_field = 'stage_relooked'
--
insert into navigo.navigo_qualification 
(select 'pointcall', 'stage_relooked', pkid, p.documentary_unit_id::int, previous.record_id::int, null, previous.stage_relooked as original_value, previous.pointcall_UHGS_id||p.pointcall_UHGS_id as final_value, 
3, '3.13 stage_relooked n''est pas cohérent dans ce block avec le code UHGS_id des deux points composants l''étape (si le rank est considéré OK)', 
to_json('{"pointcall_UHGS_id_1" : "'||previous.pointcall_UHGS_id||'", "pointcall_UHGS_id_2" : "'||p.pointcall_UHGS_id||'", "record_id_1" : "'||previous.record_id||'", "record_id_2" : "'||p.record_id||'"}'), 'Cohérence entre les stage_id et les pointcall_UHGS_id, si rank est OK ', now(), null, null
from navigocheck.check_pointcall p, 
	(select data_block_local_id, documentary_unit_id, record_id, pointcall_function, pointcall_UHGS_id, pointcall_rank, net_route_marker, stage_relooked, pointcall_indate, pointcall_outdate, pointcall_name 
	from navigocheck.check_pointcall p
	where component_description__suite_id in ('00000003' , '00000074')
	and pointcall_function<> 'T') as previous
where previous.data_block_local_id = p.data_block_local_id 
and previous.pointcall_rank::float+1 = p.pointcall_rank::float
and previous.pointcall_UHGS_id||p.pointcall_UHGS_id <> previous.stage_relooked 
)
-- Query returned successfully: 2312 rows affected, 473 msec execution time.


insert into navigo.navigo_qualification 
(select 'pointcall', 'stage_id', pkid, p.documentary_unit_id::int, previous.record_id::int, null, previous.stage_id as original_value, previous.pointcall_UHGS_id||p.pointcall_UHGS_id as final_value, 
3, '3.13 stage_id n''est pas cohérent dans ce block avec le code UHGS_id des deux points composants l''étape (si le rank est considéré OK)', 
to_json('{"pointcall_UHGS_id_1" : "'||previous.pointcall_UHGS_id||'", "pointcall_UHGS_id_2" : "'||p.pointcall_UHGS_id||'", "record_id_1" : "'||previous.record_id||'", "record_id_2" : "'||p.record_id||'"}'), 'Cohérence entre les stage_id et les pointcall_UHGS_id, si rank est OK ', now(), null, null
from navigocheck.check_pointcall p, 
	(select data_block_local_id, documentary_unit_id, record_id, pointcall_function, pointcall_UHGS_id, pointcall_rank, net_route_marker, stage_id, pointcall_indate, pointcall_outdate, pointcall_name 
	from navigocheck.check_pointcall p
	where component_description__suite_id in ('00000003' , '00000074')
	and pointcall_function<> 'T') as previous
where previous.data_block_local_id = p.data_block_local_id 
and previous.pointcall_rank::float+1 = p.pointcall_rank::float
and previous.pointcall_UHGS_id||p.pointcall_UHGS_id <> previous.stage_id 
)
-- 23 lignes


--------------
-- controle 14 : vérifier  que par captain_local_id, on a un birthplace et un citizenship
--------------
select captain_local_id,  count(distinct captain_birthplace) , array_agg(captain_birthplace) as captain_birthplaces
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and captain_local_id is not null
group by captain_local_id
having count(distinct captain_birthplace) > 1
-- 372 lignes

select captain_local_id,  count(distinct captain_citizenship) , array_agg(captain_citizenship) as captain_citizenships
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and captain_local_id is not null
group by captain_local_id
having count(distinct captain_citizenship) = 1
-- 326 lignes

--------------
-- controle 15 : vérifier  que par ship_local_id, on a un class et un flag et un tonnage
--------------
select ship_local_id,  count(distinct ship_class) , array_agg(ship_class) as ship_classes
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and ship_local_id is not null
group by ship_local_id
having count(distinct ship_class) > 1
-- 947 lignes / 6340 ok / 8293 lignes vides

select ship_local_id,  count(distinct ship_flag) , array_agg(ship_flag) as ship_flags
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and ship_local_id is not null
group by ship_local_id
having count(distinct ship_flag > 1
-- 503 lignes / 15068 ok

select ship_local_id,  count(distinct ship_tonnage) , array_agg(distinct ship_tonnage) as ship_tonnages, array_agg(distinct data_block_local_id) as data_block_local_ids, array_agg(record_id) as record_ids 
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074') 
and ship_local_id is not null
group by ship_local_id
having count(distinct ship_tonnage) > 1
-- 2071 lignes / 8197 ok



----------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE INDEX check_geo_general_idx ON navigocheck.geo_general(pointcall_uhgs_id);


create extension fuzzystrmatch ; -- OK
create extension pg_trgm; -- OK

CREATE INDEX geo_general_name_trgm_idx
  ON navigocheck.geo_general
  USING gin
  (pointcall_name COLLATE pg_catalog."default" gin_trgm_ops);
-- OK


CREATE INDEX geo_general_uhgs_id_idx
  ON navigocheck.geo_general
  USING btree
  (pointcall_uhgs_id COLLATE pg_catalog."default");


  CREATE INDEX geo_general_uhgs_id_trigram_idx
  ON navigo.geo_general USING GIST (pointcall_uhgs_id gist_trgm_ops);
-- Query returned successfully with no result in 2.6 secs.

----

CREATE INDEX pointcall__name_trgm_idx
  ON navigocheck.check_pointcall
  USING gin
  (pointcall_name COLLATE pg_catalog."default" gin_trgm_ops);

CREATE INDEX pointcall__name_idx
  ON navigocheck.check_pointcall
  USING btree
  (pointcall_name COLLATE pg_catalog."default");

----
CREATE INDEX pointcall__homeport_trgm_idx
  ON navigocheck.check_pointcall
  USING gin
  (ship_homeport COLLATE pg_catalog."default" gin_trgm_ops);

CREATE INDEX pointcall__ship_homeport_idx
  ON navigocheck.check_pointcall
  USING btree
  (ship_homeport COLLATE pg_catalog."default");

----
CREATE INDEX pointcall_uhgs_id_idx
  ON navigocheck.check_pointcall
  USING btree
  (pointcall_uhgs_id COLLATE pg_catalog."default");

 CREATE INDEX pointcall_uhgs_id_trigram_idx
  ON navigocheck.check_pointcall USING GIST (pointcall_uhgs_id gist_trgm_ops);
  
CREATE INDEX pointcall_ship_homeport_id_idx
  ON navigocheck.check_pointcall
  USING btree
  (ship_homeport_id COLLATE pg_catalog."default");

  CREATE INDEX pointcall_ship_homeport_trigram_idx
  ON navigocheck.check_pointcall USING GIST (ship_homeport_id gist_trgm_ops);

vacuum analyze  
-- Query returned successfully with no result in 14.5 secs.

-- donner les droits d'exécution à navigo sur la fonction similarity
CREATE OR REPLACE FUNCTION public.similarity(text, text)
  RETURNS real AS
'$libdir/pg_trgm', 'similarity'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;
ALTER FUNCTION public.similarity(text, text)
  OWNER TO postgres;

GRANT EXECUTE ON FUNCTION public.similarity(text, text) TO navigo ;



--- 

SELECT p.pointcall_uhgs_id, p.pointcall_name, g.pointcall_uhgs_id, g.pointcall_name, similarity(p.pointcall_name, g.pointcall_name) AS sml
  FROM navigocheck.check_pointcall p, navigocheck.geo_general g
  WHERE p.pointcall_name % g.pointcall_name 
  and p.pointcall_uhgs_id = g.pointcall_uhgs_id
  -- and similarity(p.pointcall_name, g.pointcall_name) >= 0.625
    ORDER BY sml DESC, p.pointcall_name;

 -- min de similarity : 0.3
    
--- 147222 lignes en 24 s
-- 872120 lignes en 2 min 15 soit 135 s

-----------------------------------------------------------------------------------------------

alter table navigo.uncertainity_acting_parties set schema navigocheck;
alter table navigo.uncertainity_actions set schema navigocheck;
alter table navigo.uncertainity_cargo set schema navigocheck;
alter table navigo.uncertainity_component_description set schema navigocheck;
alter table navigo.uncertainity_pointcall set schema navigocheck;
alter table navigo.uncertainity_taxes set schema navigocheck;

-----------------------------------------------------------------------------------------------

select count(*) from navigocheck.check_pointcall
 where component_description__suite_id in ('00000003' , '00000074')
 and captain_age is not null
-- 3


select count(p.*), count(q.*) from navigocheck.check_pointcall p, navigo.navigo_qualification q
 where component_description__suite_id in ('00000003' , '00000074')
 and Captain_local_id is not null and q.navigo_field = 'Captain_local_id'

 select count(q.*) from  navigo.navigo_qualification q
 where q.navigo_field = 'Captain_local_id'


select distinct navigo_table, navigo_field, flag1, flag2, method
from navigo.navigo_qualification
order by navigo_table

-- For f in fields

select count(*) as total
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
 and p.Captain_local_id is not null
 -- 81980

select navigo_table, navigo_field, flag1, flag2, method, count(*) as erreurs
from navigo.navigo_qualification
group by navigo_table, navigo_field, flag1, flag2, method
order by navigo_table


select count(*) as total
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
 and p.Captain_local_id is not null

select count(*) as total
from navigocheck.check_pointcall p
where component_description__suite_id in ('00000003' , '00000074')
 and p.record_id is not null

 select count(*) as total
from navigo.geo_general p
 where p.geo_general__lat is not null

 

  select count(*) as total
from navigo.geo_general p
 where p.pointcall_name is not null
 
select * from navigo.navigo_qualification where navigo_table = 'pointcall' and navigo_field = 'guns' and flag1<>9  limit 1

select distinct component_description__suite_id from navigo.pointcall where source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%'
ADVar, 7B10/1759
select  distinct component_description__suite_id from navigo.pointcall where source like 'ADVar%' or source like 'ADBdR%'
select count(distinct source) from navigo.pointcall where  component_description__suite_id in ('00000003' , '00000074')
order by source desc

select count(distinct component_description__component_short_title) from navigo.pointcall where  component_description__suite_id in ('00000003' , '00000074')
101
select distinct component_description__component_short_title from navigo.pointcall where  component_description__suite_id in ('00000003' , '00000074')
select distinct component_description__component_short_title from navigo.pointcall where  component_description__suite_id in ('00000003' , '00000074')


select * from navigocheck.check_pointcall  where source='NA, Washington, Bx Consulate, C20, 222 - Rochelle consulate - ADG, Labalengue'
select * from navigocheck.check_pointcall  where source='ADVar, 7B10/1759'
select * from navigocheck.check_pointcall  where source='AN, G-5-50/ - Jeff.Papers, Bondfield/ 20-10-1787'
select * from navigocheck.check_pointcall  where source='AN, G-5-50/'

select distinct source from navigo.pointcall where  component_description__suite_id in ('00000003' , '00000074')
and component_description__component_short_title='Congés, Bordeaux (1787)' order by source desc

select char_length(remarks), remarks, * from navigo.taxes
where (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
and remarks is not null and char_length(remarks)  > 0

-----------------------------------------------------------------------------------------------

select cargo_item_action , count(*) as d
from navigo.cargo
where (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by cargo_item_action
order by d desc


select calculation_02 , count(*) as d
from navigo.cargo
where (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by calculation_02
order by d desc


select distinct link_to_all, link_to_first_point, link_to_observation, link_to_signature
from navigo.cargo
where (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
and inconsistency_marker is not null

select point_id_integration, pointcall_id, point_name, point_uhgs_id from navigo.cargo
where (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
and point_id_integration is not null

-----------------------------------------------------------------------------------------------
-- import d'une table géo_general simplifiée avec les amirautés et provinces, shipping area et variantes de toponymes.
-----------------------------------------------------------------------------------------------
-- export le 26 fev 2019 pour shapefile pour intégrer les amirautés et provinces
select pointcall_uhgs_id, latitude, longitude, null as amiraute, null as province, (array_agg(distinct pointcall_name))[1] as topo1,
(array_agg(distinct pointcall_name))[2] as topo2, (array_agg(distinct pointcall_name))[3] as topo3, (array_agg(distinct pointcall_name))[4] as topo4,
(array_agg(distinct pointcall_name))[5] as topo5, (array_agg(distinct pointcall_name))[6] as topo6, (array_agg(distinct pointcall_name))[7] as topo7,
array_agg(distinct pointcall_name) as toustopos, (array_agg(distinct shippingarea__e1_name))[1] as shippingarea1,
(array_agg(distinct shippingarea__e1_name))[1] as shippingarea2, (array_agg(distinct shippingarea__e1_name))[1] as shippingarea3,
array_agg(distinct shippingarea__e1_name) as tousshippingarea
from navigocheck.geo_general
group by pointcall_uhgs_id, latitude, longitude

-- export le 11 mars 2019 pour CSV pour intégrer les amirautés et provinces
select pointcall_uhgs_id, latitude, longitude, null as amiraute, null as province, (array_agg(distinct pointcall_name))[1] as topo1,
(array_agg(distinct pointcall_name))[2] as topo2, (array_agg(distinct pointcall_name))[3] as topo3, (array_agg(distinct pointcall_name))[4] as topo4,
(array_agg(distinct pointcall_name))[5] as topo5, (array_agg(distinct pointcall_name))[6] as topo6, (array_agg(distinct pointcall_name))[7] as topo7,
array_agg(distinct pointcall_name) as toustopos, (array_agg(distinct shippingarea__e1_name))[1] as shippingarea
from navigocheck.geo_general
group by pointcall_uhgs_id, latitude, longitude

select * from navigoviz.geo_general

select pointcall_uhgs_id, latitude, longitude,  array_agg(distinct pointcall_name), array_agg(distinct shippingarea__e1_name)
from navigoviz.geo_general
group by pointcall_uhgs_id, latitude, longitude

concat(toponyme,  ' \n ', "topo1" ,  ' | ', "topo2" ,' | ', "topo3", ' | ', "topo4", ' | ', "topo5", ' | ', "topo6" , ' | ', "topo7" )

-- import le 27 février des tables enrichies avec les amirautés

export PGCLIENTENCODING=latin1
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_27fev2019/limites_amirautes.shp -a_srs EPSG:3857 -nln admiralty_limits

export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_27fev2019/export_geo_general_27fev2019.shp -append -a_srs EPSG:4326 -nln port_points
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigo" /home/plumegeo/navigo/ETL/data_27fev2019/export_geo_general_27fev2019.shp -append -a_srs EPSG:4326 -nln port_points_backup

export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_27fev2019/GSHHS_f_L1_3857.shp -append -a_srs EPSG:3857 -nln coastal_areas
comment on table navigoviz.coastal_areas is 'Import from NOAH shapefile : GSHHS_f_L1_3857.shp'
-- note : en fait la projection est 4326


pgsql2shp -f "/home/plumegeo/navigo/buffer_1mile.shp" -u navigo -P navigocorpus2018 portic navigoviz.shoreline
pgsql2shp -f "/home/plumegeo/navigo/buffer_1mile.shp" -u navigo -P navigocorpus2018 portic "SELECT ogc_fid, id, region, buffer_1mile FROM navigoviz.shoreline where buffer_1mile is not null"
pgsql2shp -f "/home/plumegeo/navigo/buffer_20mile.shp" -u navigo -P navigocorpus2018 portic "SELECT ogc_fid, id, region, buffer_20mile FROM navigoviz.shoreline where buffer_20mile is not null"
pgsql2shp -f "/home/plumegeo/navigo/shoreline.shp" -u navigo -P navigocorpus2018 portic "SELECT ogc_fid, id, region, shoreline FROM navigoviz.shoreline where shoreline is not null"
pgsql2shp -f "/home/plumegeo/navigo/sea_20mile.shp" -u navigo -P navigocorpus2018 portic "SELECT  id, valid_poly_20_mile FROM navigoviz.sea20mile where valid_poly_20_mile is not null "


-- import le 16 mai des frontières du monde dans public

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_15mai19/TM_WORLD_BORDERS-0.3.shp -append -a_srs EPSG:4326 -nln world_borders_2019 -nlt MULTIPOLYGON

select * from navigoviz.port_points where navigo.test_double_type(topo1) is true

---  test


CREATE OR REPLACE FUNCTION navigo.frequency_topo(uhgs_id text, toustopo text) RETURNS json AS
$$
	import json
	from operator import itemgetter
	global temp
	global result
	global topos
	global query
	if toustopo is not None :
		result = []
		temp = toustopo.replace('{', '').replace('}', '').strip(' ')
		temp = temp.replace('"', '').strip(' ')
		topos = temp.split(',')
		for item in topos:
		    #plpy.notice(item)
		    
		    query = "select count(*) as freq from navigo.geo_general where pointcall_uhgs_id = '"+uhgs_id+"' and trim(pointcall_name)='"+item.replace('\'', '\'\'')+"'"
		    #plpy.notice(query)
		    rv = plpy.execute(query)
		    #plpy.notice(rv[0]["freq"])
		    result.append(dict(topo=item, freq=rv[0]["freq"]))
		result = sorted(result, key=itemgetter('freq'), reverse=True)

	else :
		result = None
	#plpy.notice (result)
	return json.dumps(result, ensure_ascii=False)
$$ LANGUAGE plpython3u;

' -- quote qui améliore la lisibilité du script

ALTER FUNCTION navigo.frequency_topo(text, text) OWNER TO dba;
  
select navigo.frequency_topo('A0199508', '{"La Flotte","la Flotte en Ré","la Flotte-en-Ré","La Flotte [en Ré]","La Flotte en Ré","La Flotte île de Ré","La Flotte [La Flotte en Ré]"}')

select ((navigo.frequency_topo('A0199508', '{"La Flotte","la Flotte en Ré","la Flotte-en-Ré","La Flotte [en Ré]","La Flotte en Ré","La Flotte île de Ré","La Flotte [La Flotte en Ré]"}'))::json->>0)::json->>'topo'


select * from navigocheck.geo_general where pointcall_name % 'Cotes d'' Espagne'
-- "Cote d' Espagne";"A1965062" 
select navigo.frequency_topo(uhgs_id, toustopos) from navigoviz.port_points where uhgs_id = 'A1965062';


select ((navigo.frequency_topo(uhgs_id, toustopos))::json->>0)::json->>'topo' from navigoviz.port_points limit 20 

alter table navigoviz.port_points add column topofreq json;
alter table navigoviz.port_points add column toponyme text;

update navigoviz.port_points set topofreq = navigo.frequency_topo(uhgs_id, toustopos);
-- UPDATE 1492 (sur le serveur : 2 min : psql -U navigo -d portic -c "update navigoviz.port_points set topofreq = navigo.frequency_topo(uhgs_id, toustopos);"
update navigoviz.port_points set toponyme = (topofreq::json->>0)::json->>'topo';
-- UPDATE 1492 (rapide)

alter table navigoviz.port_points drop column  topo1;
alter table navigoviz.port_points drop column  topo2;
alter table navigoviz.port_points drop column  topo3;
alter table navigoviz.port_points drop column  topo4;
alter table navigoviz.port_points drop column  topo5;
alter table navigoviz.port_points drop column  topo6;
alter table navigoviz.port_points drop column  topo7;


select freq, topo from json_to_recordset(navigo.frequency_topo('A0199508', '{"La Flotte","la Flotte en Ré","la Flotte-en-Ré","La Flotte [en Ré]","La Flotte en Ré","La Flotte île de Ré","La Flotte [La Flotte en Ré]"}'))
as x(freq int, topo text) order by freq desc

select uhgs_id, navigo.frequency_topo(uhgs_id, toustopos) from navigoviz.port_points

select * from navigoviz.port_points where amiraute like 'Marennes'
select * from navigoviz.port_points where amiraute like 'La Rochelle'

select * from navigocheck.geo_general where pointcall_uhgs_id='A0215083'

select * from navigoviz.port_points where shiparea is null and province is not null order by province

-- 16 mai 2019 : ajout d'appartenance de pays 2019 (grosso-modo)
alter table navigoviz.port_points add column country2019_name text;
alter table navigoviz.port_points add column country2019_iso2code text;
alter table navigoviz.port_points add column country2019_region text;

update navigoviz.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
from (
select uhgs_id, toponyme, shiparea, iso2, name, region 
from navigoviz.port_points p , navigoviz.world_borders_2019 w
where st_contains(w.wkb_geometry, p.wkb_geometry)
) as k
where ports.uhgs_id = k.uhgs_id
-- Query returned successfully: 950 rows affected, 1.5 secs execution time.

select * from navigoviz.port_points where country2019_iso2code is null
-- 542 lignes : Ribadeo


select uhgs_id, toponyme, shiparea, iso2, name, region 
from navigoviz.port_points p , navigoviz.world_borders_2019 w
where st_intersects(w.wkb_geometry, st_buffer(p.wkb_geometry :: geography, 35000)) and country2019_iso2code is null
order by uhgs_id

alter table navigoviz.port_points add column point3857 geometry;
update navigoviz.port_points set point3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) ;

alter table navigoviz.world_borders_2019 add column mpolygone3857 geometry;
update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where region in (150, 2, 19, 142);
-- 150 : Europe
-- 2 : Afrique
-- 19 : Amériques
-- 142 : Orient / Asie (de la turquie à l'inde)
-- 9 : Océanie

update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where region in (9);

select distinct region from navigoviz.world_borders_2019 where mpolygone3857 is null
-- 0
select * from navigoviz.world_borders_2019   where region = 0

update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where iso2 in ('CC'); 
update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where region = 0 and iso2 <>'AQ'; 

select * from navigoviz.world_borders_2019 where mpolygone3857 is null
-- 145;"AY";"AQ";"ATA";10;"Antarctica";0;0;0;0;21.304;-80.446

update navigoviz.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
from (
select q.*
from 	(
		select uhgs_id, min (d) from
		(
		select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
		from navigoviz.port_points p , navigoviz.world_borders_2019 w
		where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 35000)) and country2019_iso2code is null
		order by uhgs_id
		) as k group by uhgs_id 
	) as k,
	(
	select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
	from navigoviz.port_points p , navigoviz.world_borders_2019 w
	where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 35000)) and country2019_iso2code is null
	order by uhgs_id
	) as q
where q.d = k.min and q.uhgs_id = k.uhgs_id
) as k
where  country2019_name is null and ports.uhgs_id = k.uhgs_id 
-- Query returned successfully: 447 rows affected, 21.9 secs execution time.

select * from navigoviz.port_points where country2019_iso2code is null
-- 95 lignes


update navigoviz.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
from (
select q.*
from 	(
		select uhgs_id, min (d) from
		(
		select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
		from navigoviz.port_points p , navigoviz.world_borders_2019 w
		where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 50000)) and country2019_iso2code is null
		order by uhgs_id
		) as k group by uhgs_id 
	) as k,
	(
	select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
	from navigoviz.port_points p , navigoviz.world_borders_2019 w
	where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 50000)) and country2019_iso2code is null
	order by uhgs_id
	) as q
where q.d = k.min and q.uhgs_id = k.uhgs_id
) as k
where  country2019_name is null and ports.uhgs_id = k.uhgs_id 
-- Query returned successfully: 18 rows affected, 2.4 secs execution time.


update navigoviz.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
from (
select q.*
from 	(
		select uhgs_id, min (d) from
		(
		select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
		from navigoviz.port_points p , navigoviz.world_borders_2019 w
		where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 100000)) and country2019_iso2code is null
		order by uhgs_id
		) as k group by uhgs_id 
	) as k,
	(
	select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
	from navigoviz.port_points p , navigoviz.world_borders_2019 w
	where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 100000)) and country2019_iso2code is null
	order by uhgs_id
	) as q
where q.d = k.min and q.uhgs_id = k.uhgs_id
) as k
where  country2019_name is null and ports.uhgs_id = k.uhgs_id 
-- Query returned successfully: 42 rows affected, 2.0 secs execution time.


update navigoviz.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
from (
select q.*
from 	(
		select uhgs_id, min (d) from
		(
		select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
		from navigoviz.port_points p , navigoviz.world_borders_2019 w
		where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 150000)) and country2019_iso2code is null
		order by uhgs_id
		) as k group by uhgs_id 
	) as k,
	(
	select uhgs_id, toponyme, shiparea, iso2, name, region, st_distance (p.point3857, w.mpolygone3857) as d
	from navigoviz.port_points p , navigoviz.world_borders_2019 w
	where st_intersects(w.mpolygone3857, st_buffer(p.point3857, 150000)) and country2019_iso2code is null
	order by uhgs_id
	) as q
where q.d = k.min and q.uhgs_id = k.uhgs_id
) as k
where  country2019_name is null and ports.uhgs_id = k.uhgs_id 
-- Query returned successfully: 10 rows affected, 2.0 secs execution time.


select * from navigoviz.port_points where country2019_iso2code is null
-- 25 lignes

/*
"Lampeduse"
"Côte d' Afrique [de l' Ouest]"
"Caps de Fer en Barbarie (35 lieux au Nord)"
"Cap Crozotte [40 lieues au sud]"
"Nord du Banc de la Casse"
"Middelhavet"
"Østersøen"
"Nord"
"Groenland"
"Malte (30 leagues off)"
"Irlande - 40 lieues au large"
"Brest [150 leagues westward]"
"Mediterranée"
"Majorque (off"
"Levant"
"Cap Sicié (off"
"Premier port français"
"Europe"
"[Beyond Finisterre cape]"
"Nord"
"Nordsøen"
"Banc de Terre Neuve"
"Vestindien"
"Mozambique"
"Indes orientales"
*/

-- League = lieue https://fr.wikipedia.org/wiki/Lieue
-- 4,678 km
pg_dump --host localhost --port 5432 --username "navigo" --format plain --encoding UTF8 --verbose --file "./portic_navigoviz_port_16052019.sql" --table "navigoviz.port_points" "portic"


CREATE TABLE navigoviz.travel(
    documentary_unit_id text,
    rank int,
    from_port text,
    to_port text,
    ship_id text,
    captain_id text,
    in_date text,
    out_date text,
    from_port_id text,
    to_port_id text

);

alter table navigoviz.travel add segment geometry;

update navigoviz.travel t set segment = ST_MakeLine(k.fromg, q.tog)
from 
( select  from_port_id, point3857 as fromg from  navigoviz.port_points ,  navigoviz.travel
where from_port_id = uhgs_id and ship_id = '0000496N') as k,
( select  to_port_id, point3857 as tog from  navigoviz.port_points ,  navigoviz.travel
where to_port_id = uhgs_id and ship_id = '0000496N') as q
where t.ship_id = '0000496N' and t.from_port_id = k.from_port_id and t.to_port_id = q.to_port_id

/*
ERREUR:  n'a pas pu écrire le fichier temporaire de la jointure hâchée : Aucun espace disponible sur le périphérique
********** Erreur **********

ERREUR: n'a pas pu écrire le fichier temporaire de la jointure hâchée : Aucun espace disponible sur le périphérique
État SQL :53100
*/

select *, st_astext(segment) from navigoviz.travel t where t.ship_id = '0000496N'
"LINESTRING(179966.547222289 6571327.60099272,-222638.981586547 6982997.92038979)"

alter table navigoviz.travel add column id serial;

update navigoviz.travel t set segment = ST_MakeLine(k.fromg, q.tog)
from 
( select  id, from_port_id, point3857 as fromg from  navigoviz.port_points ,  navigoviz.travel
where from_port_id = uhgs_id ) as k,
( select id,  to_port_id, point3857 as tog from  navigoviz.port_points ,  navigoviz.travel
where to_port_id = uhgs_id ) as q
where t.id = k.id and t.id = q.id and t.from_port_id = k.from_port_id and t.to_port_id = q.to_port_id
-- Query returned successfully: 102515 rows affected, 34.7 secs execution time.




set search_path = navigoviz, public

select count(*) from travel
-- 110290

select count(distinct t.id) from navigoviz.travel t, navigocheck.check_pointcall p
where t.documentary_unit_id = p.documentary_unit_id and p.component_description__suite_id in ('00000003', '00000074')
-- 47563

delete from navigoviz.travel t where id in 
(
select distinct t.id from navigoviz.travel t, navigocheck.check_pointcall p
where t.documentary_unit_id = p.documentary_unit_id and p.component_description__suite_id not in ('00000003', '00000074')
)
-- 55858

select count(*) from travel
-- 54432 



SELECT jsonb_build_object('type','FeatureCollection','features', jsonb_agg(feature))
FROM (SELECT jsonb_build_object(
                                   'type',       'Feature',
                                   'id',         id,
                                   'geometry',   ST_AsGeoJSON(segment)::jsonb,
                                   'properties', to_jsonb(row) - 'id' - 'segment'
                                 ) AS feature
                                 FROM (
                                 SELECT id, segment, ship_id, captain_id
                                 FROM navigoviz.travel
                                 limit 100
                                 ) row) features;

                                 
---------------------------------------------------------------------------------------------------------
-- Utiliser la cote pour construire des segments mieux
---------------------------------------------------------------------------------------------------------

-- mise à jour de coteline pour simplifier les opérations de géométrie avec une multiligne plutot qu'un polygone.
alter table trait_cote add column coteline geometry;
update trait_cote set coteline = ST_LineMerge(st_boundary(wkb_geometry)) 
where id = '0-E';

CREATE INDEX coteline_geom_idx
  ON public.trait_cote
  USING gist
  (coteline);

CREATE INDEX segment_geom_idx
  ON public.long_trajet
  USING gist
  (segment);
  
alter table long_trajet add column intersection_cote geometry ;

create table intersections_trajet_cote as (
select t.id, st_intersection(segment, coteline) as multiligne, st_startpoint(st_intersection(segment, coteline)) as startP, st_endpoint(st_intersection(segment, coteline)) as endP, st_NPoints(st_intersection(segment, coteline)) as nbpoints
from long_trajet t, trait_cote c where c.id = '0-E' and coteline::box2d && segment::box2d )

select id, st_astext(multiligne), nbpoints, st_astext(ST_StartPoint(ST_LineFromMultiPoint(multiligne))) as start, st_astext(ST_PointN(ST_LineFromMultiPoint(multiligne), 2)) as end  from intersections_trajet_cote
where nbpoints > 1

alter table intersections_trajet_cote add column intersectionPoint geometry
update intersections_trajet_cote set intersectionPoint= (st_dump(multiligne)).geom where nbpoints = 1

-- utiliser le dump sur une multiligne
select st_astext((st_dump(intersection_cote)).geom), (st_dump(intersection_cote)).path[1] from long_trajet where id = 75

update long_trajet t set intersection_cote = point_ligne 
from (
select id, nbpoints, point_ligne, ordre from (
select id, nbpoints, st_astext((st_dump(multiligne)).geom) as point_ligne, (st_dump(multiligne)).path[1] as ordre from intersections_trajet_cote where nbpoints > 1 order by id, ordre desc ) as k
where k.ordre = 1) as i
where i.nbpoints > 1 and t.id = i.id


alter table long_trajet add column f1 float;
alter table long_trajet add column f2 float;

update long_trajet t set f1 = ST_Line_Locate_Point(coteline, intersection_cote)
-- select t.id, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point) 
-- st_length(ST_Line_Substring(coteline, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point))) / 1000.0
from trait_cote c,  amers a2  
where c.id = '0-E' and t.amer_arrivee=a2.identifiant 



update long_trajet t set f2 = ST_Line_Locate_Point(coteline, a2.point)
-- select t.id, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point) 
-- st_length(ST_Line_Substring(coteline, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point))) / 1000.0
from trait_cote c,  amers a2  
where c.id = '0-E' and t.amer_arrivee=a2.identifiant 

alter table long_trajet add column cabotinage geometry;
update long_trajet set cabotinage = ST_Line_Substring(coteline, CASE WHEN f1<f2 THEN f1 ELSE f2 END,  CASE WHEN f1<f2 THEN f2 ELSE f1 END)
from trait_cote c where c.id = '0-E'

--- IMPORTANT : generalisation de la cote
update long_trajet t set cabotinage = ST_SimplifyPreserveTopology(ST_Line_Substring(coteline, CASE WHEN f1<f2 THEN f1 ELSE f2 END,  CASE WHEN f1<f2 THEN f2 ELSE f1 END), 0.01)
from trait_cote c where c.id = '0-E'  ;
-- c'est bien (plus on baisse plus c'est précis) mais inutile
update long_trajet t set cabotinage = ST_ConcaveHull(ST_Line_Substring(coteline, CASE WHEN f1<f2 THEN f1 ELSE f2 END,  CASE WHEN f1<f2 THEN f2 ELSE f1 END), 0.80)
from trait_cote c where c.id = '0-E'  ;
-- OK

/*
alter table long_trajet add column f11 float;
alter table long_trajet add column f22 float;
update long_trajet t set f11 = ST_Line_Locate_Point(ST_LineMerge(st_boundary(cabotinage)), intersection_cote) where intersection_cote is not null and st_geometrytype(cabotinage) = 'ST_Polygon';
update long_trajet t set f11 = ST_Line_Locate_Point(cabotinage, intersection_cote) where intersection_cote is not null and st_geometrytype(cabotinage) = 'ST_LineString';

update long_trajet t set f22 = ST_Line_Locate_Point(ST_LineMerge(st_boundary(cabotinage)), a2.point) from  amers a2  where t.amer_arrivee=a2.identifiant and st_geometrytype(cabotinage) = 'ST_Polygon';
update long_trajet t set f22 = ST_Line_Locate_Point(cabotinage, a2.point) from  amers a2  where t.amer_arrivee=a2.identifiant and st_geometrytype(cabotinage) = 'ST_LineString';

alter table long_trajet add column cabotinage_1 geometry;
alter table long_trajet add column cabotinage_2 geometry;

update long_trajet t set cabotinage_1 =  ST_Line_Substring(ST_LineMerge(st_boundary(cabotinage)), CASE WHEN f11<f22 THEN f11 ELSE f22 END,  CASE WHEN f11<f22 THEN f22 ELSE f11 END) where st_geometrytype(cabotinage) = 'ST_Polygon';
update long_trajet t set cabotinage_1 =  ST_Line_Substring(cabotinage, CASE WHEN f11<f22 THEN f11 ELSE f22 END,  CASE WHEN f11<f22 THEN f22 ELSE f11 END) where st_geometrytype(cabotinage) = 'ST_LineString';

update long_trajet t set cabotinage_2 =  ST_difference(ST_LineMerge(st_boundary(cabotinage)), cabotinage_1) where st_geometrytype(cabotinage) = 'ST_Polygon'
update long_trajet t set cabotinage_2 =  ST_difference(cabotinage, cabotinage_1) where st_geometrytype(cabotinage) = 'ST_LineString'

update long_trajet   set cabotinage_1 = st_setsrid(cabotinage_1, 4326);
update long_trajet   set cabotinage_2 = st_setsrid(cabotinage_2, 4326);

ST_ConcaveHull(ST_Collect(d.pnt_geom), 0.99)
select id, st_length(cabotinage_2, true)/ 1000.0 from long_trajet t where t.id = 141; -- 210 km
select id, st_length(cabotinage_1, true)/ 1000.0 from long_trajet t where t.id = 141; -- 188 km
select st_length(ST_ShortestLine(intersection_cote, a2.point), true)/ 1000.0 from long_trajet t, amers a2  where t.id = 141 and t.amer_arrivee=a2.identifiant;
-- 170 km

-- Problème : quel cabotinage choisir ? Celui qui est le plus dans la mer
select t.id, st_length(cabotinage_1, true)/ 1000.0, st_length(cabotinage_2, true)/ 1000.0,
st_length(st_intersection(cabotinage_1, c.wkb_geometry), true)/ 1000.0, st_length(st_intersection(cabotinage_2, c.wkb_geometry), true)/ 1000.0
 from long_trajet t, trait_cote c where c.id = '0-E'  

alter table long_trajet add column cabotinage_ok geometry;
update long_trajet set cabotinage_ok = CASE WHEN st_length(st_intersection(cabotinage_2, c.wkb_geometry), true)< st_length(st_intersection(cabotinage_1, c.wkb_geometry), true) THEN st_setsrid(cabotinage_2, 4326) ELSE st_setsrid(cabotinage_1, 4326) END
from  trait_cote c where c.id = '0-E'  ;
-- OK, bonne technique
*/


alter table long_trajet add column distance_cabotinage_km float;
update long_trajet set distance_cabotinage_km = st_distance(a2.point, intersection_cote, true) / 1000.0
from amers a2  
where amer_arrivee=a2.identifiant 

select id, distance_km,  vraie_distance_km, COALESCE(distance_cabotinage_km, 0) as distance_cabotinage_km, COALESCE(st_distance(a2.point, intersection_cote, true) / 1000.0, st_length(segment, true)/ 1000.0) as trajet_haute_mer, coalesce(st_length(cabotinage, true)/ 1000.0, 0) as vrai_cabotinage_km
from long_trajet t, amers a2  
where t.amer_depart=a2.identifiant 
order by id

-------------------------------------------------------------------------------------------------------
--- Contrôle XX :  il y a plus qu'une shipping area par amirauté
-------------------------------------------------------------------------------------------------------
select amiraute, array_agg(distinct shiparea) from navigoviz.port_points
group by amiraute
having cardinality(array_agg(distinct shiparea)) > 1
order by (array_agg(distinct shiparea))[1]


SELECT uhgs_id, toponyme, topofreq, total, ST_AsGeoJSON(ST_Transform(wkb_geometry,900913)), amiraute::text, province::text  
FROM navigoviz.port_points p, 
(select pointcall_uhgs_id, count(*) as total from navigo.geo_general group by pointcall_uhgs_id, geo_general__long, geo_general__lat) as k
where p.uhgs_id = k.pointcall_uhgs_id and (province = 'Bretagne' or amiraute='Vannes' or toponyme like 'Groix')
-- and p.uhgs_id='A0858758'

"Sables-d’Olonne" "Poitou" "Talmont [Talmont Saint Hilaire]"

"A0858758";"Finmarken";"[{"topo": "Finmarken", "freq": 79}, {"topo": "Finmarken", "freq": 79}]"

drop table navigoviz.admiralty_limits
select * from navigoviz.admiralty_limits order by id
update navigoviz.admiralty_limits set id = 19 where ogc_fid = 63

--------------------------------------------------------------------------------------------
-- Taxes
--------------------------------------------------------------------------------------------

select count(*) from navigo.taxes where tax_concept is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')

select point_id_integration from navigo.taxes where point_id_integration not in (select pointcall_UHGS_id from navigo.pointcall)

select documentary_unit_id from navigo.taxes where documentary_unit_id not in (select documentary_unit_id from navigo.pointcall)
select cargo_item_id from navigo.taxes where cargo_item_id not in (select cargo_item_id from navigo.cargo)

select q01, q02, q03 from navigo.taxes where q01 is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')

----------------------
-- Controle taxe 01 : type de q01, q02, q03
---------------------

select q01, q02, q03 from navigo.taxes 
where q02 is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
and (test_double_type(q02) is false)

----------------------
-- Controle taxe 01 : type de q01, q02, q03
---------------------

select distinct q01_u, count(*) as d from navigo.taxes 
where q01_u is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by q01_u order by d desc


select distinct q02_u, count(*) as d from navigo.taxes 
where q02_u is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by q02_u order by d desc


select distinct q03_u, count(*) as d from navigo.taxes 
where  q03_u is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by  q03_u order by d desc

----------------------
-- Controle taxe 02 : type énuméré de tax_concept
---------------------
select tax_concept, count(*) as d from navigo.taxes 
where  tax_concept is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by tax_concept order by d desc

----------------------
-- Controle taxe 03 : point_id_integration est relié à un pointcall.record_id
---------------------
select point_id_integration from navigo.taxes where point_id_integration not in (select record_id from navigo.pointcall)
-- null

----------------------
-- Controle taxe 04 : cohérence de la date payment_date avec celle de sortie de pointcall.record_id
---------------------
select payment_date, point_id_integration , p.pointcall_indate, p.pointcall_outdate
from navigo.taxes t , navigo.pointcall p
where  payment_date is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and p.record_id =point_id_integration and ( payment_date < p.pointcall_indate or payment_date > p.pointcall_outdate)
and pointcall_indate is null
-- 144 lignes


select payment_date, point_id_integration , p.pointcall_indate, p.pointcall_outdate, p.pointcall_date
from navigo.taxes t , navigo.pointcall p
where  payment_date is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and p.record_id =point_id_integration and (payment_date > p.pointcall_outdate)
and pointcall_indate is null
-- 144 lignes


select t.record_id, payment_date, point_id_integration , p.pointcall_indate, p.pointcall_outdate, p.pointcall_date, t.*, p.*
from navigo.taxes t , navigo.pointcall p
where  payment_date is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and p.record_id =point_id_integration and (payment_date < p.pointcall_indate)
and pointcall_outdate is null
-- 1

----------------------
-- Controle taxe 05 : cohérence de la date pointcall.documentary_unit_id avec celle de sortie de pointcall.pointcall.documentary_unit_id
---------------------
select t.record_id, p.documentary_unit_id, t.documentary_unit_id
from navigo.taxes t , navigo.pointcall p
where  t.documentary_unit_id is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and p.record_id = point_id_integration and (p.documentary_unit_id <> t.documentary_unit_id)
-- 19 lignes


----------------------
-- Controle taxe 06 : cohérence avec la table cargo
---------------------
select t.record_id, c.documentary_unit_id, t.documentary_unit_id, t.cargo_item_id, c.cargo_item_id, c.commodity_id
from navigo.taxes t , navigo.cargo c
where  t.cargo_item_id is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and (t.documentary_unit_id = c.documentary_unit_id) and t.cargo_item_id = c.record_id
-- 3 lignes

select t.record_id as taxe_record, c.documentary_unit_id, t.documentary_unit_id, t.cargo_item_id, c.record_id as cargo_record, c.cargo_item_id, c.commodity_id, t.*, c.*
from navigo.taxes t , navigo.cargo c
where  t.cargo_item_id is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and  t.cargo_item_id = c.record_id

select * from taxes where record_id not in (
select t.record_id as taxe_record
from navigo.taxes t , navigo.cargo c
where  t.cargo_item_id is not null 
and (t.source like '%G5%' or t.source like 'ADBdR%' or t.source like 'ADVar%' or t.source like '%G-5%')
and  t.cargo_item_id = c.record_id)
and cargo_item_id is not null 
and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
-- 4 lignes

----------------------
-- Controle taxe 07 : type de cargo_item_id
---------------------

select  cargo_item_id from navigo.taxes 
where cargo_item_id is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
and (test_int_type(cargo_item_id) is false)

select  documentary_unit_id from navigo.taxes 
where documentary_unit_id is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
and (test_int_type(documentary_unit_id) is false)

--------------------------------------------------------------------------------------------
--- Cargo
--------------------------------------------------------------------------------------------

----------------------
-- Controle Cargo 01 : point_id_integration
---------------------

select * from cargo where record_id not in (
select c.record_id 
from navigo.cargo c, navigo.pointcall p 
where 
point_id_integration is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
and c.point_id_integration = p.record_id
) and point_id_integration is not null 
and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
--- 143 lignes en erreur

----------------------
-- Controle Cargo 02 : pointcall_id
---------------------

select * from cargo where record_id not in (
select c.record_id, * 
from navigo.cargo c, navigo.pointcall p 
where 
pointcall_id is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
and c.pointcall_id = p.record_id
) and pointcall_id is not null 
and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
--- 111 lignes en erreur

----------------------
-- Controle Cargo 03 : quantity_u
---------------------

select cargo_item_quantity_u, count(*) as d 
from navigo.cargo c
where cargo_item_quantity_u is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
group by cargo_item_quantity_u order by d desc

select cargo_item_quantity_equivalent_u, count(*) as d 
from navigo.cargo c
where cargo_item_quantity_equivalent_u is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
group by cargo_item_quantity_equivalent_u order by d desc

select cargo_item_quantity_metrical_u, count(*) as d 
from navigo.cargo c
where cargo_item_quantity_metrical_u is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
group by cargo_item_quantity_metrical_u order by d desc

----------------------
-- Controle Cargo 03 : quantity (type float)
---------------------

select cargo_item_quantity 
from navigo.cargo c
where cargo_item_quantity is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
and test_double_type(cargo_item_quantity) is false

select cargo_item_quantity_equivalent 
from navigo.cargo c
where cargo_item_quantity_equivalent is not null and (c.source like '%G5%' or c.source like 'ADBdR%' or c.source like 'ADVar%' or c.source like '%G-5%')
and test_double_type(cargo_item_quantity_equivalent) is false

----------------------
-- Controle Cargo 03 :  type de commodity_standardized
----------------------

select commodity_standardized, count(*) as d from navigocheck.check_cargo 
where  commodity_standardized is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
group by  commodity_standardized order by upper(commodity_standardized), d desc
-- énumeration


----------------------
-- Controle Cargo 03 :  type de commodity_permanent_coding
----------------------

select commodity_permanent_coding, count(*) as d , array_agg(distinct commodity_standardized) as tab_commodity_standardized
from navigocheck.check_cargo 
where  commodity_permanent_coding is not null and (source like '%G5%' or source like 'ADBdR%' or source like 'ADVar%' or source like '%G-5%')
-- and commodity_standardized like 'Apple%'
group by  commodity_permanent_coding 
having array_length(array_agg(distinct commodity_standardized) , 1) > 1
order by upper(commodity_permanent_coding), d desc




