-- Script d'import de la base geonames
-- doc : http://www.geonames.org/ontology/documentation.html
-- version : 2019 November, Version 3.2 Elisa Kendall
-- Fichiers plats : http://download.geonames.org/export/dump/

/*
geonameid         : integer id of record in geonames database
name              : name of geographical point (utf8) varchar(200)
asciiname         : name of geographical point in plain ascii characters, varchar(200)
alternatenames    : alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table, varchar(10000)
latitude          : latitude in decimal degrees (wgs84)
longitude         : longitude in decimal degrees (wgs84)
feature class     : see http://www.geonames.org/export/codes.html, char(1)
feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
country code      : ISO-3166 2-letter country code, 2 characters
cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 200 characters
admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
admin3 code       : code for third level administrative division, varchar(20)
admin4 code       : code for fourth level administrative division, varchar(20)
population        : bigint (8 byte int) 
elevation         : in meters, integer
dem               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
timezone          : the iana timezone id (see file timeZone.txt) varchar(40)
modification date : date of last modification in yyyy-MM-dd format

*/
set search_path = geonames_nov2019, public


create extension fuzzystrmatch ;
create extension pg_trgm;
create extension postgis;

drop table allcountries

create table geonames_nov2019.allcountries (
	geonameid integer primary KEY,
	name      text,   
	asciiname text,
	alternatenames text,
	latitude float,
	longitude float,
	feature_class char(1),
	feature_code varchar(10),
	country_code char(2),
	cc2 text,
	admin1_code varchar(20),
	admin2_code varchar(80),
	admin3_code varchar(20),
	admin4_code varchar(20),
	population integer ,
	elevation integer,
	dem integer,
	timezone varchar(40),
	modification_date date
	);

	COMMENT ON COLUMN allcountries.geonameid IS 'id of record in geonames database';
 	COMMENT ON COLUMN allcountries.name IS  'name of geographical point (utf8) varchar(200)';
 	COMMENT ON COLUMN allcountries.asciiname IS  'name of geographical point in plain ascii characters, varchar(200)';
 	COMMENT ON COLUMN allcountries.alternatenames IS  'alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table';
 	COMMENT ON COLUMN allcountries.latitude IS  'latitude in decimal degrees (wgs84)';
 	COMMENT ON COLUMN allcountries.longitude IS  'longitude in decimal degrees (wgs84)';
 	COMMENT ON COLUMN allcountries.feature_class IS  'see http://www.geonames.org/export/codes.html';
 	COMMENT ON COLUMN allcountries.feature_code IS  'see http://www.geonames.org/export/codes.html';
 	COMMENT ON COLUMN allcountries.country_code IS  'ISO-3166 2-letter country code, 2 characters';
 	COMMENT ON COLUMN allcountries.cc2 IS  'alternate country codes, comma separated, ISO-3166 2-letter country code';
 	COMMENT ON COLUMN allcountries.admin1_code IS  'fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code;';
 	COMMENT ON COLUMN allcountries.admin2_code IS  'code for the second administrative division, a county in the US, see file admin2Codes.txt;';
 	COMMENT ON COLUMN allcountries.admin3_code IS  'code for third level administrative division';
 	COMMENT ON COLUMN allcountries.admin4_code IS  'code for fourth level administrative division';
 	COMMENT ON COLUMN allcountries.population IS  'bigint (8 byte int) ';
 	COMMENT ON COLUMN allcountries.elevation IS  'in meters';
 	COMMENT ON COLUMN allcountries.dem IS  'digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.';
 	COMMENT ON COLUMN allcountries.timezone IS  'the iana timezone id (see file timeZone.txt)';
 	COMMENT ON COLUMN allcountries.modification_date IS  'date of last modification in yyyy-MM-dd format';
 
 comment on table allcountries is 'Table ? importer avec le fichier allCountries_cleaned.txt (les simples quotes  ont ?t? remplac?es par des doubles quotes). \n Les m?tadonn?es de la table et le fichier ? t?l?charger sont accessibles ici : http://download.geonames.org/export/dump/'
 
 select count(*) from allcountries
-- 11 986 431
 select count(*) from geonames_nov2019.allcountries
-- 9 528 740
-- 11 986 437 

-- truncate table allcountries


select * from allcountries 
where  geonameid = 8436127

select distinct country_code, count(*) from geonames_nov2019.allcountries
group by country_code
/*
select * from allcountries 
where  geonameid = 8436127
*/

select distinct country_code, count(*) from allcountries
group by country_code

truncate geonames_nov2019.allcountries

select count(*) from geonames_nov2019.allcountries

create table geonames_nov2019.allcountries_test (
	geonameid integer primary KEY,
	name      text,   
	asciiname text,
	alternatenames text,
	latitude float,
	longitude float,
	feature_class char(1),
	feature_code varchar(10),
	country_code char(2),
	cc2 text,
	admin1_code varchar(20),
	admin2_code varchar(80),
	admin3_code varchar(20),
	admin4_code varchar(20),
	population integer ,
	elevation integer,
	dem integer,
	timezone varchar(40),
	modification_date date
	)

\copy geonames_nov2019.allcountries from '/home/plumegeo/navigo/ETL/data_14juin2020/allCountries_cleaned.txt'

WITH TEXT HEADER DELIMITER ';'

CREATE TABLE geonames_nov2019.allcountries (
    geonameid integer NOT NULL,
    name text,
    asciiname text,
    alternatenames text,
    latitude double precision,
    longitude double precision,
    feature_class character(1),
    feature_code character varying(10),
    country_code character(2),
    cc2 text,
    admin1_code character varying(20),
    admin2_code character varying(80),
    admin3_code character varying(20),
    admin4_code character varying(20),
    population integer,
    elevation integer,
    dem integer,
    timezone character varying(40),
    modification_date date,
    point3857 geometry
);

INSERT INTO geonames_nov2019.allcountries VALUES (7906467, 'Iongo', 'Iongo', NULL, -14.6875800000000005, 12.5556300000000007, 'T', 'HLL', 'AO', NULL, '13', NULL, NULL, NULL, 0, NULL, 682, 'Africa/Luanda', '2011-07-11', '0101000020110F0000B5899356B6533541F67406F4E43939C1');

select * from allcountries_test
group by country_code
	
delete from geonames_nov2019.allcountries_test where geonameid = 3038872
INSERT INTO geonames_nov2019.allcountries_test (geonameid,"name",asciiname,alternatenames, latitude,longitude, feature_class,feature_code,country_code,admin4_code,population, dem, timezone, modification_date)
	VALUES (3038872,'Riu Valira d''Orient','Riu Valira d''Orient', 'Rio Balira del Orien,Riu Valira d''Orient,Riu Valira d?Orient,R?o Balira del Orien', 42.51146,1.53647,'H', 'STM','AD','08',0,1033,'Europe/Andorra','2018-09-05') 
	-- alternatenames 'Rio Balira del Orien,Riu Valira d''Orient,Riu Valira d''Orient,R?o Balira del Orien'
	
	3038872		42.51146	1.53647	H	STM	AD		08				0		1033	Europe/Andorra	2018-09-05
-- Riu Valira d?Orient	Riu Valira d'Orient	Rio Balira del Orien,Riu Valira d'Orient,Riu Valira d?Orient,R?o Balira del Orien

select * from allcountries where geonameid = 3038872 -- Riu Valira d?Orient
select * from allcountries where geonameid = 3023203 -- Port Vieux de la Coume d?Ose
select * from allcountries where geonameid = 3038876 -- Riu d?Urina
-- Sont bien dans allcountries.txt

-- ABSENTS de allcountries.txt mais PRESENTS dans allcountries_pb.txt (fichier qui m'a permis de d?bugger plus vite)

-- Twelve Trees
select * from allcountries where geonameid = 8790520
insert into allcountries (
select * from allcountries_test where geonameid = 8790520)

-- Barney"S Lake
select * from allcountries where geonameid = 8799554
insert into allcountries (
select * from allcountries_test where geonameid = 8799554
)

-- Port Kembla
select * from allcountries where geonameid = 11886736
insert into allcountries (
select * from allcountries_test where geonameid = 11886736
)

-- Raz?yezd Khudaferin
select * from allcountries where geonameid = 388811 
insert into allcountries (
select * from allcountries_test where geonameid = 388811
)

-- Raz?yezd Kishlak
select * from allcountries where geonameid = 388770 
insert into allcountries (
select * from allcountries_test where geonameid = 388770
)

-- "Baki-Pilsen" Brewery
select * from allcountries where geonameid = 8436127 
insert into allcountries (
select * from allcountries_test where geonameid = 8436127
)


select count(*) from allcountries_test at2 
-- 9
drop table geonames_nov2019.allcountries_test

----------------------------------------------------------------------------------
select * from geonames.geonames_nov2019.allcountries a where geonameid = 3017382 


select geonameid, name, feature_code, country_code, admin1_code, admin2_code, latitude, longitude from geonames.geonames_nov2019.allcountries a 
where name like '%France%' and feature_class ='A' and feature_code like 'P%'

select geonameid, name, feature_code, country_code, admin1_code, admin2_code, latitude, longitude from geonames.geonames_nov2019.allcountries a 
where name like '%United Kingdom%' and feature_class ='A' and feature_code like 'P%'

select geonameid::int, name, feature_class, feature_code, country_code, admin1_code, admin2_code, admin3_code, latitude::float, longitude::float from geonames.geonames_nov2019.allcountries a 
where feature_class = 'A' and feature_code like 'P%' or feature_code = 'TERR' or feature_code like 'Z' or feature_code like 'ADM1%' or feature_code like 'ADM2%'  or feature_code like 'ADM3%' 
order by name

select * from geonames.geonames_nov2019.allcountries a  where geonameid=2648147
select * from geonames.geonames_nov2019.allcountries a  where geonameid=2635167

select * from geonames.geonames_nov2019.allcountries a  where geonameid=3176217

select geonameid::int, name, feature_class, feature_code, country_code, admin1_code, admin2_code, admin3_code, latitude::float, longitude::float 
from geonames.geonames_nov2019.allcountries a 
where feature_class = 'A' and (feature_code like 'P%' or feature_code = 'TERR' or feature_code like 'Z' or feature_code like 'ADM1%' or feature_code like 'ADM2%'  or feature_code like 'ADM3%') 
and admin3_code is null
order by name

Great Britain

create table geonames_nov2019.feature_code (
	Code	text,
	description_short	text, 
	description_full	text, 
	Keep text)

psql -U postgres -d geonames
\copy geonames_nov2019.feature_code from 'C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\Geonames\feature_codes.csv' WITH CSV HEADER DELIMITER ';'
-- 681


set search_path = geonames_nov2019, public;

select * from feature_code where keep = 'o';
select * from feature_code where keep = 'x';

select distinct feature_class||'.'||feature_code, count(*) 
from geonames.geonames_nov2019.allcountries a 
group by feature_class||'.'||feature_code
order by feature_class||'.'||feature_code

select geonameid::int, name, feature_class||'.'||feature_code as feature_code, alternatenames, country_code,  latitude::float, longitude::float 
from geonames.geonames_nov2019.allcountries a 
where feature_class||'.'||feature_code in (select code from feature_code where keep = 'x');

create extension fuzzystrmatch ;
create extension pg_trgm;
create extension postgis;


CREATE INDEX trgm_geoname_name_idx ON geonames.geonames_nov2019.allcountries USING GIST (name gist_trgm_ops);

select * from geonames_nov2019.allcountries where name % 'Brest'


-------------------------
-- ajouter un point (geometry)
-------------------------

alter table geonames.geonames_nov2019.allcountries add column point3857 geometry ;
update geonames.geonames_nov2019.allcountries set point3857= st_setsrid(st_transform(st_setsrid(st_makepoint(longitude, latitude), 4326), 3857), 3857) where abs(latitude) < 90;
-- 11 986 427 lignes


CREATE INDEX gist_geoname_point_idx ON geonames.geonames_nov2019.allcountries USING GIST (point3857);


select * from geonames_nov2019.feature_code fc where code like '%P.PPLH%'

select count(*) from geonames_nov2019.allcountries a where feature_class = 'P' and feature_code='PPLH'
-- 986

select * from geonames_nov2019.allcountries a where feature_class = 'P' and feature_code='PPLH' and country_code = 'FR' limit 10


select * from geonames_nov2019.feature_code fc where description_full like '%harbor%'
S.LDNG landing
S.HSTS historical site
H.POOL	pool(s)			a small and comparatively still, deep part of a larger body of water such as a stream or harbor; or a small body of standing water
H.RDST	roadstead		an open anchorage affording less protection than a harbor
L.PRT	port            a place provided with terminal and transfer facilities for loading and discharging waterborne cargo or passengers, usually located in a harbor
S.MAR	marina			a harbor facility for small boats, yachts, etc.

select count(*) from geonames_nov2019.allcountries a where feature_class = 'S' and feature_code='MAR'
-- 1636
select count(*) from geonames_nov2019.allcountries a where feature_class = 'L' and feature_code='PRT'
-- 2402
select count(*) from geonames_nov2019.allcountries a where feature_class = 'H' and feature_code='POOL'
-- 3222
select count(*) from geonames_nov2019.allcountries a where feature_class = 'H' and feature_code='RDST'
-- 426
select count(*) from geonames_nov2019.allcountries a where feature_class = 'S' and feature_code='LDNG'
-- 3164

select count(*) from geonames_nov2019.allcountries a where feature_class = 'S' and feature_code='LDNG'
and "name" not in (select name from geonames_nov2019.allcountries a where feature_class = 'L' and feature_code='PRT')

select  1636+2402+3222+426+3164
-- 10850 entités

select name from geonames_nov2019.allcountries a where feature_class = 'H' and feature_code='POOL' and country_code = 'FR' 
select name, admin2_code, admin3_code from geonames_nov2019.allcountries a where feature_class = 'S' and feature_code='MAR' and country_code = 'FR' -- 7 marinas
select name, admin2_code, admin3_code from geonames_nov2019.allcountries a where feature_class = 'H' and feature_code='POOL' and country_code = 'FR' -- Fosse d'Ouessant
select name, admin2_code, admin3_code from geonames_nov2019.allcountries a where feature_class = 'L' and feature_code='PRT' and country_code = 'FR' -- 112 ports
select name, admin2_code, admin3_code from geonames_nov2019.allcountries a where feature_class = 'H' and feature_code='RDST' and country_code = 'FR' -- 41 rades
select name, admin2_code, admin3_code from geonames_nov2019.allcountries a where feature_class = 'S' and feature_code='LDNG' and country_code = 'FR' -- 0

select 7 + 1 + 112 + 41 
-- 161


