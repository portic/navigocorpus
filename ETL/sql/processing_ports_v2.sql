--------------------------------------------------------------------------------------------------
-- Traitement des ports pour portic_v6
-- Christine PLUMEJEAUD-PERREAU, U.M.R. LIENSS 7266 
-- ANR PORTIC 
-- Le 01 février 2021
-- Transformation de geo_general en port_points avec calcul de tous les attributs intéressants
-- oblique, statut, geonameid, belonging_states, amiraute, province
-- Note : une extension de geo_general obligera à réviser  amiraute et province (des points devront récupérer leur appartenance, possible uniquement via QGIS)
-- corrections (NOMBREUSES) des états d'appartenance, et subunit, en fr et en
-- ajout de toponymes_standards
-- calculs d'alignement avec la ferme (direction et bureau)
-- calculs d'alignement avec la balance du commerce (partner_balance) entre états toflits et état navigo.
-- Calcul d'une distance à la cote (distance_water) qui permettrait de distinguer les génériques des autres ports
-- Nouveaux attributs : 
/*has_a_clerk
source_1787_available
source_1789_available
belonging_states_en
belonging_substates_en
toponyme_fr
toponyme_en
ferme_direction
ferme_bureau
ferme_bureau_uncertainty
partner_balance_1789
partner_balance_supp_1789
partner_balance_1789_uncertainty
partner_balance_supp_1789_uncertainty
state_1789_fr
substate_1789_fr
state_1789_en
substate_1789_en*/
--------------------------------------------------------------------------------------------------

-- reprendre processing_ports

select distinct source from navigo.pointcall p 
select distinct suite, component, main_port_uhgs_id, main_port_toponyme, subset from navigoviz."source" s  
select distinct subset from navigoviz."source" s  

Registre du petit cabotage (1786-1787)
Expéditions "coloniales" Marseille (1789)
Santé Marseille


CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -3
                        result = None
                else :
                    result = None
                    code = -3
                return [result, code]
            $$ LANGUAGE plpython3u;
           
select navigo.rm_parentheses_crochets('[(du bon jambon)]')
select navigo.rm_parentheses_crochets('([du bon jambon])')
select navigo.rm_parentheses_crochets('calveze [du bon jambon]')

select * from navigoviz.pointcall p where p.ship_flag_id is null and ship_uncertainity != -3



CREATE TABLE ports.geo_general as (
select pointcall_name, pointcall_uhgs_id , geo_general__lat as latitude, geo_general__long as longitude, shippingarea__e1_name as shippingarea 
from navigo.geo_general gg 
); -- 100438 Rows

select toponyme, uhgs_id, oblique , amiraute, province, status, state_1787, substate_1787 
from ports.port_points pp 
where state_1787 like 'France' and status is not null
-- importer les anciennes données
psql -U postgres -d portic_v6 -f "C:\Travail\Data\Navigo_25jan2021\portic_v5.ports.GIS-20201202.sql"

drop table ports.codes_levels ;
drop table ports.etats ;
drop table ports.matching_port ;
drop table ports.obliques ;
drop table ports.port_points cascade;
drop table ports.port_points_old;
drop table ports.world_borders ;

alter table ports.port_points rename  to port_points_old;


set role dba
reset role 

-- adaptation au schéma ports avec le nouveau geo_general BONNE VERSION
CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)
 RETURNS json
 LANGUAGE plpython3u
AS $function$
	import json
	from operator import itemgetter
	global temp
	global result
	global topos
	global query
	if toustopo is not None :
		result = []
		temp = toustopo.replace('{', '').replace('}', '').strip(' ')
		temp = temp.replace('"', '').strip(' ')
		topos = temp.split(',')
		for item in topos:
		    #plpy.notice(item)
		    
		    query = "select count(*) as freq from ports.geo_general where pointcall_uhgs_id = '"+uhgs_id+"' and (ports.rm_parentheses_crochets(pointcall_name)).value='"+item.replace('\'', '\'\'')+"'"
		    #plpy.notice(query)
		    rv = plpy.execute(query)
		    #plpy.notice(rv[0]["freq"])
		    result.append(dict(topo=item, freq=rv[0]["freq"]))
		result = sorted(result, key=itemgetter('freq'), reverse=True)

	else :
		result = None
	#plpy.notice (result)
	return json.dumps(result, ensure_ascii=False)
$function$
;
-- fin de ports.frequency_topo" 
-- BONNE VERSION		  
reset role 

select ports.frequency_topo('A0199508', '{La Flotte,la Flotte-en-Ré,La Flotte [en Ré],La Flotte [La Flotte en Ré],la Flotte en Ré,La Flotte en Ré,La Flotte île de Ré}')
-- [{"topo": "La Flotte en Ré", "freq": 225}, {"topo": "La Flotte", "freq": 5}, {"topo": "La Flotte [en Ré]", "freq": 5}, {"topo": "La Flotte île de Ré", "freq": 2}, {"topo": "la Flotte-en-Ré", "freq": 1}, {"topo": "La Flotte [La Flotte en Ré]", "freq": 1}, {"topo": "la Flotte en Ré", "freq": 1}]

-------------------------------------------------------------------
-- traitement des états (noms, en et fr, et sous-unités)
-- source : file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\TOUT_port_etat_02dec2020_silviaen%20cours_v0.xlsx
-------------------------------------------------------------------
set search_path = 'ports', public
select * from etats where etat_en like '%Papal%' 
select * from etats where etat like '%Royaume des Pays-Bas%'


update etats set etat = 'Etats pontificaux'  where etat_en like '%Papal%';
-- 51;

update etats set etat = 'Royaume uni des Pays-Bas', etat_en = 'United Kingdom of the Netherlands'  where etat like '%Royaume des Pays-Bas%';
-- 26;

select * from etats where  etat = 'USA';
-- Etats-Unis d'Amérique	Lousiana	United States	Louisiana

update etats set etat='Etats-Unis d''Amérique', etat_en='United States'  where subunit='Lousiana' and etat = 'USA';
update etats set etat='Etats-Unis d''Amérique'  where  etat = 'USA';
-- 34

select * from etats where  subunit = 'Lousiana'
update etats set subunit = 'colonies françaises d''Amérique' , subunit_en = 'French colonies in America' where subunit = 'Lousiana'
-- 3 lignes
select * from etats where subunit = 'colonies françaises d''Amérique'
update etats set subunit = 'Lousiana', subunit_en = 'Lousiana' , etat_en = 'United States' where uhgs_id ='B0000056' and dfrom = 1803;
select * from etats where uhgs_id ='B0000056';

select etat_en, * from etats where  subunit_en = 'Spanish colonies in America';
update etats set etat_en = 'Spain' where subunit_en = 'Spanish colonies in America' and etat_en is null;
-- uhgs_id ='B0000056' and dfrom = 1763;

select etat_en, * from etats where  subunit_en = 'Canary Islands';
select etat_en, * from etats where  etat = 'Espagne';
update etats set etat_en = 'Spain' where etat = 'Espagne';
-- 88 

select * from etats where subunit = 'île d''Elbe'
update etats set subunit=null, subunit_en = null where subunit = 'île d''Elbe'
-- A0223338
-- A0232139
select * from etats where uhgs_id  in ('A0223338', 'A0232139')
select * from etats where subunit_en = 'Tuscany'
/* A0222997
A0238683 */
update etats set subunit = null where subunit_en = 'Tuscany';
-- 2 lignes

-- remplacer tous les Empire français par France
select * from etats where etat = 'Empire français'
update etats set etat = 'France', etat_en ='France' where etat = 'Empire français';
-- 80 lignes

update etats set etat_en = 'Spain' where subunit_en = 'Spanish colonies in America' and etat_en is null;

select * from etats where subunit='multi-Etat' 
update etats set subunit = null where subunit='multi-Etat' ;
-- 1 lignes

select * from etats where etat = 'Ville libre de Dantzig'
update etats set etat = 'Dantzig', etat_en = 'Danzig' where etat = 'Ville libre de Dantzig'
-- 1 ligne

select * from etats where etat = 'Présides de Toscanes'

select * from etats where etat like '%Naple%'

----------------------

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en 
from etats
where uhgs_id in ('A0255750', 'A0263760', 'A0276276', 'A0288450', 'A0295717', 'A0300485')
update etats set subunit_en = 'Regency of Algiers' where uhgs_id in ('A0255750', 'A0263760', 'A0276276', 'A0288450', 'A0295717', 'A0300485')
-- Ottoman empire	Regency of Algiers	Empire ottoman	Régence d'Alger
-- 6 lignes

-- B0000971
-- A0176425 Luri (supprimé)
select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en 
from etats
where uhgs_id in ('A1945598', 'A1945818', 'B2044556')

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en 
from etats
where uhgs_id in ('A0220523', 'A0221139', 'A0222919', 'A0223331', 'A0223524', 'A0223759', 'A0224225')

update etats set subunit_en = 'Sardinia' 
where uhgs_id in ('A0220523', 'A0221139', 'A0222919', 'A0223331', 'A0223524', 'A0223759', 'A0224225', 'A0227817', 'A0234339', 'A0238716')
-- 10 lignes
update etats set subunit_en = 'Sardinia' 
where uhgs_id in ('A0242553', 'A0247243', 'A0249318', 'A0250475', 'A0252572')



select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where uhgs_id in ('A0235718')
update etats set dto = 1797 where uhgs_id in ('A0235718')
Varazze		Republic of Genoa		République de Gênes			1797
Varazze		Ligurian Republic		République ligurienne		1797	1805
Varazze		French Empire		Empire français		1805	1815

insert into etats (uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto, country2019_name, toponyme_standard_fr, geonameid, name_en, admin1_code, latitude, longitude, tgnid)
select uhgs_id, toponyme, 'République ligurienne', subunit, 'Ligurian Republic' , subunit_en, 1797, 1805 , country2019_name, toponyme_standard_fr, geonameid, name_en, admin1_code, latitude, longitude, tgnid
from etats
where uhgs_id in ('A0235718')
-- 1 ligne

insert into etats (uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto, country2019_name, toponyme_standard_fr, geonameid, name_en, admin1_code, latitude, longitude, tgnid)
select uhgs_id, toponyme, 'France', subunit, 'France' , subunit_en, 1805, 1815 , country2019_name, toponyme_standard_fr, geonameid, name_en, admin1_code, latitude, longitude, tgnid
from etats
where uhgs_id in ('A0235718') limit 1
-- 1 ligne

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where subunit='Frise' -- uhgs_id in ('A0617176')

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where subunit='Hollande';

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where subunit='Zélande';

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where subunit='Groningue'; 

update etats set subunit_en = 'Friesland' where subunit='Frise';
update etats set subunit_en = 'Holland' where subunit='Hollande';
update etats set subunit_en = 'Zeeland' where subunit='Zélande';
update etats set subunit_en = 'Groningen' where subunit='Groningue';

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where subunit='Pays de la Généralité';
update etats set subunit_en = 'Generality Lands' where subunit='Pays de la Généralité';

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where uhgs_id = 'A0834735'

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , * 
from etats where etat_en = 'Kingdom of Denmark'
update etats set etat_en = 'Denmark' where etat_en = 'Kingdom of Denmark';


select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , * 
from etats where etat_en like 'Kingdom of %'

select distinct etat_en from etats where etat_en like 'Kingdom of %'
update etats set etat_en = 'Sweden' where etat_en = 'Kingdom of Sweden';


select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , * 
from etats where etat_en in ('Sweden', 'Denmark')

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where uhgs_id in ('A1759102', 'A1763109', 'A1768313', 'A1797542')

update etats set etat_en = 'Russian Empire', subunit_en = 'Black Sea' where uhgs_id in ('A1763109', 'A1409167', 'A1416883', 'A1825289');
update etats set subunit_en = 'Baltic Sea' where uhgs_id in ('A1759102', 'A1797542');
update etats set subunit_en = 'White Sea' where uhgs_id in ('A1768313');

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where uhgs_id = 'A1965411'

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where subunit = 'Régence de Tunis'

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where etat = 'Russie'

update etats set subunit_en='Regency of Tunis', etat_en = 'Ottoman empire' where subunit = 'Régence de Tunis';
-- 10

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where etat like '%ottoman%'

update etats set  etat_en = 'Ottoman empire' where etat like '%ottoman%';
-- 68


select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where etat = 'Grande-Bretagne'
order by subunit

update etats set  subunit_en = 'England' where etat= 'Grande-Bretagne' and subunit='Angleterre'
--78
update etats set  subunit_en = 'Scotland' where etat= 'Grande-Bretagne' and subunit='Ecosse'
--15
update etats set  subunit_en = 'Irland' where etat= 'Grande-Bretagne' and subunit='Irlande'
-- 3
update etats set  subunit_en = 'Wales' where etat= 'Grande-Bretagne' and subunit='Pays de Galles'
--2
update etats set  subunit_en = 'Channel Islands' where etat= 'Grande-Bretagne' and subunit='Iles anglo-normandes'
-- 1

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats 
where toponyme = 'Saint Lunaire Bay'
-- B2122973 Saint Lunaire Bay	Grande-Bretagne	colonies britanniques d'Amérique	Great Britain	British colonies in America			Canada	Les Griquets
update etats set uhgs_id = 'B0000971'  where toponyme = 'Saint Lunaire Bay';



select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats


update etats set uhgs_id = 'A0124009' where uhgs_id = 'A0176425'
-- Luri 

-------------------------------------------------------------------
-- IMPORTER un fichier contenant les toponymes standardisés pour tous les ports
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\labels_lang.csv
-- encoding : latin1, ; as separator. 
-------------------------------------------------------------------

alter table labels_lang_csv rename column français to fr;
alter table labels_lang_csv rename column gb to en;
alter table labels_lang_csv rename column pointcall_uhgs_id to key_id;

select * from labels_lang_csv llc where key_id = 'A0160615'
A0160615	Paimbœuf	Paimbœuf
A0123256	Le Pas au Bœuf	Le Pas au Bœuf

-- attention au oe
update labels_lang_csv set fr = 'Paimbœuf', en='Paimbœuf' where key_id = 'A0160615';
update labels_lang_csv set fr = 'Le Pas au Bœuf', en='Le Pas au Bœuf' where key_id = 'A0123256';

select * from labels_lang_csv llc where key_id = 'A0079415';
-- Águilas

-------------------------------------------------------------------
-- Ajouter dans la table labels_lang_csv un fichier contenant les flags standardisés 
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\labels_lang.csv
-- encoding : latin1, ; as separator. 
-------------------------------------------------------------------

select distinct label_type from labels_lang_csv;
/*
flag
toponyme*/

comment on table labels_lang_csv is 'Table contenant les étiquettes standardisées pour l''affichage des items en francais ou en anglais (fr, en): flags, noms de ports, etc. 
Créé le 2 février 2021 à partir des fichiers excels Silvia de Décembre 2020/Janvier 2021
- flag : file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\flag%20FR%20code%20et%20nomenclature.xlsx
- toponyme : file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\TOUT_port_etat_02dec2020_silviaen%20cours_v0.xlsx'

-------------------------------------------------------------------

select uhgs_id, toustopos, shiparea, latitude, longitude from port_points pp where country2019_name is null
A0171758 {Charente}
A9999997 {Divers ports [entre Alicate en Sicile et Saint Raphael],Divers ports [entre Ancone et Cannes],Divers ports [entre Bagnara en Calabre et Toulon],Divers ports [entre Barcelone et La Ciotat],Divers ports [entre Barcelone et Port Vendres],Divers ports [entre Barleta en Pouille et Porte Ferraro],Divers ports [entre Barleta et Bandol],Divers ports [entre Barleta et Toulon],Divers ports [entre Barletta en Pouille et Toulon],Divers ports [entre Barletta et Livourne],Divers ports [entre Bisceglia et Gênes],Divers ports [entre Brindisi et Isles d' hieres],Divers ports [entre Cap Corse et Gênes],Divers ports [entre Cap Stillo en Calabre et Cervo],Divers ports [entre Castellamar et Vado],Divers ports [entre Catagne en Sicile et Gapeau],Divers ports [entre Catagne en Sicile et Nice],Divers ports [entre Catagne en Sicile et Porto Fino],Divers ports [entre Catagne et Araisso],Divers ports [entre Catane en Sicile et Bandol],Divers ports [entre Civitavecchia et Theulés],Divers ports [entre Civitavecchia et Toulon],Divers ports [entre Corigliano en Calabre et Nice],Divers ports [entre Escuillet en Sicile et Isles Sainte Marguerites],Divers ports [entre Escuillete en Sicile et La Spezia],Divers ports [entre Fiume et Isles d' hieres],Divers ports [entre Fiume et Nice],Divers ports [entre Gallipoli et Porto Ferraio],Divers ports [entre Gioia Tauro et La Ciotat],Divers ports [entre Livourne et Theulé],Divers ports [entre Malaga et Sète],Divers ports [entre Manfredonia et Porto Longone],Divers ports [entre Manfredonia et Sorrento],Divers ports [entre Manfredonia et Toulon],Divers ports [entre Marsalla et Iles de Sainte Marguerite],Divers ports [entre Marsalla et Porquerolle],Divers ports [entre Mayorque et Sète],Divers ports [entre Mayorque et Sette],Divers ports [entre Messine et Gênes],Divers ports [entre Messine et Villefranche],Divers ports [entre Montalto et Toulon],Divers ports [entre Naples et Golfe Juan],Divers ports [entre Naples et Toulon],Divers ports [entre Nicotra en Calabre et Bandol],Divers ports [entre Olivery en Sicile et Gapeau],Divers ports [entre Ortone a Mare côte de Naples et Golfe Juan],Divers ports [entre Pizzo en Calabre et Cassis],Divers ports [entre Pizzo en Calabre et Saint Raphael],Divers ports [entre Ponte di Goro et Golfe de la Spezia],Divers ports [entre Ponto di Goro et Civitavecchia],Divers ports [entre Procida et La Ciotat],Divers ports [entre Roxane dans le royaume de Naples et Port Maurice],Divers ports [entre Roxella en Calabre et Laigueglia],Divers ports [entre Roxella en Calabre et Nice],Divers ports [entre Saint Falion et Sète],Divers ports [entre Sainte Eufemie en Calabre et Porto Ferraio],Divers ports [entre Sainte Marie de Soverato en Calabre et Laigueglia],Divers ports [entre Sirella en Calabre et Gênes],Divers ports [entre Terrassino dans la Romagne et Porto Venere],Divers ports [entre Terreneuve en Sicile et Bandol],Divers ports [entre Terreneuve en Sicile et Porto Venere],Divers ports [entre Terreneuve en Sicile et Toulon],Divers ports [entre Trani en Pouille et Bandol],Divers ports [entre Trapany et Nice],Divers ports [entre Valence et Sète],Divers ports [entre Vietry dans le Royaume de Naples et Cavalaire],Divers ports d' Italie [entre Bagnara en Calabre et Marseille],Divers ports d' Italie [entre Castellamar et Alassio],Divers ports d' Italie [entre Cruesli dans le royaume de Naples et Nice],Divers ports d' Italie [entre Escuillete en Sicile et Marseille],Divers ports d' Italie [entre Naples et La Ciotat],Divers ports d' Italie [entre Terreneuve en Sicile et Ile d' Hieres],Divers ports de la côte d' Espagne [entre Almazaron et Port Vendres],Divers ports de Sardaigne,Divers ports de Sicile,Divers ports du Rousillon,Divers ports du royaume de Naples,Divers ports entre [Seistre de Levante et Toulon],Divers ports entre [Tour de l' Annonciation dans le golfe de Naples et Nice],Divers ports entre[Cadansano en Calabre et Laigueglia],Diverts ports [entre Manfredonia et Gênes],Diverts ports [entre Vietry dans le royaume de Naples et Agay]}
{$talamo,27 lieues au sud de Malte,A deux lieues de Mataron,à l' aventure,A l' ouest de L' Anos d' Almérie,à l’aventure,Alborough,Allicante,Alliquante [Alicante],Alloa,Alvert,Anaries,Angleterre,Angola,Appeldaur en Angleterre,Arcquier,Arson,Arundell,Atlantique nord,Atlantique Nord,Au plomb,Auddewall,Aurais,Auray,Avallon,Ayr,Barda près Livourne,Baretaly en Corse [Barrettali],Barfleur,Barking,Barmouth,Barnstable,Barre demont,Batelage,Bauvoir,Beddeford,Beddefort,Beddtort,Belle Ile,Belle île,Belle île en mer,Belle Isle,Belle Isle en Mer,Berdin,Bergen,Bergen en Norvege,Berghen,Bergue,Berwick,Betagne,Bettiford,Bilbao,Biliès,Bisquai,Blackeney,Blanckenberg,Blaye,Borrowtones,Boston,Bourcefranc,Bourg,Bourgneuf,Bournefeu,Bourneuf,Brèm,Breme,Bremen,Bremmen,Bristol,Broadstairs,Broussaille en Corse,Bruge,Bruges,Bruuge,Callella,Calvana dans le royaume de Naples,Camarthen,Campine,Camprin,Caole,Capeau,Carnac,Carry,Castelnaut,Catwick,Catwik,Catwyck,Cayes Saint Louis,Cerfe dans l' archipel,Chaillevette,Champagné près Luçon,Chapus,Chaput,Charron,Château d' Oléron,Château de l' île d' Oléron,Chenal de Lilleau,Chichester,Chischester,Christiana,Christiansund,Cley,Colchester,Colonies américaines,Colonies anglaises,Concarno,Condat,Copenagues,Corck en Finlande,Cote [d' Or],Côte d' Angole en Guinée,Cote d' or,Côte d' Or,Côte d’Angole,Cote de Guinée,Couarde Ile de Ré,Couarde île de Ré,Couingburn,Courege,Croizic,Cromer,Dans le courand,dans les Couraux,Dans les Couraux,Dantizg,Dantzig,Dedans le Pertuis,Dercie,Deva en Espagne,Dol,Dordrecht,Dorn,Dornenée,Dort,Dundee,Dundée,Ebbin en Pologne,Eguillon,Eguilon,Embden,en deor des couraux [en dehors des Couraux],Enebon [Hennebont],Engien,Enjien,Ennebon [Hennebont],Escalea,Eslaphes,Estaples,Etapes,Etaples,Exeter,Eysadette,Fahrsund,Falcots,Falmouth,Farhsun,Farhsund,Fecamp,Fenotiva,Feryland,Fescamp,Feversham,Fleckfjord,Flekkefiord,Flessingue,Folkstone,Fouras,Frederichstad,Frederickshull,Frédériksald,Frederiskhals,Fredrickale,Fredrickshald,Fredrickshale,Fredrickslad,Fredrikshald,Galway,Genest,Georgetown [GB],Georgetown [USA],Gernesey,Gibette [Torre della Civetta],Gloucester,Gloucester [GB],Golfe de Castagnier,Gonaives,Gottenbourg,Gottenbourge,Goujean,Goujoan,Goujouan,Grandeville,Graticiara,Gravellines,Gravesend,Greenock,Greenoock,Greenwich,Grenock,Grevesend,Gualbar Cote d' Affrique,Guernesey,Guernezay,Guernezey,Haoudewalde en Suède,Harwich,Harwick,Hastings,Henebon,Hennebon,Herque,Herquy,Hersu en Corse,Holmestrand,Hors du Pertuis,Hull,Hythe,Ile Bourbon,Ile d’Oleron,Ile d’Oléron,Ile de France,Ile de Mai,Ile Dieu,Ilfracombe,Ille d' Oléron,Illisble,illisible,Illisible en mer Baltique,Ipswith,Isle de Dieu,Isle de May,Isle de Retz,Isle Dieu,Isles Rousse,Itlande,Itlandes,James River,Jersey,Kirkaldie,Konigsberg,Konisberg,Könisberg,Konisbérg,Konisbergue [Könisberg],Konnigsberg,Konniqueberg [Könisberg],Kragervé,L’aventure,La Mariaquer,La Marque,La Martinique,La Perotine,La Pérotine,La Perottine,La Rivière de Bordeaux,La Teste,La Tour de la Gibete [Torre della Civetta],La Tramblade,La Tremblade,Laiguillon,Lamariaquaire,Lamariaquer,Landerneau,Launnay,Lauwing,Lauzure,Le Braud,Le Chateau [d' Oléron],Le long des côtes [de Normandie],Le Long des côtes [de Normandie],Le Plomb,Le Plome,Le Vivier,Lecques,Lée,Lefagavis,Leith,Limerick,Limskilus,Lioopol,Lisbonne,Liverpol,lle de Ré,Loix,Lomariaquer,London,Loumariaquer,Lubeck,Lunckilns,Luogo en rivière de Gênes,Lydd,Lyme Regis,Maden,Madere,Madère,Maerluys,Maeslays,Maesluys,Malden,Mandahl,Mandal,Marcili (Marcilly?),Maren,Marene,Marens,Margate,Marrenne,Martinique,Martrou,Masnoualy,Mayon,Mayoure,Measen,Memel,Mestredame,Middlebourg,Miquelon,Moric,Moriq,Morit,Morlan,Mornac,Mortagne,Moss,Moulouty en Corse,Nascaw,Nedeness,Negton en Ecosse,Netteroe,Netteroé,Neucaster,Newbedfort,Newcastel,Newcaster,Newhaven,Newport,Newyorck,Nieuport,Nivelan,Noordfarra [Nord Ferro],Noordwick,Noordwyck,Noorswick,Noortwick,Nord Farro,Nordbergen,Norden,Nordfolk,Nordwyck,Normoutier,Normoutiér,Nortbergen,Nortwick,Norvaige [Norvège],Norvege,Norvège,Norwig,Nouvelle Angleterre,Nouvelle Orléans,Nouvelle York,Nowergue,Ollonne,Oporto,Orée,Ortege,Osternsoer,Padstow,Parth,Pêche du poisson frais,Pegelsham,Peroisic,Perotine,Pérotine,Pertuis,Philadephie,Pilau,Pilon,Plimout [Plymouth],Plomb,Plouharnel,Plymouthe,Pointe à Pitre,Pointe du Chapus,Pomavelo,Pomeles,Pontaves,Ponteves,Pontevés,Pontevez,Pontheves,Pool,Poole,Pornavalo,Pornic,Porsmouth,Port Launay,Portalty,Porterieux,Portevés,Portion,Portmavallo,Portnavallo,Portnavalo,Porto en Espagne,Quernic,Quimperlay,Rachusa,Rade de Ré,Ramsgate,Ransgate,Ré,Regica [Riga?],Reibnits,Rhé,Rhedon,Rhoméo,Riberon,Riga,Rivedoux,Rivière de Bordeaux,Riviere de Soudre,Rivierre de Bordeaux,Rochelle,Rocherfort,Rochester,Rognan,Rome en Corse,Rostock,Roterdam,Rotterdam,Rottérdam,Saint André de Cusac,Saint André de Cuzac,Saint Antoine,Saint Antoine de Carrara,Saint Domingue,Saint Esprit,Saint Hubes,Saint Marin de Ré,Saint Martain de Ré,Saint Petersbourg,Saint Savignen,Saint Savinien,Saint Ubes,Saint Ubes en Portugal,Saint Valery,Saint Vallerie,Saint Vallery,Sainte,Sainte-Lucie,Sainte Vallery,Saintes,Salette,Sallenelle,Sandwich,Sartet en Corse,Scerque,Scharam,Schevelinge,Schevelingue,Scheveningen,Sefalonico,Seudre,Seudres,Shield,Shielde,Shields,Shoram,Sion en Espagne,Sira,Socoa,Song,Soubize,Souptanon [Southampton],Southampton,Southwold,Southwould,St-Martin,Stancho,Stella en Catalogne,Stetin,Sudre,Swal,Syra,Tabago,Teoule,Terranovi,Terver,Terves,Teste,Teveno,Therebis,Tirimouth,Toctolla,Torramourre,Tour de la Gilette,Tour de Mocessa,Tourneuve,Tourreme,Tourreneuve en Toscane,Tousberg,Treguier,Tremblade,Tréport,Tyngmouth,Unnamed,Vendre,Verdon,Vérdon,Vian,Vieuport,Villanova en Portugal,Virginia,Virginie,Vlandringe,Vlardinge,Warberg,Warkingson,Waterford,Watreford,Waymouth,Weimouth,Wells,Weymouth,Whitby,Whitehaven,Whivenor,Woobridge,Yarmouth,Ysle de Man,Ysle Dieu,Zecriekzee,Zeerickzee,Zeeryckzee,Zerickzee,Zerickzée,Zériczée,Zierickzee,Zierkzee}
D0000008 {Chine}
B0000720 {Portsmouth}

/*
A9999997	{Divers ports [entre Alicate en Sicile et Saint Raphael],Divers ports [entre Ancone et Cannes],Divers ports [entre Bagnara en Calabre et Toulon],Divers ports [entre Barcelone et La Ciotat],Divers ports [entre Barcelone et Port Vendres],Divers ports [entre Barleta en Pouille et Porte Ferraro],Divers ports [entre Barleta et Bandol],Divers ports [entre Barleta et Toulon],Divers ports [entre Barletta en Pouille et Toulon],Divers ports [entre Barletta et Livourne],Divers ports [entre Bisceglia et Gênes],Divers ports [entre Brindisi et Isles ]}			
D0000008	{Chine}	PWN-CANT	31.23222	121.4692
	{$talamo,Alborough,Allicante,Alliquante [Alicante],Alloa,Alvert,Anaries,Angleterre,Angola,Appeldaur en Angleterre,Arcquier,Arson,Arundell,Atlantique nord,Atlantique Nord,Au plomb,Auddewall,Aurais,Auray,Avallon,Ayr,Barda près Livourne,Baretaly en Corse [Barrettali],Barfleur,Barking,Barmouth,Barnstable,Barre demont,Batelage,Bauvoir,Beddeford,Beddefort,Beddtort,Belle Ile,Belle île,Belle île en mer,Belle Isle,Belle Isle en Mer,Berdin,Bergen,Bergen en Norvege,Berghen,Bergue,Berwick,Betagne,Bettiford,Bilbao,Biliès,Bisquai,Blackeney,Blanckenberg,Blaye,Borrowtones,Boston,Bourcefranc,Bourg,Bourgneuf,Bournefeu,Bourneuf,Brèm,Breme,Bremen,Bremmen,Bristol,Broadstairs,Broussaille en Corse,Bruge,Bruges,Bruuge,Callella,Calvana dans le royaume de Naples,Camarthen,Campine,Camprin,Caole,Capeau,Carnac,Carry,Castelnaut,Catwick,Catwik,Catwyck,Cayes Saint Louis,Cerfe dans ...]}			
H4444444	{Araisse en Corse,Arcudi,Ardon,Aux Plages,Barraustanes en Ecosse,Bewacarils,Borestenel en Ecosse,Bousaille en Corse,Brethegthiemton,Bussagia en Corse,Cadansano en Calabre,Carland,Chastelneau,Chrudervhal,Cootyes Plaat,Coroncau ou Sainte Catherine dans Golfe de Salonique,Cruesli dans le royaume de Naples,Doarcham,Encet,Engermont,Entel,Etamaville,Ferseau,Firnmes,Fouennée,Gaanstran,Golfe de Zeiton,Griesval,Groizilles,Gurna,Intel,Isle Larcenat aux bouches de Bonifasse,Keing,Kencarven,Maan,Madevesy,Malines,Maloure,Measy,Menidal,Molistenes en Ecosse,Moltouly en Corse,Monesterace en Calabre,North Cape Irlande,Posgroom,Roine,Roumavechert,Satalie,Serva en Catalogne,Signa,Souer,Suedpomel,Thiomée,Vandaire}			
H5555555	{Bretagne,Cabotage,Cabotage en Bretagne,Plages [Cabotage],Pour la Cote [Cabotage]}			
H6666666	{A l' aventure,A l' aventure dans la province [Bretagne],Unknown}			
H9999999	{Aurigny,Illisible,Martrou,Par les travers de Barcelonne,Unknown,Unnamed}			
A1963986	{Baltique,la Baltique,La Baltique,La mer baltique,La mer Baltique,mer Baltique,Mer Baltique,Mer Baltqiue}	BAL-BASO	56.103909	18.493651
A1964713	{Mediteranée,Méditeranée,Mediterranée,Méditerranée}	MED-BALS	39.982171	5.9729
A1965206	{Levant en caravane}	MED-MEDE	34.542762	30.340576
B0000715	{Banc [de Terre Neuve],Banc de Terre-Neuve,Banc de Terre Neuve,Banc de Terreneuve,Bancs de Terre Neuve,Grand Banc [Terre Neuve],Grand banc de Terre neuve,Grand banc de Terre Neuve,Grand Banc de Terre neuve,Grand Banc de Terre Neuve,Le Banc [de Terre-Neuve],Le Banc [de Terre Neuve]}	ACA-NEWF	46.077803	-51.943359
B0000935	{Groenland}		61.041268	-40.283207
C0000006	{Afrique,Côte d'' Afrique,Cote d'' Afrique [de l'' Ouest],Côte d'' Afrique [de l'' Ouest],Cote d’Afrique}	ACE-CTIM	7.08563	-14.263916
C0000013	{Mozambique}	ISW-MOZA	-14.847107	42.487792
D0000004	{Indes orientales,Indes Orientales,Les Indes orientales}	INE-CORO	13.633176	85.400387
*/
 

-- Cas A0171758 {Charente}
select * from navigo.geo_general gg where pointcall_uhgs_id = 'A0171758'
select * from port_points pp where toponyme ilike '%Charente%' 
select * from port_points pp where uhgs_id = 'A0171758' and toponyme ilike '%Charente%' and country2019_name is null
select * from port_points pp where uhgs_id = 'A0171758 '
select * from navigo.pointcall p where pointcall_uhgs_id = 'A0171758 '; --espace à la fin en trop
select * from navigo.pointcall p where pointcall_uhgs_id = 'A0171758'
-- mise à jour de  navigocheck.check_pointcall cp pour ce uhgs_id : pas la peine, ils sont déjà "trimés"
update navigocheck.check_pointcall set pointcall_uhgs_id = 'A0171758' where pointcall_uhgs_id = 'A0171758 '
select * from navigocheck.check_pointcall p where pointcall_uhgs_id = 'A0171758'
select * from navigocheck.check_pointcall cp where pointcall_uhgs_id  = 'A0171758'
select count(*) from navigocheck.check_pointcall cp where pointcall_uhgs_id  = 'A0171758'
delete from port_points pp where uhgs_id = 'A0171758 '
-- 2898

--- Cas 2 B0000720 {Portsmouth}
select * from navigo.pointcall p where pointcall_uhgs_id = 'B0000720';
select * from navigo.geo_general gg where pointcall_uhgs_id = 'B0000720'
select * from ports.port_points gg where uhgs_id = 'B0000720'
update ports.port_points set latitude = 45.07556, longitude = -70.69472  where uhgs_id = 'B0000720';
update ports.port_points set toponyme = 'Porthmouth', geom=st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where uhgs_id = 'B0000720';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857), 3857) where uhgs_id = 'B0000720';
update ports.port_points set country2019_iso2code = k.country2019_iso2code, country2019_name=k.country2019_name, country2019_region=k.country2019_region
from (
	select country2019_iso2code , country2019_name , country2019_region from ports.port_points gg where toponyme = 'Portland'
	) as k  
where uhgs_id = 'B0000720';
update navigo.geo_general set 

-- cas 3 D0000008 {Chine}
select * from navigo.pointcall p where pointcall_uhgs_id = 'D0000008';
 -- 2 lignes
select pkid, pointcall_uhgs_id from navigocheck.check_pointcall cp    where pointcall_uhgs_id = 'D0000008';
58208
58387
select pointcall_uhgs_id from navigocheck.uncertainity_pointcall up   
where pkid in (select pkid from navigocheck.check_pointcall cp    where pointcall_uhgs_id = 'D0000008')
-- Je lui attribue les coordonnées de Shanghai, car 2 pointcall correspondent. Mais je marque pointcall_uncertainity à -1
update ports.port_points set latitude = 31.23222, longitude = 121.4692  where uhgs_id = 'D0000008';
select * from port_points pp where uhgs_id = 'D0000008';
update ports.port_points set toponyme = 'Shanghai', geom=st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where uhgs_id = 'D0000008';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857), 3857) where uhgs_id = 'D0000008';
update navigocheck.uncertainity_pointcall up set pointcall_uhgs_id = -1     
where pkid in (select pkid from navigocheck.check_pointcall cp    where pointcall_uhgs_id = 'D0000008'); -- localisation générique

------------------------------------------------------------------------
-- Traitement de l'incertitude des points (localisation des ports)
-- Importer le fichier portic_v4.ports.cotes_rivers-20201202.sql
-- Il contient les rivières WDBII_river_f_L01 à WDBII_river_f_L11 et le trait de cote de la NOAA GSHHS_f_L1.shp
------------------------------------------------------------------------
-- importer les fichiers geo (cote et rivers)
psql -U postgres -d portic_v6 -f "C:\Travail\Data\Navigo_25jan2021\portic_v4.ports.cotes_rivers-20201202.sql"

alter table ports.port_points add column ogc_fid serial;
alter table ports.port_points add primary key (ogc_fid);

alter table ports.port_points add column distance_river_meters float;

update ports.port_points p set distance_river_meters = k.min from (
select pp.ogc_fid , min(st_distance(pp.point3857, line_3857 )) 
from ports.rivers_f tc, ports.port_points pp
where   st_distance(pp.point3857, tc.line_3857 ) is not null and point3857 is not null
group by pp.ogc_fid) as k 
where k.ogc_fid = p.ogc_fid
-- 1002 ; 3min17s

ALTER TABLE ports.port_points ADD COLUMN distance_cote_meters_precise float;

update ports.port_points p set distance_cote_meters_precise = k.min from (
select pp.ogc_fid , min(st_distance(pp.point3857, trait_cote3857 )) 
from ports.GSHHS_f_L1_3857 tc, ports.port_points pp
where   st_distance(pp.point3857, tc.trait_cote3857 ) is not null and point3857 is not null
group by ogc_fid) as k 
where k.ogc_fid = p.ogc_fid
-- 1002, 11 min 58

select * from ports.port_points p limit 10
select min(distance_cote_meters_precise), max(distance_cote_meters_precise), avg(distance_cote_meters_precise) 
from port_points where distance_cote_meters_precise is not null
-- 6.648180805487549	1145144.1812522141	9764.489833683445

/*
alter table port_points add column distance_cote float;
update port_points set distance_cote = case when distance_cote_meters> distance_cote_meters_precise then distance_cote_meters_precise else distance_cote_meters end 
*/

alter table port_points add column distance_water float;
update port_points set distance_water = case when distance_cote_meters_precise> distance_river_meters then distance_river_meters else distance_cote_meters_precise end
-- 1008

select min(distance_water), max(distance_water), avg(distance_water) from port_points where distance_water is not null
-- 6.648180805487549	482358.5755038081	5237.565775188685

select toponyme, round(distance_water/1000) as dw, * from  port_points where distance_water > 20*1000
order by dw
-- 38

select toponyme, round(distance_water/1000) as dw, uhgs_id, longitude, latitude,  country2019_name 
from  port_points where distance_water > 10*1000
order by dw 
-- 93

-- Laisser l'appréciation concernant la généricité du point à Silvia (qui corrigera les positions des ports comme Bruges ou Tunis)
-- La requete à faire : mise à jour de navigocheck.uncertainity_pointcall
/*
update navigocheck.uncertainity_pointcall up set pointcall_uhgs_id = -1     
where pkid in (select pkid from navigocheck.check_pointcall cp    where pointcall_uhgs_id in (-- precisier la liste --));
*/
------------------------------------------------------------------------

alter table ports.port_points add column belonging_states text;
alter table ports.port_points add column belonging_substates text;
alter table ports.port_points add column belonging_states_en text;
alter table ports.port_points add column belonging_substates_en text;


update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, etat, case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat_en) :: text as belonging_en
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id
        -- 659

--------------------------------------------
-- 21 etats à spécifier et toponyme_standards (fr/en)
--------------------------------------------
select toponyme, uhgs_id, longitude, latitude,  country2019_name 
from ports.port_points p  where belonging_states is null and country2019_name<>'France'
order by country2019_name

-- Vérifier aussi les ports de corse (country2019_name='France')
-- Leuri en Corse [Luri] --- Corse
 

------------------------------------------------------------------------
-- Le cas de Saint Domingue (erreur de codage de Silvia)
--  fichier siège d'amirauté et ports obliques mail Silvia du 11 janvier 2021
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\toponymes_fr_ports_FRANCE_corrigé%20Silvia%20(1).csv
------------------------------------------------------------------------

select * from ports.port_points where uhgs_id = 'A0171758' and latitude is null
-- ok, absent

select * from ports.port_points where uhgs_id = 'B2171563' -- haiti
select * from navigo.geo_general gg where pointcall_uhgs_id = 'B2171563' -- plein de pointcall


B2045171
B2171563
select * from ports.port_points where uhgs_id = 'B2047921' -- Saint Domingue
select * from navigo.geo_general gg where pointcall_uhgs_id = 'B2047921' -- aucun point
Saint Domaingue point générique
19.12941822838699, -72.47149733759645


select distance_water from port_points pp where uhgs_id = 'B2047921' 

-- rajouter un point Saint Domingue copie de B2171563 (haiti)
-- Puis modifier l'appartenance de B2171563 : colonie espagnole; mail de Silvia le 11 janvier
-- Puis modifier dans navigo.geo_general et navigo.pointcall les références B2171563 devient B2047921
-- fichier siège d'amirauté et ports obliques
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\toponymes_fr_ports_FRANCE_corrigé%20Silvia%20(1).csv
select * from ports.port_points where uhgs_id = '' -- haiti

insert into ports.port_points (uhgs_id, latitude, longitude, province, geom, point3857, toustopos, shiparea, topofreq, toponyme, country2019_name, country2019_iso2code, country2019_region, belonging_states, belonging_states_en)
select uhgs_id, 19.12941822838699, -72.47149733759645, 'Saint-Domingue', 
st_setsrid(st_makepoint(longitude::float, latitude::float), 4326),
st_setsrid(st_transform(st_setsrid(st_makepoint(longitude::float, latitude::float), 4326), 3857), 3857),
toustopos, shiparea, topofreq, toponyme, 
country2019_name, country2019_iso2code, country2019_region, 
belonging_states, belonging_states_en 
from ports.port_points 
where uhgs_id = 'B2171563' -- haiti

select ogc_fid, uhgs_id from ports.port_points where province = 'Saint-Domingue'
-- 1009
update ports.port_points set uhgs_id = 'B2047921' where ogc_fid = 1009
select ogc_fid, uhgs_id, province from ports.port_points where uhgs_id = 'B2047921'


update ports.port_points p set distance_cote_meters_precise = k.min , distance_water = k.min from (
select pp.ogc_fid , min(st_distance(pp.point3857, trait_cote3857 )) 
from ports.GSHHS_f_L1_3857 tc, ports.port_points pp
where   st_distance(pp.point3857, tc.trait_cote3857 ) is not null and uhgs_id = 'B2047921'
group by ogc_fid) as k 
where k.ogc_fid = p.ogc_fid

-- changer dans la base navigo et navigo_check (mais plus après le 4 février)
-- Reporter ceci dans l'intégration Python
update navigocheck.check_pointcall set pointcall_uhgs_id = 'B2047921' where pointcall_uhgs_id = 'B2171563'
-- 133
update navigo.pointcall set pointcall_uhgs_id = 'B2047921' where pointcall_uhgs_id = 'B2171563'
-- 133

select uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto , *
from etats
where uhgs_id = 'B2171563' ;

update etats set  uhgs_id = 'B2047921' where uhgs_id = 'B2171563' ;
-- 1
select * from etats where uhgs_id = 'B2047921' 
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, etat_en, subunit_en, dto, geonameid, name_en, admin1_code, latitude, longitude)
select country2019_name, toponyme_standard_fr, toponyme, 'B2171563', 'Espagne', 'colonies espagnoles d''Amérique', 'Spain', 'Spanish colonies in America', 1795, geonameid, name_en, admin1_code, latitude, longitude 
from etats where uhgs_id = 'B2047921' 
update 	etats set toponyme_standard_fr = 'Saint-Domingue' where uhgs_id = 'B2047921' 
-- Idéalement, préciser que Saint Domingue B2047921 est un point générique 

select toponyme, toponyme_standard_fr , belonging_states , belonging_states_en , belonging_substates , belonging_substates_en , state_1789_fr, substate_1789_fr 
from port_points pp where uhgs_id = 'B2047921'
update port_points pp set toponyme_standard_fr = 'Saint-Domingue' where uhgs_id = 'B2047921' 
select toponyme, toponyme_standard_fr , belonging_states , belonging_states_en , belonging_substates , belonging_substates_en , state_1789_fr, substate_1789_fr 
from port_points pp where uhgs_id = 'B2171563'

-- bugs qui restaient dans etat

select * from etats where uhgs_id in ('A1965005', 'C0000014', 'C0000005')
update etats set etat='multi-Etat', etat_en = 'Multi-state' where uhgs_id = 'A1965005';
update etats set etat='France' where uhgs_id in ('C0000014', 'C0000005')

------------------------------------------------------------------------
-- Mise à jour des provinces des colonies 
--  fichier siège d'amirauté et ports obliques mail Silvia du 11 janvier
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\toponymes_fr_ports_FRANCE_corrigé%20Silvia%20(1).csv
-- fait dans le code python
------------------------------------------------------------------------
select distinct status from ports.port_points
update ports.port_points set province='Canada', status='siège amirauté' where uhgs_id = 'B1965595'
select * from ports.port_points where uhgs_id = 'B1965595'

-- report dans Python : pas la peine, on sauve dans port_points_old puis on recopie dans le nouveau port_points
update ports.port_points set province='Canada', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id = 'B1965595';
update ports.port_points set province='Guadeloupe', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1977677', 'B1979312');
update ports.port_points_old set province='Guadeloupe' where uhgs_id in ('B1979292'); 
update ports.port_points set province='Guyane', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B2009606');
update ports.port_points set province='Martinique', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1973450', 'B1974029', 'B1973619'); 
update ports.port_points_old set province='Martinique' where uhgs_id in ('B1973827'); 
update ports.port_points set province='Saint-Domingue', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B2045171', 'B2047921', 'B2059102', 
'B2045255', 'B2053007', 'B2057431', 'B2044556', 'B2048070', 'B2054985', 'B2048242', 'B2049573', 'B2058684');
update ports.port_points_old set province='Saint-Domingue' where uhgs_id in ('B2047006', 'B2062485'); 

update ports.port_points set province='Sainte-Lucie', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1969630');
update ports.port_points set province='Tobago', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1975512');
update ports.port_points_old set province='Tobago' where uhgs_id in ('B1975524'); 
update ports.port_points_old set province='Réunion' where uhgs_id in ('C0000004'); 
update ports.port_points set province='Réunion' where uhgs_id in ('C0000004'); 



/*        # d'après le  mail Silvia du 11 janvier 2021,  "siège d'amirauté et ports obliques"
        # Fichier : file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\toponymes_fr_ports_FRANCE_corrigé%20Silvia%20(1).csv
        query = """update ports.port_points set province='Canada', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id = 'B1965595';
        update ports.port_points set province='Guadeloupe', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1977677', 'B1979312');
        update ports.port_points set province='Guyane', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B2009606');
        update ports.port_points set province='Martinique', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1973450', 'B1974029', 'B1973619');
        update ports.port_points set province='Saint-Domingue', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B2045171', 'B2047921', 'B2059102', 
        'B2045255', 'B2053007', 'B2057431', 'B2044556', 'B2048070', 'B2054985', 'B2048242', 'B2049573', 'B2058684');
        update ports.port_points set province='Sainte-Lucie', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1969630');
        update ports.port_points set province='Tobago', status='siège amirauté', oblique=true, has_a_clerk = true where uhgs_id in ('B1975512');"""
        self.execute_sql(query)*/

-- vérification qu'aucune autre amirauté n'est à spécifier à la main

select * from ports.port_points where country2019_name = 'France' and amiraute is null
select * from ports.port_points where belonging_states_en is null

select toponyme,  uhgs_id, longitude, latitude,  country2019_name , toponyme_standard_fr , toponyme_standard_en , belonging_states_en , belonging_substates_en ,belonging_states , belonging_substates 
from ports.port_points p  where  uhgs_id = 'A0210528' -- Cap Martin est un port en comté de Nice, sans standardisation de l'orthographe et mauvaise appartenance de l'état

select toponyme,  uhgs_id, longitude, latitude,  country2019_name , toponyme_standard_fr , toponyme_standard_en , belonging_states_en , belonging_substates_en ,belonging_states , belonging_substates 
from ports.port_points p  where  uhgs_id = 'B1965595'

--------------------------------------------------------------------------------------------------
-- Les appartenances de ports aux bureaux/directions de Ferme
-- Importer le fichier file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\fermes_ports.csv
-- table fermes_ports_csv
--------------------------------------------------------------------------------------------------

select * from ports.port_points where toponyme % 'Talmont'
update fermes_ports_csv set ferme_bureau = 'Sables d''Olonne' where uhgs_id = 'A0207992';
update fermes_ports_csv set ferme_bureau_uncertainty = 0 where uhgs_id = 'A0122971';

-- report dans Python : ok
alter table ports.port_points add column ferme_direction text ;
alter table ports.port_points add column ferme_bureau text ;
alter table ports.port_points add column ferme_bureau_uncertainty int ;

update ports.port_points p set ferme_direction = f.ferme_direction , ferme_bureau = f.ferme_bureau , ferme_bureau_uncertainty=f.ferme_bureau_uncertainty 
from fermes_ports_csv f where f.uhgs_id = p.uhgs_id 
-- 27

select * from ports.port_points where ferme_direction = 'La Rochelle';

--------------------------------------------------------------------------------------------------
-- Les directions des partenaires auxquels les ports appartiennent d'après le croisement toflit18 et navigo
-- Lecture et interprétation d'après le fichier 
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\datasprint_1789_LaRochelle\alignement%20Etats%20(1).xlsx
-- Mails janvier 2021 : Silvia, Guillaume et Thierry
--------------------------------------------------------------------------------------------------

-- a mettre dans le programme Python
CREATE OR REPLACE FUNCTION ports.extract_state_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2) from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_states::json) as elt  
					from ports.port_points p
					where belonging_states = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ports.extract_substate_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates::json) as elt  
					from ports.port_points p
					where belonging_substates = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ports.extract_state_en_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2) from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_states_en::json) as elt  
					from ports.port_points p
					where belonging_states_en = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ports.extract_substate_en_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates_en::json) as elt  
					from ports.port_points p
					where belonging_substates_en = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;
       
select ports.extract_state_fordate(belonging_states, 1789) from ports.port_points where uhgs_id = 'A0255750'
select ports.extract_substate_fordate(belonging_substates, 1789) from ports.port_points where uhgs_id = 'A0255750'

select ports.extract_state_en_fordate(belonging_states_en, 1789) from ports.port_points where uhgs_id = 'A0255750'
select ports.extract_substate_en_fordate(belonging_substates_en, 1789) from ports.port_points where uhgs_id = 'A0255750'

select substring(substate_1789 from 2 for char_length(substate_1789)-2)
from 
(select ports.extract_substate_fordate(belonging_substates, 1789) as substate_1789 from ports.port_points where uhgs_id = 'B2047921') as k

-- colonies françaises d'Amérique

select uhgs_id, substring(substate_1789 from 2 for char_length(substate_1789)-2)
from 
(select uhgs_id, ports.extract_substate_fordate(belonging_substates, 1789) as substate_1789 from ports.port_points where belonging_substates is not null) as k

-- report dans Python : ok
alter table ports.port_points add column state_1789_fr text ;
alter table ports.port_points add column substate_1789_fr text ;
alter table ports.port_points add column state_1789_en text ;
alter table ports.port_points add column substate_1789_en text ;
select uhgs_id, ports.extract_substate_fordate(belonging_substates, 1789) as substate_1789 from ports.port_points where belonging_substates is not null

-- a mettre dans le programme Python : ok
update ports.port_points set substate_1789_fr = ports.extract_substate_fordate(belonging_substates, 1789) 
where belonging_substates is not null;
-- 316
update ports.port_points set state_1789_fr = ports.extract_state_fordate(belonging_states, 1789) 
where belonging_states is not null;
-- 983
update ports.port_points set substate_1789_en = ports.extract_substate_en_fordate(belonging_substates_en, 1789) 
where belonging_substates_en is not null;
update ports.port_points set state_1789_en = ports.extract_state_en_fordate(belonging_states_en, 1789) 
where belonging_states_en is not null;

select state_1789_fr, substate_1789_fr, toponyme, province 
from ports.port_points
order by state_1789_fr, province 

-- a mettre dans le programme Python : ok

alter table ports.port_points add column partner_balance_1789 text ;
alter table ports.port_points add column partner_balance_supp_1789 text ;
alter table ports.port_points add column partner_balance_1789_uncertainty int ;
alter table ports.port_points add column partner_balance_supp_1789_uncertainty int ;

select distinct state_1789_fr, substate_1789_fr from ports.port_points order by state_1789_fr

-- Etranger pour partner_balance_supp_1789
update ports.port_points set partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=-1
where not (state_1789_fr  in ('France') or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Sénégal'));
-- 623

update ports.port_points set partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0
where state_1789_fr  in ('France') and substate_1789_fr is null;

update ports.port_points set partner_balance_supp_1789='colonies françaises', partner_balance_supp_1789_uncertainty=0
where state_1789_fr  in ('France') and substate_1789_fr is not null;
-- d'Afrique aussi (car le Sénégal est colonie française d'Afrique) ??

-- dans Navigo pas de départ pour le Sénégal mais plusierus négrier qui vont vers autres côtes africaines. Faudra comprendre s'ils sont dedans
update ports.port_points set partner_balance_supp_1789='Sénégal et Guinée', partner_balance_supp_1789_uncertainty=-1
where uhgs_id in (select uhgs_id from ports.port_points where country2019_region = '2'
and country2019_name in ('Senegal', 'Equatorial Guinea' , 'Gambia', 'Nigeria') and uhgs_id in ('C0000006');
-- 5
-- select uhgs_id, * from ports.port_points where country2019_name = 'Senegal': colonie française d'Afrique
-- C0000014 Gorée
-- C0000005 Senegal
-- C0000013	Mozambique : non, au Portugal
-- C0000006	Côte d' Afrique [de l' Ouest]

-- Alignement pour partner_balance_1789

--  Sénégal : Sénégal	dans Navigo pas de départ pour le Sénégal mais plusierus négrier qui vont vers autres côtes africaines. Faudra comprendre s'ils sont dedans
update ports.port_points set partner_balance_1789='Sénégal', partner_balance_1789_uncertainty=-1
where uhgs_id in (select uhgs_id from ports.port_points where country2019_name = 'Senegal');

-- Autriche + Mecklembourg; Oldenburg ; Papenburg ; Flandre autrichienne
update ports.port_points set partner_balance_1789 = 'Etats de l''Empereur' ,  partner_balance_1789_uncertainty=-1
where state_1789_fr in ('Autriche', 'Duché de Mecklenbourg', 'Duché d''Oldenbourg');
-- 8

update ports.port_points set partner_balance_1789 = 'Prusse' ,  partner_balance_1789_uncertainty=0
where state_1789_fr in ('Prusse');
--8

-- Quatre villes hanséatiques : 	Hambourg, Brême, Lübeck, Ville libre de Dantzig
update ports.port_points set partner_balance_1789 = 'Quatre villes hanséatiques' , partner_balance_1789_uncertainty=0
where state_1789_fr in ('Hambourg', 'Lubeck', 'Brême') or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Dantzig');
-- 4
update labels_lang_csv set en='Gdańsk' where key_id = 'A1204354' and label_type  = 'toponyme';
update ports.port_points set toponyme_standard_en = 'Gdańsk' where uhgs_id ='A1204354';
select * from ports.port_points where toponyme_standard_fr % 'Dantzig';

-- Etats-Unis	Etats-Unis d'Amérique
update ports.port_points set partner_balance_1789 = 'Etats-Unis' ,  partner_balance_1789_uncertainty=0
where state_1789_fr in ('Etats-Unis d''Amérique');
-- 32

-- 	Angleterre : Grande-Bretagne (colonies comprises)
update ports.port_points set partner_balance_1789='Angleterre',  partner_balance_1789_uncertainty=0
where state_1789_fr = 'Grande-Bretagne';
-- 146


update ports.port_points set partner_balance_1789='Angleterre',  partner_balance_1789_uncertainty=0
where state_1789_fr = 'Grande-Bretagne' AND partner_balance_1789 IS null;
-- 27 ok (nouveaux ports anglais du smogglage), le 27 janvier 2022

-- Danemark	Danemark (donc DK et Norvège)	
update ports.port_points set partner_balance_1789='Danemark', partner_balance_1789_uncertainty=0
where state_1789_fr = 'Danemark';
-- 41

-- Espagne	Espagne (colonies comprises) 
update ports.port_points set partner_balance_1789='Espagne',  partner_balance_1789_uncertainty=0
where state_1789_fr = 'Espagne';

-- Hollande	Provinces-Unies 
update ports.port_points set partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0
where state_1789_fr = 'Provinces-Unies';

--	Iles françaises de l'Amérique : colonies françaises d'Amérique	
update ports.port_points set partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0
where substate_1789_fr  = 'colonies françaises d''Amérique';
-- 19

-- Portugal :	Portugal (colonies comprises)
update ports.port_points set partner_balance_1789='Portugal',  partner_balance_1789_uncertainty=0
where state_1789_fr  = 'Portugal';
-- 13

-- Russie :	Russie
update ports.port_points set partner_balance_1789='Russie', partner_balance_1789_uncertainty=0
where state_1789_fr  = 'Russie';
-- 7

-- Suède	Suède (donc Suède, Poméranie suédoise, et Finlande)
update ports.port_points set partner_balance_1789='Suède', partner_balance_1789_uncertainty=0
where state_1789_fr  = 'Suède';
-- 25

-- Ports francs

select distinct state_1789_fr, province, amiraute, toponyme, uhgs_id 
from ports.port_points where province is not null and amiraute in ('Dunkerque', 'Bayonne', 'Marseille')
order by toponyme

-- Bayonne	Bayonne (port franc) + vérifier s'il faut ajouter Saint Jean de Luz parmi ports francs (3 navires)
-- Bayonne	A0187995
update ports.port_points set partner_balance_1789='Bayonne', partner_balance_1789_uncertainty=0
where uhgs_id = 'A0187995';
-- Dunkerque	Dunkerque	port franc
update ports.port_points set partner_balance_1789='Dunkerque',  partner_balance_1789_uncertainty=0
where uhgs_id = 'A0204180';
-- Marseille : Marseille	port franc
update ports.port_points set partner_balance_1789='Marseille',  partner_balance_1789_uncertainty=0
where uhgs_id = 'A0210797';
-- Lorient	: Lorient	port franc
update ports.port_points set partner_balance_1789='Lorient', partner_balance_1789_uncertainty=0
where uhgs_id = 'A0140266';

-- Petites Iles : île de Bouin et île de Noirmoutier (prendre tous les pointcall 	demander confimation à Thierry et/ou tester avec données Navigo
select distinct state_1789_fr, province, amiraute, toponyme, uhgs_id 
from ports.port_points where toponyme % 'Bouin' or toponyme  % 'Noirmoutier' or toponyme % 'Lorient' 
order by toponyme
-- Lorient	A0140266
-- Ile de Bouin	A0165077
-- Noirmoutier	A0136403
update ports.port_points set partner_balance_1789='Petites Iles', partner_balance_1789_uncertainty=-1
where uhgs_id in ('A0165077', 'A0136403');


-- Saint-Domingue	dans "colonies françaises d'Amérique" prendre uniquement les ports de Saint-Domingue (=Haïti actuel) et le pointcall "Saint-Domingue" générique
select distinct state_1789_fr, substate_1789_fr , province, amiraute, toponyme, uhgs_id 
from ports.port_points where province % 'Domingue' 
order by toponyme

update ports.port_points set partner_balance_1789='Saint-Domingue',  partner_balance_1789_uncertainty=0
where province % 'Domingue' ;

-- Saint-Jean de Luz	Saint-Jean de Luz
update ports.port_points set partner_balance_1789='Saint-Jean de Luz',  partner_balance_1789_uncertainty=-1
where uhgs_id = 'A0122594';
-- Saint Jean de Luz	A0122594

-- Export le 04 février 2021 pour Silvia (vérifications et cartographie)
select uhgs_id, toponyme, toustopos, toponyme_standard_fr , toponyme_standard_en , 
latitude , longitude ,
state_1789_fr, substate_1789_fr , state_1789_en, substate_1789_en , 
amiraute , province ,
partner_balance_1789, partner_balance_1789_uncertainty, partner_balance_supp_1789, partner_balance_supp_1789_uncertainty,
has_a_clerk, status, source_1787_available, source_1789_available, ferme_direction, ferme_bureau, ferme_bureau_uncertainty
from port_points pp 
--where amiraute like '%Olonne%'
order by state_1789_fr, substate_1789_fr , province , amiraute



update port_points set state_1789_fr = 'Espagne', substate_1789_fr = 'Espagne	colonies espagnoles d''Amérique', state_1789_en = 'Spain', substate_1789_en = 'Spanish colonies in America', 
partner_balance_1789='Espagne', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=-1
where uhgs_id = 'B2171563'

---------------------------------------------------------------------------------------------
-- reimport de geo_general après le 4 février 
---------------------------------------------------------------------------------------------

CREATE TABLE ports.geo_general_backup as (select * from ports.geo_general); -- 100438

drop table ports.geo_general;

CREATE TABLE ports.geo_general as (
select pointcall_name, pointcall_uhgs_id , geo_general__lat as latitude, geo_general__long as longitude, null as shippingarea 
from navigo.geo_general gg 
); -- 108534

update ports.geo_general p set shippingarea = g.shippingarea
from ports.geo_general_backup g where g.pointcall_uhgs_id = p.pointcall_uhgs_id
-- 108017 (il en manque 108534-108017 : 517)

select * from ports.geo_general where pointcall_uhgs_id = 'B0000720'
select distinct pointcall_uhgs_id from ports.geo_general where pointcall_uhgs_id like 'A0171758'

---------------------------------------------------------------------------------------------
delete from labels_lang_csv where label_type = 'toponyme';
-- reimport à la main du fichier étendu avec corrections sur les toponymes de Silvia du 4 février

-- import du fichier contenant les corrections sur les états de Silvia du 4 février
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\fix_silvia04022021.csv

-- fix d'une erreur sur Cap Martin
update fix_silvia04022021_csv set dfrom=null, dto = 1792 where uhgs_id = 'A0210528' and etat = 'Monaco'


update etats e set toponyme_standard_fr=k.toponyme_standard_fr, toponyme=k.toponyme, 
uhgs_id=k.uhgs_id , etat=k.etat, subunit=k.subunit , dfrom=k.dfrom, dto=k.dto, etat_en=k.etat_en, subunit_en=k.subunit_en
from 
	(select f.toponyme_standard_fr, f.toponyme, f.uhgs_id, f.etat, f.subunit , f.dfrom, f.dto, f.etat_en, f.subunit_en 
	from etats e, fix_silvia04022021_csv f
	where f.uhgs_id = e.uhgs_id ) as k 
where k.uhgs_id = e.uhgs_id
-- 7 valeurs

insert into etats (toponyme_standard_fr, toponyme, uhgs_id, etat, subunit , dfrom, dto, etat_en, subunit_en)
select toponyme_standard_fr, toponyme, uhgs_id, etat, subunit , dfrom, dto, etat_en, subunit_en from  fix_silvia04022021_csv f
where f.uhgs_id not in (select e.uhgs_id from etats e)
-- 17 

update etats e set toponyme_standard_fr = labels.fr 
from labels_lang_csv labels 
where e.uhgs_id = key_id and label_type = 'toponyme'
-- 1030 entrées

create table port_points_bk04fev2021 as 
(select * from port_points pp )
---------------------------------------------------------------------------------------------

select pointcall_uhgs_id, pointcall_name 
from navigocheck.check_pointcall 
where pointcall_uhgs_id = 'B2047921' -- ok B2047921	Saint-Domingue générique
-- or pointcall_uhgs_id = 'B2171563'
set role dba
drop function ports.rm_parentheses_crochets (tested_value text)
CREATE OR REPLACE FUNCTION ports.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -3
                        result = None
                else :
                    result = None
                    code = -3
                return [result, code]
            $$ LANGUAGE plpython3u;
           
    
  reset role
  
  GRANT ALL ON FUNCTION ports.extract_substate_fordate(varchar, int) TO dba, postgres;
   GRANT ALL ON FUNCTION ports.extract_state_fordate(varchar, int) TO dba, postgres;
  GRANT ALL ON FUNCTION ports.extract_substate_en_fordate(varchar, int) TO dba, postgres;
  GRANT ALL ON FUNCTION ports.extract_state_en_fordate(varchar, int) TO dba, postgres;

GRANT ALL ON FUNCTION ports.frequency_topo(text, text) TO dba, postgres;
GRANT ALL ON FUNCTION ports.rm_parentheses_crochets(text) TO dba, postgres;

GRANT ALL ON FUNCTION ports.test_double_type(varchar) TO dba, postgres;
GRANT ALL ON FUNCTION ports.test_int_type(varchar) TO dba, postgres;

----------------
-- Import des shipping area

alter table uhgs_et_metarea_fr u rename column "ShippingAreaMainPtcall::E1_name" to shippingarea_name
update geo_general g set shippingarea = shippingarea_name
from uhgs_et_metarea_fr u where g.pointcall_uhgs_id  = u.pointcall_uhgs_id 
-- 108534 en 2min   

-- correction d'un mauvais point dans le fichier geo_general
select * from ports.geo_general where latitude like '%36.987377'
-- James River	B0000959	36.987377	 -76.297632
update 	ports.geo_general set latitude = '36.987377', longitude = '-76.297632' where pointcall_uhgs_id = 'B0000959'
-- « 36.987377 »

----------------

drop table if exists ports.port_points cascade;

create table if not exists ports.port_points as (
            select pointcall_uhgs_id as uhgs_id, latitude::float, longitude::float, null as amiraute, null as province, 
            (array_agg(distinct (ports.rm_parentheses_crochets(pointcall_name)).value))::text[] as toustopos, (array_agg(distinct shippingarea))[1] as shiparea
            from ports.geo_general
            group by pointcall_uhgs_id, latitude, longitude);
-- 1144            
select * from ports.port_points

-- array_agg((navigo.rm_parentheses_crochets(pointcall_name)).value )::text[]
drop table dummay;
create table dummay as (
select    array_agg(trim(latitude)::float) as t 
from ports.geo_general 
group by pointcall_uhgs_id
);
select t from dummay limit 1


SELECT current_user
SET ROLE postgres
SET ROLE dba

reset role

set search_path = ports, public

org.jkiss.dbeaver.model.exec.DBCException: Can't resolve data type _text
	at org.jkiss.dbeaver.ext.postgresql.model.data.PostgreArrayValueHandler.getValueFromObject(PostgreArrayValueHandler.java:63)
	at org.jkiss.dbeaver.ext.postgresql.model.data.PostgreArrayValueHandler.getValueFromObject(PostgreArrayValueHandler.java:1)
	at org.jkiss.dbeaver.model.impl.jdbc.data.handlers.JDBCComplexValueHandler.fetchColumnValue(JDBCComplexValueHandler.java:50)
	at org.jkiss.dbeaver.ext.postgresql.model.data.PostgreArrayValueHandler.fetchColumnValue(PostgreArrayValueHandler.java:52)
	at org.jkiss.dbeaver.model.impl.jdbc.data.handlers.JDBCAbstractValueHandler.fetchValueObject(JDBCAbstractValueHandler.java:49)
	at org.jkiss.dbeaver.ui.controls.resultset.ResultSetDataReceiver.fetchRow(ResultSetDataReceiver.java:125)
	at org.jkiss.dbeaver.ui.editors.sql.execute.SQLQueryJob.fetchQueryData(SQLQueryJob.java:717)
	at org.jkiss.dbeaver.ui.editors.sql.execute.SQLQueryJob.executeStatement(SQLQueryJob.java:540)
	at org.jkiss.dbeaver.ui.editors.sql.execute.SQLQueryJob.lambda$0(SQLQueryJob.java:440)
	at org.jkiss.dbeaver.model.exec.DBExecUtils.tryExecuteRecover(DBExecUtils.java:168)
	at org.jkiss.dbeaver.ui.editors.sql.execute.SQLQueryJob.executeSingleQuery(SQLQueryJob.java:427)
	at org.jkiss.dbeaver.ui.editors.sql.execute.SQLQueryJob.extractData(SQLQueryJob.java:812)
	at org.jkiss.dbeaver.ui.editors.sql.SQLEditor$QueryResultsContainer.readData(SQLEditor.java:3220)
	at org.jkiss.dbeaver.ui.controls.resultset.ResultSetJobDataRead.lambda$0(ResultSetJobDataRead.java:121)
	at org.jkiss.dbeaver.model.exec.DBExecUtils.tryExecuteRecover(DBExecUtils.java:168)
	at org.jkiss.dbeaver.ui.controls.resultset.ResultSetJobDataRead.run(ResultSetJobDataRead.java:119)
	at org.jkiss.dbeaver.ui.controls.resultset.ResultSetViewer$ResultSetDataPumpJob.run(ResultSetViewer.java:4516)
	at org.jkiss.dbeaver.model.runtime.AbstractJob.run(AbstractJob.java:105)
	at org.eclipse.core.internal.jobs.Worker.run(Worker.java:63)

-- alter table port_points alter column toustopos type text[]
-- Marche pas non plus

----------------------------------------------------------------

select uhgs_id, toponyme from ports.port_points where country2019_iso2code is null and point3857 is not null

--A1968875	35 degrés de latitude nord et 45 degrés de longitude du méridien de Paris
--B0000978	Colonies anglaises
--A1963986	Baltique
--A1964713	Mediterranée
--B0000715	Banc de Terre Neuve
--B0000935	Groenland
--C0000006	Côte d' Afrique [de l' Ouest]
--C0000013	Mozambique
--D0000004	Inde


update labels_lang_csv set fr = trim(fr), en=trim(en)

-- Export le 05 février 2021 pour Silvia (vérifications et cartographie)
select uhgs_id, toponyme, toustopos, toponyme_standard_fr , toponyme_standard_en , 
latitude , longitude ,
state_1789_fr, substate_1789_fr , state_1789_en, substate_1789_en , 
amiraute , province ,
partner_balance_1789, partner_balance_1789_uncertainty, partner_balance_supp_1789, partner_balance_supp_1789_uncertainty,
has_a_clerk, status, source_1787_available, source_1789_available, ferme_direction, ferme_bureau, ferme_bureau_uncertainty
from port_points pp 
--where amiraute like '%Olonne%'
order by state_1789_fr, substate_1789_fr , province , amiraute	

-- toustopos ne n'exporte pas bien.
-- Réordonnancement des colonnes pour m'arranger avec ogc_fid,	geonameid, et country2019_name dont se fiche Silvia
select ogc_fid,	geonameid, uhgs_id,	latitude, longitude,	shiparea,	toponyme,	toponyme_standard_fr,	toponyme_standard_en,	toustopos,
country2019_name,	state_1789_fr,	substate_1789_fr,	state_1789_en,	substate_1789_en,	province,	amiraute,
oblique,	has_a_clerk,	source_1787_available,	source_1789_available,	status,
ferme_direction,	ferme_bureau,	ferme_bureau_uncertainty,
partner_balance_1789,	partner_balance_supp_1789,	partner_balance_1789_uncertainty,	partner_balance_supp_1789_uncertainty
from port_points pp 
order by state_1789_fr, substate_1789_fr , province , amiraute	


--- Mettre à jour matching pour calculer les geoname_id manquants

select count(*) from port_points where geonameid is null
-- 143 avant

alter table matching_port add column new_entry boolean;
psql -U postgres -d portic_v6 -f C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\archives_sql\update_matching.sql

-- APRES

select count(*) from matching_port where new_entry is false
-- 285
select * from matching_port where new_entry is false order by uhgs_id 
select count(distinct uhgs_id) from matching_port where new_entry is false 
-- 98
select count(distinct uhgs_id) from matching_port where new_entry is true 
 uhgs_id not in (select distinct uhgs_id from matching_port where new_entry is false) 
select distinct uhgs_id, topo2 from matching_port where new_entry is true order by uhgs_id

select * from matching_port where new_entry is not null and certainity = 2 order by uhgs_id 

select count(distinct uhgs_id) from matching_port where new_entry is not null and certainity = 1 order by uhgs_id 
update ports.port_points p set geonameid = id2
 from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and certainity = 1 and geonameid is null 
 -- 134
 update ports.port_points p set geonameid = id2
 from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and certainity = 2 and geonameid is null 
-- 3
 update ports.port_points p set geonameid = id2
 from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and certainity = 3 and geonameid is null 
 -- 1
 
 select count(*) from port_points where geonameid is null
 select * from port_points where geonameid is null
-- 5

 select * from matching_port where  uhgs_id in ('A1968875', 'A0319905', 'B0000015' , 'B2123882', 'A0116827')
 
 select true, 'geo_general' as source1, pp.ogc_fid as id1, pp.uhgs_id, pp.toponyme,  'geonames' as source2, s.geonameid, s.toponyme , st_distance(s.point3857, pp.point3857 ), similarity(s.toponyme, pp.toponyme)
        from 
        (select ogc_fid, uhgs_id, unnest(toustopos) as toponyme, point3857 from ports.port_points where geonameid is null) as pp,
        (select geonameid, name as toponyme, point3857 from myremote_geonamesplaces ) as s
        where st_distance(s.point3857, pp.point3857 ) < 15000 ;
 
update  port_points set geonameid = 2512993 where uhgs_id = 'A0116827'; -- Rade de Majorque
update  port_points set geonameid = 259255 where uhgs_id = 'A0319905'; -- Koronisía / Golfe de Larta
update  port_points set geonameid = 6066545 where uhgs_id = 'B2123882'; -- Markland / Nouvelle Ecosse (point générique)
update  port_points set geonameid = 4931764 where uhgs_id = 'B0000015'; -- Burnham Rocks / Nouvelle Angleterre (point générique)

select * from port_points where toponyme % 'Larta' or toponyme % 'Nouvelle Ecosse'

 select * from port_points where geonameid is null
-- 1 : ok [{"topo": "35 degrés de latitude nord et 45 degrés de longitude du méridien de Paris", "freq": 1}]

 alter table ports.port_points_old rename to port_points_olddec2020;
 
 -- Sauver les geoname_id et sauver aussi les provinces et amirauté (plus bas)
 -- IMPORTANT pour le programme python
 create table ports.port_points_old as (select * from ports.port_points)
 --- 1144
----------------------------------------------------------------------
-- correction de  labels_lang_csv pour Saint-Domingue
----------------------------------------------------------------------

 insert into labels_lang_csv (label_type, key_id, fr, en) values ('toponyme', 'B2047921', 'Saint-Domingue', 'Saint-Domingue')

---------------------------------------------------------------------
-- ajout d'amirauté et province pour 30 nouveaux ports en France
----------------------------------------------------------------------

select * from obliques where port % 'Gerolata' or port % 'Pino' or port % 'Hersu'
update port_points set amiraute = 'Bastia', province = 'Isles de Corse' where uhgs_id in ('A0154284', 'A0197562', 'A1967002');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (1033415.9024243934545666 5098767.6583220548927784)	A0160563	41.583333	9.283333			Portevecchio en Corse			Portevecchio en Corse	SRID=4326;POINT(9.283333 41.583333)	France	FR	150	36	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
Point (1000020.05518641055095941 5103729.85879848059266806)	A0190294	41.616667	8.983333			Sartet en Corse [Sartène]			Sartet en Corse [Sartène]	SRID=4326;POINT(8.983333 41.616667)	France	FR	150	40	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
Point (1010260.66910295758862048 5082960.4530493151396513)	A1968873	41.477032	 9.075326			Figary en Corse			Figary en Corse	SRID=4326;POINT(9.075326 41.477032)	France	FR	150	117	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

select * from obliques where port % 'Portevecchio' or port % 'Sartet' or port % 'Figary'
update port_points set amiraute = 'Ajaccio', province = 'Isles de Corse' where uhgs_id in ('A0160563', 'A0190294', 'A1968873');

update port_points set amiraute = 'Sables-d’Olonne', province = 'Poitou' where uhgs_id in ('A1968951');


wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-155847.28711058179032989 5812457.49134196620434523)	A0138092	46.2	-1.4						Couarde île de Ré	SRID=4326;POINT(-1.4 46.2)	France	FR	150	34	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
Point (-141004.72544464332167991 5807098.06883828435093164)	A0170633	46.166667	-1.266667			Rivedoux			Rivedoux	SRID=4326;POINT(-1.266667 46.166667)	France	FR	150	38	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
Point (-152136.67452396999578923 5820502.80556370597332716)	A0219226	46.25	-1.366667						Coureaux [de Ré]	SRID=4326;POINT(-1.366667 46.25)	France	FR	150	46	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
Point (-134117.94514671838260256 5787560.22899006772786379)	A1968950	46.044980	 -1.204802						dans le Pertuis	SRID=4326;POINT(-1.204802 46.04498)	France	FR	150	122	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

select * from obliques where port % 'Couarde' or port % 'Rivedoux' or port % 'Coureaux'
update port_points set amiraute = 'La Rochelle', province = 'Aunis' where uhgs_id in ('A0138092', 'A0170633', 'A0219226', 'A1968950');

select distinct amiraute, province from port_points order by amiraute 
where  amiraute % 'Rochelle'

select * from obliques where port % 'Avallon' 
wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-123448.80046458118886221 5739371.42492200806736946)	A1968903	45.74369849847762	 -1.1089594426355542			Avallon			Avallon	SRID=4326;POINT(-1.10895944263555 45.7436984984776)	France	FR	150	121	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

update port_points set amiraute = 'Marennes', province = 'Saintonge' where uhgs_id in ('A1968903');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-77923.64355529232125264 5637278.20189052727073431)	A0138162	45.1	-0.7			La Marque			La Marque	SRID=4326;POINT(-0.7 45.1)	France	FR	150	35	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

select * from obliques where port % 'Marque' 
update port_points set amiraute = 'Bordeaux', province = 'Guyenne' where uhgs_id in ('A0138162');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (686470.23033168388064951 5317046.84646210540086031)	A0125625	43.033333	6.166667			Pradeau près des Iles d' Hieres			Pradeau près des Iles d' Hieres	SRID=4326;POINT(6.166667 43.033333)	France	FR	150	32	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
Point (677193.53188591753132641 5317046.84646210540086031)	A0199337	43.033333	6.083333			Ile Longue			Ile Longue	SRID=4326;POINT(6.083333 43.033333)	France	FR	150	43	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

select * from obliques where port % 'Pradeau' 
update port_points set amiraute = 'Toulon', province = 'Provence' where uhgs_id in ('A0125625', 'A0199337');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (632665.73556860804092139 5337374.76900143828243017)	A0176357	43.166667	5.683333			Lecques			Lecques	SRID=4326;POINT(5.683333 43.166667)	France	FR	150	39	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

select * from obliques where port % 'Lecques' 
update port_points set amiraute = 'La Ciotat', province = 'Provence' where uhgs_id in ('A0176357');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-380765.75791010330431163 6052066.15349983423948288)	A1968952	47.669619	 -3.420477			Dans les Couraux			Dans les Couraux	SRID=4326;POINT(-3.420477 47.669619)	France	FR	150	1130	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

update port_points set amiraute = 'Vannes', province = 'Bretagne' where uhgs_id in ('A1968952');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-224920.80850882892264053 6356880.13600314687937498)	A1968879	49.481032	 -2.020498						Le long des côtes [de Normandie]	SRID=4326;POINT(-2.020498 49.481032)	France	FR	150	113	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0

update port_points set amiraute = 'Cherbourg', province = 'Normandie' where uhgs_id in ('A1968879');


wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-441567.35058648308040574 6221299.44955814909189939)	A0137359	48.683333	-3.966667			Penpoul			Penpoul	SRID=4326;POINT(-3.966667 48.683333)	France	FR	150	33	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
select * from obliques where port % 'Penpoul' 
update port_points set amiraute = 'Brest', province = 'Bretagne' where uhgs_id in ('A0137359');

wkt_geom	uhgs_id	latitude	longitude	amiraute	province	toustopos	shiparea	topofreq	toponyme	geom	country2019_name	country2019_iso2code	country2019_region	ogc_fid	belonging_states	belonging_substates	belonging_states_en	belonging_substates_en	state_1789_fr	substate_1789_fr	state_1789_en	substate_1789_en	oblique	has_a_clerk	source_1787_available	source_1789_available	status	geonameid	toponyme_standard_fr	toponyme_standard_en	relation_state	ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty
Point (-50093.77085697282018373 5777678.72398185543715954)	A0122883	45.983333	-0.45						Saint André de Cuzac	SRID=4326;POINT(-0.45 45.983333)	France	FR	150	30	[{"1749-1815" : "France"}]		[{"1749-1815" : "France"}]		France		France		false	false	false	false		0			{"relationType" : "gvp:broaderPartitive", "relationTo" : "http://www.geonames.org/3017382", "label" : "France", "when" : {"timespans" : [{"start" : {"in" : "*"}, "end" : {"in" : "*"}}]}}			0		France	0	0
select * from obliques where port % 'Cus' 

update port_points set amiraute = 'La Rochelle', province = 'Aunis' where uhgs_id in ('A0122883');

-- diagnostique des shiparea manquants
select amiraute, array_agg(distinct shiparea) from  port_points group by amiraute;

select shiparea, toponyme from port_points where amiraute = 'Ajaccio' or amiraute='Bastia'
and MED-CORS,MED-TYNO,}

update port_points set shiparea = 'MED-TYNO' where toponyme in ('Portevecchio en Corse')
update port_points set shiparea = 'MED-CORS' where toponyme in ('Hersu en Corse [Ersa]', 'Figary en Corse', 'Pino en Corse', 'Gerolata en Corse', 'Sartet en Corse [Sartène]', 'Cap Corse', 'Roquepine en Corse', 'Baretaly en Corse [Barrettali]')


select shiparea, toponyme from port_points where amiraute = 'La Rochelle'
update port_points set shiparea = 'ACE-ROCH' where amiraute = 'La Rochelle' and shiparea is null

select shiparea, toponyme, amiraute, province from port_points where amiraute % 'Olonne'
update port_points set shiparea = 'ACE-YEUX' where amiraute % 'Olonne' and shiparea is null

select shiparea, toponyme from port_points where amiraute = 'Marennes'
update port_points set shiparea = 'ACE-ROCH' where amiraute % 'Marennes' and shiparea is null

select shiparea, toponyme from port_points where amiraute = 'Bordeaux'
update port_points set shiparea = 'ACE-ROCH' where amiraute % 'Bordeaux' and shiparea is null

update port_points set shiparea = 'MED-LIGS' where amiraute % 'Antibes' and shiparea is null;
update port_points set shiparea = 'MAN-WIGH' where amiraute % 'Bayeux' and shiparea is null;
update port_points set shiparea = 'MED-LIGS' where amiraute % 'Toulon' and shiparea is null;
update port_points set shiparea = 'ACE-IROI' where amiraute % 'Vannes' and shiparea is null;
update port_points set shiparea = 'MED-LIGS' where amiraute % 'Saint-Tropez' and shiparea is null;
update port_points set shiparea = 'MAN-PORT' where amiraute % 'Saint-Malo' and shiparea is null;
update port_points set shiparea = 'MAN-PORT' where amiraute % 'Saint-Brieuc' and shiparea is null;
update port_points set shiparea = 'MED-LIGS' where amiraute % 'Fréjus' and shiparea is null;
update port_points set shiparea = 'MED-LIGS' where amiraute % 'La Ciotat' and shiparea is null;
update port_points set shiparea = 'MAN-PLYM' where amiraute % 'Brest' and shiparea is null;
update port_points set shiparea = 'MAN-WIGH' where amiraute % 'Caen' and shiparea is null;
update port_points set shiparea = 'MAN-PORT' where amiraute % 'Cherbourg' and shiparea is null;
update port_points set shiparea = 'MAN-PORT' where amiraute % 'Cherbourg' and shiparea is null;

select shiparea, toponyme from port_points where amiraute = 'Quimper'
update port_points set shiparea = 'ACE-IROI' where amiraute % 'Quimper' 
 and ( toponyme % 'Le Passage Saint Jean' or  toponyme % 'Dinan' or toponyme % 'Benaudet [Benodet]')

-- Report des corrections dans geo_genéral
update geo_general g
set shippingarea = shiparea from port_points p where p.uhgs_id = g.pointcall_uhgs_id and g.shippingarea is null
-- 3600
update geo_general g
set shippingarea = shiparea from port_points p where  toponyme = 'Dinan' and shippingarea % 'MAN';

select distinct pointcall_uhgs_id , pointcall_name, shippingarea from geo_general p where  pointcall_name = 'Dinan'

select distinct uhgs_id , toponyme, amiraute, province  
from port_points p
where uhgs_id = 'B1965595'

select uhgs_id, * from port_points p where toponyme = 'Dinan'

select distinct uhgs_id , toponyme, amiraute, province  , geonameid
from ports.port_points p
where uhgs_id = 'B2059102'

update ports.port_points set partner_balance_supp_1789='Sénégal et Guinée', partner_balance_supp_1789_uncertainty=-1
            where uhgs_id in (select uhgs_id from ports.port_points where country2019_region = '2'
            and country2019_name in ('Senegal', 'Equatorial Guinea' , 'Gambia', 'Nigeria')) or uhgs_id in ('C0000006', 'C0000013')
            
            update ports.port_points set partner_balance_1789='Sénégal', partner_balance_1789_uncertainty=-1
        where uhgs_id in (select uhgs_id from ports.port_points where country2019_name = 'Senegal')
            
----------
-- Corrections au vu du fichier Excel de Silvia (exceptions)
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\verifier_ports_7fevrier%20en%20cours%20silvia.xlsx      
--        # Saint Pierre et Miquelon/ B1965595
--        # Cayenne/B2009606
update ports.port_points pp set oblique = true, has_a_clerk=true where pp.uhgs_id in ('B1965595', 'B2009606');
update ports.port_points pp set source_1787_available = true , source_1789_available = true where pp.uhgs_id in ('B1965595', 'B2009606');

update ports.port_points_old pp set oblique = true, has_a_clerk=true where pp.uhgs_id in ('B1965595', 'B2009606');
update ports.port_points_old pp set source_1787_available = true , source_1789_available = true where pp.uhgs_id in ('B1965595', 'B2009606');

---- Etats mal orthographiés

-- A0339882 : Largentiere : pas Grèece mais Empire ottoman
-- retrait du code dans Python spécial pour Largentiere (Grece)
select geonameid from ports.port_points_old ppo where uhgs_id = 'A0339882' -- 8740243 / Ágios Nikolaos
update ports.port_points_old set geonameid='9847649' where uhgs_id = 'A0339882' -- 9847649 / Kimolos Port
select * from ports.matching_port mp where uhgs_id = 'A0339882'

-- A0407958 : Whitby / Grande-Bretagne - Angleterre
-- A1932112 : Seyde / Empire ottoman
-- A1064986 : Geffle / Suède 

select * from ports.etats where uhgs_id in ('A0339882' , 'A0407958', 'A1932112', 'A1064986');
update ports.etats set etat = 'Empire ottoman', etat_en = 'Ottoman Empire' where uhgs_id in ('A0339882','A1932112') ;
update ports.etats set etat = 'Grande-Bretagne', etat_en = 'Great Britain', subunit='Angleterre' , subunit_en = 'England' where uhgs_id = 'A0407958';
update ports.etats set etat = 'Sweden', etat_en = 'Sweden' where uhgs_id = 'A1064986';

update ports.etats set etat = 'Etats-Unis d''Amérique', subunit='Virginie', etat_en='United States', subunit_en = 'Virginia' where uhgs_id = 'B0000959';

update ports.labels_lang_csv  c set fr = t.fr, en=t.en
from ports.labels_lang_toponyme6fev21_csv t where c.key_id = t.pointcall_uhgs_id;
-- 6
set search_path = ports, public 

insert into ports.labels_lang_csv (label_type, key_id, fr, en, remarques)
select label_type, pointcall_uhgs_id, fr, en, remarques 
from ports.labels_lang_toponyme6fev21_csv t 
where pointcall_uhgs_id not in (select key_id from ports.labels_lang_csv where label_type = 'toponyme')
-- 81

select * from ports.labels_lang_csv where label_type = 'toponyme' and key_id in 
(select key_id 
from ports.labels_lang_csv
where label_type = 'toponyme'
group by key_id
having count(key_id) > 1)
order by key_id 

/*
toponyme	A0248371	Corigliano Calabro	Corigliano Calabro
toponyme	A0248371	Corigliano Calabro	Corigliano Calabro
toponyme	A0407958	Whitby	Whitby
toponyme	A0407958	Whitby	Whitby
toponyme	A1064986	Gèfle 	Gävle
toponyme	A1064986	Gävle	Gävle
toponyme	A1932112	Seyde [Sidon]	Sidon
*/
delete from ports.labels_lang_csv where label_type = 'toponyme' and key_id = 'A1064986' and fr = 'Gävle'
toponyme	A1932112	Seyde [Sidon]	Sidon

-- Non pris en compte
dto = 1776
A1010107	Riga	Riga		Russie		Russian Empire		
A1964109	Russie	Russia		Russie		Russian Empire		
A0725419	Wolgast	Wolgast	Suède	Poméranie suédoise	Sweden	Swedish Pomerania	
A0756074	Barth	Barth	Suède	Poméranie suédoise	Sweden	Swedish Pomerania	
A0251477	['Bozza en Sardaigne [Bosa]']	Italy					
A0251618	['Saint Antoine de Carrara [Carrara, undertermined]']	Italy					



dfrom = 1814
A1039332	Landscrone	Landskrona			Suède		Sweden
dfrom = 1812
A0235841	Porteparos en Sicile					



-----------------------------------------------------------------
-- Corrections le 10 février à partir fichier vérifié par Silvia le 6 février
-- AppartenancesEtats et toponymes
-- fichier file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\fix_silvia06022021.csv
-- et file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\verifier_ports_6fevrier_traité Silvia.xlsx
----------------------------------------------------------------

-- Toponymes


select uhgs_id, toponyme, toponyme_standard_en , toponyme_standard_fr from ports.port_points pp where uhgs_id in ('A0232633', 'A0232521') 

update ports.labels_lang_csv set fr='Gaète', en='Gaeta' where key_id ='A0232633' and label_type = 'toponyme'

select * from labels_lang_csv where label_type = 'toponyme' and en like 'Gd%'
select * from labels_lang_csv where label_type = 'toponyme' and key_id in ('A0880659', 'A0886595', 'A1195707', 'A1204354', 'A0199337', 'A1470521')
select * from labels_lang_csv where label_type = 'toponyme' and en like '%?%'
A1204354	Dantzig 	Gda?sk
A1463421	Rodosto / Tekirda?	Tekirda?
A1465990	Alexandrette	?skenderun 

ń

--- importer les traductions des autres ports
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\labels_lang_toponyme09fev21.csv

select * from ports.labels_lang_csv l1 , ports.labels_lang_toponyme09fev21_csv l2 
where l1.key_id = l2.pointcall_uhgs_id and l1.label_type = 'toponyme' and l2.label_type = 'toponyme'
--toponyme	A0036401	San Pawl il-Baħar	St Paul's Bay
--toponyme	A1463421	Rodosto / Tekirdağ	Tekirdağ
--toponyme	A1465990	Alexandrette	İskenderun
--toponyme	A0135801	Saint-Pol-de-Léon	Saint-Pol-de-Léon
--toponyme	A0232633	Gaète	Gaeta
--toponyme	A1204354	Dantzig 	Gdańsk

Rodosto / Tekirdağ
update ports.labels_lang_csv set fr = 'Rodosto / Tekirdağ', en = 'Tekirdağ' where key_id='A1463421' and label_type = 'toponyme';
update ports.labels_lang_csv set fr = 'San Pawl il-Baħar' where key_id='A0036401' and label_type = 'toponyme';
update ports.labels_lang_csv set en = 'Gdańsk' where key_id='A1204354' and label_type = 'toponyme';
update ports.labels_lang_csv set en = 'İskenderun' where key_id='A1465990' and label_type = 'toponyme';
update ports.labels_lang_csv set fr = 'Saint-Pol-de-Léon', en = 'Saint-Pol-de-Léon' where key_id='A0135801' and label_type = 'toponyme';

insert into ports.labels_lang_csv (label_type, key_id, fr, en, remarques) (
	select label_type, pointcall_uhgs_id as key_id, fr, en, remarques from ports.labels_lang_toponyme09fev21_csv l2 
	where pointcall_uhgs_id not in (select key_id from labels_lang_csv where label_type = 'toponyme')
)
-- 64

select * from ports.labels_lang_csv


select * from navigoviz.source where main_port_toponyme like 'Marennes' or main_port_toponyme like 'Nantes'
A0136930	Marennes
A0124817	Nantes

select distinct source_main_port_toponyme from navigoviz.pointcall p where source_main_port_uhgs_id in ('A0136930', 'A0124817')


-- AppartenancesEtat
select * from ports.etats e where uhgs_id in ('A0238683')
			
update ports.etats set etat='Royaume de Naples',  subunit='Etat des Présides' 
where dto = 1801 and uhgs_id = 'A0238683'*

select * from ports.etats e 
where uhgs_id in ('A0090505', 'A1968873', 'A0160563', 'A0190294', 'A0154284', 'A0197562', 'A1967002', 'A0090505', 'A0249463', 'A1813720', 'B1969741', 'B2046102', 'B2826894', 'B2827324', 'B0000959', 'B0000969')

select * from ports.etats e where uhgs_id in ('A1039332')
update ports.etats set dto=1814 where uhgs_id in ('A1039332')

select * from ports.etats e where uhgs_id in ('B0000056')
-- ok
select * from ports.etats e where uhgs_id in ('A1813720')
-- ok
select * from ports.etats e where uhgs_id in ('B0000056')
-- Louisiana : ok
B0000959
B0000969

select * from ports.etats e 
where uhgs_id in ('A0090505', 'A1968873', 'A0160563', 'A0190294', 'A0154284', 'A0197562', 'A1967002', 'A0090505', 'A0249463', 'A1813720', 'B1969741', 'B2046102', 'B2826894', 'B2827324', 'B0000959', 'B0000969')

select * from ports.etats e 
where uhgs_id in ('A0190294', 'A1968873', 'A0160563', 'A0154284', 'A0197562', 'A1967002')




--- A1702257 Tripoli de Barbarie	Tripoli (Lybia) Empire ottoman	Régence de Tripoli	Ottoman empire	Regency of Tripoli
select * from ports.etats e where uhgs_id = 'A1702257'
update ports.etats set subunit='Régence de Tripoli', subunit_en = 'Regency of Tripoli' where uhgs_id = 'A1702257';
-- A1708033 Benghazi	Benghazi	Empire ottoman	Régence de Tripoli	Ottoman Empire	Regency of Tripoli
select * from ports.etats e where uhgs_id = 'A1708033'
update ports.etats set subunit_en = 'Regency of Tripoli' where uhgs_id = 'A1708033';

-- A1945818	Le Gouffre	Le Gouffre	Grande-Bretagne	Iles anglo-normandes	Great Britain	Channel Islands
-- A1945598	Guernesey	Guernesey	Grande-Bretagne	Iles anglo-normandes	Great Britain	Channel Islands
-- A1963330	Jersey	Jersey	Grande-Bretagne	Iles anglo-normandes	Great Britain	Channel Islands

select * from ports.etats e where uhgs_id = 'A1945818'
update ports.etats set subunit = 'Iles anglo-normandes', subunit_en = 'Channel Islands' where uhgs_id in ('A1945818', 'A1945598', 'A1963330');


-- A0399213	Neath	Neath	Grande-Bretagne	Pays de Galles	Great Britain	Wales
select * from ports.etats e where uhgs_id = 'A0399213'
update ports.etats set subunit = 'Pays de Galles', subunit_en = 'Wales' where uhgs_id in ('A0399213');

update ports.etats set subunit_en = 'Massachusetts' 
where uhgs_id in ('B0000043', 'B0000717', 'B0000008', 'B0000048', 'B0000062', 'B0000939', 'B2827325') and dfrom=1783
update ports.etats set subunit_en = 'British colonies in America' 
where uhgs_id in ('B0000043', 'B0000717', 'B0000008', 'B0000048', 'B0000062', 'B0000939', 'B2827325') and dto=1783

select * from  ports.etats where uhgs_id in ('B0000043', 'B0000717', 'B0000008', 'B0000048', 'B0000062', 'B0000939', 'B2827325')







-- A0354005 Gibraltar	Gibraltar	Grande-Bretagne	possessions anglaises en Méditerranée	Great Britain	possessions anglaises en Méditerranée
-- proposition Christine : British possessions in the Mediterranean
select * from ports.etats e where uhgs_id = 'A0354005'
update ports.etats set subunit = 'possessions anglaises en Méditerranée', subunit_en = 'British possessions in the Mediterranean' where uhgs_id in ('A0354005');

-- A0089345	île de Man	Isle of Man	Grande-Bretagne	Angleterre	Great Britain	England
select * from ports.etats e where uhgs_id = 'A0089345'
update ports.etats set subunit = 'Angleterre', subunit_en = 'England' where uhgs_id in ('A0089345');

A0592945	Sligo 	Sligo 	Grande-Bretagne	Irlande	Great Britain	Ireland
A0592950	Limerick 	Limerick 	Grande-Bretagne	Irlande	Great Britain	Ireland
A0593349	Drogheda 	Drogheda 	Grande-Bretagne	Irlande	Great Britain	Ireland
A0594881	Dingle 	Dingle 	Grande-Bretagne	Irlande	Great Britain	Ireland
A0595327	Ross	Ross	Grande-Bretagne	Irlande	Great Britain	Ireland
A0596366	Rush	Rush	Grande-Bretagne	Irlande	Great Britain	Ireland
A0599268	Waterford	Waterford	Grande-Bretagne	Irlande	Great Britain	Ireland
A0596535	Galway	Galway	Grande-Bretagne	Irlande	Great Britain	Ireland
A0605799	Irlande	Irland	Grande-Bretagne	Irlande	Great Britain	Ireland
A0605259	Cork	Cork	Grande-Bretagne	Irlande	Great Britain	Ireland
A0614666	Dublin	Dublin	Grande-Bretagne	Irlande	Great Britain	Ireland
A0614744	Kinsale	Kinsale	Grande-Bretagne	Irlande	Great Britain	Ireland

select * from ports.etats e 
where uhgs_id in ('A0592945', 'A0592950', 'A0593349', 'A0594881', 'A0595327', 'A0596366', 'A0599268', 'A0596535', 'A0605799', 'A0605259', 'A0614666', 'A0614744')

update ports.etats set subunit = 'Irlande', subunit_en = 'Ireland' 
where uhgs_id in ('A0592945', 'A0592950', 'A0593349', 'A0594881', 'A0595327', 'A0596366', 'A0599268', 'A0596535', 'A0605799', 'A0605259', 'A0614666', 'A0614744');

-- A0374720	Madère	Madeira	Portugal	îles atlantiques portugaises	Portugal	Portuguese Atlantic Islands
select * from ports.etats e where uhgs_id = 'A0374720'
update ports.etats set subunit = 'îles atlantiques portugaises' where uhgs_id in ('A0374720');
select * from ports.etats e where subunit like '%atlantiques portugaise%'

-- A0249937	Lampeduse	Lampedusa	principauté de Lampédouse		principality of Lampedusa
select * from ports.etats e where uhgs_id = 'A0249937'
update ports.etats set  etat_en = 'principality of Lampedusa' where uhgs_id in ('A0249937');

-- A0635679	Zélande	Zealand	Provinces-Unies	Zélande	United Provinces	Zeeland
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id = 'A0635679'
update ports.etats set  subunit = 'Zélande', subunit_en='Zeeland' where uhgs_id in ('A0635679') and dto = 1794;

-- A0752869	Emden	Emden	Prusse	Frise orientale	Prussia	East Frisia
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id = 'A0752869'
update ports.etats set  subunit = 'Frise orientale', subunit_en='East Frisia' where uhgs_id in ('A0752869') and dfrom=1744 and dto = 1806;

-- A0242964	Marsala	Marsala	Royaume de Naples	Sicile	Kingdom of Naples	Sicily
-- A0235237	Messine	Messina	Royaume de Naples	Sicile	Kingdom of Naples	Sicily
-- A0240815	Ustica	Ustica	Royaume de Naples	Sicile	Kingdom of Naples	Sicily

select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id in ('A0242964', 'A0235237', 'A0240815')
update ports.etats set  subunit = 'Sicile', subunit_en='Sicily' where uhgs_id in ('A0242964', 'A0235237', 'A0240815') ;

-- A0232633	Gaète	Gaeta	Etats pontificaux		Papal States	
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id in ('A0232633')
update ports.etats set  etat = 'Etats pontificaux', etat_en = 'Papal States' where uhgs_id in ('A0232633');


-- A0238683	Longone 	Porto Longone	Royaume de Naples	Etat des Présides	Kingdom of Naples	State of the Presidi	1801
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id in ('A0238683')
-- ok

--A0918138	Krogars	Krogars	Suède	Finlande	Sweden	Finland
--A0941547	Kaskö / Kaskinen	Kaskinen 	Suède	Finlande	Sweden	Finland
--A0932725	Abo / Turku	Turku 	Suède	Finlande	Sweden	Finland
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id in ('A0918138', 'A0941547', 'A0932725')
update ports.etats set  subunit = 'Finlande', subunit_en='Finland' where uhgs_id in ('A0918138', 'A0941547', 'A0932725') ;


-- A1039332	Landscrone	Landskrona	Suède		Sweden		1814
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id in ('A1039332')
-- ok
-- A1064986	Gèfle 	Gävle	Suède		Sweden
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto  from ports.etats e where uhgs_id in ('A1064986')
update ports.etats set  etat = 'Suède' where uhgs_id in ('A1064986');

-- Fin par lot csv

select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto
from ports.etats where uhgs_id in (select trim(uhgs_id) from ports.fix_silvia06022021_csv)
-- 0
select * from ports.etats e where uhgs_id = 'A0012229'
--- ce ne sont que des nouvelles insertions à faire

update ports.etats e set etat=state_1789_fr, etat_en =substate_1789_en, subunit = substate_1789_fr, subunit_en = substate_1789_en
from ports.fix_silvia06022021_csv fsc 
where  e.uhgs_id = fsc.uhgs_id 
-- 0

select uhgs_id , state_1789_fr,	state_1789_en,	substate_1789_fr,	substate_1789_en
from ports.fix_silvia06022021_csv fsc 

insert into ports.etats (uhgs_id, etat, etat_en, subunit, subunit_en )
select uhgs_id , state_1789_fr,	state_1789_en,	substate_1789_fr,	substate_1789_en
from ports.fix_silvia06022021_csv fsc ;
-- 108

select * from ports.labels_lang_csv llc where llc.key_id in (select uhgs_id from ports.fix_silvia06022021_csv ) and label_type = 'toponyme'
update ports.etats e set toponyme_standard_fr = fr, toponyme = fr 
from ports.labels_lang_csv llc where llc.key_id=e.uhgs_id and label_type = 'toponyme'
-- 1141 (dommage, j'ai perdu le toponyme mal orthographié de Geo_Général)


-- B0000959 Etats-Unis d'Amérique	Virginie	United States	Virginia		1776
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto
from ports.etats where uhgs_id in ('B0000959', 'B0000969', 'A0090505', 'A0249463', 'A1813720', 'B1969741', 'B2046102', 'B2826894', 'B2827324', 'C0237838')

-- Corrections manuelle de Christine
select * from ports.etats e where uhgs_id = 'A0642340';
update ports.etats e set etat_en = 'Prussia' where uhgs_id = 'A0642340'

-- Fin par lot 2 csv
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto
from ports.etats where uhgs_id in (select trim(uhgs_id) from ports.fix_silvia06022021_lot2_csv)
-- 0
--- ce ne sont que des nouvelles insertions à faire

insert into ports.etats (uhgs_id, toponyme,  etat, etat_en, subunit, subunit_en, dfrom, dto )
select uhgs_id ,toponyme_standard_fr, state_1789_fr,	state_1789_en,	substate_1789_fr,	substate_1789_en, dfrom, dto
from ports.fix_silvia06022021_lot2_csv fsc ;
-- correction manuelle des dates de Jacmel (Saint-Domingue) et appartenance à une province 
select * from  ports.labels_lang_csv llc where llc.key_id='A0190294' and label_type = 'toponyme'

select uhgs_id, toponyme , province, state_1789_en from  ports.port_points where uhgs_id = 'B2046102'
update ports.port_points set province='Saint-Domingue' where uhgs_id = 'B2046102';


select amiraute, province, state_1789_fr from ports.port_points pp where uhgs_id in 
('A0190294', 'A1968873', 'A0160563', 'A0154284', 'A0197562', 'A1967002')

-- Fin par lot 3 csv
select uhgs_id , toponyme , etat, etat_en, subunit, subunit_en, dfrom, dto, geonameid, * 
from ports.etats where uhgs_id in (select trim(uhgs) from ports.fix_silvia06022021_lot3_csv)
-- 15 mise à jour
-- supprimer ces entrées, et les réinsérer
delete from ports.etats where uhgs_id in (select trim(uhgs) from ports.fix_silvia06022021_lot3_csv)
-- 15
insert into ports.etats (uhgs_id, toponyme,  etat, etat_en, subunit, subunit_en, dfrom, dto )
select uhgs ,toponyme_standard_fr, state__fr,	state_en,	substate_fr,	substate_en, dfrom, dto
from ports.fix_silvia06022021_lot3_csv fsc ;

select * from etats where uhgs_id='A0079365'
update etats set subunit='Canaries', subunit_en = 'Canary Islands' where uhgs_id='A0079365'
-- select * from etats where uhgs_id='A0096602'
--A0096602	28.469167	-16.256776		Sainte Croix de Tanariffes	Santa Cruz de Tenerife
--A0084348	28.140838	-15.425241	ACE-CANA	Lux Isle de la grande Canarie	Puerto de la Luz


select distinct subunit from etats where subunit like 'Régence%'

select distinct etat from etats where subunit like 'Régence%'



------------------------------------------------------------------------------------------------------------------------
-- Les inclusions géographiques
------------------------------------------------------------------------------------------------------------------------

-- import d'un fichier fait à la main reprenant en partie les inclusions géo détectable par carte
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\rivers\Generiques_inclusions_geo.csv

-- complétion avec les règles
alter table ports.generiques_inclusions_geo_csv drop column column7 ;
alter table ports.generiques_inclusions_geo_csv drop column column8 ;

select * from ports.generiques_inclusions_geo_csv gigc 

select distinct state_1789_fr, substate_1789_fr from port_points pp 
order by state_1789_fr, substate_1789_fr
--Autriche	Autriche méditerranéenne
--Autriche	Pays-Bas autrichiens
--Brême	
--Chine	
--Danemark	colonies danoises
--Danemark	Norvège
--Danemark	
--Duché d'Oldenbourg	
--Duché de Courlande	
--Duché de Massa et Carrare	
--Duché de Mecklenbourg	
--Empire du Maroc	
--Empire ottoman	Iles anglo-normandes
--Empire ottoman	Régence d'Alger
--Empire ottoman	Régence de Tunis
--Empire ottoman	Tripoli
--Empire ottoman	
--Espagne	Canaries
--Espagne	colonies espagnoles d'Amérique
--Espagne	
--Etats-Unis d'Amérique	Caroline du Nord
--Etats-Unis d'Amérique	Caroline du Sud
--Etats-Unis d'Amérique	Géorgie
--Etats-Unis d'Amérique	Maryland
--Etats-Unis d'Amérique	Massachussets
--Etats-Unis d'Amérique	Massachussets [Maine]
--Etats-Unis d'Amérique	New Hampshire
--Etats-Unis d'Amérique	New York
--Etats-Unis d'Amérique	Pennsylvanie
--Etats-Unis d'Amérique	Rhode Island
--Etats-Unis d'Amérique	Virginie
--Etats-Unis d'Amérique	
--Etats pontificaux	
--France	colonies françaises d'Amérique
--France	colonies françaises en Afrique
--France	colonies françaises en Amérique
--France	colonies françaises en Asie
--France	
--Grande-Bretagne	Angleterre
--Grande-Bretagne	colonies britanniques d'Amérique
--Grande-Bretagne	colonies britanniques en Asie
--Grande-Bretagne	Ecosse
--Grande-Bretagne	Iles anglo-normandes
--Grande-Bretagne	Irlande
--Grande-Bretagne	Pays de Galles
--Grande-Bretagne	possessions anglaises en Méditerranée
--Grande-Bretagne	
--Hambourg	
--Islande	colonies danoises
--Lubeck	
--Malte	
--Mecklenbourg	
--Monaco	
--multi-Etat	
--Pologne	
--Portugal	colonies portugaises d'Afrique
--Portugal	colonies portugaises d'Amérique
--Portugal	îles atlantiques portugaises
--Portugal	
--principauté de Lampédouse	
--Principauté de Piombino	
--Provinces-Unies	colonies hollandaises en Afrique
--Provinces-Unies	Frise
--Provinces-Unies	Groningue
--Provinces-Unies	Gueldre
--Provinces-Unies	Hollande
--Provinces-Unies	Pays de la Généralité
--Provinces-Unies	Zélande
--Prusse	Frise orientale
--Prusse	
--République de Gênes	
--République de Lucques	
--République de Raguse	
--République de Venise	
--Royaume de Bonny	
--Royaume de Naples	Etat des Présides
--Royaume de Naples	Sicile
--Royaume de Naples	
--Royaume de Piémont-Sardaigne	Sardaigne
--Royaume de Piémont-Sardaigne	
--Russie	Russie - mer Baltique
--Russie	Russie - mer Blanche
--Russie	Russie - mer Noire
--Russie	
--Suède	Finlande
--Suède	Poméranie suédoise
--Suède	
--Toscane	
--zone maritime	


select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Corse	A0156739	A0156739	Corse	province='Isles de Corse'	0

select uhgs_id , toponyme_standard_fr from port_points pp where province = 'Isles de Corse' and uhgs_id != 'A0156739'

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Corse', 'A0156739', uhgs_id , toponyme_standard_fr, 'province=''Isles de Corse''' , 1
from port_points pp where province = 'Isles de Corse' and uhgs_id != 'A0156739';
-- 20
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0156739' and ughs_id_sup = 'A0156739'

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Angleterre	A0390929	A0390929	Angleterre	subunit = Angleterre	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Angleterre', 'A0390929', uhgs_id , toponyme_standard_fr, 'subunit = Angleterre' , 1
from port_points pp where substate_1789_fr = 'Angleterre' and uhgs_id != 'A0390929';
-- 90
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0390929' and ughs_id_sup = 'A0390929';

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- 	Angleterre (destination simulée pour) A0394917	A0394917	Angleterre (destination simulée pour)	subunit = Angleterre	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Angleterre', 'A0390929', 'A0394917' , 'Angleterre (destination simulée pour)', 'subunit = Angleterre' , 1
-- 1
-- delete from ports.generiques_inclusions_geo_csv where ughs_id = 'A0394917' and ughs_id_sup ='A0394917'

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Sicile	A0220588	A0220588	Sicile	subunit = Sicile	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile', 'A0220588', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0220588';
--29
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0220588' and ughs_id_sup = 'A0220588';

	
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile, underterminé', 'A0219724', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' ;
-- 29

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Russie	A1964109	A1964109	Russie	etat=Russie	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Russie', 'A1964109', uhgs_id , toponyme_standard_fr, 'etat = Russie' , 1
from port_points pp where state_1789_fr = 'Russie' and uhgs_id != 'A1964109';
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1964109' and ughs_id_sup = 'A1964109';
-- 7 + 1

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Suède	A1081238	A1081238	Suède	etat=Suède	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Suède', 'A1081238', uhgs_id , toponyme_standard_fr, 'etat = Suède' , 1
from port_points pp where state_1789_fr = 'Suède' and uhgs_id != 'A1081238';
-- 28
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1081238' and ughs_id_sup = 'A1081238';

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Toscane	A0219759	A0219759	Toscane	etat=Toscane	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Toscane', 'A0219759', uhgs_id , toponyme_standard_fr, 'etat = Toscane' , 1
from port_points pp where state_1789_fr = 'Toscane' and uhgs_id != 'A0219759';
-- 12
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0219759' and ughs_id_sup = 'A0219759';

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Etats Pontificaux	A1968753	A1968753	Etats Pontificaux	etat =Etats Pontificaux	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Etats pontificaux', 'A1968753', uhgs_id , toponyme_standard_fr, 'etat = Etats pontificaux' , 1
from port_points pp where state_1789_fr = 'Etats pontificaux' and uhgs_id != 'A1968753';
-- 13
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1968753' and ughs_id_sup = 'A1968753';


select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Nouvelle-Angleterre	B0000015	B0000015	Nouvelle-Angleterre	subunit in ('Maine', 'Massachusetts', 'New Hampshire','Rhode Island' )	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Nouvelle-Angleterre', 'B0000015', uhgs_id , toponyme_standard_fr, 'subunit_en in (Maine, Massachusetts, New Hampshire,Rhode Island)' , 1
from port_points pp where substate_1789_en in ('Maine', 'Massachusetts','New Hampshire','Rhode Island') and uhgs_id != 'B0000015';
-- 15
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'B0000015' and ughs_id_sup = 'B0000015';

select '['||substate_1789_fr||']' , '['||substate_1789_en||']' from port_points pp where substate_1789_en in ('Maine','Massachusetts','New Hampshire','Rhode Island')

select '['||substate_1789_fr||']' , '['||substate_1789_en||']', substate_1789_en from port_points pp where substate_1789_fr like '%Massachussets%'
--- 
update etats set subunit='Massachussets' where uhgs_id ='B2827324' and dfrom = 1776;
update port_points set substate_1789_fr='Massachussets' where uhgs_id ='B2827324' ;
update etats set subunit_en='Massachusetts' where subunit_en='Massuchussets' and dfrom = 1776;
update port_points set substate_1789_en='Massachusetts' where substate_1789_en ='Massuchussets' ;

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- à l'aventure	H6666666	H6666666	à l'aventure	etat='France'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'à l''aventure', 'H6666666', uhgs_id , toponyme_standard_fr, 'etat = France and subunit is null' , 1
from port_points pp where state_1789_fr = 'France' and substate_1789_fr is null and uhgs_id != 'H6666666';
-- 347
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'H6666666' and ughs_id_sup = 'H6666666';

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Portugal	A1965063	A1965063	Portugal	etat='Portugal' and subunit is null	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Portugal', 'A1965063', uhgs_id , toponyme_standard_fr, 'etat = Portugal and subunit is null' , 1
from port_points pp where state_1789_fr = 'Portugal' and substate_1789_fr is null and uhgs_id != 'A1965063';
-- 7
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965063' and ughs_id_sup = 'A1965063';

--Ecosse	A0388193	A0388193	Ecosse	subunit = 'Ecosse'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Ecosse', 'A0388193', uhgs_id , toponyme_standard_fr, 'subunit = Ecosse' , 1
from port_points pp where substate_1789_fr = 'Ecosse'  and uhgs_id != 'A0388193';
-- 18
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0388193' and ughs_id_sup = 'A0388193';

--Côte d'Espagne	A1965062	A1965062	Côte d'Espagne	etat='Espagne' and subunit is null	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côte d''Espagne', 'A1965062', uhgs_id , toponyme_standard_fr, 'etat=Espagne and subunit is null' , 1
from port_points pp where state_1789_fr = 'Espagne' and substate_1789_fr is null and uhgs_id != 'A1965062';
-- 87
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965062' and ughs_id_sup = 'A1965062';

--Espagne, undeterminé	A0074743	A0074743	Espagne, undeterminé	etat='Espagne' and subunit is null	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Espagne, undeterminé', 'A0074743', uhgs_id , toponyme_standard_fr, 'etat=Espagne and subunit is null' , 1
from port_points pp where state_1789_fr = 'Espagne' and substate_1789_fr is null and uhgs_id != 'A0074743';
-- 87
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0074743' and ughs_id_sup = 'A0074743';

----Canaries	A0079365	A0079365	Canaries	subunit='Canaries'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Canaries', 'A0079365', uhgs_id , toponyme_standard_fr, 'subunit=Canaries' , 1
from port_points pp where substate_1789_fr = 'Canaries'  and uhgs_id != 'A0079365';
-- 2
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0079365' and ughs_id_sup = 'A0079365';

-- --Côtes de Bretagne	A1964988	A1964988	Côtes de Bretagne	province='Bretagne'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Bretagne', 'A1964988', uhgs_id , toponyme_standard_fr, 'province=Bretagne' , 1
from port_points pp where province = 'Bretagne'  and uhgs_id != 'A1964988';
-- 2
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1964988' and ughs_id_sup = 'A1964988';

--Languedoc	A1965209	A1965209	Languedoc	province='Languedoc'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Languedoc', 'A1965209', uhgs_id , toponyme_standard_fr, 'province=Languedoc' , 1
from port_points pp where province = 'Languedoc'  and uhgs_id != 'A1965209';
-- 5
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965209' and ughs_id_sup = 'A1965209';

--Le long des côtes de Normandie	A1968879	A1968879	Le long des côtes de Normandie	province='Normandie'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Le long des côtes de Normandie', 'A1968879', uhgs_id , toponyme_standard_fr, 'province=Normandie' , 1
from port_points pp where province = 'Normandie'  and uhgs_id != 'A1968879';
-- 61
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1968879' and ughs_id_sup = 'A1968879';

--Côtes de Provence	A0212843	A0212843	Côtes de Provence	province='Provence'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Provence', 'A0212843', uhgs_id , toponyme_standard_fr, 'province=Provence' , 1
from port_points pp where province = 'Provence'  and uhgs_id != 'A0212843';
-- 52
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0212843' and ughs_id_sup = 'A0212843';

--Cotes du Rousillon	A1965060	A1965060	Cotes du Rousillon	province='Roussillon'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Cotes du Rousillon', 'A1965060', uhgs_id , toponyme_standard_fr, 'province=Roussillon' , 1
from port_points pp where province = 'Roussillon'  and uhgs_id != 'A1965060';
-- 6
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965060' and ughs_id_sup = 'A1965060';

--colonies anglaises [en Amérique]	B0000978	B0000978	colonies anglaises [en Amérique]	subunit='colonies britanniques d'Amérique'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'colonies anglaises [en Amérique]', 'B0000978', uhgs_id , toponyme_standard_fr, 'subunit=colonies britanniques d''Amérique' , 1
from port_points pp where substate_1789_fr = 'colonies britanniques d''Amérique'  and uhgs_id != 'B0000978';
-- 39
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'B0000978' and ughs_id_sup = 'B0000978';

--Irlande	A0605799	A0605799	Irlande	subunit='Irlande'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Irlande', 'A0605799', uhgs_id , toponyme_standard_fr, 'subunit=Irlande' , 1
from port_points pp where substate_1789_fr = 'Irlande'  and uhgs_id != 'A0605799';
-- 14
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0605799' and ughs_id_sup = 'A0605799';
-- 1

--Islande	A0146289	A0146289	Islande	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0146289' and ughs_id_sup = 'A0146289';

--Côte d'Afrique [de l'Ouest]	C0000006	C0000006	Côte d'Afrique [de l'Ouest]	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000006' and ughs_id_sup = 'C0000006';

--Côte d'Or	C0000012	C0000012	Côte d'Or	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000012' and ughs_id_sup = 'C0000012';

--Côte d'Angole	C0000009	C0000009	Côte d'Angole	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000009' and ughs_id_sup = 'C0000009';

--Mozambique	C0000013	C0000013	Mozambique	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000013' and ughs_id_sup = 'C0000013';

-- --Cap-Vert	C0000019	C0000019	Cap-Vert	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000019' and ughs_id_sup = 'C0000019';

--Côte du Brésil	B0000934	B0000934	Côte du Brésil	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'B0000934' and ughs_id_sup = 'B0000934';

--Hollande	A0617755	A0617755	Hollande	subunit='Hollande'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Hollande', 'A0617755', uhgs_id , toponyme_standard_fr, 'subunit=Hollande' , 1
from port_points pp where substate_1789_fr = 'Hollande'  and uhgs_id != 'A0617755';
-- 13
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0617755' and ughs_id_sup = 'A0617755';
-- 1

--Zélande	A0635679	A0635679	Zélande	subunit='Zélande'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Zélande', 'A0635679', uhgs_id , toponyme_standard_fr, 'subunit=Zélande' , 1
from port_points pp where substate_1789_fr = 'Zélande'  and uhgs_id != 'A0635679';
-- 4
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0635679' and ughs_id_sup = 'A0635679';
-- 1

--Royaume de Naples	A1964382	A1964382	Royaume de Naples	 etat='Royaume de Naples'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Royaume de Naples', 'A1964382', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Naples' , 1
from port_points pp where state_1789_fr = 'Royaume de Naples'  and uhgs_id != 'A1964382';
-- 85
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1964382' and ughs_id_sup = 'A1964382';
-- 1

--Sardaigne	A0223759	A0223759	Sardaigne	etat='Royaume de Piémont-Sardaigne'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sardaigne', 'A0223759', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Piémont-Sardaigne' , 1
from port_points pp where state_1789_fr = 'Royaume de Piémont-Sardaigne'  and uhgs_id != 'A0223759';
-- 24
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0223759' and ughs_id_sup = 'A0223759';
-- 1

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done 
from ports.generiques_inclusions_geo_csv 
where ughs_id_sup='A1964713'

update ports.generiques_inclusions_geo_csv  set ughs_id_sup = 'A1963986' where toponyme_sup='Mer Baltique';
-- 69
update ports.generiques_inclusions_geo_csv  set ughs_id_sup = 'A1964713' where toponyme_sup='Mer Mediterranée';
-- 421
-- rivière de Bordeaux	A1968858
update ports.generiques_inclusions_geo_csv  set ughs_id_sup = 'A1968858' where toponyme_sup='rivière de Bordeaux';
--13
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id_sup = 'A1964713' and ughs_id = 'A0156739';
	

--Italie	A1964975	A1964975	Italie	etat in ('Sardaigne', 'Gènes', 'Parme', 'Modène', 'Toscane', 'Lucques', 'Piombino', 'Naples', 'Etats Pontificaux', 'Venise')	0
--Duché de Massa et Carrare	
--Royaume de Naples
--Royaume de Piémont-Sardaigne
--République de Gênes
--République de Lucques
--République de Venise
--principauté de Lampédouse
--Principauté de Piombino
--Etats pontificaux

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Italie', 'A1964975', uhgs_id , toponyme_standard_fr, 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)' , 1
from port_points pp where state_1789_fr in ('Duché de Massa et Carrare', 'Royaume de Naples', 'Royaume de Piémont-Sardaigne', 'République de Gênes', 'République de Lucques', 'République de Venise', 'principauté de Lampédouse', 'Principauté de Piombino', 'Etats pontificaux')  
and uhgs_id != 'A1964975';
-- 173
update ports.generiques_inclusions_geo_csv  set done = 1, criteria = 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)'
 where ughs_id = 'A1964975' and ughs_id_sup = 'A1964975';
-- 1

--Côtes de Barbarie	A1965005	A1965005	Côtes de Barbarie	country2019_name in ( 'Egypt') or etat = 'Empire du Maroc' or subunit = Régence de Tunis,  Régence d'Alger, Régence de Tripoli)	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where country2019_name = 'Egypt'  and uhgs_id != 'A1965005';
-- 2
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where state_1789_fr = 'Empire du Maroc'  and uhgs_id != 'A1965005';
-- 4
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where substate_1789_fr in ('Régence de Tunis',  'Régence d''Alger', 'Régence de Tripoli')  and uhgs_id != 'A1965005';
--19
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965005' and ughs_id_sup = 'A1965005';
-- 1

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where substate_1789_fr in ('Tripoli')  and uhgs_id != 'A1965005';

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done 
from ports.generiques_inclusions_geo_csv 
where done=0
-- 0 -- fini !! jeudi 11 février 12H

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Colonies françaises en Amérique', 'B0000970', uhgs_id , toponyme_standard_fr, 'subunit=colonies françaises en Amérique' , 1
from port_points pp where substate_1789_fr = 'colonies françaises en Amérique' 
-- 2
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Colonies françaises en Amérique', 'B0000970', uhgs_id , toponyme_standard_fr, 'subunit=colonies françaises d''Amérique' , 1
from port_points pp where substate_1789_fr = 'colonies françaises d''Amérique' 
--21
--- B0000970	Colonies françaises en Amérique	

select * from port_points pp where uhgs_id = 'B0000970'
-- [{"1749-1815" : null}]
select * from port_points where state_1789_en = 'ul'
select * from etats  where uhgs_id = 'B0000970'
update  etats  set etat_en = 'France' where uhgs_id = 'B0000970';
update  etats  set subunit = 'colonies françaises d''Amérique' where subunit='colonies françaises en Amérique'

---
-- Rivière de Gênes	A0146288	A0146288	Rivière de Gênes	etat=République de Gênes

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Rivière de Gênes', 'A0146288', uhgs_id , toponyme_standard_fr, 'etat=République de Gênes' , 1
from port_points pp where state_1789_fr = 'République de Gênes' 

-- Finlande, undeterminé	A0921408	A0921408	Finlande, undeterminé	subunit=Finlande

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Finlande, undeterminé', 'A0921408', uhgs_id , toponyme_standard_fr, 'subunit=Finlande' , 1
from port_points pp where substate_1789_fr = 'Finlande' 
-- 5

SELECT * FROM ports.generiques_inclusions_geo_csv
----------------------------------------------------------

comment on table ports.codes_levels is 'list of admiralties with their head-quarters (siège amirauté) (by name). Only those cited in the Chardon report. The overseas provinces are missing (Martinique, Réunion, Guadeloupe)';
comment on table ports.obliques is 'liste of harbors having a clerk and the number of congés (taking off) known from existing records per year';
comment on table ports.matching_port is 'computed using geonames version : 2019 November - compare toponyme and location to give a geonameid to a port location';
comment on table ports.etats is 'comes from listings of Silvia Marzagalli: gives belonging states, substates (in fr, en) and range of dates between 1749 and 1815 for all ports ';
comment on table ports.world_borders is 'shp downloaded http://www.mappinghacks.com/data/ , last update 30 July 2008';
comment on table ports.fermes_ports_csv is 'alignment of ports with the management and office of the Farm / La Ferme (fiscal entity), 1789 - February 2, 2021';
comment on table ports.labels_lang_csv is 'list of toponyms, flags and standardized products in fr and en- February 10, 2021';
comment on table ports.generiques_inclusions_geo_csv is 'list of geographic inclusions between generic ports (* _sup) and ports. A port can belong to more than one generic port, depending on the grouping.
For example, a port in Sweden can also be part of the Baltic Sea. - February 11, 2021 ';

update etats set subunit = 'Régence de Tripoli' where uhgs_id = 'A1708033'	
select * from etats where uhgs_id = 'A1708033'	
-- Benghazi

update etats set etat_en = 'Russian Empire' where uhgs_id = 'A1813720' and dto = 1811
--Vyborg
select * from etats where uhgs_id = 'A1813720'

select * from etats where uhgs_id = 'A0354076'
update etats set subunit_en = 'British possessions in the Mediterranean' where uhgs_id = 'A0354076'
--	36.150000000000006	-5.4333330000000002		Rade de Gibraltar

select uhgs_id, toponyme_standard_fr, etat, subunit, etat_en, subunit_en , dfrom, dto from etats
order by etat, subunit

select uhgs_id, toponyme, etat, subunit, etat_en, subunit_en , dfrom, dto from etats
order by uhgs_id, dto , etat,  subunit

update  etats set dto=1800 where uhgs_id='A0220154' and dto is null
update etats set etat_en = 'Haiti' where etat = 'Haïti'
update  etats e set toponyme = toponyme from port_points p where p.uhgs_id = e.
select * from etats where uhgs_id = 'A1409167'
update etats set etat_en = 'Ottoman empire', subunit_en=null where dto = 1789 and uhgs_id = 'A1409167'
update etats set etat_en = 'Ottoman empire' where etat_en='Ottoman Empire'

select * from etats where toponyme_standard_fr = 'Mahon'
A0115898

select * from etats where etat_en = 'French Empire'

select * from etats where uhgs_id = 'A0238683' order by dto
--A0238683	Longone 	Royaume d'Étrurie		French Empire	Tuscany	1807	1815
--A0238683	Longone 	Royaume d'Étrurie		Kingdom of Etruria		1801	1807
update etats set  etat = 'France', subunit='Toscane', etat_en = 'France' where uhgs_id = 'A0238683' and dto=1815

--Italy	Longone 	Longone 	A0238683	Royaume de Naples	Etat des Présides		1801	3172394	Naples	04	40.85216	14.26811	7003012	https://fr.wikipedia.org/wiki/Royaume_de_Naples			Kingdom of Naples	State of the Presidi
--Italy	Longone 	Longone 	A0238683	Royaume d'Étrurie		1801	1807	3017382	Republic of France	00	46.0	2.0					Kingdom of Etruria	
--Italy	Longone 	Longone 	A0238683	France	Toscane	1807	1815	3017382	Republic of France	00	46.0	2.0					France	Tuscany


select * from etats where uhgs_id = 'A0222997' order by dfrom

A0249463	Santo Stefano		1801	Royaume de Naples	Etat des Présides	Kingdom of Naples	State of the Presidi
A0249463	Santo Stefano	1802		France		France	


create table ports.etats_unused11fev2021 as 
(select * from etats where uhgs_id not in (select uhgs_id from port_points pp))

delete from ports.etats where uhgs_id not in (select uhgs_id from port_points pp)

select * from labels_lang_csv llc order by label_type 

-------------------------------------------------------------------------------------------------------------------------
-- reprise de geo_general (ouvrir le fichier Geo_general.fmp12)
-------------------------------------------------------------------------------------------------------------------------

CREATE TABLE ports.new_geo_general (
	classentity text, 
	country text, 
	name text,
	lat text,
	long text,
	MGRS_id text,
	UHGS_id text)

	-- import homeport_missing.csv from C:\Travail\ULR_owncloud\ANR_PORTIC\Data\debug
select * from ports.homeports_missing_csv limit 10
select * from ports.new_geo_general  limit 10
drop table  ports.new_geo_general

select count(*) from ports.new_geo_general where lat is not null and ports.test_double_type(lat) is false
-- 58102
select * from ports.new_geo_general 
where (lat is not null and ports.test_double_type(lat) is false) or (long is not null and ports.test_double_type(long) is false)

alter table ports.new_geo_general  add column latitude float;
alter table ports.new_geo_general  add column longitude float;

update ports.new_geo_general  set latitude = lat::float where (lat is not null and ports.test_double_type(lat) is true);
update ports.new_geo_general  set longitude = long::float where (long is not null and ports.test_double_type(long) is true);



alter table ports.homeports_missing_csv add column latitude float;
alter table ports.homeports_missing_csv add column longitude float;
alter table ports.homeports_missing_csv add column amiraute text;
alter table ports.homeports_missing_csv add column province text;
alter table ports.homeports_missing_csv add column shiparea text;

alter table ports.homeports_missing_csv add column country2019_name text;

alter table ports.homeports_missing_csv add column oblique boolean;
alter table ports.homeports_missing_csv add column has_a_clerk boolean;
alter table ports.homeports_missing_csv add column source_1787_available boolean;
alter table ports.homeports_missing_csv add column source_1789_available boolean;
alter table ports.homeports_missing_csv add column status boolean;
alter table ports.homeports_missing_csv add column geonameid boolean;

update ports.homeports_missing_csv set latitude= g.latitude, longitude=g.longitude, country2019_name=country
from ports.new_geo_general g where UHGS_id = homeport_uhgs_id

select * from ports.new_geo_general where UHGS_id in (select distinct homeport_uhgs_id from ports.homeports_missing_csv  where latitude is null or longitude is null)
USA	North Carolina	34.816978	 -76.199347		B0000960
update ports.homeports_missing_csv set latitude=34.816978, longitude=-76.199347 where homeport_uhgs_id = 'B0000960';
select * from ports.new_geo_general where UHGS_id in (select distinct homeport_uhgs_id from ports.homeports_missing_csv  where latitude is null or longitude is null)
select * from ports.new_geo_general where UHGS_id = 'A1964984'
select count(distinct homeport_uhgs_id) from ports.homeports_missing_csv where latitude is null
-- 56/574 (9%) mais 34/296 (11%)

alter table ports.homeports_missing_csv add column geom geometry;
update ports.homeports_missing_csv set geom = st_setsrid(st_makepoint(longitude, latitude), 4326) where latitude is not null
alter table ports.homeports_missing_csv add column point3857 geometry;

select * from ports.obliques o ,ports.homeports_missing_csv where name % port
-- L' île d' Ouessant	A0125267 d1787 is null, d1789 = 11
-- Tréguier	A0171388 d1787 et d1789
-- Taillemont [Talmont]	A0135760 d1787 mais d1789 is null
-- Trinité en Riantec	A0147217 Trinité La d1787 mais d1789 is null
-- La Trinité sur mer	A0198704 Trinité La d1787 mais d1789 is null
-- Bréhat	5	4	6	17	43	17	39	52	42	42	36	32	A0217383	Ile de Bréhac	A0199797	48.85	-3.0				France							POINT (-3 48.85)
-- Ciboure					47								A0127400	Ciboure	A0217892	43.385483	-1.667275				France							POINT (-1.667275 43.385483)
-- Lannion	30	30	31	55	35	68	60	69	49	33	42		A0162246	Lanion	A1964093													

select distinct amiraute, province from ports.port_points pp order by province

select * from navigoviz.pointcall p where homeport_uhgs_id = 'A0125267'
00141868
00180092
00150904
00180093
00156874
00144330
select * from ports.port_points pp  where uhgs_id = 'A0129606'
'A0125267'

-------------------------------------------------------------------
-- 7 mars 2021 : liste des états uniques
-------------------------------------------------------------------

select distinct e.etat, e.etat_en , e.subunit , e.subunit_en  from ports.etats e 
order by etat, subunit


-- Danemark	Danemark	colonies danoises	Danish colonies
Grande-Bretagne	Great Britain	Irlande	Ireland

select * from ports.etats where etat='Danemark' and etat_en='Danemark' -- Islande
update ports.etats set etat_en='Denmark'  where etat='Danemark' and etat_en='Danemark' 
 
select * from ports.etats where etat='Grande-Bretagne' and etat_en='Great Britain' and subunit='Irlande' and subunit_en='Irland' 
update ports.etats set subunit_en = 'Ireland' where etat='Grande-Bretagne' and etat_en='Great Britain' and subunit='Irlande' and subunit_en='Irland' 
-- and subunit='Irlande' 

-------------------------------------------------------------------------
-- 10 mars 2021
-- Import des communes d'Europe
--Télécharger les communes sur toute l'europe
--https://ec.europa.eu/eurostat/fr/web/gisco/geodata/reference-data/administrative-units-statistical-units/communes#communes16
--version 2016 (EPSG 4326 ou 3857 au choix) - UTF-8
--
--Le fond Euro Boundary map coute une blinde
--https://eurogeographics.org/maps-for-europe/licensing/
--
--Chez Eurostat, les LAU2, avec Grece, Turquie et Irlande seulement
--https://ec.europa.eu/eurostat/fr/web/nuts/local-administrative-units
-- Turquie : latin 1 , EPSG 4230
-- 
--Les données des USA
--https://www.census.gov/programs-surveys/geography/guidance/tiger-data-products-guide.html
--https://www.census.gov/geographies/mapping-files/time-series/geo/cartographic-boundary.html
--https://www2.census.gov/geo/tiger/GENZ2019/shp/cb_2019_us_state_500k.zip
--1 : 500,000 (national)  projection 4269

-- achat des limites historiques de 1700 : 192 euros
-- https://www.euratlas.net/shop/maps_gis/fr_gis_1700.html
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\gis_1700\data\1700\utf8\2nd_level_divisions.shp
 
--l'UKRAINE : 4326
--http://worldmap.harvard.edu/data/geonode:ukraine_may_2014_administrative_units_9ob
--
--La Crimée : 4326
--- http://worldmap.harvard.edu/data/geonode:crimea_9sr

-- Importer le fond NON ESPON space (3857)
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\cartes\NUTS_2003\GEOM\map_template_1_(eurogeographics_20M)\non_espon_space_2003.shp
-- 
-------------------------------------------------------------------------
export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\COMM_RG_01M_2016_3857.shp\COMM_RG_01M_2016_3857.shp -a_srs EPSG:3857 -nln LAU_europe -nlt MULTIPOLYGON

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\gis_1700\data\1700\utf8\2nd_level_divisions.shp -a_srs EPSG:3395 -nln gis_1700_division2 -nlt MULTIPOLYGON

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\gis_1700\data\1700\utf8\sovereign_states.shp -a_srs EPSG:3395 -nln gis_1700_sovereign_states -nlt MULTIPOLYGON

-- ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\cartes\NUTS_2003\GEOM\map_template_1_(eurogeographics_20M)\non_espon_space_2003.shp -a_srs EPSG:4258 -nln around_europe -nlt MULTIPOLYGON
drop table around_europe 
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\non_espon_space_3857.shp -a_srs EPSG:3857 -nln around_europe -nlt MULTIPOLYGON

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\cb_2019_us_state_20m\cb_2019_us_state_20m.shp -a_srs EPSG:4269 -nln usa -nlt MULTIPOLYGON

-- Rajouter la Turquie ((latin 1)
set PGCLIENTENCODING=latin1
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\Geodata\TR_2011_LAU1.shp -a_srs EPSG:4230 -nln LAU_turquie -nlt MULTIPOLYGON

-- Rajouter l'Ukraine 4326 au niveau des régions administratives
export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\ukraine_may_2014_administrative_units_9ob\ukraine_may_2014_administrative_units_9ob.shp -a_srs EPSG:4326 -nln Ukraine -nlt MULTIPOLYGON

alter table ukraine add geom3857 geometry;
update ukraine set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857);

insert INTO LAU_europe(COMM_NAME, COMM_ID, FID, CNTR_CODE, wkb_geometry)
(SELECT ADI AS COMM_NAME , ICC_LAU_CO as COMM_ID, ICC_LAU_CO as FID, ICC as CNTR_CODE, st_setsrid(st_transform(wkb_geometry, 3857), 3857) as wkb_geometry 
FROM LAU_turquie)

alter table gis_1700_division2 add geom3857 geometry;
update gis_1700_division2 set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857);

alter table gis_1700_sovereign_states add geom3857 geometry;
update gis_1700_sovereign_states set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857);

alter table LAU_europe add methodg int;
-- 1 pour inclusion totale
-- 2 pour intersection avec des polygones mais de la meme entite sup
-- 2 pour intersection avec des polygones d'entités distinctes
-- null quand il faut étendre

alter table LAU_europe add id_hgis int; -- jointure avec num de gis_1700_division2
alter table LAU_europe add id_sup int; -- jointure avec owner_id de gis_1700_division2
alter table LAU_europe add id_sup_holder int; -- jointure avec holder_id de gis_1700_division2

select count(distinct num) from gis_1700_division2
-- 302 / 836
select count(distinct holder_id) from gis_1700_division2
-- 92

select count(*) from LAU_europe
-- 122750


select e.comm_name , d.short_name , 1 as methodg, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where st_contains(d.geom3857, e.wkb_geometry)


update LAU_europe set methodg = null, id_hgis=null, id_sup = null, id_sup_holder=null, province_code = null, province_name = null , methodnum = null

update LAU_europe e
set methodg = 1, id_hgis = d.num, id_sup=d.owner_id, id_sup_holder=d.holder_id
from gis_1700_division2 d
where methodg is null and st_contains(d.geom3857, e.wkb_geometry)  
-- 97008

select e.ogc_fid, e.comm_name , d.short_name , 2 as methodg, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
order by e.ogc_fid 
-- Andorra par exemple

select e.ogc_fid , count(distinct owner_id) as c , 2 as methodg
--, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
group by e.ogc_fid 
having count(distinct owner_id)= 1
order by e.ogc_fid 

update LAU_europe e
set methodg = 2,  id_sup=d.owner_id
from gis_1700_division2 d,
	(select e.ogc_fid , count(distinct owner_id) as c , 2 as methodg
	--, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
	from LAU_europe e, gis_1700_division2 d
	where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
	group by e.ogc_fid 
	having count(distinct owner_id)= 1 ) as k
where e.methodg is null and k.ogc_fid=e.ogc_fid 
and st_intersects(d.geom3857, e.wkb_geometry)  
-- 16404

select count(*) from LAU_europe e
where methodg is null
-- 9337

select e.ogc_fid, e.comm_name , d.short_name , 3 as methodg, d.num as id_hgis, d.owner_id as id_sup, d.holder_id as id_sup_holder
from LAU_europe e, gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
order by e.ogc_fid 

update LAU_europe e
set methodg = 3, id_hgis = d.num, id_sup=d.owner_id, id_sup_holder=d.holder_id
from gis_1700_division2 d
where methodg is null and st_intersects(d.geom3857, e.wkb_geometry)
-- 7813 (3 : il faut retraiter pour attribuer aux plus grandes unités qui intersectent)

select count(*) from LAU_europe e
where methodg is null
-- 1538

-- régler le cas de Donets'ka en Ukraine
-- c'est une partie qui devrait faire partie de la subdivision DON COSSACKS (voir histoire de la ville de Donets), qui appartient à la Russie

select * from ukraine where adm1_name = 'Donets''ka'
select e.* from LAU_europe e, ukraine u
where st_contains(u.geom3857, e.wkb_geometry) and u.adm1_name = 'Donets''ka' and methodg is null

update LAU_europe e set methodg=4, id_hgis=2, id_sup = 2344, id_sup_holder=-1
from ukraine u 
where st_contains(u.geom3857, e.wkb_geometry) and u.adm1_name = 'Donets''ka' and methodg is null
-- 6 ok

-- régler le cas la Finlande, la Suède et la Norvège

select e.* from LAU_europe e, world_borders wb 
where st_contains(wb.mpolygone3857, e.wkb_geometry) and wb."name" = 'Norway' and methodg is null

select e.* from LAU_europe e where cntr_code = 'NO' and methodg is null
update LAU_europe set methodg=4, id_hgis=71, id_sup = 892, id_sup_holder=-1 where cntr_code = 'NO' and methodg is null;
update LAU_europe set province_code ='NO', province_name = 'Norvège' where cntr_code = 'NO' ;

-- 426

select e.* from LAU_europe e, world_borders wb 
where st_contains(wb.mpolygone3857, e.wkb_geometry) and wb."name" = 'Sweden' and methodg is null


select e.* from LAU_europe e where cntr_code = 'SE' and methodg is null
update LAU_europe set methodg=4, id_hgis=158, id_sup = 8246, id_sup_holder=-1 where cntr_code = 'SE' and methodg is null;
-- 63

-- La Finlande est Russe à partir de 1809
-- https://fr.wikipedia.org/wiki/Finlande#Domination_su%C3%A9doise_puis_russe
select e.* from LAU_europe e where cntr_code = 'FI' and methodg is null
update LAU_europe set methodg=4, id_hgis=192, id_sup = 8246, id_sup_holder=-1 where cntr_code = 'FI' and methodg is null;
update LAU_europe set province_code ='FI', province_name = 'Finlande'  where cntr_code = 'FI' ;
-- 311

-- la Poméranie Suedoise (num 163 ) e Suède (8246) 
update LAU_europe set province_code ='POMS', province_name = 'Poméranie suédoise'  where id_sup = 8246 
and id_hgis=163 and nuts_code in ('DE80N', 'DE80L', 'DE80J', 'PL424' , 'PL428')
-- 270

-- régler le cas des iles SHetlands (appartiennent à GB, SHETLAND)
update LAU_europe set methodg=4, id_hgis=192, id_sup = 2165, id_sup_holder=-1 where 
comm_id in ('GBS13002772', 'GBS13002773', 'GBS13002777') 
and methodg is null;


-- régler le cas de l'Islande (IS), des iles Feroe (FO), du Groenland (GL) qui appartiennent au royaume Denmark-Norvège (892)
update LAU_europe set methodg=4, id_hgis=0, id_sup = 892, id_sup_holder=-1 where 
cntr_code  in ('FO', 'IS', 'GL') 
and methodg is null;
-- 103 + 5

-- régler le cas des canaries 
update LAU_europe set methodg=4, id_hgis=85, id_sup = 1122, id_sup_holder=-1 where 
nuts_code  in ('ES703', 'ES704', 'ES705', 'ES706', 'ES707', 'ES708', 'ES709') 
and methodg is null;
update LAU_europe set province_code='ES7XX', province_name = 'Canaries' where nuts_code  in ('ES703', 'ES704', 'ES705', 'ES706', 'ES707', 'ES708', 'ES709') 

-- 68

-- régler le cas des acores (PT200) 
update LAU_europe set methodg=4,  id_sup = 3852, id_sup_holder=-1 where 
nuts_code  in ('PT200') 
and methodg is null;
-- 156

-- régler le cas de la Martinique, Guadeloupe, Guyanne , Mayotte, Réunion

alter table LAU_europe add column province_code text;
alter table LAU_europe add column province_name text;

update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY30', province_name='Guyanne' where 
nuts_code  in ('FRY30') and methodg is null; --22
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY20', province_name='Martinique' where 
nuts_code  in ('FRY20') and methodg is null; --34
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY10', province_name='Gualeloupe' where 
nuts_code  in ('FRY10') and methodg is null; --33
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY50', province_name='Mayotte' where 
nuts_code = 'FRY50' and methodg is null; --17
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, province_code='FRY40', province_name='Réunion' where 
nuts_code = 'FRY40' and methodg is null; --24

--  le cas de BL97701 - Saint-Barthélemy
-- cédé par la France à la Suède en 1784
-- https://en.wikipedia.org/wiki/Saint_Barth%C3%A9lemy#17th_century
update LAU_europe set methodg=4,  id_sup = 8246, id_sup_holder=-1, province_code='BL97701', province_name='Saint-Barthélemy' where 
comm_id = 'BL97701' and methodg is null;

select count(*) from LAU_europe e
where methodg is null
-- 496

-- 1122	Espagne	Spain	FK	colonies espagnoles d'Amérique	Spanish colonies in America	les malouines (Falkland Islands)

update LAU_europe set methodg=4,  id_sup = 1122, id_sup_holder=-1, province_code='FK', province_name='Falkland Islands' where 
comm_id = 'FK' and methodg is null; --1



-- En Grèce

-- Paxon (Venise 2490, num = 124, STATO DEL MAR)
-- https://fr.wikipedia.org/wiki/Paxos
update LAU_europe set methodg=4,  id_sup = 2490, id_sup_holder=-1, id_hgis=124 where 
ogc_fid = 84363  and methodg is null;

-- Agkistriou
update LAU_europe set methodg=4,  id_sup = 2490, id_sup_holder=-1, id_hgis=124 where 
ogc_fid = 84729 and methodg is null;

-- Sikinou
update LAU_europe set methodg=4,  id_sup = 6084, id_sup_holder=-1, id_hgis=146 where 
ogc_fid = 84805  and methodg is null;

-- les iles européennes
update LAU_europe set methodg=4,  id_sup = 1122, id_sup_holder=-1, id_hgis=61 where 
ogc_fid = 37897  and methodg is null;

-- iles de Bretagne
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, id_hgis=14 where 
nuts_code in ('FRH01', 'FRH02', 'FRH04')  and methodg is null; --17

update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, id_hgis=11 where 
nuts_code in ('FRG05')  and methodg is null;
update LAU_europe set methodg=4,  id_sup = 1225, id_sup_holder=-1, id_hgis=12 where 
nuts_code in ('FRI32')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 1122, id_sup_holder=-1, id_hgis=73 where 
ogc_fid=96482  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 2750, id_sup_holder=-1, id_hgis=36 where 
ogc_fid=106713  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 2490, id_sup_holder=-1, id_hgis=124 where 
ogc_fid= 83701 and methodg is null;

update LAU_europe set methodg=4,  id_sup = 1423, id_sup_holder=-1, id_hgis=136 where 
nuts_code in ('UKK30')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 4117, id_sup_holder=1281, id_hgis=5 where 
nuts_code in ('DE947', 'DE94H')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 6319, id_sup_holder=-1, id_hgis=3 where 
nuts_code in ('DE94A')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=110 where 
nuts_code in ('DEF07', 'DEF09')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 1423, id_sup_holder=-1, id_hgis=77 where 
nuts_code in ('IE042')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=119 where 
nuts_code in ('DK014')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=79 where 
nuts_code in ('DK022')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=54 where 
nuts_code in ('DK031')  and methodg is null;

update LAU_europe set methodg=4,  id_sup = 892, id_sup_holder=-1, id_hgis=118 where 
nuts_code in ('DK041', 'DK042')  and methodg is null;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

select st_buffer(st_union(e.wkb_geometry), 0)
from LAU_europe e
where cntr_code = 'SE'

drop  table ports.world_1789 
create table ports.world_1789 
(
id serial,
unit text,
unit_id int,
unit_code text,
unitlevel int, --0 pour etat, 1 pour sous-unité
unit_sup_id int,
unit_sup_code text,
sourcegeom text,
sourcecode text,
geom3857 geometry
)


insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e
group by id_sup
-- 92

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e WHERE id_hgis IS NOT null
group by id_hgis
-- 275, 2min 21

select * from LAU_europe e where id_hgis is null

update LAU_europe e set id_hgis = null, province_name = 'Island', province_code = 'IS' where cntr_id = 'IS' and id_hgis = 0;
update LAU_europe e set id_hgis = null, province_name = 'Faroe Islands', province_code = 'FO' where cntr_id = 'FO' and id_hgis = 0;
update LAU_europe e set id_hgis = null, province_name = 'Greenland', province_code = 'GL' where cntr_id = 'GL' and id_hgis = 0;

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_collect(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null
group by province_code, province_name

drop table around_europe 

-- Suède
insert into ports.world_1789 (unit, unit_id, subunit, subunit_id,sourcegeom, sourcecode,  geom3857)
(select 'Denmark', 8246, 'Sweden', 158, 'LAU_europe', 'SE', st_union(e.wkb_geometry) 
from LAU_europe e
where cntr_code = 'SE')
-- correction
update ports.world_1789 set unit='Sweden', unit_id=8246, subunit_id=null, subunit=null   where sourcecode ='SE'
select * from ports.world_1789

insert into ports.world_1789 (unit, unit_id, subunit, subunit_id,sourcegeom, sourcecode,  geom3857)
(select 'Sweden', 8246, 'Finland', 192, 'LAU_europe', 'FI', ST_union(e.wkb_geometry) 
from LAU_europe e
where cntr_code = 'FI')



insert into ports.world_1789 (unit, unit_id, subunit, subunit_id,sourcegeom, sourcecode,  geom3857)
(select 'Denmark', 892, 'Norway', 71, 'LAU_europe', 'NO', ST_union(e.wkb_geometry) 
from LAU_europe e
where cntr_code = 'NO')




-- plus tard
update ports.world_1789 set geom3857 = ST_UnaryUnion (geom3857) where subunit = 'Sweden'
select * from ports.world_1789;

--------------------------------------------------------------------------------

--------------------
select count(*) from LAU_europe e
where methodg is null
-- 810

-- Traiter les cas d'intersection 3 (à cheval sur deux états) 
-- calculer l'intersection entre la division owner_id et la commune LAU
-- attribuer la commune à la partie la plus grande 

select count(*) from LAU_europe e
where methodg = 3
-- 7813

select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
-- set methodg = 3, id_hgis = d.num, id_sup=d.owner_id, id_sup_holder=d.holder_id
from gis_1700_division2 d, LAU_europe e
where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) --e.id_sup = d.owner_id and
order by e.ogc_fid, surface 
-- 54 secondes

-- and  e.comm_name = 'Andorra' 
select * from LAU_europe e where e.comm_name = 'Andorra' -- 2 communes 'Andorra' en Espagne

select k.ogc_fid, max(surface)
from 
	(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
	from gis_1700_division2 d, LAU_europe e
	where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and e.ogc_fid = 17949
	) as k
group by ogc_fid


select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  
from gis_1700_division2 d, 
	LAU_europe e,
	(select k.ogc_fid, max(surface) as surfacemax
		from 
		(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
		from gis_1700_division2 d, LAU_europe e
		where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and e.ogc_fid = 17949
		) as k
	group by ogc_fid) as qmax -- 7813
where e.ogc_fid = 17949 and methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)



select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , d.num, d.holder_id 
from gis_1700_division2 d, 
	LAU_europe e,
	(select k.ogc_fid, max(surface) as surfacemax
		from 
		(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
		from gis_1700_division2 d, LAU_europe e
		where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) 
		) as k
	group by ogc_fid) as qmax -- 7813
where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid 
and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)
-- ok

update LAU_europe e set id_hgis = s.num, id_sup=s.owner_id, id_sup_holder=s.holder_id
from (
	select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , d.num, d.holder_id 
	from gis_1700_division2 d, 
		LAU_europe e,
		(select k.ogc_fid, max(surface) as surfacemax
			from 
			(select e.ogc_fid, e.comm_name, d.owner_id , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
			from gis_1700_division2 d, LAU_europe e
			where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) 
			) as k
		group by ogc_fid) as qmax -- 7813
	where methodg = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid 
	and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)
) as s 
where s.ogc_fid = e.ogc_fid 
-- 7813

-- C'est bon, avec id_sup, toutes les LAU ont leur ETAT de renseigné

select count(*) from LAU_europe e
where methodg is null
-- 332

select black_ogc_fid, max(longueurcommune) 
from 
(
	select black_ogc_fid, id_sup, sum(commun) as longueurcommune
	from (
		select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_sup, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
		from LAU_europe e1,LAU_europe e2
		where e2.methodg is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true
		order by e2.ogc_fid
	) as k
	group by black_ogc_fid, id_sup
	-- order by black_ogc_fid
) as q
group by black_ogc_fid


update LAU_europe e set methodg=5, id_sup=s.id_sup
from (
	select lmax.black_ogc_fid, id_sup
	from 
	(
		select black_ogc_fid, max(longueurcommune) 
		from 
		(
			select black_ogc_fid, id_sup, sum(commun) as longueurcommune
			from (
				select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_sup, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
				from LAU_europe e1,LAU_europe e2
				where e2.methodg is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_sup is not null
				order by e2.ogc_fid
			) as k
			group by black_ogc_fid, id_sup
			-- order by black_ogc_fid
		) as q
		group by black_ogc_fid
	) as lmax,
	(select black_ogc_fid, id_sup, sum(commun) as longueurcommune
		from (
			select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_sup, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
			from LAU_europe e1,LAU_europe e2
			where e2.methodg is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_sup is not null
			order by e2.ogc_fid
		) as k
		group by black_ogc_fid, id_sup
		order by black_ogc_fid
	) as k
	where k.black_ogc_fid = lmax.black_ogc_fid 
) as s 
where s.black_ogc_fid = e.ogc_fid and methodg is null
-- 365
-- 27 au second passage
-- 6 au troisième passage
-- 3 au 4eme passage
-- 0 ensuite

	



select count(*) from LAU_europe e
where methodg is null
-- 0

select  id_hgis, count(*) 
from LAU_europe e
group by id_hgis

select * from LAU_europe where id_hgis is null and methodg != 2 and methodg != 5

-- Mettre hgis à jour pour les cas 2 puis 5 par extension

alter table LAU_europe add column methodnum int;

update LAU_europe e
set methodnum = 1, id_hgis = d.num
from gis_1700_division2 d
where methodnum is null and st_contains(d.geom3857, e.wkb_geometry) 
-- 97262

select e.ogc_fid , count(distinct num) as c , 2 as methodnum
from LAU_europe e, gis_1700_division2 d
where methodnum is null and st_intersects(d.geom3857, e.wkb_geometry)
group by e.ogc_fid 
having count(distinct num)= 1
order by e.ogc_fid 

update LAU_europe e
set methodnum = 2,  id_hgis=d.num
from gis_1700_division2 d,
	(select e.ogc_fid , count(distinct num) as c , 2 as methodnum
	from LAU_europe e, gis_1700_division2 d
	where methodnum is null and st_intersects(d.geom3857, e.wkb_geometry)
	group by e.ogc_fid 
	having count(distinct num)= 1) as k
where e.methodnum is null and k.ogc_fid=e.ogc_fid 
and st_intersects(d.geom3857, e.wkb_geometry)  
-- 4623

update LAU_europe e
set methodnum = 3, id_hgis = d.num
from gis_1700_division2 d 
where methodnum is null and st_intersects(d.geom3857, e.wkb_geometry)
-- 20284 (3 : il faut retraiter pour attribuer aux plus grandes unités qui intersectent)

update LAU_europe e set id_hgis = s.num
from (
	select e.ogc_fid, e.comm_name, d.num , d.short_name  
	from gis_1700_division2 d, 
		LAU_europe e,
		(select k.ogc_fid, max(surface) as surfacemax
			from 
			(select e.ogc_fid, e.comm_name, d.num , d.short_name  , round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000) as surface
			from gis_1700_division2 d, LAU_europe e
			where methodnum = 3 and st_intersects(d.geom3857, e.wkb_geometry) 
			) as k
		group by ogc_fid) as qmax -- 7813
	where methodnum = 3 and st_intersects(d.geom3857, e.wkb_geometry) and qmax.ogc_fid=e.ogc_fid 
	and qmax.surfacemax = round(st_area(st_intersection(d.geom3857, e.wkb_geometry))/1000)
) as s 
where s.ogc_fid = e.ogc_fid 
-- 20284, 3min 18 s 

select count(*) from LAU_europe e
where methodnum is null
--------------

update LAU_europe e set methodnum=5, id_hgis=s.id_hgis
from (
	select lmax.black_ogc_fid, id_hgis
	from 
	(
		select black_ogc_fid, max(longueurcommune) 
		from 
		(
			select black_ogc_fid, id_hgis, sum(commun) as longueurcommune
			from (
				select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_hgis, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
				from LAU_europe e1,LAU_europe e2
				where e2.methodnum is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_hgis is not null
				order by e2.ogc_fid
			) as k
			group by black_ogc_fid, id_hgis
			-- order by black_ogc_fid
		) as q
		group by black_ogc_fid
	) as lmax,
	(select black_ogc_fid, id_hgis, sum(commun) as longueurcommune
		from (
			select e2.ogc_fid as black_ogc_fid, e2.comm_name, e1.ogc_fid, e1.comm_name, e1.id_hgis, round(ST_Length(ST_Intersection(e1.wkb_geometry,e2.wkb_geometry))) as commun
			from LAU_europe e1,LAU_europe e2
			where e2.methodnum is null and st_touches(e1.wkb_geometry,e2.wkb_geometry)=true and e1.id_hgis is not null
			order by e2.ogc_fid
		) as k
		group by black_ogc_fid, id_hgis
		order by black_ogc_fid
	) as k
	where k.black_ogc_fid = lmax.black_ogc_fid 
) as s 
where s.black_ogc_fid = e.ogc_fid and methodnum is null;
-- 1131
-- 28
-- 6
-- 3


select  id_hgis, count(*) 
from LAU_europe e
group by id_hgis


/*
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\shp\Pologne_1789.shp -a_srs EPSG:3857 -nln Pologne_1789 -nlt MULTIPOLYGON
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\shp\Prussia_1789.shp -a_srs EPSG:3857 -nln Prussia_1789 -nlt MULTIPOLYGON

select * from ports.pologne_1789 p2 
select st_buffer(st_boundary(ST_UnaryUnion(st_collect(p.wkb_geometry)) ),0.1)
from ports.pologne_1789 p 
group by id
-- select * from ports.pologne_1789 p 
alter table  pologne_1789 add unaryunion geometry;

update pologne_1789 set unaryunion = k.geom
from (select ST_UnaryUnion(st_collect(wkb_geometry)) as geom from ports.pologne_1789 ) as k
where ogc_fid = 1

-- update ports.pologne_1789 set id = 29 where id is null
alter table  prussia_1789 add unaryunion geometry;
select * from ports.prussia_1789 p2 
update prussia_1789 set unaryunion = k.geom
from (select ST_UnaryUnion(st_collect(wkb_geometry)) as geom from ports.prussia_1789 ) as k
where ogc_fid = 1

-- ok
update world_1789 w set geom3857 = unaryunion, sourcegeom = w.sourcegeom || ', around_europe'
from ports.prussia_1789 p2 
where p2.ogc_fid = 1 and w.id  = 66

update world_1789 w set geom3857 = unaryunion, sourcegeom = w.sourcegeom || ', around_europe'
from ports.pologne_1789 p2 
where p2.ogc_fid = 1 and w.id  = 29



select st_unaryunion(st_collect(geom3857)) from (
select geom3857 from ports.world_borders_dump where name like 'Russia' and st_contains(geom3857, capitale)
-- union 
-- select geom3857 from ports.world_borders_dump where name like 'Kazakhstan' 
union 
select geom3857 from ports.world_1789 w where unit_id = 2344 and unitlevel = 0
UNION
select geom3857 from ports.world_borders_dump where name like 'Russia' and NOT st_contains(geom3857, capitale) 
AND st_isvalid(geom3857) AND pkid != 1609
union 
select geom3857 from ports.world_borders_dump where name like 'Georgia' 
) as k
-- Rajouter la Georgia
pkid = 1741

http://worldmap.harvard.edu/data/geonode:_boundaries_5c5
*/
-- 228

---------------------------------------------------------------
-- Piemont (unit_id = 4705)
--------------------------------------------------------------

-- import des circonscriptions 1861 d'Italie (en ligne)
-- + Montferra (2310)
-- + Masserano (7184)
-- Retirer de Milan (1122) les circonscriptions suivantes
-- Ossola, Valsesia, Biella, Lomallina, Alessandria, Tortona, 
-- Novi (partie qui intersecte Milan - 188), Bobbio (partie qui intersecte Milan - 39),
-- Voghera (321)
-- Ajouter la Sardaigne : num = 65 à 68

-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\Limiti_1861\Circondari_1861

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports"  C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\Limiti_1861\Circondari_1861\Circondari_1861.shp -a_srs EPSG:32632 -nln Italie_1861 -nlt MULTIPOLYGON

alter table Italie_1861 add geom3857 geometry
update Italie_1861 set geom3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857)




SELECT * FROM ports.world_1789 WHERE unit_id in (4705, 2310, 7184)
delete from ports.world_1789 WHERE unit_id in (4705, 2310, 7184) 

update LAU_europe e set id_sup = 4705 where id_sup in (2310, 7184);
update LAU_europe e set id_sup = 4705, province_code = 'SA02', province_name='Sardaigne' where id_hgis in (65,66,67,68) and id_sup=1122 -- Sardaigne
update LAU_europe e set id_sup = 4705 where ogc_fid in (92731) -- commune d'Alagna
update LAU_europe e set id_sup = 4705 where ogc_fid = 93639

SELECT * FROM ports.world_1789 WHERE unit_id in (1122) -- Milan

DROP TABLE test_piemont
create table test_piemont as (
select st_unaryunion(st_collect(e.wkb_geometry)) 
from LAU_europe e, italie_1861 i 
where e.id_sup = 1122 and i.den_circ in ('Ossola', 'Valsesia', 'Biella', 'Lomellina', 'Alessandria', 'Tortona', 'Voghera', 'Vercelli', 'Casale Monferrato', 'Pavia', 'Novi', 'Bobbio')
and st_intersects(i.geom3857 , e.wkb_geometry ) AND st_area(st_intersection(i.geom3857 , e.wkb_geometry))/st_area(e.wkb_geometry) > 0.80
)  

update LAU_europe e set id_sup = 4705
from italie_1861 i 
where e.id_sup = 1122 and i.den_circ in ('Ossola', 'Valsesia', 'Biella', 'Lomellina', 'Alessandria', 'Tortona', 'Voghera', 'Vercelli', 'Casale Monferrato', 'Pavia', 'Novi', 'Bobbio')
and st_intersects(i.geom3857 , e.wkb_geometry ) AND st_area(st_intersection(i.geom3857 , e.wkb_geometry))/st_area(e.wkb_geometry) > 0.80

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e WHERE id_sup = 4705
group by id_sup
--------------------------------------





--- Corse
-- --Corse (nuts_code = 'FRM01', 'FRM02') e France (1225)

SELECT id_sup, id_hgis, province_code , province_name FROM lau_europe le where le.nuts_code in ('FRM02', 'FRM01')
update lau_europe set id_hgis = null, province_code = 'FRM', province_name = 'Isles de Corse', id_sup='1225'
where nuts_code in ('FRM02', 'FRM01');
-- 360

-- Lorraine
-- --Lorraine [owner_id = 2514 ] ?? e France (1225)
-- la Lorraine : duché acquis par la France en 1766
SELECT id_sup, id_hgis, province_code , province_name FROM lau_europe le where id_sup = 2514
update lau_europe set id_hgis = null, province_code = 'FRL', province_name = 'Lorraine', id_sup='1225'
where id_sup = 2514;
--1015

SELECT max(id_sup) FROM lau_europe
-- 15204

-- 100000

-- --Naples (num 77 et 82) e Royaume de Naples et plus à l'Espagne
-- Naples (69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80) = NA01 (province_code), et NA 
update lau_europe set  province_code = 'NA01', province_name = 'Naples', id_sup=100000
where id_hgis in (69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80) and id_sup = 1122;

update lau_europe set  province_code = 'NA02', province_name = 'Sicile', id_sup=100000
where id_hgis in (81, 82, 83) and id_sup = 1122;

update lau_europe set  province_code = 'NA03', province_name = 'Etat des Présides', id_sup=100000
where id_hgis in (34) and id_sup = 1122;
select * from lau_europe where province_code = 'NA03'
update lau_europe set province_code = null, province_name = null, id_sup = '62' where province_code = 'NA03'


update lau_europe set  province_code = 'NA03', province_name = 'Etat des Présides', id_sup=100000, id_hgis=NULL where ogc_fid in ( 97064, 96987, 96338)
-- id_sup = 62, id_hgis = 34

-- Pays-bas (actuelle Belgique) et Milanias: mettre dans la même couleur que l'Autriche et pas dans la même couleur que l'Espagne (changement de domination en 1713)

-- Milanais (id_hgis = 33, id_sup = 1122) appartient aux habsbourgs (Autriche), owner_id = 5689
update lau_europe set   id_sup=5689  where id_hgis = 33 and id_sup = 1122

-- Pays-bas (actuelle Belgique) autrichiens
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (2, 26, 92, 91, 13, 94) and id_sup = 1122 and cntr_code in ('BE', 'LU')
--- reste à finir
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (2, 92) and id_sup = 1122 and cntr_code in ('FR');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (94) and id_sup = 1122 and cntr_code in ('FR');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (26,13) and id_sup = 1122 and cntr_code in ('NL');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (5) and id_sup = 1122 and cntr_code in ('NL');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (5) and id_sup = 1122 and cntr_code in ('DE');
update lau_europe set   id_sup=5689, province_code='AU03' where id_hgis in (94,13) and id_sup = 1122 and cntr_code in ('DE');

/*
--- Ravennes, id_hgis = 13 se retrouve dans l'Autriche, alors qu'elle appartient aux Papal States
update lau_europe set province_code = null, province_name = null, id_sup = 2140 where province_code = 'AU03' and id_sup =5689 and id_hgis=13  and nuts_code like 'IT%'
select * from lau_europe where province_code = 'AU03' and id_sup =5689 and id_hgis in (2, 26, 92, 91, 13, 94) order by nuts_code

-- Corriger sur la Suisse
update lau_europe set province_code = null, province_name = null, id_sup = 5666 where province_code = 'AU03' and id_sup =5689 and id_hgis in (2, 26, 92, 91, 13, 94) and nuts_code like 'CH%'
-- Corriger sur la France
update lau_europe set province_code = null, province_name = null, id_sup = 1225 where province_code = 'AU03' and id_sup =5689 and id_hgis in (2, 26, 92, 91, 13, 94) and nuts_code like 'FR%'
-- Corriger en allemagne De : 9642 Passau
update lau_europe set province_code = null, province_name = null, id_sup = 9642 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and (nuts_code = 'DE225' or nuts_code = 'DE228')
update lau_europe set province_code = null, province_name = null, id_sup = 9642 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and (ogc_fid = 25026)
update lau_europe set province_code = null, province_name = null where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and (ogc_fid = 1263)

-- Corriger en allemagne De : Ratzeburg
update lau_europe set province_code = null, province_name = null, id_sup = 8445 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DE80M'
update lau_europe set province_code = null, province_name = null, id_sup = 8445 where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DEF06'

-- Corriger Saxe-Hildburghausen 4289 DE24C, DEG%, DE247
update lau_europe set province_code = null, province_name = null, id_sup = 4289 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code like 'DEG%' OR nuts_code='DE247' OR nuts_code='DE24C'
-- 8583 Hechingen : DE143 (pas DE142 qui finalement aurait été Autrichienne
update lau_europe set province_code = null, province_name = null, id_sup = 8583 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DE143'
-- 8583 Hechingen :  Mössigen commune, DE142
update lau_europe set province_code = null, province_name = null, id_sup = 8583 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and ogc_fid = 20214
--MD, RO, UA e Ottoman : ERREUR sur UA et MD
update lau_europe set province_code = null, province_name = null, id_sup =  6084
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code like 'RO%' OR cntr_code like 'UA%' OR cntr_code like 'MD%'
-- correction de UA
update lau_europe set  id_sup =  5689
where   id_sup =6084 and id_hgis in (83, 55) AND cntr_code like 'UA%'
-- correction de MD
update lau_europe set  id_sup =  2750
where   id_sup =6084 and id_hgis in (33) AND cntr_code like 'MD%'


-- 1160  Bernburg : DEE09 , DEE0C  :: ERREUR sur DEE0C
update lau_europe set province_code = null, province_name = null, id_sup = 1160 
where province_code = 'AU03' and id_sup =5689 and id_hgis =2 and nuts_code = 'DEE09' or nuts_code = 'DEE0C'
-- correction de DEE0C
update lau_europe set province_code = null, province_name = null, id_sup = 5141
-- select * from lau_europe
where   id_sup =1160 and id_hgis =23 and nuts_code = 'DEE0C'



-- Corriger Sweden
update lau_europe set province_code = null, province_name = null, id_sup = 8246 
where province_code = 'AU03' and id_sup =5689 and id_hgis =94 and nuts_code = 'SE213' 

-- Corriger le Portugal
update lau_europe set province_code = null, province_name = null, id_sup = 3852 
where province_code = 'AU03' and id_sup =5689 and cntr_code  = 'PT'
update lau_europe set province_code = null, province_name = null, id_sup = 3852 
where province_code = 'AU03' and id_sup =5689 and comm_id  in ('ES7006095', 'ES7006037')
-- Corriger l'Irlande (IE) e 1423
update lau_europe set province_code = null, province_name = null, id_sup = 1423 
where province_code = 'AU03' and id_sup =5689 and cntr_code  = 'IE'

-- Corriger la Grèce/Albanie
update lau_europe set province_code = null, province_name = null, id_sup = 6084 
where province_code = 'AU03' and id_sup =5689 and id_hgis = 26 and (cntr_code  = 'AL' or cntr_code  = 'EL')


-- correction de UA : encore beaucoup à restituer à la Pologne et la Russie
-- corriger en Pologne PL
update lau_europe set province_code = null, province_name = null, id_sup = 2750 
where comm_id = 'PL2002073' and id_hgis = 13
*/


-- En 1773 a lieu la première partition de la Pologne, bouffée en partie par la Russie, la Prusse et l'Autriche. 
-- Comme il y a deux autres partitions dans les années 1790, la carte de 1800 ne t'aidera pas. 
-- Je te joins une carte de cette région, il faut prendre celle de gauche pour tracer les frontières de la Pologne en 1789  

-- Pologne
-- l'unité 10 est deux fois en Pologne (Belz et )
update lau_europe set id_sup = 2750 
where  id_hgis = 10 and id_sup = 5141  and nuts_code = 'PL812';
update lau_europe set id_sup = 2750 
where  id_hgis = 10 and id_sup = 5141  and nuts_code = 'PL822';

-- La Galicia en Pologne : gros bazard à cheval avec l'Ukraine, qui va appartenir à l'Autriche (5689)
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('PL225', 'PL219', 'PL218', 'PL217');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('SK041');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('SK042');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('SK031');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('PL821', 'PL824');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  and nuts_code in ('PL823');

update lau_europe set id_sup = 5689 
where  id_sup = 2750  and comm_id in ('PL1213023', 'PL1213062', 'PL1213043', 'PL1213052', 'PL1213072', 'PL1213082', 'PL1213011', 'PL1218102', 'PL1218013', 'PL1218093', 
'PL1218082', 'PL1213093', 'PL1218062', 'PL1218082', 'PL1218093', 'PL1218052', 'PL1218072', 'PL1218042', 'PL1218033', 'PL2410032', 'PL1218022');
update lau_europe set id_sup = 5689 
where  id_sup = 2750  
and comm_id in ('PL1206113', 'PL1209073', 'PL1209042', 'PL1209082', 'PL1209022', 
'PL1209092', 'PL1209033', 'PL1206092', 'PL1206143', 'PL1209062', 'PL1209013', 'PL1209052',
'PL1219053', 'PL1219012', 'PL1219022', 'PL1201052', 'PL1201082', 'PL1201092',
'PL1219043', 'PL1219032', 'PL1201022', 'PL1201011', 'PL1201063', 'PL1201042', 'PL1201032', 'PL1201072')

update lau_europe set id_sup = 5689 
where  id_sup = 2750 and comm_id in ('UA120338','UA120320', 'UA120328', 'UA120325', 'UA120317')
-- doute vers Brody, Busk, Lwow
-- frontières avec Krakow ok, le long du fleuve.

-- La Russie gagne aussi du terrain sur la mer Noire en 1774, après une guerre contre l'Empire ottoman et en 1783:
-- la Géorgie est annexée en 1783 ainsi que la Crimée donc tout ce qui ici sur cette carte est indiqué acquis en 1774 + "crimée indépendante" et Kouban, annexés en 1783 doit être indiqué dans ta carte comme "Russie"
--Khersons'ka 3155
--Zaporiz'ka 3171
--Donets'ka 3152
--Kharkivs'ka 3154
-- Gain sur l'Empire Ottoman (6084)
select * from LAU_europe e, ukraine u 
where u.adm1_code in (3155, 3171, 3152, 3154) and st_intersects(u.geom3857, e.wkb_geometry)
and e.id_sup = 6084

update LAU_europe e set id_sup = 2344
from ukraine u 
where u.adm1_code in (3155, 3171, 3152, 3154) and st_intersects(u.geom3857, e.wkb_geometry)
and e.id_sup = 6084
-- 46

-- Importer la crimée indépendante d'aujourd'hui
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\crimea_9sr\crimea_9sr.shp
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v6 password=postgres schemas=ports" C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\World_borders\crimea_9sr\crimea_9sr.shp -a_srs EPSG:4326 -nln Crimee_2020 -nlt MULTIPOLYGON
alter table crimee_2020 add geom3857 geometry
update crimee_2020 set geom3857 = st_setsrid(st_transform(wkb_geometry , 3857), 3857)

-- elle appartient à la Russie (1783) - Gain sur l'Empire Ottoman (6084)
update LAU_europe e set id_sup = 2344
from crimee_2020 c
where  st_intersects(c.geom3857, e.wkb_geometry)
and e.id_sup = 6084

 -- Une petite partie de Mykolayivs'ka (3162)
 update LAU_europe e set id_sup = 2344 
 where comm_id = 'UA130630' and e.id_sup = 6084

 -- UA130687 sera à découper par le Fleuve (TODO)
  update LAU_europe e set id_sup = 2344 
 where comm_id = 'UA130687' and e.id_sup = 6084
 
 

-- Rajouter Ruthenia (28) 
update lau_europe set id_sup = 5689 
where  id_sup = 2750 and id_hgis = 28 and cntr_code in ('PL', 'UA')
update lau_europe set id_sup = 5689 
where  id_sup = 2750 and comm_id in ('UA180404', 'UA180406')




-- -- du coup, La Prusse ca donne un truc uni sur la Baltique et pas deux morceaux '
-- Prusse : 5141
/*
update lau_europe set province_code = null, province_name = null, id_sup = 5141 
where  id_hgis = 94 and province_code = 'AU03' and id_sup =5689 and cntr_code = 'PL'
*/

update lau_europe set province_code = null, province_name = null, id_sup = 5141 
where  id_hgis = 94 and id_sup = 2750  and cntr_code = 'PL'


-- Silesia : 61 (e 5689 avant)
-- la Silésie (Leipzig) passe à la Prusse (était autrichienne) en 1740

update lau_europe set id_sup = 5141 
where  id_hgis = 61  and id_sup =5689 and cntr_code = 'PL'
-- and cntr_code = 'PL'

-- En Pomeralia (36), la commune Gdansk reste polonaise (2750), mais le reste de la Pomeralia devient Prusse
update lau_europe set  id_sup = 5141 
where  id_hgis = 36 and id_sup = 2750 and cntr_code = 'PL' and comm_id != 'PL2261011'

-- finalement Gdansk reste une ville libre
update lau_europe set  id_sup = 100004 
where   comm_id = 'PL2261011'

-- Podnan (35), Butow, Pomerania , Warmia, Malbork : 35, 10, 5, 35, 94, 14 
update lau_europe set  id_sup = 5141 
where  id_hgis in (35, 10, 5, 35, 94, 14) and id_sup = 2750 and cntr_code = 'PL' 


-- Venice
update lau_europe set  id_sup = 6084 
where  nuts_code in ('EL621', 'EL623', 'EL624', 'EL632', 'EL633', 'EL651', 'EL652', 'EL653', 'EL307') and id_sup = 2490 and cntr_code = 'EL' 
-- commune de Tinou 
update lau_europe set  id_sup = 6084 
where  comm_id = 'GR06127001' and id_sup = 2490 and cntr_code = 'EL' 

-- San Marino en Italie devrait être dans les Papal States (TODO)
-- IT311041060
update lau_europe set  id_sup = 6084 
where  comm_id = 'GR06127001' and id_sup = 2490 and cntr_code = 'EL' 

-- En mer Baltique, Ingria, Livonie et Estonie et les îles en face deviennent russes en 1721 (ce qui est en vert clair ici)
-- Estonie
update lau_europe set  id_sup = 2344
where id_sup=8246 and id_hgis in (86, 71, 207) and cntr_code = 'EE'

-- Livonie (Latvia - LV)
update lau_europe set  id_sup = 2344
where id_sup=8246 and id_hgis = 207 and cntr_code = 'LV'

-- L'espace germanique entre 1700 et 1789 ne bouge pas tant que ça: c'est une entité composée en gros par 350 Etats différents et ça reste extrêmement morcelé jusqu'en 1803:

-- Ecosse: acte d'union en 1707 (était déjà guvernée par le roi d'Angleterre mais après 1707 il n'y a plus deux royaumes distincts avec un seul roi, mais un seul royaume
update lau_europe set  province_code = 'UK02', province_name = 'Ecosse', id_sup=1423
where id_sup = 2165

update LAU_europe e set province_code = 'IE', province_name = 'Irlande'
where id_sup = 1423 and (cntr_code = 'IE' or nuts_code  in ('UKN0A','UKN0B','UKN0C','UKN0D','UKN0E','UKN0F', 'UKN0G','UKN06','UKN07','UKN08','UKN09')) 

update LAU_europe e set province_code = 'WL', province_name = 'Pays de Galles'
where id_sup = 1423 and (cntr_code = 'UK' and id_hgis  in (150,247,147,183,145,250,152,138,139,137,141,140,142)) 

update LAU_europe e set province_code = 'WL', province_name = 'Pays de Galles' where comm_id in ('GBW05000991', 'GBW05000984')
update LAU_europe e set province_code = 'UK02', province_name = 'Ecosse' where comm_id in ('GBS13002533', 'GBS13002532', 'GBS13002534')


-- https://fr.wikipedia.org/wiki/%C3%8Ele_de_Man#P%C3%A9riode_britannique
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_multi(geom3857), iso2, 'île de Man' from world_borders_dump where pkid = 306 

-- Jersey, Guernesey  :  Iles anglo-normandes
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(name_latn, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select 'Iles anglo-normandes', 'JE-GG',  6, 6, 1423, st_union(geom3857), 'JE-GG', 'Iles anglo-normandes' 
from world_borders_dump where id in (244, 243)

-- select sourcegeom from LAU_europe where province_code in ('JE-GG', 'IM')

----------------------------------------------------------------------------
--- Importer dans LAU les parties qui me manquent pour faire le monde
----------------------------------------------------------------------------

create table ports.world_borders_dump as (
SELECT wb.id, wb.name, wb.iso2, wb.region, wb.subregion, 
	st_setsrid(st_transform(st_setsrid(st_makepoint(lon, lat), 4326) , 3857), 3857) as capitale,
      (ST_Dump(wb.mpolygone3857)).geom AS geom3857
FROM ports.world_borders wb )

alter table ports.world_borders_dump add pkid serial;



delete from lau_europe e where methodg = 6


-- Belarus (appartient à la Pologne 2750) 
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'BY' as cntr_id,  name as name_latn, iso2 as cntr_code, 'BY'||id::text , 6, 2750, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 2738

-- Russsie dans la Prusse
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'RU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'RU'||id::text , 6, 5141, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 1609

-- Georgia (appartient à la Russie 2344)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'GE' as cntr_id,  name as name_latn, iso2 as cntr_code, 'GE'||id::text , 6, 2344, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 2292

-- La Russie de Moscou
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'RU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'RU'||id::text , 6, 2344, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 1741

select * from LAU_europe, port_points pp where cntr_id = 'RU' and pp.toponyme_standard_fr = 'Russie' and st_contains(wkb_geometry, pp.point3857)
update LAU_europe set wkb_geometry = st_multi(st_buffer(wkb_geometry, 2000)) where ogc_fid = 123716
-- select * from LAU_europe where ogc_fid = 123716

insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, fid, methodg, id_sup, wkb_geometry)
select 'RU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'RU'||id::text , 'RU'||pkid::text , 6, 2344, ST_Multi(geom3857)  
from world_borders_dump w where pkid NOT in (1741, 1609)   AND name like 'Russia' AND st_isvalid(geom3857)
-- 225


-- Bosnia and Herzegovina e Ottoman (6084)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'BA' as cntr_id,  name as name_latn, iso2 as cntr_code, 'BA'||id::text , 6, 6084, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 312

-- Montenegro e Ottoman (6084)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'ME' as cntr_id,  name as name_latn, iso2 as cntr_code, 'ME'||id::text , 6, 6084, ST_Multi(st_buffer(geom3857, 3000))  from world_borders_dump w where pkid = 3247

update LAU_europe set sourcegeom = 'world borders' where cntr_id in ('BY', 'RU', 'GE', 'RU', 'BA', 'ME')
-- select cntr_id, sourcegeom, * from LAU_europe where cntr_id in ('BY', 'RU', 'GE', 'RU', 'BA', 'ME')


-- Haiti (saint-Domingue) e France (1225)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, methodnum , province_code , province_name , wkb_geometry)
select 'HT' as cntr_id,  name as name_latn, iso2 as cntr_code, 'HT'||id::text , 6, 1225, 6, 'HT', 'Saint-Domingue', ST_Multi(geom3857)  from world_borders_dump w where name = 'Haiti'

--- Sainte-Lucie : en France
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry)
select 'LC', 'Sainte-Lucie', iso2, 'LC'||id::text,  6, 6, 1225, ST_Multi(geom3857) from world_borders_dump where pkid = 305
update LAU_europe set province_code='LC' , province_name='Sainte-Lucie' where cntr_id = 'LC' and id_sup=1225;

-- Tobago : en France
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1225, ST_Multi(geom3857), iso2, 'Tobago' from world_borders_dump where pkid = 887
update LAU_europe set province_code = 'TO' where cntr_id='TT' and province_name='Tobago';

-- Trinité (TT) : en Espagne
-- https://fr.wikipedia.org/wiki/Trinit%C3%A9_(%C3%AEle)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1122, ST_Multi(geom3857), iso2, 'Trinité' from world_borders_dump where pkid = 886

-- République dominicaine (DO) : en Espagne
-- https://fr.wikipedia.org/wiki/R%C3%A9publique_dominicaine
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1122, ST_Union(geom3857), iso2, 'République dominicaine' from world_borders_dump where id = 48 group by iso2, name, id

-- Puerto-Rico (PR) : en Espagne
-- https://fr.wikipedia.org/wiki/Porto_Rico
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1122, ST_Union(geom3857), iso2, 'Porto-Rico' from world_borders_dump where id = 174 group by iso2, name, id

-- Sainte-Croix 731 , Saint-Thomas 733 (iles vierges américaines depuis 1917) et une sans nom (732)
-- dans le Danemark en 1789 : 892
-- https://fr.wikipedia.org/wiki/Saint-Thomas_(%C3%AEles_Vierges_des_%C3%89tats-Unis)
-- https://fr.wikipedia.org/wiki/Sainte-Croix_(%C3%AEles_Vierges_des_%C3%89tats-Unis)
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, 'Sainte-Croix', iso2, iso2||id::text,  6, 6, 892, ST_Multi(geom3857), iso2, 'îles vierges' from world_borders_dump where pkid = 731

insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, 'Saint-Thomas', iso2, iso2||id::text,  6, 6, 892, ST_Multi(geom3857), iso2, 'îles vierges' from world_borders_dump where pkid = 733

insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, 'Saint-Jean', iso2, iso2||id::text,  6, 6, 892, ST_Multi(geom3857), iso2, 'îles vierges' from world_borders_dump where pkid = 732


-- https://fr.wikipedia.org/wiki/Antigua-et-Barbuda
--  Antigua-et-Barbuda e Anglais (1423) et font partie des colonies anglaises en Amérique
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, name from world_borders_dump where id = 1 group by iso2, name, id

-- https://fr.wikipedia.org/wiki/Saint-Christophe-et-Ni%C3%A9v%C3%A8s
-- Appartient à la Grande Bretagne depuis 1713
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, 'îles du Vent' from world_borders_dump where id = 178 group by iso2, name, id

-- https://fr.wikipedia.org/wiki/Saint-Vincent-et-les-Grenadines
-- Appartient à la Grande Bretagne depuis 1783
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, 'Saint-Vincent-et-les-Grenadines' from world_borders_dump where id = 213 group by iso2, name, id

-- https://en.wikipedia.org/wiki/Grenada#British_colonial_period
-- https://fr.wikipedia.org/wiki/Grenade_(pays)
-- Appartient à la Grande Bretagne depuis 1783 grenada, 70
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), iso2, 'La Grenade' from world_borders_dump where id = 70 group by iso2, name, id

-- https://fr.wikipedia.org/wiki/Barbade
-- https://fr.wikipedia.org/wiki/Histoire_de_la_Barbade
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_multi(geom3857), iso2, 'La Barbade' from world_borders_dump where id = 11 

-- https://fr.wikipedia.org/wiki/%C3%8Ele_de_Man#P%C3%A9riode_britannique
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_multi(geom3857), iso2, 'île de Man' from world_borders_dump where pkid = 306 

-- Jersey, Guernesey  :  Iles anglo-normandes
-- Appartient à la Grande Bretagne  1423
insert into LAU_europe(name_latn, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select 'Iles anglo-normandes', 'JE-GG',  6, 6, 1423, st_union(geom3857), 'JE-GG', 'Iles anglo-normandes' 
from world_borders_dump where id in (244, 243)


-- Terre-Neuve e GB en grande partie (1423) sauf St pierre et Miquelon (france, 1225) 
-- Mais à refaire manuellement
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), 'TN', 'Terre-Neuve' from world_borders_dump 
where pkid in (986, 937,941,944,940,939,951,954, 956, 957, 958, 978, 983, 984, 988) 
group by iso2, name, id

-- Saint pierre et miquelon
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1225, st_union(geom3857), 'PM', 'Saint-Pierre-et-Miquelon' from world_borders_dump 
where id = 233 and iso2 = 'PM' group by iso2, name, id

--https://fr.wikipedia.org/wiki/Canada#Colonies_britanniques_des_Maritimes
-- Reste du Canada est anglais --> 1789 au moins
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, methodnum, id_sup, wkb_geometry, province_code , province_name)
select iso2, name, iso2, iso2||id::text,  6, 6, 1423, st_union(geom3857), 'CA', 'Canada' from world_borders_dump 
where id = 24 and pkid not in (986, 937,941,944,940,939,951,954, 956, 957, 958, 978, 983, 984, 988)  group by iso2, name, id


-- Ile Maurice
-- https://fr.wikipedia.org/wiki/%C3%8Ele_Maurice#Histoire
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry)
select 'MU' as cntr_id,  name as name_latn, iso2 as cntr_code, 'MU'||id::text , 6, 1225, ST_Multi(geom3857)  from world_borders_dump w where pkid = 814
update LAU_europe set sourcegeom = 'world borders', province_code = 'MU', province_name = 'Île Maurice' where cntr_id = 'MU' and name_latn = 'Mauritius'

-- Malte
insert into LAU_europe(cntr_id, name_latn, cntr_code, nuts_code, methodg, id_sup, wkb_geometry, sourcegeom)
select 'MT' as cntr_id,  name as name_latn, iso2 as cntr_code, 'MU'||id::text , 6, 100009, ST_union(geom3857), 'world borders'  
from world_borders_dump w where id = 117 group by iso2, name, id

alter table LAU_europe add sourcegeom text default 'LAU_europe';

select * from LAU_europe where methodg = 6 and province_code is not null
update LAU_europe set sourcegeom = 'world borders' where methodg = 6 and province_code is not null

-----------
-- Corrections Italie : Oneglia et Loano sont des enclaves
-- pas au piemont 4705 mais à Genes 1345
UPDATE LAU_europe SET id_sup = 1345 WHERE comm_id IN (
'IT107008037', 'IT107008034', 'IT107008023', 'IT107008046', 'IT107008004', 'IT107008042',
'IT107008066', 'IT107008018', 'IT107008012', 'IT107008005', 'IT107008049', 'IT107008010',
'IT107008019', 'IT107008020', 'IT107008033', 'IT107008010', 'IT107008006', 'IT107008054', 'IT107008024', 'IT107008021') AND id_sup = 4705

-- au piemont 4705 en plus et pas à genes 1345 + Loano
UPDATE LAU_europe SET id_sup = 4705 WHERE comm_id IN (
'IT107008010', 'IT107008041', 'IT107008067', 'IT107008017', 'IT107008013', 'IT107009034') AND id_sup = 1345


-- Corrections Italie : Lucca
--IT309046015 Gallicano
--IT309046010 Castiglione di Garfagnana
--IT309046019 Minucciano
--IT309045011 Montignoso
UPDATE LAU_europe SET id_sup = 4974, id_hgis = 1 WHERE comm_id IN (
'IT309046015', 'IT309046010', 'IT309046019', 'IT309045011') -- AND id_sup = 5262 (Modena)

-- Corrections Italie : Modene (5562) et pas Toscane(4973)
UPDATE LAU_europe SET id_sup = 5562, id_hgis = 1 WHERE comm_id IN (
'IT309045009')
-- IT309045009 Licciana Nardi pour Mallapina-Villafranca

-- Sardinia (Piemont 4705)
--IT309045015 Tresana
--IT309045013 Podenzana
--IT309045001 Aulla
--IT309045008 Fosdinovo
UPDATE LAU_europe SET id_sup = 4705, id_hgis = 4 WHERE comm_id IN (
'IT309045015', 'IT309045013', 'IT309045001', 'IT309045008') 

-- Toscane
--IT309045002 Bagnone
--IT107011008 Calice al Cornoviglio
--IT309045016 Villafranca in Lunigiana pour Bagnone
-- IT309046024 Pietrasanta
-- IT309046030 Stazzema
-- IT309046013 Forte dei Marmi
-- IT309046028 Seravezza
-- IT309046003 barga
UPDATE LAU_europe SET id_sup = 4973, id_hgis = 3 WHERE comm_id IN (
'IT309045002', 'IT107011008', 'IT309045016', 'IT309046024', 'IT309046030', 'IT309046013', 'IT309046028', 'IT309046003') 

-- Genes
--IT107011011 Castelnuovo Magra
--IT107011020 Ortonovo
UPDATE LAU_europe SET id_sup = 1345, id_hgis = 4 WHERE comm_id IN (
'IT107011011', 'IT107011020') 

--- Monaco
--FR93062150 La Turbie
--FR93062067 Gorbio
--IT107008065 Ventimiglia
--FR93062012 Beausoleil
--Sainte-Agnès : ok
--Roquebrune : ok
--Monti : pas vu (dans Ventimiglia ?)
--Garavan : pas vu (dans Ventimiglia ?)
--Castellar : ok
UPDATE LAU_europe SET id_sup = 9347, id_hgis = 1, methodg = 6, methodnum=6 WHERE comm_id IN (
'FR93062150', 'FR93062067', 'IT107008065', 'FR93062012') 


-- Mantou/venice (2490) /papal states (2140)
--IT208038002 Berra
--IT205029039 Porto Tole
--IT205029046 Taglio di po
--IT205029002 Ariano nel Polesine
--IT205029017 Corbola
UPDATE LAU_europe SET id_sup = 2490, id_hgis = 120, methodg = 6, methodnum=6 WHERE comm_id IN (
'IT208038002', 'IT205029039', 'IT205029046', 'IT205029002', 'IT205029017') 

-- Venice (2490) / Autriche (5689)
UPDATE LAU_europe SET id_sup = 2490, id_hgis = 120, methodg = 6, methodnum=6 WHERE comm_id IN (
'IT206031009', 'IT206031012', 'IT206031023', 'IT206030134', 'IT206030056') 
--IT206031009 Grado e Venice (2490)
--IT206031012 Monfalcone e Venice (2490)
--IT206031023 Staranzano e Venice (2490)
--IT206030134 Villa Vicentina e Venice (2490)
--IT206030056 Marano Lagunare e Venice (2490)

UPDATE LAU_europe SET id_sup = 5689, id_hgis = 77, methodg = 6, methodnum=6 WHERE comm_id IN (
'IT206030004', 'IT206030120', 'IT206030018',  'IT206030082') 
--IT206030004 Aquileia e Autriche (5689)
--IT206030120 Terzo d'Aquileia e Autriche (5689)
--IT206030018 Carlino e Autriche (5689)
---- IT206030069 Palazzolo dello Stella
--IT206030082 Precenicco e Autriche (5689)


-- Oldenburg num 111 n'appartient pas au Danemark (892), il est indépendant 
UPDATE LAU_europe SET id_sup = 111, methodg = 6, methodnum=6  where id_sup = 892 and id_hgis = 111 and cntr_code  = 'DE'
select * from LAU_europe where id_sup = 892 and id_hgis = 111 and cntr_code  = 'DE'

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 111
group by id_sup


-- Swhleswig-Holstein e Danemark
UPDATE LAU_europe SET id_sup = 892, methodg = 6, methodnum=6  where id_sup = 6788 and id_hgis in (17, 18) 
-- and cntr_code  = 'DE'
-- 299
UPDATE LAU_europe SET id_sup = 7708, methodg = 6, methodnum=6  where id_sup = 8246 and id_hgis = 165 and cntr_code  = 'DE'

select * from lau_europe le where id_hgis = 6788
select * from lau_europe le where id_sup = 6788

select * from world_1789 w2 where id = 43
delete from world_1789 w2 where id = 43

-- bengale : 1765 e Angleterre(1423) depuis 1765
-- https://fr.wikipedia.org/wiki/Inde#P%C3%A9riode_coloniale

-- Vietnam 
-- https://fr.wikipedia.org/wiki/Vi%C3%AAt_Nam#Histoire

-- Pondichéry : francais (1225) au XVIII
-- https://fr.wikipedia.org/wiki/Pondich%C3%A9ry#Colonisation_fran%C3%A7aise_en_Inde

-- Amérique du sud
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)

-- delete from ports.world_1789 where unit_code = 'Amerique-du-Sud'
-- delete from ports.world_1789 where unit_code ='Sud des USA (Mexique et Texas)'

-- rajouter les etats du sud des états unis (Californie, Nevada, Utah, Arizona, ) au Mexique (100007) 
-- Nouveau mexique et Texax)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 1 as unitlevel, 100007, name, 'usa', 'usa',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'CA', 'NV', 'UT', 'AZ') 

--insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
--select 100007, 0 as unitlevel, -1, name, 'world borders', 'world borders', st_union(geom3857 )
--from world_borders_dump e where id= 120 group by name

--- Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 1 as unitlevel, 100007, name, 'world borders', 'world borders', st_union(geom3857 )
from world_borders_dump e where id= 120 group by name

-- Etendre les limites du MExique
insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 'MX', 0, -1, 'Mexique', 'world borders + usa', 'world borders + usa', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100007 and unitlevel=1
-- delete from ports.world_1789  where id = 1312

-- Texas indépendant avec Nouveau Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 1 as unitlevel, 100008, name, 'usa', 'usa',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'TX', 'NM') 

insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100008, 'TX', 0, -1, 'Texas', 'usa', 'usa', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100008 and unitlevel=1




/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1122, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('LA');
*/
-- Rajouter Hawai
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Hawai', 0 as unitlevel, -1, 'usa', 'usa', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where pkid in (3414, 3412, 3408, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396)

update ports.world_1789 set unit_id = 100010, shortname='Hawaï', sourcegeom='world borders', sourcecode='pkid' 
where  unit_code = 'Hawai';

-- Brésil e Portugal
-- https://fr.wikipedia.org/wiki/Br%C3%A9sil#Ind%C3%A9pendance_%C3%A0_l'%C3%A9gard_du_Portugal_et_empire
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'BR', 1 as unitlevel, 3852, 'Brésil - world borders', ' Brésil - world borders', st_union(geom3857)
from world_borders_dump e where id = 21*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'BR', 'BR'||id::text , 6, 6, 3852, st_union(geom3857), 'world borders', 'BR', 'Brésil'  
from world_borders_dump e where id = 21 group by name, id

-- Etendre le Portugal avec le Portugal
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 3852 or (unit_code in ('BR') and unit_sup_id = 3852) 
) as k where unit_id = 3852


--- Afrique

-- Distinguer Maroc (indépendant), Algérie, Tunisie, Egypte du reste e Ottoman (6084)
-- 141 Réunion, 170 Mayotte, ile Maurice
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814
-- delete from ports.world_1789 where unit_code = 'Afrique'

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'MA', 0 as unitlevel, -1, 'Marocco - world borders', 'Marocco - world borders',  st_union(geom3857)
from world_borders_dump e where id = 114

update ports.world_1789 set unit_id = 100006 where unit_code = 'MA' and unitlevel = 0

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'DZ', 'DZ'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'DZ', 'Algérie'  
from world_borders_dump e where id = 2 group by name, id

/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'DZ', 1 as unitlevel, 6084, 'Algérie - world borders', 'Algérie - world borders',  st_union(geom3857)
from world_borders_dump e where id = 2*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'TN', 'TN'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'TN', 'Tunisie'  
from world_borders_dump e where id = 201 group by name, id

/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'TN', 1 as unitlevel, 6084, 'Tunisie - world borders', 'Tunisie - world borders',  st_union(geom3857)
from world_borders_dump e where id = 201
*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'LY', 'LY'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'LY', 'Tripoli'  
from world_borders_dump e where id = 107 group by name, id

/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'LY', 1 as unitlevel, 6084, 'Lybie - world borders', 'Libyan Arab Jamahiriya - world borders',  st_union(geom3857)
from world_borders_dump e where id = 107
*/
/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'EG', 1 as unitlevel, 6084, 'Egypte - world borders', 'Egypt - world borders',  st_union(geom3857)
from world_borders_dump e where id = 50
*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'EG', 'EG'||id::text , 6, 6, 6084, st_multi(st_buffer(st_union(geom3857), 3000)), 'world borders', 'EG', 'Egypte'  
from world_borders_dump e where id = 50 group by name, id

-- Etendre l'empire Ottoman avec le maghreb
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 6084 or (unit_code in ('DZ', 'TN', 'LY', 'EG') and unit_sup_id = 6084) 
) as k where unit_id = 6084

-- Le reste Asie + Moyen-Orient 145 + Océanie 54, 53 + Taiwan (0) / retirer la Turquie (202)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 0 as unitlevel, -1, 'Asie + Moyen-Orient + Océanie - world borders', 'Asie + Moyen-Orient + Océanie  - world borders',  st_unaryunion(st_collect(geom3857))
from world_borders_dump e where subregion in (145, 54, 53, 30, 34, 35, 143, 0) and id<>202

-- delete from ports.world_1789  where unit_code = 'WWW'


-----------------------------
-- Fabriquer les frontières Européennes et du monde
-----------------------------



-- drop  table ports.world_1789 
create table ports.world_1789 
(
id serial,
unit text,
unit_id int,
unit_code text,
unitlevel int, --0 pour etat, 1 pour sous-unité
unit_sup_id int,
unit_sup_code text,
sourcegeom text,
sourcecode text,
geom3857 geometry
)


insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e
group by id_sup
-- 89 (pas 92)

-- mettre à jour en Danemark (892), GB (1423), ES (1122), Piemont(4705) , genes 1345

update world_1789 set geom3857 = k.geom
from (select ST_union(e.wkb_geometry) as geom
from LAU_europe e where id_sup = 1225
group by id_sup) as k where unit_id = 1225

delete from ports.world_1789  where unitlevel = 1
-- 296

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null
group by id_sup, id_hgis
-- 667 (pas 275), 2min 21


-- Gerer id_hgis si null
select * from LAU_europe e where id_hgis =0
-- une commune en Islande
update LAU_europe set id_hgis = 0 where ogc_fid = 89011
update LAU_europe set province_name =  'Acores', province_code = 'PT200' where nuts_code = 'PT200';
update LAU_europe set province_name =  'Madere', province_code = 'PT300' where nuts_code = 'PT300';

--update LAU_europe e set id_hgis = null, province_name = 'Island', province_code = 'IS' where cntr_id = 'IS' and id_hgis = 0;
--update LAU_europe e set id_hgis = null, province_name = 'Faroe Islands', province_code = 'FO' where cntr_id = 'FO' and id_hgis = 0;
--update LAU_europe e set id_hgis = null, province_name = 'Greenland', province_code = 'GL' where cntr_id = 'GL' and id_hgis = 0;

select * from LAU_europe e where id_hgis is null
select * from LAU_europe where id_hgis is null and methodnum is null and nuts_code not in ('PT200', 'FRY10', 'FRY20', 'FRY30', 'FRY40', 'FRY50')

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e where province_code is not null
group by id_sup, province_code, province_name
-- 21, 53 s

select * from ports.world_1789 where unitlevel = 1 and unit_code is not null 
delete from ports.world_1789 where unitlevel = 1 and unit_code is not null 
update LAU_europe set province_name = 'Pays-Bas autrichiens' where province_code = 'AU03'
-- 834
-- https://fr.wikipedia.org/wiki/Saint-Marin#Histoire

-- Brême
update LAU_europe SET id_sup = 100003, id_hgis = NULL, province_code='BR', province_name='Brême'
WHERE comm_id = 'DE040011000001' 

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'commune de Brême - group by province_code', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and comm_id = 'DE040011000001'
group by province_code, province_name

insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 0 as unitlevel, 'LAU_europe', 'commune de Brême', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and comm_id = 'DE040011000001'
group by province_code, province_name

select * from LAU_europe where comm_id = 'DE040011000001'

delete from ports.world_1789 where unit_id = 8246
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 8246
group by id_sup

delete from ports.world_1789 where unit_id = 165 AND unitlevel = 1 AND id= 281

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Hambourg - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_hgis = 165 and id_sup = 8246
group by id_hgis


insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_collect(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and comm_id = 'DE040011000001'
group by province_code, province_name

select * from ports.world_1789 where unit_code = 'BR'
delete from ports.world_1789 where id = 384
delete from ports.world_1789 where unit_code = 'BR'

-- Hamburg
update LAU_europe SET id_sup = 15024
WHERE comm_id = 'DE020001000001' 


delete from ports.world_1789 where unit_id = 15024
delete from ports.world_1789 where unit_id = 892

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'commune de Hambourg - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 15024
group by id_sup

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Hambourg - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 15024
group by id_sup


insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where id_sup = 892
group by id_sup

-- Gdansk
delete from ports.world_1789  where unit_id = 2750 and unitlevel = 0
delete from ports.world_1789  where unit_sup_id = 2750 and unitlevel = 1

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup = 2750
group by id_sup
-- 1

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', 'commune de Gdansk - group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup = 100004
group by id_sup

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.id_sup = 2750
group by id_sup, id_hgis

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Gdansk - group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.id_sup = 100004
group by id_sup, id_hgis

-- lampedouse pas dans naples (100000)
-- IT519084020 - Lampedusa e Linosa
-- 100005
update LAU_europe e set id_hgis = 100005, id_sup = 100005 where id_sup = 100000 and comm_id = 'IT519084020';
delete from ports.world_1789 where unitlevel = 0 and unit_id = 100000;

insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (100000, 100005)
group by id_sup;

insert into ports.world_1789 (unit_id, unitlevel, sourcegeom, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 1 as unitlevel, 'LAU_europe', 'commune de Lampedusa - group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.id_sup = 100005
group by id_sup, id_hgis;
insert into ports.world_1789 (unit, unit_code, unitlevel, sourcegeom, sourcecode, geom3857)
select e.province_name as unit, e.province_code as unit_code, 1 as unitlevel, 'LAU_europe', 'group by province_code', ST_collect(e.wkb_geometry) as geom3857
from LAU_europe e where province_code is not null and e.id_sup = 100000
group by province_code, province_name;

-- Ost Friesland pas dans les Provinces Unies 4117
update LAU_europe e set id_hgis = 100009, id_sup = 100009 where id_sup = 4117 and id_hgis = 5 and cntr_code = 'DE';
delete from ports.world_1789 where unitlevel = 0 and unit_id = 4117;
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (100009)
group by id_sup;
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (4117)
group by id_sup;

-- Ost Friesland appartient à la Prusse ? OUI
-- https://fr.wikipedia.org/wiki/Frise_orientale, oui depuis 1744
update LAU_europe e set id_sup = 5141, province_name='Frise orientale', province_code='FRO' where id_hgis = 100009
delete from ports.world_1789 where unitlevel = 0 and unit_id in (5141, 100009);
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select e.id_sup as unit_id, 0 as unitlevel, 'LAU_europe', ' group by id_sup', ST_union(e.wkb_geometry) as geom3857
from LAU_europe e where e.id_sup in (5141)
group by id_sup;

--- USA
insert into ports.world_1789 (unit_id, unitlevel,sourcegeom, sourcecode, geom3857)
select 100001 as unit_id, 0 as unitlevel, 'usa', 'usa', st_setsrid(st_transform(ST_unaryunion(st_collect(e.wkb_geometry)), 3857), 3857) as geom3857
from usa e where  stusps in ('NC', 'SC', 'GA',  'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT')

-- not in ('PR', 'LA')

select 100001 as unit_id, 0 as unitlevel, 'usa', 'usa', st_setsrid(st_transform(st_union(e.wkb_geometry), 3857), 3857) as geom3857
from usa e where  stusps not in ('PR', 'LA')

delete from ports.world_1789 where unit_id = 100001
delete from ports.world_1789 where unit_code = 'others states in usa'
delete from ports.world_1789 where unit_sup_id = 100001

--insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id, sourcegeom, sourcecode, geom3857)
--select 'others states in usa' , 1 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(ST_unaryunion(st_collect(e.wkb_geometry)), 3857), 3857) as geom3857
--from usa e where stusps not in ('PR', 'LA', 'NC', 'SC', 'GA', 'LA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA')

-- Les états de la côte Est et les autres fondateurs
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('NC', 'SC', 'GA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT')

-- La Louisiane et la Floride sont espagnoles
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1122, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('LA');

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1122, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('FL');*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 1122, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', 'LA', name  
from usa e where stusps  in ('LA');
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 1122, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', 'FL', name  
from usa e where stusps  in ('FL');

-- L'Alaska est Russe
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 2344, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('AK');*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 2344, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', 'AK', name  
from usa e where stusps  in ('AK');

-- à L'ouest, beaucoup d'états anglais
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 1423, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR');
*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, stusps as cntr_code, stusps||geoid::text , 6, 6, 1423, st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 'usa', stusps, name  
from usa e where stusps  in ('WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR');

update LAU_europe set wkb_geometry = st_multi(st_buffer(wkb_geometry, 3000)) where province_code in ('WA', 'ID', 'MT', 'ND') and id_sup = 1423

-- Cuba e espagne
-- https://fr.wikipedia.org/wiki/Cuba#Histoire_coloniale
/*
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'CU', 1 as unitlevel, 1122, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=41 and name = 'Cuba'  ;*/


insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'CU', 'CU'||id::text , 6, 6, 1122, st_union(geom3857), 'world borders', 'CU', 'Cuba'  
from world_borders_dump e where id = 41 and name = 'Cuba' group by id, name


-- Etendre l'Espagne avec la Louisiane, Cuba, La Floride
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1122 or (unit_code in ('LA', 'CU', 'FL') and unit_sup_id = 1122) 
) as k where unit_id = 1122

-- Etendre la Russie avec l'Alaska
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 2344 or (unit_code in ('AK') and unit_sup_id = 2344) 
) as k where unit_id = 2344

-- Etendre l'Angleterre avec les états de l'Ouest
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1423 or (unit_sup_id = 1423 and 
unit_code in ('WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR')) 
) as k where unit_id = 1423

update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1423 or (unit_sup_id = 1423 and 
unit_code in ('IA')) 
) as k where unit_id = 1423

-- Jamaïque e angleterre
-- https://fr.wikipedia.org/wiki/Jama%C3%AFque#Histoire
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'JM', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=90 and name = 'Jamaica'  */

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'JM', 'JM'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'JM', 'Jamaïque'  
from world_borders_dump e where id = 90 and name = 'Jamaica' group by id, name


-- https://fr.wikipedia.org/wiki/Bahamas
-- Bahamas e angleterre
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'BS', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=13 and name = 'Bahamas'  */

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'BS', 'BS'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'BS', 'Bahamas'  
from world_borders_dump e where id = 13 and name = 'Bahamas' group by id, name


-- Ile Caiman & Turks e angleterre
-- https://fr.wikipedia.org/wiki/%C3%8Eles_Turques-et-Ca%C3%AFques
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'TC', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=236 and name = 'Turks and Caicos Islands'  */

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'TC', 'TC'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'TC', 'Turks and Caicos Islands'  
from world_borders_dump e where id = 236 and name = 'Turks and Caicos Islands' group by id, name


-- Cayman Islands e angleterre depuis 1670
-- https://fr.wikipedia.org/wiki/%C3%8Eles_Ca%C3%AFmans
/*insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'KY', 1 as unitlevel, 1423, 'world borders', 'world borders', st_union(geom3857)
from world_borders_dump e where id=34 and name = 'Cayman Islands'*/

insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'KY', 'KY'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'KY', 'Îles Caïman'  
from world_borders_dump e where id = 34 and name = 'Cayman Islands' group by id, name


-- Etendre l'angleterre avec ces provinces 
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unit_id = 1423 or (unit_code in ('JM', 'BS', 'TC', 'KY') and unit_sup_id = 1423) 
) as k where unit_id = 1423

-- https://fr.wikipedia.org/wiki/Louisiane#Colonisation_europ%C3%A9enne
select distinct etat, dfrom, dto, subunit, subunit_en from etats e2 where etat = 'Etats-Unis d''Amérique'
-- pas la Louisiane LA
select *, etat, dfrom, dto, subunit from etats e2 where subunit like 'Lousiana'
update  etats set subunit = 'Louisiane', subunit_en = 'Louisiana'  where subunit like 'Lousiana'

-- Cap vert e Portugal
-- https://fr.wikipedia.org/wiki/Cap-Vert#Du_XVe_%C3%A0_la_fin_du_XVIIIe_si%C3%A8cle
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'CV', 'CV'||id::text , 6, 6, 3852, st_multi(st_union(geom3857)), 'world borders', 'CV', 'Cap Vert'  
from world_borders_dump e where id = 42 and name = 'Cape Verde' group by id, name


-- Dominica e France
-- https://fr.wikipedia.org/wiki/Dominique_(pays)#Histoire
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'DM', 'DM'||id::text , 6, 6, 1225, st_multi(st_union(geom3857)), 'world borders', 'DM', 'Dominique'  
from world_borders_dump e where id = 47 and name = 'Dominica' group by id, name

-- Montserrat e Angleterre
-- https://fr.wikipedia.org/wiki/Montserrat_(Antilles)#Colonisation_europ%C3%A9enne
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'MS', 'MS'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'MS', 'Montserrat'  
from world_borders_dump e where id = 111 and name = 'Montserrat' group by id, name


insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'VG', 'VG'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'VG', 'British Virgin Islands'  
from world_borders_dump e where id = 215 and name = 'British Virgin Islands' group by id, name

-- Anguilla e Angleterre
-- https://fr.wikipedia.org/wiki/Anguilla#Colonisation_britannique
insert into LAU_europe(name_latn, cntr_code, nuts_code, methodg, methodnum , id_sup, wkb_geometry, sourcegeom, province_code, province_name)
select  name as name_latn, 'AI', 'AI'||id::text , 6, 6, 1423, st_multi(st_union(geom3857)), 'world borders', 'AI', 'Anguilla'  
from world_borders_dump e where id = 128 and name = 'Anguilla' group by id, name


-----------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------

-- créer les noms des provinces, et des pays
alter table ports.world_1789 add shortname text;
alter table ports.world_1789 add longname text;

update ports.world_1789 w set shortname = g.short_name , longname = g.long_name 
from gis_1700_division2 g
where g.num = w.unit_id and g.owner_id = w.unit_sup_id and w.unitlevel = 2 and shortname is null
-- 526

update ports.world_1789 w set shortname = e.province_name , longname = e.province_name 
from lau_europe e where w.unit_code = e.province_code and w.unitlevel = 2 and shortname is null
-- 22
UPDATE ports.world_1789 w set unit_id = 100003 where w.unit_code = 'BR' and w.unitlevel = 0;

UPDATE ports.world_1789 w set shortname = 'Autriche' where w.unit_id=5689 ;
UPDATE ports.world_1789 w set shortname = 'Brême' where w.unit_id = 100003 ;
UPDATE ports.world_1789 w set shortname = 'Dantzig' where w.unit_id = 100004 ;
UPDATE ports.world_1789 w set shortname = 'Hambourg' where w.unit_id=15024 ;
UPDATE ports.world_1789 w set shortname = 'Lubeck' where w.unit_id=15011 ;
UPDATE ports.world_1789 w set shortname = 'Grande-Bretagne' where w.unit_id=1423 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'France' where w.unit_id=1225 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Espagne' where w.unit_id=1122 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Empire ottoman' where w.unit_id=6084 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Duché de Massa et Carrare' where w.unit_id=15058 ;
UPDATE ports.world_1789 w set shortname = 'Duché de Mecklenbourg' where w.unit_id=4918 ;
UPDATE ports.world_1789 w set shortname = 'Danemark' where w.unit_id=892 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Monaco' where w.unit_id=9347 ;
UPDATE ports.world_1789 w set shortname = 'Pologne' where w.unit_id=2750 ;
UPDATE ports.world_1789 w set shortname = 'Portugal' where w.unit_id=3852 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Prusse' where w.unit_id=5141 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Principauté de Piombino' where w.unit_id=8797 ;
UPDATE ports.world_1789 w set shortname = 'République de Gènes' where w.unit_id=1345 ;
UPDATE ports.world_1789 w set shortname = 'République de Raguse' where w.unit_id=3437 ;
UPDATE ports.world_1789 w set shortname = 'Etats pontificaux' where w.unit_id=2140  ;
UPDATE ports.world_1789 w set shortname = 'République de Venise' where w.unit_id=2490 ;
UPDATE ports.world_1789 w set shortname = 'Royaume de Naples' where w.unit_id=100000 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Royaume de Piémont-Sardaigne' where w.unit_id=4705 ;
UPDATE ports.world_1789 w set shortname = 'Russie' where w.unit_id=2344 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Suède' where w.unit_id=8246 and shortname is  null;
UPDATE ports.world_1789 w set shortname = 'Toscane' where w.unit_id=4973 ;
UPDATE ports.world_1789 w set shortname = 'République de Lucques' where w.unit_id=4974;
UPDATE ports.world_1789 w set shortname = 'principauté de Lampédouse' where w.unit_id=100005 ;
UPDATE ports.world_1789 w set shortname = 'Provinces-Unies' where w.unit_id=4117 ;
UPDATE ports.world_1789 w set shortname = 'Etats-Unis d''Amérique' where w.unit_id=100001 ;
UPDATE ports.world_1789 w set shortname =  'Duché d''Oldenbourg'	where w.unit_id=111 ;
UPDATE ports.world_1789 w set shortname =  'Danemark'	where w.unit_id=892 and shortname is  null;
UPDATE ports.world_1789 w set shortname =  'Empire du Maroc'	where w.unit_id=100006 ;

UPDATE ports.world_1789 w set shortname = 'Malte' where w.unit_id=100009 ;
update ports.world_1789 w set shortname = 'reste Afrique' where unit_code='Afrique' ;
update ports.world_1789 w set shortname = 'reste Amérique-du-Sud' where unit_code='Amerique-du-Sud' ;
update ports.world_1789 w set shortname = 'reste Asie, Moyen-Orient et Océanie' where unit_code='WWW' ;
update ports.world_1789 w set shortname = 'Mexique' where w.unit_id=100007 ;
update ports.world_1789 w set shortname = 'Texas indépendant' where w.unit_id=100008 ;
update ports.world_1789 w set shortname = 'Hawaï' where w.unit_id=100010 ;
update ports.world_1789 w set shortname = 'Svalbard' where w.unit_id=100011 ;

select unit_id, g.short_name , g.long_name 
from ports.world_1789 w, gis_1700_sovereign_states g 
where w.unitlevel = 0 and g.owner_id = w.unit_id and shortname is null; 
-- 38

UPDATE ports.world_1789 w set shortname = g.short_name
from gis_1700_sovereign_states g 
where w.unitlevel = 0 and g.owner_id = w.unit_id and shortname is null; 
-- 62




UPDATE ports.world_1789 w set shortname = g.short_name
from gis_1700_sovereign_states g 
where w.unitlevel = 1 and g.owner_id = w.unit_id and shortname is null; 
-- 62

select * from ports.world_1789 w where shortname is not null and unitlevel = 1;
100007	Mexique

--------------------------------------------------------------------------------------------------------
SELECT * FROM etats WHERE etat = 'Royaume uni des Pays-Bas'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume de Hollande'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République de Sept-Îles'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume de Bonny'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République romaine'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République ligurienne'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume d''Étrurie'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'Royaume d''Italie'
SELECT country2019_name , etat, dfrom, dto FROM etats WHERE etat = 'République batave'

SELECT country2019_name , etat, subunit, dfrom, dto FROM etats WHERE etat = 'Duché d''Oldenbourg' -- dfrom 1773
SELECT country2019_name , etat, subunit, dfrom, dto FROM etats WHERE etat = 'Duché de Courlande' -- dto : 1795


------------------------------------------------------------------------------------------------------------

create table LAU_europe_backup as (select * from LAU_europe);
create table world_1789_backup as (select * from world_1789);

drop table world_1789_backup ;
drop table lau_europe_backup ;


drop table world_1789 

CREATE TABLE ports.world_1789 (
	id serial NOT NULL,
	unit_id int4 NULL, -- either this one is filled
	unit_code text NULL, -- either this one is filled
	unitlevel int4 NULL, -- 0 for state,  1 for substate, 2 for province
	unit_sup_id int4 NULL, -- -1 if state, else the unit_id of the belonging state
	sourcegeom text NULL,
	sourcecode text NULL,
	geom3857 geometry NULL,
	shortname text NULL, -- name of the state, substate or province, as given by Silvia, in French, or from other sources in English
	longname text null
);

select id, shortname, unit, unit_id, unit_code, unitlevel, unit_sup_id, unit_sup_code, sourcegeom,sourcecode 
from world_1789 w2 where unitlevel = 0 order by unit_id 
 
/*
select id, shortname, unit, unit_id, unit_code, unitlevel, unit_sup_id, unit_sup_code, sourcegeom,sourcecode 
from world_1789 w2 where unitlevel = 1 order by unit_sup_id 
*/



/*update world_1789 set geom3857 = k.geom
from (select ST_union(e.wkb_geometry) as geom
from LAU_europe e where id_sup = 1225
group by id_sup) as k where unit_id = 1225*/


select count(*) from LAU_europe where  province_code is not null
select count(*) from LAU_europe where  province_code is  null and id_hgis is null
select * from LAU_europe where  province_code is  null and id_hgis is null order by name_latn 



insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id)
select e.id_hgis as unit_id, 2 as unitlevel, 'group by id_hgis', ST_union(e.wkb_geometry) as geom3857, e.id_sup 
from LAU_europe e WHERE id_hgis IS NOT null and e.province_code is null
group by id_sup, id_hgis;
-- 560


insert into ports.world_1789 (unit_code, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select province_code as unit_code, 2 as unitlevel, 'group by province_code', st_union(wkb_geometry), e.id_sup, province_name
from LAU_europe e WHERE  e.province_code is not null
group by id_sup, province_name, province_code 
-- 79

insert into ports.world_1789 (unit_code, unitlevel, sourcecode, geom3857, unit_sup_id)
select id_sup as unit_id, 2 as unitlevel, 'group by id_sup', st_union(wkb_geometry), e.id_sup
from LAU_europe e WHERE  province_code is  null and id_hgis is null
group by id_sup
-- 5

-- select * from ports.world_1789 where unit_sup_id = 5689

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id)
select e.unit_sup_id as unit_id, 1 as unitlevel, 'pays sans sous-etats, group by id_sup', ST_union(e.geom3857) as geom3857, e.unit_sup_id 
from ports.world_1789 e 
WHERE unit_sup_id in (100003, 100004, 111, 15058, 4918,2140, 15024, 15011, 4918, 9347, 2750, 
100005, 8797, 4974, 3437, 2490, 5708, 4973)
group by unit_sup_id;
-- 17

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id)
select e.unit_sup_id as unit_id, 1 as unitlevel, 'group by id_sup and unit_code is null', ST_union(e.geom3857) as geom3857, e.unit_sup_id 
from ports.world_1789 e 
WHERE  unit_code is null
and unit_sup_id not in (100003, 100004, 111, 15058, 4918,2140, 15024, 15011, 4918, 9347, 2750, 100005, 8797, 4974, 3437, 2490, 5708, 4973)
group by unit_sup_id
-- 74

-- cas particuliers
insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200000, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , e.shortname 
from ports.world_1789 e 
WHERE  unit_code = 'AU03' and unit_sup_id = 5689
group by unit_sup_id, unit_code, shortname;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200001, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'colonies danoises' 
from ports.world_1789 e 
WHERE  unit_code in ('IS', 'FO', 'GL', 'VI') and unit_sup_id = 892
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200002, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Norvège' 
from ports.world_1789 e 
WHERE  unit_code in ('NO') and unit_sup_id = 892
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200003, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Régence d''Alger' 
from ports.world_1789 e 
WHERE  unit_code in ('DZ') and unit_sup_id = 6084
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200004, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Régence de Tripoli' 
from ports.world_1789 e 
WHERE  unit_code in ('LY', 'EG') and unit_sup_id = 6084
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200005, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 'Régence de Tunis' 
from ports.world_1789 e 
WHERE  unit_code in ('TN') and unit_sup_id = 6084
group by unit_sup_id;

-- étendre l'empire ottoman avec la Bosnie et le monténégro
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 6084 or (unit_code =  '6084' and unit_sup_id = 6084) 
) as k where unit_id = 6084 and unitlevel = 1

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200006, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Canaries' 
from ports.world_1789 e 
WHERE  unit_code in ('ES7XX') and unit_sup_id = 1122
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200007, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies espagnoles d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('CU', 'LA', 'FL', 'FK', 'DO', 'PR', 'TT') and unit_sup_id = 1122
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200008, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies françaises d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('PM', 'FRY10', 'FRY20', 'FRY30', 'LC', 'TO', 'DM', 'HT') and unit_sup_id = 1225
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200009, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies françaises en Asie' 
from ports.world_1789 e 
WHERE  unit_code in ('MU', 'FRY40', 'FRY50') and unit_sup_id = 1225
group by unit_sup_id;

-- Etendre la France
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 1225 or (unit_code in ('FRL') and unit_sup_id = 1225) 
) as k where unit_id = 1225 and unitlevel = 1 


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200010, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies britanniques d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('TN', 'CA',  'GD', 'AG', 'KN', 'BB', 'VC', 'JM', 'BS', 'TC', 'KY', 'MS', 'AI', 'VG',
'WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200011, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Ecosse' 
from ports.world_1789 e 
WHERE  unit_code in ('UK02')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200012, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Iles anglo-normandes' 
from ports.world_1789 e 
WHERE  unit_code in ('JE-GG')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200013, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Irlande' 
from ports.world_1789 e 
WHERE  unit_code in ('IE')
and unit_sup_id = 1423
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200014, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Pays de Galles' 
from ports.world_1789 e 
WHERE  unit_code in ('WL')
and unit_sup_id = 1423
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200015, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonies portugaises d''Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('BR')
and unit_sup_id = 3852
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200016, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'îles atlantiques portugaises' 
from ports.world_1789 e 
WHERE  unit_code in ('PT200', 'CV', 'PT300')
and unit_sup_id = 3852
group by unit_sup_id;

delete from ports.world_1789 where unit_id = 4117 and unitlevel = 1
-- select * from ports.world_1789 where unit_id = 4117 and unitlevel = 1

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200017, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Frise' 
from ports.world_1789 e 
WHERE  unit_id = 18 and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200018, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Groningue' 
from ports.world_1789 e 
WHERE  unit_id = 8 and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200019, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Gueldre' 
from ports.world_1789 e 
WHERE  unit_id in (56) and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200032, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Overijssel' 
from ports.world_1789 e 
WHERE  unit_id in (79) and unit_sup_id = 4117
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200020, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Hollande' 
from ports.world_1789 e 
WHERE  unit_id in (1,60) and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200021, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Pays de la Généralité' 
from ports.world_1789 e 
WHERE  unit_id in (72) and unit_sup_id = 4117
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200022, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Zélande' 
from ports.world_1789 e 
WHERE  unit_id in (75) and unit_sup_id = 4117
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200023, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Frise orientale' 
from ports.world_1789 e 
WHERE  unit_code in ('FRO') and unit_sup_id = 5141
group by unit_sup_id;


-- étendre la prusse
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 5141 or (unit_code =  '5141' and unit_sup_id = 5141) 
) as k where unit_id = 5141 and unitlevel = 1


-- étendre la Russie
update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 2344 or (unit_code =  '2344' and unit_sup_id = 2344) 
) as k where unit_id = 2344 and unitlevel = 1

update ports.world_1789 set geom3857 = k.geom 
from ( 
select st_union(geom3857) as geom from ports.world_1789 where unitlevel = 1 and unit_id = 2344 or (unit_code =  'AK' and unit_sup_id = 2344) 
) as k where unit_id = 2344 and unitlevel = 1


select * from ports.world_1789 where unit_code = 'FRM';

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200024, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Corse' 
from ports.world_1789 e 
WHERE  unit_code in ('FRM') and unit_sup_id = 1225
group by unit_sup_id;


select *, st_area(geom3857) from ports.world_1789 where unit_code like 'NA%'
select * from lau_europe le where province_code = 'NA02' and id_sup = 100005
update lau_europe set province_code = null, province_name = null where province_code = 'NA02' and id_sup = 100005
--delete from ports.world_1789 where id = 636
select *, st_area(geom3857) from ports.world_1789 where unit_id = 100005

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200025, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Etat des Présides' 
from ports.world_1789 e 
WHERE  unit_code in ('NA03') and unit_sup_id = 100000
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200026, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Sicile' 
from ports.world_1789 e 
WHERE  unit_code in ('NA02') and unit_sup_id = 100000
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200027, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Naples' 
from ports.world_1789 e 
WHERE  unit_code in ('NA01') and unit_sup_id = 100000
group by unit_sup_id;



insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200028, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Sardaigne' 
from ports.world_1789 e 
WHERE  unit_code in ('SA02') and unit_sup_id = 4705
group by unit_sup_id;


insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200029, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Finlande' 
from ports.world_1789 e 
WHERE  unit_code in ('FI') and unit_sup_id = 8246
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200030, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'Poméranie suédoise' 
from ports.world_1789 e 
WHERE  unit_code in ('POMS') and unit_sup_id = 8246
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unitlevel, sourcecode, geom3857, unit_sup_id, shortname)
select 200031, 1 as unitlevel, 'group by unit_code manually', ST_union(e.geom3857) as geom3857, e.unit_sup_id , 
'colonie suèdoise en Amérique' 
from ports.world_1789 e 
WHERE  unit_code in ('BL97701') and unit_sup_id = 8246
group by unit_sup_id;

-------------------------
-- rajouter les pays restant dans le monde


-- delete from ports.world_1789 where unit_code = 'Amerique-du-Sud'
-- delete from ports.world_1789 where unit_code ='Sud des USA (Mexique et Texas)'

-- rajouter les etats du sud des états unis (Californie, Nevada, Utah, Arizona, ) au Mexique (100007) 
-- Nouveau mexique et Texax)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 2 as unitlevel, 100007, name, 'usa', '(Californie, Nevada, Utah, Arizona)',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'CA', 'NV', 'UT', 'AZ') 

--insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
--select 100007, 0 as unitlevel, -1, name, 'world borders', 'world borders', st_union(geom3857 )
--from world_borders_dump e where id= 120 group by name

--- Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 2 as unitlevel, 100007, name, 'world borders', 'id = 120', st_union(geom3857 )
from world_borders_dump e where id= 120 group by name

-- Etendre les limites du MExique
insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100007, 'MX', 1, 100007, 'Mexique', 'world borders + usa', 'Mexique + (Californie, Nevada, Utah, Arizona)', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100007 and unitlevel=2
-- delete from ports.world_1789  where id = 1312

-- Texas indépendant avec Nouveau Mexique
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select stusps, 2 as unitlevel, 100008, name, 'usa', 'usa',  st_buffer(st_setsrid(st_transform(e.wkb_geometry, 3857), 3857), 3000)
from usa e where stusps in ( 'TX', 'NM') 

insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , shortname , sourcegeom, sourcecode,  geom3857)
select 100008, 'TX', 1, 100008, 'Texas', 'usa', 'usa', st_union(geom3857) 
from ports.world_1789 
where unit_sup_id = 100008 and unitlevel=2


-- Rajouter Hawai
insert into ports.world_1789 (shortname, unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Hawaï', 100010, 'Hawai', 2 as unitlevel, 100010, 'world borders', 'pkid', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where pkid in (3414, 3412, 3408, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396)

insert into ports.world_1789 (shortname, unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Hawaï', 100010, 'Hawai', 1 as unitlevel, 100010, 'world borders', 'pkid', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where pkid in (3414, 3412, 3408, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393, 3394, 3395, 3396)

-- Maroc
insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 100006, 'MA', 2 as unitlevel, 100006, 'world borders', 'Marocco, id 114',  st_union(geom3857)
from world_borders_dump e where id = 114

insert into ports.world_1789 (unit_id, unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 100006, 'MA', 1 as unitlevel, 100006, 'world borders', 'Marocco, id 114',  st_union(geom3857)
from world_borders_dump e where id = 114


-- Les états de la côte Est et les autres fondateurs
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 1 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('NC', 'SC', 'GA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT');

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857, shortname)
select stusps, 2 as unitlevel, 100001, 'usa', 'usa', st_setsrid(st_transform(e.wkb_geometry, 3857), 3857) as geom3857,
name
from usa e where stusps  in ('NC', 'SC', 'GA', 'MD', 'MA', 'ME', 'NH', 'NY', 'PA', 'RI', 'VA', 'CT', 'NJ', 'DE',
'MN', 'WI', 'IL', 'KY', 'TN', 'MS', 'AL', 'IN', 'MI', 'OH', 'WV', 'VT');


-- Amérique du sud
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 1 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Amerique-du-Sud', 2 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (13, 5) and id not in (21, 59, 62, 120)


--- Afrique

-- Distinguer Maroc (indépendant), Algérie, Tunisie, Egypte du reste e Ottoman (6084)
-- 141 Réunion, 170 Mayotte, ile Maurice
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 0 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 1 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'Afrique', 2 as unitlevel, -1, 'world borders', 'world borders', st_unaryunion(st_collect(st_buffer(geom3857, 3000)))
from world_borders_dump e where subregion in (15, 11, 17, 14, 18) and id not in (114, 2, 201,107,50, 141, 170,115) and pkid<>814

-- delete from ports.world_1789 where unit_code = 'Afrique'

-- Le reste Asie + Moyen-Orient 145 + Océanie 54, 53 + Taiwan (0) / retirer la Turquie (202)
insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 0 as unitlevel, -1, 'Asie + Moyen-Orient + Océanie - world borders', 'Asie + Moyen-Orient + Océanie  - world borders',  st_unaryunion(st_collect(geom3857))
from world_borders_dump e where subregion in (145, 54, 53, 30, 34, 35, 143, 0) and id<>202

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 1 , -1, sourcegeom, sourcecode, geom3857 from ports.world_1789 where unit_code='WWW' and unitlevel=0

insert into ports.world_1789 (unit_code, unitlevel, unit_sup_id , sourcegeom, sourcecode, geom3857)
select 'WWW', 2 , -1, sourcegeom, sourcecode, geom3857 from ports.world_1789 where unit_code='WWW' and unitlevel=0

-- Svalbard (SJ) ?
insert into ports.world_1789 (unit_id, unit_sup_id, shortname, unit_code, unitlevel,  sourcegeom, sourcecode, geom3857)
select 100011, 100011, 'Svalbard', 'SJ', 2 as unitlevel, 'world borders', 'world borders', st_unaryunion(st_collect(geom3857))
from world_borders_dump e where  id = 240

insert into ports.world_1789 (unit_id, unit_sup_id, shortname, unit_code, unitlevel,  sourcegeom, sourcecode, geom3857)
select 100011, 100011, 'Svalbard', 'SJ', 1 as unitlevel, 'world borders', 'world borders', st_unaryunion(st_collect(geom3857))
from world_borders_dump e where  id = 240


-- level 0 : les états
insert into ports.world_1789 (unit_id, unit_sup_id, unitlevel,  sourcecode, geom3857)
select unit_sup_id as unit_id, -1, 0 as unitlevel, 'group by unit_sup_id', st_multi(ST_union(e.geom3857)) as geom3857 
from ports.world_1789 e 
WHERE unitlevel = 1 and unit_sup_id<> -1
-- and  (unit_code  in ('Afrique', 'WWW', 'Amerique-du-Sud')) 
group by unit_sup_id;


delete from ports.world_1789 where unit_id = 1225

insert into ports.world_1789 (unit_id, unit_sup_id, unitlevel,  sourcecode, geom3857)
select unit_sup_id as unit_id, 1225, 1 as unitlevel, 'group by unit_sup_id', st_multi(ST_union(e.geom3857)) as geom3857 
from ports.world_1789 e 
WHERE unitlevel = 2 and unit_sup_id= 1225 and ( unit_id is not null or unit_code in ('FRL', 'FRM'))
group by unit_sup_id;

insert into ports.world_1789 (unit_id, unit_sup_id, unitlevel,  sourcecode, geom3857)
select unit_sup_id as unit_id, -1, 0 as unitlevel, 'group by unit_sup_id', st_multi(ST_union(e.geom3857)) as geom3857 
from ports.world_1789 e 
WHERE unitlevel = 1 and unit_sup_id= 1225 
group by unit_sup_id;


select * from ports.world_1789 WHERE  unitlevel = 2 and shortname = 'North Carolina'


SELECT  comm_id, comm_name, name_asci ,cntr_id , nuts_code , id_hgis as unit_id, id_sup, province_code , province_name, sourcegeom 
FROM lau_europe
order by id_sup, id_hgis, province_code

-------------------------------------------------------------------------
-- 24 mars 2021
-- Fin, et envoie des métadonnées et fichiers utiles 
-- à Robin De Mourat,  Loic Charles, Guillaume Daudin, Silvia Marzagalli, Cécile Asselin.
-------------------------------------------------------------------------

------------------
-- id / id_sup
------------------


ALTER TABLE ports.world_1789 ADD COLUMN id_sup int default -1;

update ports.world_1789 w1 set id_sup = w.id 
from ports.world_1789 w 
where w1.unitlevel = 1 and w.unitlevel = 0 and w.unit_id = w1.unit_sup_id 

update ports.world_1789 w set id_sup = -1 where w.unitlevel = 2 
update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w.unitlevel = 1 and w2.unit_sup_id = w.unit_id and w2.unit_code is null

select unitlevel, id, id_sup, shortname from ports.world_1789 where shortname = 'France'

select unitlevel, id, id_sup, shortname from ports.world_1789 where id_sup = 945
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 944
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 761
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 745
select unitlevel,id, id_sup, shortname from ports.world_1789 where id_sup = 743

select unitlevel,id, id_sup, shortname from ports.world_1789 where unit_sup_id = 1225
select unitlevel,id, id_sup, shortname, * from ports.world_1789 where unit_sup_id = 1225 and unitlevel = 2

select shortname, unitlevel, id, id_sup, unit_code , unit_id, unit_sup_id from ports.world_1789 
where    id_sup=-1

select shortname, unitlevel, id, id_sup, unit_code , unit_id, unit_sup_id from ports.world_1789 
where  unit_code = '100009'
unitlevel = 1 and unit_sup_id = 100001

select * from ports.world_1789
where  unitlevel = 0 and unit_id = 100003
select * from ports.world_1789
where  unitlevel = 0 and id = 870

update ports.world_1789 w set id_sup = 671 where  id = 620 ; -- Alaska
update ports.world_1789 w set id_sup = 944 where  id in (579 ) ; -- Lorraine
update ports.world_1789 w set id_sup = 670 where  id in (596 ) ; -- Ile de Man
update ports.world_1789 w set id_sup = 833 where  id in (831, 832 ) ; -- Texas et Nouveau Mexique
update ports.world_1789 w set id_sup = 761 where  id in (578 ) ; -- Corse
update ports.world_1789 w set id_sup = 839 where  id in (834 ) ; -- Mexico
update ports.world_1789 w set id_sup = 825 where  id in (824 ) ; -- Maroc
update ports.world_1789 w set id_sup = 830 where  id in (829 ) ; -- Hawaï
update ports.world_1789 w set id_sup = 844 where  id in (843 ) ; -- Svalbard
-- Sans noms
update ports.world_1789 w set id_sup = 712 where  id in (642 ) ; -- Prusse
update ports.world_1789 w set id_sup = 657 where  id in (641 ) ; -- Pologne
update ports.world_1789 w set id_sup = 709 where  id in (643 ) ; -- Ottoman
update ports.world_1789 w set id_sup = 671 where  id in (640) ; -- Russie
update ports.world_1789 w set id_sup = 664, shortname = 'Malte' where  id in (644) ; -- Russie


update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code is not null
and w.unitlevel = 1 and w2.shortname = w.shortname and w.unit_sup_id = 100001;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code is not null
and w.unitlevel = 1 and w2.unit_code in ('AU03') and w.unit_id = 200000;

update ports.world_1789 w2 set unit_code = 'BRM' where shortname = 'Brême' and unit_code = 'BR'
update ports.lau_europe w2 set province_code = 'BRM' where province_name = 'Brême' and unit_code = 'BR'

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('BRM')
and w.unitlevel = 1 and w.unit_id = 100003 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('IS', 'FO', 'GL', 'VI')
and w.unitlevel = 1 and w.unit_id = 200001 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NO')
and w.unitlevel = 1 and w.unit_id = 200002 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('DZ')
and w.unitlevel = 1 and w.unit_id = 200003 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('LY', 'EG')
and w.unitlevel = 1 and w.unit_id = 200004 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('TN')
and w.unitlevel = 1 and w.unit_id = 200005 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('ES7XX')
and w.unitlevel = 1 and w.unit_id = 200006 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('CU', 'LA', 'FL', 'FK', 'DO', 'PR', 'TT')
and w.unitlevel = 1 and w.unit_id = 200007 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('PM', 'FRY10', 'FRY20', 'FRY30', 'LC', 'TO', 'DM', 'HT')
and w.unitlevel = 1 and w.unit_id = 200008 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('MU', 'FRY40', 'FRY50')
and w.unitlevel = 1 and w.unit_id = 200009 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code 
in ('TN', 'CA',  'GD', 'AG', 'KN', 'BB', 'VC', 'JM', 'BS', 'TC', 'KY', 'MS', 'AI', 'VG',
'WA', 'OR', 'ID', 'MT', 'WY', 'ND', 'SD', 'IA', 'NE', 'CO', 'KS', 'MO', 'OK', 'AR')
and w.unitlevel = 1 and w.unit_id = 200010 ;

-- attention, CAnada et CAlifornie
select * from ports.world_1789 w2  where w2.shortname = 'California'
select id, * from ports.world_1789  where unit_id = 100007
update ports.world_1789 set id_sup = 839 where shortname = 'California'

update ports.world_1789 set id_sup = 839 where shortname in ('Nevada', 'Utah', 'Arizona')

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('UK02')
and w.unitlevel = 1 and w.unit_id = 200011 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('JE-GG')
and w.unitlevel = 1 and w.unit_id = 200012 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('IE')
and w.unitlevel = 1 and w.unit_id = 200013 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('WL')
and w.unitlevel = 1 and w.unit_id = 200014 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('BR')
and w.unitlevel = 1 and w.unit_id = 200015 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('PT200', 'PT300', 'CV')
and w.unitlevel = 1 and w.unit_id = 200016 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('FRI')
and w.unitlevel = 1 and w.unit_id = 200017 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('GRO')
and w.unitlevel = 1 and w.unit_id = 200018 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('GUE')
and w.unitlevel = 1 and w.unit_id = 200019 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('HOL')
and w.unitlevel = 1 and w.unit_id = 200020 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('GEN')
and w.unitlevel = 1 and w.unit_id = 200021 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('ZEL')
and w.unitlevel = 1 and w.unit_id = 200022 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('OVR')
and w.unitlevel = 1 and w.unit_id = 200032 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('FRO')
and w.unitlevel = 1 and w.unit_id = 200023 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NA03')
and w.unitlevel = 1 and w.unit_id = 200025 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NA02')
and w.unitlevel = 1 and w.unit_id = 200026 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('NA01')
and w.unitlevel = 1 and w.unit_id = 200027 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('SA02')
and w.unitlevel = 1 and w.unit_id = 200028 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('FI')
and w.unitlevel = 1 and w.unit_id = 200029 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('POMS')
and w.unitlevel = 1 and w.unit_id = 200030 ;

update ports.world_1789 w2 set id_sup = w.id 
from ports.world_1789 w 
where w2.unitlevel = 2 and w2.id_sup = -1 and w2.unit_code in ('BL97701')
and w.unitlevel = 1 and w.unit_id = 200031

select unitlevel, id, id_sup, shortname from ports.world_1789 where shortname = 'Grande-Bretagne'
select unitlevel, id, id_sup, shortname from ports.world_1789 where id_sup = 859
select unitlevel, id, id_sup, unit_id, unit_sup_id , shortname, * from ports.world_1789 where id_sup = 670

update lau_europe set province_code ='FRI', province_name = 'Frise' where id_hgis=18 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='GRO', province_name = 'Groningue' where id_hgis=8 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='GUE', province_name = 'Gueldre' where id_hgis=56 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='HOL', province_name = 'Hollande' where id_hgis in (1,60) and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='GEN', province_name = 'Pays de la Généralité' where id_hgis=72 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='ZEL', province_name = 'Zélande' where id_hgis=75 and id_sup =4117 and cntr_code = 'NL';
update lau_europe set province_code ='OVR', province_name = 'Overijssel' where id_hgis=79 and id_sup =4117 and cntr_code in ('NL', 'DE');

select * from ports.world_1789 where shortname = 'Groningue'
select * from ports.world_1789 where shortname = 'Maryland'

update ports.world_1789 set unit_code = 'FRI' where shortname in ('Frise', 'Friesland') ;
update ports.world_1789 set unit_code = 'GRO' where shortname in ('Groningue', 'Groningen');
update ports.world_1789 set unit_code = 'GUE' where shortname in ('Gueldre', 'Guelders');
update ports.world_1789 set unit_code = 'HOL' where shortname in ('Hollande', 'Utrecht', 'Holland');
update ports.world_1789 set unit_code = 'GEN' where shortname in ('Pays de la Généralité', 'Generality');
update ports.world_1789 set unit_code = 'ZEL' where shortname in ('Zélande', 'Zeeland');
update ports.world_1789 set unit_code = 'OVR' where shortname = 'Overijssel'

select * from ports.world_1789 where shortname = 'Overijssel'

select id, id_sup, unit_code, unit_id, unit_sup_id , unitlevel from ports.world_1789 where unitlevel = 2 and id_sup = -1


select * from lau_europe where id_hgis=18 and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis=8 and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis=56 and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (1, 60) and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (72) and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (75) and id_sup =4117 and cntr_code = 'NL'
select * from lau_europe where id_hgis in (79) and id_sup =4117 and cntr_code in ('NL', 'DE')

select * from ports.world_1789 where unitlevel = 0 


select * from ports.world_1789 where unit_sup_id = 100009
select * from ports.world_1789 where id = 644
update ports.world_1789 set shortname = 'Malte' where id = 850 -- au lieu de Saint John
update ports.world_1789 set shortname = 'Malte' where id = 664 -- au lieu de Saint John

update ports.world_1789_23mars2021 set shortname = 'Malte' where id = 850


SELECT  comm_id, comm_name, name_asci ,cntr_id , nuts_code , unit_id as id_sup, province_code , province_name, sourcegeom 
FROM lau_europe
order by id_sup, unit_id, province_code

alter table lau_europe add column unit_id int ; -- id de l'unité d'appartenance dans la table world_1789

update lau_europe l set unit_id = w.id
from ports.world_1789 w where unitlevel = 2 and w.unit_code = province_code and w.unit_sup_id = l.id_sup 
and  province_code is not null
-- 12 020
update lau_europe l set unit_id = w.id
from ports.world_1789 w where unitlevel = 2 and w.unit_id = id_hgis and w.unit_sup_id = l.id_sup 
and  province_code is  null
-- 111 743


select * from world_1789 w2 where unitlevel = 2 and unit_code = '2344'
update lau_europe l set unit_id = 640 where unit_id is null and id_sup = 2344


update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '2750') where unit_id is null and id_sup = 2750;
update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '6084') where unit_id is null and id_sup = 6084;
update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '5141') where unit_id is null and id_sup = 5141;
update lau_europe l set unit_id = (select id from world_1789 w2 where unitlevel = 2 and unit_code = '100009') where unit_id is null and id_sup = 100009;

select * from lau_europe where unit_id is null

select distinct w.id, w.shortname , w1.id, w1.shortname, w2.id, w2.shortname 
from world_1789 w , world_1789 w1, world_1789 w2
where w1.id_sup = w.id and w.unitlevel = 0 and w1.unitlevel = 1 and w2.unitlevel = 2 and w2.id_sup = w1.id
order by w.shortname, w1.shortname 

create table ports.world_1789_23mars2021 as (select * from ports.world_1789 w)
create table ports.lau_europe_23mars2021 as (select * from ports.lau_europe )
drop table ports.lau_europe_23mars2021
drop table ports.world_1789_23mars2021

-- erreur sur une petite partie du Piemont en Italie qui appartient à l'Autriche
select * from ports.world_1789 where id = 693
select * from ports.world_1789 where id_sup = 693

update ports.world_1789 set unit_sup_id = 5689, unit_id = 5689, id_sup = 870, shortname= 'Autriche'
where id = 693 and unit_id = 1 -- pas fait


SELECT  p.source_main_port_toponyme , p.source_main_port_uhgs_id, count(distinct source_doc_id)
FROM navigoviz.pointcall p
group by p.source_main_port_toponyme , p.source_main_port_uhgs_id
order by source_main_port_toponyme

SELECT  p.toponyme_fr , p.pointcall_uhgs_id , pointcall_status, count(distinct source_doc_id)
FROM navigoviz.pointcall p
where  p.pointcall_function = 'O'
-- p.pointcall_status is not null and
group by p.toponyme_fr , p.pointcall_uhgs_id, pointcall_status
order by toponyme_fr

SELECT unitlevel, id, shortname, id_sup, state_shortname FROM world_1789_mixte
order by unitlevel, state_shortname

SELECT toponyme_standard_fr, state_1789_fr, substate_1789_fr, province, amiraute, uhgs_id, longitude,latitude FROM port_points pp 
where pp.toponyme_standard_fr like '%Luz'
in ('Saint Jean-de-Luz')
order by unitlevel, state_shortname

-------------------------------------------------------------------------------------------------
-- 31 mars 2021
--  prendre en compte les corrections obliques, siège d'amirauté et autres (nouveaux ports) de Silvia
-------------------------------------------------------------------------------------------------

alter table ports.port_points_old rename to port_points_oldfev2021 
CREATE TABLE ports.port_points_old as (select * from port_points pp )

select count(*) from port_points pp 
select count(*) from port_points_old pp 

-- importer le fichier Excel de Silvia filtré sur la France
-- 31 mars 2020

-- mettre à jour les colonnes de port_points avec et insérer les nouvelles lignes
-- colonnes

province	amiraute	has_a_clerk
conges_1787_inputdone	conges_1789_inputdone	status	incertitude_statut	
ferme_direction	ferme_bureau	ferme_bureau_uncertainty	partner_balance_1789	partner_balance_supp_1789	
partner_balance_1789_uncertainty	partner_balance_supp_1789_uncertainty

alter table port_points  add column conges_1787_inputdone boolean ;
alter table port_points  add column conges_1789_inputdone boolean ;
alter table port_points  add column incertitude_statut text ;
-- incertitude_status

alter table port_points  add column nb_conges_1787_inputdone int ;
alter table port_points  add column Nb_conges_1787_CR int ;
alter table port_points  add column Nb_sante_1787 int ;
alter table port_points  add column Nb_petitcabotage_1787 int ;

alter table port_points  add column nb_conges_1789_inputdone int ;
alter table port_points  add column Nb_conges_1789_CR int ;
alter table port_points  add column Nb_sante_1789 int ;
alter table port_points  add column Nb_petitcabotage_1789 int ;

update port_points pp set
province=v.province,
amiraute=v.amiraute,
has_a_clerk=v.has_a_clerk,
conges_1787_inputdone=v.conges_1787_inputdone,
conges_1789_inputdone=v.conges_1789_inputdone,
status=v.status,
incertitude_statut=v.incertitude_statut,
ferme_direction=v.ferme_direction,
ferme_bureau=v.ferme_bureau,
ferme_bureau_uncertainty=v.ferme_bureau_uncertainty,
partner_balance_1789=v.partner_balance_1789,
partner_balance_supp_1789=v.partner_balance_supp_1789,
partner_balance_1789_uncertainty=v.partner_balance_1789_uncertainty,
partner_balance_supp_1789_uncertainty=v.partner_balance_supp_1789_uncertainty
from "verif_ports_11fevrierb_2021_france31mars21.csv"  v
where v.uhgs_id = pp.uhgs_id ;
-- 382

select uhgs_id, toponyme_standard_fr , toponyme_standard_en , longitude , latitude , state_1789_en , state_1789_fr ,
province,	amiraute,	has_a_clerk,
conges_1787_inputdone,	conges_1789_inputdone,	status,	incertitude_statut,	
ferme_direction	ferme_bureau,	ferme_bureau_uncertainty,	partner_balance_1789,	partner_balance_supp_1789,	
partner_balance_1789_uncertainty,	partner_balance_supp_1789_uncertainty
from "verif_ports_11fevrierb_2021_france31mars21.csv" where uhgs_id not in (select uhgs_id from port_points pp)

insert into port_points (uhgs_id, toponyme_standard_fr , toponyme_standard_en , longitude , latitude , state_1789_en , state_1789_fr ,
province,	amiraute,	has_a_clerk,
conges_1787_inputdone,	conges_1789_inputdone,	status,	incertitude_statut,	
ferme_direction,	ferme_bureau,	ferme_bureau_uncertainty,	partner_balance_1789,	partner_balance_supp_1789,	
partner_balance_1789_uncertainty,	partner_balance_supp_1789_uncertainty) 
select uhgs_id, toponyme_standard_fr , toponyme_standard_en , longitude , latitude , state_1789_en , state_1789_fr ,
province,	amiraute,	has_a_clerk,
conges_1787_inputdone,	conges_1789_inputdone,	status,	incertitude_statut,	
ferme_direction,	ferme_bureau,	ferme_bureau_uncertainty,	partner_balance_1789,	partner_balance_supp_1789,	
partner_balance_1789_uncertainty,	partner_balance_supp_1789_uncertainty
from "verif_ports_11fevrierb_2021_france31mars21.csv" where uhgs_id not in (select uhgs_id from port_points pp);
-- 13

select distinct province from port_points pp order by province 
select distinct amiraute from port_points pp order by amiraute 

select * from port_points where amiraute in ('Saint-Pierre', 'Saint-Pierre-et-Miquelon')
select * from port_points where amiraute in ('Saint-Louis', 'Port-Louis (Tobago)')

update port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where   geom is null;
update port_points set point3857 = st_setsrid(st_transform(geom, 3857), 3857) where   point3857 is null;
-- 20

select toponyme_standard_fr, amiraute, status, has_a_clerk , oblique 
from port_points
where status is not null and has_a_clerk is null

update port_points set has_a_clerk = true, oblique = true where status is not null
-- 189

select toponyme_standard_fr, amiraute, status, has_a_clerk , oblique 
from port_points
where status is  null and has_a_clerk is not null

update port_points set status = 'oblique', oblique = true  where has_a_clerk is true and status is null
--Luçon	Sables-d’Olonne		true	true
--Port-Bail	Port-Bail		true	true
--Port-Louis	Lorient		true	true
--Saint-Florent	Bastia		true	true
--Léogâne 			true	

delete from port_points pp where uhgs_id = 'B2054985' and toponyme_standard_fr = 'Jacmel';
select * from port_points where toponyme_standard_fr = 'Jacmel';

status : oblique | siège d'amirauté | rien --> Silvia
has_a_clerk : vrai si     oblique | siège d'amirauté  --> Christine

/*
alter table port_points  add column nb_conges_1787_inputdone int ;
alter table port_points  add column Nb_conges_1787_CR int ;
alter table port_points  add column Nb_sante_1787 int ;
alter table port_points  add column Nb_petitcabotage_1787 int ;

alter table port_points  add column nb_conges_1789_inputdone int ;
alter table port_points  add column Nb_conges_1789_CR int ;
alter table port_points  add column Nb_sante_1789 int ;
alter table port_points  add column Nb_petitcabotage_1789 int ;
*/

/*
conges_AAAA_inputdone : vrai si tous les congés existants ont été saisis pour cette année pour ce port (qui est forcément donc au moins oblique) --> Silvia
OU 
nb_conges_AAAA_inputdone : nb de congés saisis  pour cette année pour ce port (qui est forcément donc au moins oblique)   --> Christine
Nb_conges_AAAA_CR   : nombre de congés existants connus à ce jour, mais pas forcément saisis, pour ce port oblique.  --> Silvia 
Nb_sante_AAAA  :    : nombre d'entrées santé existantes connus à ce jour, mais pas forcément saisies pour le port  --> Silvia 
Nb_petitcabotage_AAAA :  nombre d'entrées petit cabotage existantes connus à ce jour, mais pas forcément saisies pour le port   --> Silvia 
*/


select toponyme_fr, pointcall_uhgs_id , count(distinct source_doc_id) 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and extract(year from outdate_fixed) = 1787
group by toponyme_fr, pointcall_uhgs_id 

update port_points pp set nb_conges_1787_inputdone = k.c
from 
(select toponyme_fr, pointcall_uhgs_id , count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and extract(year from outdate_fixed) = 1787
group by toponyme_fr, pointcall_uhgs_id ) as k 
where pp.uhgs_id = k.pointcall_uhgs_id


update port_points pp set nb_conges_1789_inputdone = k.c
from 
(select toponyme_fr, pointcall_uhgs_id , count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and extract(year from outdate_fixed) = 1789
group by toponyme_fr, pointcall_uhgs_id ) as k 
where pp.uhgs_id = k.pointcall_uhgs_id
 
--------------------------
-- 2 avril : correction et ajouts dans ports.obliques
-- Fichier C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\obliques\ports_02avril_ajouts nb congés en cours.xlsx
--------------------------

update ports.obliques set d1787 = 1117 where port = 'Calais';

alter table ports.obliques add column port_toponyme_fr text;
alter table ports.obliques add column port_uhgs_id text;


select * from ports.obliques where port % 'Oléron';

select * from ports.port_points pp where uhgs_id = 'A0137359';


update ports.obliques set port_toponyme_fr = 'Ars-en-Ré', port_uhgs_id='A0133403' where port = 'Ars en Ré';
update ports.obliques set port_toponyme_fr = 'Beauvoir-sur-Mer', port_uhgs_id='A0181608' where port = 'Beauvoir/Mer';
update ports.obliques set port_toponyme_fr = 'Champagné-les-Marais', port_uhgs_id='A0169240' where port = 'Champagne';
update ports.obliques set port_toponyme_fr = 'Étaples ', port_uhgs_id='A0192436' where port = 'Etaples';
update ports.obliques set port_toponyme_fr = 'Genêts ', port_uhgs_id='A0177077' where port = 'Genets';
update ports.obliques set port_toponyme_fr = 'île de Bouin', port_uhgs_id='A0165077' where port = 'Bouin';
update ports.obliques set port_toponyme_fr = 'îles Chausey', port_uhgs_id='A0206322' where port = 'Chausey';
update ports.obliques set port_toponyme_fr = 'La Flotte-en-Ré', port_uhgs_id='A0199508' where port = 'Flotte La';
update ports.obliques set port_toponyme_fr = 'La Perrotine', port_uhgs_id='A1964982' where port = 'Perrotine La';
update ports.obliques set port_toponyme_fr = 'La Roche-Bernard', port_uhgs_id='A0149919' where port = 'Roche/Bernard';
update ports.obliques set port_toponyme_fr = 'La Teste', port_uhgs_id='A0189289' where port = 'Teste La';
update ports.obliques set port_toponyme_fr = 'La Tranche-sur-Mer', port_uhgs_id='A0165056' where port = 'Tranche/Mer';
update ports.obliques set port_toponyme_fr = 'Landerneau', port_uhgs_id='A0141652' where port = 'Landernau';
update ports.obliques set port_toponyme_fr = 'Le Canet', port_uhgs_id='A0127897' where port = 'Canet';
update ports.obliques set port_toponyme_fr = 'Le Château-d''Oléron', port_uhgs_id='A0138533' where port = 'Chât Oléron';
update ports.obliques set port_toponyme_fr = 'Le Conquet', port_uhgs_id='A0187969' where port = 'Conquet Le';
update ports.obliques set port_toponyme_fr = 'Le Croisic', port_uhgs_id='A0213139' where port = 'Croisic Le';
update ports.obliques set port_toponyme_fr = 'Le Pouliguen', port_uhgs_id='A0162516' where port = 'Pouliguen';
update ports.obliques set port_toponyme_fr = 'Les Sables-d''Olonne', port_uhgs_id='A0137148' where port = 'Les Sables d''Olonne';
update ports.obliques set port_toponyme_fr = 'Marans', port_uhgs_id='A1964694' where port = 'Alligre';
update ports.obliques set port_toponyme_fr = 'Ribérou', port_uhgs_id='A1964767' where port = 'Riberou';
update ports.obliques set port_toponyme_fr = 'Rochefort', port_uhgs_id='A0196496' where port = 'Rochefort ';
update ports.obliques set port_toponyme_fr = 'Saint-Denis d''Oléron', port_uhgs_id='A0141325' where port = 'St-Denis/Olé';
update ports.obliques set port_toponyme_fr = 'Saint-Gilles-sur-Vie', port_uhgs_id='A0195938' where port = 'St-Gilles/Vie';
update ports.obliques set port_toponyme_fr = 'Saint-Jean-de-Luz ', port_uhgs_id='A0122594' where port = 'St-Jean/Luz';
update ports.obliques set port_toponyme_fr = 'Saint-Laurent-de-la-Salanque', port_uhgs_id='A0209302' where port = 'St-Laurent/Sal';
update ports.obliques set port_toponyme_fr = 'Saint-Martin-de-Ré', port_uhgs_id='A0127055' where port = 'St-Martin/Ré';
update ports.obliques set port_toponyme_fr = 'Saint-Michel-en-l''Herm', port_uhgs_id='A0122971' where port = 'St-Michel/Her';
update ports.obliques set port_toponyme_fr = 'Saint-Nazaire', port_uhgs_id='A0195511' where port = 'St-Nazaire';
update ports.obliques set port_toponyme_fr = 'Saint-Valery-sur-Somme', port_uhgs_id='A0196771' where port = 'Saint-Valéry sur Somme';
update ports.obliques set port_toponyme_fr = 'Saint-Valery en Caux', port_uhgs_id='A0135548' where port = 'Saint-Valéry en Caux';
update ports.obliques set port_toponyme_fr = 'Socoa', port_uhgs_id='A0127400' where port = 'Ciboure (Socoa)';
update ports.obliques set port_toponyme_fr = 'Tonnay-Charente', port_uhgs_id='A0171758' where port = 'Charente';
update ports.obliques set port_toponyme_fr = 'Le Passage', port_uhgs_id='A0138463' where port = 'Pas St-Jean';
update ports.obliques set port_toponyme_fr = 'Mesquer', port_uhgs_id='A0148900' where port = 'Mesquer';
update ports.obliques set port_toponyme_fr = 'Penpoul', port_uhgs_id='A0137359' where port = 'Penpoul';

-- Mesquer
select * from port_points pp where pp.uhgs_id = 'A0148900'

-- Penpoul
select * from port_points pp where pp.uhgs_id = 'A0137359'
---
update ports.obliques set port_toponyme_fr = 'Abbeville', port_uhgs_id='A0169591' where port = 'Abbeville';
update ports.obliques set port_toponyme_fr = 'Aber-Ildut', port_uhgs_id='A0140339' where port = 'Aber-Ildut';
update ports.obliques set port_toponyme_fr = 'Ajaccio', port_uhgs_id='A0218974' where port = 'Ajaccio';
update ports.obliques set port_toponyme_fr = 'Argenton', port_uhgs_id='A0169612' where port = 'Argenton';
update ports.obliques set port_toponyme_fr = 'Baie d''Authie', port_uhgs_id='A0169535' where port = 'Baie d''Authie';
update ports.obliques set port_toponyme_fr = 'Barfleur', port_uhgs_id='A0181496' where port = 'Barfleur';
update ports.obliques set port_toponyme_fr = 'Bastia', port_uhgs_id='A0193167' where port = 'Bastia';
update ports.obliques set port_toponyme_fr = 'Bayonne', port_uhgs_id='A0187995' where port = 'Bayonne';
update ports.obliques set port_toponyme_fr = 'Blaye', port_uhgs_id='A0207141' where port = 'Blaye';
update ports.obliques set port_toponyme_fr = 'Bordeaux', port_uhgs_id='A0180923' where port = 'Bordeaux';
update ports.obliques set port_toponyme_fr = 'Boulogne-sur-Mer', port_uhgs_id='A0152606' where port = 'Boulogne-sur-Mer';
update ports.obliques set port_toponyme_fr = 'Bourg', port_uhgs_id='A0205721' where port = 'Bourg';
update ports.obliques set port_toponyme_fr = 'Bourgneuf', port_uhgs_id='A0138654' where port = 'Bourgneuf';
update ports.obliques set port_toponyme_fr = 'Brest', port_uhgs_id='A0200718' where port = 'Brest';
update ports.obliques set port_toponyme_fr = 'Calais', port_uhgs_id='A0165080' where port = 'Calais';
update ports.obliques set port_toponyme_fr = 'Calvi', port_uhgs_id='A0164778' where port = 'Calvi';
update ports.obliques set port_toponyme_fr = 'Caudebec', port_uhgs_id='A0180920' where port = 'Caudebec';
update ports.obliques set port_toponyme_fr = 'Cayenne', port_uhgs_id='B2009606' where port = 'Cayenne';
update ports.obliques set port_toponyme_fr = 'Cherbourg', port_uhgs_id='A0205085' where port = 'Cherbourg';
update ports.obliques set port_toponyme_fr = 'Collioure', port_uhgs_id='A0181755' where port = 'Collioure';
update ports.obliques set port_toponyme_fr = 'Courtils', port_uhgs_id='A0204935' where port = 'Courtils';
update ports.obliques set port_toponyme_fr = 'Diélette', port_uhgs_id='A0130763' where port = 'Diélette';
update ports.obliques set port_toponyme_fr = 'Dieppe', port_uhgs_id='A0130761' where port = 'Dieppe';
update ports.obliques set port_toponyme_fr = 'Dunkerque', port_uhgs_id='A0204180' where port = 'Dunkerque';
update ports.obliques set port_toponyme_fr = 'Esnandes', port_uhgs_id='A0213721' where port = 'Esnandes';
update ports.obliques set port_toponyme_fr = 'Fécamp', port_uhgs_id='A0192135' where port = 'Fécamp';
update ports.obliques set port_toponyme_fr = 'Grandcamp', port_uhgs_id='A0201236' where port = 'Grandcamp';
update ports.obliques set port_toponyme_fr = 'Granville', port_uhgs_id='A0167057' where port = 'Granville';
update ports.obliques set port_toponyme_fr = 'Gravelines', port_uhgs_id='A0189462' where port = 'Gravelines';
update ports.obliques set port_toponyme_fr = 'Harfleur', port_uhgs_id='A0204500' where port = 'Harfleur';
update ports.obliques set port_toponyme_fr = 'Honfleur', port_uhgs_id='A0187836' where port = 'Honfleur';
update ports.obliques set port_toponyme_fr = 'Isigny', port_uhgs_id='A0130741' where port = 'Isigny';
update ports.obliques set port_toponyme_fr = 'La Hougue', port_uhgs_id='A0177816' where port = 'La Hougue';
update ports.obliques set port_toponyme_fr = 'La Rochelle', port_uhgs_id='A0198999' where port = 'La Rochelle';
update ports.obliques set port_toponyme_fr = 'Le Crotoy', port_uhgs_id='A0212088' where port = 'Le Crotoy';
update ports.obliques set port_toponyme_fr = 'Le Havre', port_uhgs_id='A0187101' where port = 'Le Havre';
update ports.obliques set port_toponyme_fr = 'Le Tréport', port_uhgs_id='A0212320' where port = 'Le Tréport';
update ports.obliques set port_toponyme_fr = 'Lorient', port_uhgs_id='A0140266' where port = 'Lorient';
update ports.obliques set port_toponyme_fr = 'Marennes', port_uhgs_id='A0136930' where port = 'Marennes';
update ports.obliques set port_toponyme_fr = 'Marseille', port_uhgs_id='A0210797' where port = 'Marseille';
update ports.obliques set port_toponyme_fr = 'Mesquer', port_uhgs_id='A0148900' where port = 'Mesquer';
update ports.obliques set port_toponyme_fr = 'Moricq', port_uhgs_id='A1963997' where port = 'Moricq';
update ports.obliques set port_toponyme_fr = 'Mortagne', port_uhgs_id='A0124809' where port = 'Mortagne';
update ports.obliques set port_toponyme_fr = 'Nantes', port_uhgs_id='A0124817' where port = 'Nantes';
update ports.obliques set port_toponyme_fr = 'Noirmoutier', port_uhgs_id='A0136403' where port = 'Noirmoutier';
update ports.obliques set port_toponyme_fr = 'Omonville', port_uhgs_id='A0153622' where port = 'Omonville';
update ports.obliques set port_toponyme_fr = 'Pont-Audemer', port_uhgs_id='A0185236' where port = 'Pont-Audemer';
update ports.obliques set port_toponyme_fr = 'Pornic', port_uhgs_id='A0148531' where port = 'Pornic';
update ports.obliques set port_toponyme_fr = 'Port-Bail', port_uhgs_id='A0209786' where port = 'Port-Bail';
update ports.obliques set port_toponyme_fr = 'Portsall', port_uhgs_id='A0209662' where port = 'Portsall';
update ports.obliques set port_toponyme_fr = 'Quillebeuf', port_uhgs_id='A0173748' where port = 'Quillebeuf';
update ports.obliques set port_toponyme_fr = 'Roscoff', port_uhgs_id='A0122918' where port = 'Roscoff';
update ports.obliques set port_toponyme_fr = 'Rouen', port_uhgs_id='A0122218' where port = 'Rouen';
update ports.obliques set port_toponyme_fr = 'Royan', port_uhgs_id='A0172590' where port = 'Royan';
update ports.obliques set port_toponyme_fr = 'Saint-Malo', port_uhgs_id='A0170819' where port = 'Saint-Malo';
update ports.obliques set port_toponyme_fr = 'Saint-Pierre-et-Miquelon', port_uhgs_id='B1965595' where port = 'Saint-Pierre-et-Miquelon';
update ports.obliques set port_toponyme_fr = 'Saint-Tropez', port_uhgs_id='A0147257' where port = 'Saint-Tropez';
update ports.obliques set port_toponyme_fr = 'Soubise', port_uhgs_id='A0148208' where port = 'Soubise';
update ports.obliques set port_toponyme_fr = 'Talmont', port_uhgs_id='A0207992' where port = 'Talmont';
update ports.obliques set port_toponyme_fr = 'Touques', port_uhgs_id='A0158963' where port = 'Touques';


update port_points pp set Nb_conges_1787_CR = d1787 , Nb_conges_1789_CR = d1789
from ports.obliques o
where pp.uhgs_id = o.port_uhgs_id;
-- 74

create table port_points_backup as (select * from port_points)

 SELECT toponyme_standard_fr, uhgs_id , latitude , longitude , amiraute, country2019_name, status, partner_balance_supp_1789, geom, point3857 
 FROM port_points WHERE country2019_name IS NULL AND (partner_balance_supp_1789 = 'France')
--La Trinité	A0198704	47.5833321	-3.03333306	Vannes		oblique	France
--Le Brault	A0214663	46.3666687	-1.04999995	La Rochelle		oblique	France
--Bayeux	A0212540	49.2666664	-0.699999988	Bayeux		siège amirauté	France
--Saint-Briac	A0183923	48.6333313	-2.13333297	Saint-Malo		oblique	France
--Carentan	A0206654	49.4166679	-1.14999998	Isigny		oblique	France
 

 update port_points set toponyme = toponyme_standard_fr,country2019_name = 'France'  
where country2019_name IS NULL AND partner_balance_supp_1789 = 'France';

SELECT toponyme_standard_fr, uhgs_id , latitude , longitude , amiraute, province, country2019_name, status, partner_balance_supp_1789, geom, point3857 
 FROM port_points WHERE country2019_name IS NULL AND (partner_balance_supp_1789 like 'colonies%')

-- Silvia : il faudra rajouter dans état : 
--Petit-Goave	B2049573	18.4313889	-72.8669434	Petit-Goave	Saint-Domingue		siège amirauté	colonies françaises
--Môle-Saint-Nicolas	B2044556	19.7999992	-73.3833313	Môle-Saint-Nicolas	Saint-Domingue		siège amirauté	colonies françaises
--Port-de-Paix	B2045254	19.8333321	-72.9166641	Port-de-Paix	Saint-Domingue		siège amirauté	colonies françaises
--Saint-Louis	B2056379	18.2666664	-73.5500031	Saint-Louis	Saint-Domingue		siège amirauté	colonies françaises

Port-Louis (Tobago)	B1975512	11.1833334	-60.7333336	Port-Louis (Tobago)			siège amirauté	colonies françaises
Fort-Dauphin	B2048242	19.667778	-71.8397217	Fort-Dauphin			siège amirauté	colonies françaises
Le Carénage	B1969630	14	-61	Le Carénage			siège amirauté	colonies françaises
-- sur google, c'est 19.600018312414573, -72.94583385405137

-- https://fr.wikipedia.org/wiki/Fort-Libert%C3%A9
update port_points set province='Saint-Domingue' 
where toponyme_standard_fr='Fort-Dauphin' and uhgs_id = 'B2048242';
update port_points set province='Saint-Domingue' 
where toponyme_standard_fr='Le Carénage' and uhgs_id = 'B1969630';

select toponyme_standard_fr, uhgs_id , latitude , longitude , amiraute, province, status 
from  port_points where  province='Saint-Domingue' 
order by amiraute 

update port_points set amiraute='Port-au-Prince' where uhgs_id = 'B2045171' and toponyme_standard_fr = 'Léogâne ';
select * from port_points where uhgs_id = 'B2045171'
update port_points set amiraute='Petit-Goave' where uhgs_id = 'B2062485' and toponyme_standard_fr = 'Miragoâne ';
select * from port_points where uhgs_id = 'B2062485'
update port_points set amiraute='Jérémie' where uhgs_id = 'B2047006' and toponyme_standard_fr = 'cap Tiburon';

update port_points set toponyme_standard_fr = trim(toponyme_standard_fr)
update port_points set toponyme_standard_en = trim(toponyme_standard_en)

 select country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 'Petit-Goave' , 'Petit-Goave' , 'B2049573'
 from etats e2  where e2.toponyme_standard_fr = 'Saint-Domingue'
 
 insert into etats (country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Dominican Republic', 'France', 'colonies françaises d''Amérique', 'France', 'French colonies in America', null, 1804,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE country2019_name IS NULL AND (partner_balance_supp_1789 like 'colonies%') and province = 'Saint-Domingue';
-- 4

insert into etats (country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Dominican Republic', 'France', 'colonies françaises d''Amérique', 'France', 'French colonies in America', null, 1804,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B2048242', 'B1969630');
-- 2

-- Le Carénage : comme Sainte-Lucie
select * from etats e2 where uhgs_id = 'B1969741'
delete from etats where uhgs_id = 'B1969630'

insert into etats (etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'France', 'colonies françaises d''Amérique', 'France', 'French colonies in America', null, 1814,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B1969630');

insert into etats (etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Grande-Bretagne', 'colonies britanniques d''Amérique', 'Great Britain', 'British colonies in America', 1815, null,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B1969630');

-- Port-Louis (Tobago)	B1975512 
-- https://www.google.fr/maps/place/Tobago/@11.2481375,-60.6616201,12.5z/data=!4m5!3m4!1s0x8c49b2acf4564d43:0xcefb6c66b937c003!8m2!3d11.2336911!4d-60.6988909
-- https://fr.wikipedia.org/wiki/Port-Louis_(Maurice)
select * from etats where uhgs_id in ('B1975512');
insert into etats (country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Trinidad and Tobago', 'France', 'colonies françaises d''Amérique', 'France', 'French colonies in America', null, 1763,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B1975512');
 
insert into etats (country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Trinidad and Tobago', 'France', 'colonies françaises d''Amérique', 'France', 'French colonies in America', 1783, 1794,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B1975512');

insert into etats (country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Trinidad and Tobago', 'Grande-Bretagne', 'colonies britanniques d''Amérique', 'Great Britain', 'British colonies in America', 1763, 1783,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B1975512');

insert into etats (country2019_name, etat, subunit, etat_en , subunit_en , dfrom, dto , 
 toponyme , toponyme_standard_fr , uhgs_id)
 SELECT 'Trinidad and Tobago', 'Grande-Bretagne', 'colonies britanniques d''Amérique', 'Great Britain', 'British colonies in America', 1783, 1815,
 toponyme_standard_fr, toponyme_standard_fr, uhgs_id  
 FROM port_points WHERE uhgs_id in ('B1975512');

select * from world_borders wb where "name" like '%Tobago%'


SELECT toponyme, toponyme_standard_fr, country2019_name, partner_balance_1789, partner_balance_supp_1789
FROM port_points WHERE uhgs_id in ('A0189004');

update port_points set toponyme = toponyme_standard_fr, country2019_name = 'France' , partner_balance_supp_1789 = 'France' 
where uhgs_id in ('A0189004'); 

delete from port_points where uhgs_id = 'B2054985' -- jacmel en doublon
Cape Tiburon	B2047006
Léogâne 	B2045171
Miragoâne 	B2062485

UPDATE ports.port_points SET nb_conges_1787_inputdone = nb_conges_1787_inputdone+1 WHERE toponyme_standard_fr = 'Royan'
UPDATE ports.port_points SET nb_conges_1787_inputdone = null WHERE toponyme_standard_fr = 'Meschers'
update ports.obliques set port_toponyme_fr = 'Marans', port_uhgs_id='A1964694' where port = 'Alligre';
update ports.obliques set port_toponyme_fr = 'Royan', port_uhgs_id='A0172590' where port = 'Meschers';

select * from  port_points pp where toponyme % 'Aligre'

select * from etats where toponyme = 'Port-Louis (Tobago)'
select * from etats where uhgs_id = 'B1975512'
update etats set dfrom = 1794 where uhgs_id = 'B1975512' and dto = 1815
select  * from port_points where uhgs_id = 'B1975512'


-- toponyme = 'Port-Louis (Tobago)'

SELECT * FROM ports.port_points WHERE uhgs_id = 'A0183923'

-- Correction le 19 avril de ker Chalon
-- mail de Silvia/ Thierry le 12 avril 2021

SELECT * FROM ports.port_points WHERE uhgs_id =  'A1963870'  
-- A1963870	46.71658	-2.341046	Sables-d’Olonne	Poitou, ogc_fid = 1041

SELECT * FROM ports.port_points WHERE uhgs_id =  'A1969195'  
-- Châlon de Thierry est A1969195; 45.724946, -0.963790; 
-- amirauté de Marennes, Saintonge
delete from ports.port_points where ogc_fid = 1159 and uhgs_id =  'A1969195'  

insert into ports.port_points (uhgs_id, latitude, longitude, amiraute, province, 
toponyme, toponyme_standard_fr, toponyme_standard_en, 
geom, point3857, 
country2019_iso2code, country2019_name, country2019_region, 
belonging_states, belonging_states_en, belonging_substates, belonging_substates_en, state_1789_en, state_1789_fr, substate_1789_en, substate_1789_fr, relation_state, 
partner_balance_supp_1789, partner_balance_supp_1789_uncertainty)
select 'A1969195', 45.724946, -0.963790, 'Marennes', 'Saintonge', 'Châlon', 'Châlon', 'Châlon',
st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), st_setsrid(st_transform(st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), 3857), 3857),
country2019_iso2code, country2019_name, country2019_region, 
belonging_states, belonging_states_en, belonging_substates, belonging_substates_en, state_1789_en, state_1789_fr, substate_1789_en, substate_1789_fr, relation_state, 
partner_balance_supp_1789, partner_balance_supp_1789_uncertainty
FROM ports.port_points WHERE uhgs_id =  'A1963870' ;

update ports.port_points set shiparea='ACE-ROCH' where uhgs_id =  'A1969195';

-- Modifier toutes les entrées de pointcall qui utilisent A1963870 pour mettre à la place A1969195
select * from navigoviz.pointcall p where p.pointcall_uhgs_id = 'A1963870';
select * from navigoviz.pointcall p where p.pointcall_uhgs_id = 'A1969195';

update navigoviz.pointcall 
set pointcall_uhgs_id = 'A1969195', toponyme_fr='Châlon', toponyme_en='Châlon',
latitude=45.724946, longitude=-0.963790, pointcall_admiralty='Marennes', pointcall_province = 'Saintonge', shiparea='ACE-ROCH', 
pointcall_point = st_setsrid(st_transform(st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), 3857), 3857)
where pointcall_uhgs_id = 'A1963870';
-- 216 

update navigoviz.pointcall 
set homeport='Chalon', homeport_uhgs_id='A1969195', homeport_toponyme_fr='Châlon',homeport_toponyme_en='Châlon',
homeport_latitude=45.724946, homeport_longitude=-0.963790,
homeport_admiralty='Marennes', homeport_province = 'Saintonge'
where homeport_uhgs_id = 'A1963870';
-- 821

/*
update navigoviz.pointcall 
set homeport='Châlon', homeport_toponyme_fr='Châlon',homeport_toponyme_en='Châlon'
where homeport_uhgs_id='A1969195';

update navigoviz.pointcall 
set toponyme_fr='Châlon', toponyme_en='Châlon'
where pointcall_uhgs_id='A1969195';
*/

-- Modifier toutes les entrées de travels qui utilisent A1963870 pour mettre à la place A1969195
select * from navigoviz.built_travels bt where departure_uhgs_id = 'A1963870';
-- 201 / 202

update navigoviz.built_travels 
set departure_uhgs_id = 'A1969195', departure_fr='Châlon', departure_en='Châlon',
departure_latitude=45.724946, departure_longitude=-0.963790, departure_admiralty='Marennes', departure_province = 'Saintonge', 
departure_shiparea='ACE-ROCH', 
departure_point = st_setsrid(st_transform(st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), 3857), 3857)
where departure_uhgs_id = 'A1963870';

select * from navigoviz.built_travels bt where destination_uhgs_id = 'A1963870';
-- 114 / 226

update navigoviz.built_travels 
set destination_uhgs_id = 'A1969195', destination_fr='Châlon', destination_en='Châlon',
destination_latitude=45.724946, destination_longitude=-0.963790, destination_admiralty='Marennes', destination_province = 'Saintonge', 
destination_shiparea='ACE-ROCH', 
destination_point = st_setsrid(st_transform(st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), 3857), 3857)
where destination_uhgs_id = 'A1963870';
-- 226


update navigoviz.raw_flows 
set destination_uhgs_id = 'A1969195', destination_fr='Châlon', destination_en='Châlon',
destination_latitude=45.724946, destination_longitude=-0.963790, destination_admiralty='Marennes', destination_province = 'Saintonge', 
destination_shiparea='ACE-ROCH', 
destination_point = st_setsrid(st_transform(st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), 3857), 3857)
where destination_uhgs_id = 'A1963870';
-- 215

update navigoviz.raw_flows 
set departure_uhgs_id = 'A1969195', departure_fr='Châlon', departure_en='Châlon',
departure_latitude=45.724946, departure_longitude=-0.963790, departure_admiralty='Marennes', departure_province = 'Saintonge', 
departure_shiparea='ACE-ROCH', 
departure_point = st_setsrid(st_transform(st_setsrid(st_makepoint(-0.963790, 45.724946), 4326), 3857), 3857)
where departure_uhgs_id = 'A1963870';
-- 10

select id2, simtext, distgeo, certainity from ports.matching_port 
        where source1 = 'geo_general' and source2='geonames' and uhgs_id ='A1963870' and best is true
     
-- Mail du 23 avril 2021
-- Correction de l'amirauté de Tonnay-Charentes 
SELECT toponyme, amiraute, province, toponyme_standard_fr , toponyme_standard_en FROM ports.port_points where uhgs_id =  'A0171758';
-- Charente	La Rochelle	Aunis	Tonnay-Charente	Tonnay-Charente
UPDATE ports.port_points set amiraute='Marennes', province='Saintonge' where uhgs_id =  'A0171758';
update navigoviz.pointcall set pointcall_admiralty='Marennes', pointcall_province = 'Saintonge' where pointcall_uhgs_id = 'A0171758';-- 2898
update navigoviz.pointcall set homeport_admiralty='Marennes', homeport_province = 'Saintonge' where homeport_uhgs_id = 'A0171758';--1065
update navigoviz.built_travels set departure_admiralty='Marennes', departure_province = 'Saintonge' where departure_uhgs_id = 'A0171758';--1860
update navigoviz.built_travels set destination_admiralty='Marennes', destination_province = 'Saintonge' where destination_uhgs_id = 'A0171758';--1738

SELECT toponyme, amiraute, province, toponyme_standard_fr , toponyme_standard_en FROM ports.port_points where uhgs_id =  'A0171758';


-- Mies à jour sur mon localhost des attributs port_bureau de ferme
-- sur la base d'un fichier posté par G. Daudin en CSV le 26 mai pour la suite du datasrpint
-- table liste_direction_bureaux

select * from ports.liste_direction_bureaux 
where is_maritime is true 
order by customs_region
-- and customs_region = 'La Rochelle'
-- customs_region , customs_office 
select * from ports.liste_direction_bureaux 
where is_maritime is true and customs_region = 'Lorient' AND is_ok = true

update  ports.liste_direction_bureaux set is_maritime = false where customs_region = 'Soissons' and customs_office = 'Aubenton'
update  ports.liste_direction_bureaux set is_maritime = false where customs_region = 'Passeport du roy' 
update  ports.liste_direction_bureaux set is_maritime = false where customs_region = 'Passeports' 



alter table ports.liste_direction_bureaux add column is_ok boolean default true;
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Bordeaux, Saint-Malo, Rouen, Caen, La Rochelle'
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Rouen' and customs_office = 'Cherbourg'
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Saint-Malo' and customs_office = 'Le Légué et dépendances'
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Saint-Malo' and customs_office = 'Saint-Malo et Saint-Serven'
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Montpellier' and customs_office is null
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Lorient' and customs_office is null

update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Lorient' and (customs_office like '% Lorient' or customs_office like 'Lorient %')
--Lorient (Commerce particulier)
--Lorient (Compagnie) Cargaisons préparée en 1788. Sur des vaisseaux partis en 1789
--Port franc de Lorient
--Ville de Lorient
--Subordonné de Lorient
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Lorient' and (customs_office like 'Brest et %' )
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Bayonne' and (customs_office like 'Saint-Esprit de Bayonne' )
update  ports.liste_direction_bureaux set is_ok = false where customs_region = 'Montpellier' and (customs_office like 'Foire de Beaucaire' )
	

	


SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Dunkerque'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr = 'Saint-Esprit'
 -- A0165801	Saint-Esprit	Bayonne	Guyenne

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Bayonne'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Saint-Valéry-sur-Somme'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Boulogne'


SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Calais'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Bordeaux'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Caen'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr = 'Granville'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute = 'Granville'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme like '%vranche%'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Cherbourg'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Dunkerque'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.province like 'Normandie'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Lille'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Tourcoing'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Armentières'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Bailleul'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Baisieux'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Douai'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Lorient'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Lorient'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Brest'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Quimper'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Marseille'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Arles'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Lambesc'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Penne%'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr ilike 'Pont-Saint-Esprit'


SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Agde'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Sète'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like '%Beaucaire%'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Villeneuve-lès-Avignons'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Arles%'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.province like 'Roussillon'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr ilike '%Marans%'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Marennes'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Narbonne'
	
SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Perpignan'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Prades'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Dieppe'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Honfleur'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like '%Havre'
-- Attention, mettre à jour l'amirauté Le Havre pour A0196246 et A0204500
--A0196246	Tancarville	Havre
--A0187101	Le Havre	Le Havre
--A0204500	Harfleur	Havre

UPDATE ports.port_points set amiraute='Le Havre', province='Normandie' where uhgs_id in  ('A0196246', 'A0204500');
update navigoviz.pointcall set pointcall_admiralty='Le Havre', pointcall_province = 'Normandie' where pointcall_uhgs_id in  ('A0196246', 'A0204500');-- 198
update navigoviz.pointcall set homeport_admiralty='Le Havre', homeport_province = 'Normandie' where homeport_uhgs_id in  ('A0196246', 'A0204500');--35
update navigoviz.built_travels set departure_admiralty='Le Havre', departure_province = 'Normandie' where departure_uhgs_id in  ('A0196246', 'A0204500');--204
update navigoviz.built_travels set destination_admiralty='Le Havre', destination_province = 'Normandie' where destination_uhgs_id in  ('A0196246', 'A0204500');--236
-- fait en local et sur le serveur le 23 juin 2021

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Rouen'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Saint-Malo'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute like 'Morlaix'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr ilike '%Légué%'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Saint%Tropez'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Toulon'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Fréjus'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Antibes'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Saint-Brieuc'
-- le Légué est un bureau de ferme de Saint-Malo
A0180597	Dahouët 
A0174281	Le Légué 
A0122932	Saint-Brieuc
A0134417	Saint-Cast
A0199798	île-de-Bréhat
A0198247	Plancoët
A0127863	Binic
A0160634	Plevenon
A0189350	Erquy
A0214583	Paimpol

SELECT  distinct pp.amiraute, pp.province
FROM ports.port_points pp order by province, amiraute

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Vannes'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.toponyme_standard_fr ilike 'Vannes'


SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Nantes'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Calais'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Calais'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Fécamp'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Saint-Valéry-en-Caux'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Saint-Brieuc'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Abbeville'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Martigues'


SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Port-Bail'
SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Isigny'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Coutances'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Bayeux'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Barfleur'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp WHERE pp.amiraute ilike 'Caudebec et Quilleboeuf'

select distinct main_port_toponyme, amiraute, province from  navigoviz.source, ports.port_points pp
where main_port_uhgs_id = pp.uhgs_id and province = 'Bretagne'
order by amiraute 
-- liste des ports sans bureaux de ferme
SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province
FROM ports.port_points pp 
where  pp.amiraute in ('Saint-Brieuc', 'Vannes', 'Ajaccio', 'Aigues-Mortes', 'Barfleur', 'Bayeux', 
'Caudebec et Quilleboeuf', 'Coutances', 'Eu et Tréport', 'Fécamp', 'Isigny', 'Port-Bail', 'Quillebeuf', 
'Saint-Valéry-en-Caux', 'Abbeville', 'Martigues')
order by province, amiraute 



select pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province, pp.ferme_bureau , pp.ferme_direction 
from ports.port_points pp 
where pp.state_1789_fr = 'France'
order by amiraute 

select pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province, 
pp.ferme_bureau , pp.ferme_direction, 
ferme.customs_office , ferme.customs_region , 
difference(ferme.customs_office, pp.toponyme_standard_fr) as d, 
difference(ferme.customs_office, pp.amiraute) as d2
from ports.port_points pp , ports.liste_direction_bureaux ferme
where pp.state_1789_fr = 'France' and ferme.customs_office % pp.toponyme_standard_fr
and ferme.is_maritime is true
order by toponyme_standard_fr, d, d2
-- amiraute , 

select pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province, pp.ferme_bureau , pp.ferme_direction, 
ferme.customs_office , ferme.customs_region , difference(ferme.customs_office, pp.toponyme_standard_fr) as d, difference(ferme.customs_office, pp.amiraute) as d2
from ports.port_points pp , ports.liste_direction_bureaux ferme
where pp.state_1789_fr = 'France' and ferme.customs_office % pp.toponyme_standard_fr
and ferme.is_maritime is true
order by amiraute,  d2

select max(d), uhgs_id
from (
select pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province, pp.ferme_bureau , pp.ferme_direction, 
ferme.customs_office , ferme.customs_region , difference(ferme.customs_office, pp.toponyme_standard_fr) as d, difference(ferme.customs_office, pp.toponyme_standard_fr) as d2
from ports.port_points pp , ports.liste_direction_bureaux ferme
where pp.state_1789_fr = 'France' and ferme.customs_office % pp.toponyme_standard_fr
and ferme.is_maritime is true
) 
group  by uhgs_id, d, d2 

--- Mail de Silvia du 21 juin
-- Port : a0180365 Groix Lorient
-- Changement d'Amirauté (pas de Province, qui est la Bretagne) Il était faussement dit à Vannes.
-- et A1968952	Courreaux de Groix ? question Christine

select uhgs_id, toponyme_standard_fr, amiraute, province 
from ports.port_points
where uhgs_id = 'A0180365' 
-- A0180365	Groix	Vannes	Bretagne

UPDATE ports.port_points set amiraute='Lorient', province='Bretagne' where uhgs_id in  ('A0180365', 'A1968952'); --2
update navigoviz.pointcall set pointcall_admiralty='Lorient', pointcall_province = 'Bretagne' where pointcall_uhgs_id in  ('A0180365', 'A1968952');-- 5
update navigoviz.pointcall set homeport_admiralty='Lorient', homeport_province = 'Bretagne' where homeport_uhgs_id in  ('A0180365', 'A1968952');--106
update navigoviz.built_travels set departure_admiralty='Lorient', departure_province = 'Bretagne' where departure_uhgs_id in  ('A0180365', 'A1968952');--0
update navigoviz.built_travels set destination_admiralty='Lorient', destination_province = 'Bretagne' where destination_uhgs_id in  ('A0180365', 'A1968952');--1

select * from navigoviz.pointcall where pointcall_uhgs_id = 'A1968952'

select * from navigoviz.built_travels where extract(year from outdate_fixed) = 1789 and destination_uhgs_id = 'A1968952'

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province, pp.ferme_bureau, pp.ferme_direction 
FROM ports.port_points pp WHERE pp.toponyme_standard_fr ilike 'Beauvoir-sur-Mer'
-- A0181608

SELECT  pp.uhgs_id, pp.toponyme_standard_fr, pp.amiraute, pp.province, pp.ferme_bureau, pp.ferme_direction 
FROM ports.port_points pp WHERE pp.toponyme_standard_fr ilike 'La Hougue'


-- le 28 juin 2021 
-- Impacts de l'attribution des bureaux et directions de ferme à chaque port
-- TABLE port_points : tous les ports 
--ferme_direction
--ferme_direction_uncertainty : ajouter l'attribut à 0 si certain,  -1 si doute  
--ferme_bureau
--ferme_bureau_uncertainty
-- TABLE navigoviz.pointcall
--ferme_direction
--ferme_direction_uncertainty : ajouter l'attribut à 0 si certain,  -1 si doute  
--ferme_bureau
--ferme_bureau_uncertainty
-- TABLE navigoviz.built_travels
--departure_ferme_direction
--departure_ferme_direction_uncertainty : ajouter l'attribut à 0 si certain,  -1 si doute  
--departure_ferme_bureau
--departure_ferme_bureau_uncertainty
--destination_ferme_direction
--destination_ferme_direction_uncertainty : ajouter l'attribut à 0 si certain,  -1 si doute  
--destination_ferme_bureau
--destination_ferme_bureau_uncertainty

-- analyse les 23 et 24 juin 2021, échanges de mail et coups de fil. Mail le 28 juin 2021

-- Principe : une amirauté et ses ports sont affectés dans leur ensemble à la Direction et du bureau 
-- dont on retrouve le nom dans la source Toflit  F12 1906 (qui ne mentionne pas la Direction de Caen, attention !)
-- Parfois, des ports de certaines amirautés sont mentionnés comme ayant expréssement leur bureau de direction, 
-- et la direction peut parfois (2 cas : Beauvoir-sur-Mer et Gravelines) être différente de celle de l'Amirauté

-- Enfin, certaines amirautés n'ont jamais été mentionnées dans la source Toflit, et par un jeu de déduction géographique on les a affecté à telle ou telle direction.
--Pour la Corse, je ne crois pas que nous allons trouver des fermes. Prends-là comme une tout ? Comme pas de congés, on laisse tomber.
--Abbeville est dans la direction d’Amiens dans F12 1906 (sur)
--Aigues morte est entre Beaucaire (direction de Montpellier) et Montpellier. Donc direction de Montpellier ?
--Barfleur et Port-Bail (bureau de Cherbourg ? ), Bayeux et Isigny (bureau de Caen sans doute) , Coutances (Granville, doute), est entre Caen et Avranches (direction de Caen), donc direction de Caen ?
--Caudebec, Quillebœuf entre Rouen et Le Havre (direction de Rouen). Donc direction de Rouen. Bureau de Rouen (doute)
--Le Tréport en Normandie, donc Rouen (il y a un doute avec Amiens)
--Martigues est entre Arles (direction de Marseille) et Marseille, donc Marseille. Bureau d’Arles avec un doute ?
-- La Hougue, c’est entre Avranche et Caen, tous les deux dans la direction de Caen. Donc Direction de Caen ?


-- Décision prise pour les PORTS FRANCS : les ports des amirautés de ports francs non cité comme bureau de ferme 
-- appartiennent à la Direction mentionnée pour le port franc et ont pour bureau le port franc
--Hendaye et Soccoa appartiendront donc au bureau Bayonne de la ferme Bayonne
--Port-Louis , Hennebont et Groix et Courreaux de Groix seront affectés au bureau de Lorient, Ferme de Lorient
--Cassis,Côtes de Provence, Frioul, Marignane seront affectés au bureau de Marseille, Ferme de Marseille
--Dunkerque (bureau de Dunkerque) appartient à la direction de Lille


alter table ports.port_points ADD COLUMN ferme_direction_uncertainty int ;
alter table navigoviz.pointcall ADD COLUMN ferme_direction_uncertainty int ;
alter table navigoviz.built_travels ADD COLUMN departure_ferme_direction_uncertainty int ;
alter table navigoviz.built_travels ADD COLUMN destination_ferme_direction_uncertainty int ;

-- Amiens	Saint-Valery-sur-Somme
UPDATE ports.port_points 
set ferme_direction='Amiens',  ferme_bureau='Saint-Valéry-sur-Somme',
ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
where amiraute in  ('Saint-Valéry-sur-Somme'); --1
UPDATE navigoviz.pointcall 
set ferme_direction='Amiens',  ferme_bureau='Saint-Valéry-sur-Somme',
ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
where pointcall_admiralty in  ('Saint-Valéry-sur-Somme'); --497
UPDATE navigoviz.built_travels 
set departure_ferme_direction='Amiens',  departure_ferme_bureau='Saint-Valéry-sur-Somme',
departure_ferme_direction_uncertainty=0 , departure_ferme_bureau_uncertainty=0
where departure_admiralty in  ('Saint-Valéry-sur-Somme'); --404
UPDATE navigoviz.built_travels 
set destination_ferme_direction='Amiens',  destination_ferme_bureau='Saint-Valéry-sur-Somme',
destination_ferme_direction_uncertainty=0 , destination_ferme_bureau_uncertainty=0
where destination_admiralty in  ('Saint-Valéry-sur-Somme'); --415

-- DROP FUNCTION update_ferme(character varying,character varying,character varying)
CREATE OR REPLACE FUNCTION ports.update_ferme(p_amiraute character varying, direction character varying, bureau character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
        	nb_ports integer;
    begin
                -- EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
	            EXECUTE 'select count(*) from ports.port_points pp where amiraute = '||quote_literal(p_amiraute) into  nb_ports; 

                EXECUTE 'UPDATE ports.port_points 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where amiraute =  '||quote_literal(p_amiraute); --1
				EXECUTE 'UPDATE navigoviz.pointcall 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where pointcall_admiralty =  '||quote_literal(p_amiraute); --497
				EXECUTE 'UPDATE navigoviz.built_travels 
					set departure_ferme_direction='||quote_literal(direction)||',  departure_ferme_bureau='||quote_literal(bureau)||',
					departure_ferme_direction_uncertainty=0 , departure_ferme_bureau_uncertainty=0
					where departure_admiralty =  '||quote_literal(p_amiraute); --404
				EXECUTE 'UPDATE navigoviz.built_travels 
					set destination_ferme_direction='||quote_literal(direction)||',  destination_ferme_bureau='||quote_literal(bureau)||',
					destination_ferme_direction_uncertainty=0 , destination_ferme_bureau_uncertainty=0
					where destination_admiralty = '||quote_literal(p_amiraute); --415
                return nb_ports;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return -1;
            end;
$function$;

CREATE OR REPLACE FUNCTION ports.update_ferme_unport(p_toponyme_standard_fr character varying, direction character varying, bureau character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
        	nb_ports integer;
    begin
                -- EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
	            EXECUTE 'select count(*) from ports.port_points pp where toponyme_standard_fr = '||quote_literal(p_toponyme_standard_fr) into  nb_ports; 

                EXECUTE 'UPDATE ports.port_points 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where toponyme_standard_fr =  '||quote_literal(p_toponyme_standard_fr); --1
				EXECUTE 'UPDATE navigoviz.pointcall 
					set ferme_direction='||quote_literal(direction)||',  ferme_bureau='||quote_literal(bureau)||',
					ferme_direction_uncertainty=0 , ferme_bureau_uncertainty=0
					where toponyme_fr =  '||quote_literal(p_toponyme_standard_fr); --497
				EXECUTE 'UPDATE navigoviz.built_travels 
					set departure_ferme_direction='||quote_literal(direction)||',  departure_ferme_bureau='||quote_literal(bureau)||',
					departure_ferme_direction_uncertainty=0 , departure_ferme_bureau_uncertainty=0
					where departure_fr =  '||quote_literal(p_toponyme_standard_fr); --404
				EXECUTE 'UPDATE navigoviz.built_travels 
					set destination_ferme_direction='||quote_literal(direction)||',  destination_ferme_bureau='||quote_literal(bureau)||',
					destination_ferme_direction_uncertainty=0 , destination_ferme_bureau_uncertainty=0
					where destination_fr = '||quote_literal(p_toponyme_standard_fr); --415
                return nb_ports;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return -1;
            end;
$function$;

select ports.update_ferme('Boulogne-sur-Mer', 'Amiens', 'Boulogne-sur-Mer');
select ports.update_ferme('Boulogne', 'Amiens', 'Boulogne-sur-Mer');
-- Vérif de la fonction
select ferme_direction, ferme_bureau,  ferme_direction_uncertainty, ferme_bureau_uncertainty from ports.port_points pp where amiraute = 'Boulogne'  
select ferme_direction, ferme_bureau,  ferme_direction_uncertainty, ferme_bureau_uncertainty from navigoviz.pointcall pp where pointcall_admiralty = 'Boulogne'  
select departure_ferme_direction, departure_ferme_bureau,  departure_ferme_direction_uncertainty, departure_ferme_bureau_uncertainty from navigoviz.built_travels pp where departure_admiralty = 'Boulogne'  
select destination_ferme_direction, destination_ferme_bureau,  destination_ferme_direction_uncertainty, destination_ferme_bureau_uncertainty from navigoviz.built_travels pp where destination_admiralty = 'Boulogne'  

select ports.update_ferme('Saint-Valéry-sur-Somme', 'Amiens', 'Saint-Valéry-sur-Somme');
select ports.update_ferme('Boulogne', 'Amiens', 'Boulogne-sur-Mer');
select ports.update_ferme('Calais', 'Amiens', 'Calais');
select ports.update_ferme('Abbeville', 'Amiens', 'Abbeville');

select ports.update_ferme('Bayonne', 'Bayonne', 'Bayonne');
select ports.update_ferme_unport('Saint-Jean-de-Luz ', 'Bayonne', 'Saint-Jean-de-Luz');
select ports.update_ferme_unport('Saint-Esprit', 'Bayonne', 'Saint-Esprit');

select ports.update_ferme('Bordeaux', 'Bordeaux', 'Bordeaux');
select ports.update_ferme_unport('Libourne', 'Bordeaux', 'Libourne');
select ports.update_ferme_unport('Saint-André de Cubzac', 'Bordeaux', 'Libourne');--DOUTE Christine

select ports.update_ferme_unport('La Teste', 'Bordeaux', 'La Teste');
select ports.update_ferme_unport('Bourg', 'Bordeaux', 'Bourg');
select ports.update_ferme_unport('Castelnau', 'Bordeaux', 'Castelnau');
select ports.update_ferme_unport('Blaye', 'Bordeaux', 'Blaye');

select ports.update_ferme('Granville', 'Caen', 'Granville');
select ports.update_ferme('Coutances', 'Caen', 'Granville');
select ports.update_ferme('Caen', 'Caen', 'Caen');--8
select ports.update_ferme('Isigny', 'Caen', 'Caen');--5
select ports.update_ferme('Bayeux', 'Caen', 'Caen');--3
select ports.update_ferme('Cherbourg', 'Caen', 'Cherbourg');--6
select ports.update_ferme('Port-Bail', 'Caen', 'Cherbourg');--2 doute bureau
select ports.update_ferme('Barfleur', 'Caen', 'Cherbourg');--5 doute bureau
select ports.update_ferme('La Hougue', 'Caen', 'Caen');--2 doute bureau


select ports.update_ferme_unport('Dunkerque', 'Lille', 'Dunkerque');--1
select ports.update_ferme_unport('Gravelines', 'Lille', 'Gravelines');--1

select ports.update_ferme('Brest', 'Lorient', 'Brest');--17
select ports.update_ferme('Lorient', 'Lorient', 'Lorient');--5
select ports.update_ferme('Quimper', 'Lorient', 'Quimper');--20
select ports.update_ferme('Vannes', 'Lorient', 'Vannes');--26
select ports.update_ferme_unport('Redon', 'Lorient', 'Redon');--1

select ports.update_ferme('Arles', 'Marseille', 'Arles');--5
select ports.update_ferme('Martigues', 'Marseille', 'Arles');--4 doute bureau
select ports.update_ferme('La Ciotat', 'Marseille', 'La Ciotat');--3
select ports.update_ferme('Marseille', 'Marseille', 'Marseille');--5

select ports.update_ferme('Agde', 'Montpellier', 'Agde');--2
select ports.update_ferme_unport('Beaucaire', 'Montpellier', 'Beaucaire');--1
select ports.update_ferme_unport('Sète', 'Montpellier', 'Sète');--1
select ports.update_ferme_unport('Aigues-Mortes', 'Montpellier', 'Montpellier');--1, doute bureau

select ports.update_ferme('Nantes', 'Nantes', 'Nantes');--15
select ports.update_ferme_unport('Le Croisic', 'Nantes', 'Le Croisic');--1
select ports.update_ferme_unport('Beauvoir-sur-Mer', 'Nantes', 'Beauvoir-sur-Mer');--1
select ports.update_ferme_unport('Le Pouliguen', 'Nantes', 'Le Pouliguen');--1 doute si bureau
select ports.update_ferme_unport('Paimbœuf', 'Nantes', 'Paimbœuf');--1

select ports.update_ferme('Collioure', 'Narbonne', 'Collioure');--7
select ports.update_ferme('Narbonne', 'Narbonne', 'Narbonne');--2
select ports.update_ferme('Collioure', 'Narbonne', 'Collioure');--7
select ports.update_ferme('Narbonne', 'Narbonne', 'Narbonne');--2

select ports.update_ferme('Dieppe', 'Rouen', 'Dieppe');--2
select ports.update_ferme('Honfleur', 'Rouen', 'Honfleur');--5
select ports.update_ferme('Le Havre', 'Rouen', 'Le Havre');--3
select ports.update_ferme('Rouen', 'Rouen', 'Rouen');--3
select ports.update_ferme('Caudebec et Quilleboeuf', 'Rouen', 'Caudebec');--6 DOUTE BUREAU
select ports.update_ferme('Eu et Tréport', 'Rouen', 'Rouen');--1  (doute Ferme et bureau)
select ports.update_ferme('Fécamp', 'Rouen', 'Fécamp');--2  
select ports.update_ferme('Quillebeuf', 'Rouen', 'Quillebeuf');--1  
select ports.update_ferme('Saint-Valéry-en-Caux', 'Rouen', 'Saint-Valéry-en-Caux');--1  

select ports.update_ferme('Saint-Malo', 'Saint-Malo', 'Saint-Malo');--9  
select ports.update_ferme_unport('Saint-Servant', 'Saint-Malo', 'Saint-Servant');--1
select ports.update_ferme('Saint-Brieuc', 'Saint-Malo', 'Saint-Brieuc');--10  
select ports.update_ferme_unport('Le Légué', 'Saint-Malo', 'Le Légué');--1
select ports.update_ferme_unport('Binic', 'Saint-Malo', 'Binic');--1
select ports.update_ferme_unport('Paimpol', 'Saint-Malo', 'Paimpol');--1
select ports.update_ferme('Morlaix', 'Saint-Malo', 'Morlaix');--9 
 
select ports.update_ferme('Saint-Tropez', 'Toulon', 'Saint-Tropez');--2
select ports.update_ferme('Antibes', 'Toulon', 'Cannes');--8
select ports.update_ferme_unport('Antibes', 'Toulon', 'Antibes');--1
select ports.update_ferme('Toulon', 'Toulon', 'Toulon');--21
select ports.update_ferme_unport('La Seyne', 'Toulon', 'La Seyne');--1
select ports.update_ferme('Fréjus', 'Toulon', 'Saint-Raphaël');--5

select ports.update_ferme_unport('Le Plomb', 'La Rochelle', 'La Rochelle');--1
select ports.update_ferme_unport('île d''Aix', 'La Rochelle', 'La Rochelle');--1
select ports.update_ferme_unport('mer des Pertuis', 'La Rochelle', 'La Rochelle');--1
select ports.update_ferme_unport('rade de la Pallice', 'La Rochelle', 'La Rochelle');--1
select ports.update_ferme_unport('Le Plomb', 'La Rochelle', 'La Rochelle');--1
select ports.update_ferme_unport('Fouras', 'La Rochelle', 'La Rochelle');-- ok Thierry

select ports.update_ferme_unport('Loix', 'La Rochelle', 'Saint-Martin île de Ré');--1
select ports.update_ferme_unport('La Couarde', 'La Rochelle', 'Saint-Martin île de Ré');--1
select ports.update_ferme_unport('île de Ré', 'La Rochelle', 'Saint-Martin île de Ré');--1
select ports.update_ferme_unport('Rivedoux', 'La Rochelle', 'Saint-Martin île de Ré');--1

select ports.update_ferme_unport('Charron', 'La Rochelle', 'Alligre');-- ok Thierry
select ports.update_ferme_unport('pertuis Breton', 'La Rochelle', 'Alligre');-- ok Thierry
select ports.update_ferme_unport('Le Brault', 'La Rochelle', 'Alligre');-- ok Thierry
select ports.update_ferme_unport('L''Houmée', 'La Rochelle', 'Marennes');--1

select ports.update_ferme('Marennes', 'La Rochelle', 'Marennes');--31
-- introduction de Oléron comme bureau, à faire valider par Thierry
select ports.update_ferme_unport('coureau d''Oléron', 'La Rochelle', 'Oléron');--1
select ports.update_ferme_unport('Le Château-d''Oléron', 'La Rochelle', 'Oléron');--1
select ports.update_ferme_unport('Saint-Denis d''Oléron', 'La Rochelle', 'Oléron');--1
select ports.update_ferme_unport('La Brée', 'La Rochelle', 'Oléron');--1
select ports.update_ferme_unport('île d''Oléron', 'La Rochelle', 'Oléron');--1

select ports.update_ferme('Sables-d’Olonne', 'La Rochelle', 'Sables d''Olonne');--18
select ports.update_ferme_unport('Beauvoir-sur-Mer', 'Nantes', 'Beauvoir-sur-Mer');--1


---
-- DOUTES sur bureaux pour l'amirauté
UPDATE ports.port_points set ferme_bureau_uncertainty=-1 where amiraute in  ('Port-Bail', 'Barfleur', 'Martigues', 'Aigues-Mortes', 'Le Pouliguen', 'Caudebec et Quilleboeuf', 'Eu et Tréport', 'La Hougue'); --1
UPDATE navigoviz.pointcall set ferme_bureau_uncertainty=-1 where pointcall_admiralty in  ('Port-Bail', 'Barfleur', 'Martigues', 'Aigues-Mortes', 'Le Pouliguen', 'Caudebec et Quilleboeuf', 'Eu et Tréport', 'La Hougue'); --497
UPDATE navigoviz.built_travels set departure_ferme_bureau_uncertainty=-1 where departure_admiralty in  ('Port-Bail', 'Barfleur', 'Martigues', 'Aigues-Mortes', 'Le Pouliguen', 'Caudebec et Quilleboeuf', 'Eu et Tréport', 'La Hougue'); --404
UPDATE navigoviz.built_travels set  destination_ferme_bureau_uncertainty=-1 where destination_admiralty in  ('Port-Bail', 'Barfleur', 'Martigues', 'Aigues-Mortes', 'Le Pouliguen', 'Caudebec et Quilleboeuf', 'Eu et Tréport', 'La Hougue'); --415

-- DOUTES sur bureaux pour un port
UPDATE ports.port_points set ferme_bureau_uncertainty=-1 where uhgs_id in  ('A0122883'); --1
UPDATE navigoviz.pointcall set ferme_bureau_uncertainty=-1 where pointcall_uhgs_id in  ('A0122883'); --497
UPDATE navigoviz.built_travels set departure_ferme_bureau_uncertainty=-1 where departure_uhgs_id in  ('A0122883'); --404
UPDATE navigoviz.built_travels set  destination_ferme_bureau_uncertainty=-1 where destination_uhgs_id in  ('A0122883'); --415


UPDATE ports.port_points set ferme_direction_uncertainty = -1 where toponyme_standard_fr = 'Le Tréport';
UPDATE navigoviz.pointcall set ferme_direction_uncertainty = -1 where toponyme_fr = 'Le Tréport';
UPDATE navigoviz.built_travels set departure_ferme_direction_uncertainty = -1 where departure_fr = 'Le Tréport';
UPDATE navigoviz.built_travels set destination_ferme_direction_uncertainty = -1 where destination_fr = 'Le Tréport';
UPDATE ports.port_points set ferme_direction_uncertainty=0 where ferme_direction = 'La Rochelle';
UPDATE ports.port_points set ferme_bureau_uncertainty =0 where ferme_direction = 'Marennes';

SELECT ferme_direction, ferme_direction_uncertainty FROM ports.port_points where toponyme_standard_fr = 'Le Tréport';

select toponyme_standard_fr, amiraute, province, ferme_direction, ferme_bureau, ferme_direction_uncertainty, ferme_bureau_uncertainty , state_1789_fr 
from ports.port_points pp 
where state_1789_fr  = 'France' and ferme_direction is null
order by ferme_direction, ferme_bureau, province, amiraute

select toponyme_standard_fr, ferme_direction, ferme_bureau,  ferme_direction_uncertainty, ferme_bureau_uncertainty from ports.port_points pp where amiraute = 'Bayonne'  
select toponyme_fr, ferme_direction, ferme_bureau,  ferme_direction_uncertainty, ferme_bureau_uncertainty from navigoviz.pointcall p where pointcall_admiralty = 'Bayonne'  
select destination_fr, destination_ferme_direction, destination_ferme_bureau,  destination_ferme_direction_uncertainty, destination_ferme_bureau_uncertainty from navigoviz.built_travels pp where destination_admiralty = 'Bayonne'  

select toponyme_standard_fr from ports.port_points pp where toponyme_standard_fr like '%Seyne%' 
select toponyme_standard_fr from ports.port_points pp where uhgs_id like 'A0122594' 
Saint-Jean-de-Luz 

select uhgs_id, latitude, toponyme, toponyme_standard_fr, amiraute, province , nb_conges_1787_cr , nb_conges_1787_inputdone , nb_conges_1789_cr , nb_conges_1789_inputdone 
from ports.port_points pp where toponyme_standard_fr like 'Saint-André de Cubzac' 
-- A0122883	Saint André de Cuzac	Saint-André de Cubzac	La Rochelle	Aunis
-- 44.9963431012268, -0.4106238234052784
update ports.port_points pp set amiraute = 'Bordeaux', province='Guyenne', 
longitude = -0.4106238234052784, latitude = 44.9963431012268, geom = st_setsrid(st_makepoint(-0.4106238234052784, 44.9963431012268) , 4326)
where uhgs_id = 'A0122883';
update ports.port_points pp set point3857 = st_setsrid(st_transform(geom, 3857), 3857) ;
update ports.port_points pp set toponyme_standard_fr = 'Saint-André-de-Cubzac', toponyme_standard_en = 'Saint-André-de-Cubzac' 
where uhgs_id = 'A0122883';

-- ERREUR corrigée ensuite
-- update ports.port_points pp set toponyme_standard_fr = 'Saint-André-de-Cubzac', toponyme_standard_en = 'Saint-André-de-Cubzac' 


select uhgs_id, latitude, toponyme, toponyme_standard_fr, amiraute, province , nb_conges_1787_cr , nb_conges_1787_inputdone , nb_conges_1789_cr , nb_conges_1789_inputdone 
from ports.port_points  pp where toponyme_standard_fr like 'Saint-André-de-Cubzac' 

update ports.port_points pp set toponyme_standard_fr = toponyme_fr, toponyme_standard_en = toponyme_en
from  navigoviz.pointcall p  where p.pointcall_uhgs_id = pp.uhgs_id 
-- 1143

select uhgs_id, latitude, toponyme, toponyme_standard_fr, amiraute, province , nb_conges_1787_cr , nb_conges_1787_inputdone , nb_conges_1789_cr , nb_conges_1789_inputdone 
from ports.port_points pp where toponyme_standard_fr like 'Saint-André-de-Cubzac' and uhgs_id!= 'A0122883'

update ports.port_points set toponyme_standard_fr = toponyme, toponyme_standard_en = toponyme 
where toponyme_standard_fr like 'Saint-André-de-Cubzac' and uhgs_id!= 'A0122883' and toponyme is not  null
-- 6
A0214663	46.3666687	Le Brault
A0206654	49.4166679	Carentan
A1963870	46.71658	Chalon
A0183923	48.6333313	Saint-Briac
B2044556	19.7999992	--Môle-Saint-Nicolas
B2048242	19.667778	-- Fort-Dauphin
A0212540	49.2666664	Bayeux
B2049573	18.4313889	--Petit-Goave
B2056379	18.2666664	-- Saint-Louis
B1969630	14			-- Le Carénage
B2045254	19.8333321	-- Port-de-Paix
B1975512	11.1833334	-- Port-Louis (Tobago) / Saint-Louis
A0198704	47.5833321	La Trinité

select p.uhgs_id  , toponyme, toponyme_standard_fr , labels.fr , labels.en
from port_points p , labels_lang_csv labels 
where toponyme_standard_fr like 'Saint-André-de-Cubzac'  and p.uhgs_id = key_id and label_type = 'toponyme'
update labels_lang_csv set fr = 'Saint-André-de-Cubzac', en='Saint-André-de-Cubzac' where label_type = 'toponyme' and fr = 'Saint-André de Cubzac';

--B2044556		Saint-André-de-Cubzac	Môle-Saint-Nicolas	Môle-Saint-Nicolas
--A1963870	Chalon	Saint-André-de-Cubzac	Ker Châlon	Ker Châlon
update ports.port_points pp set toponyme_standard_fr = 'Môle-Saint-Nicolas', toponyme_standard_en = 'Môle-Saint-Nicolas'
where  pp.uhgs_id = 'B2044556';
update ports.port_points pp set toponyme_standard_fr = 'Ker Châlon', toponyme_standard_en = 'Ker Châlon'
where  pp.uhgs_id = 'A1963870';
update ports.port_points pp set toponyme_standard_fr = 'Fort-Dauphin', toponyme_standard_en = 'Fort-Dauphin'
where  pp.uhgs_id = 'B2048242';
update ports.port_points pp set toponyme_standard_fr = 'Petit-Goave', toponyme_standard_en = 'Petit-Goave'
where  pp.uhgs_id = 'B2049573';
update ports.port_points pp set toponyme_standard_fr = 'Saint-Louis', toponyme_standard_en = 'Saint-Louis'
where  pp.uhgs_id = 'B2056379';
update ports.port_points pp set toponyme_standard_fr = 'Le Carénage', toponyme_standard_en = 'Le Carénage'
where  pp.uhgs_id = 'B1969630';
update ports.port_points pp set toponyme_standard_fr = 'Port-de-Paix', toponyme_standard_en = 'Port-de-Paix'
where  pp.uhgs_id = 'B2045254';
update ports.port_points pp set toponyme_standard_fr = 'Port-Louis (Tobago)', toponyme_standard_en = 'Saint-Louis'
where  pp.uhgs_id = 'B1975512';

update ports.port_points pp set toponyme_standard_fr = 'Saint-Jean-de-Luz', toponyme_standard_en = 'Saint-Jean-de-Luz'
where  pp.toponyme_standard_fr = 'Saint-Jean-de-Luz ';
update labels_lang_csv set fr = 'Saint-Jean-de-Luz', en='Saint-Jean-de-Luz' where label_type = 'toponyme' and fr = 'Saint-Jean-de-Luz ';

update ports.port_points pp set toponyme_standard_fr = 'Saint-Valéry-en-Caux', toponyme_standard_en = 'Saint-Valéry-en-Caux'
where  pp.uhgs_id = 'A0135548';
update labels_lang_csv set fr = 'Saint-Valéry-en-Caux', en='Saint-Valéry-en-Caux' where label_type = 'toponyme' and fr = 'Saint-Valery en Caux';

---
-- 1 aout 2021
select distinct w.id, w.shortname, w.id_sup, w0.shortname 
from ports.world_1789 w, ports.world_1789 w0
where w0.unitlevel = 0 and w0.id = w.id_sup and 
-- Requete QGIS qui exporte des fonds mixte
 (w.unitlevel =0 and w.shortname = 'Etats-Unis d''Amérique') or (w.unitlevel = 2 and w.id_sup = 944 ) 
or (w.unitlevel = 1 and w.id != 944 and w.id_sup != 854)

------------------------
-- 31 aout 2021
-- Mise à jour du gazettier
-- Certains ports n'ont pas de geonameid identity : ils ont été insérés à la main depuis un fichier excel, et ne viennent pas de géo_general

select toponyme_standard_fr, ogc_fid, uhgs_id,  toustopos , topofreq, toponyme, point3857 
from ports.port_points where geonameid is null

/*
 * Châlon	1158	A1969195
Le Brault	1157	A0214663
35 lat. nord et 45 long. du méridien de Paris	349	A1968875
La Trinité	1156	A0198704
Môle-Saint-Nicolas	1148	B2044556
Fort-Dauphin	1146	B2048242
Petit-Goave	1149	B2049573
Saint-Louis	1152	B2056379
Bayeux	1153	A0212540
Le Carénage	1147	B1969630
Port-de-Paix	1150	B2045254
Port-Louis (Tobago)	1151	B1975512
Coutances	1145	A0189004
Carentan	1155	A0206654
Saint-Briac	1154	A0183923

 */

-- Pour cette raison, leurs champs topofreq, toponyme, toustopos sont null 
-- Pour que le script update_matching.sql fonctionne, il faut renseigner toustopo avec la valeur de toponyme_standard_fr 


UPDATE ports.port_points p SET toustopos= k.tab
from (
	select uhgs_id, array_agg(toponyme_standard_fr) as tab
	from ports.port_points
	where geonameid is NULL AND toustopos IS null
	group by uhgs_id ) as k 
where k.uhgs_id = p.uhgs_id and p.geonameid is NULL AND p.toustopos IS null
-- 14

UPDATE ports.port_points p set toponyme = toponyme_standard_fr where geonameid is NULL AND toponyme IS null
-- 7

-- utilisation du script update_matching.sql

-- préparation de la table gazetteer
CREATE TABLE ports.gazetteer (
	pkid varchar NOT NULL,
	linked_place_desc jsonb NULL,
	CONSTRAINT gazetteer_pkey PRIMARY KEY (pkid)
);

grant select on ports.gazetteer to api_user

-- Import de la table limite_amiraute.sql
-- sql dans  C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\gazetteer\limites_amirautes_202010281050.sql
psql -U postgres -h localhost -d portic_v6 -f /home/plumegeo/portic/limites_amirautes_202010281050.SQL

grant select on  ports.matching_port to api_user;
grant select on  ports.limites_amirautes to api_user;

truncate ports.gazetteer


UPDATE gazetteer 
SET    linked_place_desc = 
       REPLACE(linked_place_desc::text, '\', '')::jsonb where pkid = 'Flandre';

select linked_place_desc from ports.gazetteer where pkid = 'Flandre';
select linked_place_desc from ports.gazetteer where pkid like '%Corse%';


select distinct etat, subunit  from etats pp order by etat 

select distinct etat, subunit, etat_en, subunit_en, f.key_id, f.fr, f.en  
from etats pp,  labels_lang_csv f 
where etat_en % f.en
order by etat 

select key_id, fr, en from labels_lang_csv llc where label_type = 'flag' and en % 'Spain'

select key_id, fr, en from labels_lang_csv llc where label_type = 'flag' and fr % 'anglais'

select key_id, fr, en from labels_lang_csv llc where label_type = 'flag' and fr % 'français'

select distinct etat, subunit, etat_en, subunit_en
from etats
where etat not in (
select distinct etat
from etats pp,  labels_lang_csv f 
where etat_en % f.en)
order by etat 


select distinct subunit, subunit_en
from etats
order by subunit 
A0617755	Hollande	Holland
A0248908	Naples	Naples
A1964382	Royaume de Naples	Kingdom of Naples

select distinct amiraute  from port_points pp 
where province in ('Guyane', 'Saint-Domingue', 'Guadeloupe', 'Martinique', 'Tobago', 'Réunion' , 'Canada')
order by amiraute 

------------------------------------------------------------------------------------------------------------------------
-- le 05 janvier 2022
-- Mise à jour des nb_conge et nb_inputdone
-- Fichier ports_02avril_ajouts nb congés en cours_05janvier2022.csv
------------------------------------------------------------------------------------------------------------------------
select * from port_points  where uhgs_id = 'B1973619' -- absent -- La Trinité
select * from port_points  where uhgs_id = 'A0164751' -- absent -- Étel
select * from port_points  where uhgs_id = 'A0127400' -- Socoa
select * from port_points  where uhgs_id = 'A0128573' --île d'Oléron
select * from port_points  where uhgs_id = 'A0135801' --Saint-Pol-de-Léon


select pp.uhgs_id, pp.toponyme_standard_fr, pp.status, v.status , pp.has_a_clerk , v.has_a_clerk , 
pp.conges_1787_inputdone, pp.conges_1789_inputdone,  pp.nb_conges_1787_cr, pp.nb_conges_1789_cr
from port_points pp, ports.obliques_2avril2020 v where
pp.uhgs_id = v.uhgs_id and pp.state_1789_fr = 'France'  -- and pp.uhgs_id = 'B1973619' --and pp.uhgs_id = 'A0137359' 
and (
(pp.has_a_clerk is null and v.has_a_clerk is not null) or (pp.status is null and v.status is not null) 
 or (pp.has_a_clerk is not null and v.has_a_clerk is null)  or (pp.status is not null and v.status is  null)  )
 /* perdent leur statut oblique car pas de greffiers
A0128573	île d'Oléron	oblique		true	
A0127400	Socoa	oblique		true	
A0136872	Pontusval	oblique		true	
A0135801	Saint-Pol-de-Léon	oblique		true	
A0124198	Port-Louis	oblique		true
	(lui devient oblique par correction)
A0137359	Penpoul		oblique		true 
  */


select pp.uhgs_id, pp.toponyme_standard_fr, pp.status, pp.has_a_clerk , 
pp.conges_1787_inputdone, pp.conges_1789_inputdone,  pp.nb_conges_1787_cr, pp.nb_conges_1789_cr
from port_points pp, ports.obliques_2avril2020 v where
pp.uhgs_id = v.uhgs_id and pp.state_1789_fr = 'France'  -- and pp.uhgs_id = 'B1973619' --and pp.uhgs_id = 'A0137359' 
and (
(pp.nb_conges_1787_cr is null and v.nb_conges_1787_cr is not null) or (pp.nb_conges_1789_cr is null and v.nb_conges_1789_cr is not null) 
 or (pp.nb_conges_1787_cr is not null and v.nb_conges_1787_cr is null)  or (pp.nb_conges_1789_cr is not null and v.nb_conges_1789_cr is  null)  
 or (pp.nb_conges_1789_cr is not null and v.nb_conges_1789_cr is null)  or (pp.nb_conges_1789_cr is null and v.nb_conges_1789_cr is not null) )
 
 /*
  * A0169612	Argenton
A0137359	Penpoul
A0198999	La Rochelle
A0187101	Le Havre
A0136333	Narbonne
A0204263	Hennebont
A0180024	Cannes
B1977677	Basse-Terre
A0200330	Toulon
A0135548	Saint-Valéry-en-Caux
A0205141	Cancale
B1973450	Fort-Royal
A0132389	Bormes
A0218266	Carteret
A0218974	Ajaccio
A0214663	Le Brault
A0129606	Ouessant
A0206482	Audierne 
A0160686	Pont-Aven
A0128906	Saintes-Maries-de-la-Mer
A0185385	Morlaix
A0138533	Le Château-d'Oléron
A0206654	Carentan
A0186515	La Tremblade
A0218780	Agde
A0162516	Le Pouliguen
A0147257	Saint-Tropez
A0134267	Saint-Nazaire / Sanary-sur-Mer
A0213264	Tréguier
A0170819	Saint-Malo
A0167057	Granville
A0132243	Bénodet
B2048242	Fort-Dauphin
B1965595	Saint-Pierre-et-Miquelon
A0145176	Bandol
A0197884	Quiberon
A0165056	La Tranche-sur-Mer
A0127055	Saint-Martin-de-Ré
A0200627	Concarneau
A0124462	Pont-l'Abbé
A0145112	Cassis
A0214583	Paimpol
A0137954	Penerf
A0162246	Lannion
A0139494	Pontrieux 
A0131657	Belle-Île-en-Mer
A0207066	Port-Launay
A0206606	Aigues-Mortes
A0180812	Dives
A0186503	Locmariaquer
A0139174	Port-Navalo
A0202402	Sarzeau
A1965074	Vieille Roche
A0127306	Bonifacio
A0213721	Esnandes
A0177816	La Hougue
B1979312	Pointe-à-Pitre
A0173719	Muzillac
A0185868	Perros
A0211235	Luçon
A0212401	Lézardrieux 
A0199798	île-de-Bréhat
A0138920	Camaret
A0182952	Vannes
A0124996	Martigues
A0187969	Le Conquet
A0134176	Redon
A0152606	Boulogne-sur-Mer
A0142523	Douarnenez
A0123256	Le Pas au Bœuf
A0139070	Arles
A0140266	Lorient
A0198564	Le Faou
A0137148	Les Sables-d'Olonne
A0186080	Quimperlé
B2045254	Port-de-Paix
B1974029	Saint-Pierre
A0138383	Fréjus
A0166649	La Ciotat
A0170986	Sète
A0127863	Binic
A0198704	La Trinité
A0133408	Antibes
A0131832	Auray
A0187629	Dinan [Crozon]
A0213139	Le Croisic
A0212320	Le Tréport

  */
 
CREATE TABLE ports.port_points_backup05janvier22 AS (SELECT * FROM port_points)
 
update port_points pp set
has_a_clerk=v.has_a_clerk,
nb_conges_1787_inputdone=v.nb_conges_1787_inputdone,
nb_conges_1787_cr = v.nb_conges_1787_cr,
nb_conges_1789_inputdone=v.nb_conges_1789_inputdone,
nb_conges_1789_cr = v.nb_conges_1789_cr,
status=v.status
from ports.obliques_2avril2020  v
where v.uhgs_id = pp.uhgs_id ;
-- 1156

update port_points pp set oblique = has_a_clerk ;

-- d'après un fichier de Silvia en date de la réunion du 8 oct 2020 à Paris
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Reunions\2020-10-08_PARIS\liste nouvelle saisies PORTIC - suivi avancement.xlsx
-- nb_sante_1787_inputdone = 
update port_points pp set nb_sante_1787 = k.c
from 
(select pointcall_uhgs_id, count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'la Santé registre de patentes de Marseille' -- and p.pointcall_uhgs_id = 'A0210797'
and (p.source_component ilike '%200E, 545%' or p.source_component ilike '%200E, 543%' )
 and extract(year from indate_fixed) = 1787
 group by pointcall_uhgs_id
 ) as k 
where pp.uhgs_id = k.pointcall_uhgs_id
-- nb_sante_1787 = 3295

select distinct source_suite from navigoviz.pointcall p
/*
 * G5
Expéditions coloniales Marseille (1789)
la Santé registre de patentes de Marseille
Registre du petit cabotage (1786-1787)

 */

update port_points pp set nb_sante_1789 = k.c
from 
(select pointcall_uhgs_id, count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'la Santé registre de patentes de Marseille' -- and p.pointcall_uhgs_id = 'A0210797'
--and (p.source_component ilike '%200E, 545%' or p.source_component ilike '%200E, 543%' )
 and extract(year from indate_fixed) = 1789
 group by pointcall_uhgs_id
 ) as k 
where pp.uhgs_id = k.pointcall_uhgs_id
-- nb_sante_1789 = 3075

update port_points pp set nb_petitcabotage_1787 = k.c
from 
(select pointcall_uhgs_id, count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'Registre du petit cabotage (1786-1787)' -- and p.pointcall_uhgs_id = 'A0210797'
and (p.source_component ilike '%200 E 606%' or p.source_component ilike '%200 E 607%' )
 and extract(year from indate_fixed) = 1787
 group by pointcall_uhgs_id
 ) as k 
where pp.uhgs_id = k.pointcall_uhgs_id
-- nb_petitcabotage_1787 = 2526

update port_points pp set nb_petitcabotage_1789 = k.c
from 
(select pointcall_uhgs_id, count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'Registre du petit cabotage (1786-1787)' -- and p.pointcall_uhgs_id = 'A0210797'
--and (p.source_component ilike '%200 E 608%' or p.source_component ilike '%200 E 607%' )
 and extract(year from indate_fixed) = 1789
 group by pointcall_uhgs_id
 ) as k 
where pp.uhgs_id = k.pointcall_uhgs_id
-- nb_petitcabotage_1789 = 2416



select distinct  source_suite  from navigoviz.pointcall p -- , source_component
where p.source_suite = 'Marseille' -- and source_text like 'Amis Vieux Toulon, MA_11/%'
and not (source_component  like '%200E, 545/%')  

-- Retour de portic_v6 sur le serveur le 9 mars 2021
/*
Registre du petit cabotage (1786-1787)
Expéditions "coloniales" Marseille (1789)
Santé Marseille
G5
*/
update navigoviz.pointcall set source_suite='Expéditions "coloniales" Marseille (1789)' where source_suite='Expéditions coloniales Marseille (1789)';
update navigoviz.pointcall set source_suite='Santé Marseille' where source_suite='la Santé registre de patentes de Marseille';
-- fait sur le server, portic_v7 le 9 mars 2022

/*
 200 E 608
 200E, 543/
200 E 606
200 E 607
Amis Vieux Toulon, MA_11

Expéditions coloniales Marseille (1789)
*/

-- done on server portic_v6 le 07 février 2022
alter table ports.port_points add column nb_longcours_marseille_1789 int;
update ports.port_points set nb_longcours_marseille_1789 = null;
update ports.port_points set nb_longcours_marseille_1789 = 119 where toponyme_standard_fr = 'Marseille';
-- also done on server portic_v7 le 09 mars 2022

select toponyme_fr, pointcall_uhgs_id , source_component, extract(year from indate_fixed) as annee, count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'Marseille' -- and p.source_component like '%Amis Vieux Toulon%' 
and not (source_component  like '%200E, 545/%')  
group by toponyme_fr, pointcall_uhgs_id , extract(year from indate_fixed), source_component
order by source_component, annee

select toponyme_fr, pointcall_uhgs_id , extract(year from indate_fixed) as annee, count(distinct source_doc_id) as c 
from navigoviz.pointcall p 
where p.pointcall_function = 'O' and p.source_suite = 'Marseille' -- and p.source_component like '%Amis Vieux Toulon%' 
and  (source_component  like '%200E, 545/%')  
group by toponyme_fr, pointcall_uhgs_id , extract(year from indate_fixed)
order by  annee

---
-- export de la table  ports.port_points dans le fichier C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\obliques\ports_points_update05Janvier2022.sql

SELECT * FROM ports.port_points pp WHERE pp.uhgs_id = 'A0171758' -- Charente
SELECT * FROM ports.port_points pp WHERE pp.toponyme = 'Martrou' -- A0124423
SELECT * FROM ports.port_points pp WHERE pp.toponyme = 'Soubise' -- A0148208
SELECT * FROM ports.port_points pp WHERE pp.toponyme = 'Rochefort' -- A0196496
SELECT * FROM ports.port_points pp WHERE pp.toponyme = 'Port des Barques' -- A0129516
SELECT * FROM ports.port_points pp WHERE pp.toponyme_standard_fr like 'Le Pas au Bœuf' -- A0123256
-- Le Pas au Bœuf 
Le Pas au Bœuf

-- Done also on server (portic_v6) and on localhost (portic_v7)
update ports.port_points pp set toponyme_standard_fr = 'Le Pas au Bœuf', toponyme_standard_en = 'Le Pas au Bœuf' where uhgs_id ='A0123256'
SELECT * FROM ports.generiques_inclusions_geo_csv g WHERE g.ughs_id_sup = 'A0171758'

select * from ports.labels_lang_csv llc where label_type = 'toponyme' and key_id = 'A0123256'
update ports.labels_lang_csv set fr = 'Le Pas au Bœuf', en='Le Pas au Bœuf' where key_id ='A0123256' and label_type = 'toponyme' 



SELECT * FROM ports.port_points pp WHERE pp.uhgs_id = 'A0122218' -- Rouen
SELECT * FROM ports.port_points pp WHERE pp.uhgs_id = 'A0173748' -- Quillebeuf


SELECT toponyme , pp.toponyme_standard_fr, pp.toponyme_standard_en FROM ports.port_points pp WHERE pp.new_janvier2022 IS true and toponyme_standard_fr is null
-- a intégrer dans labels_lang_csv

SELECT count(distinct uhgs_id) FROM ports.etats
-- 1199 / 855 dans portic_v7

SELECT count(distinct uhgs_id) FROM ports.etats
-- 1151 / 812 dans portic_v6

SELECT * FROM ports.port_points

update ports.port_points set substate_1789_fr = null where substate_1789_fr='false'

-- pour les nouveaux ports janvier 2022
-- spécifier Etranger pour partner_balance_supp_1789
UPDATE ports.port_points SET partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 
WHERE state_1789_fr in ('Empire ottoman', 'Malte') AND partner_balance_supp_1789 IS null
-- 5

UPDATE ports.port_points SET partner_balance_1789='Saint-Domingue', partner_balance_1789_uncertainty=0 
WHERE province = 'Saint-Domingue' 
-- 19

UPDATE ports.port_points SET partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0 
WHERE amiraute = 'Port-Louis (Tobago)' AND partner_balance_1789 IS NULL
-- 2

-- 2 corrections : mail Silvia du 07 février 2022

-- also done on server portic_v6 le 07 février 2021
-- accent dans sans Saint-Valéry-sur-Somme
SELECT * FROM ports.port_points WHERE toponyme LIKE 'Saint Valery%'
SELECT uhgs_id , toponyme , toponyme_standard_fr FROM ports.port_points WHERE toponyme_standard_fr LIKE 'Saint-Val%'
update ports.port_points set toponyme_standard_fr='Saint-Valéry-sur-Somme' where uhgs_id  = 'A0196771';
select * from labels_lang_csv labels where labels.key_id = 'A0196771'
update ports.labels_lang_csv set fr = 'Saint-Valéry-sur-Somme', en = 'Saint-Valéry-sur-Somme' where key_id = 'A0196771' and label_type = 'toponyme';

-- Corse sans amirauté
select * FROM ports.port_points WHERE toponyme LIKE 'Corse';
update ports.port_points set amiraute = null where uhgs_id = 'A0156739' and toponyme LIKE 'Corse';

-- le 14 févrirer 2022
-- Corrections : Lucie  B1969741 et Le Carénage 'B1969630' n'est pas dans la province de Saint-Domingue
SELECT uhgs_id, toponyme_standard_fr, province, partner_balance_1789 FROM ports.port_points where province = 'Saint-Domingue'
	
-- sur le serveur en v6 et en local en v7 et v6
UPDATE ports.port_points SET province = null, partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0 
WHERE province = 'Saint-Domingue' and uhgs_id in ('B1969741', 'B1969630')

-- correction des input_done de la tremblade en 1787
 SELECT uhgs_id, toponyme_standard_fr, nb_conges_1787_inputdone FROM ports.port_points where toponyme like '%Tremblade%'
 -- nb_conges_1787_inputdone = 119 pour uhgs_id = A0186515
 UPDATE ports.port_points SET nb_conges_1787_inputdone = 119 where uhgs_id = 'A0186515'
 
 
-- Test de la version 13 postgres sur portic_v7
select uhgs_id from ports.port_points pp where pp.toponyme_standard_fr = 'Seudre' -- A0178037
select p.uhgs_id, p.toponyme_standard_fr, st_distance(pp.point3857, p.point3857) 
from ports.port_points pp, ports.port_points p 
where pp.toponyme_standard_fr = 'Seudre' -- A0178037

-----------------------------------------------------------
-- mise à jour suite à corrections Silvia
-- le 28 fev 2022
-- fichier file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\traduction_nom_de_pays_monde1789%20(1)%20avec%20corrections%20Silvia.xlsx
-----------------------------------------------------------

SELECT * FROM etats


update etats  set  subunit='Colonies danoises' where subunit='colonies danoises';--3
update etats  set  subunit='Colonies espagnoles d''Amérique' where subunit='colonies espagnoles d''Amérique';--2
update etats  set  subunit='Colonies françaises d''Amérique' where subunit='colonies françaises d''Amérique';--37
update etats  set  subunit='colonies françaises en Asie' where subunit='colonies françaises en Asie';--3

update etats  set  etat='Guyenne' where etat='Guyenne-Gascony';
update etats  set  subunit='Corse', subunit_en='Corsica' where subunit='Isles de Corse';--0

update etats  set  subunit='Colonies britanniques d''Amérique' where subunit='colonies britanniques d''Amérique';--89
update etats  set  subunit='Colonies portugaises d''Amérique' where subunit='colonies portugaises d''Amérique';--1
update etats  set  subunit='Georgie' where subunit='Georgia';--0
SELECT * FROM etats WHERE subunit='Georgia'--0
update etats  set  subunit='Colonie suèdoise en Amérique' where subunit='colonie suèdoise en Amérique';--0
update etats  set  etat='Confédération suisse [ou: Suisse]' where etat='Cantons suisses';--0
update etats  set  subunit='Cologne' where subunit='Eau de Cologne';
update etats  set  etat='Maroc' where etat='Empire du Maroc';--4
update etats  set  subunit='Maroc' where subunit='Empire du Maroc';--0

update etats  set  etat='Parme et Plaisance' where etat='Parme Plaisance';--0
update etats  set  subunit='Parme et Plaisance' where subunit='Parme Plaisance';--0

update etats  set  etat='Principauté de Lampédouse' where etat='principauté de Lampédouse';--1
update etats  set  etat='Empire russe' where etat='Russie';--11
update etats  set  subunit='Empire russe' where subunit='Russie';

--update etats  set  subunit='Saint-Marin' where subunit='Saint Marin';

select * from ports.port_points where amiraute = 'Bastia'
update ports.port_points set province = 'Corse' where province='Isles de Corse'--21
update ports.port_points set province = 'Guyenne' where province='Guyenne-Gascony';--0
-------------------------------

-- 01 mars 2022
-- Préparation des requêtes à faire pour mettre à jour les noms et états des homeports ports nouveaux

SELECT * FROM ports.etats WHERE uhgs_id in ('A0234281', 'A0235476', 'B2046784')

-- deja dans etat
/*
A0234281	Campofelice	Italy	Royaume de Naples	Sicile	Campofelice	Campofelice	
A0235476	Coldirodi	Italy	République de Gênes		Coldirodi	Coldirodi	
B2046784	Milfort	Haiti	France	colonies françaises d'Amérique	Milfort	Milfort	B2045171		Saint-Domingue	Jérémie
*/

	
-- Done

select * from ports.port_points where uhgs_id = 'A0136333'

select * from ports.etats where uhgs_id in ('A0618995' ,'A0629327', 'A0633170')
select * from ports.etats where subunit ilike '%Over%el%'
select * from ports.etats where subunit ilike '%Frise%'
select * from ports.etats where uhgs_id = 'A0628870'
A0686311

insert into ports.etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, etat_en, subunit_en)
select 'Netherlands', p.toponyme_standard_fr , p.toponyme , p.uhgs_id , 'Provinces-Unies', 'Overijssel', null, 1794, 2750405, 'Kingdom of the Netherlands', '00', 52.25, 5.75, 'United Provinces', 'Overijssel'
from ports.port_points p where p.uhgs_id in ('A0618995', 'A0629327', 'A0633170');
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, etat_en, subunit_en)
select 'Netherlands', p.toponyme_standard_fr , p.toponyme , p.uhgs_id , 'République batave', null, 1795, 1805, 2750405, 'Kingdom of the Netherlands', '00', 52.25, 5.75, 'Batavian Republic', null
from ports.port_points p where p.uhgs_id in ('A0618995', 'A0629327', 'A0633170');
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, etat_en, subunit_en)
select 'Netherlands', p.toponyme_standard_fr , p.toponyme , p.uhgs_id , 'Royaume de Hollande', null, 1806, 1810, 2750405, 'Kingdom of the Netherlands', '00', 52.25, 5.75, 'Kingdom of Holland', null
from ports.port_points p where p.uhgs_id in ('A0618995', 'A0629327', 'A0633170');
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, etat_en, subunit_en)
select 'Netherlands', p.toponyme_standard_fr , p.toponyme , p.uhgs_id , 'France', null, 1811, 1813, 2750405, 'Republic of France', '00', 46, 2, 'France', null
from ports.port_points p where p.uhgs_id in ('A0618995', 'A0629327', 'A0633170');
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, etat_en, subunit_en)
select 'Netherlands', p.toponyme_standard_fr , p.toponyme , p.uhgs_id , 'Royaume uni des Pays-Bas', null, 1814, null, 2750405, 'Kingdom of the Netherlands', '00', 52.25, 5.75, 'United Kingdom of the Netherlands', null
from ports.port_points p where p.uhgs_id in ('A0618995', 'A0629327', 'A0633170');


select * from ports.etats where uhgs_id = 'A0399010' -- 
select * from ports.etats where uhgs_id = 'A0399213' -- Neath
update ports.etats set country2019_name = 'United Kingdom' where  uhgs_id = 'A0399213'

--A0399010 Barmouth dans Pays de Galles
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, etat_en, subunit_en)
select e.country2019_name, p.toponyme_standard_fr , p.toponyme , p.uhgs_id ,  etat, subunit, dfrom, dto, e.geonameid, name_en, admin1_code, e.latitude, e.longitude, etat_en, subunit_en
from ports.port_points p, ports.etats e where p.uhgs_id in ('A0399010') and e.uhgs_id = 'A0399213';

select p.toponyme_standard_fr , p.uhgs_id , p.state_1789_fr , p.country2019_name , p.substate_1789_fr 
from  ports.port_points p
where p.province is null and (p.state_1789_fr='Grande-Bretagne' or p.country2019_name = 'United Kingdom')

update ports.port_points p set province = 'Guernesey'  where p.uhgs_id='A1945818' and toponyme_standard_fr = 'Le Gouffre';

select p.toponyme_standard_fr , p.uhgs_id , p.state_1789_fr , p.country2019_name , p.substate_1789_fr 
from  ports.port_points p
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
-- 66

select p.toponyme_standard_fr , p.uhgs_id , p.state_1789_fr , p.country2019_name , p.substate_1789_fr , province.provinces
from  ports.port_points p, ports."provinces_anglaises_Cary_1787" province
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
and st_contains(province.geom, p.geom)

update ports.port_points p set province = province.provinces
from ports."provinces_anglaises_Cary_1787" province
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
and st_contains(province.geom, p.geom)
-- 41 

select p.toponyme_standard_fr , p.uhgs_id , p.state_1789_fr , p.country2019_name , p.substate_1789_fr , province.provinces
from  ports.port_points p, ports."provinces_anglaises_Cary_1787" province
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
and st_intersects(st_transform(province.geom, 3857), st_buffer(p.point3857, 10000))
order by toponyme_standard_fr

-- Burry Port	A0394379	Grande-Bretagne	United Kingdom	Pays de Galles	Glamorgan shire
-- Cawsand	A0387840	Grande-Bretagne	United Kingdom	Angleterre	Devon shire

update ports.port_points p set province = province.provinces
from ports."provinces_anglaises_Cary_1787" province
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
and st_intersects(st_transform(province.geom, 3857), st_buffer(p.point3857, 10000))
-- 16 
select p.toponyme_standard_fr , p.uhgs_id , p.province from ports.port_points p where uhgs_id = 'A0390609';
update ports.port_points p set province = 'Port of Durham' where uhgs_id = 'A0390609';-- Emblestone

select p.toponyme_standard_fr , p.uhgs_id , p.state_1789_fr , p.country2019_name , p.substate_1789_fr , province.provinces
from  ports.port_points p, ports."provinces_anglaises_Cary_1787" province
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
and st_intersects(st_transform(province.geom, 3857), st_buffer(p.point3857, 30000))
order by toponyme_standard_fr


update ports.port_points p set province = province.provinces
from ports."provinces_anglaises_Cary_1787" province
where p.province is null and (p.substate_1789_fr in ('Angleterre', 'Pays de Galles') )
and st_intersects(st_transform(province.geom, 3857), st_buffer(p.point3857, 30000));
--11

update ports.port_points p set province = 'North Umberland' where uhgs_id = 'A0386900'; -- Berwick
update ports.port_points p set province = 'Cornwall' where uhgs_id = 'A0397493'; -- îles Scilly

select p.toponyme_standard_fr , p.uhgs_id , p.state_1789_fr , p.country2019_name , p.substate_1789_fr 
from  ports.port_points p
where p.province is null and p.substate_1789_fr in ('Ecosse', 'Irlande')

update ports.port_points p set province = substate_1789_fr
where p.province is null and p.substate_1789_fr in ('Ecosse', 'Irlande');
-- 48


-------------------------------
-- 8-9 mars 2022
-- Beaucoup de travail inscrit dans portic_v7.sql sur les 225 nouveaux ports, 
-- et l'ajustement avec le fichier excel importé.
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\AppartenanceEtat\Liste ports nouveaux 30-01-2022 Silvia en cours 12-02-2022_v3.csv
-- Un nouvel état (Evêché de Munster) et beaucoup de ports nouveaux dans états
-- Concernant les états, changement de nom de la Russie en 'Empire russe'
-- Voir fichier "traduction_nom_de_pays_monde1789 (1) avec corrections Silvia.xlsx"
-- Processing des provinces anglaises (comtés) exportés dans C:\Travail\ULR_owncloud\ANR_PORTIC\datasprint2_Dunkerque\micro-projet\data\provinces_anglaises_4326.geojson
-- Plus mise à jour des géométries sur la shoreline (script maritime_paths-21fev2022.sql, lignes 634 à 760)
-- MAJ de port_on_line_1mile et port_on_shoreline
-- fin le 9 mars 2022
-------------------------------

SELECT * FROM ports.port_points WHERE uhgs_id = 'A0204180'

SELECT * FROM ports.port_points WHERE toponyme_standard_fr = 'Londres'
Grande-Bretagne	Angleterre Middlesex
A0381691
select distinct partner_balance_supp_1789  from ports.port_points

-- Christine : février 2020
-- Correctifs à apporter à geo_general sur les amirautés et provinces

select * from ports.port_points where toponyme like '%Roche Bernard%'; -- Nantes ?
-- update navigoviz.port_points set amiraute='Vannes' where toponyme = 'Roche Bernard'


-----------------------------------------------------------------
-- le 17 mai 2022 puis 29 juin 2022
-- Correction du niveau d'incertitude pour les fermes
-- et Etaples comme bureau Toflit à part de Boulogne
-- corrections de quelques amirautés aussi. Le 29 juin 2022
-----------------------------------------------------------------
select * from ports.port_points where toponyme='Dunkerque' --A0204180 -- pas de bureau de ferme ni de direction
select * from ports.port_points where toponyme='Gravelines' --A0189462 -- direction : Lilles, ferme_bureau = Gravelines

-- Des erreurs manifestes dans les fermes d'après le doc de G. Daudin. 
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\Fermes\Etat des bureaux de traite 1786.pdf
update ports.port_points  set ferme_bureau = 'Dunkerque' where toponyme='Dunkerque';
update ports.port_points  set ferme_bureau_uncertainty  = 0 where toponyme='Dunkerque';
update ports.port_points  set ferme_direction_uncertainty  = 0 where toponyme='Dunkerque';
update ports.port_points  set ferme_direction  = 'Lille' where toponyme='Dunkerque';
update ports.port_points  set ferme_direction  = 'Lille' where toponyme='Gravelines';
-- update ports.port_points  set ferme_bureau = 'Gravelines' where toponyme='Gravelines';
-- update ports.port_points  set ferme_bureau_uncertainty  = 0 where toponyme='Gravelines';


select * from ports.port_points where ferme_direction='La Rochelle'

select * from ports.port_points where ferme_direction='Amiens'
update ports.port_points  set ferme_bureau = 'Etaples' where uhgs_id = 'A0192436';

update ports.port_points set ferme_bureau_uncertainty = -1 
where uhgs_id in ('A0194200', 'A0169535', 'A0212088', 'A0218540', 'A0152801');
--Ambleteuse
-- Baie d'Authie
-- Le crotoy
-- Cayeux
-- Ault


SELECT DISTINCT status FROM ports.port_points ;
-- Pour l'article et être conforme à Chardon, version article en français 
-- Pb : Thierry a mal lu Chardon, et Chardon dit qu'en 1786, les 2 amirautés de Honfleur et Touques fusionnent
--update ports.port_points  set amiraute = 'Touques', status='siège amirauté' WHERE uhgs_id = 'A0158963';
--update ports.port_points  set amiraute = 'Touques' WHERE uhgs_id in ('A1965096', 'A0172163');

-- voir aussi le mail du 13 janvier 2020 : TR: Mélanges Pierrick Pourchasse
-- Redon (mais aussi du coup Vielle Roche et probablement aussi Penestin) font partie de l'amirauté de Vannes et pas de celle de Nantes.
/*
Par la suite, d’autres sources que Chardon ont été mobilisées pour verifier le statut des ports et leur Amirauté d’appartenance. 
Voir le fichier de Silvia du 2 avril 2021, contrôle registres-congés-obliques.xlsx
Dans C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\obliques
- Schnakenbourg (Christian). L'Amirauté de France à l'époque de la monarchie administrative 1669-1792, thèse de doctorat, Droit, Paris 2, 1975, dact., 323-328 f°.
- Marzagalli, 2004 « Bordeaux et le vin », dans Jean-René Couliou et Gérard Le Bouëdec (dir.), Les ports du Ponant. L’Atlantique de Brest à Bayonne, Plomelin, Editions Palatines – Université de Bretagne Occidentale, 2004, p. 246-247.
*/
update ports.port_points  set amiraute = 'Honfleur' , status='oblique' WHERE uhgs_id = 'A0158963';--Touques, qui était Siège d'amirauté avant
update ports.port_points  set amiraute = 'Honfleur' WHERE uhgs_id in ('A1965096', 'A0172163');-- Trouville, Quai au Cocq

select * from ports.port_points where uhgs_id = 'A0173748' 
update ports.port_points  set amiraute = 'Caudebec et Quilleboeuf'  WHERE uhgs_id = 'A0173748' --Quillebeuf
-- l'erreur vient du fichier de Silvia : verif_ports_11fevrierb_2021 - corrections en cours.xlsx
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\obliques
-- c'est bien Caudebec et Quilleboeuf

update ports.port_points  set amiraute = 'Nantes' WHERE uhgs_id = 'A0207066' and toponyme_standard_fr = 'Port-Launay';-- amirauté de Quimper par erreur
-- erreur qui vient du fichier de Silvia C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\obliques\verif_ports_11fevrierb_2021 - corrections en cours.xlsx
-- mais je suis sure que c'est une erreur d'après le mail du 13 janvier 2020 : TR: Mélanges Pierrick Pourchasse

select * from ports.port_points where uhgs_id = 'A0164581' -- Kerner
update ports.port_points  set amiraute = 'Lorient' WHERE uhgs_id = 'A0164581';

select * from ports.port_points where toponyme like '%zardrieu%'
select toponyme_standard_fr , amiraute, status from ports.port_points p where toponyme in ('Redon', 'Penestin', 'Vieille Roche', 'Roche Bernard')
-- OK
/*
 * 
 * La Roche-Bernard	Nantes	oblique
Vieille Roche	Vannes	oblique
Pénestin	Vannes	
Redon	Vannes	oblique
*/
 */

---------------------------------------------------------------------------------------------------
select distinct state_1789_fr, substate_1789_fr from ports.port_points 
where state_1789_fr = 'Grande-Bretagne'

-- Colonies britanniques d'Amérique
select count(*), sum(tonnage::float)
from navigoviz.raw_flows 
where  departure_fr = 'Dunkerque' and destination_state_1789_fr = 'Grande-Bretagne' and destination_substate_1789_fr != 'Colonies britanniques d''Amérique'
and extract(year from outdate_fixed)=1789 
-- and tonnage<>'12'
-- 1474 (ships) pour 44000 tonneaux
-- 330 navires non smogglers, 29825 tonneaux


select distinct label_type from ports.labels_lang_csv llc ;
-- toponyme
-- flag
-- commodity

select uhgs_id , toponyme , toponyme_standard_fr , toponyme_standard_en , topofreq , toustopos , shiparea , state_1789_fr , state_1789_en , substate_1789_fr , substate_1789_en ,latitude , longitude 
from ports.port_points pp where pp.uhgs_id in ('A0251618', 'A0237391', 'A0167415');
