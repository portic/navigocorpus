-- Used on 5 février 2021
-- Re-used on 31 aout 2021

set search_path = 'ports', public;

create  materialized VIEW if not exists  ports.myremote_geonamesplaces AS
            SELECT *
            FROM dblink('dbname=geonames user=postgres password=mesange17 options=-csearch_path=',
                        'select geonameid::int, name, (feature_class||''.''||feature_code) as feature_code, alternatenames, country_code,  latitude::float, longitude::float , point3857
                            from geonames.geonames_nov2019.allcountries a 
                            where feature_class||''.''||feature_code in (select code from geonames.geonames_nov2019.feature_code where keep = ''x'')')
            AS t1(geonameid int, name text, feature_code text, alternatenames text , country_code text, latitude float, longitude float, point3857 geometry) ; 
-- 5 milions de lignes en 28 s
           
grant select on  ports.myremote_geonamesplaces to api_user
CREATE INDEX trgm_geoname_name_idx ON ports.myremote_geonamesplaces USING GIST (name gist_trgm_ops) --1 min 46
CREATE INDEX gist_geoname_point_idx ON ports.myremote_geonamesplaces USING GIST (point3857) -- 1 min 10

insert into matching_port(new_entry, source1, id1, uhgs_id, topo1, source2, id2, topo2, distgeo, simtext)
        select true, 'geo_general' as source1, pp.ogc_fid as id1, pp.uhgs_id, pp.toponyme,  'geonames' as source2, s.geonameid, s.toponyme , st_distance(s.point3857, pp.point3857 ), similarity(s.toponyme, pp.toponyme)
        from 
        (select ogc_fid, uhgs_id, unnest(toustopos) as toponyme, point3857 from ports.port_points where geonameid is null) as pp,
        (select geonameid, name as toponyme, point3857 from myremote_geonamesplaces ) as s
        where st_distance(s.point3857, pp.point3857 ) < 5000 ;
-- 218
        
-- 1.       etape 1 
update matching_port m set best = true , certainity = 1, new_entry=false
        from (
            select distinct m.id1, m.id2, m.simtext, m.distgeo
            from matching_port m,
            (select id1, max(simtext), min(distgeo)  from matching_port where new_entry is true and simtext > 0 and source1 = 'geo_general' and source2='geonames' group by id1 ) as k 
            where m.new_entry is true and source1 = 'geo_general' and source2='geonames' and m.id1 = k.id1 and m.simtext = k.max and m.distgeo = k.min
        ) as k 
        where m.new_entry is true and m.source1 = 'geo_general' and m.source2='geonames' and m.id1 = k.id1 and m.id2 = k.id2 and m.simtext = k.simtext and m.distgeo = k.distgeo;
-- 45
       
update ports.port_points p set geonameid = id2
        from matching_port where source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and geonameid is null;
-- 9
-- new_entry is true 
       
-- 2.     etape 2   
update matching_port m set best = true , certainity = 2, new_entry=false
        from (
            select distinct m.topo1, m.topo2, m.id1, m.id2, m.simtext, m.distgeo, m.best
            from ports.port_points p, matching_port m, 
            (select id1, max(simtext), min(distgeo)  from matching_port where new_entry is true and simtext > 0 and source1 = 'geo_general' and source2='geonames' group by id1 ) as k 
            where m.new_entry is true and source1 = 'geo_general' and source2='geonames' and m.id1 = k.id1 and m.simtext = k.max 
            and p.ogc_fid = m.id1 and p.geonameid is null
        )
        as k 
        where m.new_entry is true and m.source1 = 'geo_general' and m.source2='geonames' and m.id1 = k.id1 and m.id2 = k.id2 and m.simtext = k.simtext and m.distgeo = k.distgeo;
-- 3
update ports.port_points p set geonameid = id2
        from matching_port where  source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and geonameid is null;
-- new_entry is true and
       
-- 3. etape 3
update matching_port m set best = true, certainity = 3, new_entry=false
        from (
            select distinct m.topo1, m.topo2, m.id1, m.id2, m.simtext, m.distgeo, m.best
            from ports.port_points p, matching_port m, 
            (select id1, max(simtext), min(distgeo)  from matching_port where new_entry is true and source1 = 'geo_general' and source2='geonames' group by id1 ) as k 
            where m.new_entry is true and source1 = 'geo_general' and source2='geonames' and m.id1 = k.id1 and m.distgeo = k.min
            and p.ogc_fid = m.id1 and p.geonameid is null
        )
        as k 
        where m.new_entry is true and m.source1 = 'geo_general' and m.source2='geonames' and m.id1 = k.id1 and m.id2 = k.id2 and m.simtext = k.simtext and m.distgeo = k.distgeo;
-- 2

update ports.port_points p set geonameid = id2
        from matching_port where  source1 = 'geo_general' and source2='geonames' and id1 = p.ogc_fid and  best is true and geonameid is null;
-- new_entry is true and        

-- give rights
 grant select on ports.matching_port to api_user
 
 select uhgs_id , toponyme_standard_fr, toustopos from ports.port_points pp where relation_state is null
 
 /*
H9999999	pas mentionné	{Unknown,Unnamed}
H5555555	cabotage dans la province	{Bretagne,Cabotage,Cabotage en Bretagne,Plages [Cabotage],Pour la Cote [Cabotage]}
H8888888	illisible	{Illisble,illisible,Illisible}
H4444444	pas identifié	{$talamo,A l' Endy,Alborough,Alloa,Alvert,Arcquier,Arcudi,Ardon,Arundell,Avay,Ayr,Barking,Barmouth,Batelage,Beaux Rouge,Beddeford,Berdin,Bettiford,Bewacarils,Bisteri,Blackeney,Borrowtones,Brethegthiemton,Campine,Camprin,Caole,Cap Cassino,Cap de Craigne,Capeau,Carland,Carry,Carthagene,Cerfe dans l' archipel,Chastelneau,Chenal de Lilleau,Chrudervhal,Condat,Cootyes Plaat,Cote des Bouq,Couingburn,Courege,Doarcham,Dol,Encet,Engermont,Engien,Enjien,Entel,Etamaville,Eysadette,Fenotiva,Ferseau,Feryland,Firnmes,Fouennée,Gien,Golfe de Castagnier,Golfe de Zeiton,Golfe Luan,Griesval,Groizilles,Gurna,Intel,Isles Rousse,Keing,Kencarven,Lauzure,Le Braud,Lée,Lefagavis,Lesterapally,Limskilus,Maan,Maden,Madevesy,Malines,Malora,Maloure,Marcili (Marcilly?),Mayon,Mayoure,Measen,Measy,Menidal,Mestredame,Montelto,Morlan,Nascaw,Nedeness,Nio,Nivelan,Nortbergen,North Cape Irlande,Ortege,Par les travers de Barcelonne,Pegelsham,Pomeles,Pontaves,Ponteves,Pontevés,Pontevez,Pontheves,Portalty,Portevés,Portion,Posgroom,Pourmieu,Rachusa,Rhoméo,Rognan,Rome,Roumavechert,Saint Antoine,Saint Roch des Alcacides,Salette,Sallenelle,Scerque,Secs,Sefalonico,Sira,Song,Souer,Sour,Suedpomel,Sur la pointe de Monjouin,Swal,Syra,Terranovi,Teveno,Therebis,Thiomée,Tirimouth,Toctolla,Torramourre,Tour de Mocessa,Tourneuve,Tourreme,Vandaire,Verdon,Vérdon,Vian,Warkingson,Whivenor}
C0237838	Calabar	{Gualbar Cote d' Affrique [Calabar]}
 */