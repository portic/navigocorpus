drop schema ports cascade

create schema ports
create extension postgis
        self.loader.execute_sql("create extension if not exists postgis")
        self.loader.execute_sql("create extension if not exists fuzzystrmatch")
        self.loader.execute_sql("create extension if not exists pg_trgm")
        self.loader.execute_sql("create extension if not exists postgis_topology")
        self.loader.execute_sql("create extension if not exists plpython3u")
        
alter table navigoviz.built_travels add column in_crew text
update navigoviz.built_travels viz set in_crew = p.in_crew from navigoviz.pointcall p 
where p.pkid = viz.departure_pkid and (source_entry = 'from' or source_entry = 'both-from')

alter table navigoviz.built_travels add column tonnage_class text
update navigoviz.built_travels viz set tonnage_class = p.tonnage_class from navigoviz.pointcall p 
where p.pkid = viz.departure_pkid and (source_entry = 'from' or source_entry = 'both-from')

alter table navigoviz.built_travels add column ship_flag_id text
update navigoviz.built_travels viz set ship_flag_id = p.ship_flag_id from navigoviz.pointcall p 
where p.pkid = viz.departure_pkid and (source_entry = 'from' or source_entry = 'both-from')


select count(*) from navigoviz.built_travels bt 
-- 25910 / 71454
select count(*) from navigoviz.pointcall p  
-- 27367 / 74569
select count(*) from navigoviz.source p  
-- 37 665

select distinct province, province_color as couleur, province_color_medium as code_couleur 
from codes_levels cl 

select  amiraute, amiraute_color  as couleur, amiraute_color_medium as code_couleur 
from codes_levels cl 

order by province_code 
limit 2
select count(*) from ports.port_points pp 
-- 1020


SELECT source_doc_id, count(DISTINCT ship_name) 
FROM navigoviz.pointcall  
GROUP BY source_doc_id
HAVING count(DISTINCT ship_name) > 1
-- 303

SELECT source_doc_id, count(DISTINCT navigo.pystrip(ship_name)) 
FROM navigoviz.pointcall  
GROUP BY source_doc_id
HAVING count(DISTINCT navigo.pystrip(ship_name)) > 1
-- 294

SELECT source_doc_id, count(DISTINCT navigo.pystrip(ship_name)) 
FROM navigoviz.pointcall  
GROUP BY source_doc_id
HAVING count(DISTINCT navigo.pystrip(lower(ship_name))) > 1
-- 48

select p.source_doc_id, record_id, ship_name from navigoviz.pointcall p, 
(
SELECT source_doc_id, count(DISTINCT navigo.pystrip(ship_name)) 
FROM navigoviz.pointcall  
GROUP BY source_doc_id
HAVING count(DISTINCT navigo.pystrip(lower(ship_name))) > 1
) as k 
where k.source_doc_id = p.source_doc_id 
order by source_doc_id

select count(*) from navigo.geo_general where lat <> '' and long <> '' 

-- 6344476 / 6346027
select lat, long from navigo.geo_general
select  length(lat)-position('.' in lat)-1 as lat_precision, count(*)  from navigo.geo_general
group by lat_precision

select  length(long)-position('.' in long)-1 as long_precision, count(*)  from navigo.geo_general where uhgs_identifier in (select uhgs_id from ports.port_points pp )
group by long_precision


select lat, long, * from navigo.geo_general where length(long)-position('.' in long)-1= -1

select length(longitude)-position('.' in longitude)-1 as long_precision, count(*) 
from ports.port_points pp 
where longitude <> '' 
group by long_precision
order by long_precision

select length(latitude)-position('.' in latitude)-1 as lat_precision, count(*) 
from ports.port_points pp 
where latitude <> '' 
group by lat_precision
order by lat_precision


select port, uhgs_id from obliques where uhgs_id is null

select * from port_points pp where amiraute like 'Eu%'


select count (distinct (lat, long, uhgs_identifier)) from navigo.geo_general where lat <> '' and long <> '' 

select uhgs_id, count(*) from ports.etats e 
group by uhgs_id 
having  count(*) > 1

select uhgs_id, * from ports.port_points pp where toponyme = 'Corse'


------------------------------------------------------------
-- portic_v5 : refaire les sources        
------------------------------------------------------------ 

create table navigo.cargo ( 
	 pkid serial primary key,
	Calculation_01     text,
	Actor_id     text,
	Calculation_02     text,
	Actor_id_integration     text,
	Calculation_03     text,
	Cargo_item_action     text,
	Pointcall__Global_01     text,
	Global_01     text,
	Cargo_item_date_declared     text,
	Global_02     text,
	Cargo_item_final_destination     text,
	Global_03     text,
	Cargo_item_id     text,
	Link_to_all     text,
	Link_to_signature     text,
	Cargo_item_indate     text,
	Link_to_first_point     text,
	Link_to_observation     text,
	Cargo_item_indate_relativity     text,
	Link_to_pointcall     text,
	Point_id_integration     text,
	Cargo_item_origin     text,
	Value_marker     text,
	Cargo_item_outdate     text,
	Cargo_item_outdate_relativity     text,
	Cargo_item_owner_class     text,
	Dictionary_commodities_first__Dictionary_english     text,
	Cargo_item_owner_location     text,
	Cargo_item_owner_name     text,
	Cargo_item_ppt     text,
	Cargo_item_quantity     text,
	Cargo_item_quantity_U     text,
	Cargo_item_quantity_equivalent     text,
	Cargo_item_quantity_equivalent_U     text,
	Commodity_permanent_coding     text,
	Cargo_item_quantity_metrical     text,
	Composite_commostand_commodity     text,
	Cargo_item_quantity_metrical_U     text,
	Composite_point_action_quantity_unit     text,
	Cargo_item_sending_place     text,
	Translation_variants_01     text,
	Commodity_first_word     text,
	Translation_variants_02     text,
	Commodity_id     text,
	Translation_variants_03     text,
	Commodity_number_cases     text,
	Sum_cases     text,
	Commodity_number_words     text,
	Sum_quantity     text,
	Commodity_purpose     text,
	Sum_quantity_metric     text,
	Commodity_second_word     text,
	Actor_id2     text,
	Actor_id2_integration     text,
	Commodity_standardisation_transfer     text,
	Documentary_unit_id     text,
	Documentary_unit_id2     text,
	Documentary_unit_id_integration     text,
	Commodity_standardized     text,
	Inconsistency_marker     text,
	Commodity_third_word     text,
	Record_creation_signature     text,
	Desambiguation_marker_01     text,
	Record_creation_date     text,
	Desambiguation_marker_02     text,
	Record_id     text,
	Record_id_merging     text,
	Desambiguation_marker_03     text,
	Record_last_change_signature     text,
	Desambiguation_marker_global     text,
	Record_last_change_date     text,
	Documentary_unit_id3     text,
	Selected_record_rank     text,
	Point_name     text,
	Point_UHGS_id     text,
	Selected_records_number     text,
	Q01     text,
	Q01_U     text,
	Q02     text,
	Q02_U     text,
	Q03     text,
	Q03_U     text,
	Remarks     text,
	Cargo_item_sending_place2     text,
	Cargo_item_origin2     text,
	Source     text,
	Tasks     text);


CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) or tested_value.strip().find(']') > 0:
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -3
                        result = None
                else :
                    result = None
                    code = -3
                return [result, code]
            $$ LANGUAGE plpython3u;

create table navigo.taxes ( 
	 pkid serial primary key,
	Link_to_action     text,
	Action_id_integration     text,
	Calculation_01     text,
	Link_to_cargo_item     text,
	Link_to_cargo_item_integration     text,
	Calculation_02     text,
	Documentary_unit_id     text,
	Documentary_unit_id_integration     text,
	Calculation_03     text,
	Payment_date     text,
	Global_01     text,
	Link_to_pointcall     text,
	Point_id_integration     text,
	Global_02     text,
	Q01     text,
	Global_03     text,
	Q02     text,
	Q03     text,
	Record_id     text,
	Record_id_integration     text,
	Q01_u     text,
	Link_to_all     text,
	Q02_u     text,
	Link_to_observation     text,
	Q03_u     text,
	Dictionary_taxes__Global_02     text,
	Remarks     text,
	Record_creation_date     text,
	Research_operation     text,
	Record_creation_signature     text,
	Sequence_id     text,
	Record_last_change_date     text,
	Source     text,
	Record_last_change_signature     text,
	Tax_concept     text,
	Selected_record_rank     text,
	Selected_records_number     text);

select data_block_local_id, source, pointcall_name, pointcall_action from navigocheck.check_pointcall cp 
where data_block_local_id not in 
(select data_block_local_id from navigoviz.source) 
-- 19897


            select  distinct data_block_local_id, source, 'Marseille' ,substring(source from 1 for position('/' in source)-1), substring(source from position('/' in source)+1)::int , 'A0210797',	'Marseille'
            from navigocheck.check_pointcall p
            where source like 'Amis Vieux Toulon, MA_11/%' and source <>'Amis Vieux Toulon, MA_11/066Fils unique'
            
select distinct substring(source for position('/' in source)) from navigocheck.check_pointcall cp 
where data_block_local_id not in 
(select data_block_local_id from navigoviz.source) 
ANF, G5-154-1/ 1537

select data_block_local_id, source, pointcall_name, pointcall_action from navigocheck.check_pointcall
where substring(source for position('/' in source)) like 'ADBR, 200E, 555/%'

select * from navigoviz.pointcall where pkid = 139671
select * from navigoviz.pointcall where pkid = 118150
00331382
 200E, 545/
  200E, 545/0511
select * from navigoviz.source where data_block_local_id = '00331382'

select * from navigoviz.source where suite = 'Marseille' and component like ' 200E, 545/%'
order by data_block_local_id 

select  distinct data_block_local_id, source, 'Marseille' ,substring(source from 1 for position('/' in source)-1) , 'A0210797',	'Marseille'
from navigocheck.check_pointcall p
where  source like '%ADBdR, 200E, 545%'
order by data_block_local_id
            where source like '%ADBdR, 200%' or source like '%AdBR,200%' or source like '%AdBR, 200%' or source like 'ADBR, 200E, 555/%'

select data_block_local_id, record_id from  navigocheck.check_pointcall p where source = 'ADBdR, 200E, 545/0002'
00298362	00298362
00303752	00303752
00303762	00303762
select p1.data_block_local_id, p1.record_id, p1.source, p2.record_id , p2.source, p1.pointcall_name, p2.pointcall_name 
from  navigocheck.check_pointcall p1,  navigocheck.check_pointcall p2
where p1.source = 'ADBdR, 200E, 545/' and p2.source like 'ADBdR, 200E, 545/0%' and p1.data_block_local_id=p2.data_block_local_id
-- 4855 documents ont deux sources

drop table if exists navigoviz.pointcall cascade
select count(*) from navigoviz.pointcall
create table navigoviz.pointcall as (
            select k.identifiant as pkid, 
            -- pointcall
            pp.toponyme as pointcall, k.pointcall_uhgs_id,  
            pp.latitude, pp.longitude, 
            pp.amiraute as pointcall_admiralty, pp.province as pointcall_province, pp.belonging_states as pointcall_states,
            pp.status as pointcall_status, pp.shiparea, st_asewkt(pp.point3857) as pointcall_point,  
            k.pointcall_outdate as pointcall_out_date, 
            k.pointcall_action, 
            to_date(k.pointcall_outdate_date, 'DD/MM/YYYY') as outdate_fixed,
            k.pointcall_indate as pointcall_in_date, 
            to_date(k.pointcall_indate_date, 'DD/MM/YYYY') as indate_fixed,
            k.net_route_marker,
            k.pointcall_function,
            k.pointcall_status as navigo_status,
            -- ship
            k.ship_name, 	k.ship_local_id as ship_id, k.ship_tonnage as tonnage, k.ship_tonnage_u as tonnage_unit, k.ship_flag as flag, k.ship_class as class,
            -- homeport
            k.ship_homeport as homeport, k.ship_homeport_uhgs_id as homeport_uhgs_id,
            k.latitude as homeport_latitude, k.longitude  as homeport_longitude, 
            k.amiraute as homeport_admiralty, k.province as homeport_province, k.belonging_states as homeport_states,
            k.status as homeport_status, k.shiparea as homeport_shiparea, st_asewkt(k.point3857) as homeport_point,
            -- source
            k.doc_id as source_doc_id, k.cpsource as source_text, s.suite as source_suite, s.component as source_component, 
            s.conge_number as source_number, s.other as source_other, s.main_port_uhgs_id as source_main_port_uhgs_id, s.main_port_toponyme as source_main_port_toponyme,
            -- captain
            k.captain_local_id as captain_id, k.captain_name, k.captain_birthplace as birthplace, k.captain_status as status, k.captain_citizenship as citizenship,
            -- cargo
            cc.commodity_purpose, cc.commodity_id, cc.quantity, cc.quantity_u, cc.commodity_standardized, cc.commodity_permanent_coding, cc.all_cargos,
            -- taxes
            k.tax_concept, k.payment_date, k.q01, k.q01_u, k.q02, k.q02_u , k.q03 , k.q03_u, k.all_taxes,
            -- uncertainity
            up.ship_local_id as ship_uncertainity, up.ship_tonnage as tonnage_uncertainity, up.ship_flag as flag_uncertainity, up.ship_homeport as homeport_uncertainity,
            case when k.pointcall_function in ('A', 'O') then 0 else (case when k.pointcall_status like 'PC%' then 0 else (case when  k.pointcall_status like 'PU%' then -2 else -1 end) end) end as pointcall_uncertainity,
            up.captain_name as captain_uncertainity
            
            from 
            (
                (select cp.pkid as identifiant, cp.data_block_local_id as doc_id, cp."source" as cpsource, * 
                    from navigocheck.check_pointcall cp 
                    left outer join public.pointcall_taxes ct on cp.data_block_local_id = ct.link_to_pointcall
                ) as q  
                left outer join ports.port_points ph on q.ship_homeport_uhgs_id = ph.uhgs_id
            ) as k 
            left outer join public.pointcall_cargo cc on cc.link_to_pointcall = k.doc_id, 
            navigocheck.uncertainity_pointcall up, navigoviz."source" s, 	 ports.port_points pp  
            where  up.pkid = k.identifiant and  s.data_block_local_id =k.doc_id and s.source = k.cpsource
            and pp.uhgs_id = k.pointcall_uhgs_id   
        )     
-- 147472

					select cp.pkid as identifiant, count(*)
                    from navigocheck.check_pointcall cp 
                    left outer join navigoviz.source s on cp.data_block_local_id = s.data_block_local_id 
                    group by identifiant
                    having count(*) > 1

 200E, 545/0511
select * from navigocheck.check_pointcall where pointcall_outdate_date = '1795-02=10'
select * from navigoviz.pointcall where pointcall_out_date = '1795-02=10'
select * from navigoviz.pointcall where pointcall_in_date = '1795-02=10'
1759-02-28
update navigoviz.pointcall set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  else replace(pointcall_in_date, '>', '=') end 
            where position('00' in pointcall_in_date ) = 0
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(pointcall_out_date, '>', '=') end  
            where position('00' in pointcall_out_date ) = 0 and position('99' in pointcall_out_date ) = 0
select pkid, pointcall_in_date from navigoviz.pointcall  where position('00' in pointcall_in_date ) = 0 and pointcall_in_date like '1759%02%29%'
            1759<02<29
            
-- Manip manuelle  à refaire si réimport          
update    navigoviz.pointcall set  pointcall_in_date =   '1759<02<28'     where pkid = 84650
update    navigo.pointcall set  pointcall_indate =   '1759<02<28'     where pkid = 84650
update    navigocheck.check_pointcall set  pointcall_indate =   '1759<02<28'     where pkid = 84650

-- 84650	1759<02<29
select pkid, pointcall_in_date from navigoviz.pointcall  where position('00' in pointcall_in_date ) = 0 and pointcall_in_date like '1795%02%10%'
select pkid, pointcall_in_date from navigoviz.pointcall  where position('00' in pointcall_in_date ) = 0 and pointcall_in_date like '1759%04%17%'

1795-02=10
select * from ports.port_points pp where uhgs_id = 'A9999997'
select * from ports.etats e where uhgs_id = 'A0381691'

update ports.port_points p set toponyme = e.toponyme
from  ports.etats e where e.uhgs_id = p.uhgs_id and p.toponyme is null
-- 176

select * from navigo.pointcall where pkid = 139671

select * from ports.port_points where uhgs_id = 'A0171758'
select * from navigo.pointcall p where record_id = '00123391'
1759-02-29

delete from navigo.pointcall p where pkid = 19295 and 

----------------------------------------

select link_to_pointcall, json_array_length(all_cargos::json) 
from public.pointcall_cargo c, navigoviz.pointcall p
where c.link_to_pointcall = p.source_doc_id  


select  json_array_length(all_cargos::json) 
from navigoviz.pointcall p
where p.source_suite = 'Marseille' and all_cargos is not null

select pp.uhgs_id, p.pointcall, p.pointcall_states, pp.toponyme , pp.belonging_states 
from navigoviz.pointcall p , ports.port_points pp 
where pp.uhgs_id = p.pointcall_uhgs_id and pp.toponyme <> p.pointcall 
-- and pp.belonging_states <> p.pointcall_states

select distinct pp.uhgs_id, p.pointcall, p.pointcall_states, pp.toponyme , pp.belonging_states 
from navigoviz.pointcall p , ports.port_points pp 
where pp.uhgs_id = p.pointcall_uhgs_id and pp.toponyme <> p.pointcall 

homeport_states
source_main_port_toponyme

select distinct p.homeport_uhgs_id, p.homeport,  pp.toponyme 
from navigoviz.pointcall p , ports.port_points pp 
where pp.uhgs_id = p.homeport_uhgs_id and pp.toponyme <> p.homeport 


alter table navigoviz.pointcall add column commodity_purpose2 text:
-- 00
 update navigoviz.pointcall set commodity_purpose2=(all_cargos->>1)::json->>'commodity_purpose'
select  ((all_cargos)->>1)::json->>'commodity_purpose'
from navigoviz.pointcall p
where p.source_suite = 'Marseille' and all_cargos is not null

alter table ports.port_points add column belonging_substates text;

 		select uhgs_id, STRING_AGG(belonging, ',') as appartenances_subunit
            from (
                select  uhgs_id, subunit,  case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit) :: text as belonging
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            where uhgs_id = 'A0046492'

            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id
        

        
select * from ports.port_points where uhgs_id = 'A0046492'
[{"1749-1794" : "Pays-Bas autrichiens"},{"1795-1815" : null}]



------------------------------------
-- STAGE (distance en miles calculées par Dedieu
------------------------------------

SELECT t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id, t.distance_dep_dest,  length_miles, s.stage_id
FROM navigoviz.built_travels t , navigo.pointcall p, navigo.stages s 
where t.departure_pkid = p.pkid and p.stage_id = s.stage_id and length(p.stage_id)> 8
-- 45614 / 50996

SELECT t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id, t.distance_dep_dest,  length_miles, s.stage_id
FROM navigoviz.built_travels t , navigo.pointcall p, navigo.stages s 
where t.destination_pkid = p.pkid and p.stage_id = s.stage_id and length(p.stage_id)> 8
-- 48302

SELECT t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id, t.distance_dep_dest,  length_miles, s.stage_id
FROM navigoviz.built_travels t , navigo.pointcall p, navigo.stages s 
where t.departure_pkid = p.pkid and p.stage_id = s.stage_id and length(p.stage_id)> 8
-- 45614 / 50996

update navigoviz.built_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
from (
		select id, length_miles as  distance_dep_dest_miles from (
			SELECT t.id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id, t.distance_dep_dest,  length_miles, s.stage_id
			FROM navigoviz.built_travels t , navigo.pointcall p, navigo.stages s 
			where t.destination_pkid = p.pkid and p.stage_id = s.stage_id and length(p.stage_id)> 8
			union 
			SELECT t.id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id, t.distance_dep_dest,  length_miles, s.stage_id
			FROM navigoviz.built_travels t , navigo.pointcall p, navigo.stages s 
			where t.departure_pkid = p.pkid and p.stage_id = s.stage_id and length(p.stage_id)> 8
		) as k
		-- 63232
) as q
where q.id = t.id
---  75497

update navigoviz.built_travels t set distance_homeport_dep_miles = q.distance_homeport_dep_miles
from (
	select id, length_miles as  distance_homeport_dep_miles  from (
	SELECT t.id, t.departure, t.departure_uhgs_id, t.homeport , t.homeport_uhgs_id ,t.distance_homeport_dep ,  length_miles, s.stage_id
	FROM navigoviz.built_travels t , navigo.stages s 
	where (homeport_uhgs_id||departure_uhgs_id = s.stage_id or departure_uhgs_id||homeport_uhgs_id = s.stage_id ) and length(s.stage_id)> 8
	) as k
	-- 47564
) as q
where q.id = t.id
-- 33060  

select t.id, t.departure, t.departure_uhgs_id, t.homeport , t.homeport_uhgs_id ,t.distance_homeport_dep 
FROM navigoviz.built_travels t 
where distance_homeport_dep_miles is null  and homeport_uhgs_id is not null and homeport_uhgs_id<>departure_uhgs_id
union 
select t.id, t.departure, t.departure_uhgs_id, t.destination , t.destination_uhgs_id , t.distance_dep_dest 
FROM navigoviz.built_travels t 
where distance_dep_dest_miles is null  and destination_uhgs_id is not null and departure_uhgs_id is not null and destination_uhgs_id<>departure_uhgs_id

select * from navigo.stages s where st01_name = 'Port d'' Envaux' A0212986
select * from navigo.stages s where st02_name = 'Port d'' Envaux'


select pointcall from navigoviz.pointcall where pointcall_uhgs_id ='B2125493'

------------------------------------
-- Bilan des saisie portic_v5
------------------------------------

SELECT count(*)
-- , pointcall_in_date, pointcall_in_date2, pointcall_out_date, pointcall_out_date2, date_fixed
FROM navigoviz.pointcall
WHERE pointcall_out_date LIKE '%1789%' or pointcall_in_date LIKE '%1789%'
-- 15978 + 16416 = 31278

SELECT count(*)
-- , pointcall_in_date, pointcall_in_date2, pointcall_out_date, pointcall_out_date2, date_fixed
FROM navigoviz.pointcall
WHERE  pointcall_out_date LIKE '%1787%' or pointcall_in_date LIKE '%1787%'
-- 39324 + 38499 = 75815

SELECT distinct pointcall_in_date, pointcall_in_date2, pointcall_out_date, pointcall_out_date2, date_fixed
FROM navigoviz.pointcall
WHERE  pointcall_out_date LIKE '%1787%' or pointcall_in_date LIKE '%1787%'


SELECT source_suite, source_main_port_toponyme, source_component, 1787, count(*)  
FROM navigoviz.pointcall
WHERE  pointcall_out_date LIKE '%1787%' or pointcall_in_date LIKE '%1787%'
group by source_suite, source_main_port_toponyme , source_component
union 
(SELECT source_suite, source_main_port_toponyme, source_component, 1789, count(*)  
FROM navigoviz.pointcall
WHERE  pointcall_out_date LIKE '%1789%' or pointcall_in_date LIKE '%1789%'
group by source_suite, source_main_port_toponyme , source_component
)
union 
(SELECT source_suite, source_main_port_toponyme, source_component, extract (year from date_fixed) as annee, count(*)  
FROM navigoviz.pointcall
WHERE  pointcall_out_date LIKE '%1789%' or pointcall_in_date LIKE '%1789%'
group by source_suite, source_main_port_toponyme , source_component, extract (year from date_fixed)
)


SELECT source_suite, source_main_port_toponyme, source_component, 1787, count(*)  
FROM navigoviz.pointcall
WHERE  source_component = '200 E 606' or source_component = '200 E 607' or  source_component = ' 200 E 608'
group by source_suite, source_main_port_toponyme , source_component
-- le cabotage à Marseille

SELECT source_suite, source_main_port_toponyme, source_component, 1787, count(*)  
FROM navigoviz.pointcall
WHERE  source_component like 'Amis %'
group by source_suite, source_main_port_toponyme , source_component
-- 218 Amis Vieux Toulon, MA_11

alter table navigoviz.source add column subset text;
update navigoviz.source set subset = 'Registre du petit cabotage (1786-1787)' where component = '200 E 606' or component = '200 E 607' or  component = ' 200 E 608';
update navigoviz.source set subset = 'Expéditions "coloniales" Marseille (1789)' where component = 'Amis Vieux Toulon, MA_11' ;
update navigoviz.source set subset = 'Santé Marseille' where subset is null and suite = 'Marseille';


select distinct component from navigoviz."source" s where suite = 'Marseille'
component like '200 E %'
-- 3961

-- extract (year from date_fixed) as annee,
-- case when pointcall_out_date  is not null then substring(pointcall_out_date for 4) else substring(pointcall_in_date for 4) end
SELECT source_suite, source_main_port_toponyme, case when pointcall_out_date  is not null then substring(pointcall_out_date for 4) else substring(pointcall_in_date for 4) end as YYYY, count(*)  
FROM navigoviz.pointcall
--  WHERE  pointcall_out_date LIKE '%1789%' or pointcall_in_date LIKE '%1789%'
group by source_suite,  source_main_port_toponyme, YYYY
order by YYYY, source_suite, source_main_port_toponyme
-- export dans le fichier bilan portic_V5

SELECT source_suite, case when pointcall_out_date  is not null then substring(pointcall_out_date for 4) else substring(pointcall_in_date for 4) end as YYYY, count(*)  
FROM navigoviz.pointcall
  WHERE  pointcall_out_date LIKE '%1787%' or pointcall_in_date LIKE '%1787%'
group by source_suite,  YYYY
order by YYYY, source_suite


SELECT *, pointcall_in_date, pointcall_in_date2, pointcall_out_date, pointcall_out_date2, date_fixed
FROM navigoviz.pointcall
WHERE  pointcall_out_date LIKE '%1878%' or pointcall_in_date LIKE '%1878%'  
--- erreur de saisie du pointcalll_out_date du record_id 00143961 : 1878<02<25! 
-- pointcall_in_date : 1787>02>23!

------------------------------------
-- Droits sur l'API
------------------------------------

GRANT USAGE ON SCHEMA navigo to api_user;
GRANT USAGE ON SCHEMA navigocheck to api_user;
GRANT USAGE ON SCHEMA navigoviz to api_user;
GRANT USAGE ON SCHEMA ports to api_user;
GRANT USAGE ON SCHEMA public to api_user;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public to api_user;
grant CONNECT on database portic_v5 to  api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to api_user;

GRANT USAGE ON SCHEMA navigo to porticapi;
GRANT USAGE ON SCHEMA navigocheck to porticapi;
GRANT USAGE ON SCHEMA navigoviz to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;
GRANT USAGE ON SCHEMA public to porticapi;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public to porticapi;
grant CONNECT on database portic_v5 to  porticapi;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi;


SELECT case when c.table_name= 'built_travels' then 'travels' else 'pointcalls' end as API, 
        c.column_name as name, 
        case when c.table_name= 'built_travels' then 't' else 'p' end||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('built_travels')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid;
       

select distinct pointcall, pointcall_uhgs_id , pointcall_states  
from navigoviz.pointcall p 
where pointcall_states like '%France%' and pointcall_province is null and pointcall_substates is null
-- Alger (off)	A1964989
Golfe de Lyon	A1964251
Cap Martin	A0210528
Roque Brunes	A0195707
Cap Saint Martin (off	A1965168
Port Cros (off	A1964197
[{"1749-1792" : "Monaco"},{"1793-1815" : "France"}]

select tonnage_uncertainity , sum(tonnage::float) 
from navigoviz.pointcall p
where pointcall_states like '%France%' and pointcall_province is null and pointcall_substates is null
group by tonnage_uncertainity

select pointcall_states, navigo.extract_state_fordate(pointcall_states, 1787) from navigoviz.pointcall p  where p.pointcall_uhgs_id = 'A0134597'
select navigo.extract_state_fordate(pointcall_states, 1798) from navigoviz.pointcall p  where p.pointcall_uhgs_id = 'A0134597'

CREATE OR REPLACE FUNCTION navigo.extract_state_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
			select null into state;
            execute 'select states::text from (
			select  pointcall, pointcall_uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
				from 
				(
				select  pointcall, pointcall_uhgs_id, json_array_elements(pointcall_states::json) as elt  
				from navigoviz.pointcall p
				where pointcall_states = '||quote_literal(tested_value)||' and tested_value is not null 
			) as q
			) as k
			where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
			INTO state;

                	--EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
           return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;
       
       
select json_array_length(pointcall_states::json)  from navigoviz.pointcall p

[{"1749-1792" : "Royaume de Piémont-Sardaigne"},{"1793-1815" : "France"},{"1815-1815" : "Royaume de Piémont-Sardaigne"}]

select pointcall, pointcall_uhgs_id, dates, states::text from (
	select  pointcall, pointcall_uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
		from 
		(
		select  pointcall, pointcall_uhgs_id, json_array_elements(pointcall_states::json) as elt  
		from navigoviz.pointcall p
	) as q
) as k
where substring(dates for 4)::int <= 1787 and substring(dates from 6 for 4)::int >= 1787 and states::text  like '"France"'

select navigo.extract_state_fordate(pointcall_states, 1787) from navigoviz.pointcall p  
where p.pointcall_uhgs_id = 'A0134597'



select distinct  travel_rank,ship_id,departure,departure_uhgs_id,departure_admiralty,departure_province,departure_states,destination,destination_uhgs_id,destination_admiralty,destination_province,destination_states,tonnage,tonnage_unit,source_suite,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,captain_uncertainity,cargo_uncertainity,taxe_uncertainity,travel_uncertainity
                    from navigoviz.built_travels,
                            ( select ship_id as subject, travel_rank as subject_order
                            from navigoviz.built_travels
                            where   source_entry <> 'both-to'  and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')
            and (destination_uhgs_id in ('A0145112') OR departure_uhgs_id in ('A0145112'))
                                ) as k
                    where ship_id = k.subject and  source_entry <> 'both-to'  and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')
                    and travel_rank >= k.subject_order and travel_rank <= k.subject_order
                    
select count(*), source_main_port_toponyme , substring(pointcall_out_date2 for 4)
from navigoviz.pointcall p where p.pointcall_function is null
group by source_main_port_toponyme, substring(pointcall_out_date2 for 4)
order by source_main_port_toponyme, substring(pointcall_out_date2 for 4)
-- 19432 / 147472


select count(*), source_main_port_toponyme , substring(pointcall_out_date2 for 4)
from navigoviz.pointcall p where p.ship_uncertainity =-2
group by source_main_port_toponyme, substring(pointcall_out_date2 for 4)
order by source_main_port_toponyme, substring(pointcall_out_date2 for 4)

select tonnage_uncertainity , sum(tonnage::float) 
from navigoviz.pointcall p
where navigo.extract_state_fordate(pointcall_states, 1787)='"France"' and pointcall_province is null and pointcall_substates is null
group by tonnage_uncertainity

select count(*), cargo_uncertainity
from navigoviz.pointcall 
group by cargo_uncertainity 

select count(*), up.data_block_leader_marker 
from navigoviz.pointcall v, navigo.pointcall up  
where up.pkid = v.pkid 
group by up.data_block_leader_marker


select count(*), up.ship_name 
from navigoviz.pointcall v, navigocheck.uncertainity_pointcall up  
where up.pkid = v.pkid 
group by up.ship_name

select count(*), up.captain_name 
from navigoviz.pointcall v, navigocheck.uncertainity_pointcall up  
where up.pkid = v.pkid 
group by up.captain_name
21841	-3
394	-2
56046	-1
69191	0

select count(*), pointcall
from navigoviz.pointcall v  
where pointcall = source_main_port_toponyme 
group by pointcall

select count(*)
from navigoviz.pointcall v  
where pointcall = source_main_port_toponyme or pointcall_function = 'O' or pointcall_function = 'Z'
-- 63440
-- 70447


select count(*), v.pointcall 
from navigoviz.pointcall v, navigocheck.uncertainity_pointcall up  
where up.pkid = v.pkid and up.captain_name = 0
group by v.pointcall

SELECT count(*), cargo_uncertainity FROM navigoviz.pointcall 
WHERE pointcall='Nantes' and (pointcall = source_main_port_toponyme or pointcall_function = 'O' ) 
group by cargo_uncertainity
SELECT count(*), captain_uncertainity FROM navigoviz.pointcall 
WHERE pointcall='Nantes' and (pointcall = source_main_port_toponyme or pointcall_function = 'O' ) 
group by captain_uncertainity
SELECT count(*), flag_uncertainity FROM navigoviz.pointcall 
WHERE pointcall='Nantes' and (pointcall = source_main_port_toponyme or pointcall_function = 'O' ) 
group by flag_uncertainity
SELECT count(*),homeport_uncertainity FROM navigoviz.pointcall 
WHERE pointcall='Nantes' and (pointcall = source_main_port_toponyme or pointcall_function = 'O' )
group by homeport_uncertainity


select distinct ship_id, departure,  destination
from navigoviz.built_travels bt where departure_shiparea = 'GUL-CARN'

select distinct ship_id, departure,  destination
from navigoviz.built_travels bt where departure_shiparea = 'GUL-LEEW'

select id, travel_rank , distance_dep_dest , departure , destination , outdate_fixed , ship_name, "class", q01 , q02, q03, tonnage, tonnage_class , abs(outdate_fixed-indate_fixed) 
from navigoviz.built_travels bt 
where ship_id = '0016174N'


select distinct ship_id, departure,  departure_states 
from navigoviz.built_travels bt where departure = 'Marseille' and destination_shiparea = 'GUL-CARN'

select distinct  travel_rank,ship_id,departure,departure_uhgs_id,departure_admiralty,departure_province,departure_states,destination,destination_uhgs_id,destination_admiralty,destination_province,destination_states,tonnage,tonnage_unit,source_suite,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,captain_uncertainity,cargo_uncertainity,taxe_uncertainity,travel_uncertainity
                    from navigoviz.built_travels,
                            ( select ship_id as subject, travel_rank as subject_order
                            from navigoviz.built_travels
                            where   source_entry <> 'both-to'  and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787') and destination_uhgs_id in ('A0210797')
                            and distance_dep_dest > 0 and ship_id = '0016174N'
                                ) as k
                    where ship_id = k.subject and  source_entry <> 'both-to'  and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')
                    and travel_rank <= k.subject_order and true
                    
                    
select distinct  travel_rank,ship_id,departure,departure_uhgs_id,departure_admiralty,departure_province,departure_states,destination,destination_uhgs_id,destination_admiralty,destination_province,destination_states,tonnage,tonnage_unit,source_suite,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,captain_uncertainity,cargo_uncertainity,taxe_uncertainity,travel_uncertainity
                    from navigoviz.built_travels,
                            ( select ship_id as subject, travel_rank as subject_order
                            from navigoviz.built_travels
                            where   source_entry <> 'both-to'  and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')
            and (destination_uhgs_id in ('A0210797') OR departure_uhgs_id in ('A0210797'))  and ship_id = '0016174N'
                                ) as k
                    where ship_id = k.subject and  source_entry <> 'both-to'  and (substring(departure_out_date for 4) = '1787' or substring(destination_in_date for 4) = '1787')
                    and travel_rank >= k.subject_order -0 and travel_rank <= k.subject_order+0
                    

20748	-3
1703	-2
55979	-1
69042	0

SELECT * FROM navigoviz.pointcall WHERE pointcall='Nantes' and (pointcall = source_main_port_toponyme or pointcall_function = 'O' )



select id, travel_rank , source_main_port_toponyme , distance_dep_dest , departure , destination , outdate_fixed , ship_name, "class", 
((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe, 
tonnage, tonnage_class , abs(outdate_fixed-indate_fixed) as duree_jours
from navigoviz.built_travels bt 
where  tonnage is not null and (q01 is not null or q02 is not null or q03 is not null)


select commodity_purpose, tonnage_class, tonnage, distance_dep_dest,  taxe/tonnage::float as ratio_taxe, distance_dep_dest::float/duree_jours as vitesse, distance_dep_dest/tonnage::float as capacite_tonnage from (
select id, commodity_purpose , travel_rank , source_main_port_toponyme , distance_dep_dest , departure , destination , outdate_fixed , ship_name, "class", 
((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe, 
tonnage, tonnage_class , abs(outdate_fixed-indate_fixed) as duree_jours
from navigoviz.built_travels bt 
where  tonnage is not null  and (q01 is not null or q02 is not null or q03 is not null)
) as k
where  tonnage::float > 0 and duree_jours ::float > 0


SELECT ogc_fid, uhgs_id, total, toponyme as  toponym, belonging_states, belonging_substates, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom, 3857)) as point
        FROM 
		(	select pointcall_uhgs_id, pointcall, count( *) as total
	         from navigoviz.pointcall gg 
	         where (substring(pointcall_out_date for 4) = '1787' or substring(pointcall_in_date for 4) = '1787')  
	         and pointcall_uhgs_id in ('A0214466', 'A0157543')
	         group by pointcall_uhgs_id, pointcall 
         ) as k 
         join ports.port_points p  
         on p.uhgs_id = k.pointcall_uhgs_id 
        where p.toponyme is not null 
        order by toponyme 

select record_id, pointcall , pointcall_uhgs_id  from navigoviz.pointcall
where pointcall in 
(
	select pointcall
	from navigoviz.pointcall p 
	group by pointcall 
	having count(distinct pointcall_uhgs_id ) > 1
) 
order by  pointcall

/*
 * 
 Agon	2
Alger (off)	2
Angleterre	2
Bizerte (off	2
Calella	2
Cap Couronne (off	2
Cap d' Héraclée (off	2
Cap Saint Sebastien (off	2
Cap Saint Sébastien (off	2
Cap Sicié (off	6
Gênes	2
Iles d' Hyères (off	3
Libourne	2
Mahon (off	3
Majorque (off	3
Planier (off	2
Tossa	2
Toulon (off	5
	8
 */

select record_id, pkid, pointcall, pointcall_uhgs_id, pointcall_point , pointcall_province , pointcall_admiralty ,source_doc_id , source_text , ship_name 
from navigoviz.pointcall p where pointcall = 'Agon'
00123028	18933
00144247	38987
00144440	39150

select * from navigo.pointcall where pkid in (18933, 38987,39150 )
                select *
                from ports.port_points gg where uhgs_id = 'A0214466'     
                A1465990 //alexandrette
                A0214466 //agon
A0157543 //agon

select * from ports.port_points where uhgs_id = 'A0214466'  
select * from ports.port_points where uhgs_id = 'A0209567'     

select toponyme, pointcall_uhgs_id, uhgs_id from 
(
                (select cp.pkid as identifiant, cp.data_block_local_id as doc_id, cp."source" as cpsource, * 
                    from navigocheck.check_pointcall cp 
                    left outer join public.pointcall_taxes ct on cp.data_block_local_id = ct.link_to_pointcall
                ) as q  
                left outer join ports.port_points ph on q.ship_homeport_uhgs_id = ph.uhgs_id
            ) as k where toponyme='Agon'
            
select * from ports.port_points where uhgs_id = 'A0743522'



SELECT case when c.table_name= 'built_travels' then 'travels' else 'pointcalls' end as API, 
        c.column_name as name, 
        case when c.table_name= 'built_travels' then 't' else 'p' end||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('built_travels')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid
union
SELECT case when c.table_name= 'built_travels' then 'travels' else 'pointcalls' end as API, 
        c.column_name as name, 
        case when c.table_name= 'built_travels' then 't' else 'p' end||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('pointcall')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid
union       
SELECT 'ports' as API, 
        c.column_name as name, 
        'p'||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('port_points')  and c.table_schema = 'ports' and pgd.objoid = st.relid
order by api, shortname 

select case when province is not null then pointcall_province else navigo.extract_state_fordate(pointcall_substates, 1787 ) end from navigoviz.pointcall 

select navigo.extract_state_fordate(pointcall_substates, 1787 ) 
from navigoviz.pointcall where pointcall_substates like 'Colonie%'

select source_doc_id, source_suite, source_main_port_toponyme , source_main_port_uhgs_id, pointcall, pointcall_uhgs_id , pointcall_action, pointcall_function, coalesce (pointcall_out_date2 , pointcall_in_date2) as obs_date, 
homeport_uhgs_id , homeport,  ship_id , ship_flag_id , ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit , 
commodity_purpose, commodity_standardized, quantity , commodity_purpose2, commodity_standardized2, quantity2, commodity_purpose3, commodity_standardized3, quantity3, commodity_purpose4, commodity_standardized4, quantity4, 
tax_concept , ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe
from navigoviz.pointcall limit 1

select latitude, longitude, pointcall_status, pointcall_admiralty, pointcall_province, pointcall_substates, pointcall_states, shiparea
json_array_elements(pointcall_states::json)
json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json)
from navigoviz.pointcall limit 1


select source_doc_id, source_component , source_main_port_uhgs_id, source_entry 
id, travel_rank, ship_id,  travel_uncertainity,
ship_name, ship_flag_id ,  "class" as ship_class, tonnage , tonnage_class , tonnage_unit , 
captain_id, captain_name, birthplace, status, citizenship, in_crew
homeport_uhgs_id , homeport, 
departure, departure_uhgs_id, outdate_fixed, departure_action, departure_function, departure_navstatus,
destination, destination_uhgs_id, indate_fixed, destination_action, destination_function, destination_navstatus,
commodity_purpose, commodity_standardized, quantity , commodity_purpose2, commodity_standardized2, quantity2, commodity_purpose3, commodity_standardized3, quantity3, commodity_purpose4, commodity_standardized4, quantity4, 
tax_concept , ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe,
distance_dep_dest, distance_homeport_dep , distance_dep_dest_miles, distance_homeport_dep_miles
from navigoviz.built_travels bt 

/*pointcall, pointcall_uhgs_id , pointcall_action, pointcall_function, coalesce (pointcall_out_date2 , pointcall_in_date2) as obs_date, 
homeport_uhgs_id , homeport,  ship_id , ship_flag_id , ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit , 
commodity_purpose, commodity_standardized, quantity , commodity_purpose2, commodity_standardized2, quantity2, commodity_purpose3, commodity_standardized3, quantity3, commodity_purpose4, commodity_standardized4, quantity4, 
tax_concept , ((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end)) as taxe
*/
from navigoviz.built_travels bt 
limit 1
alter table ports.port_points add column state_1787 text;
alter table ports.port_points add column substate_1787 text;

update ports.port_points p set state_1787 = etat
from 
		(
			select uhgs_id, states::text, replace(states::text,'"','') as etat from (
			select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
				from 
				(
				select  toponyme, uhgs_id, json_array_elements(belonging_states::json) as elt  
				from ports.port_points  p
			) as q
			) as k
			where substring(dates for 4)::int <= 1787 and substring(dates from 6 for 4)::int >= 1787
		)
		as k
where k.uhgs_id = p.uhgs_id 


update ports.port_points p set substate_1787 = etat
from 
		(
			select uhgs_id, states::text, replace(states::text,'"','') as etat from (
			select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
				from 
				(
				select  toponyme, uhgs_id, json_array_elements(belonging_substates::json) as elt  
				from ports.port_points  p
			) as q
			) as k
			where substring(dates for 4)::int <= 1787 and substring(dates from 6 for 4)::int >= 1787
		)
		as k
where k.uhgs_id = p.uhgs_id 

SELECT ogc_fid AS pkid, uhgs_id, toponyme, latitude, longitude, amiraute, province, state_1787, substate_1787, oblique, status 
FROM ports.port_points


select source_doc_id, source_suite , source_main_port_uhgs_id, source_main_port_toponyme, pointcall, pointcall_uhgs_id , pointcall_action, pointcall_function, 
coalesce ( outdate_fixed, indate_fixed ) as obs_date, pointcall_out_date2 as outdate, pointcall_in_date2 as indate, 
homeport_uhgs_id , homeport,  
ship_id , ship_flag_id , ship_name , "class" as ship_class, tonnage , tonnage_class , tonnage_unit , 
captain_id, captain_name, birthplace, status, citizenship, in_crew, 
commodity_purpose, commodity_standardized, quantity , commodity_purpose2, commodity_standardized2, quantity2, commodity_purpose3, commodity_standardized3, quantity3, commodity_purpose4, commodity_standardized4, quantity4, 
tax_concept , 
((case when q01 is null then 0 else q01::float*240 end) + (case when q02 is null then 0 else q02::float*12 end) + (case when q03 is null then 0 else q03::float end))
 from navigoviz.pointcall 
WHERE substring(pointcall_out_date for 4)::int = 1787 OR substring(pointcall_in_date for 4)::int= 1787


select * from ports.port_points pp where uhgs_id = 'A0167415' 

select uhgs_id, toponyme, state_1787, substate_1787, p.captain_name , p.citizenship , p.homeport , p.birthplace 
from ports.port_points pp, navigoviz.pointcall p 
where p.ship_flag_id = pp.uhgs_id 

-- 75815
select ship_flag_id, homeport_uhgs_id 
from navigoviz.pointcall p 
where substring(pointcall_out_date for 4)::int = 1787 OR substring(pointcall_in_date for 4)::int= 1787
 and p.homeport_uhgs_id != null 
 and p.ship_flag_id != null 
 and ship_flag_id % homeport_uhgs_id -- 39324
  -- 39324
  
 39324 / 75815 * 100
 -- Est-ce que in_crew est saisi à Marseille ? Non
  -- Est-ce que tonnage est est bien renseigné si in_crew contient l'information ? Oui
 -- Est-ce que le port de la source est bien un port oblique ? (Ars en Ré / Aligre de Marans dans ce cas) : Oui
 select record_id , in_crew, source_doc_id , source_component, source_main_port_toponyme , pp.status as source_port_status, tonnage, tonnage_unit, pointcall, pointcall_out_date , pointcall_in_date
 from navigoviz.pointcall p , ports.port_points pp
 where in_crew = 'tx' and p.source_main_port_uhgs_id = pp.uhgs_id 
 

----------------
-- done on 07 August 2020
alter table navigoviz.pointcall add column data_block_leader_marker text;
update navigoviz.pointcall p set data_block_leader_marker = cp.data_block_leader_marker 
from navigocheck.check_pointcall cp 
where p.pkid = cp.pkid; 
comment on COLUMN navigoviz.pointcall.data_block_leader_marker IS 'Filled with ''A'' when the pointcall line (not all entries of the same documentary_unit_id) in FileMaker has been manually checked against the content of the source (source_doc_id). A programm must be called after to propagate corrections from lines with A to other of the same documentary_unit_id. Not yet done in FileMaker. The main cause of inconsistencies in FileMaker database are due to this missing program for propagation of fixes to other entries of the same documentary_unit_id (source_doc_id).'

        
update navigoviz.pointcall set cargo_uncertainity = -3 where cargo_uncertainity is null; --29811
update navigoviz.pointcall set taxe_uncertainity = -3 where taxe_uncertainity is null; --130586

----------------

select source_suite , taxe_uncertainity, count(*)
from navigoviz.pointcall
group by source_suite, taxe_uncertainity

select source_doc_id from navigoviz.pointcall where source_suite = 'Marseille' and taxe_uncertainity is null
select  link_to_pointcall, * from navigocheck.check_taxes where link_to_pointcall='00183927'


select source_doc_id, taxe_uncertainity, pointcall , p.tax_concept , c.*
from navigoviz.pointcall p, navigocheck.check_taxes c
where source_doc_id = link_to_pointcall 
--and link_to_pointcall='00183927'

alter table navigoviz.debug_travels add column cargo_uncertainity int default -3


SELECT * 