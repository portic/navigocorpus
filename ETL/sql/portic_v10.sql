----------------------------------------------------------------------------------------
-- 19 juillet 2023 - Fabrique de la base portic_v10
-- Christine PLumejeaud-Perreau, UMR 7301 MIGRINTER 
-- ANR PORTIC
----------------------------------------------------------------------------------------

-- créer la base
sudo -u postgres created --encoding=UTF8  --owner=navigo portic_v8
sudo -u postgres psql -U postgres -p 5432 -d portic_v10 -c "GRANT dba TO navigo;"


CREATE ROLE navigo WITH CREATEDB LOGIN PASSWORD 'navigocorpus2018';
GRANT mydba TO navigo;
GRANT dba TO navigo;

-- Utiliser le script BuildNavigoviz.setExtensionsFunctions()
-- il fait ceci

create extension if not exists postgis ;
create extension if not exists fuzzystrmatch;
create extension if not exists pg_trgm;
create extension if not exists postgis_topology;
create extension if not exists plpython3u;
 
select * from pg_language;
SELECT lanpltrusted FROM pg_language WHERE lanname LIKE 'plpython3u';


create schema navigo;
create schema navigocheck;
create schema navigoviz;
create schema ports;
 
create role mydba with superuser noinherit; 
create role porticapi;
create role api_user;
ALTER USER porticapi WITH PASSWORD 'portic';
ALTER USER api_user WITH PASSWORD 'portic';


-- à la toute fin (quand on a toutes les données)

GRANT ALL ON SCHEMA navigo TO navigo;
GRANT ALL ON SCHEMA navigocheck TO navigo;
GRANT ALL ON SCHEMA navigoviz TO navigo;
GRANT ALL ON SCHEMA ports TO navigo;
GRANT ALL ON SCHEMA public TO navigo;
GRANT ALL ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO navigo;
grant ALL on all sequences in schema navigoviz, navigo, navigocheck, ports, public to navigo;

ALTER ROLE api_user NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN NOREPLICATION NOBYPASSRLS;

GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigocheck to  api_user;
GRANT USAGE ON SCHEMA navigo to  api_user;
GRANT USAGE ON SCHEMA public to  api_user;
GRANT USAGE ON SCHEMA ports to  api_user;
GRANT USAGE ON SCHEMA ports to  api_user;

ALTER ROLE porticapi NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN NOREPLICATION NOBYPASSRLS;


GRANT USAGE ON SCHEMA navigoviz TO porticapi;
GRANT USAGE ON SCHEMA navigocheck to porticapi;
GRANT USAGE ON SCHEMA navigo to porticapi;
GRANT USAGE ON SCHEMA public to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;

GRANT SELECT ON TABLE ports.port_points TO api_user;
GRANT SELECT ON TABLE ports.port_points TO porticapi;

GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO  api_user;
grant CONNECT on database portic_v8 to  api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to  api_user;

GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO porticapi;
grant CONNECT on database portic_v8 to porticapi;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi;

----------------------------------------------------------------------------------------------------------
-- Traitement des ports
----------------------------------------------------------------------------------------------------------

-- 1. run BuildPorts (activate setExtensionsFunctions())
-- 2. run BuildPorts (activate setExtensionsFunctions())

-- supprimer le dernier " de la ligne query = 
CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)
 RETURNS json
 LANGUAGE plpython3u
AS $function$
	import json
	from operator import itemgetter
	global temp
	global result
	global topos
	global query
	if toustopo is not None :
		result = []
		temp = toustopo.replace('{', '').replace('}', '').strip(' ')
		temp = temp.replace('"', '').strip(' ')
		topos = temp.split(',')
		for item in topos:
		    #plpy.notice(item)
		    
		    query = "select count(*) as freq from navigocheck.check_pointcall  where pointcall_uhgs_id = '"+uhgs_id+"' and pointcall_name='"+item.replace('\'', '\'\'')+"'""
		    
		    #plpy.notice(query)
		    rv = plpy.execute(query)
		    #plpy.notice(rv[0]["freq"])
		    result.append(dict(topo=item, freq=rv[0]["freq"]))
		result = sorted(result, key=itemgetter('freq'), reverse=True)

	else :
		result = None
	#plpy.notice (result)
	return json.dumps(result, ensure_ascii=False)
$function$
;

-- 3. Importer le vieux fichier de portic_v9 :  ports.port_points et etats, generiques_geoinclusions, labels_lang_csv
-- D:\Data\BDD-backups\portic\portic_v9\ports_[ports-points_etats_geoinclusion_labels].sql

sudo -u postgres psql -U postgres -p 5432 -d portic_v10 -f D:\Data\BDD-backups\portic\portic_v9\ports_[ports-points_etats_geoinclusion_labels].sql

-- 4 Traitement du fichier de Silvia

-- complétion des toponymes

select * from ports.etats where uhgs_id = 'A0406727';-- Bridlington e Grande-Bretagne
select * from ports.etats where uhgs_id = 'A0242201';--Maiori e Royaume de Naples
select * from ports.etats where uhgs_id = 'A0221982';-- Capo Caccia e Royaume de Piémont-Sardaigne : MISSING


drop table ports.correction_ports_toponymes_missing ;
select * from ports.correction_ports_toponymes_missing ;

select toponyme , topofreq , toustopos , p.toponyme_standard_fr , p.toponyme_standard_en, c.toponyme_standard_fr, c.toponyme_standard_en
from ports.port_points p , ports.correction_ports_toponymes_missing c
where c.uhgs_id = p.uhgs_id ;

update ports.port_points p set toponyme_standard_fr =  c.toponyme_standard_fr, toponyme_standard_en = c.toponyme_standard_en
from ports.correction_ports_toponymes_missing c
where c.uhgs_id = p.uhgs_id ;

select 'toponyme' as label_type, uhgs_id as key_id, toponyme_standard_fr as fr, toponyme_standard_en as en 
from ports.correction_ports_toponymes_missing c;

insert into ports.labels_lang_csv (label_type, key_id, fr, en)
select 'toponyme' as label_type, uhgs_id as key_id, toponyme_standard_fr as fr, toponyme_standard_en as en 
from ports.correction_ports_toponymes_missing c;
-- 29


insert into ports.etats (uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto, country2019_name, toponyme_standard_fr, geonameid, name_en, admin1_code, latitude, longitude, tgnid)
select 'A0221982', 'Capo Caccia', etat, subunit, etat_en , subunit_en, dfrom, dto, country2019_name, 'Capo Caccia', geonameid, name_en, admin1_code, latitude, longitude, tgnid
from ports.etats
where uhgs_id in ('A0250475');


select * from ports.etats where subunit like '%zone marit%'

select * from ports.etats where uhgs_id in (select uhgs_id from ports.correction_ports_toponymes_missing where etatslike = 'Zone maritime');
-- proche de Monaco	A1969424 à supprimer
delete from ports.etats where uhgs_id in (select uhgs_id from ports.correction_ports_toponymes_missing where etatslike = 'Zone maritime');
-- 2 lignes supprimées

-- les 3 petites corrections


-- A0251618	A CORRIGER: c'est : Carrare (et pas Sant'Antoine de Carrare)
select toponyme , topofreq , toustopos , p.toponyme_standard_fr , p.toponyme_standard_en
from ports.port_points p
where p.uhgs_id = 'A0251618';
-- where p.uhgs_id = 'A0237391';

update ports.port_points p set toponyme_standard_fr = 'Carrare', toponyme_standard_en='Carrara'
where p.uhgs_id = 'A0251618';

select * from ports.etats e where uhgs_id = 'A0251618';
update ports.etats set toponyme_standard_fr = 'Carrare' where uhgs_id = 'A0251618';

select * from ports.labels_lang_csv llc where llc.label_type = 'toponyme' and key_id='A0251618';
update ports.labels_lang_csv set fr = 'Carrare', en='Carrara' where label_type = 'toponyme' and key_id='A0251618';


-- A0237391	éliminer: c'était l'ancien code de Carrare
-- Christine : j'attend pour supprimer de voir qu'il n'est vraiment pas utilisé

-- A0167415	ce code correspond à "France", il faut corriger le toponyme qui actuellement est "Bernic (off)"
-- A0167415	Bernic (sur mer)	Bernic (off)

update ports.port_points p set toponyme_standard_fr = 'France', toponyme_standard_en='France'
where p.uhgs_id = 'A0167415';

update ports.labels_lang_csv set fr = 'France', en='France' where label_type = 'toponyme' and key_id='A0167415';

--- l'onglet ETATS

select * from ports.labels_lang_csv where label_type = 'flag' and key_id = 'A0128592'
-- ok et savoyard comme nécessaire

select * from ports.labels_lang_csv where label_type = 'flag' and key_id = 'A0765201'
-- flag	A0765201	brémois	Bremen		1350
select * from ports.labels_lang_csv where label_type = 'flag' and key_id = 'A1149784';
-- flag	A1149784	polonais	Polish		1354
select * from ports.labels_lang_csv where label_type = 'flag' and key_id = 'A1968862';
-- flag	A1968862	oldenbourgeois	Oldenburg		1361
update ports.labels_lang_csv  set fr = 'Oldembourgois', en = 'Oldenburguese' where label_type = 'flag' and key_id = 'A1968862';

-- Les MISSING flag 
select * from ports.labels_lang_csv where label_type = 'flag' and key_id = 'A0253040'
-- missing
select * from ports.labels_lang_csv where label_type = 'flag' and key_id = 'A1632045';
-- missing

select * from ports.port_points pp where pp.uhgs_id = 'A0253040';
select * from ports.etats e  where uhgs_id = 'A0253040';
select * from ports.port_points pp where pp.uhgs_id = 'A1632045';
select * from ports.etats e  where uhgs_id = 'A1632045';

-- A1632045	Marocain	Moroccan
-- A0253040	Algérien	Algerian

insert into ports.labels_lang_csv (label_type, key_id, fr, en)
values 
('flag' , 'A1632045', 'Marocain' , 'Moroccan'  ), 
('flag' , 'A0253040', 'Algérien' , 'Algerian'  );
-- 2 lignes


--- les nouvelles identifications

select uhgs , "toponyme fr" , "toponyme GB" , lat, long, lat::float , replace(long, '.', ','), replace(long, ',', '.')::float(53), etatlike  from ports.nouveaux_ports_v10 n;

update ports.nouveaux_ports_v10  set long = '23.093' where uhgs = 'A1970729';

select uhgs, fr, en, lat::float, long::float, etatlike from 
(select uhgs , "toponyme fr" as fr, "toponyme GB" as en, lat ,  trim(replace(long, ',', '.')) as long, etatlike  from ports.nouveaux_ports_v10 n) as k;

select uhgs, fr, en, lat::float, long::float, etatlike, array[fr] as toustopos, fr as toponyme,  ports.frequency_topo(uhgs, array_to_string(array[fr], ','))  from 
(select uhgs , "toponyme fr" as fr, "toponyme GB" as en, lat ,  trim(replace(long, ',', '.')) as long, etatlike  from ports.nouveaux_ports_v10 n) as k;

select count(*) as freq from navigocheck.check_pointcall  where pointcall_uhgs_id = 'A0219924' 
--- insertdate

insert into ports.port_points (uhgs_id, latitude, longitude, toponyme, insertdate, toustopos, topofreq,  toponyme_standard_fr, toponyme_standard_en)
select uhgs,  lat, long, fr as toponyme, 'juillet 2023', array[fr] as toustopos,  ports.frequency_topo(uhgs, array_to_string(array[fr], ',')), fr, en  from 
(select uhgs , "toponyme fr" as fr, "toponyme GB" as en, lat ,  trim(replace(long, ',', '.')) as long, etatlike  from ports.nouveaux_ports_v10 n) as k;
--- 59 
-- 16 

update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where insertdate = 'juillet 2023';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857) where insertdate = 'juillet 2023';


select p.uhgs_id , p.toponyme_standard_fr , n.etatlike 
from ports.port_points p, ports.nouveaux_ports_v10 n  where insertdate = 'juillet 2023' and p.uhgs_id = n.uhgs 

select * from ports.etats e where uhgs_id in (select n.uhgs from ports.nouveaux_ports_v10 n)
-- Limekilns	A0383127	Grande-Bretagne

insert into ports.etats (uhgs_id, toponyme, etat, subunit, etat_en , subunit_en, dfrom, dto, country2019_name, toponyme_standard_fr, geonameid, name_en, admin1_code, latitude, longitude, tgnid)
select p.uhgs_id, p.toponyme_standard_fr, etat, subunit, etat_en , subunit_en, dfrom, dto, e.country2019_name, p.toponyme_standard_fr, e.geonameid, name_en, admin1_code, e.latitude, e.longitude, tgnid
from ports.etats e, ports.port_points p, ports.nouveaux_ports_v10 n
where e.uhgs_id = n.etatlike and insertdate = 'juillet 2023' and p.uhgs_id = n.uhgs and p.uhgs_id!= 'A0383127'
order by p.uhgs_id;
-- 42 ajouts correspondants dans etats
-- 23 ajouts correspondants dans etats

select * from ports.port_points pp where pp.country2019_name = 'France';
-- 

-- attention, augmenter le buffer progressivement de 1 à 10 km puis 50 km
update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 1000)) and p.country2019_name is null and insertdate = 'juillet 2023' and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate = 'juillet 2023' ;
-- 40 + 14
update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 10000)) and p.country2019_name is null and insertdate = 'juillet 2023' and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate = 'juillet 2023' ;
-- 16 + 2
update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 50000)) and p.country2019_name is null and insertdate = 'juillet 2023' and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate = 'juillet 2023' ;
-- 2 lignes dont une double appartenance de Sercq (A1946025) à Jersey & Guernsey
-- Reste A1965240	Arzew (sur) sans code. 
       
select count(*) from ports.port_points where country2019_iso2code is null and point3857 is not null
-- 31

--- pour le gazettier, les appartenances étatiques, en commençant par les ports de France, a priori,
-- puis en lisant états (il ya des ports aujourd'hui francais qui ne l'étaient pas avant comme Roquebrune)

-- tous les ports francais nouveau     
update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
            from (
                select  uhgs_id, toponyme, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances
                from ports.port_points 
                where country2019_name = 'France' and uhgs_id not in (select uhgs_id from ports.etats e) and insertdate = 'juillet 2023'
            ) as k 
        where pp.uhgs_id = k.uhgs_id  and belonging_states is null;
-- 19 / 21 + 9
-- attention, Silvia spécifie en zone maritime
-- A1970715	Sormiou (sur)	{"1749-1815" : "France"}	{"1749-1815" : "France"}
-- A1965182	Cap Couronne
update ports.port_points pp set belonging_states = null, belonging_states_en=null where uhgs_id in ('A1970715', 'A1965182', 'A1969424');
-- proche de Monaco	A1969424 à supprimercar en zone maritime


-- 'A0221982', 'Capo Caccia' à mettre à jour au niveau belonging_states

-- prendre en compte les nouveaux ports dont on a spécifié les apparenances étatiques, mais aussi les corrections
update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, etat, case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat_en) :: text as belonging_en
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id  and (insertdate = 'juillet 2023' or pp.uhgs_id = 'A0221982');
-- 35 / 44
       
select count(*) from ports.port_points where belonging_substates is null ; -- 1138 / 1073 / 1103 / 1088
update ports.port_points pp set belonging_substates = null, belonging_substates_en=null where uhgs_id in ('A1970715', 'A1965182', 'A1969424');

update ports.port_points pp set belonging_substates = '['||k.appartenances ||']', belonging_substates_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, subunit,  case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit_en) :: text as belonging_en
                from ports.etats 
                WHERE subunit IS NOT null
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id and (insertdate = 'juillet 2023' or pp.uhgs_id = 'A0221982');
-- 15  / 22     
       
update ports.port_points set state_1789_fr = ports.extract_state_fordate(belonging_states, 1789), 
state_1789_en = ports.extract_state_en_fordate(belonging_states_en, 1789) ,
substate_1789_fr = ports.extract_substate_fordate(belonging_substates, 1789) ,
substate_1789_en = ports.extract_substate_en_fordate(belonging_substates_en, 1789) 
where  (insertdate = 'juillet 2023' or uhgs_id in ('A0221982', 'A1970715', 'A1965182', 'A1969424'));
-- 61 / 77

update ports.port_points set state_1789_fr =null,
state_1789_en = null,
substate_1789_fr = null ,
substate_1789_en = null 
where  (insertdate = 'juillet 2023' or uhgs_id in ('A0221982', 'A1970715', 'A1965182', 'A1969424'));

select count(*) from  ports.port_points  where  state_1789_en is null or state_1789_fr is null  ;
-- 41, ce sont les points en zone maritim
select * from ports.port_points  where  state_1789_en  is null and (insertdate = 'juillet 2023'  or uhgs_id in ('A0221982', 'A1970715', 'A1965182', 'A1969424'));
-- 7 

update ports.port_points pp set relation_state = k.appartenances
                from (
                select uhgs_id, json_agg(arelation) as appartenances
                    from
                    (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                    case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
                    'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
                    from (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is not null
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is null
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is not null 
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is  null 
                        )
                    ) as k 
                    group by uhgs_id, etat, geonameid, tgnid
                    ) as k
                    group by uhgs_id
                    order by uhgs_id
                ) as k
                where pp.uhgs_id = k.uhgs_id and relation_state is null;
-- 35 + 9

-- France et Iceland
update ports.port_points pp set relation_state = k.appartenances ::text
            from (
            select uhgs_id,  json_agg(arelation) as appartenances
                from
                (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                'http://www.geonames.org/'||geonameid ,
                'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
                from (
                    
                    select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    from ports.port_points 
                    where  country2019_name = 'France'
                    and uhgs_id not in (select uhgs_id from ports.etats e)
                    union all
                    (
                    select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    )
                ) as k 
                group by uhgs_id, etat, geonameid
                ) as k
                group by uhgs_id
                order by uhgs_id
            ) as k
            where pp.uhgs_id = k.uhgs_id and relation_state is null ;
-- 21 + 7
           
-- supprimer des [] sur relation_state
update ports.port_points set relation_state = substring(relation_state from 2 for length(relation_state)-2) where substring (relation_state from 1 for 1)='[';
-- 56 / 16

------------------------
-- Traiter ici les ports pour la mise à jour des attributs suivants 
-- Si à l'étranger
-- shiparea
-- partner_balance_1789  partner_balance_supp_1789 
-- partner_balance_1789_uncertainty partner_balance_supp_1789_uncertainty

select * from ports.port_points where state_1789_fr != 'France' and insertdate = 'juillet 2023' and shiparea is null

-- select * from ports.port_points where uhgs_id = 'B2826728'
-- select * from ports.port_points where toponyme_standard_fr  = 'Biddeford'

-- supprimer B2826728 and insertdate = 'juillet 2023'

-- Si en France
-- provinces amirauté  
-- ferme	bureau	
-- ferme_direction_uncertainty ferme_bureau_uncertainty 
-- partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
-- partner_balance_1789=null, partner_balance_1789_uncertainty=null,

-- A0137297 (Marselle) : bureau de ferme de Alligre ou bureau de ferme de La Rochelle ? 
update ports.port_points set toponyme_standard_fr = 'France', toponyme = 'France', toponyme_standard_en = 'France' where uhgs_id = 'A0167415';-- bernic


update ports.port_points set amiraute ='Marennes', province='Saintonge', shiparea='ACE-ROCH', ferme_direction='La Rochelle', ferme_bureau='Marennes', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0218987';
update ports.port_points set amiraute ='La Rochelle', province='Aunis', shiparea='ACE-ROCH', ferme_direction='La Rochelle', ferme_bureau='Saint-Martin île de Ré', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A1970649';
update ports.port_points set amiraute ='Vannes', province='Bretagne', shiparea='ACE-IROI', ferme_direction='Lorient', ferme_bureau='Vannes', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0164751';
update ports.port_points set amiraute ='La Rochelle', province='Aunis', shiparea='ACE-ROCH', ferme_direction='La Rochelle', ferme_bureau='La Rochelle', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=-1, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A1970651';
update ports.port_points set amiraute ='Caen', province='Normandie', shiparea='MAN-WIGH', ferme_direction='Caen', ferme_bureau='Caen', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0214518';
update ports.port_points set amiraute ='Bordeaux', province='Guyenne', shiparea='ACE-ROCH', ferme_direction='Bordeaux', ferme_bureau='Bordeaux', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=-1, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0137983';
update ports.port_points set amiraute ='Bordeaux', province='Guyenne', shiparea='ACE-ROCH', ferme_direction='Bordeaux', ferme_bureau='Blaye', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=-1, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0145457';
update ports.port_points set amiraute ='Antibes', province='Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau='Cannes', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0126854';
update ports.port_points set amiraute ='Bastia', province='Corse', shiparea='MED-TYNO', ferme_direction='', ferme_bureau='', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0134994';
update ports.port_points set amiraute ='Marseille', province='Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau='Marseille', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0135619';
update ports.port_points set amiraute ='Toulon', province='Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau='Toulon', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0140249';
update ports.port_points set amiraute ='Toulon', province='Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau='Toulon', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0149305';
update ports.port_points set amiraute ='Agde', province='Languedoc', shiparea='MED-LION', ferme_direction='Montpellier', ferme_bureau='Agde', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0172105';
update ports.port_points set amiraute ='Marseille', province='Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau='Marseille', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0188101';
update ports.port_points set amiraute ='Marseille', province='Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau='Marseille', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0190464';
update ports.port_points set amiraute ='Toulon', province='Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau='Toulon', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0214237';
update ports.port_points set amiraute ='Marseille', province='Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau='Marseille', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A1965187';
update ports.port_points set amiraute ='Ajaccio', province='Corse', shiparea='MED-CORS', ferme_direction='', ferme_bureau='', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A1970730';
update ports.port_points set amiraute ='Marseille', province='Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau='Marseille', ferme_direction_uncertainty=0, ferme_bureau_uncertainty=0, partner_balance_1789=null, partner_balance_1789_uncertainty=null, partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A1970734';
-- 20

update ports.port_points set shiparea='NOR-THAM', partner_balance_1789='Angleterre', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0395865';
update ports.port_points set shiparea='ANE-SHAN', partner_balance_1789='Angleterre', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0599363';
update ports.port_points set shiparea='NOR-FORH', partner_balance_1789='Angleterre', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0383127';
update ports.port_points set shiparea='BAL-BELT', partner_balance_1789='Danemark', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0818744';
update ports.port_points set shiparea='BAL-SKAG', partner_balance_1789='Danemark', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0849490';
update ports.port_points set shiparea='MAN-PORT', partner_balance_1789='Angleterre', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1946025';
update ports.port_points set shiparea='BAL-SKAG', partner_balance_1789='Danemark', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0854497';
update ports.port_points set shiparea='ANE-LUND', partner_balance_1789='Angleterre', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0396041';
update ports.port_points set shiparea='ANE-MALI', partner_balance_1789='Angleterre', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0596365';
update ports.port_points set shiparea='BAL-BOTS', partner_balance_1789='Suède', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0952914';
update ports.port_points set shiparea='MED-BALN', partner_balance_1789='Espagne', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1965258';
update ports.port_points set shiparea='MED-BALN', partner_balance_1789='Espagne', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0121217';
update ports.port_points set shiparea='BAL-BELT', partner_balance_1789='Suède', partner_balance_supp_1789='Etranger', partner_balance_1789_uncertainty=0, partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1051905';
-- 14

update ports.port_points set shiparea='MED-SIST', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0018039';
update ports.port_points set shiparea='MED-TYNO', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0222488';
update ports.port_points set shiparea='MED-LIGS', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0236030';
update ports.port_points set shiparea='MED-AEGS', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0324413';
update ports.port_points set shiparea='MED-AEGN', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0329771';
update ports.port_points set shiparea='MED-AEGS', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0343682';
update ports.port_points set shiparea='MED-MEDE', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0351381';
update ports.port_points set shiparea='BSE-STRA', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1527889';
update ports.port_points set shiparea='BSE-STRA', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1533395';
update ports.port_points set shiparea='ACE-CASA', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1680286';
update ports.port_points set shiparea='ACE-AGAD', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1683009';
update ports.port_points set shiparea='MED-IONE', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1965487';
update ports.port_points set shiparea='MED-TYWS', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1970727';
update ports.port_points set shiparea='MED-AEGS', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1970728';
update ports.port_points set shiparea='MED-ADRC', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0248902';
update ports.port_points set shiparea='MED-SIST', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1727261';
update ports.port_points set shiparea='MED-AEGN', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1970729';
update ports.port_points set shiparea='MED-TYNO', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A0219924';
update ports.port_points set shiparea='MED-TYNO', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1970732';
update ports.port_points set shiparea='MED-SIST', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1970733';
update ports.port_points set shiparea='MED-SIST', partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id='A1970735';
-- 21


update ports.port_points set amiraute='Bordeaux', province='Guyenne', shiparea='ACE-ROCH', ferme_direction='Bordeaux', ferme_bureau='Bordeaux', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0137984';
update ports.port_points set amiraute='Brest', province='Bretagne', shiparea='MAN-PLYM', ferme_direction='Lorient', ferme_bureau='Brest', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0151643';
update ports.port_points set amiraute='Marennes', province='Saintonge', shiparea='ACE-ROCH', ferme_direction='La Rochelle', ferme_bureau='Marennes', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0165215';
update ports.port_points set amiraute='Brest', province='Bretagne', shiparea='MAN-PLYM', ferme_direction='Lorient', ferme_bureau='Brest', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0172916';
update ports.port_points set amiraute='Quimper', province='Bretagne', shiparea='ACE-IROI', ferme_direction='Lorient', ferme_bureau='Quimper', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0174956';
update ports.port_points set amiraute='Saint-Malo', province='Bretagne', shiparea='MAN-PORT', ferme_direction='Saint-Malo', ferme_bureau='Saint-Malo', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0195826';
update ports.port_points set amiraute='Saint-Malo', province='Bretagne', shiparea='MAN-PORT', ferme_direction='Saint-Malo', ferme_bureau='Saint-Malo', partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0203568';
update ports.port_points set shiparea='NOR-GBIG',  partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0630351';
update ports.port_points set shiparea='NOR-GBIG',  partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0631920';
update ports.port_points set shiparea='NOR-THAM',  partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0635133';
update ports.port_points set shiparea='BAL-BAWE',  partner_balance_1789='Danemark', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0760639';
update ports.port_points set shiparea='BAL-SKAG',  partner_balance_1789='Danemark', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0884415';
update ports.port_points set shiparea='BAL-BOTS',  partner_balance_1789='Suède', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A0948094';
update ports.port_points set shiparea='ACE-FINI',  partner_balance_1789='Espagne', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'A1964909';
update ports.port_points set shiparea='AUE-GRAY',  partner_balance_1789='Etats-Unis', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'B2826728';
update ports.port_points set shiparea='AUE-BALT',  partner_balance_1789='Etats-Unis', partner_balance_1789_uncertainty=0, partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 where uhgs_id = 'B0000517';
--16

-- en zone maritime
update ports.port_points set shiparea='MED-BALN' where uhgs_id='A0117763';
update ports.port_points set shiparea='MED-LIGS' where uhgs_id='A1970715';
update ports.port_points set shiparea='MED-LION' where uhgs_id='A1965182';
update ports.port_points set shiparea='MED-BALS' where uhgs_id='A1965240';
update ports.port_points set shiparea='MED-TYNO' where uhgs_id='A1969513';
update ports.port_points set shiparea='MED-BALS' where uhgs_id='A1970731';

--- les nouvelles identifications : insérer leur toponyme dans labels_lang_csv (toponyme)
-- 59 / 52 
insert into ports.labels_lang_csv (label_type, key_id, fr, en)
select 'toponyme', uhgs , "toponyme fr" , "toponyme GB" from ports.nouveaux_ports_v10 n
where uhgs not  in (select key_id from ports.labels_lang_csv  where label_type='toponyme');--52 + 15 sans Kolijnsplaat
-- select * from ports.port_points pp where pp.toponyme_standard_fr = 'Kolijnsplaat' -- A0635133

-- les ports qui sont utilisés mais pas dans ma table port
select cp.pointcall_uhgs_id , min(cp.pointcall_name) as toponyme,  array_agg( distinct trim(cp.pointcall_name)) as toustopos , array_agg( distinct trim(cp.record_id)) as les_pointcall_records , 'juillet 2023'
from navigocheck.check_pointcall cp 
where cp.pointcall_uhgs_id not in (select uhgs_id from ports.port_points) 
group by cp.pointcall_uhgs_id;

-- seul nouveau 
-- A0226455	La Planose	{"La Planose"}	{00325290}	juillet 2023
-- https://www.geonames.org/255096/planos.html
-- lat 37.81635, long 20.86437 Planos (en) et Tsivili (fr) https://en.wikipedia.org/wiki/Planos en Grèce, geoname_id = 255096

-- Identifiés autrement 
--A0632438	Cootyes Plaat	{"Cootyes Plaat"}	{00120575}	juillet 2023 à remplacer par A0635133	Kolijnsplaat
--A0829022	Danemark	{Danemark}	{00281827,00282505}	juillet 2023
--A0932858	Carleby en Finlande	{"Carleby en Finlande","Carleby en Fintlande [Finlande]"}	{00300893,00304282}	juillet 2023
--A1964107	Etats de Venise	{"Etats de Venise"}	{00303014,00303036}	juillet 2023

select * from navigocheck.check_pointcall cp  where pointcall_uhgs_id  = 'A0226455'
-- A0632438 Cootyes Plaat [Colijnsplaat]

-- les homeports qui sont codés mais pas dans ma table port
-- insert into ports.port_points (uhgs_id, toustopos, insertdate)
select cp.ship_homeport_uhgs_id, min(cp.ship_homeport) as toponyme,  array_agg( distinct trim(cp.ship_homeport)) as toustopos , array_agg( distinct trim(cp.record_id)) as les_pointcall_records , 'juillet 2023'
from navigocheck.check_pointcall cp 
where cp.ship_homeport_uhgs_id not in (select uhgs_id from ports.port_points pp2) 
group by cp.ship_homeport_uhgs_id;

-- les captain_birthplace_id qui sont codés mais pas dans ma table port

-- insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate)
select cp.captain_birthplace_id ,min(cp.captain_birthplace) as toponyme,  array_agg( distinct trim(cp.captain_birthplace)) as toustopos , array_agg( distinct trim(cp.record_id)) as les_pointcall_records , 'juillet 2023' 
from navigocheck.check_pointcall cp 
where cp.captain_birthplace_id not in (select uhgs_id from ports.port_points pp2) 
group by cp.captain_birthplace_id;
-- 1 ligne : erreur sur le captain_birthplace_id
-- Marseille	Marseille	{Marseille}	{00365168,00365169}	juillet 2023

-- les captain_citizenship_id qui sont codés mais pas dans ma table port

-- insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate)
select cp.captain_citizenship_id, min(cp.captain_citizenship) as toponyme, array_agg( distinct trim(cp.captain_citizenship))  as toustopos , array_agg( distinct trim(cp.record_id)) as les_pointcall_records , 'juillet 2023' 
from navigocheck.check_pointcall cp 
where cp.captain_citizenship_id not in (select uhgs_id from ports.port_points pp2) 
group by cp.captain_citizenship_id;

select toponyme_standard_fr , toponyme_standard_en  from ports.port_points pp where pp.uhgs_id ='A1969195'
select uhgs_id, toponyme_standard_fr , toponyme_standard_en  from ports.port_points pp where pp.toponyme_standard_fr ='Danemark'

-- Signaler les doublons 
-- DOUBLON 'A1813720', 'A1770543' -- Vyborg
-- DOUBLON ou presque A0917656 Egersund A0907996 
-- DOUBLON A0237391	de Carrare A0251618
A0829022	Danemark

select uhgs_id, toponyme_standard_fr , toponyme_standard_en, latitude , longitude  
from ports.port_points pp 
where pp.toponyme_standard_fr in 
(
select distinct ship_homeport
from navigocheck.check_pointcall cp 
where cp.ship_homeport_uhgs_id not in (select uhgs_id from ports.port_points pp2) )

-- DOUBLON 'A1813720', 'A1770543' -- Vyborg

select pointcall_uhgs_id , pointcall_name, record_id  from navigocheck.check_pointcall cp 
where pointcall_uhgs_id='A1813720' or ship_homeport_uhgs_id ='A1813720' or captain_birthplace_id = 'A1813720' or captain_citizenship_id='A1813720';

select * from ports.etats where uhgs_id = 'A1813720';
select * from ports.labels_lang_csv where key_id = 'A1770543' and label_type='toponyme';
select * from ports.generiques_geoinclusions gg where ughs_id ='A1770543';

delete from ports.port_points where uhgs_id = 'A1813720';
delete from ports.etats where uhgs_id = 'A1813720';
delete from ports.labels_lang_csv  where key_id = 'A1813720' and label_type='toponyme';
delete from ports.generiques_geoinclusions where ughs_id = 'A1813720';

-- DOUBLON ou presque A0917656 Egersund A0907996 

select pointcall_uhgs_id , pointcall_name, record_id  from navigocheck.check_pointcall cp 
where pointcall_uhgs_id='A0907996' or ship_homeport_uhgs_id ='A0907996' or captain_birthplace_id = 'A0907996' or captain_citizenship_id='A0907996';

select * from ports.etats where uhgs_id = 'A0907996';
select * from ports.labels_lang_csv where key_id = 'A0907996' and label_type='toponyme';
select * from ports.generiques_geoinclusions gg where ughs_id ='A0907996';

delete from ports.port_points where uhgs_id = 'A0917656';
delete from ports.etats where uhgs_id = 'A0917656';
delete from ports.labels_lang_csv  where key_id = 'A0917656' and label_type='toponyme';
delete from ports.generiques_geoinclusions where ughs_id = 'A0917656';

-- DOUBLON A0237391	de Carrare A0251618
-- Impossible de le supprimer
select pointcall_uhgs_id , pointcall_name, record_id  from navigocheck.check_pointcall cp 
where pointcall_uhgs_id='A0237391'  or ship_homeport_uhgs_id ='A0237391' or captain_birthplace_id = 'A0237391' or captain_citizenship_id='A0237391';

select * from ports.etats where uhgs_id = 'A0907996';
select * from ports.labels_lang_csv where key_id = 'A0907996' and label_type='toponyme';
select * from ports.generiques_geoinclusions gg where ughs_id ='A0907996';

/*
delete from ports.port_points where uhgs_id = 'A0917656';
delete from ports.etats where uhgs_id = 'A0917656';
delete from ports.labels_lang_csv  where key_id = 'A0917656' and label_type='toponyme';
delete from ports.generiques_geoinclusions where ughs_id = 'A0917656';
*/


-- nouveaux DOUBLONS identifiés suite au mail de Silvia le 21 juillet (repérés dans les homeports_uhgs_id)
-- A0635668	Staveren A0617348 Stavoren
-- A1064246	Gothenbourg A1039917 Gothembourg
-- A1964053	Copenagues A0816635 Copenhague
-- A1964093	Lanion A0162246 Lannion
-- A1964984	Isle de Batz A0217141 île de Batz
-- A1965140	Worckum A0637735 Workum
-- A0601048	Daingle en Irlande A0594881 Dingle

select pointcall_uhgs_id , pointcall_name, record_id, ship_homeport_uhgs_id, captain_birthplace_id, captain_citizenship_id  from navigocheck.check_pointcall cp 
where pointcall_uhgs_id='A0601048' or captain_birthplace_id = 'A0601048' or captain_citizenship_id='A0601048';
-- or ship_homeport_uhgs_id ='A1964053' 

select * from ports.etats where uhgs_id = 'A0816635';
select * from ports.labels_lang_csv where key_id = 'A0594881' and label_type='toponyme';
select * from ports.generiques_geoinclusions gg where ughs_id ='A0907996';

delete from ports.etats where uhgs_id = 'A0601048';
delete from ports.labels_lang_csv  where key_id = 'A0601048' and label_type='toponyme';
delete from ports.generiques_geoinclusions where ughs_id = 'A0601048';


select uhgs_id, toponyme_standard_fr  from ports.port_points pp where pp.uhgs_id  ='A0594881'
where pointcall_uhgs_id='A0237391'  or ship_homeport_uhgs_id ='A0237391' or captain_birthplace_id = 'A0237391' or captain_citizenship_id='A0237391';

---------- 
-- supprimer certaines données ? or task_fr_1787 
select * from navigocheck.check_pointcall cp where ship_homeport_uhgs_id = 'A0128576'; -- Ault
-- ('00283274',00283275,00283277,00283278,00283346,00283347,00283348,00283349,00283490,00283513,00283612,00283613,00283614,00283784,00283785,00283786,00283787,00283904,00283905,00284000,00284005,00284011,00284026,00284027,00284058,00284138,00284140,00284175,00284188,00285055,00285188,00285190)
-- ANF, G5-154-2 (Saint Valery sur Somme) 

select substring(pointcall_date from 0 for 5) as annee, record_id , pointcall_action, pointcall_function, pointcall_name, link_to_source_01 
from navigocheck.check_pointcall cp 
where link_to_source_01 = 'ANF, G5-154-2' and substring(pointcall_date from 0 for 5) = '1783'
order by annee;

select distinct substring(pointcall_date from 0 for 5) as annee
from navigocheck.check_pointcall cp 
where link_to_source_01 = 'ANF, G5-154-2';


-----------------------------------------------------------------
-- pour le calcul des routes, voir le script sur le GIT : D:\Dev\portic_humanum\porticapi\sql\portic_routepaths_allfunctions.sql
-- sudo -u postgres psql -U postgres -d portic_v10 -f /home/plumegeo/portic/portic_V8/portic_V8_shoreline16jan2023.sql
psql -U postgres -d portic_v10 -f D:\Data\BDD-backups\portic\2023011_data_18janvier2023\portic_V8_shoreline16jan2023.sql
-- sudo -u postgres psql -U postgres -d portic_v10 -f /home/plumegeo/portic/portic_V8/portic_V8_shoreline20miles16jan2023.sql
psql -U postgres -d portic_v10 -f D:\Data\BDD-backups\portic\2023011_data_18janvier2023\portic_V8_shoreline20miles16jan2023.sql

-- en V10  : calculer id_shoreline - Pour les ports à terre ou proches de la cote
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='1mile'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 53 + 15

-- en V10  : calculer id_shoreline - Pour les ports en mer ou loin de la cote
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.ogc_fid, distance_shoreline=k1.dmin, type_cote='20mile'
from (
	select p.uhgs_id, min(st_distance(c.line_20_mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline_valid_20mile  c
			  where p.port_on_shoreline is null and point3857 is not null  and st_contains(c.wkb_geometry, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.ogc_fid, st_distance(c.line_20_mile , point3857) as d, ST_ClosestPoint(c.line_20_mile, point3857) as port_on_shoreline
			  from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where p.port_on_shoreline is null and point3857 is not null  and st_contains(c.wkb_geometry, point3857) 
    ) as k2
where ports.port_on_shoreline is null   and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 5 + 1



-- en V10  : calculer id_shoreline - Pour les ports très loin de la cote (34 dont / 3 à 50 km, 14 à 100 km

update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='50km'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile, 50000), point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '50km' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile,50000), point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 0 en 1 min

update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='100km'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile, 100000), point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '100km' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile,100000), point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 14 en 3 s

----------------------------------------
-- IMPORTANT : port_on_line_1mile
----------------------------------------

select toponyme , port_on_shoreline, id_shoreline, type_cote, port_on_line_1mile, distance_shoreline
from ports.port_points p
where insertdate = 'juillet 2023';

update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where  point3857 is not null and st_contains(c.buffer_1mile , point3857)     
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where point3857 is not null and st_contains(buffer_1mile, point3857) 
    ) as k2
where  ports.port_on_line_1mile is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 54 en 22 s + 15 en 22s

update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile, id_shoreline = k2.id, distance_shoreline=k1.dmin
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline  c
			  where  point3857 is not null    
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(c.line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where  point3857 is not null 
    ) as k2
where ports.port_on_line_1mile is null and  ports.insertdate = 'juillet 2023' and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 5 pour compléter + 75

select oblique, has_a_clerk , * from ports.port_points pp where oblique is null ;
select oblique, has_a_clerk , status , * from ports.port_points pp where status ='oblique'  and oblique is null;

comment on column ports.port_points.oblique is 'true if the port has had a clerk of admiralty to record taxes - same field as ''has_a_clerk''';
comment on column ports.port_points.has_a_clerk is 'true if the port has had a clerk of admiralty to record taxes - same field as ''oblique''';
select distinct status from ports.port_points pp where oblique is not null ;
comment on column ports.port_points.status is '''siège amirauté'' for head of admiralty quarter, or just ''oblique'' if the port has had a clerk of admiralty to record taxes - null else ';

alter table ports.port_points drop column ogc_fid  ;
alter table ports.port_points add column ogc_fid serial;
alter table ports.port_points add primary key (ogc_fid);

-----------------------------------------------------------------------------------------------
-- les geoinclusions
-----------------------------------------------------------------------------------------------

select distinct ughs_id_sup , toponyme_sup , criteria from ports.generiques_geoinclusions gg order by criteria
-- A0151330	île de Ré	carte  : A1970649	Lilleau des Niges
-- A0223338	île d'Elbe	carte : A1969513 
-- A1968858	rivière de Bordeaux	carte : A0137984, A0165215
-- A1963922	Catalogne	Flag du catalan - Appartient à la région Catalonia (level 2) de world_1789 - code ES51% dans la NUTS  : A0121217
-- A1964878	Espagne atlantique	etat='Espagne' and subunit is null  + carte : A1964909 / A1964878
-- A1964713	Mer Mediterranée	: shiparea
-- A1963986 Mer Baltique	carte ou ship area  (en gardant les noms génériques comme Suède). On peut donc avoir une appartenance multiple d'un port (mer baltique et Suède par exemple)
-- A1965005	Côtes de Barbarie	country2019_name in ( 'Egypt') or etat = 'Empire du Maroc' or subunit = 'Régence de Tunis,  'Régence d'Alger', 'Régence de Tripoli')
-- H6666666	à l'aventure	etat = France and subunit is null
-- A1081238	Suède	etat = Suède : A0952914 et A0948094 (Finlande)
-- A1964975	Italie	"etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)"
-- A0219759	Toscane	etat = Toscane : A1970732
-- A0074743	Espagne, undeterminé	etat='Espagne' and subunit is null
-- A1965062	Côte d'Espagne	etat='Espagne' and subunit is null
-- A0223759	Sardaigne	etat='Royaume de Piémont-Sardaigne'
-- A0146288	Rivière de Gênes	etat=République de Gênes : A0236030
-- A1964382	Royaume de Naples	etat=Royaume de Naples : A1970733 et A1970735
-- A0178037	Seudre	Observed in navigo data for Z markers : Arvert ?
-- A1964988	Côtes de Bretagne	province='Bretagne'
-- A0156739	Corse	province='Isles de Corse'
-- A1965209	Languedoc	province='Languedoc'
-- A1968879	Le long des côtes de Normandie	province='Normandie'
-- A0212843	Côtes de Provence	province='Provence'
-- A1965060	Cotes du Rousillon	province='Roussillon'
--A1968879	Le long des côtes de Normandie	province=Normandie
--A0617755	hollandais	state_1789_fr = Provinces-Unies
--A0388193	Ecosse	subunit = 'Ecosse'
--A0390929	Angleterre	subunit = Angleterre
--A0219724	Sicile, underterminé	subunit = Sicile
--A0220588	Sicile	subunit = Sicile
--B0000015	Nouvelle-Angleterre	subunit in ('Maine', 'Massachusetts', 'New Hampshire','Rhode Island' )
--B0000978	colonies anglaises [en Amérique]	subunit='colonies britanniques d'Amérique'
--A0628870	Frise	subunit='Frise'
--A0617755	Hollande	subunit='Hollande'
--A0605799	Irlande	subunit='Irlande'
--A0635679	Zélande	subunit='Zélande'
--A0079365	Canaries	subunit=Canaries

-- alignement  flags - homeports (états correspondant)

select * from ports.port_points pp where pp.uhgs_id = 'A1964909'

set search_path = ports, public, navigo, navigoviz, navigochecked;

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Corse', 'A0156739', uhgs_id , toponyme_standard_fr, 'province=''Corse''' 
from port_points pp where province = 'Corse' and uhgs_id != 'A0156739' and insertdate='juillet 2023';
-- 2

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Angleterre', 'A0390929', uhgs_id , toponyme_standard_fr, 'subunit = Angleterre' 
from port_points pp where substate_1789_fr = 'Angleterre' and uhgs_id != 'A0390929' and insertdate='juillet 2023';
-- 2


insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Sicile', 'A0220588', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' 
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0220588' and insertdate='juillet 2023';
--2
	
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Sicile, underterminé', 'A0219724', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' 
from port_points pp where substate_1789_fr = 'Sicile' and insertdate='juillet 2023';
-- 2


insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Russie', 'A1964109', uhgs_id , toponyme_standard_fr, 'etat = Russie' 
from port_points pp where state_1789_fr = 'Russie' and uhgs_id != 'A1964109' and insertdate='juillet 2023';
-- 0


insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Suède', 'A1081238', uhgs_id , toponyme_standard_fr, 'etat = Suède' 
from port_points pp where state_1789_fr = 'Suède' and uhgs_id != 'A1081238' and insertdate='juillet 2023';
-- 28 ?

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Toscane', 'A0219759', uhgs_id , toponyme_standard_fr, 'etat = Toscane' 
from port_points pp where state_1789_fr = 'Toscane' and uhgs_id != 'A0219759' and insertdate='juillet 2023';
-- 3


insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Etats pontificaux', 'A1968753', uhgs_id , toponyme_standard_fr, 'etat = Etats pontificaux' 
from port_points pp where state_1789_fr = 'Etats pontificaux' and uhgs_id != 'A1968753' and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Nouvelle-Angleterre', 'B0000015', uhgs_id , toponyme_standard_fr, 'subunit_en in (Maine, Massachusetts, New Hampshire,Rhode Island)' 
from port_points pp where substate_1789_en in ('Maine', 'Massachusetts','New Hampshire','Rhode Island') and uhgs_id != 'B0000015' and insertdate='juillet 2023';
-- 1

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'à l''aventure', 'H6666666', uhgs_id , toponyme_standard_fr, 'etat = France and subunit is null' 
from port_points pp where state_1789_fr = 'France' and substate_1789_fr is null and uhgs_id != 'H6666666' and insertdate='juillet 2023';
-- 347

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Portugal', 'A1965063', uhgs_id , toponyme_standard_fr, 'etat = Portugal and subunit is null' 
from port_points pp where state_1789_fr = 'Portugal' and substate_1789_fr is null and uhgs_id != 'A1965063' and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Ecosse', 'A0388193', uhgs_id , toponyme_standard_fr, 'subunit = Ecosse' 
from port_points pp where substate_1789_fr = 'Ecosse'  and uhgs_id != 'A0388193' and insertdate='juillet 2023';
-- 1

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côte d''Espagne', 'A1965062', uhgs_id , toponyme_standard_fr, 'etat=Espagne and subunit is null' 
from port_points pp where state_1789_fr = 'Espagne' and substate_1789_fr is null and uhgs_id != 'A1965062' and insertdate='juillet 2023';
-- 3

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Espagne, undeterminé', 'A0074743', uhgs_id , toponyme_standard_fr, 'etat=Espagne and subunit is null'
from port_points pp where state_1789_fr = 'Espagne' and substate_1789_fr is null and uhgs_id != 'A0074743' and insertdate='juillet 2023';
-- 3

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Canaries', 'A0079365', uhgs_id , toponyme_standard_fr, 'subunit=Canaries' 
from port_points pp where substate_1789_fr = 'Canaries'  and uhgs_id != 'A0079365' and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côtes de Bretagne', 'A1964988', uhgs_id , toponyme_standard_fr, 'province=Bretagne' 
from port_points pp where province = 'Bretagne'  and uhgs_id != 'A1964988' and insertdate='juillet 2023';
-- 6


insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Languedoc', 'A1965209', uhgs_id , toponyme_standard_fr, 'province=Languedoc' 
from port_points pp where province = 'Languedoc'  and uhgs_id != 'A1965209' and insertdate='juillet 2023';
-- 1

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Le long des côtes de Normandie', 'A1968879', uhgs_id , toponyme_standard_fr, 'province=Normandie' 
from port_points pp where province = 'Normandie'  and uhgs_id != 'A1968879' and insertdate='juillet 2023';
-- 1

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côtes de Provence', 'A0212843', uhgs_id , toponyme_standard_fr, 'province=Provence' 
from port_points pp where province = 'Provence'  and uhgs_id != 'A0212843' and insertdate='juillet 2023';
-- 9

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Cotes du Rousillon', 'A1965060', uhgs_id , toponyme_standard_fr, 'province=Roussillon' 
from port_points pp where province = 'Roussillon'  and uhgs_id != 'A1965060' and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'colonies anglaises [en Amérique]', 'B0000978', uhgs_id , toponyme_standard_fr, 'subunit=colonies britanniques d''Amérique' 
from port_points pp where substate_1789_fr = 'colonies britanniques d''Amérique'  and uhgs_id != 'B0000978' and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Irlande', 'A0605799', uhgs_id , toponyme_standard_fr, 'subunit=Irlande'
from port_points pp where substate_1789_fr = 'Irlande'  and uhgs_id != 'A0605799' and insertdate='juillet 2023';
-- 2

--Islande	A0146289	A0146289	Islande	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'A0146289' and ughs_id_sup = 'A0146289';

--Côte d'Afrique [de l'Ouest]	C0000006	C0000006	Côte d'Afrique [de l'Ouest]	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'C0000006' and ughs_id_sup = 'C0000006';

--Côte d'Or	C0000012	C0000012	Côte d'Or	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'C0000012' and ughs_id_sup = 'C0000012';

--Côte d'Angole	C0000009	C0000009	Côte d'Angole	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'C0000009' and ughs_id_sup = 'C0000009';

--Mozambique	C0000013	C0000013	Mozambique	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'C0000013' and ughs_id_sup = 'C0000013';

-- --Cap-Vert	C0000019	C0000019	Cap-Vert	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'C0000019' and ughs_id_sup = 'C0000019';

--Côte du Brésil	B0000934	B0000934	Côte du Brésil	aucun port pour l'instant à associer	0
update ports.generiques_geoinclusions  set done = 1 where ughs_id = 'B0000934' and ughs_id_sup = 'B0000934';

select toponyme , uhgs_id  , insertdate, province, state_1789_fr,  substate_1789_fr  from ports.port_points pp2 
where pp2.state_1789_fr = 'Provinces-Unies';
--Hollande	A0617755	A0617755	Hollande	subunit='Hollande'	0
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Hollande', 'A0617755', uhgs_id , toponyme_standard_fr, 'subunit=Hollande' 
from port_points pp where substate_1789_fr = 'Hollande'  and uhgs_id != 'A0617755' and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Zélande', 'A0635679', uhgs_id , toponyme_standard_fr, 'subunit=Zélande' 
from port_points pp where substate_1789_fr = 'Zélande'  and uhgs_id != 'A0635679' and insertdate='juillet 2023';
-- 1

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Frise', 'A0628870', uhgs_id , toponyme_standard_fr, 'subunit=Frise' 
from port_points pp where substate_1789_fr = 'Frise'  and uhgs_id != 'A0628870' and insertdate='juillet 2023';
-- 2

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'hollandais', 'A0617755', uhgs_id , toponyme_standard_fr, 'state_1789_fr = Provinces-Unies' 
from port_points pp where state_1789_fr = 'Provinces-Unies'  and uhgs_id != 'A0617755' and insertdate='juillet 2023';
-- 3

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Royaume de Naples', 'A1964382', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Naples' 
from port_points pp where state_1789_fr = 'Royaume de Naples'  and uhgs_id != 'A1964382' and insertdate='juillet 2023';
-- 3

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Sardaigne', 'A0223759', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Piémont-Sardaigne' 
from port_points pp where state_1789_fr = 'Royaume de Piémont-Sardaigne'  and uhgs_id != 'A0223759' and insertdate='juillet 2023';
-- 1

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Mer Baltique', 'A1963986', uhgs_id , toponyme_standard_fr, 'shiparea like BAL-' 
from port_points pp where shiparea like 'BAL-%' and uhgs_id != 'A1963986' and insertdate='juillet 2023';
--8

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Mer Mediterranée', 'A1964713', uhgs_id , toponyme_standard_fr, 'shiparea like MED-' 
from port_points pp where shiparea like 'MED-%' and uhgs_id != 'A1964713' and insertdate='juillet 2023';
-- 37

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'rivière de Bordeaux', 'A1968858', uhgs_id , toponyme_standard_fr, 'carte' 
from port_points pp where  uhgs_id != 'A1968858' and insertdate='juillet 2023' and uhgs_id in ('A0137984', 'A0165215');
-- 2


--Italie	A1964975	A1964975	Italie	etat in ('Sardaigne', 'Gènes', 'Parme', 'Modène', 'Toscane', 'Lucques', 'Piombino', 'Naples', 'Etats Pontificaux', 'Venise')	0
--Duché de Massa et Carrare	
--Royaume de Naples
--Royaume de Piémont-Sardaigne
--République de Gênes
--République de Lucques
--République de Venise
--principauté de Lampédouse
--Principauté de Piombino
--Etats pontificaux

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Italie', 'A1964975', uhgs_id , toponyme_standard_fr, 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)' 
from port_points pp where state_1789_fr in ('Duché de Massa et Carrare', 'Royaume de Naples', 'Royaume de Piémont-Sardaigne', 'République de Gênes', 'République de Lucques', 'République de Venise', 'principauté de Lampédouse', 'Principauté de Piombino', 'Etats pontificaux')  
and uhgs_id != 'A1964975' and insertdate='juillet 2023';
-- 5

--Côtes de Barbarie	
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' 
from port_points pp where country2019_name = 'Egypt'  and uhgs_id != 'A1965005' and insertdate='juillet 2023';
-- 0
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' 
from port_points pp where state_1789_fr = 'Empire du Maroc'  and uhgs_id != 'A1965005' and insertdate='juillet 2023';
-- 0
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' 
from port_points pp where substate_1789_fr in ('Régence de Tunis',  'Régence d''Alger', 'Régence de Tripoli')  and uhgs_id != 'A1965005' and insertdate='juillet 2023';
--2

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' 
from port_points pp where substate_1789_fr in ('Tripoli')  and uhgs_id != 'A1965005' and insertdate='juillet 2023';
-- 0



insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Colonies françaises en Amérique', 'B0000970', uhgs_id , toponyme_standard_fr, 'subunit=colonies françaises en Amérique' 
from port_points pp where substate_1789_fr = 'colonies françaises d''Amérique'  and insertdate='juillet 2023';
-- 0

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria)
select 'Rivière de Gênes', 'A0146288', uhgs_id , toponyme_standard_fr, 'etat=République de Gênes' 
from port_points pp where state_1789_fr = 'République de Gênes' and insertdate='juillet 2023';
-- 1 

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Finlande, undeterminé', 'A0921408', uhgs_id , toponyme_standard_fr, 'subunit=Finlande' 
from port_points pp where  uhgs_id != 'A0921408' and substate_1789_fr = 'Finlande' and insertdate='juillet 2023';
-- 2

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Finlande, undeterminé', 'A0921408', uhgs_id , toponyme_standard_fr, 'subunit=Finlande' 
from port_points pp where  uhgs_id != 'A0921408' and substate_1789_fr = 'Finlande' and insertdate='juillet 2023';


-- A0151330	île de Ré	carte  : A1970649	Lilleau des Niges
-- A0223338	île d'Elbe	carte : A1969513 
-- A1968858	rivière de Bordeaux	carte : A0137984, A0165215
-- A1963922	Catalogne	Flag du catalan - Appartient à la région Catalonia (level 2) de world_1789 - code ES51% dans la NUTS  : A0121217
-- A1964878	Espagne atlantique	etat='Espagne' and subunit is null  + carte : A1964909 / A1964878

insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'île de Ré', 'A0151330', uhgs_id , toponyme_standard_fr, 'carte' 
from port_points pp where  uhgs_id != 'A0151330' and uhgs_id = 'A1970649' and insertdate='juillet 2023';
-- 1
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'île d''Elbe', 'A0223338', uhgs_id , toponyme_standard_fr, 'carte' 
from port_points pp where  uhgs_id != 'A0223338' and uhgs_id = 'A1969513' and insertdate='juillet 2023';
-- 1
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Catalogne', 'A1963922', uhgs_id , toponyme_standard_fr, 'carte' 
from port_points pp where  uhgs_id != 'A1963922' and uhgs_id = 'A0121217' and insertdate='juillet 2023';
-- 1
insert into ports.generiques_geoinclusions (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria )
select 'Espagne atlantique', 'A1964878', uhgs_id , toponyme_standard_fr, 'carte' 
from port_points pp where  uhgs_id != 'A1964878' and uhgs_id = 'A1964909' and insertdate='juillet 2023';
-- 1

select * from ports.generiques_geoinclusions where toponyme_sup =  'Seudre';

select * from port_points pp where toponyme_standard_fr = 'Arvert'; --doublon
delete from port_points pp where ogc_fid = '1741';

-- vérification des doublons possibles
select count(ogc_fid ) as c, uhgs_id  from port_points group by uhgs_id having count(ogc_fid ) > 1;
-- A0383127
-- A1965240

select * from port_points pp where uhgs_id  = 'A0383127'; --doublon 1752 Limekilns en Ecosse
delete from port_points where ogc_fid = 1752;
select * from port_points pp where uhgs_id  = 'A1965240'; --doublon 36  Arzew (sur)	Arzew (off) 1733
update port_points set toponyme_standard_fr = 'Arzew (sur)', toponyme_standard_en=  'Arzew (off)' where ogc_fid = 36;
delete from port_points where ogc_fid = 1733;

-- Mail de Silvia le 24 juillet
-- Planosa  (UHGS = A0222488 et pas A0226455 que tu indiques plus bas et que je ne retrouve pas ni comme pointcall, ni colle homeport, ni comme capt_birthplace) 
-- comme Pianosa (FR) et  Pianosa (GB), lat 42.583333 long 10.066667, Etats_like = A0251183 ; c'est plutôt celui-ci et pas le point que tu proposes en Grèce, compte tenu que le navire capturé est le lendemain sur le cap Corse
-- A0222488 est le code de Pianosa
select * from navigocheck.check_pointcall cp where pointcall_uhgs_id = 'A0226455';-- record id 00325290 -- doc_id = 00325282
select * from port_points pp where pp.uhgs_id = 'A0222488';
update port_points set 


-- pointcall = A0632438 a été remplacée par A0635133
select * from port_points pp where pp.uhgs_id = 'A0632438'; -- Kolijnsplaat
-- pointcall A0829022 Danemark a été remplacé par A0824104
select * from port_points pp where pp.uhgs_id = 'A0824104'; -- Danemark
-- pointcall A0932858 avait déjà entretemps été remplacé par A0952914
select * from port_points pp where pp.uhgs_id = 'A0952914'; -- Carleby
-- pointcall A1964107 avait déjà entretemps été remplacé par A0219489
select * from port_points pp where pp.uhgs_id = 'A0219489'; -- Vénitien

select * from port_points pp where pp.uhgs_id = 'A0921408'; -- Finlande, undeterminé
select * from port_points pp where pp.uhgs_id = 'A0948094'; -- Finlande
select pointcall_uhgs_id , pointcall_name, record_id, ship_homeport_uhgs_id, captain_birthplace_id, captain_citizenship_id  from navigocheck.check_pointcall cp 
where pointcall_uhgs_id='A0948094' or captain_birthplace_id = 'A0948094' or captain_citizenship_id='A0948094' or ship_homeport_uhgs_id = 'A0948094';


-----------------------------------------------------------------------------------------------
-- Les tables de navigo avec LoadFilemaker /ligne 1444 de v8b
-----------------------------------------------------------------------------------------------

-- SOURCES

select * from navigocheck.check_pointcall cp  where link_to_source_01 is null and source='ANF, G5-140';

select count(*) from navigocheck.check_pointcall cp;
-- 139283
select count(distinct link_to_source_01) from navigocheck.check_pointcall cp;
-- JOINTURE : pointcall.link_to_source_01 = sources.Link_to_source
-- 89
select count(distinct Link_to_source) from navigoviz."source" s ;
-- 89
-- Le compte est bon

select count(*) from navigoviz."source" s ;
-- 67340
select count(distinct "source") from navigocheck.check_pointcall cp;
-- 42366

select count(*)
from navigocheck.check_pointcall cp, navigoviz."source" s 
where s.data_block_local_id = cp.data_block_local_id and s.source = regexp_replace(cp."source", E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )
-- le compte est bon. 
-- JOINTURE : sources.data_block_local_id = pointcall.data_block_local_id and sources.source = regexp_replace(pointcall."source", E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )


-- CARGO

select * from navigoviz.cargo c where c.cargo_item_sending_place_uhgs_id != c.first_point__pointcall_uhgs_id 
and c.link_to_pointcall in (select distinct record_id from navigocheck.check_pointcall cp);
-- 3190 cargo qui ne sont pas chargés au point initial mais ailleurs
select * from navigoviz.cargo c where c.cargo_item_sending_place_uhgs_id = c.first_point__pointcall_uhgs_id 
and c.link_to_pointcall in (select distinct record_id from navigocheck.check_pointcall cp);
-- 77508 cargo chargés au point initial

-- 00324124	(Sette) 00324122 (celui de Marseille)
select record_id , pointcall_name , data_block_local_id  
from navigocheck.check_pointcall cp where  data_block_local_id = '00324122' order by cp.pointcall_rank ;
-- record_id = '00176594'

select commodity_purpose  from navigo.cargo c where c.pointcall__data_block_local_id = '00324122'
-- 3190
select commodity_purpose, link_to_pointcall , pointcall__data_block_local_id , pointcall__record_id, cargo_item_action , cargo_item_sending_place , first_point__pointcall_name 
from navigo.cargo c where c.link_to_pointcall  = '00324122';
-- Dans navigo, pas de lien pour Sette avec le bon pointcall (le link_to_pointcall est celui de Marseille) 
select commodity_purpose, link_to_pointcall , data_lineage  , cargo_item_action , cargo_item_sending_place , pointcall__data_block_local_id 
from navigoviz.cargo c where c.link_to_pointcall  = '00324122';

select coalesce(c.cargo_item_sending_place, c.first_point__pointcall_name ) , commodity_purpose, link_to_pointcall , data_lineage  , cargo_item_action , cargo_item_sending_place , pointcall__data_block_local_id 
from navigoviz.cargo c where c.pointcall__data_block_local_id  = '00324122';
-- bon exemple : données enrichies par portic

-- POINTCALLS

select to_date(pointcall_indate_date, 'DD/MM/YYYY') as outdate_fixed from navigocheck.check_pointcall cp ;
select to_date(pointcall_outdate_date, 'DD/MM/YYYY') as outdate_fixed from navigocheck.check_pointcall cp ;
--where pointcall_outdate_date like '%?%';

select pointcall_rank, pointcall_outdate_date, pointcall_date  from navigocheck.check_pointcall cp  where pointcall_outdate_date like '%?%'; --17
select source, pointcall_rank , pointcall_outdate_date, pointcall_date, pointcall_outdate, pointcall_indate  from navigocheck.check_pointcall cp  
where pointcall_outdate_date not like '%?%';
-- OK, tous de rangs 1 : on peut remplacer par le premier jour du mois (pointcall_outdate)
--where pointcall_date like '%00%';--40

select p.source_doc_id from navigoviz.pointcall p 
where source_subset = 'Poitou' and ( extract(year from outdate_fixed) = 1789 or extract(year from indate_fixed) = 1789);

select to_date('25/02/1799', 'DD/MM/YYYY'), to_date('1799=02=25', 'YYYY/MM/DD') as outdate_fixed from navigocheck.check_pointcall cp ;

select replace(pointcall_outdate, '00', '01'), pointcall_outdate_date, 
case when pointcall_outdate_date!='?' then to_date(pointcall_outdate_date, 'DD/MM/YYYY') else 
to_date(replace(pointcall_outdate, '00', '01'), 'YYYY/MM/DD') end as outdate_fixed 
from navigocheck.check_pointcall cp  
where pointcall_outdate_date like '%?%';

-- IMPORTER ship_class_standardized
psql -U postgres -d portic_v10 -f D:\Data\BDD-backups\portic\portic_v9\navigoviz_[ship_class_standardized].sql
psql -U postgres -d portic_v10 -f D:\Data\BDD-backups\portic\portic_v9\navigoviz_[decompte_sources_saisies].sql
psql -U postgres -d portic_v10 -f D:\Data\BDD-backups\portic\portic_v9\navigoviz_[routepaths].sql
psql -U postgres -d portic_v10 -f D:\Data\BDD-backups\portic\portic_v9\navigoviz_[shoreline].sql

--------------
-- les routes
--------------

select * from navigoviz.built_travels bt where bt.captain_id  is null and ship_id is not null; --158

select bt.departure_fr , bt.destination_fr ,  bt.source_suite, extract(year from coalesce(bt.outdate_fixed, bt.indate_fixed))  , bt.*
from navigoviz.built_travels bt 
where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
and source_entry != 'both-to'
order by extract(year from coalesce(bt.outdate_fixed, bt.indate_fixed)) ;
-- 1732


--- 1901
update navigoviz.uncertainity_travels set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, %d, %f) 
                        where pointpath is null
 
select from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr from navigoviz.routepaths where pointpath is null


select distinct bt.departure_fr , bt.destination_fr 
-- bt.source_suite, extract(year from coalesce(bt.outdate_fixed, bt.indate_fixed))  , bt.*
from navigoviz.built_travels bt 
where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
and source_entry != 'both-to';
-- 1290


select distinct bt.departure_fr , bt.destination_fr 
from navigoviz.built_travels bt 
where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
and source_entry != 'both-to'
except(
select distinct  bt.destination_fr, bt.departure_fr 
from navigoviz.built_travels bt 
where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
and source_entry != 'both-to'
)
-- 1146 où on n'a pas d'aller-departure_ferme_direction_uncertainty 

select distinct bt.departure_fr , bt.destination_fr 
from navigoviz.built_travels bt 
where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
and source_entry != 'both-to'
except (
	select distinct bt.departure_fr , bt.destination_fr 
	from navigoviz.built_travels bt 
	where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
	and source_entry != 'both-to'
	except(
	select distinct  bt.destination_fr, bt.departure_fr 
	from navigoviz.built_travels bt 
	where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
	and source_entry != 'both-to'
	)
)-- 144 allers-retours

select 144 + 1146 -- 1290 à calculer

select from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr from navigoviz.routepaths where pointpath is null

insert into navigoviz.routepaths (from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr)
select distinct bt.departure_uhgs_id, bt.destination_uhgs_id, bt.departure_fr , bt.destination_fr 
from navigoviz.built_travels bt 
where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null and source_entry != 'both-to'
-- 1291

-- appeler le programme Python : updateRoutePaths

-- # De base, aller chercher dans l'ensemble des routes déjà calculées 
update navigoviz.built_travels  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id and to_uhgs_id=destination_uhgs_id)
            and r.pointpath is not null;

-- Insérer les geometries des retours avec les points dans l'ordre inverse 
/*        query = """update navigoviz.built_travels t set pointpath = r.reverse_pointpath ,  geom = st_makeline(r.reverse_pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = destination_uhgs_id  and to_uhgs_id= departure_uhgs_id)
            and r.reverse_pointpath is not null
*/
            
-- #La géometrie en 4326 (coordonnées lat/long) pour les viz (shiproutes.portic.fr)
update navigoviz.built_travels set geom4326 = st_setsrid(st_transform(geom, 4326), 4326) where geom4326 is null ;
-- # La distance calculée suivant la route maritime de l'algo en km
update navigoviz.built_travels set distance_dep_dest_km = round((st_length(geom)/1000.0)::numeric,3) where geom is not null and distance_dep_dest_km is null;

/*
delete from navigoviz.routepaths where id in 
(
select r1.id
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id  and ST_IsSimple(st_makeline(r2.pointpath)) != ST_IsSimple(st_makeline(r1.pointpath)) and r1.id < r2.id
);
*/

select count(*) from navigoviz.routepaths;
-- 6378
select count(*) from navigoviz.routepaths where ST_IsSimple(st_makeline(pointpath)) is false;
--- 15
-- supprimer les doublons en général
delete from navigoviz.routepaths where id in (
select r1.id
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id   and r1.id < r2.id and ST_IsSimple(st_makeline(r2.pointpath)) );

select id, round(elapsetime/1000.0) as elapsetimes, * from  navigoviz.routepaths
where ST_IsSimple(st_makeline(pointpath)) is false
order by elapsetime;
-- 14

--- les paths qui coupent la terre
select max(id) from navigoviz.routepaths r;
-- 6799 sur la V9
-- 8090 sur la v10

select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
 and r.id > 6799
group by r.id
order by longueur desc;

7888	3361058.1101829787
7278	912474.7682635277
8090	598941.7996976912
7890	433383.01688962226
7453	270989.43328582007
7786	216117.52988996866
7855	201362.97981143417
7080	196923.43386490303
7032	112999.28709666002
8089	70864.75406440736
7033	67102.7686187211
7895	57305.952445759605

select * from navigoviz.routepaths r where id = 7278

update navigoviz.routepaths r set m_offset = (c).tampon,
p_distance_ratio = (c).distance,
pointpath = (c).pointslist,
nbPoints = (c).nbPoints,
elapseTime = (c).duration 
from 
(	
	
	select  r2.id, r2.from_uhgs_id , r2.to_uhgs_id, ports.portic_points_on_path(r2.from_uhgs_id, r2.to_uhgs_id, 5000, 10000) as c 
	from  navigoviz.routepaths r2
	where  id in (7278) 

) as k
where r.to_uhgs_id = k.to_uhgs_id and r.from_uhgs_id =k.from_uhgs_id;


select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
 and r.id = 7278
group by r.id
order by longueur desc;
-- refaire le calcul ne change rien à l'affaire


update navigoviz.routepaths r set longueur_intersection = k.longueur, taux_intersection = k.longueur / st_length(st_makeline(r.pointpath)) * 100
from 
(
select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
and r.id > 6799 and longueur_intersection is null
group by r.id
) as k 
where k.id = r.id;
-- 285 / 1291, soit 22%

select min(taux_intersection), avg(taux_intersection), stddev(taux_intersection), max(taux_intersection) 
from navigoviz.routepaths
where longueur_intersection is not null and id > 6799;

select count(*) from navigoviz.routepaths where taux_intersection < 1 and id > 6799;
-- 251 (moins d'un mètre, on s'en fout)
select count(*), min(longueur_intersection) from navigoviz.routepaths where taux_intersection > 3 and id > 6799;
-- 22	1278.9016029868294 

select * from ports.port_points pp, navigoviz.shoreline s
where pp.uhgs_id = 'A0932725' and st_contains(s.buffer_1mile, pp.port_on_line_1mile ); -- Abo


-- liste des ports qui potentiellement ne sont pas bien placés (ils tombent sur une ile en face d'eux) 
select from_uhgs_id , nbpoints  from navigoviz.routepaths where taux_intersection > 3 and id > 6799 and nbpoints =2
union
(select to_uhgs_id  , nbpoints  from navigoviz.routepaths where taux_intersection > 3 and id > 6799 and nbpoints =2);

select array_agg(''''||uhgs_id||'''')  from ports.port_points pp, navigoviz.shoreline s
where pp.uhgs_id in ( 
	select from_uhgs_id   from navigoviz.routepaths where taux_intersection > 3 and id > 6799 and nbpoints =2
	union
	(select to_uhgs_id    from navigoviz.routepaths where taux_intersection > 3 and id > 6799 and nbpoints =2)
)
and st_contains(s.buffer_1mile, pp.port_on_line_1mile ); -- Abo

select distinct data_lineage from navigoviz.cargo c 
select  * from navigoviz.routepaths where taux_intersection >= 1 and  id > 6799  order by longueur_intersection desc ;

select distinct p.citizenship , p.citizenship_uhgs_id  from navigoviz.pointcall p 

select distinct p.citizenship , p.citizenship_uhgs_id  from navigoviz.pointcall p 
where citizenship_uhgs_id not in (select pointcall_uhgs_id from ports.port_points); --117
/*
 * English	A0390929
espagnol	A0074743
Espagnol	A0074743
Français	A0167415
françois	A0167415
Genoese	A0219524
 */

select distinct p.citizenship , p.citizenship_uhgs_id  from navigoviz.pointcall p 
where citizenship_uhgs_id  in (select key_id from ports.labels_lang_csv llc); 
--0

select distinct p.birthplace  , p.birthplace_uhgs_id  from navigoviz.pointcall p 
where trim(birthplace_uhgs_id) not in (select pointcall_uhgs_id from ports.port_points);
-- cassis	A0145112
Cassis	A0145112
Brest	A0200718
Aber Wrach	A0150810
Agde	A0139070
Agde	A0218780

select uhgs_id  from ports.port_points where toponyme_standard_fr ='Brest'
A0200718
select uhgs_id  from ports.port_points where toponyme_standard_fr ='Cassis'
A0145112
select uhgs_id, toponyme_standard_fr  from ports.port_points where uhgs_id ='A0150810'
-- A0150810	Aber-Wrac'h
select uhgs_id, toponyme_standard_fr  from ports.port_points where uhgs_id ='A0139070'
-- A0139070	Arles
select uhgs_id, toponyme_standard_fr  from ports.port_points where uhgs_id ='A0218780'
-- A0218780	Agde

select count(*)  from navigoviz.pointcall p 
where trim(birthplace_uhgs_id) not in (select pointcall_uhgs_id from ports.port_points);
-- 163

select count(distinct birthplace_uhgs_id)  from navigoviz.pointcall p 
where trim(birthplace_uhgs_id)  in (select pointcall_uhgs_id from ports.port_points);
-- ?

select distinct label_type  from  ports.labels_lang_csv llc
where trim(birthplace_uhgs_id)  in (select pointcall_uhgs_id from ports.port_points);

select * from navigoviz.ship_class_standardized scs 
select distinct shipclass_standard from navigoviz.ship_class_standardized scs ;
select count(ship_class_standardized), ship_class_standardized
from navigoviz.pointcall p 
group by ship_class_standardized
order by count(ship_class_standardized) desc;

select count(tonnage_unit), tonnage_unit from navigoviz.pointcall
group by tonnage_unit
order by count(tonnage_unit) desc;

update navigoviz.pointcall set tonnage_unit = 'tx' where tonnage_unit = 'Tx' or tonnage_unit = 'tonneaux';
update navigoviz.pointcall set tonnage_unit = 'quintaux' where tonnage_unit = 'Quintaux' ;

select distinct tonnage_class  from navigoviz.pointcall ;
select distinct certitude  from navigoviz.pointcall ;

select * from ports.labels_lang_csv llc 
where label_type = 'flag'

select net_route_marker, pointcall_uncertainity, certitude, certitude_reason, fixed_net_route_marker  from navigoviz.pointcall p where p.net_route_marker != p.fixed_net_route_marker

select count(net_route_marker) , net_route_marker  from navigoviz.pointcall group by net_route_marker

select distinct net_route_marker , fixed_net_route_marker  from navigoviz.pointcall
-- A, Q, Z 
select distinct neat_route_marker  from navigocheck.check_pointcall
-- A, Q, Z 

select distinct pointcall_function  from navigocheck.check_pointcall
-- A, t, T, Z, O 
select count(pointcall_function) , pointcall_function  from navigoviz.pointcall group by pointcall_function
-- A, O, T, Z
update navigoviz.pointcall set pointcall_function = 'T' where pointcall_function = 't';
update navigoviz.built_travels b set departure_function  = 'T' where departure_function= 't';
update navigoviz.built_travels b set destination_function  = 'T' where destination_function= 't';
update navigoviz.raw_flows set departure_function = 'T' where departure_function= 't';
update navigoviz.raw_flows  set destination_function  = 'T' where destination_function= 't';




select count(pointcall_action) as c, pointcall_action, '- '||pointcall_action  from navigoviz.pointcall group by pointcall_action order by c desc
update navigoviz.pointcall set pointcall_action = 'In-out' where pointcall_action = 'In-Out';
update navigoviz.built_travels b set departure_action  = 'In-out' where departure_action= 'In-Out';
update navigoviz.built_travels b set destination_action  = 'In-out' where destination_action= 'In-Out';
update navigoviz.raw_flows set departure_action = 'In-out' where departure_action= 'In-Out';
update navigoviz.raw_flows  set destination_action  = 'In-out' where destination_action= 'In-Out';


select count(tonnage_class) , tonnage_class  from navigoviz.pointcall where pointcall_function = 'O' group by tonnage_class 


select count(ship_flag_id) as c , ship_flag_id , ship_flag_standardized_fr, ship_flag_standardized_en  
from navigoviz.pointcall
-- where pointcall_function = 'O' 
group by ship_flag_id , ship_flag_standardized_fr, ship_flag_standardized_en
order by c desc;

select flag, homeport_uhgs_id , homeport_toponyme_fr  from navigoviz.pointcall where ship_flag_id='A0241206'

insert into ports.labels_lang_csv (label_type, key_id, fr, en)
values ('flag', 'A0223759',	'Sarde', 	'Sardinian');

insert into ports.labels_lang_csv (label_type, key_id, fr, en)
values ('flag', 'A0241206',	'Piombino', 	'Piombino');

		


update ports.labels_lang_csv set key_id='A0353892', fr='monégasque', en='Monegasco'
where label_type = 'flag' and key_id='A0353888';

update ports.labels_lang_csv set key_id='A0237391', fr='carrarais', en='Carraran'
where label_type = 'flag' and key_id='A0251618';

update ports.labels_lang_csv set key_id='A0219727', fr='Impérial (méditerranéen)', en='Imperial (Mediterranean)'
where label_type = 'flag' and key_id='([Imperial])';

update navigoviz.pointcall set ship_flag_standardized_fr = fr, ship_flag_standardized_en = en
from ports.labels_lang_csv
where label_type = 'flag' and key_id = ship_flag_id and ship_flag_id in ('A0353892', 'A0237391', 'A0219727', 'A0223759', 'A0241206');
-- 928

update navigoviz.built_travels  set ship_flag_standardized_fr = fr, ship_flag_standardized_en = en
from ports.labels_lang_csv
where label_type = 'flag' and key_id = ship_flag_id and ship_flag_id in ('A0353892', 'A0237391', 'A0219727', 'A0223759', 'A0241206');
-- 657

update navigoviz.raw_flows  set ship_flag_standardized_fr = fr, ship_flag_standardized_en = en
from ports.labels_lang_csv
where label_type = 'flag' and key_id = ship_flag_id and ship_flag_id in ('A0353892', 'A0237391', 'A0219727', 'A0223759', 'A0241206');
-- 497

select distinct source_entry from navigoviz.built_travels bt 
select distinct source_suite  from navigoviz.pointcall bt 

select count(navigo_status) as c, navigo_status, '- '||navigo_status  from navigoviz.pointcall p  group by navigo_status order by c desc

select  p.record_id , p.pkid , p.source_doc_id, p.net_route_marker, p.fixed_net_route_marker  , source_component , p.source_suite , p.source_main_port_toponyme , extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.pointcall_status_uncertainity , p.pointcall , p.pointcall_function , p.pointcall_action 
from navigoviz.pointcall p where navigo_status = 'FC-TT'; -- FC-RF
--00168955	49104	00168954	A	Z	ANF, G5-140/Rouen	G5	Rouen	1787	2.0	33	0	Le Havre	T	In
-- 00168953	49102	00168952	A	A	ANF, G5-140/Rouen	G5	Rouen	1787	2.0	24		Vierville	T	in
select p.net_route_marker, p.fixed_net_route_marker  ,navigo_status,  extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.ship_id, p.pointcall_uncertainity  , p.pointcall , p.pointcall_function , p.pointcall_action
from navigoviz.pointcall p where ship_id in ('0007725N', '0003446N') order by ship_id, p.pointcall_rankfull ;

update navigoviz.built_travels set departure_navstatus = 'FC-RF' where departure_navstatus= 'FC-TT';
update navigoviz.built_travels  set destination_navstatus  = 'FC-RF' where destination_navstatus= 'FC-TT';
update navigoviz.raw_flows set departure_navstatus = 'FC-RF' where departure_navstatus= 'FC-TT';
update navigoviz.raw_flows  set destination_navstatus  = 'FC-RF' where destination_navstatus= 'FC-TT';
update navigoviz.pointcall  set navigo_status = 'FC-RF' where navigo_status= 'FC-TT';

select  p.net_route_marker, p.fixed_net_route_marker , source_doc_id, p.ship_id, p.captain_id , p.source_suite , p.source_main_port_toponyme , extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.pointcall_status_uncertainity , p.pointcall , p.pointcall_function , p.pointcall_action 
from navigoviz.pointcall p where navigo_status = 'FPC-RS';--FC-RS
update navigoviz.pointcall  set navigo_status = 'FC-RS' where navigo_status= 'FPC-RS';
update navigoviz.built_travels set departure_navstatus = 'FC-RS' where departure_navstatus= 'FPC-RS';
update navigoviz.built_travels  set destination_navstatus  = 'FC-RS' where destination_navstatus= 'FPC-RS';
update navigoviz.raw_flows set departure_navstatus = 'FC-RS' where departure_navstatus= 'FPC-RS';
update navigoviz.raw_flows  set destination_navstatus  = 'FC-RS' where destination_navstatus= 'FPC-RS';

select source_text , source_component , p.source_suite , p.source_main_port_toponyme , extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.pointcall_status_uncertainity , p.pointcall , p.pointcall_function , p.pointcall_action
from navigoviz.pointcall p where source_doc_id = '00320781';

select  p.net_route_marker, p.fixed_net_route_marker, ship_id , pointcall_uncertainity , source_doc_id, source_text , source_component , p.source_suite , p.source_main_port_toponyme , extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.pointcall_status_uncertainity , p.pointcall , p.pointcall_function , p.pointcall_action 
from navigoviz.pointcall p where navigo_status = 'FC-RT';--FC-RF

update navigoviz.pointcall  set navigo_status = 'FC-RF' where navigo_status= 'FC-RT';
update navigoviz.built_travels set departure_navstatus = 'FC-RF' where departure_navstatus= 'FC-RT';
update navigoviz.built_travels  set destination_navstatus  = 'FC-RF' where destination_navstatus= 'FC-RT';
update navigoviz.raw_flows set departure_navstatus = 'FC-RF' where departure_navstatus= 'FC-RT';
update navigoviz.raw_flows  set destination_navstatus  = 'FC-RF' where destination_navstatus= 'FC-RT';


select  source_doc_id, source_text , source_component , p.source_suite , p.source_main_port_toponyme , extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.pointcall_status_uncertainity , p.pointcall , p.pointcall_function , p.pointcall_action 
from navigoviz.pointcall p where navigo_status = 'PU-RF';--FC-RF

select p.net_route_marker, p.fixed_net_route_marker  ,navigo_status, source_text , source_component , p.source_suite , p.source_main_port_toponyme , extract(year from date_fixed), p.pointcall_rank_dedieu , p.pointcall_rankfull , p.pointcall_status_uncertainity , p.pointcall , p.pointcall_function , p.pointcall_action
from navigoviz.pointcall p where source_doc_id = '00174446' order by p.pointcall_rank_dedieu ;

select uhgs_id  from ports.port_points pp where geom is null;
select count(*)from ports.port_points;

--------------------------------------------------------------------------
-- le 9 avril 2024
-- BUG signalé ici https://github.com/medialab/portic-storymaps-2023/issues/30
-- VIZ : https://medialab.github.io/portic-storymaps-2023/#/fr/atlas/styles-navigation
-- Ponant (329 voyages vers Marseille consignés en temps de paix)
-- Méditerranée occidentale (6 986 voyages vers Marseille consignés en temps de paix)
-- Empire Ottoman : 1104

--  Or, pour les années de paix, j'ai 143 départs depuis Stockholm, 156 depuis Amsterdam, 137 depuis Rotterdam, 266 depuis Londres, et rien qu'avec ces ports, on dépasse allégrement le 329 voyages.
SELECT extract(year from indate_fixed) , count(*)
FROM navigoviz.raw_flows 
WHERE destination = 'Marseille'
group by extract(year from indate_fixed) ;
-- 23131

/* guerre
1779	2322
1759	1923
1799	2686
paix
1749	2288
1769	2318
1787	6145
1789	5449
*/

select * from navigoviz.raw_flows 
WHERE 
--destination = 'Marseille'
source_doc_id = '00308638'
and departure_state_1789_fr='Grande-Bretagne'
and extract(year from indate_fixed) not in (1759, 1779, 1799);


-- marseille destination_uhgs_id = A0210797
-- destination_function = 'O'
-- flag, flag_ship_id
-- source_suite 
-- source_doc_id, travel_rank

select distinct source_suite , departure_navstatus, departure_function, destination_navstatus, destination_function
from navigoviz.raw_flows 
WHERE destination_uhgs_id = 'A0210797';
/*
 * la Santé registre de patentes de Marseille
Registre du petit cabotage (1786-1787)
G5
*/
 */
-- departure_substates, departure_states
 
 
 SELECT extract(year from indate_fixed) , departure_state_1789_fr, departure_substate_1789_fr, ship_class_standardized, count(*)
FROM navigoviz.raw_flows 
WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1759, 1779, 1799)
--and departure = 'Stockholm'
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, ship_class_standardized 
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, ship_class_standardized ;

-- raw data to comptute the answer
-- 
select extract(year from indate_fixed) , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*)
from navigoviz.raw_flows
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1759, 1779, 1799)
) and travel_rank= 1
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized ;

-- estimated_tonnage_using_shipclass tonnage_estime_en_tx
-- import depuis un fichier CSV téléchargé en ligne dans C:\Travail\Projets\ANR_PORTIC\Data\tonnage\Estimation_tonnage_Niccolo.xlsx

-- departure_substate_1789_fr in (Autriche méditerranéenne, Russie - mer Noire)
--Sélection des ports français des groupes "Ponant" et "méditerrannée occidentale" à partir de departure_province selon les règles suivantes :

'Normandie': 'Ponant',
'Aunis': 'Ponant',
'Guyenne': 'Ponant',
'Flandre': 'Ponant',
'Picardie': 'Ponant',
'Bretagne': 'Ponant',

'Languedoc': 'méditerranée occidentale',
'Provence': 'méditerranée occidentale',
'Corse': 'méditerranée occidentale',
'Roussillon': 'méditerranée occidentale'

--Sélection des ports étrangers des groupes "méditerrannée occidentale" à partir de departure_state_1789_fr selon les règles suivantes :

'Suède': 'Ponant',
'Portugal': 'Ponant',
'Empire russe': 'Ponant',
'Danemark': 'Ponant',
'Autriche': 'Ponant',
'Provinces-Unies': 'Ponant',
'Prusse': 'Ponant',
'Grande-Bretagne': 'Ponant',
'Pologne': 'Ponant',
'Hambourg': 'Ponant',


'Brême': 'méditerranée occidentale',
'République de Lucques': 'méditerranée occidentale'
'Royaume de Piémont-Sardaigne': 'méditerranée occidentale',
'Espagne': 'méditerranée occidentale',
'Maroc': 'méditerranée occidentale',
'République de Venise': 'méditerranée occidentale',
'République de Gênes': 'méditerranée occidentale',
'République de Raguse': 'méditerranée occidentale',
'Toscane': 'méditerranée occidentale',
'République romaine': 'méditerranée occidentale',
'Duché de Massa et Carrare': 'méditerranée occidentale',
'Royaume de Naples': 'méditerranée occidentale',
'Malte': 'méditerranée occidentale',
'République ligurienne': 'méditerranée occidentale',
'Etats pontificaux': 'méditerranée occidentale',
'Monaco': 'méditerranée occidentale',
Autriche méditerranéenne
Russie - mer Noire


'Empire ottoman': 'empire ottoman',

----
-- la requête brute
select extract(year from indate_fixed) , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*) as nbNavires, sum(e.tonnage_estime_en_tx) as tonnnage_cumul
from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1759, 1779, 1799)
) and travel_rank= 1 
and e.ship_class = ship_class_standardized
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized ;

-- trouver le nombre d'escales

select count(travel_rank) as nbEscales , source_doc_id , annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, tonnage_estime_en_tx
from (
	select source_doc_id, travel_rank, extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, e.tonnage_estime_en_tx
	from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
	WHERE source_doc_id in (
		SELECT source_doc_id
		FROM navigoviz.raw_flows 
		WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1759, 1779, 1799)
	) 
	and e.ship_class = ship_class_standardized
	-- group by source_doc_id, travel_rank, extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
	order by source_doc_id, travel_rank, extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized 
) as total
group by source_doc_id,  tonnage_estime_en_tx, annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province

-- Ponant

/*
 * La visu affiche "329 voyages vers Marseille" pour le "Ponant. 
 * Or, pour les années de paix, j'ai 143 départs depuis Stockholm, 156 depuis Amsterdam, 137 depuis Rotterdam, 266 depuis Londres, 
 * et rien qu'avec ces ports, on dépasse allégrement le 329 voyages.
 */
select annee_dep, departure_state_1789_fr, sum(nbNavires), sum(tonnnage_cumul)
from (
select extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*) as nbNavires, sum(e.tonnage_estime_en_tx) as tonnnage_cumul
from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
	and source_suite = 'la Santé registre de patentes de Marseille'
)  and travel_rank= 1 
and e.ship_class = ship_class_standardized
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized 
) as k
where k.departure_state_1789_fr in ('Empire russe', 'Portugal', 'Suède', 'Danemark', 'Autriche', 'Pologne', 'Provinces-Unies', 'Prusse', 'Grande-Bretagne', 'Hambourg')
	and k.departure_substate_1789_fr not in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (k.departure_province in ('Normandie', 'Aunis', 'Guyenne', 'Bretagne', 'Flandre', 'Picardie'))
group by annee_dep, departure_state_1789_fr;

la Santé registre de patentes de Marseille
Registre du petit cabotage (1786-1787)
G5

-- vérif par rapport aux dires de Silvia
select departure_state_1789_fr, sum(nbNavires) as n, sum(tonnnage_cumul) as ton
from (
select extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*) as nbNavires, sum(e.tonnage_estime_en_tx) as tonnnage_cumul
from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
	and source_suite = 'la Santé registre de patentes de Marseille'
)  and travel_rank= 1 
and e.ship_class = ship_class_standardized
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized 
) as k
where k.departure_state_1789_fr in ('Empire russe', 'Portugal', 'Suède', 'Danemark', 'Autriche', 'Pologne', 'Provinces-Unies', 'Prusse', 'Grande-Bretagne', 'Hambourg')
	and k.departure_substate_1789_fr not in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (k.departure_province in ('Normandie', 'Aunis', 'Guyenne', 'Bretagne', 'Flandre', 'Picardie'))
group by departure_state_1789_fr; 

-- du PONANT
select  sum(nbNavires) as n, sum(tonnnage_cumul) as ton
from (
select extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*) as nbNavires, sum(e.tonnage_estime_en_tx) as tonnnage_cumul
from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
	and source_suite = 'la Santé registre de patentes de Marseille'
	)  and travel_rank= 1 
and e.ship_class = ship_class_standardized
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized 
) as k
where k.departure_state_1789_fr in ('Empire russe', 'Portugal', 'Suède', 'Danemark', 'Autriche', 'Pologne', 'Provinces-Unies', 'Prusse', 'Grande-Bretagne', 'Hambourg')
	and k.departure_substate_1789_fr not in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (k.departure_province in ('Normandie', 'Aunis', 'Guyenne', 'Bretagne', 'Flandre', 'Picardie'))
	--and ;
-- 993	227020
-- 926	214740
	
-- select 926+8100+1180 (10206)

-- de MEDITERRANEE OCCIDENTALE
select  sum(nbNavires) as n, sum(tonnnage_cumul) as ton
from (
select extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*) as nbNavires, sum(e.tonnage_estime_en_tx) as tonnnage_cumul
from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
	and source_suite = 'la Santé registre de patentes de Marseille'
	)  and travel_rank= 1 
and e.ship_class = ship_class_standardized
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized 
) as k
where departure_state_1789_fr in ('Brême', 'République de Lucques', 'Royaume de Piémont-Sardaigne', 'Espagne', 'Maroc', 
'République de Venise', 'République de Gênes', 'République de Raguse', 'Toscane', 'République romaine',
'Duché de Massa et Carrare', 'Royaume de Naples', 'Malte', 'République ligurienne', 'Etats pontificaux', 'Monaco')
	or departure_substate_1789_fr in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (departure_province in ('Languedoc', 'Provence', 'Corse', 'Roussillon'))
--group by departure_state_1789_fr; 

-- 13299	713155
-- 8100	520285
	
	
select  sum(nbNavires) as n, sum(tonnnage_cumul) as ton
from (
select extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, count(*) as nbNavires, sum(e.tonnage_estime_en_tx) as tonnnage_cumul
from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
WHERE source_doc_id in (
	SELECT source_doc_id
	FROM navigoviz.raw_flows 
	WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
	and source_suite = 'la Santé registre de patentes de Marseille'
	)  and travel_rank= 1 
and e.ship_class = ship_class_standardized
group by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized  
order by extract(year from indate_fixed), departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized 
) as k
where departure_state_1789_fr in ('Empire ottoman');
-- 1180	142765

-----------------------------------------	
-- calculs avec nb steps aussi
	-------------------------------------
-- du PONANT
select nbEscales, sum(n) as n, sum(ton) as ton
from (
	select  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales, count(distinct source_doc_id) as n, sum(tonnage_estime_en_tx) as ton
	from (
		select count(distinct travel_rank) as nbEscales , source_doc_id , annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, tonnage_estime_en_tx
		from (
			select source_doc_id, travel_rank, extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, e.tonnage_estime_en_tx
			from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
			WHERE source_doc_id in (
				SELECT source_doc_id
				FROM navigoviz.raw_flows 
				WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
			) 
			and e.ship_class = ship_class_standardized
		) as total
		group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, source_doc_id,  tonnage_estime_en_tx
	) as subq
	group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales
) as q3
where departure_state_1789_fr in ('Empire russe', 'Portugal', 'Suède', 'Danemark', 'Autriche', 'Pologne', 'Provinces-Unies', 'Prusse', 'Grande-Bretagne', 'Hambourg')
	and departure_substate_1789_fr not in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (departure_province in ('Normandie', 'Aunis', 'Guyenne', 'Bretagne', 'Flandre', 'Picardie'))
group by   nbEscales
order by  nbEscales;

-- de MEDITERRANNEE OCCIDENTALE
select nbEscales, departure_state_1789_fr, sum(n) as n, sum(ton) as ton
from (
	select  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales, count(distinct source_doc_id) as n, sum(tonnage_estime_en_tx) as ton
	from (
		select count(distinct travel_rank) as nbEscales , source_doc_id , annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, tonnage_estime_en_tx
		from (
			select source_doc_id, travel_rank, extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, e.tonnage_estime_en_tx
			from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
			WHERE source_doc_id in (
				SELECT source_doc_id
				FROM navigoviz.raw_flows 
				WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
			) 
			and e.ship_class = ship_class_standardized
		) as total
		group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, source_doc_id,  tonnage_estime_en_tx
	) as subq
	group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales
) as q3
where departure_state_1789_fr in ('Brême', 'République de Lucques', 'Royaume de Piémont-Sardaigne', 'Espagne', 'Maroc', 
'République de Venise', 'République de Gênes', 'République de Raguse', 'Toscane', 'République romaine',
'Duché de Massa et Carrare', 'Royaume de Naples', 'Malte', 'République ligurienne', 'Etats pontificaux', 'Monaco')
	or departure_substate_1789_fr in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (departure_province in ('Languedoc', 'Provence', 'Corse', 'Roussillon'))
group by  departure_state_1789_fr, nbEscales
order by  departure_state_1789_fr, nbEscales;


select nbEscales,  sum(n) as n, sum(ton) as ton
from (
	select  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales, count(distinct source_doc_id) as n, sum(tonnage_estime_en_tx) as ton
	from (
		select count(distinct travel_rank) as nbEscales , source_doc_id , annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, tonnage_estime_en_tx
		from (
			select source_doc_id, travel_rank, extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, e.tonnage_estime_en_tx
			from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
			WHERE source_doc_id in (
				SELECT source_doc_id
				FROM navigoviz.raw_flows 
				WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1758, 1759, 1778, 1779, 1798, 1799)
			) 
			and e.ship_class = ship_class_standardized
		) as total
		group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, source_doc_id,  tonnage_estime_en_tx
	) as subq
	group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales
) as q3
where departure_state_1789_fr in ('Brême', 'République de Lucques', 'Royaume de Piémont-Sardaigne', 'Espagne', 'Maroc', 
'République de Venise', 'République de Gênes', 'République de Raguse', 'Toscane', 'République romaine',
'Duché de Massa et Carrare', 'Royaume de Naples', 'Malte', 'République ligurienne', 'Etats pontificaux', 'Monaco')
	or departure_substate_1789_fr in ('Russie - mer Noire', 'Autriche méditerranéenne')
	or (departure_province in ('Languedoc', 'Provence', 'Corse', 'Roussillon'))
group by   nbEscales
order by   nbEscales;

--- 
select nbEscales,  sum(n) as n, sum(ton) as ton
from (
	select  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales, count(distinct source_doc_id) as n, sum(tonnage_estime_en_tx) as ton
	from (
		select count(distinct travel_rank) as nbEscales , source_doc_id , annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, tonnage_estime_en_tx
		from (
			select source_doc_id, travel_rank, extract(year from indate_fixed) as annee_dep , departure_state_1789_fr, departure_substate_1789_fr, departure_province, ship_class_standardized, e.tonnage_estime_en_tx
			from navigoviz.raw_flows, estimated_tonnage_using_shipclass e
			WHERE source_doc_id in (
				SELECT source_doc_id
				FROM navigoviz.raw_flows 
				WHERE destination_uhgs_id = 'A0210797' and extract(year from indate_fixed) not in (1759, 1779, 1799)
			) 
			and e.ship_class = ship_class_standardized
		) as total
		where departure_state_1789_fr in ('Empire ottoman')
		group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, source_doc_id,  tonnage_estime_en_tx
	) as subq
	group by  annee_dep, departure_state_1789_fr, departure_substate_1789_fr, departure_province, nbEscales
) as q3
group by   nbEscales
order by   nbEscales;

ACA-NEWF
-- departure_shiparea = 'ACA-NEWF'

select extract(year from indate_fixed) as annee_dep, r.tonnage_unit, * from navigoviz.raw_flows r
where
extract(year from indate_fixed) in (1789, 1787)
and source_suite = 'la Santé registre de patentes de Marseille'
and tonnage_unit='Quintaux';

departure_state_1789_fr = 'Grande-Bretagne' and ship_flag_standardized_en = 'French';
-- 


select count(travel_id) from navigoviz.raw_flows rf 
where rf.captain_name like '%Smith%';
-- 113

select travel_id , captain_name from navigoviz.raw_flows rf 
where rf.captain_name like '%Smith%';
-- Smithson, Rodolphe
-- Smith, Thomas

select travel_id , captain_name, departure_fr,  departure_rank_dedieu , departure_navstatus , departure_function, destination_fr , ship_id, ship_name , travel_uncertainity 
from navigoviz.raw_flows rf 
where rf.captain_name like '%Baudrier%' 
order by outdate_fixed ;

select travel_id , captain_name, departure_rank_dedieu , departure_navstatus , departure_function, departure_fr,  outdate_fixed,  destination_fr , ship_id, ship_name , travel_uncertainity 
from navigoviz.built_travels rf  
where rf.captain_id = '00006530'
order by outdate_fixed ;

select count(*) from navigoviz.captain c 
-- 13153
select count(*) from navigoviz.ship c 
-- 12608
select count(*) from navigoviz.captain c where c.captain_id in (select distinct captain_id from navigoviz.built_travels bt)
select count(*) from navigoviz.ship c where c.ship_id in (select distinct ship_id from navigoviz.built_travels bt)

select count(distinct captain_name)  from navigoviz.built_travels bt
-- 21386
select count(distinct ship_name)  from navigoviz.built_travels bt
-- 6583

select * from navigoviz.captain c where captain_id = '00006530'

select count(*) from ports.port_points pp where pp.state_1789_fr = 'France'
-- 544 / 1707 

select 544 / 1707.0 

select uhgs_id , toponyme , toponyme_standard_fr, toponyme_standard_en, geonameid , longitude , latitude  
from ports.port_points pp 
where geonameid is not null and toponyme is not null;

select uhgs_id, unnest(toustopos) as toponyme, longitude , latitude  , ogc_fid 
from ports.port_points pp ;

drop table ports.geo_general_example;
CREATE TABLE ports.geo_general_example (
	uhgs_id text NULL,
	toponyme text NULL,
	latitude text NULL,
	longitude text NULL,
	ogc_fid int);

