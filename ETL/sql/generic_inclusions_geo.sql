------------------------------------------------------------------------------------------------------------------------
-- ANR PORTIC - 
-- Christine Plumejeaud - UMR 7301 Migrinter
-- Les inclusions géographiques
------------------------------------------------------------------------------------------------------------------------

-- import d'un fichier fait à la main reprenant en partie les inclusions géo détectable par carte
-- file:///C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\rivers\Generiques_inclusions_geo.csv

-- Mise à jour de la table et du script en janvier 2022 avec les nouveaux ports anglais et les ports suivants
/*
A0152878	Saint Cyr : La Ciotat, Provence
A0233335	Rossano : Naples
A0242201	Maiori : Naples
A0234673	Marina di Monasterace : Naples Monasterace
A0234281	Campofelice : Sicile
A0235476	Coldirodi : république de Gènes / Coldirodi dans San Rémo
A0336866	Katarina : Empire Ottoman
A1907798	Es Sur : Moyen-Orient, reste Asie
D3674030	Bien Dong : china
*/

-- complétion avec les règles
alter table ports.generiques_inclusions_geo_csv drop column column7 ;
alter table ports.generiques_inclusions_geo_csv drop column column8 ;

select * from ports.generiques_inclusions_geo_csv gigc 

select distinct state_1789_fr, substate_1789_fr from port_points pp 
order by state_1789_fr, substate_1789_fr
--Autriche	Autriche méditerranéenne
--Autriche	Pays-Bas autrichiens
--Brême	
--Chine	
--Danemark	colonies danoises
--Danemark	Norvège
--Danemark	
--Duché d'Oldenbourg	
--Duché de Courlande	
--Duché de Massa et Carrare	
--Duché de Mecklenbourg	
--Empire du Maroc	
--Empire ottoman	Iles anglo-normandes
--Empire ottoman	Régence d'Alger
--Empire ottoman	Régence de Tunis
--Empire ottoman	Tripoli
--Empire ottoman	
--Espagne	Canaries
--Espagne	colonies espagnoles d'Amérique
--Espagne	
--Etats-Unis d'Amérique	Caroline du Nord
--Etats-Unis d'Amérique	Caroline du Sud
--Etats-Unis d'Amérique	Géorgie
--Etats-Unis d'Amérique	Maryland
--Etats-Unis d'Amérique	Massachussets
--Etats-Unis d'Amérique	Massachussets [Maine]
--Etats-Unis d'Amérique	New Hampshire
--Etats-Unis d'Amérique	New York
--Etats-Unis d'Amérique	Pennsylvanie
--Etats-Unis d'Amérique	Rhode Island
--Etats-Unis d'Amérique	Virginie
--Etats-Unis d'Amérique	
--Etats pontificaux	
--France	colonies françaises d'Amérique
--France	colonies françaises en Afrique
--France	colonies françaises en Amérique
--France	colonies françaises en Asie
--France	
--Grande-Bretagne	Angleterre
--Grande-Bretagne	colonies britanniques d'Amérique
--Grande-Bretagne	colonies britanniques en Asie
--Grande-Bretagne	Ecosse
--Grande-Bretagne	Iles anglo-normandes
--Grande-Bretagne	Irlande
--Grande-Bretagne	Pays de Galles
--Grande-Bretagne	possessions anglaises en Méditerranée
--x Grande-Bretagne	
--Hambourg	
--x Islande	colonies danoises
--Lubeck	
--Malte	
--Mecklenbourg	
--Monaco	
--multi-Etat	
--Pologne	
--Portugal	colonies portugaises d'Afrique
--Portugal	colonies portugaises d'Amérique
--Portugal	îles atlantiques portugaises
--Portugal	
--principauté de Lampédouse	
--Principauté de Piombino	
--Provinces-Unies	colonies hollandaises en Afrique
--Provinces-Unies	Frise
--Provinces-Unies	Groningue
--Provinces-Unies	Gueldre
--Provinces-Unies	Hollande
--Provinces-Unies	Pays de la Généralité
--Provinces-Unies	Zélande
--Prusse	Frise orientale
--Prusse	
--République de Gênes	
--République de Lucques	
--République de Raguse	
--République de Venise	
--Royaume de Bonny	
--Royaume de Naples	Etat des Présides
--Royaume de Naples	Sicile
--Royaume de Naples	
--Royaume de Piémont-Sardaigne	Sardaigne
--Royaume de Piémont-Sardaigne	
--Russie	Russie - mer Baltique
--Russie	Russie - mer Blanche
--Russie	Russie - mer Noire
--Russie	
--Suède	Finlande
--Suède	Poméranie suédoise
--Suède	
--Toscane	
--zone maritime	

---------- Nouveaux en février 2023
-- Evêché de Münster
-- France	Corse
-- Maroc ?
-- Provinces-Unies	Overijssel

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=1 limit 1 
-- Corse	A0156739	A0156739	Corse	province='Isles de Corse'	0

select uhgs_id , toponyme_standard_fr from port_points pp where province = 'Isles de Corse' and uhgs_id != 'A0156739'

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Corse', 'A0156739', uhgs_id , toponyme_standard_fr, 'province=''Isles de Corse''' , 1
from port_points pp where province = 'Isles de Corse' and uhgs_id != 'A0156739';
-- 20
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0156739' and ughs_id_sup = 'A0156739'

-- ajout de nouveaux ports Corse / 14 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Corse', 'A0156739', uhgs_id , toponyme_standard_fr, 'province=''Isles de Corse''' , 1
from port_points pp where province = 'Corse' and uhgs_id != 'A0156739' 
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0156739');
-- 9

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- Angleterre	A0390929	A0390929	Angleterre	subunit = Angleterre	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Angleterre', 'A0390929', uhgs_id , toponyme_standard_fr, 'subunit = Angleterre' , 1
from port_points pp where substate_1789_fr = 'Angleterre' and uhgs_id != 'A0390929';
-- 90

-- 14 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Angleterre', 'A0390929', uhgs_id , toponyme_standard_fr, 'subunit = Angleterre' , 1
from port_points pp where substate_1789_fr = 'Angleterre' and uhgs_id != 'A0390929'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0390929');
-- 45
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0390929' and ughs_id_sup = 'A0390929';

-- Nouveaux ports anglais - 20 janvier
-- repérer les nouveaux ports anglais
alter table homeports_smogglage_csv add column new_janvier2022 boolean default false;
update homeports_smogglage_csv set new_janvier2022 = true where ship_homeport_uhgs_id in (
select uhgs_id from ports.port_points where geonameid is null and state_1789_fr = 'Grande-Bretagne')
alter table port_points add column new_janvier2022 boolean default false;
update port_points set new_janvier2022 = true where  geonameid is null and state_1789_fr = 'Grande-Bretagne';

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Angleterre', 'A0390929', uhgs_id , toponyme_standard_fr, 'subunit = Angleterre' , 1
from port_points pp where substate_1789_fr = 'Angleterre' and uhgs_id != 'A0390929' and uhgs_id in 
(select ship_homeport_uhgs_id from homeports_smogglage_csv where new_janvier2022 is true);
-- 22


select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- 	Angleterre (destination simulée pour) A0394917	A0394917	Angleterre (destination simulée pour)	subunit = Angleterre	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Angleterre', 'A0390929', 'A0394917' , 'Angleterre (destination simulée pour)', 'subunit = Angleterre' , 1
-- 1
-- delete from ports.generiques_inclusions_geo_csv where ughs_id = 'A0394917' and ughs_id_sup ='A0394917'


select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- Sicile	A0220588	A0220588	Sicile	subunit = Sicile	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile', 'A0220588', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0220588';
--29
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0220588' and ughs_id_sup = 'A0220588';

-- 14 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile', 'A0220588', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0220588'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0220588');
-- 15

delete from ports.generiques_inclusions_geo_csv 
where ughs_id_sup = 'A0220588' and ughs_id in ('A0231220', 'A0243031');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile, underterminé', 'A0219724', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' ;
-- 29

-- 14 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile, underterminé', 'A0219724', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0219724'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0219724');

delete from ports.generiques_inclusions_geo_csv 
where ughs_id_sup = 'A0219724' and ughs_id in ('A0231220', 'A0243031');

-- Nouveaux ports sicile - 20 janvier
update port_points set new_janvier2022 = true where  geonameid is null and substate_1789_fr = 'Sicile';
select uhgs_id from ports.port_points where  geonameid is null and substate_1789_fr = 'Sicile';
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile', 'A0220588', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0220588' and new_janvier2022 is true;

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sicile, underterminé', 'A0219724', uhgs_id , toponyme_standard_fr, 'subunit = Sicile' , 1
from port_points pp where substate_1789_fr = 'Sicile' and uhgs_id != 'A0219724' and new_janvier2022 is true ;

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 


-- Russie	A1964109	A1964109	Russie	etat=Russie	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Russie', 'A1964109', uhgs_id , toponyme_standard_fr, 'etat = Russie' , 1
from port_points pp where state_1789_fr = 'Russie' and uhgs_id != 'A1964109';

-- 14 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Russie', 'A1964109', uhgs_id , toponyme_standard_fr, 'etat = Russie' , 1
from port_points pp where state_1789_fr = 'Empire russe' and uhgs_id != 'A1964109'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1964109');
-- 4


update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1964109' and ughs_id_sup = 'A1964109';
-- 7 + 1

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- Suède	A1081238	A1081238	Suède	etat=Suède	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Suède', 'A1081238', uhgs_id , toponyme_standard_fr, 'etat = Suède' , 1
from port_points pp where state_1789_fr = 'Suède' and uhgs_id != 'A1081238';
-- 28
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1081238' and ughs_id_sup = 'A1081238';

-- 14 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Suède', 'A1081238', uhgs_id , toponyme_standard_fr, 'etat = Suède' , 1
from port_points pp where state_1789_fr = 'Suède' and uhgs_id != 'A1081238'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1081238');
-- 11

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- Toscane	A0219759	A0219759	Toscane	etat=Toscane	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Toscane', 'A0219759', uhgs_id , toponyme_standard_fr, 'etat = Toscane' , 1
from port_points pp where state_1789_fr = 'Toscane' and uhgs_id != 'A0219759';
-- 12
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0219759' and ughs_id_sup = 'A0219759';


select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- Etats Pontificaux	A1968753	A1968753	Etats Pontificaux	etat =Etats Pontificaux	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Etats pontificaux', 'A1968753', uhgs_id , toponyme_standard_fr, 'etat = Etats pontificaux' , 1
from port_points pp where state_1789_fr = 'Etats pontificaux' and uhgs_id != 'A1968753';
-- 13
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1968753' and ughs_id_sup = 'A1968753';

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Etats pontificaux', 'A1968753', uhgs_id , toponyme_standard_fr, 'etat = Etats pontificaux' , 1
from port_points pp where state_1789_fr = 'Etats pontificaux' and uhgs_id != 'A1968753'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1968753');
--2 
select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- Nouvelle-Angleterre	B0000015	B0000015	Nouvelle-Angleterre	subunit in ('Maine', 'Massachusetts', 'New Hampshire','Rhode Island' )	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Nouvelle-Angleterre', 'B0000015', uhgs_id , toponyme_standard_fr, 'subunit_en in (Maine, Massachusetts, New Hampshire,Rhode Island)' , 1
from port_points pp where substate_1789_en in ('Maine', 'Massachusetts','New Hampshire','Rhode Island') and uhgs_id != 'B0000015';
-- 15
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'B0000015' and ughs_id_sup = 'B0000015';

select '['||substate_1789_fr||']' , '['||substate_1789_en||']' from port_points pp where substate_1789_en in ('Maine','Massachusetts','New Hampshire','Rhode Island')

select '['||substate_1789_fr||']' , '['||substate_1789_en||']', substate_1789_en from port_points pp where substate_1789_fr like '%Massachussets%'
--- 
update etats set subunit='Massachussets' where uhgs_id ='B2827324' and dfrom = 1776;
update port_points set substate_1789_fr='Massachussets' where uhgs_id ='B2827324' ;
update etats set subunit_en='Massachusetts' where subunit_en='Massuchussets' and dfrom = 1776;
update port_points set substate_1789_en='Massachusetts' where substate_1789_en ='Massuchussets' ;

-- 14/02/2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Nouvelle-Angleterre', 'B0000015', uhgs_id , toponyme_standard_fr, 'subunit_en in (Maine, Massachusetts, New Hampshire,Rhode Island)' , 1
from port_points pp where substate_1789_en in ('Maine', 'Massachusetts','New Hampshire','Rhode Island') and uhgs_id != 'B0000015'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B0000015');
-- 2

-- France à l''aventure
select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 
-- à l'aventure	H6666666	H6666666	à l'aventure	etat='France'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'à l''aventure', 'H6666666', uhgs_id , toponyme_standard_fr, 'etat = France and subunit is null' , 1
from port_points pp where state_1789_fr = 'France' and substate_1789_fr is null and uhgs_id != 'H6666666';
-- 347
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'H6666666' and ughs_id_sup = 'H6666666';

-- nouveau en janvier 2022 : St Cyr A0152878
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'à l''aventure', 'H6666666', uhgs_id , toponyme_standard_fr, 'etat = France and subunit is null' , 1
from port_points pp where  uhgs_id = 'A0152878';

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'à l''aventure', 'H6666666', uhgs_id , toponyme_standard_fr, 'etat = France and subunit is null' , 1
from port_points pp where state_1789_fr = 'France' and substate_1789_fr is null and uhgs_id != 'H6666666' 
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'H6666666');
-- 145 le 14/02/2023 

-- A1969432 Bouches de Bonifacio
-- A0134678	Dans le golfe de Saint Florent en Corse (sur mer)
-- beaucoup A0156739 en Corse. Il leur manque l'état approprié. 
-- voir C:\Travail\Projets\ANR_PORTIC\Data\ports\archives_sql\portic_v8b.sql ligne 1050 qui règlent le pb
select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done from ports.generiques_inclusions_geo_csv where done=0 limit 1 

-- Portugal	A1965063	A1965063	Portugal	etat='Portugal' and subunit is null	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Portugal', 'A1965063', uhgs_id , toponyme_standard_fr, 'etat = Portugal and subunit is null' , 1
from port_points pp where state_1789_fr = 'Portugal' and substate_1789_fr is null and uhgs_id != 'A1965063'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965063');
-- A0355726	Portugais
-- 7 / 5 le 14 février 2023

update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965063' and ughs_id_sup = 'A1965063';

--Ecosse	A0388193	A0388193	Ecosse	subunit = 'Ecosse'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Ecosse', 'A0388193', uhgs_id , toponyme_standard_fr, 'subunit = Ecosse' , 1
from port_points pp where substate_1789_fr = 'Ecosse'  and uhgs_id != 'A0388193'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0388193');
-- 18 / 18 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0388193' and ughs_id_sup = 'A0388193';

-- 3 nouveaux port d'Ecosse en janvier 2022
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Ecosse', 'A0388193', uhgs_id , toponyme_standard_fr, 'subunit = Ecosse' , 1
from port_points pp where substate_1789_fr = 'Ecosse'  and new_janvier2022 is true;

--Côte d'Espagne	A1965062	A1965062	Côte d'Espagne	etat='Espagne' and subunit is null	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côte d''Espagne', 'A1965062', uhgs_id , toponyme_standard_fr, 'etat=Espagne and subunit is null' , 1
from port_points pp where state_1789_fr = 'Espagne' and substate_1789_fr is null and uhgs_id != 'A1965062'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965062');
-- 87 / 38 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965062' and ughs_id_sup = 'A1965062';

--Espagne, undeterminé	A0074743	A0074743	Espagne, undeterminé	etat='Espagne' and subunit is null	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Espagne, undeterminé', 'A0074743', uhgs_id , toponyme_standard_fr, 'etat=Espagne and subunit is null' , 1
from port_points pp where state_1789_fr = 'Espagne' and substate_1789_fr is null and uhgs_id != 'A0074743'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0074743');
-- 87 / 38 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0074743' and ughs_id_sup = 'A0074743';

----Canaries	A0079365	A0079365	Canaries	subunit='Canaries'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Canaries', 'A0079365', uhgs_id , toponyme_standard_fr, 'subunit=Canaries' , 1
from port_points pp where substate_1789_fr = 'Canaries'  and uhgs_id != 'A0079365'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0079365');
-- 2
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0079365' and ughs_id_sup = 'A0079365';

-- --Côtes de Bretagne	A1964988	A1964988	Côtes de Bretagne	province='Bretagne'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Bretagne', 'A1964988', uhgs_id , toponyme_standard_fr, 'province=Bretagne' , 1
from port_points pp where province = 'Bretagne'  and uhgs_id != 'A1964988'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1964988');
-- 2 / 50 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1964988' and ughs_id_sup = 'A1964988';

--Languedoc	A1965209	A1965209	Languedoc	province='Languedoc'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Languedoc', 'A1965209', uhgs_id , toponyme_standard_fr, 'province=Languedoc' , 1
from port_points pp where province = 'Languedoc'  and uhgs_id != 'A1965209'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965209');
-- 5 / 5 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965209' and ughs_id_sup = 'A1965209';

--Le long des côtes de Normandie	A1968879	A1968879	Le long des côtes de Normandie	province='Normandie'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Le long des côtes de Normandie', 'A1968879', uhgs_id , toponyme_standard_fr, 'province=Normandie' , 1
from port_points pp where province = 'Normandie'  and uhgs_id != 'A1968879'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1968879');
-- 61 / 29 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1968879' and ughs_id_sup = 'A1968879';

--Côtes de Provence	A0212843	A0212843	Côtes de Provence	province='Provence'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Provence', 'A0212843', uhgs_id , toponyme_standard_fr, 'province=Provence' , 1
from port_points pp where province = 'Provence'  and uhgs_id != 'A0212843'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0212843');
-- 52 / 28  le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0212843' and ughs_id_sup = 'A0212843';
-- nouveau en janvier 2022 : St Cyr A0152878
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Provence', 'A0212843', uhgs_id , toponyme_standard_fr, 'province=Provence' , 1
from port_points pp where province = 'Provence'  and uhgs_id = 'A0152878';

--Cotes du Rousillon	A1965060	A1965060	Cotes du Rousillon	province='Roussillon'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Cotes du Rousillon', 'A1965060', uhgs_id , toponyme_standard_fr, 'province=Roussillon' , 1
from port_points pp where province = 'Roussillon'  and uhgs_id != 'A1965060'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965060');
-- 6 / 3 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965060' and ughs_id_sup = 'A1965060';

--colonies anglaises [en Amérique]	B0000978	B0000978	colonies anglaises [en Amérique]	subunit='colonies britanniques d'Amérique'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'colonies anglaises [en Amérique]', 'B0000978', uhgs_id , toponyme_standard_fr, 'subunit=colonies britanniques d''Amérique' , 1
from port_points pp where substate_1789_fr = 'Colonies britanniques d''Amérique'  and uhgs_id != 'B0000978'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B0000978');
-- 39 / 5 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'B0000978' and ughs_id_sup = 'B0000978';

--Irlande	A0605799	A0605799	Irlande	subunit='Irlande'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Irlande', 'A0605799', uhgs_id , toponyme_standard_fr, 'subunit=Irlande' , 1
from port_points pp where substate_1789_fr = 'Irlande'  and uhgs_id != 'A0605799'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0605799');
-- 14 / 11 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0605799' and ughs_id_sup = 'A0605799';
-- 1

-- Nouveau port d'Irlande Janvier 2022 : #1
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Irlande', 'A0605799', uhgs_id , toponyme_standard_fr, 'subunit=Irlande' , 1
from port_points pp where substate_1789_fr = 'Irlande'  and uhgs_id != 'A0605799' and new_janvier2022 is true;


--Islande	A0146289	A0146289	Islande	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0146289' and ughs_id_sup = 'A0146289';

--Côte d'Afrique [de l'Ouest]	C0000006	C0000006	Côte d'Afrique [de l'Ouest]	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000006' and ughs_id_sup = 'C0000006';

--Côte d'Or	C0000012	C0000012	Côte d'Or	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000012' and ughs_id_sup = 'C0000012';

--Côte d'Angole	C0000009	C0000009	Côte d'Angole	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000009' and ughs_id_sup = 'C0000009';

--Mozambique	C0000013	C0000013	Mozambique	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000013' and ughs_id_sup = 'C0000013';

-- --Cap-Vert	C0000019	C0000019	Cap-Vert	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'C0000019' and ughs_id_sup = 'C0000019';

--Côte du Brésil	B0000934	B0000934	Côte du Brésil	aucun port pour l'instant à associer	0
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'B0000934' and ughs_id_sup = 'B0000934';

--Hollande	A0617755	A0617755	Hollande	subunit='Hollande'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Hollande', 'A0617755', uhgs_id , toponyme_standard_fr, 'subunit=Hollande' , 1
from port_points pp where substate_1789_fr = 'Hollande'  and uhgs_id != 'A0617755'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0617755');
-- 13 / 11 le 14/02/2023

-- Frise A0628870
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Frise', 'A0628870', uhgs_id , toponyme_standard_fr, 'subunit=Frise' , 1
from port_points pp where substate_1789_fr = 'Frise'  and uhgs_id != 'A0628870'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0628870');

select pp.state_1789_fr , pp.substate_1789_fr from port_points pp where pp.uhgs_id in (select ughs_id from generiques_inclusions_geo_csv where toponyme='Provinces-Unies' and ughs_id_sup = 'A0617755')

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'hollandais', 'A0617755', uhgs_id , toponyme_standard_fr, 'state_1789_fr = Provinces-Unies' , 1
from port_points pp where state_1789_fr = 'Provinces-Unies'  and uhgs_id != 'A0617755'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0617755');
-- 40 le 17/02/2023

update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0617755' and ughs_id_sup = 'A0617755';
-- 1

--Zélande	A0635679	A0635679	Zélande	subunit='Zélande'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Zélande', 'A0635679', uhgs_id , toponyme_standard_fr, 'subunit=Zélande' , 1
from port_points pp where substate_1789_fr = 'Zélande'  and uhgs_id != 'A0635679'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0635679');
-- 4
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0635679' and ughs_id_sup = 'A0635679';
-- 1

--Royaume de Naples	A1964382	A1964382	Royaume de Naples	 etat='Royaume de Naples'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Royaume de Naples', 'A1964382', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Naples' , 1
from port_points pp where state_1789_fr = 'Royaume de Naples'  and uhgs_id != 'A1964382'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1964382');

-- 85 / 39 le 14 février 2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1964382' and ughs_id_sup = 'A1964382';
-- 1

-- Nouveaux ports de Naples en janvier 2022
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Royaume de Naples', 'A1964382', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Naples' , 1
from port_points pp where state_1789_fr = 'Royaume de Naples'  and uhgs_id in ('A0233335', 'A0242201', 'A0234673');


--Sardaigne	A0223759	A0223759	Sardaigne	etat='Royaume de Piémont-Sardaigne'	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Sardaigne', 'A0223759', uhgs_id , toponyme_standard_fr, 'etat=Royaume de Piémont-Sardaigne' , 1
from port_points pp where state_1789_fr = 'Royaume de Piémont-Sardaigne'  and uhgs_id != 'A0223759'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0223759');
-- 24 / 10 le 14/02/2023
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A0223759' and ughs_id_sup = 'A0223759';
-- 1

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done 
from ports.generiques_inclusions_geo_csv 
where ughs_id_sup='A1964713'

update ports.generiques_inclusions_geo_csv  set ughs_id_sup = 'A1963986' where toponyme_sup='Mer Baltique';
-- 69

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Mer Baltique', 'A1963986', uhgs_id , toponyme_standard_fr, 'shiparea like BAL-' , 1
from port_points pp where shiparea like 'BAL-%'  and uhgs_id != 'A1963986'
and uhgs_id not in (select ughs_id  from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1963986');
-- 39

update ports.generiques_inclusions_geo_csv  set ughs_id_sup = 'A1964713' where toponyme_sup='Mer Mediterranée';

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Mer Mediterranée', 'A1964713', uhgs_id , toponyme_standard_fr, 'shiparea like MED-' , 1
from port_points pp where shiparea like 'MED-%'  and uhgs_id != 'A1964713'
and uhgs_id not in (select ughs_id  from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1964713');
-- 223

-- 421
-- rivière de Bordeaux	A1968858
update ports.generiques_inclusions_geo_csv  set ughs_id_sup = 'A1968858' where toponyme_sup='rivière de Bordeaux';
--13
update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id_sup = 'A1964713' and ughs_id = 'A0156739';
	

--Italie	A1964975	A1964975	Italie	etat in ('Sardaigne', 'Gènes', 'Parme', 'Modène', 'Toscane', 'Lucques', 'Piombino', 'Naples', 'Etats Pontificaux', 'Venise')	0
--Duché de Massa et Carrare	
--Royaume de Naples
--Royaume de Piémont-Sardaigne
--République de Gênes
--République de Lucques
--République de Venise
--principauté de Lampédouse
--Principauté de Piombino
--Etats pontificaux

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Italie', 'A1964975', uhgs_id , toponyme_standard_fr, 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)' , 1
from port_points pp where state_1789_fr in ('Duché de Massa et Carrare', 'Royaume de Naples', 'Royaume de Piémont-Sardaigne', 'République de Gênes', 'République de Lucques', 'République de Venise', 'principauté de Lampédouse', 'Principauté de Piombino', 'Etats pontificaux')  
and uhgs_id != 'A1964975'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1964975');
-- 173 / 66 Le 14/02/2023

update ports.generiques_inclusions_geo_csv  set done = 1, criteria = 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)'
 where ughs_id = 'A1964975' and ughs_id_sup = 'A1964975';
-- 1

-- 5 nouveaux ports italiens en janvier 2022
/*A0233335	Rossano : Naples
A0242201	Maiori : Naples
A0234673	Marina di Monasterace : Naples Monasterace
A0234281	Campofelice : Sicile
A0235476	Coldirodi : république de Gènes / Coldirodi dans San Rémo*/

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Italie', 'A1964975', uhgs_id , toponyme_standard_fr, 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)' , 1
from port_points pp where state_1789_fr in ('Duché de Massa et Carrare', 'Royaume de Naples', 'Royaume de Piémont-Sardaigne', 'République de Gênes', 'République de Lucques', 'République de Venise', 'Principauté de Lampédouse', 'Principauté de Piombino', 'Etats pontificaux')  
and uhgs_id in ('A0233335', 'A0242201', 'A0234673', 'A0234281', 'A0235476');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Italie', 'A1964975', uhgs_id , toponyme_standard_fr, state_1789_fr, 'etat in (Duché de Massa et Carrare,Royaume de Naples,Royaume de Piémont-Sardaigne,République de Gênes, République de Lucques, République de Venise, principauté de Lampédouse, Principauté de Piombino, Etats pontificaux	)' , 1
from port_points pp where state_1789_fr in ('Duché de Massa et Carrare', 'Royaume de Naples', 'Royaume de Piémont-Sardaigne', 'République de Gênes', 'République de Lucques', 'République de Venise', 'Principauté de Lampédouse', 'Principauté de Piombino', 'Etats pontificaux')  
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1964975');
-- 0 le 16 février 2023

--Côtes de Barbarie	A1965005	A1965005	Côtes de Barbarie	country2019_name in ( 'Egypt') or etat = 'Empire du Maroc' or subunit = Régence de Tunis,  Régence d'Alger, Régence de Tripoli)	0
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where country2019_name = 'Egypt'  and uhgs_id != 'A1965005'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965005');
-- 2 / 0 le 16 février 2023
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where state_1789_fr = 'Empire du Maroc'  and uhgs_id != 'A1965005'
-- 4 

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where state_1789_fr = 'Maroc'  and uhgs_id != 'A1965005'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965005');
-- 3 le 16 février 2023

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where substate_1789_fr in ('Régence de Tunis',  'Régence d''Alger', 'Régence de Tripoli')  and uhgs_id != 'A1965005'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1965005');
--19 / 4 le 16 février 2023

update ports.generiques_inclusions_geo_csv  set done = 1 where ughs_id = 'A1965005' and ughs_id_sup = 'A1965005';
-- 1

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Côtes de Barbarie', 'A1965005', uhgs_id , toponyme_standard_fr, 'country2019_name in (Egypt) or etat = Empire du Maroc or subunit = Régence de Tunis,  Régence d''Alger, Régence de Tripoli)' , 1
from port_points pp where substate_1789_fr in ('Tripoli')  and uhgs_id != 'A1965005';

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done 
from ports.generiques_inclusions_geo_csv 
where done=0
-- 0 -- fini !! jeudi 11 février 2022 12H

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Colonies françaises d''Amérique', 'B0000970', uhgs_id , toponyme_standard_fr, 'subunit=colonies françaises en Amérique' , 1
from port_points pp where substate_1789_fr = 'Colonies françaises d''Amérique' 
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B0000970');
-- 11 le 16 février 2023

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Colonies françaises en Amérique', 'B0000970', uhgs_id , toponyme_standard_fr, 'subunit=colonies françaises en Amérique' , 1
from port_points pp where substate_1789_fr = 'colonies françaises en Amérique' 
-- 2
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Colonies françaises en Amérique', 'B0000970', uhgs_id , toponyme_standard_fr, 'subunit=colonies françaises d''Amérique' , 1
from port_points pp where substate_1789_fr = 'colonies françaises d''Amérique' 
--21
--- B0000970	Colonies françaises en Amérique	

select * from port_points pp where uhgs_id = 'B0000970'
-- [{"1749-1815" : null}]
select * from port_points where state_1789_en = 'ul'
select * from etats  where uhgs_id = 'B0000970'
update  etats  set etat_en = 'France' where uhgs_id = 'B0000970';
update  etats  set subunit = 'colonies françaises d''Amérique' where subunit='colonies françaises en Amérique'

---
-- Rivière de Gênes	A0146288	A0146288	Rivière de Gênes	etat=République de Gênes

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Rivière de Gênes', 'A0146288', uhgs_id , toponyme_standard_fr, 'etat=République de Gênes' , 1
from port_points pp where state_1789_fr = 'République de Gênes' 
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0146288');
-- 10 le 16 février 2023

-- nouveau janvier 2022 : A0235476
update port_points pp set new_janvier2022 = true where pp.uhgs_id in ('A0235476', 'A0336866', 'A1907798', 'D3674030')

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Rivière de Gênes', 'A0146288', uhgs_id , toponyme_standard_fr, 'etat=République de Gênes' , 1
from port_points pp where pp.uhgs_id =  'A0235476' and state_1789_fr = 'République de Gênes' and new_janvier2022 is true

-- Finlande, undeterminé	A0921408	A0921408	Finlande, undeterminé	subunit=Finlande

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Finlande, undeterminé', 'A0921408', uhgs_id , toponyme_standard_fr, 'subunit=Finlande' , 1
from port_points pp where substate_1789_fr = 'Finlande' 
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0921408');

-- 5 / 2 le 16 février 2023

SELECT * FROM ports.generiques_inclusions_geo_csv --2676
create table ports.generiques_inclusions_geo_csv_backup as (select * from ports.generiques_inclusions_geo_csv)
--2637

-- Rouen - Quillebeuf
select uhgs_id from ports.port_points pp where pp.toponyme_standard_fr = 'Seudre' -- A0178037
select uhgs_id from ports.port_points pp where pp.toponyme_standard_fr = 'Marennes' -- A0136930
select uhgs_id from ports.port_points pp where pp.toponyme_standard_fr = 'Rouen' -- A0122218
select uhgs_id from ports.port_points pp where pp.toponyme_standard_fr = 'Quillebeuf' -- A0173748

select toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done 
from ports.generiques_inclusions_geo_csv where done=0 limit 1 

------------------------------------------------------------------------------------------------------
-- ICI : refait ci-dessous car absent du fichier et de la table en v8 le 11 février 2023
-- Seudre - Marennes  (ok, vu avec Silvia le 31 janvier 2022 : étendre aux petits ports de Seudre)

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Seudre', 'A0178037', 'A0136930' , 'Marennes', 'Observed in navigo data for Z markers' , 1;

insert into ports.generiques_inclusions_geo_csv (ughs_id_sup, toponyme_sup , ughs_id , toponyme , criteria , done)
select  'A0136930' , 'Marennes', 'Seudre', 'A0178037', 'Observed in navigo data for Z markers' , 1;


insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Seudre', 'A0178037', uhgs_id, toponyme_standard_fr, 'Observed in navigo data for Z markers' , 1 
from ports.port_points pp where toponyme in ( 'La Tremblade', 'Avallon', 'Chaillevette', 'Mornac', 'Dercie', 'Ribérou [Saujon]');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Seudre', 'A0178037', uhgs_id, toponyme_standard_fr, 'Rivère Seudre sur la carte' , 1 
from ports.port_points pp where toponyme  in ( 'Arveres  [Arvert]', 'L'' Eguille', 'Breuil');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Seudre', 'A0178037', uhgs_id, toponyme_standard_fr, 'Rivère Seudre sur la carte' , 1 
from ports.port_points pp where toponyme_standard_fr  in ( 'Châlon');


-- non à retirer !  vu avec Silvia le 31 janvier 2022
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Rouen', 'A0122218', 'A0173748' , 'Quillebeuf', 'Observed in navigo data for Z markers' , 1

delete FROM ports.generiques_inclusions_geo_csv WHERE ughs_id_sup = 'A0122218' AND ughs_id='A0173748';

-- non à retirer ! vu avec Silvia le 31 janvier 2022
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Rochefort', 'A0196496', 'A0171758' , 'Charente', 'Observed in navigo data as very closed' , 1

delete FROM ports.generiques_inclusions_geo_csv WHERE ughs_id_sup = 'A0196496' AND ughs_id='A0171758';


-- Port-Bail A0209786 et Carteret A0218266 (même greffier pour les 2 ports) -- ok vu avec Silvia le 31 janvier 2022
select * from ports.port_points pp where toponyme_standard_fr = 'Carteret'

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Port-Bail', 'A0209786', 'A0218266' , 'Carteret', 'Observed in navigo data for Z markers' , 1;

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Carteret', 'A0218266', 'A0209786' , 'Port-Bail', 'Observed in navigo data for Z markers' , 1;

------------------------------------------------------------------------------------------------------
-- le 11 février 2023 
-- Des entrées à refaire ci dessus car absentes  
-- A0219769	Barletto en Calabre[Calabria	Calabre (undeterminé)

SELECT * FROM ports.generiques_inclusions_geo_csv WHERE ughs_id_sup = 'A0209786';-- Port-Bail
SELECT * FROM ports.generiques_inclusions_geo_csv WHERE ughs_id_sup = 'A0218266'; -- Carteret
SELECT * FROM ports.generiques_inclusions_geo_csv WHERE ughs_id_sup = 'A0178037'; -- Seudre

select uhgs_id , toponyme , toponyme_standard_fr  from port_points pp where pp.toponyme like '%Calabre%'
-- A0219769	Barletto en Calabre[Calabria	Calabre (undeterminé)

select pp.uhgs_id , pp.toponyme , pp.toponyme_standard_fr , pp.state_1789_fr , insertdate
from port_points pp where uhgs_id = 'A0220382'; -- Pendemeli en Calabre


select array_agg(''''||uhgs_id||'''') from 
(
select pp.uhgs_id , pp.toponyme , pp.toponyme_standard_fr , pp.state_1789_fr ,  insertdate, italie.den_reg 
from port_points pp , ports."Italie_Reg01012021_WGS84" italie 
where den_reg in ( 'Calabria') and st_contains(geom3857 , pp.point3857 )
union (
select pp.uhgs_id , pp.toponyme , pp.toponyme_standard_fr , pp.state_1789_fr , insertdate, 'Calabria'
from port_points pp where uhgs_id = 'A0220382' )
order by den_reg, toponyme_standard_fr
) as k
-- Liste des ports en Calabre
-- ('A0237507','A0246006','A0251398','A0245045','A0237463','A0231692','A0219769','A0223462','A0223015','A0234691','A0252570','A0243730','A0235114','A0248371','A0232232','A0248655','A0245950','A0243031','A0242462','A0222229','A0230257','A0249254','A0245388','A0220382','A1968869','A0233615','A0232618','A0252539','A0230146','A0235714','A0241324','A0230211','A0240863','A0230384','A0230689','A0247233','A0238490','A1969358','A0233335','A0234673')

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Calabre (undeterminé)', 'A0219769', uhgs_id , toponyme_standard_fr, 'Ports de Calabre (croisement géographique avec région italienne Calabria - buffer 2 km)' , 1
from port_points pp where uhgs_id != 'A0219769'
and uhgs_id in ('A0237507','A0246006','A0251398','A0245045','A0237463','A0231692','A0219769','A0223462','A0223015','A0234691','A0252570','A0243730','A0235114','A0248371','A0232232','A0248655','A0245950','A0243031','A0242462','A0222229','A0230257','A0249254','A0245388','A0220382','A1968869','A0233615','A0232618','A0252539','A0230146','A0235714','A0241324','A0230211','A0240863','A0230384','A0230689','A0247233','A0238490','A1969358','A0233335','A0234673')
-- 39


-- Vérifier tous les ports aux USA (B0000919)
select * from ports.generiques_inclusions_geo_csv where ughs_id_sup = 'B0000919';

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'étasunien', 'B0000919', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant)' , 1
from port_points pp where state_1789_fr = 'Etats-Unis d''Amérique'  and uhgs_id != 'B0000919'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B0000919');
-- 19

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Caroline du Nord', 'B0000960', uhgs_id , toponyme_standard_fr, 'sustate Caroline du Nord' , 1
from port_points pp where substate_1789_fr = 'Caroline du Nord'  and uhgs_id != 'B0000960'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B0000960');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Caroline', 'B0000969', uhgs_id , toponyme_standard_fr, 'sustate Caroline du Nord / Caroline du Sud' , 1
from port_points pp where substate_1789_fr in ('Caroline du Nord', 'Caroline du Sud') and uhgs_id != 'B0000969'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B0000969');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'britannique', 'A0395415', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Grande-Bretagne') and uhgs_id != 'A0395415'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0395415');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'carrarais', 'A0251618', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Duché de Massa et Carrare') and uhgs_id != 'A0251618'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0251618');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'danois', 'A0824104', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Danemark') and uhgs_id != 'A0824104'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0824104');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'espagnol', 'A0074743', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Espagne') and substate_1789_fr is null and uhgs_id != 'A0074743'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0074743');
-- dejà fait

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'français', 'A0167415', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('France') and uhgs_id != 'A0167415'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0167415');
-- 327

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'génois', 'A0219524', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('République de Gênes') and uhgs_id != 'A0219524'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0219524');
-- 42

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'maltais', 'A0036064', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Malte') and uhgs_id != 'A0036064'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0036064');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'meckelmbourgeois', 'A0676229', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Duché de Mecklenbourg') and uhgs_id != 'A0676229'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0676229');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'ottoman', 'A1968748', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Empire ottoman') and uhgs_id != 'A1968748'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1968748');
-- 136

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'papenbourgeois', 'A0799311', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Evêché de Münster') and uhgs_id != 'A0799311'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0799311');
-- 0

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'prussien', 'A1156318', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Prusse') and uhgs_id != 'A1156318'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1156318');
-- 14

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'prussien', 'A1156318', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Prusse') and uhgs_id != 'A1156318'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1156318');
-- 14

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'ragusois', 'A1264541', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('République de Raguse') and uhgs_id != 'A1264541'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1264541');
-- 1

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'savoyard', 'A0128592', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Royaume de Piémont-Sardaigne') and uhgs_id != 'A0128592'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0128592');
-- 31

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'vénitien', 'A0219489', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('République de Venise') and uhgs_id != 'A0219489'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0219489');

select * from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1264541'

select 'hambourgeois', 'A0714848', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Hambourg') and uhgs_id != 'A0714848'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0714848');
-- 0

select * from port_points pp where pp.toponyme_standard_fr  = 'Hambourg' -- A0743522


insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'monégasque', 'A0353888', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Monaco') and uhgs_id != 'A0353888'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0353888');

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Monaco', 'A0353892', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Monaco') and uhgs_id != 'A0353892'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0353892');

select * from port_points pp where pp.toponyme_standard_fr  = 'Monaco' -- A0353892

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Pays-Bas autrichiens', 'A0049520', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where substate_1789_fr in ('Pays-Bas autrichiens') and uhgs_id != 'A0049520'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0049520');

select * from port_points pp where pp.uhgs_id  = 'A1149784' -- A0353892

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'polonais', 'A1149784', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Pologne') and uhgs_id != 'A1149784'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1149784');
-- 0 

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Piombino', 'A0251559', uhgs_id , toponyme_standard_fr, 'alignement  flags - homeports (états correspondant) ' , 1
from port_points pp where state_1789_fr in ('Principauté de Piombino') and uhgs_id != 'A0251559'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0251559');



-- Traiter aussi la Catalogne à rajouter dans Geoinclusion
-- utiliser la table world_borders : niveau commune ES51%


select * from world_1789 w where w.unit_code like 'ES%'
select * from world_1789 w where w.shortname  like 'Catalo%' and unitlevel = 2

-- Silvia : Pour le pointcall A1963922, c'est "Catalogne" appartenant donc à Espagne, lat-long 42,2 . 
-- A priori tu devrais quand même l'avoir car il était dans captain_citizenship


select * from port_points pp where pp.uhgs_id  = 'A1963922' -- Catalogne
select * from port_points pp where pp.uhgs_id  = 'A0087160' -- Catalogne

select pp.uhgs_id , pp.toponyme , pp.toponyme_standard_fr  
from world_1789 w , port_points pp 
where w.shortname  = 'Catalonia' and unitlevel = 2
and st_contains(w.geom3857, pp.point3857)
order by toponyme_standard_fr; --31

select pp.uhgs_id , pp.toponyme , pp.toponyme_standard_fr  , state_1789_fr
from world_1789 w , port_points pp 
where w.shortname  = 'Catalonia' and unitlevel = 2
and st_intersects(w.geom3857, st_buffer(pp.point3857 , 5000))
order by toponyme_standard_fr; --39

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Catalogne', 'A1963922', uhgs_id , toponyme_standard_fr, 'Appartient à la région Catalonia (level 2) de world_1789 - code ES51% dans la NUTS ' , 1
from port_points pp where  uhgs_id != 'A1963922'
and uhgs_id in 
	(select pp.uhgs_id 
	from world_1789 w , port_points pp 
	where w.shortname  = 'Catalonia' and unitlevel = 2
	and st_intersects(w.geom3857, st_buffer(pp.point3857 , 5000))
	order by toponyme_standard_fr	)
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1963922');
-- 38

update ports.generiques_inclusions_geo_csv set criteria = 'Flag du catalan - '||criteria
where ughs_id_sup = 'A1963922';

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Catalogne', 'A0087160', uhgs_id , toponyme_standard_fr, 'Appartient à la région Catalonia (level 2) de world_1789 - code ES51% dans la NUTS ' , 1
from port_points pp where  uhgs_id != 'A0087160'
and uhgs_id in 
	(select pp.uhgs_id 
	from world_1789 w , port_points pp 
	where w.shortname  = 'Catalonia' and unitlevel = 2
	and st_intersects(w.geom3857, st_buffer(pp.point3857 , 5000))
	order by toponyme_standard_fr	)
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A0087160');
-- 38


insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Guadeloupe', 'B1979292', uhgs_id , toponyme_standard_fr, 'province=Guadeloupe ' , 1
from port_points pp where province in ('Guadeloupe') and uhgs_id != 'B1979292'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B1979292');
-- 0 le 18/02/2023

insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Martinique', 'B1973827', uhgs_id , toponyme_standard_fr, 'province=Martinique ' , 1
from port_points pp where province in ('Martinique') and uhgs_id != 'B1973827'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B1973827');
-- 1 le 18/02/2023

-- Saint-Domingue	B2047921
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Saint-Domingue', 'B2047921', uhgs_id , toponyme_standard_fr, 'province=Saint-Domingue ' , 1
from port_points pp where province in ('Saint-Domingue') and uhgs_id != 'B2047921'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B2047921');
-- 7 le 18/02/2023

-- rivière de Bordeaux A1968858
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'rivière de Bordeaux', 'A1968858', uhgs_id , toponyme_standard_fr, 'carte ' , 1
from port_points pp where uhgs_id  in ('A1968858','A0215306','A0213627','A0212488','A0209381','A0207141','A0205721','A0202072','A0196731','A0195706','A0194558','A0189289','A0180923','A0177829','A0177190','A0175325','A0172590','A0158925','A0148390','A0147192','A0145654','A0140740','A0138162','A0135760','A0133336','A0127472','A0124809','A0122883') 
and uhgs_id != 'A1968858'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1968858');
-- 15 

-- Chypre	A1381781
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Chypre', 'A1381781', uhgs_id , toponyme_standard_fr, 'carte' , 1
from port_points pp where uhgs_id  in ('A1378826', 'A1379621', 'A1381781', 'A1383518') and uhgs_id != 'A1381781'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'A1381781');
-- 0



select 'Nouvelle Ecosse', 'B2123882', uhgs_id , toponyme_standard_fr, 'carte' , 1, toponyme 
from port_points pp where  uhgs_id != 'B2123882' and substate_1789_fr = 'Colonies britanniques d''Amérique'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B2123882');
-- B2120163	Halifax
-- B0000940	Middleton
-- B2125493	îles de la Madeleine

-- Nouvelle Ecosse	B2123882
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Nouvelle Ecosse', 'B2123882', uhgs_id , toponyme_standard_fr, 'carte' , 1 
from port_points pp where  uhgs_id != 'B2123882' and uhgs_id in ('B2120163', 'B0000940', 'B2125493')
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup = 'B2123882');

--Terre-Neuve, île de	B211989
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Terre-Neuve, île de', 'B211989', uhgs_id , toponyme_standard_fr, 'carte et Colonies britanniques d''Amérique et Canada mais pas Gaspésie ni Nouvelle-Ecosse' , 1 
from port_points pp where  uhgs_id != 'B211989' and substate_1789_fr = 'Colonies britanniques d''Amérique' and pp.country2019_name = 'Canada'
and uhgs_id not in ('B2116612', 'B2118113', 'B2136191', 'B0000721')
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('B2123882', 'B211989'))

-- Gibraltar	A0354005
-- -- A0354076, A0354005, A0354220
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Gibraltar', 'A0354005', uhgs_id , toponyme_standard_fr, 'carte ' , 1 
from port_points pp where  uhgs_id != 'A0354005' 
and uhgs_id  in ('A0354076',  'A0354220')
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('A0354005'))
-- 0 

-- Marennes	A0136930
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Gibraltar', 'A0354005', uhgs_id , toponyme_standard_fr, 'carte ' , 1 
from port_points pp where  uhgs_id != 'A0354005' 
and uhgs_id  in ('A0354076',  'A0354220')
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('A0354005'))

select ughs_id, toponyme  from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('A0136930') -- Seudre est dans Marennes
select ughs_id, toponyme  from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('A0178037') -- Seudre contient aussi A0218987, A0129594, A1968855


-- A0134258 Saintonge
insert into ports.generiques_inclusions_geo_csv (toponyme_sup , ughs_id_sup , ughs_id , toponyme , criteria , done)
select 'Saintonge', 'A0134258', uhgs_id , toponyme_standard_fr, 'amiraute=Marennes sauf Ile Olérons (A0128573) ' , 1 
from port_points pp where  uhgs_id != 'A0134258' 
and amiraute = 'Marennes'
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('A0134258'))
and uhgs_id not in (select ughs_id from ports.generiques_inclusions_geo_csv where  ughs_id_sup in ('A0128573'))


select * from ports.generiques_inclusions_geo_csv where done != 1; -- 5745/978
-- hambourgeois	A0714848	A0743522	Hambourg	alignement  flags - homeports (états correspondant) 	324	5204
-- monégasque	A0353888	A0209756	Monaco	alignement  flags - homeports (états correspondant) 	44	5233
-- monégasque	A0353888	A0353892	Monaco	alignement  flags - homeports (états correspondant) 	13	5234
-- Pays-Bas autrichien	A0049520	A0041886	Autriche	alignement  flags - homeports (états correspondant) 	38	5237
-- polonais	A1149784	A1204354	Pologne	alignement  flags - homeports (états correspondant) 	28	5242
-- Piombino		A0251559	Principauté de Piombino	alignement  flags - homeports (états correspondant) 	2	5277

select count(*) from ports.generiques_inclusions_geo_csv; -- 7230
select count(*) from (select distinct toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria from ports.generiques_inclusions_geo_csv) as k; -- 4591

drop table ports.generiques_geoinclusions;
create table ports.generiques_geoinclusions as 
(select distinct toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria from ports.generiques_inclusions_geo_csv)
-- adapter le code de portic_humanum\navigocorpus\Check\Pointcall_itineraries.py pour utiliser cette table à la place

 -- pour pouvoir supprimer les doublons à l'avenir
alter table ports.generiques_geoinclusions add column id serial;


select distinct toponyme_sup, ughs_id_sup from ports.generiques_geoinclusions 

select count(*) from ports.generiques_geoinclusions where toponyme_sup='île de Ré' --8
select count(*) from ports.generiques_geoinclusions where toponyme_sup='Ile d''Oléron' --7
select count(*) from ports.generiques_geoinclusions where toponyme_sup='Mer Mediterranée' --644
select count(*) from ports.port_points pp where shiparea like 'MED-%'; -- 644
select *  from ports.port_points pp  where toponyme like 'Rivière %' 
-- rivière de Bordeaux A1968858
-- rivière de Gênes A0219729

