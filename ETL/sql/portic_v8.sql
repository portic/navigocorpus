----------------------------------------------------------------------------------------
-- 5 nov 2022 - Fabrique de la base portic_v8
-- Christine PLumejeaud-Perreau, UMR 7301 MIGRINTER 
-- ANR PORTIC
----------------------------------------------------------------------------------------

-- créer la base
sudo -u postgres created --encoding=UTF8  --owner=navigo portic_v8
sudo -u postgres psql -U postgres -p 5432 -d portic_v8 -c "GRANT dba TO navigo;"


CREATE ROLE navigo WITH CREATEDB LOGIN PASSWORD 'navigocorpus2018';
GRANT mydba TO navigo;
GRANT dba TO navigo;

-- Utiliser le script BuildNavigoviz.setExtensionsFunctions()
-- il fait ceci

create extension if not exists postgis ;
create extension if not exists fuzzystrmatch;
create extension if not exists pg_trgm;
create extension if not exists postgis_topology;
create extension if not exists plpython3u;
 
select * from pg_language;
SELECT lanpltrusted FROM pg_language WHERE lanname LIKE 'plpython3u';


create schema navigo;
create schema navigocheck;
create schema navigoviz;
create schema ports;
 
create role mydba with superuser noinherit; 
create role porticapi;
create role api_user;
ALTER USER porticapi WITH PASSWORD 'portic';
ALTER USER api_user WITH PASSWORD 'portic';


-- à la toute fin (quand on a toutes les données)

GRANT ALL ON SCHEMA navigo TO navigo;
GRANT ALL ON SCHEMA navigocheck TO navigo;
GRANT ALL ON SCHEMA navigoviz TO navigo;
GRANT ALL ON SCHEMA ports TO navigo;
GRANT ALL ON SCHEMA public TO navigo;
GRANT ALL ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO navigo;
grant ALL on all sequences in schema navigoviz, navigo, navigocheck, ports, public to navigo;

GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigocheck to  api_user;
GRANT USAGE ON SCHEMA navigo to  api_user;
GRANT USAGE ON SCHEMA public to  api_user;
GRANT USAGE ON SCHEMA ports to  api_user;
GRANT USAGE ON SCHEMA ports to  api_user;

GRANT USAGE ON SCHEMA navigoviz TO porticapi;
GRANT USAGE ON SCHEMA navigocheck to porticapi;
GRANT USAGE ON SCHEMA navigo to porticapi;
GRANT USAGE ON SCHEMA public to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;
GRANT USAGE ON SCHEMA ports to porticapi;

GRANT SELECT ON TABLE ports.port_points TO api_user;
GRANT SELECT ON TABLE ports.port_points TO porticapi;

GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO  api_user;
grant CONNECT on database portic_v8 to  api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to  api_user;

GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO porticapi;
grant CONNECT on database portic_v8 to porticapi;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to porticapi;


--- Une fonction à bien mettre en place dans navigo

CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -4
                        result = None
                else :
                    result = None
                    code = -4
                return [result, code]
            $$ LANGUAGE plpython3u;

-----------------------------------------------------------------------------------------------
-- la table port.port_points
-----------------------------------------------------------------------------------------------
       
-- 1. run BuildPorts
-- redo CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)

-- supprimer le dernier " de la ligne query = 
-- modifier la table ports.geo_general --> navigo.geo_general_complet
CREATE OR REPLACE FUNCTION ports.frequency_topo(uhgs_id text, toustopo text)
 RETURNS json
 LANGUAGE plpython3u
AS $function$
	import json
	from operator import itemgetter
	global temp
	global result
	global topos
	global query
	if toustopo is not None :
		result = []
		temp = toustopo.replace('{', '').replace('}', '').strip(' ')
		temp = temp.replace('"', '').strip(' ')
		topos = temp.split(',')
		for item in topos:
		    #plpy.notice(item)
		    
		    query = "select count(*) as freq from navigocheck.check_pointcall  where pointcall_uhgs_id = '"+uhgs_id+"' and pointcall_name='"+item.replace('\'', '\'\'')+"'""
		    
		    #plpy.notice(query)
		    rv = plpy.execute(query)
		    #plpy.notice(rv[0]["freq"])
		    result.append(dict(topo=item, freq=rv[0]["freq"]))
		result = sorted(result, key=itemgetter('freq'), reverse=True)

	else :
		result = None
	#plpy.notice (result)
	return json.dumps(result, ensure_ascii=False)
$function$
;

-- 2.a run BuildPorts.setExtensionsFunctions
-- 2.b run BuildPorts.setExtensionsFunctions

-- 3. Importer le vieux fichier de ports.port_points et le renommer en backup
-- dans C:\Travail\Data\BDD-backups\portic\portic_port_points_V7_nov2022.sql
sudo -u postgres psql -U postgres -p 5432 -d portic_v8 -f /home/plumegeo/portic/portic_V8/portic_port_points_V7_nov2022.sql
-- 1434 entrées
alter table ports.port_points rename to port_points_old

drop table ports.port_points;
create table ports.port_points as (select * from ports.port_points_old);
alter table ports.port_points add column insertdate text default '2019-2020';
update ports.port_points set insertdate = 'janvier 2022' where new_janvier2022 is true;
alter table ports.port_points drop column new_janvier2022;
-- type int using (case when true then 2022 else 2023 end)


-- 4. Importer le fichier CSV de Geogeneral
-- Il est dans navigo.geo_general

-- pour la v6 et v8
CREATE TABLE navigo.geo_general_complet (
	country text NULL,
	class_name text NULL,
	pointcall_name text NULL,
	latitude text NULL,
	longitude text NULL,
	pointcall_uhgs_id text NULL,
	mgrs_id text NULL
);
sudo -U postgres psql -p 5432 -d portic_v8 -U postgres

\COPY navigo.geo_general_complet FROM /home/plumegeo/portic/portic_V8/geogeneral_28sept2022.txt
COPY  13912271 en v8

-- les ports




select pointcall_uhgs_id, count(*) as c from (
	select cp.pointcall_uhgs_id, array_agg(distinct cp.pointcall_name) , trim(gg.latitude) , trim(gg.longitude), 'novembre 2022'
	from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
	where cp.pointcall_uhgs_id  not in (select uhgs_id from ports.port_points pp2) 
	and gg.pointcall_uhgs_id = cp.pointcall_uhgs_id and gg.latitude != '' and gg.longitude != ''
	group by cp.pointcall_uhgs_id, gg.latitude , gg.longitude
) as k --208 au lieu de 210 si null long/lat
group by pointcall_uhgs_id
having count(*) > 1;
 
-- A0395415	2

select cp.pointcall_uhgs_id, array_agg(distinct cp.pointcall_name) , trim(gg.latitude) , trim(gg.longitude), 'novembre 2022'
from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
where cp.pointcall_uhgs_id  not in (select uhgs_id from ports.port_points pp2) 
and gg.pointcall_uhgs_id = cp.pointcall_uhgs_id and  cp.pointcall_uhgs_id in ('A0332141', 'A0395415')
group by cp.pointcall_uhgs_id, gg.latitude , gg.longitude;
-- A0332141	{"Isle de Hidre en Morée","Isle d' Hidre"}	37.347778	23.458889	novembre 2022
-- A0395415	{"Isles Anglaises"}	54	-2	novembre 2022

insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate)
select cp.pointcall_uhgs_id, array_agg(distinct cp.pointcall_name) , trim(gg.latitude) , trim(gg.longitude), 'novembre 2022'
from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
where cp.pointcall_uhgs_id  not in (select uhgs_id from ports.port_points pp2) 
and gg.pointcall_uhgs_id = cp.pointcall_uhgs_id and gg.latitude != '' and gg.longitude != '' and  cp.pointcall_uhgs_id !='A0395415'
group by cp.pointcall_uhgs_id, gg.latitude , gg.longitude;
-- 206

-- les homeports qui sont codés mais pas dans ma table port
insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate)
select cp.ship_homeport_uhgs_id, array_agg( distinct trim(cp.ship_homeport)) , trim(gg.latitude) , trim(gg.longitude), 'novembre 2022'
from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
where cp.ship_homeport_uhgs_id not in (select uhgs_id from ports.port_points pp2) 
and gg.pointcall_uhgs_id = cp.ship_homeport_uhgs_id and gg.latitude != '' and gg.longitude != '' and  gg.pointcall_uhgs_id !='A0395415' 
group by cp.ship_homeport_uhgs_id, gg.latitude, gg.longitude;
-- 3 au lieu de 6

-- insertion de l'anomalie, avec choix arbitaire de la longitude (-2 et pas -4)
insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate) 
select 'A0395415', ARRAY['British', 'Isles Anglaises'], '54', '-2', 'novembre 2022';
-- 1

-- les captain_birthplace_id qui sont codés mais pas dans ma table port

insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate)
select cp.captain_birthplace_id , array_agg( distinct trim(cp.captain_birthplace)) , trim(gg.latitude) , trim(gg.longitude), 'novembre 2022'
from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
where cp.captain_birthplace_id not in (select uhgs_id from ports.port_points pp2) 
and gg.pointcall_uhgs_id = cp.captain_birthplace_id and gg.latitude != '' and gg.longitude != '' and  gg.pointcall_uhgs_id !='A0395415' 
group by cp.captain_birthplace_id, gg.latitude, gg.longitude;
-- 26


-- les captain_citizenship_id qui sont codés mais pas dans ma table port

insert into ports.port_points (uhgs_id, toustopos, latitude, longitude, insertdate)
select cp.captain_citizenship_id  , array_agg( distinct trim(cp.captain_citizenship)) , trim(gg.latitude) , trim(gg.longitude), 'novembre 2022'
from navigocheck.check_pointcall cp , navigo.geo_general_complet gg 
where cp.captain_citizenship_id not in (select uhgs_id from ports.port_points pp2) 
and gg.pointcall_uhgs_id = cp.captain_citizenship_id and gg.latitude != '' and gg.longitude != '' and  gg.pointcall_uhgs_id !='A0395415' 
group by cp.captain_citizenship_id, gg.latitude, gg.longitude;
-- 7

/*
 * A0355726	{Portuguese}	39.5	-8	novembre 2022
A0395415	{British}	54	-2	novembre 2022
A0395415	{British}	54	-4	novembre 2022
A0753723	{Eckernførde}	54.466667	9.833333	novembre 2022
A1156318	{Prussian}	53.5	18	novembre 2022
 */

-- mais il manque shipping_area
-- Importer le fichier pointcall_UHGS_coding_10jan2022_task(fr_87-89) exporté de la vue pointcall_UHGS_coding
-- Deux colonnes : UHGS_id, shiparea

-- Import du fichier C:\Travail\Data\10-Navigo_29sept2022\pointcall_UHGS_coding.csv

select count(*) from ports.pointcall_uhgs_coding puc 
-- 139406
select * from ports.pointcall_uhgs_coding puc limit 1

update ports.port_points pp set shiparea = ps.shiparea 
from ports.pointcall_uhgs_coding ps
where ps."Pointcall_UHGS_id"  = pp.uhgs_id and pp.insertdate = 'novembre 2022' and ps.shiparea != '';
-- 22

update ports.port_points set topofreq = ports.frequency_topo(uhgs_id, array_to_string(toustopos, ',')) 
--select ports.frequency_topo(uhgs_id, array_to_string(toustopos, ','))  from ports.port_points
where array_length(toustopos, 1) > 0 and ports.test_double_type(longitude) is true and topofreq is null and insertdate = 'novembre 2022';
-- 210 + 33

select * from ports.port_points where topofreq is null and insertdate = 'novembre 2022';
update ports.port_points set toponyme = (topofreq::json->>0)::json->>'topo' where toponyme is null and insertdate = 'novembre 2022'
-- 210 + 33

update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where insertdate = 'novembre 2022';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857) where insertdate = 'novembre 2022';

-- STOP ici

-- 5. Import de ports.world_borders
-- Fichier portic_v6_GIS_18janvier2022.sql dans C:\Travail\Data\Navigo_10janvier2022
-- psql -U postgres -D portic_v7 -p 5435 -f portic_v6_GIS_18janvier2022.sql
-- generiques_inclusions_geo_csv 
-- gshhs_f_l1_3857
-- lau_europe
-- limites_amirautes
-- rivers_f
-- world_1789
-- world_borders
-- wup2018_f13_capital_cities_csv

-- à zipper
-- etats
-- labels_lang_csv
-- generiques_inclusions_geo_csv 

sudo -u postgres psql -U postgres -d portic_v8 -f /home/plumegeo/portic/portic_V8/portic_v6_GIS_18janvier2022.sql

CREATE TABLE ports.etats (
	country2019_name text NULL,
	toponyme_standard_fr text NULL,
	toponyme text NULL,
	uhgs_id text NULL,
	etat text NULL,
	subunit text NULL,
	dfrom int4 NULL,
	dto int4 NULL,
	geonameid int4 NULL,
	name_en text NULL,
	admin1_code text NULL,
	latitude float8 NULL,
	longitude float8 NULL,
	tgnid int4 NULL,
	wikipedia text NULL,
	admin2_code text NULL,
	admin3_code text NULL,
	etat_en text NULL,
	subunit_en text NULL
);
-- sudo -u postgres psql -U postgres -d portic_v8 -f /home/plumegeo/portic/portic_V8/etats_portic_v7.sql
/*SELECT country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, 
			wikipedia, admin2_code, admin3_code, etat_en, subunit_en
            FROM dblink('dbname=portic_v7 user=postgres password=mesange17 options=-csearch_path=',
            'select country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, 
			wikipedia, admin2_code, admin3_code, etat_en, subunit_en
            from ports.etats')
            AS t1(country2019_name text, toponyme_standard_fr text, toponyme text, uhgs_id text, etat text, subunit text,
           dfrom int4, dto int4, geonameid int4, name_en text, admin1_code text, latitude float8, longitude float8, tgnid int4,
          	wikipedia text, admin2_code text, admin3_code text, etat_en text, subunit_en text);
 */

drop table ports.generiques_inclusions_geo_csv;
create table  ports.generiques_inclusions_geo_csv as
SELECT toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria, done
            FROM dblink('dbname=portic_v7 user=postgres password=mesange17 options=-csearch_path=',
            'select toponyme_sup, ughs_id_sup, ughs_id, toponyme, criteria, done
            from ports.generiques_inclusions_geo_csv')
            AS t1(toponyme_sup text, ughs_id_sup text, ughs_id text, toponyme text, criteria text, done int4);
-- 5274
           
create table  ports.labels_lang_csv as
SELECT label_type, key_id, fr, en, remarques
            FROM dblink('dbname=portic_v7 user=postgres password=mesange17 options=-csearch_path=',
            'select label_type, key_id, fr, en, remarques
            from ports.labels_lang_csv')
            AS t1(label_type text, key_id text, fr text, en text, remarques text);
           

update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 50000)) and p.country2019_name is null and insertdate = 'novembre 2022' and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate = 'novembre 2022' ;
-- 197
-- 33
       
select uhgs_id, toponyme, shiparea from ports.port_points p where   country2019_name is null and insertdate = 'novembre 2022'  
select toponyme , * from ports.port_points where country2019_name is null and point3857 is not null
select count(*) from ports.port_points where country2019_name is null and point3857 is not null

-- récuperer la liste des ports à traiter pour toponyme_standard_fr, et etats d'appartenance

select uhgs_id , toponyme, country2019_name, shiparea , amiraute , province , has_a_clerk , state_1789_fr, substate_1789_fr , 
'renseigner le code UHGS_id d'' port ayant la même appartenance étatique' as etat_like
from ports.port_points 
where insertdate = 'novembre 2022' and toponyme_standard_fr is null and geom is not null
order by country2019_name, uhgs_id;

----------------------------------------------------------------
-- done le 06 novembre 2022 pour Silvia puis le 08 novembre
----------------------------------------------------------------

-- export des ports codés avec géolocalisation
select uhgs_id , toponyme_standard_fr, toponyme_standard_en, toustopos , longitude , latitude , shiparea , amiraute , province , has_a_clerk , state_1789_fr, substate_1789_fr  
from ports.port_points 
where  toponyme_standard_fr is not null --and geom is not null
order by state_1789_fr, substate_1789_fr  ;

select count(uhgs_id )
-- , toponyme_standard_fr, toponyme_standard_en, toustopos , longitude , latitude , shiparea , amiraute , province , has_a_clerk , state_1789_fr, substate_1789_fr  
from ports.port_points ;
-- 1676

select count(distinct pointcall_uhgs_id), mgrs_id  
from  navigo.geo_general_complet ggc 
where ggc.pointcall_uhgs_id in (select uhgs_id from ports.port_points)
group by mgrs_id ;
-- 456 nouvelles entrées

select count(distinct pointcall_uhgs_id) 
from  navigo.geo_general_complet ggc 
where mgrs_id !='' and ggc.pointcall_uhgs_id in (select uhgs_id from ports.port_points)
-- 1442 réutilisation de la base NGA / 1634 

select count(distinct pointcall_uhgs_id) 
from  navigo.geo_general_complet ggc 
where mgrs_id ='' and ggc.pointcall_uhgs_id in (select uhgs_id from ports.port_points)
-- 456 nouvelles entrées

-- Pourcentage de valeurs saisies manuellement
select 456.0/1634 * 100.0;



select *
from  navigo.geo_general_complet ggc 
where mgrs_id ='';


select *
-- , toponyme_standard_fr, toponyme_standard_en, toustopos , longitude , latitude , shiparea , amiraute , province , has_a_clerk , state_1789_fr, substate_1789_fr  
from ports.port_points 
where amiraute = 'Honfleur';

------------------------------------------------
-- Import du travail d'identification de Silvia le 19/12/2022 
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\ports_nouveaux_v8_8nov2022_retour SIlvia_15dec2022.csv
----------------------------------------------
update ports.port_points p set uhgs_id = 'A0184606' where toponyme = 'Sainte Croix' and uhgs_id = 'A0172233';
select * from ports.port_points p where toponyme = 'Sainte Croix'; -- A0172233
update ports.ports_nouveaux_v8 set toponyme_standard_fr = 'Sainte-Croix-sur-Mer' , toponyme_standard_gb  = 'Sainte-Croix-sur-Mer'
where uhgs_id= 'A0184606' and toponyme = 'Sainte Croix';
-- Il faut garder saint-croix avec son ancien Uhgs_id pour matcher avec les données

select * from ports.port_points p where uhgs_id = 'A0184606' and toponyme = 'Sainte Croix' 

insert into ports.port_points (uhgs_id , latitude , longitude, province, toustopos, topofreq , toponyme, geom, point3857 , country2019_name, country2019_iso2code, country2019_region, insertdate)
select 'A0172233' , latitude , longitude ,  'Normandie' , array[toponyme], topofreq, toponyme , geom, point3857 , country2019_name , country2019_iso2code, country2019_region , 'novembre 2022'
from port_points 
where uhgs_id= 'A0184606' and toponyme = 'Sainte Croix'; -- doublon inséré
update ports.port_points p set insertdate  = 'decembre 2022' where toponyme = 'Sainte Croix' and uhgs_id = 'A0184606';


alter table ports.ports_nouveaux_v8 rename column "0"  to uhgs_id;

select * 
from ports.port_points p
left join ports_nouveaux_v8 pnv on p.uhgs_id = pnv.uhgs_id 
where insertdate = 'novembre 2022' and p.toponyme_standard_fr is null and p.geom is not null
-- 243

select * 
from ports.ports_nouveaux_v8 p
where new_geogeneral = 'oui' --32
and uhgs_id  in (select uhgs_id from ports.port_points)
-- 29 // 32
select * from port_points pp where uhgs_id in ('A0248501', 'A0380288', 'A0185628');
-- Montalto, Borrowstounness, Moëlan : trois ports déjà traités à ne pas considérer dans les entrées de Silvia.
alter table ports.ports_nouveaux_v8 add column doublon boolean default false;
update ports.ports_nouveaux_v8 set doublon = true where uhgs_id in ('A0248501', 'A0380288', 'A0185628');

select p.uhgs_id, p.toponyme , ggc.*
from ports.ports_nouveaux_v8 p, navigo.geo_general_complet ggc 
where new_geogeneral = 'oui' --32
and uhgs_id  not in (select uhgs_id from ports.port_points)
and p.uhgs_id = ggc.pointcall_uhgs_id 

alter table ports.ports_nouveaux_v8 add column latitude text;
alter table ports.ports_nouveaux_v8 add column longitude text;

update ports.ports_nouveaux_v8 p set latitude = ggc.latitude , longitude = ggc.longitude
from navigo.geo_general_complet ggc
where uhgs_id  not in (select uhgs_id from ports.port_points)
and p.uhgs_id = ggc.pointcall_uhgs_id ;
-- 25

select p.uhgs_id, p.toponyme , p.*
from ports.ports_nouveaux_v8 p
where new_geogeneral = 'oui' and doublon is false 
and p.latitude is null

--A1969635 Bussaglia 42.28142672670764, 8.688200538683539
--A1969636	Porto Rapine 37.885400466439656, 24.0132294563653 Pórto Ráfti / Limin Markopoulou
--A1965341	Risoé en Norvège 58.72122959307206, 9.232822702175216
--A1548964	Scala Nova 37.86992236452521, 27.26546517124274

select * from port_points pp where pp.toponyme_standard_fr = 'Pórto Ráfti'
select * from port_points pp where pp.toponyme_standard_en = 'Porto Rafti'
select * from port_points pp where pp.toponyme_standard_en = 'Limin Markopoulou'

update ports.ports_nouveaux_v8 p set latitude = '42.28142672670764' , longitude = '8.688200538683539'
where p.uhgs_id = 'A1969635' and toponyme = 'Bussagia en Corse';
update ports.ports_nouveaux_v8 p set latitude = '36.539571' , longitude = '22.958583'
where p.uhgs_id = 'A1969636' and toponyme = 'Porto Rapine';
-- correction 20/12/2022

update ports.ports_nouveaux_v8 p set latitude = '58.72122959307206' , longitude = '9.232822702175216'
where p.uhgs_id = 'A1965341' and toponyme = 'Risoé en Norvège';
update ports.ports_nouveaux_v8 p set latitude = '37.86992236452521' , longitude = '27.26546517124274'
where p.uhgs_id = 'A1548964' and toponyme = 'Scala Nova';

-------------
-- traiter les ports déjà présents dans port_points (new_geogeneral != 'oui')
-------------
update ports.port_points p set toponyme_standard_fr = n.toponyme_standard_fr  , toponyme_standard_en = n.toponyme_standard_gb , province =n.province  
from ports.ports_nouveaux_v8 n 
where p.uhgs_id = n.uhgs_id  and n.new_geogeneral is null;
-- 243

insert into labels_lang_csv (label_type, key_id, fr, en) 
select 'toponyme', uhgs_id, toponyme_standard_fr, toponyme_standard_gb
from ports.ports_nouveaux_v8
where new_geogeneral is null and toponyme_standard_fr is not null;
-- 208 / 225
-- toponyme_standard_fr est null quand c'est OpenSea

-------------
-- traiter les etats, tous
insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, n.toponyme_standard_fr , n.toponyme , n.uhgs_id , e.etat, e.subunit, e.dfrom, e.dto, e.geonameid, e.name_en, e.admin1_code, e.latitude, e.longitude, e.tgnid, e.wikipedia, e.etat_en, e.subunit_en
from etats e, ports_nouveaux_v8 n 
where n.etat_like = e.uhgs_id and n.etat_like not in ('A0124817', 'A0122218', 'A0209302', 'A0166649');
-- 268

-- France
select * from ports.etats e where e.uhgs_id = 'A0124817'
select * from ports.etats e where e.uhgs_id = 'A0122218'
select * from ports.etats e where e.uhgs_id = 'A0209302'
select * from ports.etats e where e.uhgs_id = 'A0166649' 
-- Corse
select * from ports.etats e where e.uhgs_id = 'A1968873'; -- OK


-- contredit par mail du 20/12/2022
-- corriger une erreur sur Roquebrune
select etat_like, toponyme, country2019_name  from ports_nouveaux_v8  where uhgs_id in ('A1969424', 'A0195707'); 
-- 'Roquebrune / proche de Monaco' comme Nice A0148992
-- 'Roquebrune / proche de Monaco' comme A0353892
select * from etats e where e.uhgs_id = 'A0353892'

-- correction mail du 20/12/2022 / -- 'Roquebrune / proche de Monaco' comme A0353892 (Monaco)
update ports_nouveaux_v8 set etat_like =  'A0353892', country2019_name  = null where uhgs_id in ('A1969424', 'A0195707');


insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, n.toponyme_standard_fr , n.toponyme , n.uhgs_id , e.etat, e.subunit, e.dfrom, e.dto, e.geonameid, e.name_en, e.admin1_code, e.latitude, e.longitude, e.tgnid, e.wikipedia, e.etat_en, e.subunit_en
from etats e, ports_nouveaux_v8 n 
where n.etat_like = e.uhgs_id and n.etat_like ='A0148992';

delete from etats where uhgs_id in ('A1969424', 'A0195707');
--6

insert into etats (country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en, admin1_code, latitude, longitude, tgnid, wikipedia, etat_en, subunit_en)
select e.country2019_name, n.toponyme_standard_fr , n.toponyme , n.uhgs_id , e.etat, e.subunit, e.dfrom, e.dto, e.geonameid, e.name_en, e.admin1_code, e.latitude, e.longitude, e.tgnid, e.wikipedia, e.etat_en, e.subunit_en
from etats e, ports_nouveaux_v8 n 
where n.etat_like = e.uhgs_id and n.uhgs_id in ('A1969424', 'A0195707');
-- 4

select * from etats where uhgs_id = 'A0195707'


-------------
-- traiter les ports à ajouter port_points 
-- new_geogeneral = 'oui' and doublon = false

-- corriger une erreur : A0220290	Cote des Bouq
update ports.ports_nouveaux_v8 set province = 'Provence' where uhgs_id = 'A0220290' and toponyme = 'Cote des Bouq';

insert into ports.port_points (uhgs_id , latitude , longitude, province, toustopos, toponyme, country2019_name)
select uhgs_id , latitude , longitude ,  province , array[toponyme],  toponyme , country2019_name 
from ports_nouveaux_v8 n
where new_geogeneral = 'oui' and doublon is false;
-- 29
update ports.port_points set insertdate = 'decembre 2022' where uhgs_id in (select uhgs_id from ports_nouveaux_v8 n
where new_geogeneral = 'oui' and doublon is false);

insert into labels_lang_csv (label_type, key_id, fr, en) 
select 'toponyme', uhgs_id, toponyme_standard_fr, toponyme_standard_gb
from ports.ports_nouveaux_v8
where new_geogeneral = 'oui' and doublon is false and toponyme_standard_fr is not null;
-- 29

-- Attention, vérifions que les topo fr et en sont cohérents
update ports.port_points p set toponyme_standard_fr = trim(' ' from toponyme_standard_fr), toponyme_standard_en=trim(' ' from toponyme_standard_en);
-- 1706
update ports.labels_lang_csv p set fr = trim(' ' from fr), en=trim(' ' from en);
-- 1892
select '|'||trim(' ' from fr)||'|' from ports.labels_lang_csv

select key_id, fr, en,  toponyme_standard_fr, toponyme_standard_en 
from (
select c.*, p.toponyme_standard_fr , p.toponyme_standard_en  
from labels_lang_csv c, port_points p
where p.uhgs_id = c.key_id and c.label_type = 'toponyme') as k 
where  k.fr != k.toponyme_standard_fr or k.en != k.toponyme_standard_en;
-- query utilisée pour soumettre les Bizarre à Silvia


select  * from labels_lang_csv where key_id = 'A0220290' and label_type = 'toponyme';
delete from labels_lang_csv where key_id = 'A0220290' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A0220290', 'Port-de-Bouc (sur)', 'Port-de-Bouc (off)'  );

select  * from labels_lang_csv where key_id = 'A1964273' and label_type = 'toponyme';
delete from labels_lang_csv where key_id = 'A1964273' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A1964273', 'île de Riou (à 1 lieue de)', 'île de Riou (off, one league)'  );

select  * from labels_lang_csv where key_id = 'A0333146' and label_type = 'toponyme';
delete from labels_lang_csv where key_id = 'A0333146' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A0333146', 'île de Kéa', 'Kea'  );


select  * from labels_lang_csv where key_id = 'A0081452' and label_type = 'toponyme';
-- Vilagarcía de Arousa	Vilagarcía de Arousa
delete from labels_lang_csv where key_id = 'A0081452' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A0081452', 'Vilagarcía de Arousa', 'Vilagarcía de Arousa'  );


select  * from labels_lang_csv where key_id = 'A0339127' and label_type = 'toponyme';
-- Golfe de Monte-Santo / golfe Singitique	Singitic Gulf
delete from labels_lang_csv where key_id = 'A0339127' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A0339127', 'Golfe de Monte-Santo / golfe Singitique', 'Singitic Gulf'  );

update labels_lang_csv c set fr='Carry-le-Rouet',en='Carry-le-Rouet' 
where c.key_id = 'A1415379' and c.label_type = 'toponyme';

select  * from labels_lang_csv where key_id = 'A1415379' and label_type = 'toponyme';
delete from labels_lang_csv where key_id = 'A1415379' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A1415379', 'Odesse', 'Odessa'  );


select  * from labels_lang_csv where key_id = 'A1409167' and label_type = 'toponyme';

update labels_lang_csv c set fr='Ochakiv',en='Ochakov' 
where c.key_id = 'A1409167' and c.label_type = 'toponyme';

select * from port_points p where toponyme_standard_fr = 'Odessa'; --A1415379
select toponyme_standard_fr, toponyme_standard_en, * from port_points p where toponyme_standard_fr = 'Ochakiv'; -- A1409167

select * from etats  e  where toponyme_standard_fr = 'Odessa';
select * from etats where uhgs_id = 'A1415379';
select * from etats where uhgs_id = 'A1409167';

delete from labels_lang_csv where key_id = 'A1964255' and label_type = 'toponyme';

update labels_lang_csv c set fr='Canal de Jarre',en='Jarre siland (off)' 
where c.key_id = 'A1964255' and c.label_type = 'toponyme';

insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A1964255', 'Canal de Jarre', 'Jarre siland (off)'  );

select * from labels_lang_csv where key_id = 'A1965452' and label_type = 'toponyme';

delete from labels_lang_csv where key_id = 'A1965452' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A1965452', 'embouchure du Rhône (à l''ouest de)', 'Rhone mouth (off, west)'  );

  	
select * from labels_lang_csv where key_id = 'A0152336' and label_type = 'toponyme';
delete from labels_lang_csv where key_id = 'A0152336' and label_type = 'toponyme';
insert into labels_lang_csv (label_type, key_id, fr, en)
values ('toponyme', 'A0152336', 'Pomègues', 'Pomègues'  );

select * from labels_lang_csv where key_id = 'A0196771' and label_type = 'toponyme';
update labels_lang_csv c set fr='Saint-Valery-sur-Somme', en='Saint-Valery-sur-Somme'
where key_id = 'A0196771' and label_type = 'toponyme';


select * from labels_lang_csv where key_id = 'A0196771' and label_type = 'toponyme';
update labels_lang_csv c set fr='Saint-Valery-en-Caux', en='Saint-Valery-en-Caux'
where key_id = 'A0135548' and label_type = 'toponyme';




--Ochakiv et  Odessa
--and p.toponyme_standard_fr != c.fr or p.toponyme_standard_en != c.en 
--and (p.toponyme_standard_fr is not null );

update ports.port_points p set toponyme_standard_fr = c.fr, toponyme_standard_en = c.en
from labels_lang_csv c
where c.label_type = 'toponyme' and key_id = p.uhgs_id and key_id in (select uhgs_id from ports.ports_nouveaux_v8
where new_geogeneral = 'oui' and doublon is false and toponyme_standard_fr is not null)
-- 29

update ports.port_points p set toponyme_standard_fr = c.fr, toponyme_standard_en = c.en
from labels_lang_csv c
where c.label_type = 'toponyme' and key_id = p.uhgs_id 
and key_id in ('A0196771', 'A0333146', 'A0339127', 'A0081452', 'A1964255', 
'A1965452', 'A1964273', 'A0152336', 'A0333146', 'A1409167', 'A1415379', 'A0220290', 'A1964273', 'A0135548');

select key_id, fr, en,  toponyme_standard_fr, toponyme_standard_en 
from (
select c.*, p.toponyme_standard_fr , p.toponyme_standard_en  
from labels_lang_csv c, port_points p
where p.uhgs_id = c.key_id and c.label_type = 'toponyme') as k 
where  k.fr != k.toponyme_standard_fr or k.en != k.toponyme_standard_en;
-- query pour controle le 26 décembre 2022 : plus de BIzarre.



---------------
update ports.port_points pp set shiparea = ps.shiparea 
from ports.pointcall_uhgs_coding ps
where ps."Pointcall_UHGS_id"  = pp.uhgs_id and pp.insertdate = 'decembre 2022' and ps.shiparea != '';
-- 0

update ports.port_points set topofreq = ports.frequency_topo(uhgs_id, array_to_string(toustopos, ',')) 
--select ports.frequency_topo(uhgs_id, array_to_string(toustopos, ','))  from ports.port_points
where array_length(toustopos, 1) > 0 and ports.test_double_type(longitude) is true and topofreq is null and insertdate = 'decembre 2022';
-- 29

select * from ports.port_points where insertdate = 'decembre 2022';

-- Mail de Silvia du 20/12/2022
update ports.port_points set longitude='22.958583', latitude='36.539571' 
where uhgs_id = 'A1969636' and toponyme = 'Porto Rapine';
update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where uhgs_id = 'A1969636' and toponyme = 'Porto Rapine';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857)  where uhgs_id = 'A1969636' and toponyme = 'Porto Rapine';

select * from ports.port_points where topofreq is null and insertdate = 'decembre 2022';
update ports.port_points set toponyme = (topofreq::json->>0)::json->>'topo' where toponyme is null and insertdate = 'decembre 2022'
-- 0

update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where insertdate = 'decembre 2022';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857) where insertdate = 'decembre 2022'; 

select * from ports.port_points where insertdate = 'decembre 2022';-- 30

select * from ports.port_points where insertdate = 'novembre 2022'; -- 243

select * from ports.port_points where belonging_states is null ; -- 278 / 35


-------------------------------------
-- suite

-- 20/12/2022 : refaire l'appartenance aux pays actuels en corrigeant le buffer à 50 km
update ports.port_points ports set country2019_name = null, country2019_iso2code = null, country2019_region = null
where insertdate in ('novembre 2022', 'decembre 2022');

update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , p.point3857) and p.country2019_name is null and insertdate in ('novembre 2022', 'decembre 2022') and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate in ('novembre 2022', 'decembre 2022') ;
-- 152
       
update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 5000)) and p.country2019_name is null and insertdate in ('novembre 2022', 'decembre 2022') and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate in ('novembre 2022', 'decembre 2022') ;
-- 74
       
update ports.port_points ports set country2019_name = k.name, country2019_iso2code=k.iso2, country2019_region=k.region
        from (
        select uhgs_id, toponyme, shiparea, iso2, name, region 
        from ports.port_points p , ports.world_borders w
        where st_intersects(w.mpolygone3857 , st_buffer(p.point3857, 50000)) and p.country2019_name is null and insertdate in ('novembre 2022', 'decembre 2022') and point3857 is not null
        ) as k
        where ports.uhgs_id = k.uhgs_id and country2019_name is null and insertdate in ('novembre 2022', 'decembre 2022') ;
-- 33
       
select * from ports.port_points p, ports_nouveaux_v8 pnv 
where insertdate in ('novembre 2022', 'decembre 2022') and p.country2019_name is null 
and p.uhgs_id = pnv.uhgs_id ;
-- A0220290	Cote des Bouq -- le reste   Open sea
select p.uhgs_id, p.amiraute , p.toponyme , p.toponyme_standard_fr , p.country2019_name , pnv.country2019_name, pnv.new_geogeneral, pnv."delete" , pnv.doublon  from ports.port_points p, ports_nouveaux_v8 pnv 
where insertdate in ('novembre 2022', 'decembre 2022') and p.country2019_name is not null and p.country2019_name != pnv.country2019_name 
and p.uhgs_id = pnv.uhgs_id ;   
-- A1367884		Bouches de Cataro	Bouches de Kotor	Croatia	Montenegro : Silvia a raison (Montenegro)

-- A0128592		sarde	Sarde	France	Italy : Silvia a faux
-- A1968748		Grec et Sujet du Grand Seigneur	Grec ottoman	Turkey	Greece : Silvia a faux
-- A1969438		Golfe de Cazadaly	Golfe de Cazadaly	Greece	Turkey : Silvia a faux

update ports.port_points p set country2019_name = w.name, country2019_iso2code=w.iso2, country2019_region=w.region
from ports.world_borders w
where w."name" = 'Montenegro' and p.uhgs_id = 'A1367884'; -- Bouches de Cataro

update ports.port_points p set country2019_name = w.name, country2019_iso2code=w.iso2, country2019_region=w.region
from ports.world_borders w
where w."name" = 'France' and p.uhgs_id = 'A0220290'; -- A0220290	Cote des Bouq

-- select toponyme, country2019_name from ports.port_points where toponyme in ('Roussillon', 'Argelès', 'Barneville', 'Bagniol', 'Piron');
-- France pour tous après la correction du 20/12/2022 (inutile)

-- Choix de Christine (20/12/2022) : à la place de OpenSea, mettre un country si possible, car dans les eaux territoriales (moins de 50 km)
-- Et donc affecter une amirauté/province si au large de la France
-- choix différent de Silvia. Mais si un navire coule au large de Monaco, alors qu'il s'y rendait, c'est bien de la valeur de commerce pour Monaco qui est perdue. 

select toponyme , toponyme_standard_fr , toponyme_standard_en , substate_1789_fr , state_1789_fr  from ports.port_points p where uhgs_id = 'A0249463'
update ports.port_points set toponyme_standard_fr='Porto Santo Stefano', toponyme_standard_en='Porto Santo Stefano' where uhgs_id = 'A0249463';
update ports.labels_lang_csv  set fr='Porto Santo Stefano', en='Porto Santo Stefano' where key_id  = 'A0249463' and label_type = 'toponyme';

select toponyme , toponyme_standard_fr , toponyme_standard_en , substate_1789_fr , state_1789_fr  , substate_1789_en , state_1789_en
from ports.port_points p where uhgs_id = 'B2125493';

select toponyme , toponyme_standard_fr , toponyme_standard_en , substate_1789_fr , state_1789_fr  , substate_1789_en , state_1789_en
from ports.port_points p where uhgs_id = 'A0676229';
update ports.port_points set toponyme_standard_fr='Mecklembourg' where uhgs_id = 'A0676229';
update ports.labels_lang_csv  set fr='Mecklembourg' where key_id  = 'A0676229' and label_type = 'toponyme';

select * from ports.port_points p where uhgs_id = 'A0395415';
------------------------------
-- le 20/12/2022 - Reprise des appartenances d'états
-- Fichier ports_nouveaux_v8_8nov2022_cpp.21.12.2022_UTF8.csv
-- import dans la table etats et renommer l'autre table etats 
-------------

alter table ports.etats rename to etats_v7
drop table ports.etats;
CREATE TABLE ports.etats (
	Etats_like text NULL,
	country2019_name text NULL,
	toponyme_standard_fr text NULL,
	toponyme text NULL,
	uhgs_id text NULL,
	etat text NULL,
	subunit text NULL,
	dfrom text NULL,
	dto text NULL,
	geonameid text NULL,
	name_en text NULL,
	admin1_code text NULL,
	latitude text NULL,
	longitude text NULL,
	tgnid text NULL,
	wikipedia text NULL,
	admin2_code text NULL,
	admin3_code text NULL,
	etat_en text NULL,
	subunit_en text NULL
);

sudo -u postgres psql -p 5432 -d portic_v8 -U postgres

\COPY ports.etats FROM /home/plumegeo/portic/portic_V8/etats_v8_8nov2022_21dec2022_UTF8.csv delimiter ';' CSV header
--  ports_nouveaux_v8_8nov2022_cpp.21.12.2022_UTF8 renommé en etats_v8_8nov2022_21dec2022_UTF8.csv sur le server
-- COPY  1661 en v8

-- reconvertir en supprimant les espaces
select replace(dfrom, ' ', '')::int , replace(dto, ' ', '')::int , replace(geonameid, ' ', '')::int,
replace(latitude, ',', '.')::float , replace(longitude, ',', '.')::float, replace(tgnid, ' ', '')::int
from ports.etats

alter table ports.etats alter column dfrom type int using replace(dfrom, ' ', '')::int;
alter table ports.etats alter column dto type int using replace(dto, ' ', '')::int;
alter table ports.etats alter column geonameid type int using replace(geonameid, ' ', '')::int;
alter table ports.etats alter column tgnid type int using replace(tgnid, ' ', '')::int;
alter table ports.etats alter column latitude type float using replace(latitude, ',', '.')::float;
alter table ports.etats alter column longitude type float using replace(longitude, ',', '.')::float;

alter table ports.etats drop column etats_like;

select count(*) from ports.etats --1661
select count(*) from ports.etats_v7 --1800

select * from ports.etats_v7
where uhgs_id not in (select uhgs_id  from ports.etats) 
-- and country2019_name = 'Italy' 
order by uhgs_id, dto;
-- exportés dans C:\Travail\Data\BDD-backups\portic\data_06nov2022\etats_portic_v7_pasfait21dec2022.csv en UTF8

select * from ports.etats_v7
where uhgs_id not in (select uhgs_id  from ports.etats) 
and subunit = 'Sicile'
order by uhgs_id, dto;

alter table ports.etats add column checked boolean default true;
insert into ports.etats (checked, country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en,
admin1_code, latitude, longitude, tgnid, wikipedia, admin2_code, admin3_code, etat_en, subunit_en)
(select false, country2019_name, toponyme_standard_fr, toponyme, uhgs_id, etat, subunit, dfrom, dto, geonameid, name_en,
admin1_code, latitude, longitude, tgnid, wikipedia, admin2_code, admin3_code, etat_en, subunit_en
from ports.etats_v7 where uhgs_id not in (select uhgs_id  from ports.etats) order by uhgs_id, dto);

update ports.etats set dto = 1806 
where subunit = 'Sicile' and checked is false; --9

insert into ports.etats (checked, country2019_name, toponyme_standard_fr, toponyme, uhgs_id, 
etat, subunit, dfrom, dto, geonameid, name_en,
admin1_code, latitude, longitude, tgnid, wikipedia,  etat_en)
(select false, country2019_name, toponyme_standard_fr, toponyme, uhgs_id,
'Royaume de Sicile', null, 1806 , 1815, 2523119,	'Sicilia',	
4,	37.75,	14.25,	7594681,	'https://fr.wikipedia.org/wiki/Royaume_de_Sicile','Kingdom of Sicily'
from ports.etats where subunit = 'Sicile' and checked is false)


select * from ports.etats 
where checked is false
order by uhgs_id, dto;

-- Doublon ici A0219727
 
select * from ports.etats e where uhgs_id = 'A0219727';
delete from ports.etats e where uhgs_id = 'A0219727';

insert into ports.etats (checked, country2019_name, toponyme_standard_fr, toponyme, uhgs_id, 
etat, subunit, dfrom, dto, geonameid, name_en,
admin1_code, latitude, longitude, etat_en, subunit_en)
values (false, 'Italy', 'Impérial (méditérranen)', 'Imperial [Mediterranean', 'A0219727',
'Autriche', 'Autriche méditerranéenne', null , null, 2782113,	'Republic of Austria',	
'00',	47.33333,	13.33333, 'Austria',	'Austria - Mediterranean ports'	);

-- D'après Silvia, pas d'erreur. Donc je mets checked à true
-- update ports.etats set checked = true; --je ne le fais pas le 26 dec 2022, au cas où

-- update ports.port_points set country2019_name = 'France' where toponyme in ('Roussillon', 'Argelès', 'Barneville', 'Bagniol', 'Piron');
-- en fluo jaune et France dans le fichier de Silvia, inutile après correction country2019_name le 20/12/2022
select * from etats e where subunit = 'Sardaigne';
-- pour tous les ports de Sardaigne, mettre 1749	1815 à null
update etats e set dfrom = null, dto=null where subunit = 'Sardaigne' and dfrom = 1749 and dto=1815; --24
update etats e set latitude = 40, longitude=9, admin1_code = 14, name_en='Sardegna' where subunit = 'Sardaigne' ; --32

-- tous les ports francais nouveau     
update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
            from (
                select  uhgs_id, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'France')::text as appartenances
                from ports.port_points 
                where country2019_name = 'France' and uhgs_id not in (select uhgs_id from ports.etats e)
                union 
                (select 'A0146289' as uhgs_id, json_build_object(1749 ||'-'||1815, 'Iceland')::text as appartenances_en, json_build_object(1749 ||'-'||1815, 'Islande')::text as appartenances)
            ) as k 
        where pp.uhgs_id = k.uhgs_id  and belonging_states is null;
-- 46 + 5 / 519

-- il ya des ports aujourd'hui francais qui ne l'étaient pas avant comme Roquebrune,
update ports.port_points pp set belonging_states = '['||k.appartenances ||']', belonging_states_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, etat, case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), etat_en) :: text as belonging_en
                from ports.etats 
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id and belonging_states is null;
-- 192 / 1188

select count(*) from ports.port_points where belonging_substates is null ; -- 1138 / 1073


update ports.port_points pp set belonging_substates = '['||k.appartenances ||']', belonging_substates_en = '['||k.appartenances_en ||']'
        from (
            select uhgs_id, STRING_AGG(belonging, ',') as appartenances, STRING_AGG(belonging_en, ',') as appartenances_en
            from (
                select  uhgs_id, subunit,  case when dfrom is null then 1749 else dfrom end as orderingdate, 
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit) :: text as belonging,
                json_build_object((case when dfrom is null then 1749 else dfrom end)||'-'||(case when dto is null then 1815 else dto end), subunit_en) :: text as belonging_en
                from ports.etats 
                WHERE subunit IS NOT null
                order by uhgs_id, orderingdate
            ) as k 
            group by uhgs_id
        ) as k
        where pp.uhgs_id = k.uhgs_id and belonging_substates is null;
-- 65 / 632 / 658
       
update ports.port_points set state_1789_fr = ports.extract_state_fordate(belonging_states, 1789) 
where belonging_states is not null  and state_1789_fr is null; 
-- 243 / 1671
           
select state_1789_fr, country2019_name , amiraute , toponyme , toponyme_standard_fr 
from ports.port_points where    state_1789_fr is not null and insertdate in ('novembre 2022', 'decembre 2022')
order by country2019_name; --243

update ports.port_points set state_1789_en = ports.extract_state_en_fordate(belonging_states_en, 1789) 
            where belonging_states_en is not null and state_1789_en is null;   
-- 243 / 1671 
-- fonction qui a du être installée (absente, je ne sais pourquoi)
       CREATE OR REPLACE FUNCTION ports.extract_substate_fordate (tested_value VARCHAR, dateparam int) RETURNS VARCHAR AS 
        $BODY$
        declare
        	state varchar;
        begin
	            
	            execute 'select substring(states::text from 2 for char_length(states::text)-2)  from (
				select  toponyme, uhgs_id, json_object_keys(elt::json) as dates, (elt::json)->json_object_keys(elt::json) as states
					from 
					(
					select  toponyme, uhgs_id, json_array_elements(belonging_substates::json) as elt  
					from ports.port_points p
					where belonging_substates = '||quote_literal(tested_value)||'
				) as q
				) as k
				where substring(dates for 4)::int <= '||quote_literal(dateparam)||'::int and substring(dates from 6 for 4)::int >= '||quote_literal(dateparam)||'::int '
				INTO state;

                --EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return state;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE; 
       
update ports.port_points set substate_1789_fr = ports.extract_substate_fordate(belonging_substates, 1789) 
where belonging_substates is not null and substate_1789_fr is null;
-- 77 / 660

update ports.port_points set substate_1789_en = ports.extract_substate_en_fordate(belonging_substates_en, 1789) 
where belonging_substates_en is not null  ;
-- 633 / 660

select * from  ports.port_points  where substate_1789_en='false' or state_1789_en = 'false' or state_1789_fr = 'false';--0


update ports.port_points pp set relation_state = k.appartenances
                from (
                select uhgs_id, json_agg(arelation) as appartenances
                    from
                    (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                    case when geonameid is not null then 'http://www.geonames.org/'||geonameid else 'http://vocab.getty.edu/tgn/'||tgnid end,
                    'label', etat, 'when', json_build_object('timespans', json_agg(intervalle))) as arelation
                    from (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in',dto))   as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is not null
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in',dfrom), 'end', json_build_object('in','*'))  as intervalle
                        from ports.etats 
                        where dfrom is not null and dto is null
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in',dto)) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is not null 
                        )
                        union all
                        (
                        select  uhgs_id, etat, geonameid, tgnid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                        from ports.etats 
                        where dfrom is null and dto is  null 
                        )
                    ) as k 
                    group by uhgs_id, etat, geonameid, tgnid
                    ) as k
                    group by uhgs_id
                    order by uhgs_id
                ) as k
                where pp.uhgs_id = k.uhgs_id and relation_state is null;
-- 192 / 1188

-- France et Iceland
update ports.port_points pp set relation_state = k.appartenances ::text
            from (
            select uhgs_id,  json_agg(arelation) as appartenances
                from
                (select uhgs_id, etat, json_build_object('relationType', 'gvp:broaderPartitive', 'relationTo', 
                'http://www.geonames.org/'||geonameid ,
                'label', etat, 'when', json_build_object('timespans', json_agg (intervalle))) as arelation
                from (
                    
                    select  uhgs_id, 'France' as etat, 3017382 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    from ports.port_points 
                    where  country2019_name = 'France'
                    and uhgs_id not in (select uhgs_id from ports.etats e)
                    union all
                    (
                    select  'A0146289' as uhgs_id, 'Iceland' as etat, 2629691 as geonameid, json_build_object('start', json_build_object('in','*'), 'end', json_build_object('in','*')) as intervalle
                    )
                ) as k 
                group by uhgs_id, etat, geonameid
                ) as k
                group by uhgs_id
                order by uhgs_id
            ) as k
            where pp.uhgs_id = k.uhgs_id and relation_state is null ;
-- 51 / 519 / 483 après correction qui enlève les ports français à l'étranger dans le passé

select relation_state , toponyme, uhgs_id , insertdate
from port_points pp where substring (relation_state from 1 for 1)='[';

-- supprimer des [] sur relation_state
update ports.port_points set relation_state = substring(relation_state from 2 for length(relation_state)-2) where substring (relation_state from 1 for 1)='[';
-- 1670

------------------------
-- Traiter ici les ports pour la mise à jour des 
-- shiparea
-- provinces amirauté  
-- ferme	bureau	
-- ferme_direction_uncertainty ferme_bureau_uncertainty
-- partner_balance_1789  partner_balance_supp_1789
-- partner_balance_1789_uncertainty partner_balance_supp_1789_uncertainty
-- Si en France
-- partner_balance_supp_1789='France', partner_balance_supp_1789_uncertainty=0,
-- partner_balance_1789=null, partner_balance_1789_uncertainty=null,

update ports.port_points set amiraute = 'Toulon', shiparea ='MED-LIGS' where uhgs_id ='A0178523';

select toponyme_standard_fr , * from  ports.port_points 
where insertdate in ('novembre 2022', 'decembre 2022')
and state_1789_fr = 'France'
order by amiraute ;

update port_points p set toponyme_standard_fr = 'Bussaglia', toponyme_standard_en = 'Bussaglia' where uhgs_id = 'A1969635'
select * from ports.labels_lang_csv c where c.label_type = 'toponyme' and key_id = 'A1969635';

select toponyme_standard_fr, * from port_points pp where pp.uhgs_id = 'A0037189'
-- uhgs_id		amiraute Province shiparea	Ferme_direction	ferme_bureau ferme_bureau_uncertainty
update port_points set amiraute = 'Collioure', province = 'Roussillon', shiparea='MED-LION', ferme_direction='Narbonne', ferme_bureau = 'Collioure', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0205843', 'A0145441', 'A0159449');

update port_points set amiraute = 'Agde', province = 'Languedoc', shiparea='MED-LION', ferme_direction='Montpellier', ferme_bureau = 'Agde', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A1964989');
update port_points set amiraute = 'Sète', province = 'Languedoc', shiparea='MED-LION', ferme_direction='Montpellier', ferme_bureau = 'Sète', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A1965452');
update port_points set amiraute = 'Arles', province = 'Provence', shiparea='MED-LION', ferme_direction='Marseille', ferme_bureau = 'Arles', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0220290');
update port_points set amiraute = 'Martigues', province = 'Provence', shiparea='MED-LION', ferme_direction='Marseille', ferme_bureau = 'Arles', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0154590');
update port_points set amiraute = 'Aigues-Mortes', province = 'Languedoc', shiparea='MED-LION', ferme_direction='Montpellier', ferme_bureau = 'Montpellier', ferme_bureau_uncertainty = -1, partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0183438');

update port_points set amiraute = 'Marseille', province = 'Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau = 'Marseille', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0169168', 'A0150005', 'A0163296', 'A0177113', 'A1965189', 'A1964255', 'A1964273', 'A0210083', 'A1969434', 'A1968874', 'A1969433', 'A0133301', 'A0200683', 'A0152336');

update port_points set amiraute = 'La Ciotat', province = 'Provence', shiparea='MED-LIGS', ferme_direction='Marseille', ferme_bureau = 'La Ciotat', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0216933');

update port_points set amiraute = 'Toulon', province = 'Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau = 'Toulon', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0143491', 'A1969429');

update port_points set amiraute = 'Saint-Tropez', province = 'Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau = 'Saint-Tropez', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0127905', 'A0156440', 'A0165679', 'A1965243');

update port_points set amiraute = 'Antibes', province = 'Provence', shiparea='MED-LIGS', ferme_direction='Toulon', ferme_bureau = 'Cannes', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A1969427');


select * from port_points where uhgs_id in ('A1969424', 'A0195707'); 
update port_points set province = null where uhgs_id = 'A0195707';

select * from ports.ports_nouveaux_v8 pnv where uhgs_id = 'A0178523'  -- Hyères mal placé
-- à supprimer A0178523

-- corriger lat et long 43.08613645952041, 6.150292341697001 de hyères
select * from ports.ports_nouveaux_v8 pnv where uhgs_id = 'A1964985' -- Hyères mal placé
update port_points set latitude = '43.08613645952041', longitude = '6.150292341697001' where uhgs_id = 'A1964985'
update ports.port_points set geom = st_setsrid(st_makepoint(longitude::float, latitude::float), 4326) where uhgs_id = 'A1964985';
update ports.port_points set point3857 = st_setsrid(st_transform(geom, 3857) , 3857) where uhgs_id = 'A1964985'; 

-- uhgs_id shiparea partner_balance_1789 partner_balance_supp_1789
update port_points set  shiparea='MED-BALN', partner_balance_1789='Espagne', partner_balance_supp_1789 = 'Etranger'
where uhgs_id in ('A1969426', 'A0116876', 'A1969357', 'A1969359');

update port_points set  shiparea='MED-LIGS',  partner_balance_supp_1789 = 'Etranger'
where uhgs_id in ('A0195707', 'A1969424', 'A0231670', 'A0234305', 'A0233524', 'A0230039', 'A0219524', 'A0230039', 'A0251561', 'A0247200', 'A0236722');

update port_points set  shiparea='MED-TYNO',  partner_balance_supp_1789 = 'Etranger'
where uhgs_id in ('A1969425', 'A1964258', 'A0230288');

-- Corse
update port_points set amiraute= 'Ajaccio', province = 'Corse', shiparea='MED-CORS',  partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0194925', 'A0148968', 'A1969432');
update port_points set amiraute= 'Bastia', province = 'Corse', shiparea='MED-TYNO',  partner_balance_supp_1789 = 'France'
where uhgs_id in ('A1969355', 'A0208969');
update port_points set amiraute= 'Bastia', province = 'Corse', shiparea='MED-CORS',  partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0134678', 'A0194462', 'A1969635', 'A0172784');


-- France atlantique
update port_points set amiraute = 'Lorient', province = 'Bretagne', shiparea='ACE-IROI', ferme_direction='Lorient', ferme_bureau = 'Lorient', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0130581');
update port_points set amiraute = 'Quimper', province = 'Bretagne', shiparea='ACE-IROI', ferme_direction='Lorient', ferme_bureau = 'Quimper', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0188937');

-- Nprmandie
update port_points set amiraute = 'Coutances', province = 'Normandie', shiparea='MAN-PORT', ferme_direction='Caen', ferme_bureau = 'Granville', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0139550', 'A1965136', 'A0203931', 'A1964124', 'A0136297');

update port_points set amiraute = 'Port-Bail', province = 'Normandie', shiparea='MAN-WIGH', ferme_direction='Caen', ferme_bureau = 'Cherbourg', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0205396');

update port_points set amiraute = 'Cherbourg', province = 'Normandie', shiparea='MAN-WIGH', ferme_direction='Caen', ferme_bureau = 'Cherbourg', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0190737', 'A0140454');

update port_points set amiraute = 'Caen', province = 'Normandie', shiparea='MAN-WIGH', ferme_direction='Caen', ferme_bureau = 'Caen', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0172791');

update port_points set amiraute = 'Caudebec et Quilleboeuf', province = 'Normandie', shiparea='MAN-WIGH', ferme_direction='Rouen', ferme_bureau = 'Caudebec', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0172233');-- Sainte croix

update port_points set amiraute = 'Caudebec et Quilleboeuf', province = 'Normandie', shiparea='MAN-WIGH', ferme_direction='Rouen', ferme_bureau = 'Caudebec', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0184606');-- Sainte croix doublon

update port_points set amiraute = 'Saint-Valéry-en-Caux', province = 'Normandie', shiparea='MAN-WIGH', ferme_direction='Rouen', ferme_bureau = 'Saint-Valéry-en-Caux', partner_balance_supp_1789 = 'France'
where uhgs_id in ('A0126014', 'A0138054');

-- Martinique
update port_points set amiraute = 'Fort-Royal', province = 'Martinique', shiparea='GUL-LEEW', partner_balance_1789 = 'Iles françaises de l''Amérique', partner_balance_supp_1789 = 'colonies françaises'
where uhgs_id in ('B1973619');

-- concat(toponyme, concat ('; ',concat(concat( concat(concat( "amiraute", ' - ' ),  "province" ), '\n'),  "shiparea" )))

-- verif des partner, etat par état

select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , province
from ports.port_points where province = 'Saint-Domingue' ; 
select * from ports.port_points where uhgs_id = 'B2116612';--Gaspésie
select * from ports.etats where uhgs_id = 'B1973619';-- La Trinité Isle de la Martinique
select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , province
from ports.port_points where uhgs_id = 'B2116612' ; 
select * from etats e where e.uhgs_id = 'B2116612'

select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , 
partner_balance_1789_uncertainty , partner_balance_supp_1789_uncertainty 
from ports.port_points where state_1789_fr in ('Autriche', 'Duché de Mecklenbourg', 'Duché d''Oldenbourg') 
order by state_1789_fr;
update ports.port_points set partner_balance_1789 = 'Etats de l''Empereur' ,  partner_balance_1789_uncertainty=-1, 
partner_balance_supp_1789= 'Etranger', partner_balance_supp_1789_uncertainty=0
where state_1789_fr in ('Autriche', 'Duché de Mecklenbourg', 'Duché d''Oldenbourg');


select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , partner_balance_supp_1789_uncertainty 
from ports.port_points where province = 'Saint-Domingue' 
order by toponyme_standard_fr;

UPDATE ports.port_points SET partner_balance_1789='Saint-Domingue', partner_balance_1789_uncertainty=0 ,
partner_balance_supp_1789= 'colonies françaises', partner_balance_supp_1789_uncertainty=0
WHERE province = 'Saint-Domingue' ;
-- 16

select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , 
partner_balance_1789_uncertainty, partner_balance_supp_1789_uncertainty 
from ports.port_points where amiraute = 'Port-Louis (Tobago)' 
order by toponyme_standard_fr;

UPDATE ports.port_points SET partner_balance_1789='Iles françaises de l''Amérique', 
partner_balance_supp_1789='colonies françaises',
partner_balance_1789_uncertainty=0 , partner_balance_supp_1789_uncertainty=0
WHERE amiraute = 'Port-Louis (Tobago)' ;

select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , 
partner_balance_1789_uncertainty, partner_balance_supp_1789_uncertainty 
from ports.port_points where state_1789_fr in ('Empire ottoman', 'Malte')
order by toponyme_standard_fr;

UPDATE ports.port_points SET partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0 
WHERE state_1789_fr in ('Empire ottoman', 'Malte');


select  toponyme_standard_fr , partner_balance_1789, partner_balance_supp_1789, state_1789_fr , 
partner_balance_1789_uncertainty , partner_balance_supp_1789_uncertainty 
from ports.port_points where state_1789_fr in ('Grande-Bretagne')  
--or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Dantzig')
order by state_1789_fr;


update ports.port_points set partner_balance_1789 = 'Prusse' ,  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Prusse'); --21
        
update ports.port_points set partner_balance_1789 = 'Quatre villes hanséatiques' , partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Hambourg', 'Lubeck', 'Brême') or uhgs_id in (select uhgs_id from ports.port_points where toponyme_standard_fr % 'Dantzig');
--4
update ports.port_points set partner_balance_1789 = 'Etats-Unis' ,  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr in ('Etats-Unis d''Amérique');  --43 
update ports.port_points set partner_balance_1789='Angleterre',  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Grande-Bretagne' ;--260
update ports.port_points set partner_balance_1789='Danemark', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Danemark';--67
update ports.port_points set partner_balance_1789='Espagne',  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Espagne';--127
update ports.port_points set partner_balance_1789='Hollande', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr = 'Provinces-Unies'; --76
update ports.port_points set partner_balance_1789='Iles françaises de l''Amérique', partner_balance_1789_uncertainty=0
        where substate_1789_fr  = 'Colonies françaises d''Amérique'; --31       
update ports.port_points set partner_balance_1789='Portugal',  partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  = 'Portugal'; --19
update ports.port_points set partner_balance_1789='Russie', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  = 'Empire russe';--12
update ports.port_points set partner_balance_1789='Suède', partner_balance_1789_uncertainty=0,
partner_balance_supp_1789='Etranger', partner_balance_supp_1789_uncertainty=0
        where state_1789_fr  = 'Suède';--39
update ports.port_points set partner_balance_1789='Bayonne', partner_balance_1789_uncertainty=0 where uhgs_id = 'A0187995';
update ports.port_points set partner_balance_1789='Dunkerque',  partner_balance_1789_uncertainty=0 where uhgs_id = 'A0204180';       
update ports.port_points set partner_balance_1789='Marseille',  partner_balance_1789_uncertainty=0 where uhgs_id = 'A0210797';      
update ports.port_points set partner_balance_1789='Lorient', partner_balance_1789_uncertainty=0 where uhgs_id = 'A0140266' ;
update ports.port_points set partner_balance_1789='Petites Iles', partner_balance_1789_uncertainty=-1 where uhgs_id in ('A0165077', 'A0136403');
update ports.port_points set partner_balance_1789='Saint-Domingue',  partner_balance_1789_uncertainty=0 where province % 'Domingue' ;        
update ports.port_points set partner_balance_1789='Saint-Jean de Luz',  partner_balance_1789_uncertainty=-1 where uhgs_id = 'A0122594';

        

select toponyme_standard_fr , uhgs_id , substate_1789_fr, state_1789_en ,
partner_balance_1789, partner_balance_supp_1789 , 
partner_balance_1789_uncertainty, partner_balance_supp_1789_uncertainty
from ports.port_points where state_1789_fr != 'France' and province is not null;
-- ok pour les ports anglais

update ports.port_points set shiparea = 'NOR-THAM' where uhgs_id in ('A0632714', 'A0631608', 'A0634886', 'A0633324', 'A0619074', 'A0398981', 'A0385403', 'A1968898') ;
update ports.port_points set shiparea = 'NOR-GBIG' where uhgs_id in ('A1968907', 'A1968861', 'A0799311', 'A0650841') ;
update ports.port_points set shiparea = 'BAL-BELT' where uhgs_id in ('A0826018', 'A0821280') ;
update ports.port_points set shiparea = 'BAL-BAWE' where uhgs_id in ('A0753723', 'A0821752', 'A0676229') ;
update ports.port_points set shiparea = 'BAL-BASE' where uhgs_id in ('A1017571', 'A1156318') ;
update ports.port_points set shiparea = 'BAL-FINL' where uhgs_id in ('A0973559', 'A1813720', 'A1813720', 'A1770543') ;
update ports.port_points set shiparea = 'BAL-BOTS' where uhgs_id in ('A0921408', 'A1048030', 'A1087555', 'A0927510', 'A0921408') ;
update ports.port_points set shiparea = 'MAN-PORT' where uhgs_id in ('A0404560', 'A0388384', 'A0408296', 'A0403027');
update ports.port_points set shiparea = 'MAN-PLYM' where uhgs_id in ('A0400237', 'A0391498');
update ports.port_points set shiparea = 'ANE-LUND' where uhgs_id in ('A0393369');
update ports.port_points set shiparea = 'ANE-IRSE' where uhgs_id in ('A0382274', 'A0383580');
update ports.port_points set shiparea = 'NOR-FORH' where uhgs_id in ('A0401257', 'A0402382', 'A0401154', 'A0410926', 'A0405889');
update ports.port_points set shiparea = 'ANE-FAST' where uhgs_id in ('A0611715');

update ports.port_points set shiparea = 'NOR-UTSN' where uhgs_id in ('A0907996');
update ports.port_points set shiparea = 'ACE-PORT' where uhgs_id in ('A0355789', 'A0354857');
update ports.port_points set shiparea = 'ACE-CADI' where uhgs_id in ('A0355946', 'A0373585', 'A0072993', 'A1670806', 'A1630692');
update ports.port_points set shiparea = 'MED-ALBO' where uhgs_id in ('A1969440', 'A0101305', 'A0103058', 'A0102158', 'A0081001', 'A0071603', 'A0073282', 'A0073198');
update ports.port_points set shiparea = 'ACE-AGAD' where uhgs_id in ('A1646638');
update ports.port_points set shiparea = 'ACE-MADE' where uhgs_id in ('A0361316', 'A0359243');
update ports.port_points set shiparea = 'MED-BALS' where 
uhgs_id in ('A1969437', 'A0077990', 'A0276216', 'A1969439', 'A1965240', 'A0310256', 'A0117646', 'A1964250', 'A1965021', 
'A0106586', 'A0112914', 'A0098949', 'A0086954', 'A0097425', 'A1964261', 'A1964164', 'A1967566');
update ports.port_points set shiparea = 'MED-BALN' where  uhgs_id in ('A0069484', 'A0118479', 'A0085642', 'A0078125', 'A0076325', 'A1965192',
'A1969428', 'A0073149', 'A0069664', 'A0116027', 'A0119937', 'A0074288', 'A0086026');

update ports.port_points set shiparea = 'MED-SARS' where  uhgs_id in ('A0221982', 'A0229236', 'A0230168', 'A0251359', 'A0224526', 'A0225413');
update ports.port_points set shiparea = 'MED-TYWC' where  uhgs_id in ('A0247240', 'A0223331', 'A0221620', 'A0222006', 'A0225432');
update ports.port_points set shiparea = 'MED-MEDE' where  uhgs_id in ('A1492510', 'A1902798', 'A0411800');
update ports.port_points set shiparea = 'BSE-STRA' where  uhgs_id in ( 'A1968748', 'A1470796');
update ports.port_points set shiparea = 'MED-AEGN' where  uhgs_id in ( 'A1528139', 'A0344481', 'A1483064', 'A0340497', 'A0328709', 'A0333807', 'A0333881', 'A0333912', 'A1969356', 'A0334008', 'A0334025', 'A0334037', 'A0339596', 'A1969546', 'A1472455', 'A1969438', 'A1465534', 'A0352677');
update ports.port_points set shiparea = 'MED-AEGS' where  uhgs_id in ( 'A0324726' ,'A1969431', 'A1969636', 'A0324963', 'A0325257', 'A0327198', 'A0345910', 'A0345674', 'A0343217', 'A0343014', 'A0340367', 'A0332316', 'A0340367', 'A0344167', 'A0344046', 'A0332316', 'A1548964', 'A0347746', 'A0346870');

update ports.port_points set shiparea = 'MED-IONE' where  uhgs_id in ('A0310406', 'A0315703' ,'A0310936', 'A0327753', 'A0315211', 'A1969547');
update ports.port_points set shiparea = 'MED-IONN' where  uhgs_id in ('A0231692', 'A0242462', 'A0230211', 'A0252570', 'A1969358', 'A0231581', 'A0235114' ,'A0223462');
update ports.port_points set shiparea = 'MED-IONS' where  uhgs_id in ('A1963886');
update ports.port_points set shiparea = 'MED-SIST' where  uhgs_id in ('A0027865', 'A1969436','A0221526', 'A0237249', 'A0037189', 'A0036064', 'A1969306');
update ports.port_points set shiparea = 'MED-TYSC' where  uhgs_id in ('A0248035', 'A0251649', 'A0245045', 'A0248655', 'A0220382', 'A0247154', 'A0220382, ''A0247154', 'A0237463', 'A0230257', 'A0235714', 'A0245915', 'A0236107', 'A1969442', 'A1969400', 'A0246822');
update ports.port_points set shiparea = 'MED-ADRS' where  uhgs_id in ('A0228907', 'A0230079', 'A0251340', 'A0222224', 'A0233039');
update ports.port_points set shiparea = 'MED-ADRN' where  uhgs_id in ('A0219489', 'A0219727', 'A1270963');

update ports.port_points set shiparea = 'MED-ADRC' where  uhgs_id in ('A1367884');
update ports.port_points set shiparea = 'MED-TYEC' where  uhgs_id in ('A0234379', 'A1969435', 'A0235907', 'A0234511', 'A0222837');

update ports.port_points set shiparea = 'BAL-SKAG' where  uhgs_id in ('A0840374', 'A0891587');
update ports.port_points set shiparea = 'ACA-STLA' where  uhgs_id in ('B2119401', 'B2116612');
update ports.port_points set shiparea = 'ACA-NEWF' where  uhgs_id in ('B2121123', 'B2116612', 'B0000979', 'B2126749');

update ports.port_points set shiparea = 'INE-CORO' where  uhgs_id in ('D3874565');





-----------------------------------------------------------------------------------------------
-- BACKUPS faits le 27 dec 2022
-- C:\Travail\Data\BDD-backups\portic
-- etats
-- labels_lang
-- port_points

-- Ajouter aussi A0115430 Vera (toponyme FR et GB) 37.25,-1.866667, etatslike = A0081001
-- mail du 26 dec 2022, 8h
-- Corriger les toponymes de 
--A1483064 Cesme 
--A1528139 Siagi / S??ac?k
-- Signaler les doublons 
-- DOUBLON 'A1813720', 'A1770543'
-- DOUBLON ou presque A0917656 Egersund A0907996


-- correction de bureaux de ferme le 3 janvier 2023 : V7 local et server et V8 serveur
update ports.port_points set ferme_bureau = 'Oléron' where uhgs_id = 'A1964982' and toponyme_standard_fr = 'La Perrotine';
update ports.port_points set ferme_bureau = 'Oléron' where uhgs_id = 'A0159385' and toponyme_standard_fr = 'Saint-Trojan';
update ports.port_points set ferme_bureau = 'Oléron' where uhgs_id = 'A0215022' and toponyme_standard_fr = 'Pointe d'' Ors';
update ports.port_points set ferme_bureau = 'La Rochelle' where uhgs_id = 'A0152629' and toponyme_standard_fr = 'L''Houmée';
update ports.port_points set ferme_bureau = 'La Rochelle' where uhgs_id = 'A0137297' and toponyme_standard_fr = 'Marselleq';

-- inutile sur cette version Alligre
select uhgs_id , toponyme_standard_fr  from ports.port_points where ferme_bureau = 'Alligre' ;
--A0144350	Charron
--A0219226	pertuis Breton
--A0214663	Le Brault
--A1964694	Marans
select uhgs_id , toponyme_standard_fr,ferme_bureau, amiraute  from ports.port_points where uhgs_id in ('A0144350', 'A0219226', 'A0214663', 'A1964694') ;
update ports.port_points set ferme_bureau = 'Aligre' where ferme_bureau = 'Alligre' ;

-- correction coquille sur amirauté de La Chaume le 3 janvier 2023
update ports.port_points set amiraute = 'Sables-d’Olonne' where toponyme_standard_fr = 'La Chaume' and uhgs_id = 'A0166852';

-- toponyme vide
update ports.port_points pp set toponyme = 'Chalon' where pp.uhgs_id = 'A1969195';

-----------------------------------------------------------------------------------------------
-- Les tables de navigo avec LoadFilemaker /ligne 1038 de v7
-----------------------------------------------------------------------------------------------

select distinct suite from navigoviz."source" s 
--G5
--Expéditions coloniales Marseille (1789)
--la Santé registre de patentes de Marseille
--Registre du petit cabotage (1786-1787)

select * from navigoviz."source" s 
-- data_block_local_id, documentary_unit_id, source, p.component_description__component_source
/* supprimer ce calcul */ 
-- suite, component, conger_number, other, 
-- main_port_uhgs_id, main_port_toponyme, 
-- subset 
/* reprendre : 
 * suite <- portic_suite 
 * component <- component_description__component_source
 * conger_number : null
 * other : null
 * main_port_uhgs_id : possible
 * main_port_toponyme : possible
 * subset
 */

select distinct subset from navigoviz."source" s 
/*
 * Poitou
Expéditions coloniales Marseille (1789)

la Santé registre de patentes de Marseille
Registre du petit cabotage (1786-1787)
*/
 
p.component_description__component_source

select p.component_description__component_id , p.component_description__component_short_title ,
p.component_description__component_source , p.component_description__suite_id ,
p.suite_description__suite_title 
from navigo.pointcall p 

select distinct component_description__component_source from navigo.pointcall p order by component_description__component_source;

select p.data_block_local_id , p.documentary_unit_id , p.source, p.component_description__component_id , p.component_description__component_short_title ,
p.component_description__component_source , p.component_description__suite_id ,
p.suite_description__suite_title 
from navigo.pointcall p 
where component_description__component_source is null;

select * 
from navigo.pointcall p 
where component_description__component_source is null;
-- 45

delete  from navigo.pointcall p 
where component_description__component_source is null;
-- 45

-----
select replace('ANF, G5--125-1/Noirmoutier en l'' Ile', ' ', '')=replace('ANF, G5--125-1/Noirmoutier en l''Ile', ' ', '')
select replace('ANF, G5-151B/Saint-Martin-de-Ré
', ' ', '')=replace('ANF, G5-151B/Saint-Martin-de-Ré',' ','')
select component_description__component_source, regexp_replace(component_description__component_source, E'[\\s\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )=replace('ANF, G5-151B/Saint-Martin-de-Ré',' ','')
from navigo.pointcall p 
where component_description__component_source like 'ANF, G5-151B/Saint-Martin-de-Ré%'

select component_description__component_source, regexp_replace(component_description__component_source, E'[\\s\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )=replace('ANF, G5-153/Saint-Nazaire',' ','')
from navigo.pointcall p 
where component_description__component_source like 'ANF, G5-153/Saint-Nazaire%'

select regexp_replace('ANF, G5--125-1/Noirmoutier en l'' Ile', E'[\\s\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )=regexp_replace('ANF, G5--125-1/Noirmoutier en l''Ile', E'[\\s\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g')
-- Integration de la table component_list sortie de Filemaker avec ajout de trois colonnes : portic_remarques	portic_used	portic_suite


select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1
-- 228 : MAL !! 
            
select *
from navigocheck.check_pointcall cp 
where cp.data_block_local_id = '00301999'
order by pointcall_outdate ; 
-- distinct component_description__component_source, "source" 
-- '00149449' 

select * from navigoviz."source" where data_block_local_id = '00149449' ;
-- un doc_unit_id is null
select * from navigoviz."source" where data_block_local_id = '00181229' ;
select * from navigoviz."source" where data_block_local_id = '00179889' ;


select * from navigoviz."source" s 
where data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1
    )
and s.documentary_unit_id is null
order by data_block_local_id;
-- 233
-- il y a des sources dont le texte "source" diffère pour un même doc id

select data_block_local_id from navigoviz."source" s 
where data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1
    )
	and s.documentary_unit_id is null
group by data_block_local_id
having count(data_block_local_id) > 1

/*
00353709
00300526
00301148
00303012
00335413
00301999
00310213
*/

select * 
from navigoviz."source" s, navigocheck.check_pointcall cp  
where s.data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
	where data_block_local_id in (
		select data_block_local_id from navigoviz."source" s 
	            group by data_block_local_id
	            having count(data_block_local_id) > 1
	    )
		and s.documentary_unit_id is null
	group by data_block_local_id
	having count(data_block_local_id) > 1)
and cp.data_block_local_id = s.data_block_local_id 
order by cp.data_block_local_id, pointcall_outdate ; 

ADBdR, 200E, 525/
ADBdR, 200E, 545/
ANF, G5-76/5854

-- Corriger pointcall avec ces valeurs de source pour les data_block_local_id
/*
ADBdR, 200E, 525/0235 -> 00300526
ADBdR, 200E, 525/0311 -> 00301148
ADBdR, 200E, 525/0405 -> 00301999
ADBdR, 200E, 525/0503 -> 00303012
ADBdR, 200E, 545/0283 -> 00310213
ANF, G5-76/5854 et 5860 -> 00335413
ADBdR, 200E, 545/0757 -> 00353709
*/
select * from navigoviz."source" s where data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
		where data_block_local_id in (
			select data_block_local_id from navigoviz."source" s 
		            group by data_block_local_id
		            having count(data_block_local_id) > 1
		    )
			and s.documentary_unit_id is null
		group by data_block_local_id
		having count(data_block_local_id) > 1
	)



select source, cp.component_description__component_source , data_block_local_id , * 
from navigocheck.check_pointcall cp  
where cp.data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
	where data_block_local_id in (
		select data_block_local_id from navigoviz."source" s 
	            group by data_block_local_id
	            having count(data_block_local_id) > 1
	    )
		and s.documentary_unit_id is null
	group by data_block_local_id
	having count(data_block_local_id) > 1)
order by cp.data_block_local_id, pointcall_outdate ;
-- Extraction faite pour Silvia en windows_1250

delete from navigoviz."source" s 
where data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
		where data_block_local_id in (
			select data_block_local_id from navigoviz."source" s 
		            group by data_block_local_id
		            having count(data_block_local_id) > 1
		    )
			and s.documentary_unit_id is null
		group by data_block_local_id
		having count(data_block_local_id) > 1
	)
and source in ('ADBdR, 200E, 525/', 'ADBdR, 200E, 545/', 'ANF, G5-76/5854');
-- 7

select record_id , data_block_local_id, documentary_unit_id, "source"  from navigo.pointcall p 
where data_block_local_id in (
	select data_block_local_id
	from navigo.pointcall p 
    group by data_block_local_id
    having  count(distinct documentary_unit_id)!=1
 )  and documentary_unit_id!=null
 order by data_block_local_id;

select record_id , data_block_local_id, documentary_unit_id, "source"  
from navigo.pointcall p 
where data_block_local_id in (
	select data_block_local_id from  
	(select distinct data_block_local_id, documentary_unit_id, source,  regexp_replace(component_description__component_source, E'[\\s\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ) from navigocheck.check_pointcall
	) as s
	group by data_block_local_id
	having count(data_block_local_id) > 1
) 
and documentary_unit_id is null
order by data_block_local_id;
-- Extraction pour silvia le 5 janvier 2023

00155444	00149474	00149474	ANF, G5-91/9010
00149474	00149474	00149474	ANF, G5-91/9010
00296240	00149474		ANF, G5-91/9010

-- corrections manuelles reportées dans le code
delete from navigoviz."source" s 
where data_block_local_id in (
	select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1
    )
and s.documentary_unit_id is null;
-- 219


select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1
-- 2 : MAL !! 00149228 et 00104863

select * from navigoviz."source" where data_block_local_id = '00149228' ;
-- ANF, G5-62/rafraichi
-- ANF, G5-62/rafraîchi
select * from navigoviz."source" where data_block_local_id = '00104863' ;
select *
from navigocheck.check_pointcall cp 
where cp.data_block_local_id = '00104863'
order by pointcall_outdate ;             
-- ANF, G5-39B-1/Abbeville / ANF, G5-115A/La Hougue
-- congé pris à Abbeville, pour arriver au Havre, et l'arrivée marquée comme component de la Hougue ce qui est faux.

-- corrections manuelles reportées dans le code
delete from navigoviz."source" s 
where data_block_local_id = '00104863' and component = 'ANF, G5-115A/La Hougue';

delete from navigoviz."source" s 
where data_block_local_id = '00149228' and source = 'ANF, G5-62/rafraichi';

select *
from navigocheck.check_pointcall cp 
where cp.data_block_local_id in ('00104863', '00149228')
order by pointcall_outdate ;  

select data_block_local_id from navigoviz."source" s 
            group by data_block_local_id
            having count(data_block_local_id) > 1;
            -- OK, Source terminé
           
update navigoviz.source s set main_port_uhgs_id =k.uhgs_id, main_port_toponyme =k.toponyme
            from (
            select q2.component, q2.uhgs_id, q2.toponyme, q1.max from (
            select component, max(c)  from (
                select distinct substring(source from 1 for position('/' in source)) as component, pp.uhgs_id , pp.toponyme , pp.amiraute , pp.province , pp.shiparea, count(*) as c
                from navigocheck.check_pointcall p, ports.port_points  pp
                where p.pointcall_uhgs_id = pp.uhgs_id 
                and (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
                    and substring(source from 1 for position('/' in source)) not like 'G5-%') 
                group by component, pp.uhgs_id, pp.toponyme , pp.amiraute , pp.province , pp.shiparea
                order by component) as k
            group by component
            ) as q1,
            (select distinct substring(source from 1 for position('/' in source)) as component, pp.uhgs_id , pp.toponyme , pp.amiraute , pp.province , pp.shiparea, count(*) as c
                from navigocheck.check_pointcall p, ports.port_points   pp
                where p.pointcall_uhgs_id = pp.uhgs_id 
                and (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
                    and substring(source from 1 for position('/' in source)) not like 'G5-%') 
                group by component, pp.uhgs_id, pp.toponyme , pp.amiraute , pp.province , pp.shiparea
                order by component
            ) as q2
            where q1.component = q2.component and q2.c = q1.max
            ) as k
            where position('/' in k.component)>0 
            
            
            and substring(s.component from 1 for position('/' in k.component)-1) 
            = substring(k.component from 1 for position('/' in k.component)-1)
-- 41041
            
select distinct component  from navigoviz.source s
where s.suite = 'G5' and main_port_uhgs_id is null;


-- set main_port_uhgs_id ='A0135548', main_port_toponyme = 'Saint Valery en Caux' where component = 'ANF, G5, 154-1, 1539'
select * from navigoviz.source s where source like 'ANF, G5, 154-1, 1539%'

select distinct cp.component_description__component_source, pointcall_uhgs_id , pointcall_name 
from navigocheck.check_pointcall cp 
where cp.component_description__component_source 
in ('ANF, G5-91/La Flotte-en-Ré', 'ADMorbihan, 10B-19/Lorient', 'ADVar, 7B10/', 
'ANF, G5-155/', 'ANF, G5--125-2/Omonville-la-Rogue', 'ANF, G5--125-1/Noirmoutier en l'' Ile')
and pointcall_function ='O';

/*ANF, G5-91/La Flotte-en-Ré	A0199508	La Flotte en Ré
ADMorbihan, 10B-19/Lorient	A0140266	Lorient
ADVar, 7B10/	A0147257	Saint Tropez
ANF, G5-155/	A0207992	Talmont
ANF, G5--125-2/Omonville-la-Rogue A0153622	Omonville
ANF, G5--125-1/Noirmoutier en l' Ile	A0136403	Noirmoutier*/

update navigoviz."source" set main_port_uhgs_id ='A0136403', main_port_toponyme = 'Noirmoutier' where component = 'ANF, G5--125-1/Noirmoutier en l'' Ile' ;
-- 558
update navigoviz."source" set main_port_uhgs_id ='A0153622', main_port_toponyme = 'Omonville' where component = 'ANF, G5--125-2/Omonville-la-Rogue';
-- 33
update navigoviz."source" set main_port_uhgs_id ='A0207992', main_port_toponyme = 'Talmont' where component = 'ANF, G5-155/';
-- 21
update navigoviz."source" set main_port_uhgs_id ='A0147257', main_port_toponyme = 'Saint-Tropez' where component = 'ADVar, 7B10/';
-- 535
update navigoviz."source" set main_port_uhgs_id ='A0140266', main_port_toponyme = 'Lorient' where component = 'ADMorbihan, 10B-19/Lorient';
-- 1349
update navigoviz."source" set main_port_uhgs_id ='A0199508', main_port_toponyme = 'La Flotte-en-Ré' where component = 'ANF, G5-91/La Flotte-en-Ré';
-- 399

select distinct suite, subset from navigoviz.source;
select * from navigoviz.source where subset is null;
delete from navigoviz.source where subset is null;

update navigoviz.source set subset = 'Poitou' where subset = 'G5' and suite = 'G5' and main_port_uhgs_id in ('A1964694', 'A0171758', 
        'A0136930', 'A0196496', 'A0198999', 'A0137148', 'A0127055', 'A0133403', 'A0213721', 'A0199508', 'A0148208', 'A0141325', 'A0138533', 'A1964982', 'A0186515', 
        'A0124809', 'A1964767', 'A0172590', 'A0181608',  'A0169240', 'A0165056', 'A1963997', 'A0136403', 'A0195938', 'A0122971',  'A0207992', 'A0165077');
        
----------------------------------------------------------------------
-- cargo le 04 janvier 2023
----------------------------------------------------------------------

-- Import des traductions et catégories de Pierre Nicolo (21 sept 2022)
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\cargo\Arborescence produits NAVIGO_pour Christine et Géraldine.xlsx
-- onglet : Tableau synthèse et occurrence
-- Copié dans    
-- C:\\Travail\\Data\\10-Navigo_29sept2022\\Arborescence_produits_Nicolo21sep2022.xlsx
-- sheet_name=Import_postgres 
       
create or replace view navigoviz.pointcall_cargo as (
            select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'data_block_local_id' as data_block_local_id,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'pointcall_rank' as pointcall_rank,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'pointcall_name' as pointcall_name,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_purpose' as commodity_purpose,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_id' as commodity_id,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity' as quantity,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity_u' as quantity_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'cargo_item_action' as cargo_item_action,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'category_portic_fr' as category_portic_fr,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'category_toflit18_RE' as category_toflit18_RE,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'category_toflit18_RE_aggregate' as category_toflit18_RE_aggregate,
            jsonb_agg(to_jsonb(r.*) ) as all_cargos
            from 
                (
                select c.link_to_pointcall, c.pointcall__data_block_local_id as data_block_local_id, 
                c.pointcall__pointcall_rank as pointcall_rank, c.pointcall__pointcall_name as pointcall_name,
                 c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                 c.cargo_item_action,
                 c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
                 coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
                 labels.commodity_standardized_en ,  labels.category_portic_en ,
                 labels.commodity_standardized_fr ,  labels.category_portic_fr ,
                 labels.category_toflit18_Revolution_Empire  as category_toflit18_RE,
                 labels.category_toflit18_Revolution_Empire_aggregate  as category_toflit18_RE_aggregate
                from navigocheck.check_cargo c left join navigoviz.cargo_categories labels
                on labels.record_id = c.commodity_id
                ) as r
            group by r.link_to_pointcall
        );
       
alter table navigo.cargo_categories set schema navigoviz;      
drop view IF EXISTS navigocheck.pointcall_cargo cascade

select c.link_to_pointcall , c.pointcall_name, c.pointcall_rank, c.cargo_item_action , c.cargo_item_sending_place  
from navigoviz.pointcall_cargo c 
where link_to_pointcall = data_block_local_id;
-- 8300

select  c.pointcall__pointcall_rank, c.pointcall__pointcall_name  from navigocheck.check_cargo c 
[{"quantity": null, "quantity_u": null, "commodity_id": "00000226", "cargo_item_action": "Loading", "commodity_purpose": "Olive oil", "link_to_pointcall": "00000051", "category_portic_en": "Fuels, intermediate products or other materials", "cargo_item_owner_name": null, "cargo_item_owner_class": null, "cargo_item_sending_place": "Marseille", "cargo_item_owner_location": null, "commodity_standardized_en": "Olive oil", "pointcall__data_block_local_id": "00000049"}]
       
select c.link_to_pointcall , c.pointcall_name, c.pointcall_rank, c.cargo_item_action , c.cargo_item_sending_place  from navigoviz.pointcall_cargo c 
where link_to_pointcall != data_block_local_id
and data_block_local_id in (select data_block_local_id from navigocheck.check_pointcall cp)
-- 562

select count(*)
from navigoviz.pointcall_cargo c 
where link_to_pointcall = data_block_local_id
and data_block_local_id in (select data_block_local_id from navigocheck.check_pointcall cp)
-- 49348

----------------------------------------------------------------------------------------
-- Cargo : export pour Guillaume Daudin (toflit'8 category) le 05 janvier 2023
----------------------------------------------------------------------------------------

select c.* , (array_agg(cc.commodity_purpose )), array_length(array_agg(cc.commodity_purpose ), 1) as  nb_occurences
from cargo_categories c, navigocheck.check_cargo cc 
where c.record_id = cc.commodity_id 
group by c.pkid;

select c.record_id, commodity_standardized_fr , commodity_standardized_en , (array_agg(cc.commodity_purpose )) as list_commodity_purpose, array_length(array_agg(cc.commodity_purpose ), 1) as  nb_occurences
from cargo_categories c, navigocheck.check_cargo cc 
where c.record_id = cc.commodity_id 
group by c.record_id, commodity_standardized_fr , commodity_standardized_en 
order by commodity_standardized_fr;

select distinct regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ), commodity_id 
from navigo.cargo c 
where link_to_pointcall in (select  record_id from navigo.pointcall p)
and commodity_id is  null;
-- 147 / 2887/  2861 / 7428
-- regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )


select distinct regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ), commodity_id 
from navigo.cargo c 
where link_to_pointcall in (select  record_id from navigo.pointcall p)
and commodity_id is not null
order by commodity_id;
-- 2740

select ct.commodity_purpose , ct.commodity_id , ct.revolutionempire , ct."RE_aggregate"  from cargo_toflit18 ct 
-- 1134

select * from public.cargo_toflit18 where commodity_purpose ilike '%acier%';
select distinct  ct.commodity_id , ct.revolutionempire , ct."RE_aggregate"  from cargo_toflit18 ct 
order by commodity_id
-- 575

alter table navigoviz.cargo_categories add column category_toflit18_Revolution_Empire text;
alter table navigoviz.cargo_categories add column category_toflit18_Revolution_Empire_aggregate text;

update navigoviz.cargo_categories c set category_toflit18_Revolution_Empire = k.revolutionempire, category_toflit18_Revolution_Empire_aggregate= k.reagg
from 
(select distinct  ct.commodity_id , ct.revolutionempire , ct."RE_aggregate" as reagg from cargo_toflit18 ct 
order by commodity_id) as  k 
where k.commodity_id = c.record_id ;


select c.record_id, commodity_standardized_fr , commodity_standardized_en , (array_agg(cc.commodity_purpose )) as list_commodity_purpose, array_length(array_agg(cc.commodity_purpose ), 1) as  nb_occurences, 
c.category_portic_fr , c.category_portic_en ,
c.category_toflit18_revolution_empire , c.category_toflit18_revolution_empire_aggregate 
from navigoviz.cargo_categories c, navigocheck.check_cargo cc 
where c.record_id = cc.commodity_id 
and cc.link_to_pointcall in (select  record_id from navigo.pointcall p)
and cc.commodity_id is not null
group by c.record_id, commodity_standardized_fr , commodity_standardized_en ,
c.category_toflit18_revolution_empire , c.category_toflit18_revolution_empire_aggregate, c.category_portic_fr , c.category_portic_en 
order by nb_occurences;
-- 761 export le soir du 5 janvier 2023 pour Silvia et Guillaume

select  ct.commodity_purpose , ct.commodity_id , ct.revolutionempire , ct."RE_aggregate", ct.*  from cargo_toflit18 ct 
where "RE_aggregate" is null;

select distinct record_id, unnest(list_commodity_purpose) as source_portic_fr, commodity_standardized_fr, commodity_standardized_en, nb_occurences as nb_occurences_record_id, category_portic_fr  from (
select c.record_id, commodity_standardized_fr , commodity_standardized_en , (array_agg(cc.commodity_purpose )) as list_commodity_purpose, array_length(array_agg(cc.commodity_purpose ), 1) as  nb_occurences, 
c.category_portic_fr , c.category_portic_en ,
c.category_toflit18_revolution_empire , c.category_toflit18_revolution_empire_aggregate 
from navigoviz.cargo_categories c, navigocheck.check_cargo cc 
where c.record_id = cc.commodity_id 
and cc.link_to_pointcall in (select  record_id from navigo.pointcall p)
and cc.commodity_id is not null
group by c.record_id, commodity_standardized_fr , commodity_standardized_en ,
c.category_toflit18_revolution_empire , c.category_toflit18_revolution_empire_aggregate, c.category_portic_fr , c.category_portic_en 
order by nb_occurences
) as k 
where 
category_toflit18_revolution_empire is null -- 239 
-- category_toflit18_revolution_empire_aggregate is null --254
order by category_portic_fr, record_id; -- 372
-- 239 / 254
-- Export pour completion toflit18 (mail Guillaume) le 06 janvier 2023
--Missing category_toflit18_revolution_empire_aggregate : 
--Sortie pour pêche
--Êtres humains

c.record_id, commodity_standardized_fr , commodity_standardized_en , (array_agg(cc.commodity_purpose )) as list_commodity_purpose, array_length(array_agg(cc.commodity_purpose ), 1) as  nb_occurences, 
c.category_portic_fr , c.category_portic_en ,
c.category_toflit18_revolution_empire , c.category_toflit18_revolution_empire_aggregate 

-- refaire un export complet pour Guillaume avec un compte des commodity_purpose (nb_occurences_source)
select distinct record_id, commodity_purpose as source_portic_fr, commodity_standardized_fr, commodity_standardized_en, 
nb_occurences_source , category_portic_fr  , nb_commodity_id
from 
navigoviz.cargo_categories c, 
(
	select cc.commodity_purpose, count(*) nb_occurences_source, (array_agg(distinct cc.commodity_id  ))[1] as commodity_id, array_length(array_agg(distinct cc.commodity_id  ), 1) as nb_commodity_id
	from navigoviz.cargo_categories c, navigocheck.check_cargo cc 
	where c.record_id = cc.commodity_id 
	and cc.link_to_pointcall in (select  record_id from navigo.pointcall p)
	and cc.commodity_id is not null
	group by commodity_purpose 
) as k 
where 
k.commodity_id = c.record_id 
-- and category_toflit18_revolution_empire is null -- 239 
-- category_toflit18_revolution_empire_aggregate is null --254
order by category_portic_fr, record_id, nb_occurences_source;


select record_id , commodity_purpose, commodity_id , regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', ', ', 'g' ) as test
-- distinct regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ) as commodity_purpose, commodity_id 
                    from navigo.cargo c 
                    where link_to_pointcall in (select  record_id from navigo.pointcall p)
                    and  commodity_id is not null
                    and commodity_id in ('00001494', '00001480');
 
-- 23 janvier 2023 Refaire un export complet pour Guillaume avec un compte des commodity_purpose (nb_occurences_source)
select distinct record_id, commodity_purpose_ok as source_portic_fr, commodity_standardized_fr, commodity_standardized_en, 
nb_occurences_source , category_portic_fr  , nb_commodity_id
from 
navigoviz.cargo_categories c, 
(
	select cc.commodity_purpose, regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ) as commodity_purpose_ok,
	count(*) nb_occurences_source, (array_agg(distinct cc.commodity_id  ))[1] as commodity_id, array_length(array_agg(distinct cc.commodity_id  ), 1) as nb_commodity_id
	from navigoviz.cargo_categories c, navigocheck.check_cargo cc 
	where c.record_id = cc.commodity_id 
	and cc.link_to_pointcall in (select  record_id from navigo.pointcall p)
	and cc.commodity_id is not null
	group by commodity_purpose 
) as k 
where 
k.commodity_id = c.record_id 
-- and category_toflit18_revolution_empire is null -- 239 
-- category_toflit18_revolution_empire_aggregate is null --254
order by category_portic_fr, record_id, nb_occurences_source;
-- sauver dans C:\Travail\ULR_owncloud\ANR_PORTIC\Data\cargo\classification_produits_portic_toflit_23janvier2023_all.csv

select cc.commodity_purpose, count(*) nb_occurences_source, array_agg(distinct cc.commodity_id  ) as list_commodity_id, array_length(array_agg(distinct cc.commodity_id  ), 1) as nb_commodity_id
from navigoviz.cargo_categories c, navigocheck.check_cargo cc 
where c.record_id = cc.commodity_id 
and cc.link_to_pointcall in (select  record_id from navigo.pointcall p )
and cc.commodity_id is not null
group by commodity_purpose 
having array_length(array_agg(distinct cc.commodity_id  ), 1) > 1

-- Dans le fichier navigo : navigocheck.check_cargo, il y a des commodity_purpose associées à plus qu'un commodity_id
select distinct commodity_id  
from navigo.cargo cc
where commodity_purpose = 'Grain' 
and pointcall__data_block_local_id  in (select s.data_block_local_id  from navigoviz."source" s)
order by commodity_id;


select link_to_pointcall , cc.pointcall__pointcall_name , record_id ,  cc.commodity_id , cc.commodity_purpose , source
from navigo.cargo cc
where commodity_purpose in (
	select  cc.commodity_purpose
	from navigoviz.cargo_categories c, navigo.cargo cc --navigocheck.check_cargo cc 
	where c.record_id = cc.commodity_id 
	and cc.link_to_pointcall in (select  record_id from navigo.pointcall p)
	and cc.commodity_id is not null
	group by commodity_purpose 
	having array_length(array_agg(distinct cc.commodity_id  ), 1) > 1
)
and pointcall__data_block_local_id  in (select s.data_block_local_id  from navigoviz."source" s)
order by commodity_id, commodity_purpose;

/*
 * Boeuf
Bouteilles
Divers effets
diverses marchandises
Fèves
Fruits
Fruits frais
Grain
Grains
huile de poisson
Huile de sardine
marchandise
marchandises
Marchandises
Marchandises diverses
Poisson salé
Pour la pêche
son
Viande de pate
vin muscat
 */
----------------------------------------------------------------------------------------
-- dates des pointcall  : erreurs corrigées manuellement 
----------------------------------------------------------------------------------------

select count(*) from navigoviz.pointcall pc 
-- 139321

select count(*) from navigocheck.check_pointcall cp   
-- 139439
select count(*) from navigocheck.check_pointcall cp   where data_block_local_id is null;
-- 45
select * from navigocheck.check_pointcall cp   where 
pkid not in (select pkid from navigoviz.pointcall);

-- echec comme d'habitude de cette requete
update navigoviz.pointcall 
set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
                            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(pointcall_in_date, '!', ''), '>', '=')  else replace(pointcall_in_date, '>', '=')  end 
                          end 
where position('00' in pointcall_in_date ) = 0
            
-- 1759-02-29


select record_id, pointcall_indate, pkid from navigo.pointcall
where pointcall_indate in ('1759<02<29','1789>11>31' , '1787>17>04!', '1789>04>31',
'1789>04>31', '1789>09>31', '1787>02>29!')

select record_id, pointcall_indate, pkid from navigo.pointcall 
where  record_id in ('00306428', '00306976');

--00306428	1788>11>31	85279
--00306976	1788>11>31	85822
update    navigo.pointcall set  pointcall_indate =   '1788>11>30'     where   record_id in ('00306428', '00306976');
update    navigocheck.check_pointcall set  pointcall_indate =   '1788>11>30'     where record_id  in ('00306428', '00306976');
update    navigoviz.pointcall set  pointcall_in_date =   '1788>11>30'     where  record_id in ('00306428', '00306976');-- # 1788>11>31 


-- 00297588	1759<02<29	76555
update    navigo.pointcall set  pointcall_indate =   '1759<02<28'     where record_id  = '00297588';
update    navigocheck.check_pointcall set  pointcall_indate =   '1759<02<28'     where record_id  = '00297588';
update    navigoviz.pointcall set  pointcall_in_date =   '1759<02<28'     where  record_id in ('00297588');

-- 00302494	1748=31=12	81393
update    navigo.pointcall set  pointcall_indate =   '1748=12=31'     where record_id  = '00302494';
update    navigocheck.check_pointcall set  pointcall_indate =   '1748=12=31'     where record_id  = '00302494';
update    navigoviz.pointcall set  pointcall_in_date =   '1748=12=31'     where  record_id in ('00302494');

-- 00302493		1748<31<12!
update    navigo.pointcall set  pointcall_outdate =   '1748<12<31!'     where record_id  = '00302493';
update    navigocheck.check_pointcall set  pointcall_outdate =   '1748<12<31!'     where record_id  = '00302493';
update    navigoviz.pointcall set  pointcall_out_date =   '1748<12<31!'     where  record_id in ('00302493');

-- 00313825	1759=11=31	92616	1759=11=31
update    navigo.pointcall set  pointcall_indate =   '1759=11=30'     where record_id  = '00313825';
update    navigocheck.check_pointcall set  pointcall_indate =   '1759=11=30'     where record_id  = '00313825';
update    navigoviz.pointcall set  pointcall_in_date =   '1759=11=30'     where  record_id in ('00313825');

-- 00365122	1789>16>07!	139226	1789=16=07
update    navigo.pointcall set  pointcall_indate =   '1789>07>16!'      where record_id  = '00365122';
update    navigocheck.check_pointcall set  pointcall_indate =   '1789>07>16!'      where record_id  = '00365122';
update    navigoviz.pointcall set  pointcall_in_date =   '1789>07>16!'      where  record_id in ('00365122');

-- 00312165 1749=30=29 
update    navigo.pointcall set  pointcall_indate =   '1749=06=29'      where record_id  = '00312165';
update    navigocheck.check_pointcall set  pointcall_indate =   '1749=06=29'      where record_id  = '00312165';
update    navigoviz.pointcall set  pointcall_in_date =   '1749=06=29'      where  record_id in ('00312165');

-- 00154851	1787>17>04	35727
update    navigo.pointcall set  pointcall_indate =   '1787>04>17'      where record_id  = '00154851';
update    navigocheck.check_pointcall set  pointcall_indate =   '1787>04>17'      where record_id  = '00154851';
update    navigoviz.pointcall set  pointcall_in_date =   '1787>04>17'      where  record_id in ('00154851');

-- 00327941	1799=11=[$]	31/10/1799
update    navigo.pointcall set  pointcall_outdate =   '1799=10=31'     where record_id  = '00327941';
update    navigocheck.check_pointcall set  pointcall_outdate =   '1799=10=31'     where record_id  = '00327941';
update    navigoviz.pointcall set  pointcall_out_date =   '1799=10=31'     where  record_id in ('00327941');

-- 00301528	1748=22=10	10/10/1749
update    navigo.pointcall set  pointcall_outdate =   '1748=10=22'     where record_id  = '00301528';
update    navigocheck.check_pointcall set  pointcall_outdate =   '1748=10=22'     where record_id  = '00301528';
update    navigoviz.pointcall set  pointcall_out_date =   '1748=10=22'     where  record_id in ('00301528');

-- 00331147	1779=11=31	01/12/1779
update    navigo.pointcall set  pointcall_outdate =   '1779=12=01'     where record_id  = '00331147';
update    navigocheck.check_pointcall set  pointcall_outdate =   '1779=12=01'     where record_id  = '00331147';
update    navigoviz.pointcall set  pointcall_out_date =   '1779=12=01'     where  record_id in ('00331147');

-- 00297588	1759=02=29	01/03/1759
update    navigo.pointcall set  pointcall_outdate =   '1759=03=01'     where record_id  = '00297588';
update    navigocheck.check_pointcall set  pointcall_outdate =   '1759=03=01'     where record_id  = '00297588';
update    navigoviz.pointcall set  pointcall_out_date =   '1759=03=01'     where  record_id in ('00297588');

-- 1748=31=12
-- 1788=11=31
-- 1759=11=31
-- 1789=16=07 
-- 1749=30=29
-- 1787=17=04
-- 1748-31-12
select record_id, pointcall_indate, pointcall_outdate  from navigo.pointcall
where pointcall_outdate like '1748%31%12%';

select record_id, pointcall_in_date, pkid, pointcall_in_date2  from navigoviz.pointcall
where pointcall_in_date2 like '1749=30=29';

select data_block_local_id  from navigo.pointcall  where record_id = '00312165';
-- 00312165
select * from navigo.pointcall where data_block_local_id = '00312165'; --29/06/1751

update navigoviz.pointcall 
set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
                            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(pointcall_in_date, '!', ''), '>', '=')  else replace(pointcall_in_date, '>', '=')  end 
                          end 
where position('00' in pointcall_in_date ) = 0;
-- 73691
update navigoviz.pointcall set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(replace(pointcall_in_date, '00', '01'), '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(replace(pointcall_in_date, '00', '01'), '!', ''), '>', '=')  else replace(replace(pointcall_in_date, '00', '01'), '>', '=')  end
            end  where position('00' in pointcall_in_date ) > 0;
-- 22
           
update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null;
-- 73713

-- OUTDATES
update navigoviz.pointcall set pointcall_out_date2 = null;
update navigoviz.pointcall set pointcall_out_date2 = 
		case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   
		else replace(pointcall_out_date, '>', '=') end  
        where position('00' in pointcall_out_date ) = 0 and position('99' in pointcall_out_date ) = 0;
-- 72458
update navigoviz.pointcall set pointcall_out_date2 = 
		case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '00', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   
			else replace(replace(pointcall_out_date, '00', '01'), '>', '=') end  
            where position('00' in pointcall_out_date ) > 0 ;
-- 113

/*
           -- NE PAS FAIRE
update navigoviz.pointcall set pointcall_out_date2 = 
		case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '99', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   
		else replace(replace(pointcall_out_date, '99', '01'), '>', '=') end  
        where position('99' in pointcall_out_date ) > 0 ;
-- 918 : introduit des erreurs
*/

           
-- echec comme d'habitude de cette requete : corrections sur pointcall_out_date ci dessus
update navigoviz.pointcall set outdate_fixed= to_date(pointcall_out_date2, 'YYYY=MM=DD')  
where pointcall_out_date is not null ;

-- OK

select record_id, source_doc_id , pointcall_out_date, pointcall_out_date2, outdate_fixed, pointcall_in_date , pointcall_in_date2 , indate_fixed , * 
from navigoviz.pointcall where pointcall_out_date2 like '%1759=02=29%';


select record_id,  pointcall_out_date, pointcall_out_date2, outdate_fixed, pointcall_in_date , pointcall_in_date2 , indate_fixed , * 
from navigoviz.pointcall where source_doc_id = '00327940';

select record_id, pointcall_outdate,pointcall_outdate_date  , pointcall_indate , pointcall_indate_date , pointcall_name ,* 
from navigo.pointcall where data_block_local_id  = '00297586'
order by pointcall_rank ;
-- 00327941	1799=11=[$]	31/10/1799
select record_id, data_block_local_id , pointcall_outdate,pointcall_outdate_date  , pointcall_indate , pointcall_indate_date ,* 
from navigo.pointcall where record_id  = '00301528';

select record_id, pointcall_out_date, pointcall_out_date2 , outdate_fixed, pointcall_in_date , pointcall_in_date2 , indate_fixed, pointcall  ,* 
from navigoviz.pointcall 
where  substring(pointcall_out_date for 4) != substring(outdate_fixed::text for 4) 
order by source_doc_id , pointcall_rank_dedieu  ; 
-- 34 ligne si OK
-- erreur sur l'année 1799 qui est devenue 1701
-- 00325325	1799=09=21	1701=09=21	1701-09-21
-- Pb réglé

select position('99' in '1799=09=21' ); --3

select  pointcall_out_date,  
		case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '99', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   
		else replace(replace(pointcall_out_date, '99', '01'), '>', '=') 
		end  
from navigoviz.pointcall 
where position('99' in pointcall_out_date ) > 0 ;

select record_id, pointcall_out_date, pointcall_out_date2 , outdate_fixed, pointcall_in_date , pointcall_in_date2 , indate_fixed, pointcall  ,* 
from navigoviz.pointcall 
where  substring(pointcall_in_date for 4) != substring(indate_fixed::text for 4) 
order by source_doc_id , pointcall_rank_dedieu  ; -- tout OK
-- 00302514	1799=09=14	1701=09=14	1701-09-14				Nice	81409	00302514	Nice	A0148992	Nice	Nice	43.697126	7.275338			[{"1749-1792" : "Royaume de Piémont-Sardaigne"},{"1793-1815" : "France"},{"1815-1815" : "Royaume de Piémont-Sardaigne"}]		[{"1749-1792" : "Kingdom of Sardinia"},{"1793-1815" : "France"},{"1815-1815" : "Kingdom of Sardinia"}]		Royaume de Piémont-Sardaigne	Kingdom of Sardinia									MED-LIGS	POINT (809886.9215089533 5418690.743335355)						Etranger		0	1799=09=14	Out	1701-09-14					PC-RS	1.0	Sainte Thérèse				French	Felouque	A0167415																																	00302513	ADBR, 200E, 555/1	la Santé registre de patentes de Marseille	ADBdR, 200E, 555/			la Santé registre de patentes de Marseille		Suaud, Honnoré	Nice	A0148992	capitaine																																				-4	-4	-2	-4	-1	-1	-1	-1	-4	0	-4	-4		1701=09=14

-- Correction de source

select data_block_local_id, p.pointcall_name , pointcall_rank, pointcall_function, source, component_description__component_source 
from navigo.pointcall p where p.data_block_local_id = '00104863';
--00104863	Abbeville	1.0	O	ANF, G5-39B-1/6079	ANF, G5-39B-1/Abbeville
--00104863	Le Havre	2.0	T	ANF, G5-39B-1/6079	ANF, G5-115A/La Hougue



----------------------------------------------------------------------------------------
-- tonnage faux
----------------------------------------------------------------------------------------
select * from navigoviz.pointcall where tonnage like 'achetéàOstende%';
select record_id, ship_tonnage  from navigo.pointcall p where  data_block_local_id = '00283632'; -- records  00283632 et 00284682

select ship_tonnage, record_id, data_block_local_id, * 
from navigocheck.check_pointcall 
where ship_tonnage is not null and navigo.test_double_type(ship_tonnage) is false;
-- extraction faite pour Silvia


update navigoviz.pointcall p set tonnage = null where source_doc_id = '00283632';
update navigo.pointcall p set ship_tonnage = null where data_block_local_id = '00283632';
update navigocheck.check_pointcall  p set ship_tonnage = null where data_block_local_id = '00283632';

update navigoviz.pointcall p set tonnage = 196.5 where source_doc_id = '00148691';
update navigo.pointcall p set ship_tonnage = 196.5 where data_block_local_id = '00148691';
update navigocheck.check_pointcall  p set ship_tonnage = 196.5 where data_block_local_id = '00148691';

select q01_5, record_id, source_doc_id , * 
from navigoviz.pointcall p  
where q01_5 is not null and navigo.test_double_type(q01_5) is false;

select q02_5, record_id, source_doc_id , * 
from navigoviz.pointcall p  
where q02_5 is not null and navigo.test_double_type(q02_5) is false;

select q03_5, record_id, source_doc_id , * 
from navigoviz.pointcall p  
where q03_5 is not null and navigo.test_double_type(q03_5) is false;


select commodity_purpose , tax_concept1 ,* from navigoviz.pointcall p  where source_doc_id  = '00148691'

select * from navigocheck.check_pointcall  where data_block_local_id = '00148691' or documentary_unit_id  = '00148691';
update navigoviz.pointcall p set tonnage_class = '[1-20]' where p.tonnage::float <= 20 and p.tonnage_unit <>'quintaux'

----------------------------------------------------------------------------------------
-- Ship_class_standardized : travail de Pierre Nicolo revu le 05 janvier 2023
----------------------------------------------------------------------------------------
-- Fichier source UTF8 importé en BDD
-- C:\Travail\ULR_owncloud\ANR_PORTIC\Data\tonnage\Ship_class_stand_Christine_V_OK_5_01_2023_UTF8.csv
-- table : navigoviz.ship_class_standardized 

-- ajout de l'attribut ship_class_standardized

----------------------------------------------------------------------------------------

-- supprimer de port
-- k.source_1787_available as homeport_source_1787_available, k.source_1789_available as homeport_source_1789_available, 

-- supprimés de pointcall
-- DELETED s.conge_number as source_number, s.other as source_other,
-- DELETED cc.commodity_standardized_en, cc.commodity_permanent_coding,
-- DELETED pour homeport 
-- 	k.belonging_states as homeport_states, k.belonging_substates as homeport_substates, k.belonging_states_en as homeport_states_en, k.belonging_substates_en as homeport_substates_en,
--	k.nb_conges_1787_inputdone as homeport_nb_conges_1787_inputdone, k.Nb_conges_1787_CR as homeport_Nb_conges_1787_CR, k.nb_conges_1789_inputdone as homeport_nb_conges_1789_inputdone, k.Nb_conges_1789_CR as homeport_Nb_conges_1789_CR, 
--	k.incertitude_status as homeport_status_uncertainity,
--	k.status as homeport_status, k.shiparea as homeport_shiparea, st_asewkt(k.point3857) as homeport_point,

-- cargo : simplifié 
-- 1) no more versions 2, 3, et 4 des commodities
-- 2) no more commodity_permanent_coding neither commodity_standardized_en
-- 3) cargo_item_sending_place : either cargo_item_sending_place if not null, or first_point__pointcall_name.
--		cargo_item_sending_place_uhgs_id : calculé
-- 4) all_cargos : mentionne la version commodity_standardized_en et link_to_pointcall 
-- and data_block_local_id, pointcall_rank, pointcall_name,  
-- c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
-- commodity_standardized_en ,  category_portic_en 
-- 5) Il reste hors all_cargos : 
--	cc.commodity_purpose, cc.commodity_id, cc.quantity, cc.quantity_u, 
--	cc.commodity_standardized_fr, cc.category_portic_fr, cc.category_toflit18_RE, cc.category_toflit18_RE_aggregate,
--	cc.cargo_item_action,  cc.cargo_item_sending_place, cc.cargo_item_sending_place_uhgs_id

-- cargo 
--            cc.nb_cargo,
--            cc.commodity_purpose, cc.commodity_id, cc.quantity, cc.quantity_u, 
--            cc.commodity_standardized_fr, cc.category_portic_fr, cc.category_toflit18_RE, cc.category_toflit18_RE_aggregate,
--            cc.data_lineage, cc.cargo_item_action,  
--            cc.cargo_item_sending_place, cc.cargo_item_sending_place_uhgs_id, cc.cargo_item_sending_pointcallid,
--            cc.all_cargos,
            
drop view IF EXISTS  navigoviz.pointcall_taxes
----------------------------------------------------------------------------------------
-- Pb des chargements et déchargements dans navigo
----------------------------------------------------------------------------------------


select * from navigoviz.pointcall_cargo pc where pc.data_block_local_id = '00304793'
select * from navigoviz.pointcall_cargo pc where pc.link_to_pointcall  = '00304793'

select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' ,
(all_cargos->>1)::json->>'cargo_item_action',
(all_cargos->>1)::json->>'cargo_item_sending_place',
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id'
from navigoviz.pointcall  
where source_doc_id = '00304793'
order by pointcall_rank_dedieu;
-- suite Marseille : LOADING sur In et cargo_item_sending_place ou H4444444 et La Roquette not found in port.port_points
--1.0	Malaga	A0107465 			LOADING 	Fruits sec						
--2.0	La Roquette	H4444444		LOADING 	Soude					
--3.0	Sette	A0170986	2	Fruits sec	In	Malaga	Soude	In	La Roquette
--4.0	Tines	A0122514							
--5.0	Marseille	A0210797	2	Fruits sec	In	Malaga Soude	In	La Roquette	

select * from ports.port_points pp where toponyme_standard_fr = 'Tines' or toponyme_standard_fr = 'La Roquette';
select * from navigo.geo_general_complet ggc where ggc.pointcall_name  = 'Tines' or pointcall_name = 'La Roquette';
-- 'La Roquette' est inconnue

-- retrouver le UHGS_id du point de départ à partir de cargo_item_sending_place
select uhgs_id , toponyme , toponyme_standard_fr , toustopos
from ports.port_points pp where 'Tines' = any(pp.toustopos );

select uhgs_id , toponyme , toponyme_standard_fr , toustopos
from ports.port_points pp where 'La Roquette' = any(pp.toustopos );

alter view navigoviz.pointcall_cargo add cargo_item_sending_place_uhgs_id

select c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
c.pointcall__pointcall_rank as pointcall_rank, c.pointcall__pointcall_name as pointcall_name,
 c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
 c.cargo_item_action,
 c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
 coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
 pp.uhgs_id as cargo_item_sending_place_uhgs_id,
 labels.commodity_standardized_en ,  labels.category_portic_en ,
 labels.commodity_standardized_fr ,  labels.category_portic_fr ,
 labels.category_toflit18_Revolution_Empire  as category_toflit18_RE,
 labels.category_toflit18_Revolution_Empire_aggregate  as category_toflit18_RE_aggregate
from navigocheck.check_cargo c 
left join navigoviz.cargo_categories labels on labels.record_id = c.commodity_id
left join ports.port_points pp on coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) = any(pp.toustopos )
where uhgs_id is null and link_to_pointcall in (select  record_id from navigo.pointcall p);


select * from (
-- bonne requete
select  c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
c.pointcall__pointcall_rank as pointcall_rank, c.pointcall__pointcall_name as pointcall_name,
 c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
 c.cargo_item_action,
 c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
 coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
 coalesce(pp.uhgs_id, c.first_point__pointcall_uhgs_id) as cargo_item_sending_place_uhgs_id,
 labels.commodity_standardized_en ,  labels.category_portic_en ,
 labels.commodity_standardized_fr ,  labels.category_portic_fr ,
 labels.category_toflit18_Revolution_Empire  as category_toflit18_RE,
 labels.category_toflit18_Revolution_Empire_aggregate  as category_toflit18_RE_aggregate
from navigocheck.check_cargo c 
left join navigoviz.cargo_categories labels on labels.record_id = c.commodity_id
left join ports.port_points pp on c.cargo_item_sending_place = any(pp.toustopos ) or c.cargo_item_sending_place % pp.toponyme 
where 
-- cargo_item_sending_place is not null  and uhgs_id is null and cargo_item_sending_place!=first_point__pointcall_name and
 c."source" in  (select s."source"  from navigoviz."source" s)
order by cargo_item_sending_place
) as k
where data_block_local_id = '00304793';


select * from (
-- bad requete
select  c.record_id, c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
c.pointcall__pointcall_rank as pointcall_rank, c.pointcall__pointcall_name as pointcall_name,
 c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
 c.cargo_item_action,
 c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
 coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
 coalesce(pp.uhgs_id, c.first_point__pointcall_uhgs_id) as cargo_item_sending_place_uhgs_id,
 labels.commodity_standardized_en ,  labels.category_portic_en ,
 labels.commodity_standardized_fr ,  labels.category_portic_fr ,
 labels.category_toflit18_Revolution_Empire  as category_toflit18_RE,
 labels.category_toflit18_Revolution_Empire_aggregate  as category_toflit18_RE_aggregate
from navigocheck.check_cargo c 
left join navigoviz.cargo_categories labels on labels.record_id = c.commodity_id
left join ports.port_points pp on  coalesce(c.cargo_item_sending_place = any(pp.toustopos), c.cargo_item_sending_place % pp.toponyme  )
-- where c."source" in  (select s."source"  from navigoviz."source" s)
order by cargo_item_sending_place
) as k
where data_block_local_id = '00304793';

select c.record_id, c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
c.cargo_item_sending_place , pp.uhgs_id
from navigocheck.check_cargo c left join ports.port_points pp
on c.cargo_item_sending_place = any(pp.toustopos ) --or c.cargo_item_sending_place % pp.toponyme
where pointcall__data_block_local_id = '00304793'
union 
select c.record_id, c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
c.cargo_item_sending_place , pp.uhgs_id
from navigocheck.check_cargo c left join ports.port_points pp
on c.cargo_item_sending_place % pp.toponyme
where pointcall__data_block_local_id = '00304793'
limit 1

Malaga	D3874565
Malaga	A0036064
Malaga	A0036549
-- Martigue
select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place 
from navigoviz.pointcall  
where source_doc_id = '00305305'
order by pointcall_rank_dedieu;

-- suite Marseille : LOADING sur In et cargo_item_sending_place 
--1.0	Bastia	A0193167	LOADING	 		Planche		
--2.0		A9999997				
--3.0	Toulon	A0200330	1	Planche	In	Bastia
--4.0	Marseille	A0210797				


select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
(all_cargos->>1)::json->>'commodity_standardized_fr' ,
(all_cargos->>1)::json->>'cargo_item_action',
(all_cargos->>1)::json->>'cargo_item_sending_place'
from navigoviz.pointcall  
where source_doc_id = '00141308'
order by pointcall_rank_dedieu;


select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' ,
(all_cargos->>1)::json->>'cargo_item_action',
(all_cargos->>1)::json->>'cargo_item_sending_place',
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id'
from navigoviz.pointcall  
where source_doc_id = '00141308'
order by pointcall_rank_dedieu;


-- suite G5 : LOADING sur Out aussi
--1.0	Redon	A0134176	LOADING 	A vide					
--2.0	Mesquer	A0148900	2	A vide	In	Redon	Sel	Out	Redon LOADING 	Sel
--3.0	Redon	A0134176							




select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
(all_cargos->>1)::json->>'cargo_item_action' as cargo_item_action,
(all_cargos->>1)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id,
(all_cargos->>2)::json->>'commodity_standardized_fr' as commodity_standardized_fr2,
(all_cargos->>2)::json->>'cargo_item_action' as cargo_item_action2,
(all_cargos->>2)::json->>'cargo_item_sending_place' as cargo_item_sending_place2,
(all_cargos->>2)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id2,
(all_cargos->>3)::json->>'commodity_standardized_fr' as commodity_standardized_fr3,
(all_cargos->>3)::json->>'cargo_item_action' as cargo_item_action3,
(all_cargos->>3)::json->>'cargo_item_sending_place' as cargo_item_sending_place3,
(all_cargos->>3)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id3,
(all_cargos->>4)::json->>'commodity_standardized_fr' as commodity_standardized_fr4,
(all_cargos->>4)::json->>'cargo_item_action' as cargo_item_action4,
(all_cargos->>4)::json->>'cargo_item_sending_place' as cargo_item_sending_place4,
(all_cargos->>4)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id4,
(all_cargos->>5)::json->>'commodity_standardized_fr' as commodity_standardized_fr5,
(all_cargos->>5)::json->>'cargo_item_action' as cargo_item_action5,
(all_cargos->>5)::json->>'cargo_item_sending_place' as cargo_item_sending_place5,
(all_cargos->>5)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id5
from navigoviz.pointcall  
where source_doc_id = '00310196'
order by pointcall_rank_dedieu;

--1.0	Ponte di Goro	A0242525		LOADING in		Passager	, Chanvre																
--2.0	Messine	A0235237				LOADING in		Divers, assortiment			, Marchandise													
--3.0	Genes	A0232521	1	Marchandise	In	Messine	A0235237			LOADING in		Riz	Divers, assortiment												
--4.0	Marseille	A0210797	5	Passager	In	Goro	A0242525	Riz	In	Gênes	A0232521	Chanvre	In	Goro	A0242525	Divers, assortiment	In	Messine	A0235237	Divers, assortiment	In	Gênes	A0232521


select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
(all_cargos->>1)::json->>'cargo_item_action' as cargo_item_action,
(all_cargos->>1)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id,
(all_cargos->>2)::json->>'commodity_standardized_fr' as commodity_standardized_fr2,
(all_cargos->>2)::json->>'cargo_item_action' as cargo_item_action2,
(all_cargos->>2)::json->>'cargo_item_sending_place' as cargo_item_sending_place2,
(all_cargos->>2)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id2,
(all_cargos->>3)::json->>'commodity_standardized_fr' as commodity_standardized_fr3,
(all_cargos->>3)::json->>'cargo_item_action' as cargo_item_action3,
(all_cargos->>3)::json->>'cargo_item_sending_place' as cargo_item_sending_place3,
(all_cargos->>3)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id3,
(all_cargos->>4)::json->>'commodity_standardized_fr' as commodity_standardized_fr4,
(all_cargos->>4)::json->>'cargo_item_action' as cargo_item_action4,
(all_cargos->>4)::json->>'cargo_item_sending_place' as cargo_item_sending_place4,
(all_cargos->>4)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id4,
(all_cargos->>5)::json->>'commodity_standardized_fr' as commodity_standardized_fr5,
(all_cargos->>5)::json->>'cargo_item_action' as cargo_item_action5,
(all_cargos->>5)::json->>'cargo_item_sending_place' as cargo_item_sending_place5,
(all_cargos->>5)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id5
from navigoviz.pointcall  
where source_doc_id = '00305305'
order by pointcall_rank_dedieu;


--1.0	Bastia	A0193167		LOADING Planche			
--2.0		A9999997					
--3.0	Toulon	A0200330	1	Planche	In	Bastia	A0193167
--4.0	Marseille	A0210797					

select source_doc_id , * from navigoviz.pointcall p where p.record_id = '00305308';
-- Toulon
select * from navigoviz.pointcall p where p.record_id = '00141308';
-- mesquer
select * from navigoviz.pointcall p where p.record_id = '00310199';
-- Genes

select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
(all_cargos->>1)::json->>'cargo_item_action' as cargo_item_action,
(all_cargos->>1)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id,
(all_cargos->>2)::json->>'commodity_standardized_fr' as commodity_standardized_fr2,
(all_cargos->>2)::json->>'cargo_item_action' as cargo_item_action2,
(all_cargos->>2)::json->>'cargo_item_sending_place' as cargo_item_sending_place2,
(all_cargos->>2)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id2,
(all_cargos->>3)::json->>'commodity_standardized_fr' as commodity_standardized_fr3,
(all_cargos->>3)::json->>'cargo_item_action' as cargo_item_action3,
(all_cargos->>3)::json->>'cargo_item_sending_place' as cargo_item_sending_place3,
(all_cargos->>3)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id3,
(all_cargos->>4)::json->>'commodity_standardized_fr' as commodity_standardized_fr4,
(all_cargos->>4)::json->>'cargo_item_action' as cargo_item_action4,
(all_cargos->>4)::json->>'cargo_item_sending_place' as cargo_item_sending_place4,
(all_cargos->>4)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id4,
(all_cargos->>5)::json->>'commodity_standardized_fr' as commodity_standardized_fr5,
(all_cargos->>5)::json->>'cargo_item_action' as cargo_item_action5,
(all_cargos->>5)::json->>'cargo_item_sending_place' as cargo_item_sending_place5,
(all_cargos->>5)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id5
from navigoviz.pointcall  
where source_doc_id = '00182772'
order by pointcall_rank_dedieu;

select outdate_fixed , pointcall , all_cargos ,
commodity_purpose, cargo_item_action    
from navigoviz.pointcall p 
where source_doc_id = '00182772'
order by pointcall_rank_dedieu ;
-- Carthagènes --> Marseille

--1.0	Carthagene	A0118264																									
--2.0	Marseille	A0210797	6	Soude	In	Carthagene	A0118264	Laine	In	Carthagene	A0118264	Soude	In-out	Carthagene	A0118264	Sparterie, espart	In	Carthagene	A0118264	Laine	In-out	Carthagene	A0118264	Sparterie, espart	In-out	Carthagene	A0118264


select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
(all_cargos->>1)::json->>'cargo_item_action' as cargo_item_action,
(all_cargos->>1)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id,
(all_cargos->>2)::json->>'commodity_standardized_fr' as commodity_standardized_fr2,
(all_cargos->>2)::json->>'cargo_item_action' as cargo_item_action2,
(all_cargos->>2)::json->>'cargo_item_sending_place' as cargo_item_sending_place2,
(all_cargos->>2)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id2,
(all_cargos->>3)::json->>'commodity_standardized_fr' as commodity_standardized_fr3,
(all_cargos->>3)::json->>'cargo_item_action' as cargo_item_action3,
(all_cargos->>3)::json->>'cargo_item_sending_place' as cargo_item_sending_place3,
(all_cargos->>3)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id3,
(all_cargos->>4)::json->>'commodity_standardized_fr' as commodity_standardized_fr4,
(all_cargos->>4)::json->>'cargo_item_action' as cargo_item_action4,
(all_cargos->>4)::json->>'cargo_item_sending_place' as cargo_item_sending_place4,
(all_cargos->>4)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id4,
(all_cargos->>5)::json->>'commodity_standardized_fr' as commodity_standardized_fr5,
(all_cargos->>5)::json->>'cargo_item_action' as cargo_item_action5,
(all_cargos->>5)::json->>'cargo_item_sending_place' as cargo_item_sending_place5,
(all_cargos->>5)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id5
from navigoviz.pointcall  
where source_doc_id = '00149602'
order by pointcall_rank_dedieu;

--1.0	Vannes	A0182952				LOADING Tonneaux vides, futailles vides					
--2.0	La Flotte en Ré	A0199508	2	Tonneaux vides, futailles vides	In	Vannes	A0182952	Tonneaux vides, futailles vides	In-out	Vannes	A0182952
--3.0	dans le Pertuis	A1968950									


-- Vannes --> La Flotte en Ré --> dans le Pertuis

select * from navigoviz.pointcall p where p.record_id = '00149602';
-- La Flotte en Ré

select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
(all_cargos->>1)::json->>'cargo_item_action' as cargo_item_action,
(all_cargos->>1)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id,
(all_cargos->>2)::json->>'commodity_standardized_fr' as commodity_standardized_fr2,
(all_cargos->>2)::json->>'cargo_item_action' as cargo_item_action2,
(all_cargos->>2)::json->>'cargo_item_sending_place' as cargo_item_sending_place2,
(all_cargos->>2)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id2,
(all_cargos->>3)::json->>'commodity_standardized_fr' as commodity_standardized_fr3,
(all_cargos->>3)::json->>'cargo_item_action' as cargo_item_action3,
(all_cargos->>3)::json->>'cargo_item_sending_place' as cargo_item_sending_place3,
(all_cargos->>3)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id3,
(all_cargos->>4)::json->>'commodity_standardized_fr' as commodity_standardized_fr4,
(all_cargos->>4)::json->>'cargo_item_action' as cargo_item_action4,
(all_cargos->>4)::json->>'cargo_item_sending_place' as cargo_item_sending_place4,
(all_cargos->>4)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id4,
(all_cargos->>5)::json->>'commodity_standardized_fr' as commodity_standardized_fr5,
(all_cargos->>5)::json->>'cargo_item_action' as cargo_item_action5,
(all_cargos->>5)::json->>'cargo_item_sending_place' as cargo_item_sending_place5,
(all_cargos->>5)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id5
from navigoviz.pointcall  
where source_doc_id = '00110062'
order by pointcall_rank_dedieu;

--1.0	Dunkerque	A0204180	1	Divers, assortiment	In-out	Dunkerque	A0204180
--2.0	Ostende	A0049808					



select * from navigoviz.pointcall p where p.record_id = '00110062';
-- Dunkerque

select pointcall_rank_dedieu, pointcall, pointcall_uhgs_id , 
jsonb_array_length(all_cargos) as nb_cargo,
commodity_standardized_fr , 
cargo_item_action , 
cargo_item_sending_place ,
cargo_item_sending_place_uhgs_id,
(all_cargos->>1)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
(all_cargos->>1)::json->>'cargo_item_action' as cargo_item_action,
(all_cargos->>1)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
(all_cargos->>1)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id,
(all_cargos->>2)::json->>'commodity_standardized_fr' as commodity_standardized_fr2,
(all_cargos->>2)::json->>'cargo_item_action' as cargo_item_action2,
(all_cargos->>2)::json->>'cargo_item_sending_place' as cargo_item_sending_place2,
(all_cargos->>2)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id2,
(all_cargos->>3)::json->>'commodity_standardized_fr' as commodity_standardized_fr3,
(all_cargos->>3)::json->>'cargo_item_action' as cargo_item_action3,
(all_cargos->>3)::json->>'cargo_item_sending_place' as cargo_item_sending_place3,
(all_cargos->>3)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id3,
(all_cargos->>4)::json->>'commodity_standardized_fr' as commodity_standardized_fr4,
(all_cargos->>4)::json->>'cargo_item_action' as cargo_item_action4,
(all_cargos->>4)::json->>'cargo_item_sending_place' as cargo_item_sending_place4,
(all_cargos->>4)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id4,
(all_cargos->>5)::json->>'commodity_standardized_fr' as commodity_standardized_fr5,
(all_cargos->>5)::json->>'cargo_item_action' as cargo_item_action5,
(all_cargos->>5)::json->>'cargo_item_sending_place' as cargo_item_sending_place5,
(all_cargos->>5)::json->>'cargo_item_sending_place_uhgs_id' as  cargo_item_sending_place_uhgs_id5
from navigoviz.pointcall  
where source_doc_id = '00331406'
order by pointcall_rank_dedieu;

--1.0	Arles	A0139070									
--2.0	Marseille	A0210797	2	Divers, assortiment (Foire de Beaucaire)	In-out	Arles	A0139070	Passager	In	Arles	A0139070


create table navigoviz.cargo as (
                select  c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
                c.pointcall__pointcall_rank as pointcall_rank, c.pointcall__pointcall_name as pointcall_name,
                c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                c.cargo_item_action,
                c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
                coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
                coalesce(pp.uhgs_id, c.first_point__pointcall_uhgs_id) as cargo_item_sending_place_uhgs_id,
                labels.commodity_standardized_en ,  labels.category_portic_en ,
                labels.commodity_standardized_fr ,  labels.category_portic_fr ,
                labels.category_toflit18_Revolution_Empire  as category_toflit18_RE,
                labels.category_toflit18_Revolution_Empire_aggregate  as category_toflit18_RE_aggregate
                from navigocheck.check_cargo c 
                left join navigoviz.cargo_categories labels on labels.record_id = c.commodity_id
                left join ports.port_points pp on coalesce(c.cargo_item_sending_place = any(pp.toustopos ), c.cargo_item_sending_place % pp.toponyme )
                -- NB : c.cargo_item_sending_place % pp.toponyme peut renvoyer plus d'une ligne de port correspondant..
                -- where c."source" in  (select s."source"  from navigoviz."source" s) --Fait buguer sur le doc id 00304793 par exemple
                ) -- 151778
                
alter table navigoviz.cargo add column cargo_item_sending_place_pointcallid text;
update navigoviz.cargo c set cargo_item_sending_place_pointcallid = p.record_id 
from navigocheck.check_pointcall p 
where c.data_block_local_id = p.data_block_local_id 
and c.cargo_item_sending_place_uhgs_id = p.pointcall_uhgs_id ;
-- 77755

alter table navigoviz.cargo add column portic boolean default false;
update navigoviz.cargo c set portic = true 
from navigocheck.check_pointcall p
where c.data_block_local_id = p.data_block_local_id ;
-- 77915

select * from navigoviz.cargo where portic is true and pointcall_name != 'Marseille'
order by data_block_local_id , pointcall_rank 

--------------------------------------------------
-- cargo_item_action : signification ??
--------------------------------------------------

select  c.cargo_item_action, count(*) as c
from navigoviz.cargo c
where portic is true --and pointcall_name != 'Marseille'
group by c.cargo_item_action
order by c;


--NULL	2
--Foundering	5
--Loading	5
--Unloading	5
--in	13
--Transit	15
--In-out	62
--In	898
--out	1205
--Sailing around	3653
--Out	31125

-- avec Marseille
--Loading	5 : LOADING
--Unloading	5 : UNLOADING
--Foundering	6 : ?
--	12 : ?
--In-out	329 : PASSING
--Transit	1129 : PASSING
--Sailing around	3653 : FISHING
--Out	34978 : LOADING
--out	1205	: LOADING
--In	36580 : UNLOADING
--in	13	: UNLOADING

-- Foundering : en train de sombrer / naufrage
select link_to_pointcall , data_block_local_id , pointcall_name , commodity_purpose , cargo_item_sending_place 
from cargo c where cargo_item_action = 'Foundering' and portic is true;


--00364222	00364222	Marseille	bois de construction	Plages Romaines
--00299872	00299869	Friou	tonneaux vuides	Gênes
--00299872	00299869	Friou	oranges	Gênes
--00297553	00297551	Plage de l' O[uest] de l' embouchure du Rhône	Marchandises	Barcellone
--00326260	00326258	Salau	Fruits secs	Nice
--00326260	00326258	Salau	anchois	Nice



select pointcall_rank_dedieu, pointcall , pointcall_action , pointcall_function , navigo_status, pointcall_in_date  
from navigoviz.pointcall p where source_doc_id = '00326258' order by pointcall_rank_dedieu;

--1.0	Nice	Out	A	PC-RS	
--2.0	Salau	Foundering		PC-RS	1779=05=22!
--3.0	Barcelonne	In-out		PC-RS	1779=06=18
--4.0	Marseille	In		PC-RS	1779=07=05

select pointcall_rank_dedieu, pointcall , pointcall_action , pointcall_function , navigo_status, pointcall_in_date  , source_suite 
from navigoviz.pointcall p where source_doc_id = '00299869' order by pointcall_rank_dedieu;

--1.0	Genes	Out	A	PC-RS	
--2.0	Agay	In-out		PC-RS	1769=03=25
--3.0	Frioul	Foundering	O	PC-RS	1769=03=31


select pointcall_rank_dedieu, pointcall , pointcall_action , pointcall_function , navigo_status, pointcall_in_date  , c.commodity_purpose , c.cargo_item_action , source_suite 
from navigoviz.pointcall p
left join navigoviz.cargo c on record_id =c.link_to_pointcall 
where source_doc_id = '00297551'  
order by pointcall_rank_dedieu;

--1.0	Barcelonne	Out	A	PC-RS			
--2.0	Plage de l' O[uest] de l' embouchure du Rhône	Foundering		PC-RS	1759=02=21	Marchandises	Foundering
--3.0	Marseille	In	O	PC-RS	1759=03=06		

------------------------------

select pointcall_rank_dedieu, record_id , pointcall , pointcall_action , pointcall_function , navigo_status, pointcall_in_date  , source_suite,
c.commodity_purpose , c.cargo_item_action , c.cargo_item_sending_place , c.cargo_item_sending_place_pointcallid ,  
c.cargo_item_owner_name , c.cargo_item_owner_class  , c.cargo_item_owner_location ,
c
from navigoviz.pointcall p
left join navigoviz.cargo c on record_id =c.link_to_pointcall 
where source_doc_id = '00364222'  
order by pointcall_rank_dedieu;


-- Abandon de cette solution 
create table navigoviz.cargo as (
                select  c.link_to_pointcall, c.pointcall__pointcall_uhgs_id ,c.pointcall__data_block_local_id as data_block_local_id, 
                c.pointcall__pointcall_rank as pointcall_rank, c.pointcall__pointcall_name as pointcall_name,
                c.commodity_purpose, c.commodity_id, c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                c.cargo_item_action,
                c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
                coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
                coalesce(pp.uhgs_id, c.first_point__pointcall_uhgs_id) as cargo_item_sending_place_uhgs_id,
                labels.commodity_standardized_en ,  labels.category_portic_en ,
                labels.commodity_standardized_fr ,  labels.category_portic_fr ,
                labels.category_toflit18_Revolution_Empire  as category_toflit18_RE,
                labels.category_toflit18_Revolution_Empire_aggregate  as category_toflit18_RE_aggregate
                from navigocheck.check_cargo c 
                left join navigoviz.cargo_categories labels on labels.record_id = c.commodity_id
                left join ports.port_points pp on coalesce(c.cargo_item_sending_place = any(pp.toustopos ), c.cargo_item_sending_place % pp.toponyme )
                -- NB : c.cargo_item_sending_place % pp.toponyme peut renvoyer plus d'une ligne de port correspondant..
                -- where c."source" in  (select s."source"  from navigoviz."source" s) --Fait buguer sur le doc id 00304793 par exemple
                )
                
drop table navigoviz.cargo;
create table navigoviz.cargo as (
    select  p.record_id as pointcall_id, p.pointcall_rank , p.pointcall_name ,
    		p.data_block_local_id ,
    			c.link_to_pointcall, c.pointcall__data_block_local_id , 
                -- c.pointcall__pointcall_rank , --as pointcall_rank
                -- c.pointcall__pointcall_name ,--as pointcall_name
                
                c.commodity_purpose, c.commodity_id, 
                c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                c.cargo_item_action,
                c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
                c.cargo_item_sending_place,
                psending.record_id as cargo_item_sending_pointcallid,
                psending.pointcall_uhgs_id as cargo_item_sending_place_uhgs_id,
                c.first_point__pointcall_name,
                c.first_point__pointcall_uhgs_id
                --coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) as cargo_item_sending_place,
                -- coalesce(pp.uhgs_id, c.first_point__pointcall_uhgs_id) as cargo_item_sending_place_uhgs_id,
                from navigocheck.check_pointcall p
                left join navigocheck.check_cargo c  on  p.record_id  = c.link_to_pointcall 
                left join navigocheck.check_pointcall psending on c.pointcall__data_block_local_id = psending.data_block_local_id and coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) = psending.pointcall_name 

                order by p.data_block_local_id, p.pointcall_rank
                ) -- 167444 / 168000
-- pas satisfaisant pour insérer tous les produits de provenance (toutes les lignes si un port envoie 2 produits avant Marseille par exemple)

drop table navigoviz.cargo;

-- etape 1 : compute sending place : cargo_item_sending_pointcallid
create table navigoviz.cargo as (
    select  
    			c.link_to_pointcall, 
    			c.pointcall__data_block_local_id ,               
                c.commodity_purpose, c.commodity_id, 
                c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                c.cargo_item_action,
                c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
                c.cargo_item_sending_place,
                psending.record_id as cargo_item_sending_pointcallid,
                psending.pointcall_uhgs_id as cargo_item_sending_place_uhgs_id,
                c.first_point__pointcall_name,
                c.first_point__pointcall_uhgs_id,
                'ORIGINAL' as data_lineage
                from  navigocheck.check_cargo c  
                left join navigocheck.check_pointcall psending on c.pointcall__data_block_local_id = psending.data_block_local_id 
                		and coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) = psending.pointcall_name 
                		and c.pointcall__pointcall_rank > psending.pointcall_rank 
                		and upper(c.cargo_item_action) = 'IN'
                order by c.pointcall__data_block_local_id, c.pointcall__pointcall_rank
); -- compute sending place : cargo_item_sending_pointcallid
-- 152333
update navigoviz.cargo set cargo_item_sending_place_uhgs_id=pp.uhgs_id 
from ports.port_points pp 
where cargo_item_sending_place is not null and cargo_item_sending_pointcallid is null 
and cargo_item_sending_place = any(pp.toustopos );
-- On complete en vérifiant les variants.
-- 1314

alter table navigoviz.cargo add column sending_pointcallid_uncertainty int default 0;

update navigoviz.cargo set cargo_item_sending_place_uhgs_id=pp.uhgs_id , sending_pointcallid_uncertainty= -1
from ports.port_points pp 
where cargo_item_sending_place is not null and cargo_item_sending_place_uhgs_id is null 
and cargo_item_sending_place % pp.toponyme; --on prend port au hasard parmi les noms ressemblants
-- 1342

update navigoviz.cargo c set cargo_item_sending_pointcallid = p.record_id 
from navigocheck.check_pointcall p 
where cargo_item_sending_pointcallid is null and c.pointcall__data_block_local_id   = p.data_block_local_id 
and c.cargo_item_sending_place_uhgs_id = p.pointcall_uhgs_id;
-- 255
       
/*
-- tests
select * from navigoviz.cargo where pointcall__pointcall_name='Marseille' 
select * from navigoviz.cargo where data_block_local_id='00182407' order by pointcall_rank  ;
select * from navigoviz.cargo where data_block_local_id='00304793' order by pointcall_rank  ;
select * from navigoviz.cargo where data_block_local_id='00310196' order by pointcall_rank  ;

--alter table navigoviz.cargo add column sending_placeid text;
--alter table navigoviz.cargo add column sending_place_uhgs_id text;
--alter table navigoviz.cargo add column sending_place text;
--
--update navigoviz.cargo set sending_place = coalesce(cargo_item_sending_place, first_point__pointcall_name);
--update navigoviz.cargo set sending_place_uhgs_id = coalesce(cargo_item_sending_place_uhgs_id, first_point__pointcall_uhgs_id);
--update navigoviz.cargo set sending_placeid = cargo_item_sending_pointcallid;

alter table navigoviz.cargo add column portic_cargo_action text ;
update navigoviz.cargo set portic_cargo_action = upper(cargo_item_action);


-- insert into navigoviz.cargo
update navigoviz.cargo c 
set commodity_purpose = m.commodity_purpose, commodity_id = m.commodity_id,
portic_cargo_action = case when m.portic_cargo_action in ('IN', 'LOADING') then 'OUT' else (case when m.portic_cargo_action in ('OUT', 'UNLOADING') then 'IN' else null end) end
from 
(select * from navigoviz.cargo where link_to_pointcall is not null ) as m
where m.cargo_item_sending_pointcallid = c.pointcall_id 
-- 61806

delete from navigoviz.cargo where data_block_local_id is null;
-- 43900 
*/

-- etape 2 : add sending place
-- add sending place
insert into navigoviz.cargo 
(select distinct cargo_item_sending_pointcallid as link_to_pointcall,
	pointcall__data_block_local_id ,               
    commodity_purpose, c.commodity_id, 
    quantity, quantity_u,
    case when upper(cargo_item_action) in ('IN', 'LOADING') then 'OUT' else upper(cargo_item_action) end as cargo_item_action,
    cargo_item_owner_name, cargo_item_owner_class, cargo_item_owner_location,
    cargo_item_sending_place,
    cargo_item_sending_pointcallid,
    cargo_item_sending_place_uhgs_id,
    first_point__pointcall_name,
    first_point__pointcall_uhgs_id,
    'ADDED_PORTIC' as data_lineage                
from navigoviz.cargo  c, 

(select distinct cargo_item_sending_pointcallid as link_to_pointcall, commodity_id
from navigoviz.cargo 
except 
(select distinct link_to_pointcall, commodity_id
from navigoviz.cargo)) as filtre


 where c.cargo_item_sending_pointcallid = filtre.link_to_pointcall and c.commodity_id=filtre.commodity_id
-- upper(cargo_item_action) in ('IN', 'LOADING', 'TRANSIT', 'IN-OUT')
-- where cargo_item_sending_pointcallid||commodity_id  not in (select distinct link_to_pointcall||commodity_id  from cargo)
)
-- 39242

-- 38965
-- 151785
-- 93573

select distinct cargo_item_sending_pointcallid as link_to_pointcall, commodity_id
from navigoviz.cargo 
except 
(select distinct link_to_pointcall, commodity_id
from navigoviz.cargo)
-- 39482

select distinct cargo_item_sending_pointcallid as link_to_pointcall, commodity_id
from navigoviz.cargo 


alter table navigoviz.cargo add column portic boolean default false;
update navigoviz.cargo c set portic = true 
            from navigocheck.check_pointcall p
            where c.pointcall__data_block_local_id = p.data_block_local_id;
-- 116635
-- 117713
delete from navigoviz.cargo where portic is false;
-- 73862

alter table navigoviz.cargo add column commodity_standardized_fr text;
alter table navigoviz.cargo add column commodity_standardized_en text;
alter table navigoviz.cargo add column category_portic_fr text;
alter table navigoviz.cargo add column category_portic_en text;
alter table navigoviz.cargo add column category_toflit18_revolution_empire text;
alter table navigoviz.cargo add column category_toflit18_revolution_empire_aggregate text;

update navigoviz.cargo c set 
commodity_standardized_fr = labels.commodity_standardized_fr,
commodity_standardized_en = labels.commodity_standardized_en,
category_portic_fr = labels.category_portic_fr,
category_portic_en = labels.category_portic_en,
category_toflit18_revolution_empire = labels.category_toflit18_revolution_empire,
category_toflit18_revolution_empire_aggregate = labels.category_toflit18_revolution_empire_aggregate
from navigoviz.cargo_categories labels where labels.record_id = c.commodity_id;
-- 117477

select * from navigoviz.cargo c
-- etape 3 : PAS UTILE DE LA GARDER, mais bien pour diagnostique transport
-- Faire la jointure left sur pointcall 
drop table cargo_pointcall;
create table navigoviz.cargo_pointcall as (
    select  p.record_id as pointcall_id, p.pointcall_rank , p.pointcall_name ,
    p.data_block_local_id , c.*
    from navigocheck.check_pointcall p
    left join navigoviz.cargo c  on  p.record_id  = c.link_to_pointcall 
    order by p.data_block_local_id, p.pointcall_rank
); -- 183709 / 184282 / 183512 / 182606



                
-- vérif avec des BON CAS DE TESTS
                
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00182407' order by pointcall_rank  ;
-- Salatée - Tripoli de Sirie - Chypre - Porte comte - Toulon - Marseille
select cargo_item_action, data_lineage, *  from navigoviz.cargo_pointcall where data_block_local_id='00304793' order by pointcall_rank  ;
-- Malaga (fruits secs) - La Roquette (soude) - Sette [Sète] (soude, fruits secs) - Tignes - Marseille (soude, fruits secs)
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00310196' order by pointcall_rank  ;
-- Goro (chanvre, passagers) - Messine (marchandises1, marchandises2) - Genes (marchandises1, riz, marchandises3) - Marseille (chanvre, passagers, riz,marchandises2, marchandises3)
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00108236' order by pointcall_rank  ;
-- Cherbourg (Lest) - Marennes(?)
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00062253' order by pointcall_rank  ;
-- Sables d' Olonne (Lest) - Saint Savinien(?)
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00062246' order by pointcall_rank  ;
-- Sables d' Olonne (Bois de construction) -- La Rochelle (?)
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00305305' order by pointcall_rank  ;
-- Bastia en Corse (Planches) -- Divers ports d' Italie -- Toulon (Planches) -- Marseille
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00141308' order by pointcall_rank  ;
-- Redon (à vide) -- Mesquer (Sel (out) et à vide (in))  - Redon
select cargo_item_action, * from navigocheck.check_cargo cc  where pointcall__data_block_local_id ='00141308' ;
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_block_local_id='00149465' order by pointcall_rank  ;
-- Chalon ( Chaux)  - La Flotte en Ré (Out Vide, In Chaux) -- Chalon (?) 
select cargo_item_action, * from navigocheck.check_cargo cc  where pointcall__data_block_local_id ='00149465' ;

select ship_name , ship_id , * from navigo.pointcall p where data_block_local_id = '00141308'

-- vérif avec des BON CAS DE TESTS
select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall 
where data_block_local_id in ('00182407', '00304793', '00310196', '00108236', '00062253', '00062246', '00305305', '00141308', '00149465')   order by data_block_local_id, pointcall_rank  ;

select ship_id, ship_name, captain_name  from pointcall where source_doc_id = '00141308';
select ship_id, ship_name, captain_name  from pointcall where source_doc_id = '00305305';

select departure , all_cargos,  destination, destination_all_cargos  
from built_travels t where t.source_doc_id = '00141308'
order by travel_id ;

select departure , all_cargos,  destination, destination_all_cargos  
from raw_flows t  where t.source_doc_id = '00149465'
order by travel_id ;
-- [{"commodity_id": "00000469", "data_lineage": "ADDED_PORTIC_G5", "cargo_item_action": "IN", "commodity_purpose": "Vide", "link_to_pointcall": "00155435", "category_portic_en": "Ballast and empty", "category_portic_fr": "Sur lest et vides", "commodity_standardized_en": "Empty", "commodity_standardized_fr": "A vide", "first_point__pointcall_name": "Chalon", "first_point__pointcall_uhgs_id": "A1969195", "pointcall__data_block_local_id": "00155435", "sending_pointcallid_uncertainty": 0, "category_toflit18_revolution_empire": "Produits inconnus", "category_toflit18_revolution_empire_aggregate": "Divers"}]

select departure , all_cargos,  destination, destination_all_cargos  
from raw_flows t  where t.source_doc_id = '00062246'
order by travel_id ;
-- [{"commodity_id": "00000469", "data_lineage": "ORIGINAL", "cargo_item_action": "In", "commodity_purpose": "A vide", "link_to_pointcall": "00141308", "category_portic_en": "Ballast and empty", "category_portic_fr": "Sur lest et vides", "commodity_standardized_en": "Empty", "commodity_standardized_fr": "A vide", "first_point__pointcall_name": "Redon", "cargo_item_sending_pointcallid": "00143770", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00141308", "sending_pointcallid_uncertainty": 0, "cargo_item_sending_place_uhgs_id": "A0134176", "category_toflit18_revolution_empire": "Produits inconnus", "category_toflit18_revolution_empire_aggregate": "Divers"}, 
-- {"commodity_id": "00000129", "data_lineage": "ORIGINAL", "cargo_item_action": "Out", "commodity_purpose": "Sel", "link_to_pointcall": "00141308", "category_portic_en": "Other foodstuff", "category_portic_fr": "Autres produits alimentaires", "commodity_standardized_en": "Salt", "commodity_standardized_fr": "Sel", "first_point__pointcall_name": "Redon", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00141308", "sending_pointcallid_uncertainty": 0, "category_toflit18_revolution_empire": "Sel", "category_toflit18_revolution_empire_aggregate": "Subsistances"}]
-- [{"commodity_id": "00000469", "data_lineage": "ADDED_PORTIC_G5", "cargo_item_action": "In", "commodity_purpose": "A vide", "link_to_pointcall": "00143746", "category_portic_en": "Ballast and empty", "category_portic_fr": "Sur lest et vides", "commodity_standardized_en": "Empty", "commodity_standardized_fr": "A vide", "first_point__pointcall_name": "Redon", "cargo_item_sending_pointcallid": "00143770", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00143746", "sending_pointcallid_uncertainty": 0, "cargo_item_sending_place_uhgs_id": "A0134176", "category_toflit18_revolution_empire": "Produits inconnus", "category_toflit18_revolution_empire_aggregate": "Divers"}, 
{"commodity_id": "00000129", "data_lineage": "ADDED_PORTIC_G5", "cargo_item_action": "IN", "commodity_purpose": "Sel", "link_to_pointcall": "00143746", "category_portic_en": "Other foodstuff", "category_portic_fr": "Autres produits alimentaires", "commodity_standardized_en": "Salt", "commodity_standardized_fr": "Sel", "first_point__pointcall_name": "Redon", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00143746", "sending_pointcallid_uncertainty": 0, "category_toflit18_revolution_empire": "Sel", "category_toflit18_revolution_empire_aggregate": "Subsistances"}]
-- [{"commodity_id": "00000469", "data_lineage": "ORIGINAL", "cargo_item_action": "In", "commodity_purpose": "A vide", "link_to_pointcall": "00141308", "category_portic_en": "Ballast and empty", "category_portic_fr": "Sur lest et vides", "commodity_standardized_en": "Empty", "commodity_standardized_fr": "A vide", "first_point__pointcall_name": "Redon", "cargo_item_sending_pointcallid": "00143770", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00141308", "sending_pointcallid_uncertainty": 0, "cargo_item_sending_place_uhgs_id": "A0134176", "category_toflit18_revolution_empire": "Produits inconnus", "category_toflit18_revolution_empire_aggregate": "Divers"}, {"commodity_id": "00000129", "data_lineage": "ORIGINAL", "cargo_item_action": "Out", "commodity_purpose": "Sel", "link_to_pointcall": "00141308", "category_portic_en": "Other foodstuff", "category_portic_fr": "Autres produits alimentaires", "commodity_standardized_en": "Salt", "commodity_standardized_fr": "Sel", "first_point__pointcall_name": "Redon", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00141308", "sending_pointcallid_uncertainty": 0, "category_toflit18_revolution_empire": "Sel", "category_toflit18_revolution_empire_aggregate": "Subsistances"}]
select '{"a":[1,2,3,4,5]}'::jsonb @@ '$.a[*] > 2' 
select '[{"a":1}, {"a":2}, {"a":0}]'::jsonb @@ '$.a[*] > 2' 
select jsonb_path_query_array('{"a":[1,2,3,4,5]}', '$.a[*] ? (@ >= $min && @ <= $max)', '{"min":2, "max":4}') 
select jsonb_path_query_array('[{"a":1}, {"a":2}, {"a":0}]', '$.a[*] ? (@ >= 1 )') 

select '[{"a":1}, {"a":2}, {"a":0}]'::jsonb @@ '$.a[*] > 2' , ('[{"a":1}, {"a":2}, {"a":0}]'::jsonb)[?(@.a > 1)]
$..book[?(@.title contains 'Lord')]

select jsonb_array_elements_text(all_cargos->>'cargo_item_action' @>'Out') 
from built_travels t where t.source_doc_id = '00141308'
order by travel_id ;

select '[{"cargo_item_action":"In"}, {"cargo_item_action":"Out"}, {"cargo_item_action":"In-out"}]'::jsonb @@ '$.cargo_item_action[*] == "Out"' 
select '[{"cargo_item_action":"In"}, {"cargo_item_action":"Out"}, {"cargo_item_action":"In-out"}]'::jsonb @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"' 

select all_cargos @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"', 
destination_all_cargos @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"'
from built_travels t where t.source_doc_id = '00141308'
order by travel_id ;

select jsonb_path_query_array(all_cargos, '$.cargo_item_action[*] ? (@ == "Out" )') 
from built_travels t where t.source_doc_id = '00141308'
order by travel_id ;

select jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")') 
from built_travels t where t.source_doc_id = '00141308'
order by travel_id ;
-- [{"commodity_id": "00000129", "data_lineage": "ORIGINAL", "cargo_item_action": "Out", "commodity_purpose": "Sel", "link_to_pointcall": "00141308", "category_portic_en": "Other foodstuff", "category_portic_fr": "Autres produits alimentaires", "commodity_standardized_en": "Salt", "commodity_standardized_fr": "Sel", "first_point__pointcall_name": "Redon", "first_point__pointcall_uhgs_id": "A0134176", "pointcall__data_block_local_id": "00141308", "sending_pointcallid_uncertainty": 0, "category_toflit18_revolution_empire": "Sel", "category_toflit18_revolution_empire_aggregate": "Subsistances"}]

-- etape 4 : compiler les cargaisons par pointcalls (OUT ou IN)
create table navigoviz.pointcall_cargo as (
            select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'data_block_local_id' as data_block_local_id,
            jsonb_array_length(jsonb_agg(to_jsonb(r.*) )) as nb_cargo,
			((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'data_lineage' as data_lineage,
			
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_purpose' as commodity_purpose,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_id' as commodity_id,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity' as quantity,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'quantity_u' as quantity_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'cargo_item_action' as cargo_item_action,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'cargo_item_sending_place' as cargo_item_sending_place,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'cargo_item_sending_place_uhgs_id' as cargo_item_sending_place_uhgs_id,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'cargo_item_sending_pointcallid' as cargo_item_sending_pointcallid,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'commodity_standardized_fr' as commodity_standardized_fr,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'category_portic_fr' as category_portic_fr,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'category_toflit18_revolution_empire' as category_toflit18_RE,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'category_toflit18_revolution_empire_aggregate' as category_toflit18_RE_aggregate,
            jsonb_agg(to_jsonb(r.*) ) as all_cargos
            from (
                select * from navigoviz.cargo as c 
                --left join navigoviz.cargo_categories labels on labels.record_id = c.commodity_id
                --where portic is true
            ) as r
             
            group by r.link_to_pointcall
            );

----------------------------------------------------------------------------------
-- fini : on passe à raw_flows ?
----------------------------------------------------------------------------------

-- Dans raw_flow, adaptation pour les cargaisons du G5 en OUT : si pas de cargaison définie à l'arrivée, elles sont marquées en IN. 

select round(s.length_miles ::float) , * 
from navigo.stages s
where length != 'xxx';

-- si length en length_unit alors length_miles aussi renseigné ? OUI
select * from navigo.stages s 
where s.length_unit is not null and length_miles is null;

-- symetrique ? : non
select * from navigo.stages s1,navigo.stages s2
where s1.st01_id = s2.st02_id and s1.st02_id = s2.st01_id
and s1.length_miles != s2.length_miles;
-- 586  
select * from navigo.stages s1,navigo.stages s2
where s1.st01_id = s2.st02_id and s1.st02_id = s2.st01_id
and s1.length != s2.length;
-- 120

select * from navigo.stages s1,navigo.stages s2
where s1.st01_id = s2.st02_id and s1.st02_id = s2.st01_id
and s1.length_miles != s2.length_miles
union
(select * from navigo.stages s1,navigo.stages s2
where s1.st01_id = s2.st02_id and s1.st02_id = s2.st01_id
and s1.length != s2.length);
-- export pour Silvia et Pierre 

select * from navigo.stages s1,navigo.stages s2
where s1.st01_id = s2.st02_id and s1.st02_id = s2.st01_id
and s1.length_miles = s2.length_miles and s1.length_miles is not null;
-- 1606 à intégrer (on va la considérer comme symétrique)

select count(*) from navigo.stages s1
where length != 'xxx' and length_miles is not null;
-- 3656



-- un float ? oui
select * from navigo.stages where navigo.test_double_type(length_miles) is false and length_miles is not null;

select distinct length from navigo.stages where navigo.test_double_type(length) is false and length is not null;
-- 'xxx' à écarter de la requête (car en plus length_miles vaut alors 0.0)

select * from navigo.stages s 
where s.length_unit is not null and navigo.test_double_type(length) is false; --and length_miles is null;
-- 7528	A1683009	Sainte Croix de Barbarie	A0120232	Cadix	xxx	xxx	0.0
-- 9681	A0230165	Syracuze	A1383518	Famagouste	xxx	xxx	0.0

select distinct length_unit from navigo.stages s ;
-- NULL
-- leagues
-- xxx
-- km

select  * from navigo.stages s 
where length_miles = '0.0' 
and st02_id not in ('H8888888', 'H4444444', 'H9999999', 'A9999997') 
and st01_id not in ('H8888888','H4444444','H9999999', 'A9999997')
and st01_id!=st02_id 
and length !='xxx';
-- 4740	A0148992	Nice	A9999997	Various European and Mediterranean ports	xxx		0.0

select round(s.length_miles ::float) , * 
from navigo.stages s
where length != 'xxx';
-- requete OK

--------------------------------------------
--- penser à faire un bridge dans raw_flows en écartant ces departs ou ces destinations
--  ('H8888888','H4444444','H9999999', 'A9999997')
-- raw_flows
--  calculer les paths sur raw_flows.
--------------------------------------------

-- dans pointcall_uncertainty function (SKIPPED FOR THE MOMENT)
alter table navigoviz.pointcall add column if not exists pointcall_outdate_uncertainity int default 0;
update navigoviz.pointcall set pointcall_outdate_uncertainity = -1 where source_suite = 'G5';
-- 86200

select count(travel_id) from navigoviz.raw_flows;
-- 74254

-- vérifs des stages
select source_suite , departure , departure_uhgs_id , destination , destination_uhgs_id , source_doc_id 
-- , travel_id , nb_cargo, commodity_purpose, cargo_item_action, destination_nb_cargo, destination_commodity_purpose, destination_cargo_item_action , *
from navigoviz.raw_flows r 
where r.distance_dep_dest_miles is null 
and departure_uhgs_id not in ('H8888888', 'H6666666', 'H4444444', 'H9999999', 'A9999997' ) 
and destination_uhgs_id not in ('H8888888', 'H6666666', 'H4444444', 'H9999999', 'A9999997' ) ;
-- and source_doc_id='00105589';
-- G5	Boulogne sur Mer	A0152606	Angleterre	A0390929	00105589
-- G5	Saint Martin de Ré	A0127055		A1969195	00153910
[{"pkid": 539, "portic": true, "quantity": null, "record_id": "00000542", "quantity_u": null, 
"commodity_id": "00000542", "data_lineage": "ADDED_PORTIC", "portic_marseille": "Oui", "cargo_item_action": "OUT", "commodity_purpose": "Chaux", "link_to_pointcall": "00155435", "category_portic_en": "Fuels, intermediate products or other materials", "category_portic_fr": "Combustibles, produits intermédiaires et autres matières", "cargo_item_owner_name": null, "cargo_item_owner_class": null, "cargo_item_sending_place": null, "cargo_item_owner_location": null, "commodity_standardized_en": "Lime", "commodity_standardized_fr": "Chaux", "first_point__pointcall_name": "Chalon", "cargo_item_sending_pointcallid": "00155435", "first_point__pointcall_uhgs_id": "A1969195", "pointcall__data_block_local_id": "00149465", "sending_pointcallid_uncertainty": 0, "cargo_item_sending_place_uhgs_id": "A1969195", "category_toflit18_revolution_empire": "Matières premières diverses", "category_toflit18_revolution_empire_aggregate": "Matières premières"}, {"pkid": 466, "portic": true, "quantity": null, "record_id": "00000469", "quantity_u": null, 
"commodity_id": "00000469", "data_lineage": "ADDED_PORTIC", "portic_marseille": "Non", "cargo_item_action": "OUT", "commodity_purpose": "Vide", "link_to_pointcall": "00155435", "category_portic_en": "Ballast and empty", "category_portic_fr": "Sur lest et vides", "cargo_item_owner_name": null, "cargo_item_owner_class": null, "cargo_item_sending_place": null, "cargo_item_owner_location": null, "commodity_standardized_en": "Empty", "commodity_standardized_fr": "A vide", "first_point__pointcall_name": "Chalon", "cargo_item_sending_pointcallid": "00155435", "first_point__pointcall_uhgs_id": "A1969195", "pointcall__data_block_local_id": "00149465", "sending_pointcallid_uncertainty": 0, "cargo_item_sending_place_uhgs_id": "A1969195", "category_toflit18_revolution_empire": "Produits inconnus", "category_toflit18_revolution_empire_aggregate": "Divers"}]

select * from navigo.stages s
where s.st01_id = 'A0390929' and s.st02_id = 'A0152606';

select * from navigo.pointcall p where data_block_local_id = '00106339' order by p.pointcall_rank ;
select * from navigo.pointcall p where data_block_local_id = '00108554' order by p.pointcall_rank ;
select * from navigo.pointcall p where data_block_local_id = '00153910' order by p.pointcall_rank ;
select * from navigo.pointcall p where data_block_local_id = '00164545' order by p.pointcall_rank ;

00164545
select * from ports.port_points pp where pp.uhgs_id = 'A1969195'
update ports.port_points pp set toponyme = 'Chalon' where pp.uhgs_id = 'A1969195';
update navigoviz.pointcall  pp set pointcall = 'Chalon' where pp.pointcall_uhgs_id = 'A1969195';
update navigoviz.raw_flows   set destination = 'Chalon' where destination_uhgs_id = 'A1969195';
update navigoviz.raw_flows   set departure = 'Chalon' where departure_uhgs_id = 'A1969195';

select * from navigo.stages s
where s.st01_id = 'A0152606' and s.st02_id = 'A0390929';
-- 459	A0152606	Boulogne sur Mer	A0390929	England (Kingdom)			26.0

update navigoviz.raw_flows t set distance_dep_dest_miles = q.distance_dep_dest_miles
            from (
                    select distinct k.travel_id, round(length_miles::float ::float)  as  distance_dep_dest_miles from (
                        SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,   length_miles
                        FROM navigoviz.raw_flows t , navigo.stages s 
                        where length_miles is not null  and (length is null or length != 'xxx' )
                        and t.departure_uhgs_id = s.st01_id and t.destination_uhgs_id = s.st02_id 
                    ) as k
            ) as q
            where q.travel_id = t.travel_id;
           
select * from navigo.stages s
where s.st01_id = 'A0152606' and s.st02_id = 'A0390929';

select * from navigoviz.pointcall p where p.source_doc_id = '00305305'


select cargo_item_action, data_lineage, * from navigoviz.cargo_pointcall where data_lineage is null order by data_block_local_id, pointcall_rank  ;


select * from navigoviz.cargo_pointcall where pointcall_name = 'Marennes'


select pointcall_rank_dedieu, pointcall , pointcall_action , pointcall_function , navigo_status, pointcall_in_date  , c.commodity_purpose , c.cargo_item_action , source_suite 
from navigoviz.pointcall p
left join navigoviz.cargo c on record_id =c.link_to_pointcall 
where source_doc_id = '00297551'  
order by pointcall_rank_dedieu;




select * from navigoviz.pointcall where source_doc_id  = '00337716' order by pointcall_rank_dedieu ;
-- Marennes --> Libourne : poste du sel à Libourne
--00337716	Marennes	A0136930
--00347617	Libourne	A0215306

select * from navigoviz.pointcall where source_doc_id  = '00133054' order by pointcall_rank_dedieu ;

select data_block_local_id, link_to_pointcall , pointcall_name , cargo_item_action , commodity_purpose , cargo_item_sending_place , cargo_item_sending_place_pointcallid 
from navigoviz.cargo 
where portic is true 
and cargo_item_action not in  ('Out', 'Sailing around')
-- and pointcall_name != 'Marseille'
order by data_block_local_id , pointcall_rank  ;

--------------------------------------------
-- pointcall uncertainty : pb sur pointcall_out_date2 et outdate_fixed si 
--------------------------------------------

select pointcall_out_date, pointcall_out_date2 , pointcall_in_date,  pointcall_in_date2, outdate_fixed, indate_fixed from pointcall p 
where p.date_fixed is null and substring(pointcall_out_date for 4)!='1799';
-- 1787<10<99!
-- 1787<09<99!
-- 1799...
select * from navigocheck.check_pointcall cp where cp.pointcall_outdate in ('1787<10<99!', '1787<09<99!');
select pointcall_out_date, replace((replace(replace(replace(pointcall_out_date, '<99', '<28'), '<', '-'), '!', '')::date - 1)::text, '-', '=')  , * from navigoviz.pointcall
where position('<99' in pointcall_out_date ) > 0;

-- correction
update navigoviz.pointcall set pointcall_out_date2 = replace((replace(replace(replace(pointcall_out_date, '<99', '<28'), '<', '-'), '!', '')::date - 1)::text, '-', '=') 
where position('<99' in pointcall_out_date ) > 0;

update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(pointcall_out_date, '>', '=') end  
            where substring(pointcall_out_date for 4)='1799';

select pointcall_out_date, pointcall_out_date2 , pointcall_in_date,  pointcall_in_date2, outdate_fixed, indate_fixed from pointcall p 
where p.date_fixed is null and  p.pointcall_out_date2 is null ;
-- correction ok

select source_doc_id , * from navigoviz.pointcall p 
where date_fixed ='1789-02-15' and navigo_status is null;

select * from navigocheck.check_pointcall cp 
where data_block_local_id ='00308614' order by pointcall_rank ;

select * from navigocheck.check_pointcall cp 
where cp.pointcall_status  is null and data_block_local_id is not null;
-- 00315649
-- 00323592

select record_id , (cp.pointcall_status is null) as corrige, pointcall_name , pointcall_rank , pointcall_function , pointcall_action , pointcall_status, * from navigo.pointcall cp 
where data_block_local_id in 
(select data_block_local_id from navigocheck.check_pointcall cp 
where cp.pointcall_status  is null and data_block_local_id is not null)
order by data_block_local_id, pointcall_rank;

update navigo.pointcall set pointcall_status = 'PC-RS'
where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
'00364778', '00364786', '00364788', '00364787', '00364794', '00364796'); 

update navigocheck.check_pointcall set pointcall_status = 'PC-RS'
where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
'00364778', '00364786', '00364788', '00364787', '00364794', '00364796');

update navigoviz.pointcall set pointcall_status = 'PC-RS'
where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
'00364778', '00364786', '00364788', '00364787', '00364794', '00364796');

--attention doc 00323268 
--00364788	true	Falmouth en Angleterre
--00364787	true	Sur Carthagène

--00364213 : FC-RS / doc 00315649
select * from navigoviz.pointcall p where navigo_status ='FC-RS' and source_suite !='G5';

-- correction manuelle
update navigoviz.pointcall set navigo_status = 'FC-RS', pointcall_rank_dedieu = '4.0' where record_id = '00364213';
update navigocheck.check_pointcall set pointcall_status = 'FC-RS', pointcall_rank = '4.0' where record_id = '00364213';
update navigo.pointcall set pointcall_status = 'FC-RS', pointcall_rank = '4.0' where record_id = '00364213';

-- analyse des résultats
-- public.test_pointcall
-- public.pointcall_checked

select count(*) from navigoviz.pointcall p ;
-- 139314
select count(*) from public.test_pointcall p ;
-- 93599
select count(*) from public.pointcall_checked p ;
-- 93599

-- les pointcall qui n'ont pas été analysés alors qu'ils avaient un ship_id
select ship_id, net_route_marker, source_suite, source_component , source_doc_id, pointcall_rank_dedieu, * from navigoviz.pointcall p
where p.pkid not in (select pkid from test_pointcall) and ship_id is not null and (net_route_marker is null or net_route_marker != 'Q' );  
-- ok ca corrige ceux là 2374 NULL net_route_marker + 614 Q net_route_marker = 2988

select ship_id, net_route_marker, source_suite, source_component , source_doc_id, pointcall_rank_dedieu, * from navigoviz.pointcall p
where p.pkid not in (select pkid from test_pointcall) and ship_id is  null ;-- and (net_route_marker is null or net_route_marker != 'Q' ); 
-- 42734

select 42734 + 2988 + 93599 -- 139321 > 139314 (ok on peut avoir des ship_id vide et net_route_marker vide ?)
-- c'est bon

-- Question : est-ce qu'on corrige / vérifie les entrées dont le net_route_marker est non null, != de Q mais le ship_id est vide
select ship_id, net_route_marker, source_suite, source_component , source_doc_id, pointcall_rank_dedieu, * from navigoviz.pointcall p
where p.pkid not in (select pkid from test_pointcall) and ship_id is  null and (net_route_marker is not null or net_route_marker != 'Q' ) ;
-- 180 lignes...

select  net_route_marker , count(*)
from navigoviz.pointcall p
group by net_route_marker;
--A	82628
--Z	11145
--z	1
--NULL	44928
--Q	614
--Y	5

select * from navigo.pointcall p where p.net_route_marker in ('Y', 'z');


-- ppur ces entrées non encore codées, on peut spécifier le niveau d'incertitude
-- si santé et PC-RS ou PU-RS alors 0
-- si santé et PC-RS et date inexacte, alors 
select ship_id, net_route_marker, source_suite, source_component , source_doc_id, pointcall_rank_dedieu, * from navigoviz.pointcall p
where p.pkid not in (select pkid from test_pointcall) and ship_id is  null; --and (net_route_marker is not null or net_route_marker != 'Q' ) ;
-- 42730

select p.navigo_status, count(*) from navigoviz.pointcall p
where p.pkid not in (select pkid from test_pointcall) and ship_id is  null --and (net_route_marker is not null or net_route_marker != 'Q' ) ;
group by navigo_status;

select ship_id, net_route_marker, source_suite, source_component , source_doc_id, pointcall_rank_dedieu, navigo_status , * from navigoviz.pointcall p
where ship_id is  null and navigo_status like '%-ML';

select * from navigo.pointcall p where p.pointcall_status  = 'PC-RS1';

select postgis_version()

-- FC-RS	414 : santé -2
-- PC-RS	27889 : santé 0
-- PC-RS1	1 : santé 0 -- erreur
-- PU-RS	36 : santé 0 ou -5 (infirmés par les sources elles-mêmes, ils n'y sont pas allés)

-- FC-RF	6809 : congés -2 futur
-- FG-RF	1 : congés -2 sailing around
-- PG-RF	345 : congés -2 sailing around
-- FA-RF	2  : congés alternative -2
-- PC-RF	7111: congés 0 si function = O, -2 sinon

-- FC-ML	63 Expéditions coloniales Marseille (1789) : -2	
-- PC-ML    59 Expéditions coloniales Marseille (1789) : 0
update navigoviz.pointcall set pointcall_uncertainity = 0, 
certitude='Observé', certitude_reason = 'rules without ship_id: PC-RS, PU-RS, or (PC-RF and function = O)'
where ship_id is  null 
and (navigo_status in ('PC-RS', 'PC-RS1', 'PU-RS', 'PC-ML') or (navigo_status in ('PC-RF', 'PG-RF') and pointcall_function ='O'));
-- 34949 (fait sur local V8) et 35298 sur le serveur v8

update navigoviz.pointcall set pointcall_uncertainity = -2, 
certitude='Déclaré', certitude_reason = 'rules without ship_id: FC-RS, FC-RF, FG-RF, PG-RF, FA-RF, FC-ML or (PC-RF and function != O)'
where ship_id is  null 
and (navigo_status in ('FC-RS', 'FC-RF', 'FG-RF', 'PG-RF', 'FA-RF', 'FC-ML') or (navigo_status='PC-RF' and pointcall_function !='O'));
-- 7780 (fait sur local V8 et le server V8)

select 7717 + 34890 -- 42607 à comparer aux 42630
select 7780 + 34949 -- 42729 à comparer aux 42630

-- changer raw_flows pour faire comme dans uncertainity_travels/ built_travels

--------------------------------------------------------------------------
-- rajouter une midate date_fixed devient midate_fixed
--------------------------------------------------------------------------

select * from navigo.pointcall p2 where pointcall_outdate like '1878%';
-- 00143961
update navigo.pointcall set pointcall_outdate = '1787<02<25!' where record_id = '00143961' and pointcall_outdate like '1878%';
select * from navigo.pointcall p2 where pointcall_outdate like '1479%';
select * from navigo.pointcall where data_block_local_id = '00320921' order by pointcall_rank ;
update navigo.pointcall set pointcall_outdate = '1749=10=04' where record_id = '00320922' and pointcall_outdate = '1479=10=04';
-- 00320922 / 00320921

--------------------------------------------------------------------------
-- article Histoire et Mesure : chiffrage des sources : janvier 2023 sur V7 ou v8
--------------------------------------------------------------------------


select source_subset,  extract(year from p.date_fixed), count(distinct source_doc_id)
from navigoviz.pointcall p 
where p.pointcall_function  = 'O' and extract(year from p.date_fixed) not in (1780, 1781,1782,1783,1784,1785,1788,1786, 1790, 1791, 1878, 1479)
group by source_subset , extract(year from p.date_fixed);

select distinct source_component  from navigoviz.pointcall p where source_subset='Registre du petit cabotage (1786-1787)'
-- 9906

select source_component, extract(year from p.date_fixed), count(distinct source_doc_id), count(source_doc_id)
from navigoviz.pointcall p 
where p.pointcall_function  = 'O' and extract(year from p.date_fixed) not in (1780, 1781,1782,1783,1784,1785,1788, 1790, 1791, 1878, 1479)
group by source_component , extract(year from p.date_fixed) 
order by source_component , extract(year from p.date_fixed) ;

Registre du petit cabotage (1789-1790) ADBdR, 200 E 608 
Registre du petit cabotage (1787-1789) ADBdR, 200 E 607 543
Registre du petit cabotage (1786-1787) ADBdR, 200 E 606

select source_subset,  extract(year from p.date_fixed), count(distinct source_doc_id)
from navigoviz.pointcall p 
where extract(year from p.date_fixed) not in (1748, 1758, 1768, 1778, 1780, 1781,1782,1783,1784,1785,1788,1786, 1790, 1791,1798, 1878, 1479)
group by source_subset , extract(year from p.date_fixed);--p.pointcall_function  = 'O' and  : fausse le compte pour 1779

select source_subset,  extract(year from p.date_fixed), count(distinct source_doc_id),  array_agg(distinct pointcall_function)
from navigoviz.pointcall p 
where  extract(year from p.date_fixed) =1779
group by source_subset , extract(year from p.date_fixed);
-- {A,O,T,NULL}

select  extract(year from p.date_fixed), pointcall_function, source_doc_id, p.pointcall , pointcall_rankfull 
from navigoviz.pointcall p 
where  extract(year from p.date_fixed) =1779
order by source_doc_id, pointcall_rankfull;

select * from navigo.pointcall p 
where p.data_block_local_id  not in (select distinct data_block_local_id from navigoviz."source" s)

select count(*) from ports.port_points
select count(*) from navigoviz.cargo_categories cc ;

select count(*) from navigo.cargo c where link_to_pointcall is not null
and link_to_pointcall in (select distinct record_id from navigoviz.pointcall p);
-- 151746 / 77882
select count(*) from navigo.cargo c where link_to_pointcall is not null and commodity_id is null
and link_to_pointcall in (select distinct record_id from navigoviz.pointcall p);
-- 33949 / 231 
select 33949/151746.0 
select 231/77882.0

select count(distinct ship_id) from navigoviz.pointcall p  -- ship_id is not null and
where ship_id is not null and source_doc_id in 
(select distinct source_doc_id  from navigoviz.pointcall p 
where p.source_suite = 'G5' and extract(year from p.date_fixed )=1787 and pointcall_function = 'O')   ;
-- 109 096
-- 82351 G5
-- 62702/62704 pointcalls avec 31843 distinct doc id G5 en 1787 - 10186 ship_id distincts renseignés, en V7
-- 62715 / 62717 pointcalls avec 31883 distinct doc id G5 en 1787 - 10184 ship_id distincts renseignés, en V8
select 62702/62704.0;

select source_suite, p.source_component , extract(year from p.date_fixed ), count(distinct source_doc_id) as nbconges, count(distinct ship_id) as nbship, count(pkid) as nbpointcalls  
from navigoviz.pointcall p 
where   pointcall_function = 'O' 
group by source_suite, p.source_component ,extract(year from p.date_fixed )
order by source_suite, p.source_component ,extract(year from p.date_fixed );

select source_suite,  count(distinct source_doc_id) as nbconges, count(distinct ship_id) as nbship, count(pkid) as nbpointcalls  
from navigoviz.pointcall p 
where  ship_id is  null  
group by source_suite 
order by source_suite;

source_suite nbconges nbship_renseignes nbpointcalls_total nbpointcall_atraiter
Expéditions coloniales Marseille (1789)	119	57	247 122
G5	43936	10559	86200	14269
la Santé registre de patentes de Marseille	12918	3159	42968	26436
Registre du petit cabotage (1786-1787)	4942	1038	9906	1907

--pavillon et tonnage renseignés par registres

select count(*) from navigo.pointcall p where p.ship_flag is not null and position('[' in ship_flag) =0;
-- 134526 / 32393

select component_description__component_source, count(*) 
from navigo.pointcall p 
where p.ship_homeport  is not  null and p.pointcall_function = 'O'
group by component_description__component_source ; 
-- 4868 --and position('[' in ship_flag) =0;

--------------------------------------------------------------------------
-- Mise en place API pour le dictionnaire cargo_categories en janvier 2023 sur V7 ou v8
--------------------------------------------------------------------------

SELECT 'cargo_categories' as API, 
        c.column_name as name, 
        case when c.table_name= 'built_travels' then 't' else 
            (case when c.table_name= 'port_points' then 'pp' else 'p' end) end||navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('cargo_categories')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid;

comment on column navigoviz.cargo_categories.pkid is 'Identifiant du produit dans Portic'   ; 
comment on column navigoviz.cargo_categories.record_id is 'Identifiant du produit dans navigo (lien avec commodity_id dans pointcall)'   ; 
comment on column navigoviz.cargo_categories.commodity_standardized_fr  is 'Nom normé du produit dans Portic, langue : français'   ;  
comment on column navigoviz.cargo_categories.commodity_standardized_en  is 'Nom normé du produit dans Portic, langue : anglais'   ;  
comment on column navigoviz.cargo_categories.category_portic_fr is 'Catégorie du produit dans Portic, langue : français'   ;  
comment on column navigoviz.cargo_categories.category_portic_en is 'Catégorie du produit dans Portic, langue : anglais'   ; 
comment on column navigoviz.cargo_categories.portic_marseille is 'Produit dans le périmètre du datasprint Marseille '  ; 
comment on column navigoviz.cargo_categories.category_toflit18_revolution_empire  is 'Catégorie du produit dans Toflit18, Révolution-Empire, langue : français'   ;  
comment on column navigoviz.cargo_categories.category_toflit18_revolution_empire_aggregate  is 'Catégorie du produit dans Toflit18, Révolution-Empire agrégée, langue : anglais'   ; 

-- adaptation de la requete travels que doit faire bernard sur la v8
-- ship_class_standardized 
/*
 select shipcaptain_travel_rank,style_dashed,uncertainty_color,
 departure_en as  departure,departure_en as  departure_uhgs_id,departure_en as  departure_latitude,departure_en as  departure_longitude,
 outdate_fixed,
 departure_en as  departure_nb_conges_1787_inputdone,departure_en as  departure_nb_conges_1787_cr,departure_en as  departure_nb_conges_1789_inputdone,departure_en as  departure_nb_conges_1789_cr,
 destination_en as  destination,destination_en as  destination_uhgs_id,
 destination_en as  destination_latitude,destination_en as  destination_longitude,
 indate_fixed,destination_en as  destination_nb_conges_1787_inputdone,destination_en as  destination_nb_conges_1787_cr,destination_en as  destination_nb_conges_1789_inputdone,destination_en as  destination_nb_conges_1789_cr,
 st_astext(geom4326, 7) as geom4326,
 distance_dep_dest_miles,tonnage,captain_name,homeport_toponyme_en as  homeport,ship_name,ship_flag_standardized_en as  ship_flag,
 nb_cargo, 
 all_cargos,
 destination_nb_cargo,
 destination_all_cargos
 --  commodity_standardized as  commodity1,commodity_purpose,quantity,quantity_u,commodity_standardized2 as  commodity2,commodity_purpose2,quantity2,quantity_u2,commodity_standardized3 as  commodity3,commodity_purpose3,quantity3,quantity_u3,commodity_standardized4 as  commodity4,commodity_purpose4,quantity4,quantity_u4 
 from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0006095N' )
 */
 
        select r.link_to_pointcall, jsonb_strip_nulls(jsonb_agg(to_jsonb(r.*) )) as all_cargos, jsonb_array_length(jsonb_agg(to_jsonb(r.*) )) as nb_cargo
        from (select * from navigoviz.cargo as c ) as r group by r.link_to_pointcall
        
        select * from navigoviz.pointcall p ;-- 139 321
        
        port_points
        
-------------------------------------------------------
-- faire la doc de l'API
        
		SELECT  
        c.column_name as name, 
        navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type, 
        pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position  )
        left outer join pg_catalog.pg_statio_all_tables st on (pgd.objoid=st.relid and  c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('built_travels')  and c.table_schema = 'navigoviz' and pgd.objoid = st.relid;
        
       
select * from      information_schema."tables" t  where t.table_schema ='navigoviz'
select * from      information_schema.columns t where t.table_schema ='navigoviz' and 

SELECT  
		c.table_name,
        c.column_name as name, 
        navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type
        FROM information_schema.columns c 
        --left  join pg_catalog.pg_statio_all_tables st on ( c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('cargo_categories','built_travels', 'pointcall', 'raw_flows', 'port_points')  and c.table_schema in ('navigoviz', 'ports')
        order by table_schema, table_name
-- ok pour avoir les attributs, mais pas les descriptions 
        
SELECT  
		c.table_name,
        c.column_name as name, 
        navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type,
        pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_statio_all_tables st on (c.table_schema=st.schemaname and c.table_name=st.relname)
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position and pgd.objoid = st.relid )
        --left  join pg_catalog.pg_statio_all_tables st on ( c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('cargo_categories','built_travels', 'pointcall', 'raw_flows', 'port_points')  
        and c.table_schema in ('navigoviz', 'ports')
order by table_schema, table_name
-- ok pour avoir les attributs et les descriptions
        
CREATE OR REPLACE VIEW ports.myremote_geonames
AS SELECT t1.geonameid,
    t1.name,
    t1.feature_code,
    t1.country_code,
    t1.admin1_code,
    t1.latitude,
    t1.longitude
   FROM dblink('dbname=geonames user=postgres password=mesange17 options=-csearch_path='::text, 'select geonameid, name, feature_code, country_code, admin1_code, latitude, longitude from geonames.geonames_nov2019.allcountries a 
                        where feature_class =''A'' and feature_code like ''P%'' or feature_code = ''TERR'' or feature_code like ''Z'' '::text) t1(geonameid integer, name text, feature_code text, country_code text, admin1_code text, latitude double precision, longitude double precision);

-- Permissions

ALTER TABLE ports.myremote_geonames OWNER TO postgres;
GRANT ALL ON TABLE ports.myremote_geonames TO postgres;
GRANT SELECT ON TABLE ports.myremote_geonames to api_user;
GRANT SELECT ON TABLE ports.myremote_geonames TO shinken;

drop table IF EXISTS navigoviz.pointcall_cargo cascade;

vacuum (full) navigoviz.pointcall ;
vacuum (full) navigoviz.built_travels  ;
vacuum (full) navigoviz.raw_flows  ;
vacuum (full) navigoviz.cargo  ;
vacuum (full) navigoviz.cargo_categories  ;
vacuum (full) navigoviz.cargo_pointcall  ;
vacuum (full) navigoviz.pointcall_taxes  ;
vacuum (full) navigoviz.uncertainity_travels  ;

-- Exemples de l'API
select travel_id,travel_rank,departure_pointcall_id,departure,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_states,departure_substates,departure_status,departure_shiparea,departure_state_1789_fr,departure_substate_1789_fr,departure_state_1789_en,departure_substate_1789_en,departure_ferme_direction,departure_ferme_direction_uncertainty,departure_ferme_bureau,departure_ferme_bureau_uncertainty,departure_partner_balance_1789,departure_partner_balance_supp_1789,departure_partner_balance_1789_uncertainty,departure_partner_balance_supp_1789_uncertainty,departure_status_uncertainity,departure_nb_conges_1787_inputdone,departure_Nb_conges_1787_CR,departure_nb_conges_1789_inputdone,departure_Nb_conges_1789_CR,departure_fr,departure_en,departure_point,departure_pkid,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination_pointcall_id,destination,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_states,destination_substates,destination_status,destination_shiparea,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_status_uncertainity,destination_nb_conges_1787_inputdone,destination_Nb_conges_1787_CR,destination_nb_conges_1789_inputdone,destination_Nb_conges_1789_CR,destination_fr,destination_en,destination_point,destination_pkid,destination_action,destination_in_date,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_class_standardized,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,birthplace_uhgs_id,status,citizenship,citizenship_uhgs_id,all_cargos,nb_cargo,destination_nb_cargo,destination_all_cargos,all_taxes,tax_concept1,taxe_amount01,tax_concept2,taxe_amount02,tax_concept3,taxe_amount03,tax_concept4,taxe_amount04,tax_concept5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,birthplace_uncertainity,birthplace_uhgs_id_uncertainity,citizenship_uncertainity,shipclass_uncertainity,style_dashed,uncertainty_color,pointpath,geom,geom4326,distance_dep_dest_km,orthodromic_distance_dep_dest_km,distance_dep_dest_miles
from navigoviz.built_travels bt where ship_id ='0008150N';

select travel_id,travel_rank,departure_pointcall_id,departure,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_states,departure_substates,departure_status,departure_shiparea,departure_state_1789_fr,departure_substate_1789_fr,departure_state_1789_en,departure_substate_1789_en,departure_ferme_direction,departure_ferme_direction_uncertainty,departure_ferme_bureau,departure_ferme_bureau_uncertainty,departure_partner_balance_1789,departure_partner_balance_supp_1789,departure_partner_balance_1789_uncertainty,departure_partner_balance_supp_1789_uncertainty,departure_status_uncertainity,departure_nb_conges_1787_inputdone,departure_Nb_conges_1787_CR,departure_nb_conges_1789_inputdone,departure_Nb_conges_1789_CR,departure_fr,departure_en,departure_point,departure_pkid,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination_pointcall_id,destination,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_states,destination_substates,destination_status,destination_shiparea,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_status_uncertainity,destination_nb_conges_1787_inputdone,destination_Nb_conges_1787_CR,destination_nb_conges_1789_inputdone,destination_Nb_conges_1789_CR,destination_fr,destination_en,destination_point,destination_pkid,destination_action,destination_in_date,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_class_standardized,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,birthplace_uhgs_id,status,citizenship,citizenship_uhgs_id,all_cargos,nb_cargo,destination_nb_cargo,destination_all_cargos,all_taxes,tax_concept1,taxe_amount01,tax_concept2,taxe_amount02,tax_concept3,taxe_amount03,tax_concept4,taxe_amount04,tax_concept5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,birthplace_uncertainity,birthplace_uhgs_id_uncertainity,citizenship_uncertainity,shipclass_uncertainity,style_dashed,uncertainty_color,pointpath,geom,geom4326,distance_dep_dest_km,orthodromic_distance_dep_dest_km,distance_dep_dest_miles
from navigoviz.raw_flows rf  where ship_id ='0008150N';

select travel_id,travel_rank,departure_pointcall_id,departure,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_states,departure_substates,departure_status,departure_shiparea,departure_state_1789_fr,departure_substate_1789_fr,departure_state_1789_en,departure_substate_1789_en,departure_ferme_direction,departure_ferme_direction_uncertainty,departure_ferme_bureau,departure_ferme_bureau_uncertainty,departure_partner_balance_1789,departure_partner_balance_supp_1789,departure_partner_balance_1789_uncertainty,departure_partner_balance_supp_1789_uncertainty,departure_status_uncertainity,departure_nb_conges_1787_inputdone,departure_Nb_conges_1787_CR,departure_nb_conges_1789_inputdone,departure_Nb_conges_1789_CR,departure_fr,departure_en,departure_point,departure_pkid,departure_rank_dedieu,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination_pointcall_id,destination,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_states,destination_substates,destination_status,destination_shiparea,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_status_uncertainity,destination_nb_conges_1787_inputdone,destination_Nb_conges_1787_CR,destination_nb_conges_1789_inputdone,destination_Nb_conges_1789_CR,destination_fr,destination_en,destination_point,destination_pkid,destination_rank_dedieu,destination_action,destination_in_date,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_class_standardized,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,birthplace_uhgs_id,status,citizenship,citizenship_uhgs_id,all_cargos,nb_cargo,destination_nb_cargo,destination_all_cargos,all_taxes,tax_concept1,taxe_amount01,tax_concept2,taxe_amount02,tax_concept3,taxe_amount03,tax_concept4,taxe_amount04,tax_concept5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,birthplace_uncertainity,birthplace_uhgs_id_uncertainity,citizenship_uncertainity,shipclass_uncertainity,style_dashed,uncertainty_color,pointpath,geom,geom4326,distance_dep_dest_km,orthodromic_distance_dep_dest_km,distance_dep_dest_miles
from navigoviz.built_travels bt where ship_id ='0008150N';

select pkid,record_id,pointcall,pointcall_uhgs_id,latitude,longitude,pointcall_admiralty,pointcall_province,pointcall_states,pointcall_substates,pointcall_states_en,pointcall_substates_en,pointcall_status,shiparea,pointcall_point,pointcall_out_date,pointcall_out_date2,pointcall_action,outdate_fixed,pointcall_in_date,pointcall_in_date2,indate_fixed,net_route_marker,fixed_net_route_marker,certitude,certitude_reason,pointcall_rankfull,pointcall_rank_dedieu,date_fixed,navigo_status,pointcall_function,data_block_leader_marker,toponyme_fr,toponyme_en,ferme_direction,ferme_direction_uncertainty,ferme_bureau,ferme_bureau_uncertainty,partner_balance_1789,partner_balance_supp_1789,partner_balance_1789_uncertainty,partner_balance_supp_1789_uncertainty,state_1789_fr,substate_1789_fr,state_1789_en,substate_1789_en,pointcall_status_uncertainity,nb_conges_1787_inputdone,Nb_conges_1787_CR,nb_conges_1789_inputdone,Nb_conges_1789_CR,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_class_standardized,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,homeport_ferme_direction,homeport_ferme_bureau,homeport_ferme_bureau_uncertainty,homeport_partner_balance_1789,homeport_partner_balance_supp_1789,homeport_partner_balance_1789_uncertainty,homeport_partner_balance_supp_1789_uncertainty,source_doc_id,source_text,source_suite,source_component,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,birthplace_uhgs_id,status,citizenship,citizenship_uhgs_id,all_cargos,nb_cargo,all_taxes,tax_concept1,taxe_amount01,tax_concept2,taxe_amount02,tax_concept3,taxe_amount03,tax_concept4,taxe_amount04,tax_concept5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,pointcall_uncertainity,captain_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,birthplace_uncertainity,birthplace_uhgs_id_uncertainity,citizenship_uncertainity,shipclass_uncertainity
from navigoviz.pointcall p where record_id = '00180090';

select ogc_fid,uhgs_id,latitude,longitude,toponyme,belonging_states,belonging_substates,geonameid,amiraute,province,shiparea,status,toustopos,topofreq,geom,point3857,country2019_name,country2019_iso2code,country2019_region,oblique,relation_state,has_a_clerk,source_1787_available,source_1789_available,belonging_states_en,belonging_substates_en,toponyme_standard_fr,toponyme_standard_en,ferme_direction,ferme_direction_uncertainty,ferme_bureau,ferme_bureau_uncertainty,partner_balance_1789,partner_balance_supp_1789,partner_balance_1789_uncertainty,partner_balance_supp_1789_uncertainty,state_1789_fr,substate_1789_fr,state_1789_en,substate_1789_en,conges_1787_inputdone,conges_1789_inputdone,incertitude_status,nb_conges_1787_inputdone,Nb_conges_1787_CR,Nb_sante_1787,Nb_petitcabotage_1787,nb_conges_1789_inputdone,Nb_conges_1789_CR,Nb_sante_1789,Nb_petitcabotage_1789,nb_longcours_marseille_1789,id_shoreline,distance_shoreline,port_on_shoreline,type_cote,port_on_line_1mile,port_on_line_20mile,id_valid_20mile,insertdate
from ports.port_points pp where pp.uhgs_id = 'A0136930';

departure_rank_dedieu : plus
destination_rank_dedieu : plus
homeport_status : ? erreur fichier excel
homeport_shiparea : ? erreur fichier excel
distance_homeport_dep_miles : ok. moins

SELECT  
		c.table_name,
        c.column_name as name, 
        navigo.pystrip(to_char(c.ordinal_position::int, '009')) as shortname,
        c.data_type as type,
        pgd.description as description
        FROM information_schema.columns c 
        left outer join pg_catalog.pg_statio_all_tables st on (c.table_schema=st.schemaname and c.table_name=st.relname)
        left outer join pg_catalog.pg_description pgd on (pgd.objsubid=c.ordinal_position and pgd.objoid = st.relid )
        --left  join pg_catalog.pg_statio_all_tables st on ( c.table_schema=st.schemaname and c.table_name=st.relname)
        where c.table_name in ('built_travels')  
        and c.table_schema in ('navigoviz', 'ports')
order by table_schema, table_name, column_name
-- ok pour avoir les attributs et les descriptions

-- Ok pour API

-------------------------------------------------------------------------------
-- Rajouter les tables captain et ship pour avoir l'API qui marche
-------------------------------------------------------------------------------
CREATE TABLE navigoviz.captain (
		captain_id text,
		occurences_names json,
		occurences_birthplaces json,
		occurences_citizenships json
	);

INSERT INTO navigoviz.captain(captain_id)
	SELECT DISTINCT captain_id FROM navigoviz.built_travels where captain_id is not null;
--- 12329 
-- 12210 le 21 juillet 2022
-- 12995 le 16 janvier 2023

update navigoviz.captain c set occurences_names = k.occurences_name
FROM
(select captain_local_id, array_to_json(array_agg(json_build_object(captain_name ,occurences))) as occurences_name
	FROM
	(
	Select captain_id as captain_local_id, trim(captain_name) as captain_name, count (distinct source_doc_id) as occurences
	FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels)
	GROUP BY trim(captain_name) , captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;


update navigoviz.captain c set occurences_birthplaces = k.occurence_birthplace
FROM
(select captain_local_id, array_to_json(array_agg(json_build_object(birthplace ,occurences))) as occurence_birthplace
FROM
(
Select captain_id as captain_local_id, birthplace, count (distinct source_doc_id) as occurences
FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels)
	GROUP BY birthplace,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0 and birthplace is not null
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 2769


update navigoviz.captain c set occurences_citizenships = k.occurences_citizenship
FROM
(select captain_local_id, array_to_json(array_agg(json_build_object(citizenship ,occurences))) as occurences_citizenship
FROM
(
Select captain_id as captain_local_id, citizenship, count (distinct source_doc_id) as occurences
FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels)
	GROUP BY citizenship,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0 and citizenship is not null
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;


CREATE TABLE navigoviz.ship(
	ship_id text,
	occurences_names json,
	occurences_homeports_fr json,--use standardized homeports fr et en; should be only ONE ?
	occurences_homeports_en json,--use standardized homeports fr et en; should be only ONE ?
	occurences_flags_fr json,-- use standardized flag fr et en; should be only ONE ?
	occurences_flags_en json,-- use standardized flag fr et en; should be only ONE ?
	occurences_class json,
	occurences_tonnageclass json
);

INSERT INTO navigoviz.ship(ship_id,occurences_names)
select ship_id, array_to_json(array_agg(json_build_object(ship_name ,occurences))) as occurences_name
FROM
(
Select ship_id, trim(ship_name) as ship_name, count (distinct source_doc_id) as occurences
FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
	GROUP BY trim(ship_name),ship_id
	ORDER BY ship_id, occurences DESC
) as k
GROUP BY ship_id;
-- 11618
-- 11527 le 21 juillet 2022
-- 12418 le 16 janvier 2023


update navigoviz.ship s set occurences_flags_fr = k.flags
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_flag ,t.occurences))) as flags
	FROM(
		Select ship_id,
		ship_flag_standardized_fr as ship_flag, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
		WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, p.ship_flag_standardized_fr
		ORDER BY ship_id, occurences DESC
		) t
	WHERE occurences > 0 and ship_flag is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11876

update navigoviz.ship s set occurences_flags_en = k.flags
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_flag ,t.occurences))) as flags
	FROM(
		Select ship_id,
		ship_flag_standardized_en as ship_flag, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, p.ship_flag_standardized_en
		ORDER BY ship_id, occurences DESC
		) t
	WHERE occurences > 0 and ship_flag is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11876

update navigoviz.ship s set occurences_homeports_fr = k.homeports
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_homeport ,t.occurences))) as homeports
	FROM(
		Select ship_id,
		homeport_toponyme_fr as ship_homeport, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, homeport_toponyme_fr
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0 and ship_homeport is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
---  6611

update navigoviz.ship s set occurences_homeports_en = k.homeports
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_homeport ,t.occurences))) as homeports
	FROM(
		Select ship_id,
		homeport_toponyme_en as ship_homeport, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, homeport_toponyme_en
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0 and ship_homeport is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
--- 6611


update navigoviz.ship s set occurences_class = k.class
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_class ,t.occurences))) as class
	FROM(
		Select ship_id,
		trim(lower(p.ship_class_standardized)) as ship_class, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, trim(lower(ship_class_standardized))
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0 and ship_class is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- ship_class_standardized et plus class seulement
-- 4753

update navigoviz.ship s set occurences_tonnageclass = k.tonnageclass
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_tonnage ,t.occurences))) as tonnageclass
	FROM(
		Select ship_id,
		tonnage_class as ship_tonnage, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, tonnage_class
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0 and ship_tonnage is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;	
-- 9002


-- Renvoyer les variantes de noms de navires ou de capitaines
-- role api_user / password portic
GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigocheck to api_user;
GRANT USAGE ON SCHEMA navigo to api_user;
GRANT USAGE ON SCHEMA public to api_user;
GRANT USAGE ON SCHEMA ports to api_user;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO api_user;
grant CONNECT on database portic_v7 to api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to api_user;

comment on table navigoviz.ship is 'List of ships''descriptions with variantes for the name, flag, homeport, tonnage_class and class of ship';
comment on table navigoviz.captain is 'List of captains''descriptions with variantes for the name, citizenship, birthplace of captain';

comment on column navigoviz.ship.ship_id is 'unique identifier of ship set by Silvia who identifies the same ships through out various source documents';
comment on column navigoviz.ship.occurences_names is 'JSON array of various names of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_homeports_fr is 'JSON array of various homeports (standardized name in FR lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_homeports_en is 'JSON array of various homeports (standardized name in EN lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_flags_fr is 'JSON array of various flag (standardized name in FR lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_flags_en is 'JSON array of various flag (standardized name in EN lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_class is 'JSON array of various class types of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_tonnageclass is 'JSON array of various categories of tonnage of the same ship through sources, with the frequecy of appearance in documents';

comment on column navigoviz.captain.captain_id is 'unique identifier of captain set by Silvia who identifies the same captain through out various source documents';
comment on column navigoviz.captain.occurences_names is 'JSON array of various names of the same captain through sources, with the frequecy of appearance in documents';
comment on column navigoviz.captain.occurences_birthplaces is 'JSON array of various birthplace of the same captain through sources, with the frequecy of appearance in documents';
comment on column navigoviz.captain.occurences_citizenships is 'JSON array of various citizenship of the same captain through sources, with the frequecy of appearance in documents';

-- suite le 16 juin : ajout de ship_id, captain_id et min et max date pour Bernard
alter table navigoviz.ship add column captain_list json;
alter table navigoviz.ship add column mindate text;
alter table navigoviz.ship add column maxdate text;
alter table navigoviz.ship add column nb_sourcedoc int;
alter table navigoviz.captain add column ship_list json;
alter table navigoviz.captain add column mindate text;
alter table navigoviz.captain add column maxdate text;
alter table navigoviz.captain add column nb_sourcedoc int;



comment on column navigoviz.ship.captain_list is 'JSON array of various captains having sailed on the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.mindate is 'First date of appearance of this ship in the sources';
comment on column navigoviz.ship.maxdate is 'Last date of appearance of this ship in the sources';
comment on column navigoviz.ship.nb_sourcedoc is 'Number of documents mentionning this ship in the sources';
comment on column navigoviz.captain.ship_list is 'JSON array of various ships on which this captain has been sailing through sources, with the frequecy of appearance in documents';
comment on column navigoviz.captain.mindate is 'First date of appearance of this captain in the sources';
comment on column navigoviz.captain.maxdate is 'Last date of appearance of this captain in the sources';
comment on column navigoviz.captain.nb_sourcedoc is 'Number of documents mentionning this captain in the sources';


update navigoviz.ship s set captain_list = k.captain_list
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.captain_id ,t.occurences))) as captain_list
	FROM(
		Select ship_id, captain_id , count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from navigoviz.built_travels)
		GROUP BY ship_id, captain_id
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0 and captain_id is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;	
-- 12414

update navigoviz.ship s set mindate = to_char(k.mindate, 'yyyy-mm-dd'), maxdate=to_char(k.maxdate, 'yyyy-mm-dd'), nb_sourcedoc=k.nb
from (
		Select ship_id, min(p.date_fixed) as mindate, max(p.date_fixed) as maxdate, count(distinct source_doc_id) as nb
		FROM navigoviz.pointcall p
		WHERE p.ship_id in (select distinct ship_id from navigoviz.built_travels)
		GROUP BY ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 12418

update navigoviz.captain s set ship_list = k.ship_list
FROM
(SELECT t.captain_id, array_to_json(array_agg(json_build_object(t.ship_id ,t.occurences))) as ship_list
	FROM(
		Select captain_id, ship_id , count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from navigoviz.built_travels)
		GROUP BY captain_id, ship_id
		ORDER BY captain_id, occurences desc
		) t
	 WHERE occurences > 0 and ship_id is not null
	 GROUP BY t.captain_id
) as k
WHERE s.captain_id = k.captain_id;
-- 12995

update navigoviz.captain s set mindate = to_char(k.mindate, 'yyyy-mm-dd'), maxdate=to_char(k.maxdate, 'yyyy-mm-dd'), nb_sourcedoc=k.nb
from (
		Select captain_id, min(p.date_fixed) as mindate, max(p.date_fixed) as maxdate, count(distinct source_doc_id) as nb
		FROM navigoviz.pointcall p
		WHERE p.captain_id in (select distinct captain_id from navigoviz.built_travels)
		GROUP BY captain_id
) as k
WHERE s.captain_id = k.captain_id;
-- 12995

-- Et christine : V8, refaire ships et captains, et dedans remplace ship_class par ship_class_standardized
-- fait le 16 janvier 2023 matin
 
-------------------------------------------------------------------------------
-- Travail pour Bernard (shiproutes) sur les dates inviz_date, outviz_date
-------------------------------------------------------------------------------

/* si departure_out_date est exacte et duration = 0
 * (case when position('=' in p.pointcall_out_date) > 0 then true else false end) as date_precise

1) prendre le trajet suivant (pas rouge ni dashed)
2) prendre la date de départ de ce trajet comme invizdate_fixed
-- NON calculer la duration et prendre la moitié

si destination_out_date est exacte et duration = 0
1) prendre le trajet précédent (pas rouge ni dashed)
2) prendre la date d'arrivée de ce trajet comme outvizdate_fixed
*/

alter table built_travels add column outvizdate_fixed date;
comment on column navigoviz.built_travels.outvizdate_fixed is 'out date of a ship, but modified for arranging the look of the chronogram';

alter table built_travels add column invizdate_fixed date;
comment on column navigoviz.built_travels.invizdate_fixed is 'in date of a ship, but modified for arranging the look of the chronogram';

alter table raw_flows  add column outvizdate_fixed date;
comment on column navigoviz.raw_flows.outvizdate_fixed is 'out date of a ship, but modified for arranging the look of the chronogram';

alter table raw_flows add column invizdate_fixed date;
comment on column navigoviz.raw_flows.invizdate_fixed is 'in date of a ship, but modified for arranging the look of the chronogram';

alter table uncertainity_travels  add column outvizdate_fixed date;
comment on column navigoviz.uncertainity_travels.outvizdate_fixed is 'out date of a ship, but modified for arranging the look of the chronogram';

alter table uncertainity_travels add column invizdate_fixed date;
comment on column navigoviz.uncertainity_travels.invizdate_fixed is 'in date of a ship, but modified for arranging the look of the chronogram';

-- En plus de oudate_fixed et indate_fixed, je met outvizdate_fixed toujours renseignée et invizdate_fixed 


select bt.travel_id , bt.travel_rank , bt.ship_name , bt.ship_id , bt.captain_name , bt.style_dashed , departure_out_date , outdate_fixed ,  departure_navstatus , destination_in_date , indate_fixed , destination_navstatus 
from built_travels bt 
where bt.outdate_fixed = bt.indate_fixed 
order by bt.travel_id , bt.travel_rank;
-- 24926


select all_cargos  from navigoviz.built_travels bt 
-- [{"quantity": "170.0", "quantity_u": "charges", "commodity_id": "00000298", "cargo_item_action": "In", "commodity_purpose": "Fèves", "link_to_pointcall": "00186125", "commodity_standardized_en": "Broad bean", "commodity_standardized_fr": "Fèves", "commodity_permanent_coding": "ID-EBBAxx-xx-xxxxxx-FBxx"}, {"quantity": "1500.0", "quantity_u": "charges", "commodity_id": "00000027", "cargo_item_action": "Transit", "commodity_purpose": "Bled", "link_to_pointcall": "00186125", "commodity_standardized_en": "Wheat", "commodity_standardized_fr": "Blé", "commodity_permanent_coding": "ID-BDxxxx-xx-xxxxxx-FBxx"}, {"quantity": null, "quantity_u": null, "commodity_id": "00000014", "cargo_item_action": "In", "commodity_purpose": "Cuirs", "link_to_pointcall": "00186125", "commodity_standardized_en": null, "commodity_standardized_fr": null, "commodity_permanent_coding": "ID-OLAxxx-xx-xxxxxx-xxxx"}, {"quantity": "1.0", "quantity_u": "unité", "commodity_id": "00000002", "cargo_item_action": "In", "commodity_purpose": "Passager", "link_to_pointcall": "00186125", "commodity_standardized_en": "Passenger", "commodity_standardized_fr": "Passagers", "commodity_permanent_coding": "IY-AAxxxx-xx-xxxxxx-xxxx"}]
-- PC-RF	FC-RF

1787=06=16	1787-06-16	PC-RF	1787>06>16!	1787-06-16	FC-RF
			1787-10-25	FC-RF	1787>10>25!	1787-10-25	FC-RF
			1787-03-06	FC-RF				1787-03-06	PC-RF

select departure, destination, bt.ship_name , bt.ship_id , bt.captain_name , bt.style_dashed , 
bt.departure_out_date , bt.outdate_fixed ,  bt.departure_navstatus , bt.destination_in_date , bt.indate_fixed , bt.destination_navstatus , 
p.pointcall , p.outdate_fixed , p.pointcall_out_date , p.fixed_net_route_marker  
from built_travels bt 
left join pointcall p on p.ship_id = bt.ship_id and p.pointcall_rankfull + 1 = bt.ra
-- and substring(p.pointcall_rank_dedieu , 0, strpos( p.pointcall_rank_dedieu, '.'))::int + 1 = bt.destination_rank_dedieu 
and p.fixed_net_route_marker='A'
where bt.outdate_fixed = bt.indate_fixed and departure_navstatus like 'PC%'  and  destination_navstatus like 'FC%'
order by bt.travel_id , bt.travel_rank;
-- 23523
-- substring(p.pointcall_rank_dedieu , 0, strpos( p.pointcall_rank_dedieu, '.'))::int

select bt.departure, bt.destination, bt.ship_name , bt.ship_id , bt.captain_name , bt.style_dashed , 
bt.departure_out_date , bt.outdate_fixed ,  bt.departure_navstatus , bt.destination_in_date , bt.indate_fixed , bt.destination_navstatus , 
u.departure  , u.departure_out_date  
from navigoviz.built_travels bt 
left join navigoviz.uncertainity_travels u on u.ship_id = bt.ship_id and u.travel_rank  = bt.travel_rank + 2
where bt.outdate_fixed = bt.indate_fixed and bt.departure_navstatus like 'PC%'  and  bt.destination_navstatus like 'FC%'
order by bt.travel_id , bt.travel_rank;

select * from navigoviz.uncertainity_travels
where duration = 0 and direct is false and couleur = 'red';

select * from uncertainity_travels bt where couleur = 'green' and bt.travel_uncertainity != 0;
select * from uncertainity_travels bt where couleur = 'grey' and bt.travel_uncertainity != -1;
select * from uncertainity_travels bt where couleur = 'orange' and bt.travel_uncertainity != -2;
select * from uncertainity_travels bt where couleur = 'red' and bt.travel_uncertainity != -3;

select departure_out_date, outdate_fixed , duration, 
destination_in_date, indate_fixed, 
direct , travel_uncertainity , travel_rank , ship_id
from uncertainity_travels bt
order by ship_id, travel_rank
-- where duration=0 

------------- SOLUTION  vizdates  sur uncertainity_travels
select t.departure_out_date, t.outdate_fixed , t.duration, nextt.duration, t.indate_fixed ,
nextt.indate_fixed as original_nextdate, nextt.indate_fixed - nextt.duration/2 as vizindate_fixed, nextt.destination_in_date, 
t.direct , t.travel_uncertainity , t.travel_rank , t.ship_id
from uncertainity_travels t left join  uncertainity_travels nextt on nextt.ship_id = t.ship_id and nextt.travel_rank = t.travel_rank +1 
where position('=' in t.destination_in_date)=0  and position('=' in t.departure_out_date)>0
order by ship_id, travel_rank;

update uncertainity_travels t set  invizdate_fixed = null; --70963

update uncertainity_travels t set  invizdate_fixed = nextt.indate_fixed - nextt.duration/2
from uncertainity_travels nextt 
where nextt.ship_id = t.ship_id and nextt.travel_rank = t.travel_rank +1 
and t.duration = 0
and position('=' in t.destination_in_date)=0  and position('=' in t.departure_out_date)>0; --15139

update uncertainity_travels t set  outvizdate_fixed = null; --70963

-- update uncertainity_travels t set  outvizdate_fixed = t.indate_fixed - t.duration/2
-- where t.destination_in_date is null and  departure_out_date is null ;

update uncertainity_travels t set  outvizdate_fixed = t.indate_fixed - t.duration/2
from uncertainity_travels previous 
where previous.ship_id = t.ship_id and previous.travel_rank + 1 = t.travel_rank  
and previous.duration = 0 ; -- 16090

select t.departure_out_date, t.destination_in_date , t.outdate_fixed , t.duration,  t.indate_fixed , t.invizdate_fixed, t.indate_fixed - t.duration/2 as vizoutdate_fixed, 
t.direct , t.travel_uncertainity , t.travel_rank , t.ship_id
from uncertainity_travels t 
where t.destination_in_date is null and  departure_out_date is null 
order by ship_id, travel_rank;

---------- SOLUTION  vizdates  sur built_travels
alter table built_travels add column outvizdate_fixed date;
comment on column navigoviz.built_travels.outvizdate_fixed is 'out date of a ship, but modified for arranging the look of the chronogram';

alter table built_travels add column invizdate_fixed date;
comment on column navigoviz.built_travels.invizdate_fixed is 'in date of a ship, but modified for arranging the look of the chronogram';

alter table raw_flows  add column outvizdate_fixed date;
comment on column navigoviz.raw_flows.outvizdate_fixed is 'out date of a ship, but modified for arranging the look of the chronogram';

alter table raw_flows add column invizdate_fixed date;
comment on column navigoviz.raw_flows.invizdate_fixed is 'in date of a ship, but modified for arranging the look of the chronogram';


alter table built_travels add column duration int;
comment on column navigoviz.built_travels.duration is 'duration, as computed following original data (not viz dates)';

update built_travels b set duration = t.duration 
from uncertainity_travels t where t.ship_id = b.ship_id and  t.travel_rank = b.travel_rank ;

update built_travels  t set  invizdate_fixed = indate_fixed; --70963

update built_travels t set  invizdate_fixed = nextt.indate_fixed - nextt.duration/2
from built_travels nextt 
where nextt.ship_id = t.ship_id and nextt.travel_rank = t.travel_rank +1 
and t.duration = 0
and position('=' in t.destination_in_date)=0  and position('=' in t.departure_out_date)>0; --15139

update built_travels t set  outvizdate_fixed = outdate_fixed; --70963

-- update built_travels set  outvizdate_fixed = indate_fixed - duration/2
-- where departure_out_date is null  and destination_in_date is null -- and duration = 0;--158 / 20956

update built_travels t set  outvizdate_fixed = t.indate_fixed - t.duration/2
from built_travels previous 
where previous.ship_id = t.ship_id and previous.travel_rank + 1 = t.travel_rank  
and previous.duration = 0 ; -- 16090
-- and position('=' in previous.destination_in_date)>0  and position('=' in previous.departure_out_date)>0; --15139

select t.departure_out_date, t.outdate_fixed , t.outvizdate_fixed,  t.duration, 
destination_in_date, indate_fixed, invizdate_fixed, 
t.style_dashed  , t.travel_uncertainity , t.travel_rank , t.ship_id
from built_travels t 
order by ship_id, travel_rank;
-- a l'air ok

-- correction d'un bug pour Bernard

alter table navigoviz.built_travels add column shipcaptain_travel_rank int;
comment on column navigoviz.built_travels.shipcaptain_travel_rank is 'rank of this segment of travel for a combination of one captain_id and one ship_id';

update navigoviz.built_travels b  set shipcaptain_travel_rank = k.shipcaptain_travel_rank
from (
select departure_pkid, destination_pkid ,
captain_id, ship_id , outdate_fixed, travel_rank, travel_uncertainity, departure , destination , travel_rank,
row_number() over (PARTITION BY ship_id, captain_id order by outdate_fixed asc, travel_rank asc, destination_uncertainity asc) as shipcaptain_travel_rank
from navigoviz.built_travels ut 
where source_entry != 'both-to' 
) as k
where b.departure_pkid = k.departure_pkid and b.destination_pkid = k.destination_pkid;
-- 70963

-- pour l'API
alter table navigoviz.raw_flows add column duration int
update navigoviz.raw_flows t set duration = indate_fixed - outdate_fixed;--74255

-- Vérif de raw_flows , en particulier sur Marseille
select departure, destination, source_doc_id, duration, travel_rank, shipcaptain_travel_rank  
from navigoviz.raw_flows 
where source_suite != 'G5'
order by source_doc_id , travel_rank ;
-- 00182402, 00182413, 00182516 : cas intéressants

-- Vérif de travels , en particulier sur Marseille
select departure, destination, source_doc_id, ship_id, duration, shipcaptain_travel_rank , 
departure_out_date, destination_in_date , outvizdate_fixed , invizdate_fixed ,
uncertainty_color 
from navigoviz.built_travels  
where source_doc_id in ('00182402', '00182413', '00182516')
order by source_doc_id , travel_rank ;

select departure, destination, source_doc_id, ship_id, duration, shipcaptain_travel_rank , 
departure_out_date, destination_in_date , outvizdate_fixed , invizdate_fixed ,
uncertainty_color 
from navigoviz.built_travels  
where ship_id in ('0015812N', '0015799N')
order by ship_id , travel_rank ;

-- vérif d'un cas bizarre
-- 0	14	0000479N direct, mais aucune des deux dates connues, et certifié
select t.source_doc_id , t.source_suite , t.departure , t.destination , t.departure_out_date, t.outdate_fixed , t.outvizdate_fixed,  t.duration, 
destination_in_date, indate_fixed, invizdate_fixed, 
t.style_dashed  , t.travel_uncertainity , t.travel_rank , t.ship_id
from built_travels t 
where ship_id = '0000479N'
order by ship_id, travel_rank;

select net_route_marker, fixed_net_route_marker  , source_doc_id, pointcall_rankfull, pointcall_rank_dedieu, p.pointcall , p.pointcall_function , p.navigo_status, p.pointcall_out_date , p.pointcall_in_date  
from navigoviz.pointcall p 
where source_doc_id  in ('00151468','00153324','00149583','00141344', '00148687', '00150811', '00138336', '00152825', '00151717')
order by p.pointcall_rankfull  ;

select * from navigo.pointcall p where p.data_block_local_id = '00148687'; -- Lestage et délestage Chaillevette (1780-1787)
-- Fait un in à cette date à Chaillevette

-- API : travels

select travel_id,travel_rank,departure_pointcall_id,departure,departure_uhgs_id,departure_latitude,departure_longitude,departure_admiralty,departure_province,departure_states,departure_substates,departure_status,departure_shiparea,departure_state_1789_fr,departure_substate_1789_fr,departure_state_1789_en,departure_substate_1789_en,departure_ferme_direction,departure_ferme_direction_uncertainty,departure_ferme_bureau,departure_ferme_bureau_uncertainty,departure_partner_balance_1789,departure_partner_balance_supp_1789,departure_partner_balance_1789_uncertainty,departure_partner_balance_supp_1789_uncertainty,departure_status_uncertainity,departure_nb_conges_1787_inputdone,departure_Nb_conges_1787_CR,departure_nb_conges_1789_inputdone,departure_Nb_conges_1789_CR,departure_fr,departure_en,departure_point,departure_pkid,departure_rank_dedieu,departure_out_date,departure_action,outdate_fixed,departure_navstatus,departure_function,destination_pointcall_id,destination,destination_uhgs_id,destination_latitude,destination_longitude,destination_admiralty,destination_province,destination_states,destination_substates,destination_status,destination_shiparea,destination_state_1789_fr,destination_substate_1789_fr,destination_state_1789_en,destination_substate_1789_en,destination_ferme_direction,destination_ferme_direction_uncertainty,destination_ferme_bureau,destination_ferme_bureau_uncertainty,destination_partner_balance_1789,destination_partner_balance_supp_1789,destination_partner_balance_1789_uncertainty,destination_partner_balance_supp_1789_uncertainty,destination_status_uncertainity,destination_nb_conges_1787_inputdone,destination_Nb_conges_1787_CR,destination_nb_conges_1789_inputdone,destination_Nb_conges_1789_CR,destination_fr,destination_en,destination_point,destination_pkid,destination_rank_dedieu,destination_action,destination_in_date,indate_fixed,destination_navstatus,destination_function,ship_name,ship_id,tonnage,tonnage_unit,flag,class,ship_class_standardized,ship_flag_id,ship_flag_standardized_fr,ship_flag_standardized_en,in_crew,tonnage_class,homeport,homeport_uhgs_id,homeport_latitude,homeport_longitude,homeport_admiralty,homeport_province,homeport_point,homeport_state_1789_fr,homeport_substate_1789_fr,homeport_state_1789_en,homeport_substate_1789_en,homeport_toponyme_fr,homeport_toponyme_en,source_entry,source_doc_id,source_text,source_suite,source_component,source_main_port_uhgs_id,source_main_port_toponyme,source_subset,captain_id,captain_name,birthplace,birthplace_uhgs_id,status,citizenship,citizenship_uhgs_id,all_cargos,nb_cargo,destination_nb_cargo,destination_all_cargos,all_taxes,tax_concept1,taxe_amount01,tax_concept2,taxe_amount02,tax_concept3,taxe_amount03,tax_concept4,taxe_amount04,tax_concept5,taxe_amount05,ship_uncertainity,tonnage_uncertainity,flag_uncertainity,homeport_uncertainity,departure_uncertainity,destination_uncertainity,captain_uncertainity,travel_uncertainity,cargo_uncertainity,taxe_uncertainity,pointcall_outdate_uncertainity,birthplace_uncertainity,birthplace_uhgs_id_uncertainity,citizenship_uncertainity,shipclass_uncertainity,style_dashed,uncertainty_color,pointpath,geom,geom4326,distance_dep_dest_km,orthodromic_distance_dep_dest_km,distance_dep_dest_miles,outvizdate_fixed,invizdate_fixed,shipcaptain_travel_rank,duration
from navigoviz.built_travels bt 
where bt.ship_id = '0007498N' -- Aimable Ursule 
order by travel_rank ;
-----------------------------------------------------------------------------------------------------------
-- Travail sur les trajectoires

-- Aimable Ursule - ship 0007498N (2 capitaines qui se suivent)
-- Fidèle Marianne - ship 0002931N 
-- Marie Louise - ship 0004155N (André Guinès) : aller-retour à Terre-Neuve
-- Turgot, ship id 0021517N (Jacques Mahé) avec une controverse sur le retour de la guadeloupe
-- Femmes et Enfants, 0003951N (Janz, Rinck) : Bordeaux à Riga à recalculer
-- Notre Dame des Carmes 0004171N (Page Nicolas)
-- Accord 0015562N Gautier, Gaspard 
-- Accord 0015563N Nicolas, Jean François : BUG du à geom4326 null -- Gerbis	A0000144	Sfax	A0019937
-- Aimable Marianne (8878N) et Bernard, Nicolas (9224, 9225) : 2 captaines qui sont surement les mêmes
-- Saint Antoine 10860N et Valery, Toussaint (11258) : Divers ports en méditérannée, port inconnu donc pas de dessin de trajectoire. 
-- Notre dame de consolation - ship 0000479N : tri sur vizdates et puis sur shipcaptain_travel_rank 
-- Marie - ship  0014700N (captain Arnaud Etienne) : un bordel avec Gibraltar
-- 0012925N	Suzanne
-- Jeune Bromme Kamp	0008150N 0008150N

-- Bernard, ses cas de tests
-- capitaine : "Taylor" pour les combinaisons capitaines/navire avec le même id de capitaine
-- navire "Accord" pour des id différents
-- navire "Abdon" 66N avec 2 captaines qui doivent être les mêmes
-- navire "Achille" (les deux derniers)
-- navire "Aimable Marianne", les deux premiers ont le même id et s'enchainent bien sur le chronogramme



select distinct source_entry from navigoviz.built_travels ut ;
-- both-from
-- from

select style_dashed,uncertainty_color,departure_en as  departure,departure_uhgs_id,departure_latitude,departure_longitude,outdate_fixed,departure_nb_conges_1787_inputdone,departure_nb_conges_1787_cr,departure_nb_conges_1789_inputdone,departure_nb_conges_1789_cr,destination_en as  destination,destination_uhgs_id,destination_latitude,destination_longitude,indate_fixed,destination_nb_conges_1787_inputdone,destination_nb_conges_1787_cr,destination_nb_conges_1789_inputdone,destination_nb_conges_1789_cr,st_astext(geom4326, 7) as geom4326,distance_dep_dest_miles,tonnage,captain_name,homeport_toponyme_en as  homeport,ship_name,ship_flag_standardized_en as  ship_flag,(all_cargos::json->>0)::json->>'commodity_standardized_en' as  commodity1,(all_cargos::json->>1)::json->>'commodity_standardized_en' as  commodity2,(all_cargos::json->>2)::json->>'commodity_standardized_en' as  commodity3,(all_cargos::json->>3)::json->>'commodity_standardized_en' as  commodity4 from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0015563N' );

select style_dashed,uncertainty_color,departure_en as  departure,departure_uhgs_id,departure_latitude,departure_longitude,outdate_fixed,departure_nb_conges_1787_inputdone,departure_nb_conges_1787_cr,departure_nb_conges_1789_inputdone,departure_nb_conges_1789_cr,destination_en as  destination,destination_uhgs_id,destination_latitude,destination_longitude,indate_fixed,destination_nb_conges_1787_inputdone,destination_nb_conges_1787_cr,destination_nb_conges_1789_inputdone,destination_nb_conges_1789_cr,st_astext(geom4326, 7) as geom4326,distance_dep_dest_miles,tonnage,captain_name,homeport_toponyme_en as  homeport,ship_name,ship_flag_standardized_en as  ship_flag,(all_cargos::json->>0)::json->>'commodity_standardized_en' as  commodity1,(all_cargos::json->>1)::json->>'commodity_standardized_en' as  commodity2,(all_cargos::json->>2)::json->>'commodity_standardized_en' as  commodity3,(all_cargos::json->>3)::json->>'commodity_standardized_en' as  commodity4 from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0015563N' ) and (captain_id = '00015561' );
select * from captain c where c.captain_id = '00015561'
select * from ship s  where s.ship_id  = '0015563N' -- captain_list : [{"00015561" : 2}]
select * from ship s  where s.ship_id  = '0015562N' -- captain_list : [{"00015560" : 2}]
select * from captain c where c.captain_id = '00015560'

select style_dashed,uncertainty_color,departure_en as  departure,departure_uhgs_id,departure_function ,
--departure_latitude,departure_longitude, 
departure_out_date , outdate_fixed, departure_navstatus , destination_in_date, indate_fixed, destination_navstatus ,
destination_en as  destination,destination_uhgs_id,
--destination_latitude,destination_longitude,
st_astext(geom4326, 7) as geom4326,
captain_name,ship_name,
(all_cargos::json->>0)::json->>'commodity_standardized_en' as  commodity1,(all_cargos::json->>1)::json->>'commodity_standardized_en' as  commodity2,(all_cargos::json->>2)::json->>'commodity_standardized_en' as  commodity3,(all_cargos::json->>3)::json->>'commodity_standardized_en' as  commodity4 
from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0010860N' )
order by travel_rank ;

select style_dashed,uncertainty_color,departure_en as  departure,departure_uhgs_id,departure_latitude,departure_longitude,outdate_fixed,departure_nb_conges_1787_inputdone,departure_nb_conges_1787_cr,departure_nb_conges_1789_inputdone,departure_nb_conges_1789_cr,destination_en as  destination,destination_uhgs_id,destination_latitude,destination_longitude,indate_fixed,destination_nb_conges_1787_inputdone,destination_nb_conges_1787_cr,destination_nb_conges_1789_inputdone,destination_nb_conges_1789_cr,
st_astext(geom4326, 7) as geom4326,distance_dep_dest_miles,tonnage,captain_name,homeport_toponyme_en as  homeport,ship_name,ship_flag_standardized_en as  ship_flag,(all_cargos::json->>0)::json->>'commodity_standardized_en' as  commodity1,(all_cargos::json->>1)::json->>'commodity_standardized_en' as  commodity2,(all_cargos::json->>2)::json->>'commodity_standardized_en' as  commodity3,(all_cargos::json->>3)::json->>'commodity_standardized_en' as  commodity4
from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0015563N' ) and (captain_id = '00015561' )
order by travel_rank ;

select st_astext(geom4326, 7) as geom4326, * from built_travels where geom4326 is null;


    
select style_dashed, uncertainty_color, departure_en as  departure,departure_uhgs_id,departure_latitude,departure_longitude,outdate_fixed,departure_nb_conges_1787_inputdone,departure_nb_conges_1787_cr,departure_nb_conges_1789_inputdone,departure_nb_conges_1789_cr,destination_en as  destination,destination_uhgs_id,destination_latitude,destination_longitude,indate_fixed,destination_nb_conges_1787_inputdone,destination_nb_conges_1787_cr,destination_nb_conges_1789_inputdone,destination_nb_conges_1789_cr,st_astext(geom4326, 7) as geom4326,distance_dep_dest_miles,tonnage,captain_name,
homeport_toponyme_en as  homeport,ship_name,ship_flag_standardized_en as  ship_flag
--commodity_standardized as  commodity1,commodity_standardized2 as  commodity2,commodity_standardized3 as  commodity3,commodity_standardized4 as  commodity4 
from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0002931N' ) and (captain_id = '00002918' )


--- 
-- pour l'article

select * from ship_class_standardized scs 
where scs.shipclass_source  ilike '%barge%';

select * from ship_class_standardized scs 
where scs.shipclass_source  ilike '%barque%';
select * from ship_class_standardized scs 
where scs.shipclass_standard  ilike '%barque%';

select count(distinct shipclass_source) from ship_class_standardized 

select count(distinct shipclass_standard) from ship_class_standardized 

select * from ship_class_standardized where shipclass_source ilike '%pinque%';
-- Bateau [Pinque]

select * from ship_class_standardized where shipclass_standard ilike '%bateau%';

---------------
select source_suite , source_doc_id , style_dashed,uncertainty_color,departure_en as  departure,departure_uhgs_id,departure_function ,
--departure_latitude,departure_longitude, 
departure_out_date , outdate_fixed, departure_navstatus , destination_in_date, indate_fixed, destination_navstatus ,
destination_en as  destination, destination_uhgs_id,
--destination_latitude,destination_longitude,
captain_name,ship_name
from navigoviz.built_travels 
where source_entry <> 'both-to' and (ship_id = '0014815N' )
order by travel_rank ;
--  0010860N; 0014700N; 0014815N

select ship_name, ship_id, captain_name , pointcall_rank_dedieu, navigo_status , pointcall_function, pointcall_action, pointcall_uncertainity, 
pointcall_in_date , pointcall_out_date
from navigoviz.pointcall p 
where p.pointcall_function = 'T' and p.source_suite='la Santé registre de patentes de Marseille' 
and substring(p.pointcall_rank_dedieu , 0, strpos( p.pointcall_rank_dedieu, '.'))::int > 4;
-- recherche de cas illogiques sur pointcall_in_date
1787>01>02!

PU-RS
Saint Louis	0014748N

select source_doc_id, source_suite, ship_id, pointcall_function, navigo_status, pointcall_rankfull, pointcall_out_date, outdate_fixed , pointcall_in_date , indate_fixed 
from navigoviz.pointcall 
where pointcall_function = 'T' and navigo_status='PC-RS'  and not position ('=' in coalesce (pointcall_out_date, pointcall_in_date ) ) > 0



select source_doc_id , ship_name, ship_id, captain_name , pointcall, pointcall_rank_dedieu, navigo_status , pointcall_function, pointcall_action, pointcall_uncertainity, 
pointcall_in_date , pointcall_out_date
from navigoviz.pointcall p 
where ship_id = '0014815N'
order by pointcall_rankfull ;

--  0008150N

update navigoviz.pointcall set pointcall_uncertainity = 0, 
            certitude='Observé', certitude_reason = 'rules without ship_id: PC-RS, PU-RS, or (PC-RF and function = O)'
            where (ship_id is  null or pointcall_uncertainity=-4)
            and (navigo_status in ('PC-RS', 'PC-RS1', 'PU-RS', 'PC-ML') or (navigo_status in ('PC-RF', 'PG-RF') and pointcall_function ='O'))
-- 42409 : ca corrige comme il faut : important pour des trajets inférés venant du passé vers le point O dans les congés : vert
update navigoviz.pointcall set pointcall_uncertainity = -2, 
        certitude='Déclaré', certitude_reason = 'rules without ship_id: FC-RS, FC-RF, FG-RF, PG-RF, FA-RF, FC-ML or (PC-RF and function != O)'
        where (ship_id is  null or pointcall_uncertainity=-4)
        and (navigo_status in ('FC-RS', 'FC-RF', 'FG-RF', 'PG-RF', 'FA-RF', 'FC-ML') or (navigo_status='PC-RF' and pointcall_function !='O'))
-- 17641 : ce serait pas la peine... et ca a des effets de bord
        
select ship_id, ship_name , certitude, pointcall_rank_dedieu , pointcall_function , navigo_status 
from navigoviz.pointcall where pointcall_uncertainity = -4
order by navigo_status;
-- PC-RC/FC-RC : on s'en fout car RC
-- FM-RF : ok pour -4 ou -2 dans les congés
-- PC-RF : vient de (rank = 1 et pas O) : ok pour -4 ou -2 dans les congés
-- FC-RT : erreur 0004373N	Saint Clair

------------------------------------------------------------------------------------
-- completion de travels / routepaths
-- V8  : fait en local et sur le server 20/01/2023
------------------------------------------------------------------------------------

        
select source_doc_id , ship_name, ship_id, captain_name , departure, departure_uhgs_id  , destination, destination_uhgs_id , b.travel_rank , geom4326  
from navigoviz.built_travels b
where ship_id = '0015563N' -- 0015563N -- 0003951N
order by  b.travel_rank ;
-- Gerbis	A0000144	Sfax	A0019937
-- Bordeaux	A0180923	Riga	A1010107 et inverse

select ports.portic_points_on_path('A0000144'::text, 'A0019937'::text, 36000.0 :: float, 50000) 
select ports.portic_points_on_path('A0180923'::text, 'A1010107'::text, 36000.0 :: float, 100000) 
-- 3 min 58 et 26 points

-- update navigoviz.uncertainity_travels set pointpath = ports.portic_points_on_path('A0000144' , 'A0019937', 10000.0, 50000) 
--  where pointpath is null;

-------------------
-- Gerbis	A0000144	Sfax	A0019937

insert into  navigoviz.routepaths  
(from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
(select 'A0000144', 'Djerba', 'A0019937', 'Sfax', (c).tampon, (c).distance, (c).pointslist , (c).duration ,(c).nbPoints
from (
    select   
    ports.portic_points_on_path('A0000144'::text, 'A0019937'::text, 5000.0 :: float, 10000) as c
) as calcul)

select  * from navigoviz.routepaths where from_uhgs_id = 'A0000144' and to_uhgs_id = 'A0019937';
select  toponyme , toponyme_standard_fr  from ports.port_points pp  where uhgs_id  in ('A0000144', 'A0019937');
select  toponyme , toponyme_standard_fr  from ports.port_points pp  where uhgs_id  in ('A0180923', 'A1010107');

update navigoviz.built_travels 
set pointpath = r.pointpath, geom = st_makeline(r.pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.pointpath), 4326), 4326),
distance_dep_dest_km = round((st_length(st_makeline(r.pointpath))/1000.0) :: numeric, 3)
from navigoviz.routepaths r
where departure_uhgs_id = 'A0000144' and destination_uhgs_id  = 'A0019937'
and from_uhgs_id=departure_uhgs_id and to_uhgs_id=destination_uhgs_id;

-- et l'inverse

update  navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(r.pointpath)
 where from_uhgs_id = 'A0000144' and to_uhgs_id = 'A0019937';

update navigoviz.built_travels 
set pointpath = ports.portic_array_reverse(r.pointpath), geom = st_makeline(ports.portic_array_reverse(r.pointpath)), geom4326 = st_setsrid(st_transform(st_makeline(ports.portic_array_reverse(r.pointpath)), 4326), 4326),
distance_dep_dest_km = round((st_length(st_makeline(ports.portic_array_reverse(r.pointpath)))/1000.0) :: numeric, 3)
from navigoviz.routepaths r
where departure_uhgs_id = 'A0000144' and destination_uhgs_id  = 'A0019937'
and from_uhgs_id=destination_uhgs_id and to_uhgs_id=departure_uhgs_id;

-------------------
-- Bordeaux	A0180923	Riga	A1010107 et inverse   
update  navigoviz.routepaths r set m_offset= (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select 'A0180923' as departure_uhgs_id, 'A1010107' as destination_uhgs_id, 
    ports.portic_points_on_path('A0180923'::text, 'A1010107'::text, 36000.0 :: float, 100000) as c
) as calcul
where  r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.destination_uhgs_id;
--38 min

select  * from navigoviz.routepaths where from_uhgs_id = 'A0180923' and to_uhgs_id = 'A1010107';
-- ne pas insérer les reverses dans routepaths : si plus simple ! 
update  navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(r.pointpath)
 where from_uhgs_id = 'A0180923' and to_uhgs_id = 'A1010107';

/*
insert into  navigoviz.routepaths  
(from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
(select 'A0180923', 'Bordeaux', 'A1010107', 'Riga', (c).tampon, (c).distance, (c).pointslist , (c).duration ,(c).nbPoints
from (
    select   
    ports.portic_points_on_path('A0180923'::text, 'A1010107'::text, 36000.0 :: float, 100000) as c
) as calcul)
*/

--ICI
update navigoviz.built_travels 
set pointpath = r.pointpath, geom = st_makeline(r.pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.pointpath), 4326), 4326),
distance_dep_dest_km = round((st_length(st_makeline(r.pointpath))/1000.0) :: numeric, 3)
from navigoviz.routepaths r
where departure_uhgs_id = 'A0180923' and destination_uhgs_id  = 'A1010107'
and from_uhgs_id=departure_uhgs_id and to_uhgs_id=destination_uhgs_id;

-- et l'inverse

update navigoviz.built_travels 
set pointpath = ports.portic_array_reverse(r.pointpath), geom = st_makeline(ports.portic_array_reverse(r.pointpath)), geom4326 = st_setsrid(st_transform(st_makeline(ports.portic_array_reverse(r.pointpath)), 4326), 4326),
distance_dep_dest_km = round((st_length(st_makeline(ports.portic_array_reverse(r.pointpath)))/1000.0) :: numeric, 3)
from navigoviz.routepaths r
where departure_uhgs_id = 'A1010107' and destination_uhgs_id  = 'A0180923'
and from_uhgs_id=destination_uhgs_id and to_uhgs_id=departure_uhgs_id;

select * from navigoviz.routepaths where from_uhgs_id = 'A0180923' and to_uhgs_id = 'A1010107';
delete from navigoviz.routepaths where nbpoints = 2 and from_uhgs_id = 'A0180923' and to_uhgs_id = 'A1010107';

select  geom4326  from navigoviz.built_travels where departure_uhgs_id = 'A0180923' and destination_uhgs_id = 'A1010107';

select  geom4326  from navigoviz.built_travels where departure_uhgs_id = 'A0180923' and destination_uhgs_id = 'A1010107';

/*
 * 
 * update  navigoviz.routepaths r set m_offset= (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select distinct departure_uhgs_id, destination_uhgs_id, 
    ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 36000, 50000) as c
	from navigoviz.built_travels t where pointpath is not null
) as calcul
where  r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.destination_uhgs_id
or (r.to_uhgs_id = calcul.departure_uhgs_id and r.from_uhgs_id = calcul.destination_uhgs_id);

 */
------------------------------------------------------------------------------------

/* Test de Correction du pointcall_rankfull  à reporter dans le code */
-- 17 janvier 2023
update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id),  coalesce(p.outdate_fixed, p.indate_fixed), pointcall_rank_dedieu) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, p.pointcall_rank_dedieu , pointcall, navigo_status , pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                -- where source_doc_id = '00188173' -- 00188173 cas extreme
                where ship_id is null
                order by coalesce(ship_id, source_doc_id),  coalesce(p.outdate_fixed, p.indate_fixed),  pointcall_rank_dedieu
            ) as k where p.pkid = k.pkid 

select pointcall_rank_dedieu, pointcall_rankfull, pointcall, pointcall_function  , ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), pointcall_out_date) as test, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                where ship_id = '0014815N'
                -- where source_doc_id = '00314040'
                --order by coalesce(ship_id, source_doc_id), date_dates, source_doc_id , pointcall_function
                order by pointcall_rankfull
-- le 3 et 4 sont inversés. 
                
select source_doc_id, source_suite, ship_id, pointcall_function, navigo_status, pointcall_rankfull, pointcall_out_date, outdate_fixed , pointcall_in_date , indate_fixed 
from navigoviz.pointcall 
where pointcall_function = 'T' and navigo_status='PC-RS'  and not position ('=' in coalesce (pointcall_out_date, pointcall_in_date ) ) > 0

----------------------------------------------------------------
-- filtrer les marchandises à l'arrivée et au départ
----------------------------------------------------------------

            
select jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")')  ::jsonb as all_cargos
from built_travels
select jsonb_path_query_array(destination_all_cargos, '$[*] ? (@.cargo_item_action == "In" ||  @.cargo_item_action == "in")')  ::jsonb as destination_all_cargos
from built_travels
select  all_cargos, destination_all_cargos
from built_travels where style_dashed is true;
-- 14953



select jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")')  ::jsonb as all_cargos, nb_cargo , 
JSONB_ARRAY_LENGTH(all_cargos) as c
from built_travels
where nb_cargo != JSONB_ARRAY_LENGTH(all_cargos)

-- pas de marchandises sur les trajets déduits (les retours)
update built_travels set all_cargos = null, destination_all_cargos = null , nb_cargo =0, destination_nb_cargo =0
 where style_dashed is true;
-- 14950

update built_travels 
set all_cargos = jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Transit" || @.cargo_item_action == "In-out" || @.cargo_item_action == "Loading" || @.cargo_item_action == "OUT" || @.cargo_item_action == "Out" ||  @.cargo_item_action == "out")')  ::jsonb,
destination_all_cargos = jsonb_path_query_array(destination_all_cargos, '$[*] ? (@.cargo_item_action == "Transit" || @.cargo_item_action == "In-out" || @.cargo_item_action == "Unloading" || @.cargo_item_action == "IN" || @.cargo_item_action == "In" ||  @.cargo_item_action == "in")')  ::jsonb
 where style_dashed is false;
-- 56007

update built_travels
set nb_cargo = JSONB_ARRAY_LENGTH(all_cargos) , destination_nb_cargo = JSONB_ARRAY_LENGTH(destination_all_cargos)
 where style_dashed is false;
-- 56007

                    
select ship_id, ship_name, captain_name  from pointcall p where record_id  = '00305306';


select travel_rank, source_suite , source_doc_id , style_dashed,uncertainty_color,departure_en as  departure,departure_uhgs_id,departure_function ,
--departure_latitude,departure_longitude, 
departure_out_date , outdate_fixed, departure_navstatus , destination_in_date, indate_fixed, destination_navstatus ,
destination_en as  destination, destination_uhgs_id,
--destination_latitude,destination_longitude,
captain_name,ship_name
from navigoviz.built_travels 
where source_entry <> 'both-to' and (ship_id = '0012925N' )
order by travel_rank ;

select p.pointcall_rankfull , source_suite , source_doc_id , pointcall ,pointcall_function , net_route_marker , fixed_net_route_marker ,
--departure_latitude,departure_longitude, 
pointcall_out_date , outdate_fixed, navigo_status  , pointcall_in_date, indate_fixed, 
--destination_latitude,destination_longitude,
captain_name,ship_name
from navigoviz.pointcall p  
where (ship_id = '0006678N' )
order by pointcall_rankfull ;

select toponyme_standard_fr , state_1789_fr, substate_1789_fr , belonging_states  from ports.port_points pp where pp.uhgs_id = 'A0354220'

-- Pour l'article, part du G5 sur l'ensemble de la base
select source_suite , count(distinct source_doc_id) from navigoviz.pointcall p 
group by source_suite;
select 43935.0/(119+43935+16027+4942)


select destination_state_1789_fr , destination_substate_1789_fr , count(*) from navigoviz.raw_flows rf 
where source_suite = 'G5' 
-- and destination_admiralty != departure_admiralty and destination_state_1789_fr='France'
group by destination_state_1789_fr, destination_substate_1789_fr

-- 29775 pour la France dont 26080 pour des voyages entre amirautés différentes 
-- 25660 + 4 pour la France hors colonie 

select destination_state_1789_fr , count(*) from navigoviz.raw_flows rf 
where source_suite = 'G5' and destination_admiralty = departure_admiralty 
group by destination_state_1789_fr;
-- 3257

select style_dashed  , count(*) from navigoviz.built_travels bt  
where source_suite = 'G5' 
and destination_admiralty = departure_admiralty 
group by style_dashed;
-- 4968

select 3257 + 26080
-- 29337
select  count(*) from navigoviz.raw_flows rf 
where source_suite = 'G5'  
-- total 42262

select style_dashed  , count(*) from navigoviz.built_travels bt  
group by style_dashed;
false	56036
true	14954


select travel_uncertainity , uncertainty_color  , count(*) from navigoviz.built_travels bt  
group by travel_uncertainity, uncertainty_color;
false	56036
true	14954

select 14954.0/(56036+14954) --21 % de trajets reconstruits
select (56036+14954) --
 
select 26080.0/42262 --61.7%
select 25664.0/42262 --60.7%

