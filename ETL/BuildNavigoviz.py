# -*- coding: utf-8 -*-
'''
Created on 11 may 2020
@author: cplumejeaud
ANR PORTIC : used to build tables for navigoviz : pointcall and travels API
This requires :
1. to have loaded data (pointcall, taxes, cargo) from navigo with LoadFilemaker.py. In schema navigo, navigocheck.
2. to have build a table ports.port_points listing all ports ( using geo_general ) with additional data and manual editing for admiralty, province.
'''

# Comprendre les imports en Python : http://sametmax.com/les-imports-en-python/
# print sys.path
# sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
## pour avoir le path d'un package
## print (psycopg2.__file__)
## C:\Users\cplume01\AppData\Local\Programs\Python\Python37-32\lib\site-packages\psycopg2\__init__.py

from __future__ import nested_scopes
import os, sys



import logging
import configparser
import xlsxwriter
import importlib
import random
from xlrd import open_workbook, cellname, XL_CELL_TEXT, XL_CELL_DATE, XL_CELL_BLANK, XL_CELL_EMPTY, XL_CELL_NUMBER, \
    xldate_as_tuple

from LoadFilemaker import LoadFilemaker





#from ..Check.Pointcall_itineraries import CheckPointcalls
'''p = os.path.abspath('.')
sys.path.insert(1, p)
print(sys.path)'''
import pandas as pd
import pandas.io.sql as sql

class BuildNavigoviz(object):

    def __init__(self, config):
        """
        Ouvre les fichiers de log et une connexion à la base de données (en fonction des paramètres de config)
        """
        ## Ouvrir le fichier de log
        logging.basicConfig(filename=config.get('log', 'file'), level=int(config.get('log', 'level')), filemode='w')
        self.logger = logging.getLogger('BuildNavigoviz')
        self.logger.debug('log file for DEBUG')
        self.logger.info('log file for INFO')
        self.logger.warning('log file for WARNINGS')
        self.logger.error('log file for ERROR')

        self.loader = LoadFilemaker(config)
    def setExtensionsFunctions(self, config):
        """
        Installe les extensions nécessaires à l'import et l'utilisation des données de navigocorpus dans portic, 
        crée les 3 schémas navigo, navigocheck et navigoviz
        et crée les 3 fonctions spécifiques à navigo pour importer les données
        Ces fonctions programmées en PLPython 3 permettent de retirer les crochets et parenthèses des données de navigo 
        pour les conserver épurées dans navigocheck, avec également un code associé à la présence de ces signes
        - 0 : pas de signe
        - -1 : parenthèses autour ()
        - -2 : crochets autour []
        - -3 : donnée manquante
        testé le 14 mai 2020 et le 2 février 2021
        """
        #Prerequis : en tant que postgres, 
        #CREATE ROLE mydba WITH SUPERUSER NOINHERIT;
        #CREATE ROLE navigo WITH CREATEDB LOGIN PASSWORD *****;

        #GRANT mydba TO navigo;

        self.loader.execute_sql('SET ROLE mydba')
        self.loader.execute_sql("create extension if not exists postgis")
        self.loader.execute_sql("create extension if not exists fuzzystrmatch")
        self.loader.execute_sql("create extension if not exists pg_trgm")
        self.loader.execute_sql("create extension if not exists postgis_topology")
        self.loader.execute_sql("create extension if not exists plpython3u")

        self.loader.execute_sql("create schema if not exists navigo")
        self.loader.execute_sql("create schema if not exists navigocheck")
        self.loader.execute_sql("create schema if not exists navigoviz")
        self.loader.execute_sql("create schema if not exists ports")


        self.loader.execute_sql("DROP FUNCTION if exists navigo.test_double_type(tested_value VARCHAR)") 

        query = """CREATE OR REPLACE FUNCTION navigo.test_double_type (tested_value VARCHAR) RETURNS boolean AS 
        $BODY$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::float'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.loader.execute_sql(query)

        self.loader.execute_sql("DROP FUNCTION if exists  navigo.test_int_type(tested_value VARCHAR)") 

        query = """CREATE OR REPLACE FUNCTION navigo.test_int_type (tested_value VARCHAR) RETURNS boolean AS 
        $BODY$
            begin
                EXECUTE 'select '||quote_literal(tested_value)||'::int'; 	
                return true;

            exception when others then 
                raise notice '% %', SQLERRM, SQLSTATE;
                return false;
            end;
        $BODY$ LANGUAGE plpgsql VOLATILE;"""
        self.loader.execute_sql(query)

        self.loader.execute_sql("drop type qual_value cascade")
        query = """CREATE TYPE qual_value AS (
            value   text,
            code  integer
            )"""
        self.loader.execute_sql(query)

        self.loader.execute_sql("DROP FUNCTION if exists navigo.rm_parentheses_crochets(tested_value text)") 

        #Commented 02 fev 2021 : or tested_value.strip().find(']') > 0
        query = """CREATE OR REPLACE FUNCTION navigo.rm_parentheses_crochets (tested_value text) RETURNS qual_value AS 
            $$
                global result
                global code
                if tested_value is not None : 
                    result = tested_value.strip()
                    code = 0
                    if (tested_value.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        code = -1
                    if (tested_value.strip().find('[') == 0) :
                        result = result.replace('[', '').replace(']', '') 
                        code = -2
                    if (result.strip().find('(') == 0):
                        result = result.replace('(', '').replace(')', '') 
                        if code > -1 : 
                            code = -1
                    if (result.strip().find('[') == 0):
                        result = result.replace('[', '').replace(']', '')
                        code = -2 
                    if(len(result.strip()) == 0):
                        code = -4
                        result = None
                else :
                    result = None
                    code = -4
                return [result, code]
            $$ LANGUAGE plpython3u;"""
        self.loader.execute_sql(query)

        self.loader.execute_sql("DROP FUNCTION if exists navigo.pystrip(x text)") 

        query = """CREATE FUNCTION navigo.pystrip(x text)
            RETURNS text
            AS $$
            global x
            x = x.strip()  # ok now
            return x
            $$ LANGUAGE plpython3u;"""
        self.loader.execute_sql(query)


        #4 autres functions sont installées dans ports
        #extract_state_fordate
        #extract_substate_fordate
        #extract_state_en_fordate
        #extract_substate_en_fordate

        self.loader.execute_sql('RESET ROLE')

        #Faire exécuter ce fichier pour avoir toutes les fonctions de calculs de paths en mer
        # porticapi\sql\portic_routepaths_allfunctions.sql


    def buildSourceTable(self, config):
        """
        A partir de la table navigocheck.check_pointcall, 
        cette fonction fabrique une table source qui liste pour chaque data_block_local_id (identifiant du document source)
        - la suite
        - la composante
        de ce document. 
        Le data_block_local_id est commun au n pointcalls (=escales) mentionnés dans le document : 
        "je suis parti de Smyrne pour aller à Marseille en passant par Livourne"

        Utilisation de la liste de component téléchargée de Filemaker
        file_name=C:\\Travail\\Data\\10-Navigo_29sept2022\\liste_sources_description.xlsx
        sheet_name=component_list
        jointure sur pointcall sur le critère 
        pointcall.component_description__component_source = sources.component_source
        
        Modification en février 2023
        pointcall.link_to_source_01 = sources.Link_to_source
        Attention, incroyable, des blancs et des retours à la ligne empêchent la jointure parfaite dans FileMaker
        Solution : 
        regexp_replace('ANF, G5--125-1/Noirmoutier en l'' Ile', E'[\\s\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )

        Les suites utilisées dans Portic
        - Expéditions coloniales Marseille (1789)
        - G5
        - la Santé registre de patentes de Marseille
        - Registre du petit cabotage (1786-1787)

        suite <- portic_suite (renseigné à la main dans le fichier Excel)
        component <- component_description__component_source
        conger_number : null
        other : null
        main_port_uhgs_id : possible, calculé dans la moulinette ci dessous
        main_port_toponyme : possible, calculé dans la moulinette ci dessous
        subset : calculé dans la moulinette ci dessous

        Testée le 11 mai 2020
        Revu le 04 janvier 2023 

        Revu le 21 février 2023 après remaniement de FileMaker qui a fait disparaitre la colonne pointcall.component_description__component_source

        
        """
        self.loader.execute_sql("drop table if exists navigoviz.source cascade");


        query = """create table if not exists navigoviz.source (
            data_block_local_id text , -- primary key avec source (on a parfois dans un même document deux sources différentes, ce qui est une incohérence, oui)
            source text, -- primary key avec source (on a parfois dans un même document deux sources différentes, ce qui est une incohérence, oui)
            link_to_source text,
            suite text,
            component text,
            conge_number integer,
            other text,
            main_port_uhgs_id text,
            main_port_toponyme text,
            subset text
        );"""
        #            documentary_unit_id text , suppression le 22 février 2023
        self.loader.execute_sql(query)
        self.loader.execute_sql("truncate navigoviz.source")
        self.loader.execute_sql("comment on table navigoviz.source is 'Table built by hand by analysing source of pointcall, in order to help to group data by various kind of sources : by SUITE (Marseille or G5), or COMPONENT (i.e. sub-registers) of G5 , mostly reporting a main port';")
        self.loader.execute_sql("comment on column navigoviz.source.data_block_local_id is 'identifier of source entry in navigo ; should be unique (it is not)';")
        #self.loader.execute_sql("comment on column navigoviz.source.documentary_unit_id is 'OLD identifier of source entry in navigo ; should be unique (it is not)';")
        self.loader.execute_sql("comment on column navigoviz.source.source is 'how the reference to the original source was given : suite, component and folio number';")
        self.loader.execute_sql("comment on column navigoviz.source.link_to_source is 'clé étrangère de FileMaker vers la description des sources (composantes et suites)';")
        self.loader.execute_sql("comment on column navigoviz.source.suite is '[G5 (for congés)] or [Registre du petit cabotage (1786-1787)] or [Expéditions coloniales Marseille (1789)] [la Santé registre de patentes de Marseille]'")
        #self.loader.execute_sql("comment on column navigoviz.source.suite is '[G5 (for congés)] or [Registre du petit cabotage (1786-1787)] or [Expéditions \"coloniales Marseille\" (1789)] [Santé Marseille]'")
        self.loader.execute_sql("comment on column navigoviz.source.component is 'G5 has sub-registers, attached to one main port (with some small port sometimes) - component gives the identification of those registers';")
        self.loader.execute_sql("comment on column navigoviz.source.conge_number is 'page, or folio number of the register where the documentary entry should be found';")
        self.loader.execute_sql("comment on column navigoviz.source.other is 'textual page (not parsed as a number) of the register where the documentary entry should be found';")
        self.loader.execute_sql("comment on column navigoviz.source.main_port_uhgs_id is 'navigo identifier of the main port reported by the register (most of the Out comes from this port)';")
        self.loader.execute_sql("comment on column navigoviz.source.main_port_toponyme is 'name of the main port reported by the register (most of the Out comes from this port)';")
        self.loader.execute_sql("comment on column navigoviz.source.subset is 'Sous-ensemble comme cahier du petits cabotage à Marseille.';")

        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ANF, G5-125-1' where link_to_source_01='ANF, G5-125'") 
        #-- 577
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ANF, G5-151B' where link_to_source_01 in ('ANF, G5-151BB-2', 'ANF, G5-151B-2')")
        #--919
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ANF, G5-12-4' where link_to_source_01 in ('ANF, G5-12-4-3')")
        #--14
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200E, 555' where link_to_source_01 in ('ADBR, 200E, 555')")
        #--2
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200E, 535' where link_to_source_01 in ('AdBR, 200E535')")
        #--2

        # Renseigner les vides : sources --> link_to_source_01
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ANF, G5-140' where link_to_source_01 is null and source='ANF, G5-140'") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ANF, G5-154-1' where link_to_source_01 is null and source in ('ANF, G5, 154-1, 8209', 'ANF, G5, 154-1, 1539') ") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ANF, G5-154-2' where link_to_source_01 is null and source in ('ANF, G5-154-2, 1790, Saint Valéry sur Somme') ") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200E, 535' where link_to_source_01 is null and source in ('AdBR, 200E535/0001') ") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200E, 554' where link_to_source_01 is null and source in ('ADBdR, 200E, 5540457') ") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200 E 606' where link_to_source_01 is null and source in ('AdBR,200 E 606') ") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200 E 607' where link_to_source_01 is null and source in ('AdBR,200 E 607') ") 
        self.loader.execute_sql("update navigocheck.check_pointcall set link_to_source_01 = 'ADBdR, 200 E 608' where link_to_source_01 is null and source in ('AdBR, 200 E 608') ") 


        query = """insert into navigoviz.source (data_block_local_id, source, link_to_source)
        (
            select distinct data_block_local_id, 
            regexp_replace(source, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ) as source, 
            regexp_replace(link_to_source_01, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ) as link_to_source01 
            from navigocheck.check_pointcall
        )"""
        self.loader.execute_sql(query)

        '''
        query = """delete from navigoviz."source" s 
        where data_block_local_id in (
            select data_block_local_id from navigoviz."source" s 
                    group by data_block_local_id, source
                    having count(*) > 1
            )
        and s.documentary_unit_id is null;"""
        self.loader.execute_sql(query)
        
        
        query = """delete from navigoviz."source" s 
        where data_block_local_id in ('00149405', '00149406') and source = 'ANF, G5-62/rafraichi';"""
        #  00149228
        self.loader.execute_sql(query)
        '''


        query = """update navigoviz.\"source\" source set suite = portic_suite, component=s.component_source, subset = portic_suite
        from navigo.sources s where regexp_replace(s.link_to_source, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' ) = source.link_to_source
        """
        self.loader.execute_sql(query)

        ## Ceci crée quatre catégories de suite qui sont
        #-- Registre du petit cabotage (1786-1787)
        #-- Expéditions coloniales Marseille (1789)
        #-- la Santé registre de patentes de Marseille
        #-- G5
        
        query = """update navigoviz.source s set main_port_uhgs_id =k.uhgs_id, main_port_toponyme =k.toponyme
            from (
            select q2.component, q2.uhgs_id, q2.toponyme, q1.max from (
            select component, max(c)  from (
                select distinct substring(source from 1 for position('/' in source)) as component, pp.uhgs_id , pp.toponyme , pp.amiraute , pp.province , pp.shiparea, count(*) as c
                from navigocheck.check_pointcall p, ports.port_points  pp
                where p.pointcall_uhgs_id = pp.uhgs_id 
                and (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
                    and substring(source from 1 for position('/' in source)) not like 'G5-%') 
                group by component, pp.uhgs_id, pp.toponyme , pp.amiraute , pp.province , pp.shiparea
                order by component) as k
            group by component
            ) as q1,
            (select distinct substring(source from 1 for position('/' in source)) as component, pp.uhgs_id , pp.toponyme , pp.amiraute , pp.province , pp.shiparea, count(*) as c
                from navigocheck.check_pointcall p, ports.port_points   pp
                where p.pointcall_uhgs_id = pp.uhgs_id 
                and (source like '%G5%' and substring(source from 1 for position('/' in source)) not like 'ANF, G5-91%' 
                    and substring(source from 1 for position('/' in source)) not like 'G5-%') 
                group by component, pp.uhgs_id, pp.toponyme , pp.amiraute , pp.province , pp.shiparea
                order by component
            ) as q2
            where q1.component = q2.component and q2.c = q1.max
            ) as k
            where position('/' in k.component)>0 
            and substring(s.component from 1 for position('/' in k.component)-1) = substring(k.component from 1 for position('/' in k.component)-1)
        """
        
        self.loader.execute_sql(query)

        #self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0135548', main_port_toponyme = 'Saint Valery en Caux' where component = 'ANF, G5, 154-1, 1539'")
        self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0199508', main_port_toponyme = 'La Flotte-en-Ré' where component = 'ANF, G5-91/La Flotte-en-Ré'")
        self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0140266', main_port_toponyme = 'Lorient' where component = 'ADMorbihan, 10B-19/Lorient'")
        self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0147257', main_port_toponyme = 'Saint-Tropez' where component = 'ADVar, 7B10/'")
        self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0207992', main_port_toponyme = 'Talmont' where component = 'ANF, G5-155/'")
        self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0153622', main_port_toponyme = 'Omonville' where component = 'ANF, G5--125-2/Omonville-la-Rogue'")
        self.loader.execute_sql("update navigoviz.\"source\" set main_port_uhgs_id ='A0136403', main_port_toponyme = 'Noirmoutier' where component = 'ANF, G5--125-1/Noirmoutier en l'' Ile'")

        query = """select distinct link_to_source , component  from navigoviz."source" s where s.main_port_uhgs_id is null;
            update navigoviz."source" s set main_port_uhgs_id = 'A0210797', main_port_toponyme = 'Marseille'
            where s.link_to_source like 'ADBdR%' or s.link_to_source = 'Ami Vieux Toulon, MA_11'"""
        self.loader.execute_sql(query)

        query = """select data_block_local_id, source from navigoviz."source" s 
            group by data_block_local_id, source
            having count(data_block_local_id) > 1"""
        rows = self.loader.select_sql(query)
        for rowk in rows:
            self.logger.error('Doublon dans navigoviz.source à traiter (data_block_local_id): '+rowk[0])
            print ('data_block_local_id: ', rowk[0])
        if len(rows) > 0 : 
            exit()    
        
        # Désactivation des corrections (voir au début)
        #print("Fixes : " + config.get('navigoviz', 'source'))
        #self.loader.execute_sql(config.get('navigoviz', 'source'))

        query = """update navigoviz.source set subset = 'Poitou' where subset = 'G5' and suite = 'G5' and main_port_uhgs_id in ('A1964694', 'A0171758', 
        'A0136930', 'A0196496', 'A0198999', 'A0137148', 'A0127055', 'A0133403', 'A0213721', 'A0199508', 'A0148208', 'A0141325', 'A0138533', 'A1964982', 'A0186515', 
        'A0124809', 'A1964767', 'A0172590', 'A0181608',  'A0169240', 'A0165056', 'A1963997', 'A0136403', 'A0195938', 'A0122971',  'A0207992', 'A0165077')"""
        self.loader.execute_sql(query)
        #-- Poitou avec ile de Bouin (A0165077)

        

    def buildCargoTaxes (self, config) : 

        # Externaliser les codages et catégories dans une table à part
        self.loader.execute_sql("drop table IF EXISTS navigoviz.cargo_categories cascade")

        self.loader.loadfile(config, 'cargo_categories')
        #Bouger la table dans navigoviz

        # pkid, record_id commodity_standardized_en commodity_standardized_fr  category_portic_fr category_portic_en portic_marseille
        self.loader.execute_sql("alter table navigo.cargo_categories set schema navigoviz")

        self.loader.execute_sql("alter table navigoviz.cargo_categories add column category_toflit18_Revolution_Empire text")
        self.loader.execute_sql("alter table navigoviz.cargo_categories add column category_toflit18_Revolution_Empire_aggregate text")


        ## Charger les catégories toflit et les faire les jointures pour récupérer pour via les commodity_purpose les categories toflit
        ## source --> orthographic --> simplification --> revolutionempire --> RE_aggregate
        ## See : https://github.com/medialab/toflit18_data/tree/master/base 
        orthographic = "https://raw.githubusercontent.com/medialab/toflit18_data/master/base/classification_product_orthographic.csv"
        simplification =  "https://raw.githubusercontent.com/medialab/toflit18_data/master/base/classification_product_simplification.csv"
        ## Revolution Empire : 
        revolution_empire = "https://raw.githubusercontent.com/medialab/toflit18_data/master/base/classification_product_revolutionempire.csv"
        ## Révolution Empire Agregated : 
        RE_aggregate = "https://raw.githubusercontent.com/medialab/toflit18_data/master/base/classification_product_RE_aggregate.csv"
        	
        import pandas as pd
        import pandas.io.sql as sql
        from sqlalchemy import create_engine

        #Create a query to seek after the toflit'18 data in github
        host = config.get('base', 'host')
        port = config.get('base', 'port')
        dbname = config.get('base', 'dbname')
        user = config.get('base', 'user')
        password = config.get('base', 'password')

        connectstring = f"""postgresql://{user}:{password}@{host}:{port}/{dbname}""" #?searchpath=navigo
        print(connectstring)

        dbschema='navigo' # Searches left-to-right
        engine = create_engine(connectstring) #, connect_args={'options': '-csearch_path={}'.format(dbschema)}
        #engine.execution_options(schema_translate_map={None: "navigo"})

        #meta = MetaData(self.connectable, schema=schema)
        #from sqlalchemy import create_engine, MetaData

        # engine = create_engine(connectstring)
        #meta = MetaData(engine, 'navigo')
        # meta.reflect(engine, schema='navigo')
        # pdsql = pd.io.sql.PandasSQLAlchemy(engine, meta=meta)
        
        self.loader.execute_sql("alter table navigo.cargo add if not exists commodity_purpose_fixed text")
        self.loader.execute_sql("update navigo.cargo set commodity_purpose_fixed = regexp_replace(commodity_purpose, E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )")
        #-- 21 retours à la ligne
        self.loader.execute_sql("update navigo.cargo set commodity_purpose_fixed = regexp_replace(commodity_purpose_fixed, E'[\\u00A0]+', '', 'g' );")
        #-- 189 espaces insécable
        self.loader.execute_sql("update navigo.cargo set commodity_purpose_fixed = regexp_replace(commodity_purpose_fixed, E'[\\u2026]+', '...', 'g' );")
        #-- 0 points de suspension
        self.loader.execute_sql("update navigo.cargo set commodity_purpose_fixed = regexp_replace(commodity_purpose_fixed, '( ){2,}', ' ', 'g' );")
        #-- 22 espaces 
        self.loader.execute_sql("update navigo.cargo set commodity_purpose_fixed = regexp_replace(commodity_purpose_fixed, E'[\\u2019]+', '''', 'g' ) ;")
        #-- 95 apostrophes  


        query = """select distinct commodity_purpose_fixed as commodity_purpose, commodity_id 
                    from navigo.cargo c 
                    where link_to_pointcall in (select  record_id from navigo.pointcall p)
                    and commodity_id is not null
                    order by commodity_id;"""
        commodities = sql.read_sql_query(query, engine) #, params={'schema' : 'navigo'} #, meta=dict(schema='navigo')
        
        first=pd.read_csv(orthographic)
        #Filtrer orthographic sur source = commodity_purpose vu dans cargo
        subset = pd.merge(commodities, first, left_on='commodity_purpose', right_on='source')
        
        second=pd.read_csv(simplification)
        subset2 = pd.merge(subset, second, left_on='orthographic', right_on='orthographic')

        third = pd.read_csv(revolution_empire)
        subset3 = pd.merge(subset2, third, left_on='simplification', right_on='simplification')

        fourth = pd.read_csv(RE_aggregate)
        subset4 = pd.merge(subset3, fourth, left_on='revolutionempire', right_on='revolutionempire')
        subset4.to_sql('cargo_toflit18', con=engine, if_exists='replace')

        query = """
        update navigoviz.cargo_categories c set category_toflit18_Revolution_Empire = k.revolutionempire, category_toflit18_Revolution_Empire_aggregate= k.reagg
            from 
            (select distinct  ct.commodity_id , ct.revolutionempire , ct."RE_aggregate" as reagg from public.cargo_toflit18 ct 
            order by commodity_id) as  k 
            where k.commodity_id = c.record_id ;
        """
        self.loader.execute_sql(query)

        #Add comments for this table
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.pkid is 'Identifiant du produit dans Portic'") 
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.record_id is 'Identifiant du produit dans navigo (lien avec commodity_id dans pointcall)'")
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.commodity_standardized_fr  is 'Nom normé du produit dans Portic, langue : français'") 
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.commodity_standardized_en  is 'Nom normé du produit dans Portic, langue : anglais'") 
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.category_portic_fr is 'Catégorie du produit dans Portic, langue : français'") 
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.category_portic_en is 'Catégorie du produit dans Portic, langue : anglais'")
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.portic_marseille is 'Produit dans le périmètre du datasprint Marseille '")
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.category_toflit18_revolution_empire  is 'Catégorie du produit dans Toflit18, Révolution-Empire, langue : français'")
        self.loader.execute_sql("comment on column navigoviz.cargo_categories.category_toflit18_revolution_empire_aggregate  is 'Catégorie du produit dans Toflit18, Révolution-Empire agrégée, langue : anglais'")


        self.loader.execute_sql("alter table navigocheck.check_cargo add column if not exists first_point__pointcall_name text")
        self.loader.execute_sql("alter table navigocheck.check_cargo add column if not exists first_point__pointcall_uhgs_id text")
        query = """ 
            update navigocheck.check_cargo c 
            set first_point__pointcall_name = p.pointcall_name , first_point__pointcall_uhgs_id=p.pointcall_uhgs_id 
            from navigocheck.check_pointcall p 
            where p.data_block_local_id = c.pointcall__data_block_local_id and p.pointcall_rank = '1.0';
        """
        self.loader.execute_sql(query)

        #Table CARGO
        self.loader.execute_sql("drop table IF EXISTS navigoviz.cargo cascade")
        query = """create table navigoviz.cargo as (
            select  
    			c.link_to_pointcall, 
    			c.pointcall__data_block_local_id ,               
                c.commodity_purpose, c.commodity_id, 
                c.cargo_item_quantity as quantity, c.cargo_item_quantity_u as quantity_u,
                c.cargo_item_action,
                c.cargo_item_owner_name, c.cargo_item_owner_class, c.cargo_item_owner_location,
                c.cargo_item_sending_place,
                psending.record_id as cargo_item_sending_pointcallid,
                psending.pointcall_uhgs_id as cargo_item_sending_place_uhgs_id,
                c.first_point__pointcall_name,
                c.first_point__pointcall_uhgs_id,
                'ORIGINAL' as data_lineage
                from  navigocheck.check_cargo c  
                left join navigocheck.check_pointcall psending on c.pointcall__data_block_local_id = psending.data_block_local_id 
                		and coalesce (c.cargo_item_sending_place, c.first_point__pointcall_name) = psending.pointcall_name 
                        and c.pointcall__pointcall_rank > psending.pointcall_rank 
                        and upper(c.cargo_item_action) = 'IN'
                order by c.pointcall__data_block_local_id, c.pointcall__pointcall_rank
            )
        """
        self.loader.execute_sql(query)

        #On complete en vérifiant les variants toponymiques de sending place.
        query = """ update navigoviz.cargo set cargo_item_sending_place_uhgs_id=pp.uhgs_id 
            from ports.port_points pp 
            where cargo_item_sending_place is not null and cargo_item_sending_pointcallid is null 
            and cargo_item_sending_place = any(pp.toustopos ) """
        self.loader.execute_sql(query)

        self.loader.execute_sql("alter table navigoviz.cargo add column sending_pointcallid_uncertainty int default 0")

        #on prend un port au hasard parmi les noms ressemblants à sending place
        query = """update navigoviz.cargo set cargo_item_sending_place_uhgs_id=pp.uhgs_id , sending_pointcallid_uncertainty=-1
            from ports.port_points pp 
            where cargo_item_sending_place is not null and cargo_item_sending_place_uhgs_id is null 
            and cargo_item_sending_place % pp.toponyme """
        self.loader.execute_sql(query)

        #Rajouter la correspondance avec le pointcall où s'effectue le chargement quand elle manque :  cargo_item_sending_place_pointcallid
        query = """update navigoviz.cargo c set cargo_item_sending_pointcallid = p.record_id 
            from navigocheck.check_pointcall p 
            where cargo_item_sending_pointcallid is null and c.pointcall__data_block_local_id   = p.data_block_local_id 
            and c.cargo_item_sending_place_uhgs_id = p.pointcall_uhgs_id """
        self.loader.execute_sql(query)

        query = """
        insert into navigoviz.cargo 
            (select distinct cargo_item_sending_pointcallid as link_to_pointcall,
                pointcall__data_block_local_id ,               
                commodity_purpose, c.commodity_id, 
                quantity, quantity_u,
                case when upper(cargo_item_action) in ('IN', 'LOADING') then 'OUT' else upper(cargo_item_action) end as cargo_item_action,
                cargo_item_owner_name, cargo_item_owner_class, cargo_item_owner_location,
                cargo_item_sending_place,
                cargo_item_sending_pointcallid,
                cargo_item_sending_place_uhgs_id,
                first_point__pointcall_name,
                first_point__pointcall_uhgs_id,
                'ADDED_PORTIC' as data_lineage                
            from navigoviz.cargo c, 
                (select distinct cargo_item_sending_pointcallid as link_to_pointcall, commodity_id
                from navigoviz.cargo 
                except 
                (select distinct link_to_pointcall, commodity_id
                from navigoviz.cargo)) as filtre
            where c.cargo_item_sending_pointcallid = filtre.link_to_pointcall and c.commodity_id=filtre.commodity_id  
            -- where upper(cargo_item_action) in ('IN', 'LOADING', 'TRANSIT', 'IN-OUT')
            -- where cargo_item_sending_pointcallid||commodity_id  not in (select distinct link_to_pointcall||commodity_id  from cargo)
            )
        """
        self.loader.execute_sql(query)

        self.loader.execute_sql("alter table navigoviz.cargo add column portic boolean default false");
        query = """ update navigoviz.cargo c set portic = true 
            from navigocheck.check_pointcall p
            where c.pointcall__data_block_local_id = p.data_block_local_id 
        """
        self.loader.execute_sql(query)
        self.loader.execute_sql("delete from navigoviz.cargo where portic is false");
        self.loader.execute_sql("alter table navigoviz.cargo drop column portic ");

        # Ajouter les catégories ici car la table sera diffusée par l'API. 
        self.loader.execute_sql("alter table navigoviz.cargo add column commodity_standardized_fr text")
        self.loader.execute_sql("alter table navigoviz.cargo add column commodity_standardized_en text")
        self.loader.execute_sql("alter table navigoviz.cargo add column category_portic_fr text")
        self.loader.execute_sql("alter table navigoviz.cargo add column category_portic_en text")
        self.loader.execute_sql("alter table navigoviz.cargo add column category_toflit18_revolution_empire text")
        self.loader.execute_sql("alter table navigoviz.cargo add column category_toflit18_revolution_empire_aggregate text")

        query = """ update navigoviz.cargo c set 
            commodity_standardized_fr = labels.commodity_standardized_fr,
            commodity_standardized_en = labels.commodity_standardized_en,
            category_portic_fr = labels.category_portic_fr,
            category_portic_en = labels.category_portic_en,
            category_toflit18_revolution_empire = labels.category_toflit18_revolution_empire,
            category_toflit18_revolution_empire_aggregate = labels.category_toflit18_revolution_empire_aggregate
            from navigoviz.cargo_categories labels where labels.record_id = c.commodity_id;
        """
        self.loader.execute_sql(query)

        

        # Table TAXES 
        # # pointcall__data_block_local_id as link_to_pointcall has disappeared in Filemaker janvier 2022
        self.loader.execute_sql("drop table IF EXISTS  navigoviz.pointcall_taxes cascade")
        query = """	create  table navigoviz.pointcall_taxes as (select r.link_to_pointcall, 
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'tax_concept' as tax_concept1,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'payment_date' as payment_date,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q01' as q01_1,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q01_u' as q01_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q02' as q02_1,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q02_u' as q02_u,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q03' as q03_1,
            ((jsonb_agg(to_jsonb(r.*)))::json->>0)::json->>'q03_u' as q03_u,
            jsonb_agg(to_jsonb(r.*) ) as all_taxes,
            ((jsonb_agg(to_jsonb(r.*)))::json->>1)::json->>'tax_concept' as tax_concept2,
            ((jsonb_agg(to_jsonb(r.*)))::json->>1)::json->>'q01' as q01_2,
            ((jsonb_agg(to_jsonb(r.*)))::json->>1)::json->>'q02' as q02_2,
            ((jsonb_agg(to_jsonb(r.*)))::json->>1)::json->>'q03' as q03_2,
            ((jsonb_agg(to_jsonb(r.*)))::json->>2)::json->>'tax_concept' as tax_concept3,
            ((jsonb_agg(to_jsonb(r.*)))::json->>2)::json->>'q01' as q01_3,
            ((jsonb_agg(to_jsonb(r.*)))::json->>2)::json->>'q02' as q02_3,
            ((jsonb_agg(to_jsonb(r.*)))::json->>2)::json->>'q03' as q03_3,
            ((jsonb_agg(to_jsonb(r.*)))::json->>3)::json->>'tax_concept' as tax_concept4,
            ((jsonb_agg(to_jsonb(r.*)))::json->>3)::json->>'q01' as q01_4,
            ((jsonb_agg(to_jsonb(r.*)))::json->>3)::json->>'q02' as q02_4,
            ((jsonb_agg(to_jsonb(r.*)))::json->>3)::json->>'q03' as q03_4,
            ((jsonb_agg(to_jsonb(r.*)))::json->>4)::json->>'tax_concept' as tax_concept5,
            ((jsonb_agg(to_jsonb(r.*)))::json->>4)::json->>'q01' as q01_5,
            ((jsonb_agg(to_jsonb(r.*)))::json->>4)::json->>'q02' as q02_5,
            ((jsonb_agg(to_jsonb(r.*)))::json->>4)::json->>'q03' as q03_5

            from 
                (
                select   link_to_pointcall , c.tax_concept, null as payment_date , c.q01, c.q01_u, c.q02, c.q02_u, c.q03, c.q03_u  
                from navigocheck.check_taxes c
                ) as r	
            group by r.link_to_pointcall
            )       
        """
        self.loader.execute_sql(query)
        

    def buildPointcallTable(self, config):
        """
        La table navigoviz.pointcall est une compilation des données retenues pour la visualisation associée aux informations géographiques.
        Elle est conforme à l'API pointcall et donc contient des informations sur l'amirauté, la province ou le statut du port 
        qui viennent de la table port_points et non présentes à l'origine dans Geo_Général qui ne contient que les latitude, longitude et shiparea
        Elle contient également un attribut calculé, pointcall_rankfull, essentiel pour ensuite calculer des trajectoires : 
        il ordonne les visites de différents pointcalls pour un même bateau suivant  l'ordre chronologique et ceci, toute source confondue.
        
        Testée le 11 mai 2020
        Réactualisé le 8 février 2021
        Puis le 02 avril 2021
        Puis le 04 janvier 2022
        """
        
        
        # Table POINTCALL

        self.loader.execute_sql("drop table if exists navigoviz.pointcall cascade")
        query = """	create table navigoviz.pointcall as (
            select k.identifiant as pkid, 
            k.record_id,
            -- pointcall
            pp.toponyme as pointcall , pp.uhgs_id as pointcall_uhgs_id, 
            pp.toponyme_standard_fr as toponyme_fr, pp.toponyme_standard_en as toponyme_en,
            pp.latitude, pp.longitude, 
            pp.amiraute as pointcall_admiralty, pp.province as pointcall_province, 
            pp.belonging_states as pointcall_states, pp.belonging_substates as pointcall_substates,
            pp.belonging_states_en as pointcall_states_en, pp.belonging_substates_en as pointcall_substates_en,
            pp.state_1789_fr, pp.state_1789_en, pp.substate_1789_fr, pp.substate_1789_en, 
            pp.nb_conges_1787_inputdone, pp.Nb_conges_1787_CR, pp.nb_conges_1789_inputdone, pp.Nb_conges_1789_CR, 
            pp.incertitude_status as pointcall_status_uncertainity,
            pp.status as pointcall_status, pp.shiparea, st_asewkt(pp.point3857) as pointcall_point,  
            pp.ferme_direction, pp.ferme_direction_uncertainty, pp.ferme_bureau, pp.ferme_bureau_uncertainty,
            pp.partner_balance_1789, pp.partner_balance_supp_1789, pp.partner_balance_1789_uncertainty, pp.partner_balance_supp_1789_uncertainty,
            k.pointcall_outdate as pointcall_out_date, 
            k.pointcall_action, 
            -- to_date(k.pointcall_outdate_date, 'DD/MM/YYYY') as outdate_fixed,
            case when k.pointcall_outdate_date!='?' then to_date(k.pointcall_outdate_date, 'DD/MM/YYYY') else to_date(replace(k.pointcall_outdate, '00', '01'), 'YYYY/MM/DD') end as outdate_fixed ,
            k.pointcall_indate as pointcall_in_date, 
            -- to_date(k.pointcall_indate_date, 'DD/MM/YYYY') as indate_fixed,
            case when k.pointcall_indate_date!='?' then to_date(k.pointcall_indate_date, 'DD/MM/YYYY') else to_date(replace(k.pointcall_indate, '00', '01'), 'YYYY/MM/DD') end as indate_fixed ,
            k.data_block_leader_marker,
            k.neat_route_marker as net_route_marker,
            k.pointcall_function,
            k.pointcall_status as navigo_status,
            k.pointcall_rank as pointcall_rank_dedieu,
            -- ship
            k.ship_name, k.ship_id, k.ship_tonnage as tonnage, k.ship_tonnage_u as tonnage_unit, k.ship_flag as flag, k.ship_class as class,
            k.ship_flag_id, k.ship_increw as in_crew,
            -- homeport
            k.ship_homeport as homeport, k.ship_homeport_uhgs_id as homeport_uhgs_id,
            k.toponyme_standard_fr as homeport_toponyme_fr, k.toponyme_standard_en as homeport_toponyme_en,
            k.latitude as homeport_latitude, k.longitude  as homeport_longitude, 
            k.amiraute as homeport_admiralty, k.province as homeport_province, 
            --k.belonging_states as homeport_states, k.belonging_substates as homeport_substates, k.belonging_states_en as homeport_states_en, k.belonging_substates_en as homeport_substates_en,
            k.state_1789_fr as homeport_state_1789_fr, k.state_1789_en as homeport_state_1789_en, k.substate_1789_fr as homeport_substate_1789_fr, k.substate_1789_en as homeport_substate_1789_en,
            --k.nb_conges_1787_inputdone as homeport_nb_conges_1787_inputdone, k.Nb_conges_1787_CR as homeport_Nb_conges_1787_CR, k.nb_conges_1789_inputdone as homeport_nb_conges_1789_inputdone, k.Nb_conges_1789_CR as homeport_Nb_conges_1789_CR, 
            --k.incertitude_status as homeport_status_uncertainity,
            --k.status as homeport_status, k.shiparea as homeport_shiparea, 
            st_asewkt(k.point3857) as homeport_point,
            k.ferme_direction as homeport_ferme_direction, k.ferme_bureau as  homeport_ferme_bureau, k.ferme_bureau_uncertainty as homeport_ferme_bureau_uncertainty,
            k.partner_balance_1789 as homeport_partner_balance_1789, k.partner_balance_supp_1789 as homeport_partner_balance_supp_1789, k.partner_balance_1789_uncertainty as homeport_partner_balance_1789_uncertainty, 
            k.partner_balance_supp_1789_uncertainty as homeport_partner_balance_supp_1789_uncertainty,
            -- source 
            k.doc_id as source_doc_id, k.cpsource as source_text, s.suite as source_suite, s.component as source_component, 
            s.main_port_uhgs_id as source_main_port_uhgs_id, s.main_port_toponyme as source_main_port_toponyme,
            s.subset as source_subset,
            -- captain
            k.captain_local_id as captain_id, k.captain_name, k.captain_birthplace as birthplace, k.captain_birthplace_id as birthplace_uhgs_id,
            k.captain_status as status, k.captain_citizenship as citizenship, k.captain_citizenship_id as citizenship_uhgs_id,
            -- cargo 
            cc.nb_cargo,
            cc.all_cargos,
            -- taxes
            jsonb_strip_nulls(k.all_taxes) as all_taxes,
            k.tax_concept1, k.tax_concept2, k.tax_concept3, k.tax_concept4, k.tax_concept5,
            ((case when k.q01_1 is null then 0 else k.q01_1::float*240 end) + (case when k.q02_1 is null then 0 else k.q02_1::float*12 end) + (case when k.q03_1 is null then 0 else k.q03_1::float end)) as taxe_amount01,
            ((case when k.q01_2 is null then 0 else k.q01_2::float*240 end) + (case when k.q02_2 is null then 0 else k.q02_2::float*12 end) + (case when k.q03_2 is null then 0 else k.q03_2::float end)) as taxe_amount02 ,
            ((case when k.q01_3 is null then 0 else k.q01_3::float*240 end) + (case when k.q02_3 is null then 0 else k.q02_3::float*12 end) + (case when k.q03_3 is null then 0 else k.q03_3::float end))  as taxe_amount03,
            ((case when k.q01_4 is null then 0 else k.q01_4::float*240 end) + (case when k.q02_4 is null then 0 else k.q02_4::float*12 end) + (case when k.q03_4 is null then 0 else k.q03_4::float end))  as taxe_amount04,
            ((case when k.q01_5 is null then 0 else k.q01_5::float*240 end) + (case when k.q02_5 is null then 0 else k.q02_5::float*12 end) + (case when k.q03_5 is null then 0 else k.q03_5::float end)) as taxe_amount05, 
            -- uncertainity
            up.ship_id as ship_uncertainity, up.ship_tonnage as tonnage_uncertainity, up.ship_flag as flag_uncertainity, up.ship_homeport as homeport_uncertainity,
            -1 as pointcall_uncertainity,
            up.captain_name as captain_uncertainity, up.ship_class as shipclass_uncertainity, up.captain_birthplace as birthplace_uncertainity, 
            up.captain_citizenship as citizenship_uncertainity, up.captain_birthplace_id as birthplace_uhgs_id_uncertainity
            
            from 
            (
                (select cp.pkid as identifiant, cp.data_block_local_id as doc_id, regexp_replace(cp."source", E'[\\n\\r\\f\\u000B\\u0085\\u2028\\u2029]+', '', 'g' )  as cpsource,  * 
                    from navigocheck.check_pointcall cp 
                    left outer join navigoviz.pointcall_taxes ct on cp.record_id = ct.link_to_pointcall
                    -- cp.data_block_local_id = ct.link_to_pointcall
                ) as q  
                left outer join ports.port_points ph on q.ship_homeport_uhgs_id = ph.uhgs_id
            ) as k 
            left outer join     
                (
                select r.link_to_pointcall, jsonb_strip_nulls(jsonb_agg(to_jsonb(r.*) )) as all_cargos, jsonb_array_length(jsonb_agg(to_jsonb(r.*) )) as nb_cargo
                from (select * from navigoviz.cargo as c ) as r group by r.link_to_pointcall
                ) as cc
                on cc.link_to_pointcall = k.record_id
            left outer join ports.port_points pp on  pp.uhgs_id = k.pointcall_uhgs_id ,
            navigocheck.uncertainity_pointcall up, navigoviz."source" s 
            where  up.pkid = k.identifiant and   s.data_block_local_id = k.doc_id and s.source = k.cpsource
            -- and pp.uhgs_id = k.pointcall_uhgs_id
            )
        """
        self.loader.execute_sql(query)

        #Spécifier la source_subset 'Poitou_1789' qui inclut l'île de Bouin
        query = """update navigoviz.pointcall p set source_subset = 'Poitou_1789' where source_doc_id in (
            select source_doc_id from navigoviz.pointcall p where p.source_doc_id in (
                select p.source_doc_id from navigoviz.pointcall p where source_subset = 'Poitou' 
                and ( extract(year from outdate_fixed) = 1789 or extract(year from indate_fixed) = 1789)
	        )
        ) """
        self.loader.execute_sql(query)

        self.loader.execute_sql("alter table navigoviz.pointcall add PRIMARY KEY (pkid) ")
        self.loader.execute_sql("alter table  navigoviz.pointcall alter pointcall_point type geometry using pointcall_point::geometry")
        self.loader.execute_sql("CREATE INDEX gist_pointcall_point_idx ON navigoviz.pointcall USING GIST (pointcall_point);")

        ## Calcul de l'incertitude des valeurs cargo et taxes
        self.loader.execute_sql("alter table navigoviz.pointcall add column cargo_uncertainity int default -4")
        self.loader.execute_sql("alter table navigoviz.pointcall add column taxe_uncertainity int default -4")

        query = """
            update navigoviz.pointcall p set cargo_uncertainity = uc.commodity_purpose 
            from navigocheck.uncertainity_cargo uc, navigocheck.check_cargo cc 
            where cc.link_to_pointcall = p.record_id and cc.pkid = uc.pkid;
            """        
            #cc.link_to_pointcall = source_doc_id
        self.loader.execute_sql(query)

        query = """ update navigoviz.pointcall p set taxe_uncertainity = uc.tax_concept 
            from navigocheck.uncertainity_taxes uc, navigocheck.check_taxes cc 
            where cc.link_to_pointcall = p.record_id and cc.pkid = uc.pkid
            """
            #cc.link_to_pointcall = source_doc_id
        self.loader.execute_sql(query)
    

        # Ajouter des colonnes de dates corrigées
        self.loader.execute_sql("alter table navigoviz.pointcall add column  pointcall_in_date2 text")
        self.loader.execute_sql("alter table navigoviz.pointcall add column  pointcall_out_date2 text")

        '''

        -- Manip manuelle  à refaire si réimport  car  1788>11>31 (novembre a 31 jours)
        --00306428	1788>11>31	85279
        --00306976	1788>11>31	85822
        update    navigo.pointcall set  pointcall_indate =   '1788>11>30'     where   record_id in ('00306428', '00306976');
        update    navigocheck.check_pointcall set  pointcall_indate =   '1788>11>30'     where record_id  in ('00306428', '00306976');
        update    navigoviz.pointcall set  pointcall_in_date =   '1788>11>30'     where  record_id in ('00306428', '00306976');-- # 1788>11>31 


        -- 00297588	1759<02<29	76555 car 1759 n'est pas bissextiel : 1759<02<29 est impossible   
        update    navigo.pointcall set  pointcall_indate =   '1759<02<28'     where record_id  = '00297588';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1759<02<28'     where record_id  = '00297588';
        update    navigoviz.pointcall set  pointcall_in_date =   '1759<02<28'     where  record_id in ('00297588');

        -- 00302494	1748=31=12	81393
        update    navigo.pointcall set  pointcall_indate =   '1748=12=31'     where record_id  = '00302494';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1748=12=31'     where record_id  = '00302494';
        update    navigoviz.pointcall set  pointcall_in_date =   '1748=12=31'     where  record_id in ('00302494');

        -- 00302493		1748<31<12!
        update    navigo.pointcall set  pointcall_outdate =   '1748<12<31!'     where record_id  = '00302493';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1748<12<31!'     where record_id  = '00302493';
        update    navigoviz.pointcall set  pointcall_out_date =   '1748<12<31!'     where  record_id in ('00302493');

        -- 00313825	1759=11=31	92616	1759=11=31
        update    navigo.pointcall set  pointcall_indate =   '1759=11=30'     where record_id  = '00313825';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1759=11=30'     where record_id  = '00313825';
        update    navigoviz.pointcall set  pointcall_in_date =   '1759=11=30'     where  record_id in ('00313825');

        -- 00365122	1789>16>07!	139226	1789=16=07
        update    navigo.pointcall set  pointcall_indate =   '1789>07>16!'      where record_id  = '00365122';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1789>07>16!'      where record_id  = '00365122';
        update    navigoviz.pointcall set  pointcall_in_date =   '1789>07>16!'      where  record_id in ('00365122');

        -- 00312165 1749=30=29 
        update    navigo.pointcall set  pointcall_indate =   '1749=06=29'      where record_id  = '00312165';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1749=06=29'      where record_id  = '00312165';
        update    navigoviz.pointcall set  pointcall_in_date =   '1749=06=29'      where  record_id in ('00312165');

        -- 00154851	1787>17>04	35727
        update    navigo.pointcall set  pointcall_indate =   '1787>04>17'      where record_id  = '00154851';
        update    navigocheck.check_pointcall set  pointcall_indate =   '1787>04>17'      where record_id  = '00154851';
        update    navigoviz.pointcall set  pointcall_in_date =   '1787>04>17'      where  record_id in ('00154851');

        -- 00327941	1799=11=[$]	31/10/1799
        update    navigo.pointcall set  pointcall_outdate =   '1799=10=31'     where record_id  = '00327941';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1799=10=31'     where record_id  = '00327941';
        update    navigoviz.pointcall set  pointcall_out_date =   '1799=10=31'     where  record_id in ('00327941');

        -- 00301528	1748=22=10	10/10/1749
        update    navigo.pointcall set  pointcall_outdate =   '1748=10=22'     where record_id  = '00301528';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1748=10=22'     where record_id  = '00301528';
        update    navigoviz.pointcall set  pointcall_out_date =   '1748=10=22'     where  record_id in ('00301528');

        -- 00331147	1779=11=31	01/12/1779
        update    navigo.pointcall set  pointcall_outdate =   '1779=12=01'     where record_id  = '00331147';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1779=12=01'     where record_id  = '00331147';
        update    navigoviz.pointcall set  pointcall_out_date =   '1779=12=01'     where  record_id in ('00331147');

        -- 00297588	1759=02=29	01/03/1759
        update    navigo.pointcall set  pointcall_outdate =   '1759=03=01'     where record_id  = '00297588';
        update    navigocheck.check_pointcall set  pointcall_outdate =   '1759=03=01'     where record_id  = '00297588';
        update    navigoviz.pointcall set  pointcall_out_date =   '1759=03=01'     where  record_id in ('00297588');


        update navigo.pointcall set pointcall_status = 'PC-RS'
        where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
        '00364778', '00364786', '00364788', '00364787', '00364794', '00364796'); 

        update navigocheck.check_pointcall set pointcall_status = 'PC-RS'
        where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
        '00364778', '00364786', '00364788', '00364787', '00364794', '00364796');

        update navigoviz.pointcall set pointcall_status = 'PC-RS'
        where record_id in ('00298656', '00329982', '00364847', '00364848', '00364902', '00364903', '00364904', '00364252',
        '00364778', '00364786', '00364788', '00364787', '00364794', '00364796');


        update navigocheck.check_pointcall set pointcall_status = 'FC-RS', pointcall_rank = '4.0' where record_id = '00364213';
        update navigo.pointcall set pointcall_status = 'FC-RS', pointcall_rank = '4.0' where record_id = '00364213';
        update navigoviz.pointcall set navigo_status = 'FC-RS', pointcall_rank_dedieu = '4.0' where record_id = '00364213';

        '''

    
        #pointcall_in_date2 calculé vaut toujours un jour de moins que le prochain départ (dans l'ordre lexicographique) si <
        # corrigé pour les ! 
        query = """update navigoviz.pointcall 
            set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
                                            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(pointcall_in_date, '!', ''), '>', '=')  else replace(pointcall_in_date, '>', '=')  end 
                                     end 
            where position('00' in pointcall_in_date ) = 0"""
        self.loader.execute_sql(query)
        # corrigé pour les 00
        query = """update navigoviz.pointcall set pointcall_in_date2 = case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(replace(pointcall_in_date, '00', '01'), '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
            else case when position('!' in pointcall_in_date ) > 0 then replace(replace(replace(pointcall_in_date, '00', '01'), '!', ''), '>', '=')  else replace(replace(pointcall_in_date, '00', '01'), '>', '=')  end
            end  where position('00' in pointcall_in_date ) > 0"""
        self.loader.execute_sql(query)

        #indate_fixed valait souvent null (calculé en amont dans la construction de pointcall, mais mal) : 1789>11>31
        query = """update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null"""
        self.loader.execute_sql(query)

        #pointcall_out_date2 calculé vaut toujours un jour de moins que le prochain départ (dans l'ordre lexicographique) si <
        query = """update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(pointcall_out_date, '>', '=') end  
            where position('00' in pointcall_out_date ) = 0 and position('99' in pointcall_out_date ) = 0"""
        self.loader.execute_sql(query)
        # replace 00 par 01
        query = """update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(replace(pointcall_out_date, '00', '01'), '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(replace(pointcall_out_date, '00', '01'), '>', '=') end  
                    where position('00' in pointcall_out_date ) > 0 """
        self.loader.execute_sql(query)
        # correction de 1787<10<99! et 1787<09<99!
        query = """update navigoviz.pointcall set pointcall_out_date2 = replace((replace(replace(replace(pointcall_out_date, '<99', '<28'), '<', '-'), '!', '')::date - 1)::text, '-', '=') 
        where position('<99' in pointcall_out_date ) > 0;"""
        self.loader.execute_sql(query)
        query = """ update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   else replace(pointcall_out_date, '>', '=') end  
            where substring(pointcall_out_date for 4)='1799';"""
        self.loader.execute_sql(query)    

        #6196 outdate_fixed valait null, alors que pointcall_out_date était renseigné
        # attention outdate_fixed était calculé sur pointcall_out_date (à la date exacte connue) et pas pointcall_out_date2 (renseigne meme si date < ou >)
        query = """update navigoviz.pointcall set outdate_fixed= to_date(pointcall_out_date2, 'YYYY=MM=DD')  where pointcall_out_date is not null"""
        self.loader.execute_sql(query)

        ## Rajouter une colonne state_fr et substate_fr renseignée par rapport à la date du pointcall
        self.loader.execute_sql("alter table navigoviz.pointcall add column state_fr text;")
        self.loader.execute_sql("alter table navigoviz.pointcall add column substate_fr text;")
        self.loader.execute_sql("alter table navigoviz.pointcall add column state_en text;")
        self.loader.execute_sql("alter table navigoviz.pointcall add column substate_en text;")

        self.loader.execute_sql("update navigoviz.pointcall set state_fr = ports.extract_state_fordate(pointcall_states, extract (year from coalesce (outdate_fixed , indate_fixed ))::int)")
        #self.loader.execute_sql("update navigoviz.pointcall set state_en = ports.extract_state_en_fordate(pointcall_states_en, extract (year from coalesce (outdate_fixed , indate_fixed ))::int)")
        self.loader.execute_sql("update navigoviz.pointcall set substate_fr = ports.extract_substate_fordate(pointcall_substates, extract (year from coalesce (outdate_fixed , indate_fixed ))::int)")
        self.loader.execute_sql("update navigoviz.pointcall set substate_en = ports.extract_substate_en_fordate(pointcall_substates_en, extract (year from coalesce (outdate_fixed , indate_fixed ))::int)")

        query = """
            update  navigoviz.pointcall p set state_en = e.etat_en 
            from ports.etats e 
            where p.pointcall_uhgs_id = e.uhgs_id 
            and (dfrom is null or dfrom <= extract (year from coalesce (outdate_fixed , indate_fixed ))::int) 
            and (dto is null or extract (year from coalesce (outdate_fixed , indate_fixed ))::int <= dto)"""
        self.loader.execute_sql(query) 

        self.loader.execute_sql("alter table navigoviz.pointcall add column ship_flag_standardized_fr text")
        self.loader.execute_sql("alter table navigoviz.pointcall add column ship_flag_standardized_en text")
        #self.loader.execute_sql("comment on column navigoviz.pointcall.ship_flag_id is 'identifier of the flag'")
        self.loader.execute_sql("update navigoviz.pointcall viz set ship_flag_standardized_fr = p.fr, ship_flag_standardized_en = p.en from ports.labels_lang_csv p where label_type = 'flag' and viz.ship_flag_id = p.key_id")

        ''' # Non, je laisse cela de coté. Il faudrait le faut aussi pour birthplace
        self.loader.execute_sql("alter table navigoviz.pointcall add column citizenship_standardized_fr text")
        self.loader.execute_sql("alter table navigoviz.pointcall add column citizenship_standardized_en text")
        self.loader.execute_sql("update navigoviz.pointcall viz set citizenship_standardized_fr = p.fr, citizenship_standardized_en = p.en from ports.labels_lang_csv p where label_type = 'flag' and viz.citizenship_uhgs_id = p.key_id")
        '''

        self.loader.execute_sql("alter table navigoviz.pointcall add column ship_class_standardized text")
        self.loader.execute_sql("comment on column navigoviz.pointcall.ship_class_standardized is 'Manual standardization of the field \"class\" (ship_class in navigo)'")
        self.loader.execute_sql("update navigoviz.pointcall viz set ship_class_standardized = p.shipclass_standard from navigoviz.ship_class_standardized p where shipclass_source = viz.class")

        #one of [1-20] [21-50] [51-100] [101-200] [201-500] [501 et plus] d.tonnage_unit === "quintaux", on divise par 24.0
        self.loader.execute_sql("alter table navigoviz.pointcall add column tonnage_class text")
        self.loader.execute_sql("comment on column navigoviz.pointcall.in_crew is 'one of [1-20] [21-50] [51-100] [101-200] [201-500] [501 et plus] - si tonnage_unit = quintaux alors division par 24.0'")
        self.loader.execute_sql("update navigoviz.pointcall set tonnage = replace(tonnage, ' ', '')")
        self.loader.execute_sql("update navigoviz.pointcall set tonnage = replace(tonnage, ',', '.')")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[1-20]' where p.tonnage::float <= 20 and p.tonnage_unit <>'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[21-50]' where p.tonnage::float > 20 and p.tonnage::float <= 50 and p.tonnage_unit <>'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[51-100]' where p.tonnage::float > 50 and p.tonnage::float <= 100 and p.tonnage_unit <>'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[101-200]' where p.tonnage::float > 100 and p.tonnage::float <= 200 and p.tonnage_unit <>'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[201-500]' where p.tonnage::float > 200 and p.tonnage::float <= 500 and p.tonnage_unit <>'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[501 et plus]' where p.tonnage::float > 500 and p.tonnage_unit <>'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[1-20]' where p.tonnage::float / 24.0 <= 20 and p.tonnage_unit = 'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[21-50]' where p.tonnage::float / 24.0 > 20 and p.tonnage::float  / 24.0 <= 50 and p.tonnage_unit ='quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[51-100]' where p.tonnage::float / 24.0 > 50 and p.tonnage::float/ 24.0  <= 100 and p.tonnage_unit = 'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[101-200]' where p.tonnage::float / 24.0 > 100 and p.tonnage::float/ 24.0 <= 200 and p.tonnage_unit = 'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[201-500]' where p.tonnage::float / 24.0 > 200 and p.tonnage::float/ 24.0 <= 500 and p.tonnage_unit = 'quintaux'")
        self.loader.execute_sql("update navigoviz.pointcall p set tonnage_class = '[501 et plus]' where p.tonnage::float / 24.0 > 500 and p.tonnage_unit = 'quintaux'")
 
        #Correction manuelle du tonnage
        '''
        update navigoviz.pointcall p set tonnage = null where source_doc_id = '00283632';
        update navigo.pointcall p set ship_tonnage = null where data_block_local_id = '00283632';
        update navigocheck.check_pointcall  p set ship_tonnage = null where data_block_local_id = '00283632';

        update navigoviz.pointcall p set tonnage = 196.5 where source_doc_id = '00148691';
        update navigo.pointcall p set ship_tonnage = 196.5 where data_block_local_id = '00148691';
        update navigocheck.check_pointcall  p set ship_tonnage = 196.5 where data_block_local_id = '00148691';
        '''      

        # Corriger in_crew qui n'est renseigné que sur le pointcall Marseille et pas les escales intermédiaires (sinon, on fausse raw_flows et built_travels)
        query = """update navigoviz.pointcall p1 set in_crew = p2.in_crew  
            from navigoviz.pointcall p2
            where p1.source_doc_id = p2.source_doc_id and p2.in_crew is not null and p1.in_crew is null"""
        self.loader.execute_sql(query)

        # Ajouter des colonnes de dates corrigées date_fixed et pointcall_rankfull (il y a une colonne pointcall_rank dans navigo mais elle est fausse souvent après corrections sur data leader marker A)
        self.loader.execute_sql("alter table navigoviz.pointcall add column if not exists date_fixed date")
        self.loader.execute_sql("alter table navigoviz.pointcall add column if not exists pointcall_rankfull int")

        # pointcall_rankfull traverse les différents doc_id
        # date_fixed = coalesce(p.outdate_fixed, p.indate_fixed) : (outdate si connue sinon indate), de type date
        ## Fix le  19 janvier 2022 : remplacer p.pointcall_out_date - par coalesce(pointcall_in_date, pointcall_out_date) NOn -  par source_doc_id, pointcall_function
        # select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), pointcall_out_date) as pointcall_rankfull, p.pkid,
        # Fixe le 17 janvier 2023 ou pas ??? 
        # qui remplace ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), source_doc_id , pointcall_function
        # par ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), source_doc_id, pointcall_rank_dedieu
        query = """
            update navigoviz.pointcall p set pointcall_rankfull = k.pointcall_rankfull, date_fixed= k.date_dates
            from (
                select ROW_NUMBER () OVER (PARTITION BY ship_id  ORDER by coalesce(ship_id, source_doc_id), coalesce(p.outdate_fixed, p.indate_fixed), source_doc_id, pointcall_rank_dedieu) as pointcall_rankfull, p.pkid,
                p.source_doc_id,  p.ship_id, pointcall_out_date, pointcall_in_date, coalesce(p.outdate_fixed, p.indate_fixed) as date_dates
                from navigoviz.pointcall p 
                --order by coalesce(ship_id, source_doc_id), date_dates, source_doc_id , pointcall_function
            ) as k where p.pkid = k.pkid """
        self.loader.execute_sql(query)

        self.loader.execute_sql(" alter table navigoviz.pointcall add column if not exists pointcall_outdate_uncertainity int default 0 ");


        print('End buildPointcallTable')

    

    def updatePointcallUncertainity(self, config, compute=False):
        if compute:
            tableoutput = config.get('itineraries_output', 'table_name')
            programme = config.get('itineraries_input', 'programme')

            #spec = importlib.util.spec_from_file_location("CheckPointcalls", "D:\\Dev\\portic_humanum\\navigocorpus\\Check\\Pointcall_itineraries.py")
            spec = importlib.util.spec_from_file_location("CheckPointcalls", programme)
            algoZ = importlib.util.module_from_spec(spec)
            sys.modules["CheckPointcalls"] = algoZ
            spec.loader.exec_module(algoZ)
            algo = algoZ.CheckPointcalls(config)
            
            data = algo.loadDataFromDB(config)

            algo.initAlgo(data)

            ## Init the compilation of results
            resultat = pd.DataFrame(columns=algo.data_columns.append(["certitude", "certitude_reason", "verif"]))

            ship_id =  None #'0000388N' # None #'0000684N' #'0000068N' 
            
            while algo.end_seq < data.shape[0] and algo.current_doc_id is None:
                ports = algo.take_a_sequence(data, ship_id)
                
                if ports is not None:
                    etapes = algo.passage1Algo(ports)
                    result = algo.passage2Algo(etapes)
                    result['verif'] = "idem"  #Add a new column verif with values "idem"

                    saveit = False
                    indexNet = result.columns.get_loc("net_route_marker");
                    indexCert = result.columns.get_loc("certitude");
                    indexVerif = result.columns.get_loc("verif");

                    for i in range(0, result.shape[0]) :
                        if (ports.iloc[i, indexNet] is not None and result.iloc[i, indexNet] != (ports.iloc[i, indexNet]).upper()):
                            #print('net_route_marker différent')
                            saveit = True
                            result.iloc[i, indexVerif]='#net_route_marker#'
                        if (result.iloc[i, indexCert] != ports.iloc[i, indexCert]):
                            #print('cert différent')
                            result.iloc[i, indexVerif]=result.iloc[i, indexVerif]+'#certitude#'
                            saveit = True
                    '''if saveit:
                        #print(ports)
                        #print(result)'''
                    resultat = pd.concat([resultat, result], ignore_index=True)
                    '''test = result["net_route_marker"].reset_index(drop=True).compare(ports["net_route_marker"].reset_index(drop=True))
                    #Marche aussi, mais moins précis. Ne renseigne pas verif avec la nature de la différence
                    '''

            verifs = resultat.drop_duplicates(subset=['pkid'])
            #https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_sql.html
            
            verifs.to_sql(tableoutput, con=algo.engine, if_exists='replace', method='multi')#Ecrase la vérif ancienne.
            

            tableinput = config.get('itineraries_input', 'table_name')


            query = """select t.ship_id, t.ship_name, t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as net_route_marker,
            t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.date_fixed,
            t.has_input_done,t.pointcall_admiralty,t.pointcall_province,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,
            t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
            from %s t left join %s v on v.pkid = t.pkid
            order by ship_id, pointcall_rankfull 
            """%(tableinput, tableoutput)

            #print(query)
            #Si l'algo n'a pas vérifié le  net_route_marker on prend la valeur de celui de Silvia : coalesce(v.net_route_marker, t.net_route_marker) as net_route_marker
            # Si Silvia n'a pas vérifié net_route_marker (null), on prend celui de l'algo
            data = sql.read_sql_query(query, algo.engine)

            #Save the results in database : public.pointcall_checked
            finaltableoutput = config.get('itineraries_final_output', 'table_name')
            data.to_sql(finaltableoutput, con=algo.engine, if_exists='replace', method='multi')

            #Save the result in Excel
            algo.outputAnalysis(config, data)
        else : 
            print("skipped pointcall A/Z control and uncertainty computing")

        ## Traiter de l'incertitude des pointcalls à partir des résultats de l'algo : mettre à jour navigoviz.pointcall
        finaltableoutput = config.get('itineraries_final_output', 'table_name')
        if True:
            # couleur verte
            self.loader.execute_sql("update navigoviz.pointcall p set pointcall_uncertainity = 0 from %s c where c.certitude='Observé' and p.pkid=c.pkid"%(finaltableoutput))
            self.loader.execute_sql("update navigoviz.pointcall p set pointcall_uncertainity = -1 from %s c where c.certitude='Confirmé' and p.pkid=c.pkid"%(finaltableoutput))
        
            # couleur grise car déclaration [FC] qu'on ne peut pas vérifier avec les sources disponobles 
            self.loader.execute_sql("update navigoviz.pointcall p set pointcall_uncertainity = -2  from %s c where  c.certitude='Déclaré' and p.pkid=c.pkid"%(finaltableoutput))
            # gris car derniers d'une liste chronologique pour une année de saisie. 
            self.loader.execute_sql("update navigoviz.pointcall p set pointcall_uncertainity = -4 from %s c where  c.certitude is NULL and p.pkid=c.pkid"%(finaltableoutput))
            
            # couleur orange # debat entre algo et silvia - controverse
            self.loader.execute_sql("update navigoviz.pointcall p set pointcall_uncertainity = -3 from "+finaltableoutput+" c where c.verif like '%#net_route_marker%' and p.pkid=c.pkid") 
            
            # couleur rouge : infirmés
            self.loader.execute_sql("update navigoviz.pointcall p set pointcall_uncertainity = -5  from %s c where c.certitude='Infirmé' and p.pkid=c.pkid"%(finaltableoutput))

        #Actualiser la valeur de net_route_marker avec les corrections en créant une nouvelle column fixed_net_route_marker
        self.loader.execute_sql("alter table navigoviz.pointcall add column if not exists fixed_net_route_marker text")
        #Rajouter les columns certitude
        self.loader.execute_sql("alter table navigoviz.pointcall add column if not exists certitude text")
        self.loader.execute_sql("alter table navigoviz.pointcall add column if not exists certitude_reason text")
        if True:
            self.loader.execute_sql("update navigoviz.pointcall p set fixed_net_route_marker = c.net_route_marker from %s c where p.pkid=c.pkid"%(finaltableoutput))
            self.loader.execute_sql("update navigoviz.pointcall p set certitude = c.certitude from %s c where p.pkid=c.pkid"%(finaltableoutput))
            self.loader.execute_sql("update navigoviz.pointcall p set certitude_reason = c.certitude_reason from %s c where p.pkid=c.pkid"%(finaltableoutput))


        # Utililité de pointcall_outdate_uncertainity ???
        self.loader.execute_sql(" alter table navigoviz.pointcall add column if not exists pointcall_outdate_uncertainity int default 0 ");
        self.loader.execute_sql("update navigoviz.pointcall set pointcall_outdate_uncertainity = -1 where source_suite = 'G5'");

        ## Tous les attributs à 0 sauf (pointcall) et sauf si -2 [deduced] ou missing -4
        self.loader.execute_sql(" update navigoviz.pointcall set cargo_uncertainity = 0 where cargo_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set taxe_uncertainity = 0 where taxe_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set captain_uncertainity = 0 where captain_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set flag_uncertainity = 0 where flag_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set homeport_uncertainity = 0 where homeport_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set tonnage_uncertainity = 0 where tonnage_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set ship_uncertainity = 0 where ship_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set shipclass_uncertainity = 0 where shipclass_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set birthplace_uhgs_id_uncertainity = 0 where birthplace_uhgs_id_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set birthplace_uncertainity = 0 where birthplace_uncertainity = -1")
        self.loader.execute_sql(" update navigoviz.pointcall set citizenship_uncertainity = 0 where citizenship_uncertainity = -1")

        # Attention : tous les pointcall sans ship_id sont incontrôlés ou avec un seul document avec ce ship_id (dernier de la liste). 
        # On leur met des valeurs suivant des règles
        query = """update navigoviz.pointcall set pointcall_uncertainity = 0, 
            certitude='Observé', certitude_reason = 'rules without ship_id: PC-RS, PU-RS, or (PC-RF and function = O)'
            where (ship_id is  null or pointcall_uncertainity=-4)
            and (navigo_status in ('PC-RS', 'PC-RS1', 'PU-RS', 'PC-ML') or (navigo_status in ('PC-RF', 'PG-RF') and pointcall_function ='O'))
            """
        self.loader.execute_sql(query)

        '''
        query = """update navigoviz.pointcall set pointcall_uncertainity = -2, 
        certitude='Déclaré', certitude_reason = 'rules without ship_id: FC-RS, FC-RF, FG-RF, PG-RF, FA-RF, FC-ML or (PC-RF and function != O)'
        where (ship_id is  null or pointcall_uncertainity=-4)
        and (navigo_status in ('FC-RS', 'FC-RF', 'FG-RF', 'PG-RF', 'FA-RF', 'FC-ML') or (navigo_status='PC-RF' and pointcall_function !='O'));
        """
        self.loader.execute_sql(query)
        '''

        # Incertitude sur les tonnages
        # Pour réinitialiser en cas de bug
        ''' 
        update navigoviz.pointcall set tonnage_uncertainity = 0 where tonnage_uncertainity = -1

        update navigoviz.pointcall p set tonnage_uncertainity = -2
        from navigocheck.uncertainity_pointcall up 
        where up.pkid = p.pkid and up.ship_tonnage = -2 and p.tonnage_uncertainity = -1
        -- 12
        update navigoviz.pointcall p set tonnage_uncertainity = -4
        from navigocheck.uncertainity_pointcall up 
        where up.pkid = p.pkid and up.ship_tonnage = -4 and p.tonnage_uncertainity = -1
        -- 4
        '''

        # exclure les congés pris à Marennes  (166 cas de navires)
        # Si les tonneaux maximum sont > 20 et que la dispersion > 20 % et que la différence entre min et max excède 5 tonneaux, alors c'est louche
        #Hypothèse : pour les petits tonneaux, de 1 à 20, la confusion sur les chiffres est facile, mais cela reste du petit tonnage.
        query = """update navigoviz.pointcall p set tonnage_uncertainity = -3 from 
        (
            select ship_id, min(tonnage), max(tonnage), avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
            from 
            (
            select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
            from navigoviz.pointcall p
            where tonnage is not null 
            --and (pointcall ='Marennes' or pointcall ='Nantes')
            -- and (pointcall_uhgs_id != 'A0136930' and pointcall_uhgs_id != 'A0124817')
            and (source_main_port_toponyme !='Marennes' )
            ) as k 
            group by ship_id
            having (max(tonnage) - min(tonnage))>5 and stddev(tonnage)/avg(tonnage)*100 > 20 and max(tonnage) > 20
        ) as k 
        where p.ship_id = k.ship_id 
        and tonnage_uncertainity<> -2 and tonnage_uncertainity<> -4"""
        self.loader.execute_sql(query)

        # Comparer les congés de Marennes avec les autres (qui surévaluent en moyenne de 1.5 fois - mail Silvia 15 janvier 2021 - "Vérification de cohérence")
        # si pointcall du datablock leader marker = Marennes , tolérance à la hausse de tonnages + 50% sur es pointcalls 
        # en clair, un navire de 80 tonneaux ailleurs, sera facilement enregistré à  130 tx dans ces deux ports) 
        # et pas dans le sens inverse donc pas +/- mais juste +). Ca serait d'ailleurs intéressant de chiffrer la surévaluation mais au pif, ils ajoutent entre moitié et 2/3)
        # Si Exceptionnels (150 % plus élevés) alors marquer ceux là aussi comme incertains (13 cas de navires)
        query = """update navigoviz.pointcall p set tonnage_uncertainity = -3 from 
        (

            select marennes.ship_id
            from 
                (select ship_id,  avg(tonnage) as moyenneMarennesNantes, max(tonnage) as maxMarennesNantes
                    from 
                    (
                    select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
                    from navigoviz.pointcall p
                    where tonnage is not null 
                    --and (pointcall ='Marennes' or pointcall ='Nantes')
                    -- and (pointcall_uhgs_id = 'A0136930' or pointcall_uhgs_id = 'A0124817')
                    and (source_main_port_toponyme ='Marennes' ) -- or source_main_port_toponyme ='Nantes'
                    ) as k 
                    group by ship_id
                ) as marennes,

                (select ship_id, min(tonnage), max(tonnage) as maxautres, avg(tonnage) as moyenne, stddev(tonnage) as ecarttype, stddev(tonnage)/avg(tonnage)*100 as dispersion
                    from 
                    (
                    select ship_id, pointcall, pointcall_uhgs_id , case when tonnage_unit = 'quintaux' or tonnage_unit = 'quintaux]' then tonnage::float/24 else tonnage::float end
                    from navigoviz.pointcall p
                    where tonnage is not null 
                    --and (pointcall ='Marennes' or pointcall ='Nantes')
                    -- and (pointcall_uhgs_id != 'A0136930' and pointcall_uhgs_id != 'A0124817')
                    and (source_main_port_toponyme !='Marennes' )--and source_main_port_toponyme !='Nantes'
                    ) as k 
                    group by ship_id
                ) as autres
            where autres.ship_id = marennes.ship_id and moyenneMarennesNantes > moyenne*1.50 and maxautres > 20
        ) as k 
        where p.ship_id = k.ship_id and tonnage_uncertainity<> -2 and tonnage_uncertainity<> -4
        """
        self.loader.execute_sql(query)

    def computeUncertainityTravels_WithPathInOption(self, config, computePath=True) : 
        ''' First run the algo Pointcall_itineraries to build a table named navigoviz.pointcall_checked
            where the column net_route_marker is checked by the algorithm, and having a column named certitude
        '''

        ''' Commented on 16 juin 2022
        # 0. Les points manquants
        self.loader.execute_sql("update navigoviz.pointcall set pointcall_uncertainity = -4 where (pointcall_uhgs_id = 'H4444444' or pointcall_uhgs_id = 'H9999999')")


        # 1. pointcall_uncertainity : 
        # # navigo_status = 'PC-RF' and pointcall_function like 'O' '
        # # navigo_status = 'PC-RS' '

        #Par défaut, rien de sûr
        query = """update navigoviz.pointcall p set pointcall_uncertainity = -1 where pointcall_uncertainity<> -4"""
        self.loader.execute_sql(query)

        ## tout ce qui est observé ou sûr / 
        query = """update navigoviz.pointcall p set pointcall_uncertainity = 0 
        where 
            (
                ((navigo_status like 'PC-RF' or navigo_status like 'PG-RF')   and pointcall_function like 'O') 
                or navigo_status like 'PC-RS' 
                or navigo_status like 'PC-ML' 
                or (navigo_status like 'PC-RC' and pointcall_action = 'In-out')
            )
            and pointcall_uncertainity <> -4 ;"""
        self.loader.execute_sql(query)

        #PU : point impossible
        query = """update navigoviz.pointcall p set pointcall_uncertainity = -3 where navigo_status like 'PU-%' 
        and pointcall_uncertainity <> -4 """
        self.loader.execute_sql(query)
        '''

        self.loader.execute_sql("drop table if exists public.uncertainity_travels_historien cascade")
        query="""
            create table public.uncertainity_travels_historien as 
            (
            select  depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as depart_pointcall_uhgs_id, arrivee.pointcall_uhgs_id as arrivee_pointcall_uhgs_id,
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            depart.fixed_net_route_marker as depart_net_route_marker , arrivee.fixed_net_route_marker as arrivee_net_route_marker,
            depart.pointcall_action as departure_action, 
            depart.certitude as departure_certitude,
            depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, 
            arrivee.certitude as destination_certitude,
            arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, 
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            arrivee.pointcall_out_date as arrivee_outdate,

            depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            depart.tonnage_class as depart_tonnage_class, arrivee.tonnage_class as arrivee_tonnage_class,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.fixed_net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            pointcall_uhgs_id, certitude,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, tonnage_class
            from navigoviz.pointcall d
            where   fixed_net_route_marker = 'A' 
            order by d.ship_id, d.pointcall_rankfull) as depart, 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.fixed_net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, 
            pointcall_out_date, pointcall_uhgs_id, certitude,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, tonnage_class
            from navigoviz.pointcall d
            where   fixed_net_route_marker = 'A' 
            order by d.ship_id, d.pointcall_rankfull) as arrivee 
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id and depart.pointcall_uhgs_id<>arrivee.pointcall_uhgs_id
            order by depart.ship_id, depart.id 
        ) -- travel_heloise_historien /// public.uncertainity_travels_historien
        """
        self.loader.execute_sql(query)

        self.loader.execute_sql("drop table if exists public.uncertainity_travels_algo cascade")

        query="""
            create table public.uncertainity_travels_algo as 
            (
            select  depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as depart_pointcall_uhgs_id, arrivee.pointcall_uhgs_id as arrivee_pointcall_uhgs_id,
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            depart.fixed_net_route_marker as depart_net_route_marker , arrivee.fixed_net_route_marker as arrivee_net_route_marker,
            depart.pointcall_action as departure_action, 
            depart.certitude as departure_certitude,
            depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, 
            arrivee.certitude as destination_certitude,
            arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, 
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            arrivee.pointcall_out_date as arrivee_outdate,

            depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            depart.tonnage_class as depart_tonnage_class, arrivee.tonnage_class as arrivee_tonnage_class,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.fixed_net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            pointcall_uhgs_id, certitude,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, tonnage_class
            from navigoviz.pointcall d
            where   net_route_marker != 'Q' 
            order by d.ship_id, d.pointcall_rankfull) as depart, 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.fixed_net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, 
            pointcall_out_date, pointcall_uhgs_id, certitude,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, tonnage_class
            from navigoviz.pointcall d
            where   net_route_marker != 'Q' 
            order by d.ship_id, d.pointcall_rankfull) as arrivee 
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id and depart.pointcall_uhgs_id<>arrivee.pointcall_uhgs_id
            AND (depart.pointcall_uncertainity = -5 OR arrivee.pointcall_uncertainity= -5)
            order by depart.ship_id, depart.id 
        ) -- travel_heloise_algo /// public.uncertainity_travels_algo (les controverses)
        """
        self.loader.execute_sql(query)


        self.loader.execute_sql("drop table if exists navigoviz.uncertainity_travels cascade")

        #ADD ALL FIELDS
        query = """ create table navigoviz.uncertainity_travels as (
                        select row_number() over (PARTITION BY ship_id order by outdate_fixed asc, id asc, destination_uncertainity asc) as travel_rank, * 
                        from 
                            (
                            select * from public.uncertainity_travels_historien
                            union
                            select * from public.uncertainity_travels_algo 
                            where destination_uncertainity = -5 or departure_uncertainity = -5
                            order by ship_id, outdate_fixed, id  
                            ) as k
                    )
            """
        self.loader.execute_sql(query)

        #direct : vrai si trajet annoncé par les sources. faux si déduit par l'algo (infered)
        self.loader.execute_sql("alter table navigoviz.uncertainity_travels add column direct boolean default false;")

        # Amendé le 24.02.2023 car un retour de Marseille vers Ajaccio est alors en train plein
        self.loader.execute_sql("update navigoviz.uncertainity_travels set direct = true where departure_uncertainity = 0 and  depart_pointcall_uhgs_id != 'A0210797';")

        ''' #commented le 10 janvier 2023
        self.loader.execute_sql("""update navigoviz.uncertainity_travels set direct = true where departure_navstatus = 'PC-RS' 
            and source_depart in ('Registre du petit cabotage (1786-1787)', 'la Santé registre de patentes de Marseille', 'Santé Marseille')""")
        '''
        #-- les déclarés passés sont en trait plein et donc tous les trajets arrivants à Marseille sont directs : style_dashed = false 
        # SI ils sont dans le même document   
        query = """update navigoviz.uncertainity_travels set direct = true 
            where (departure_navstatus in ('PC-RS', 'PC-RS1', 'PU-RS', 'PC-ML') or (departure_navstatus='PC-RF' )) 
            and doc_depart=doc_destination
            """
        self.loader.execute_sql(query)
        #-- tous les trajets partants de Marseille sont aussi directs SI ils sont dans le même document
        # les déclarés futurs doivent être en trait plein aussi même s'ils sont incertains : style_dashed = false 
        query = """update navigoviz.uncertainity_travels set direct = true 
                where (destination_navstatus in ('FC-RS', 'FC-RF', 'FG-RF', 'PG-RF', 'FA-RF', 'FC-ML')  )
                and doc_depart=doc_destination
                """
        self.loader.execute_sql(query)

        self.loader.execute_sql("alter table navigoviz.uncertainity_travels add column couleur text default 'grey';")
        self.loader.execute_sql("alter table navigoviz.uncertainity_travels add column travel_uncertainity int default -1;")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set couleur = 'green', travel_uncertainity = 0 where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1); ")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set couleur = 'orange', travel_uncertainity = -2  where departure_uncertainity = -3 or destination_uncertainity =-3;")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set couleur = 'red', travel_uncertainity = -3 where destination_uncertainity = -5 or departure_uncertainity = -5; ")

        ''' ## Commenté le 22 février 2023 pour économiser de la place sur le serveur --> code déplacé dans Built_travels
        ## Fabriquer la géometrie qui contourne les terres (peut prendre du temps)
        self.loader.execute_sql("alter table navigoviz.uncertainity_travels add column pointpath geometry[]");
        self.loader.execute_sql("alter table navigoviz.uncertainity_travels add column geom geometry");

        # De base, aller chercher dans l'ensemble des routes déjà calculées 
        query = """update navigoviz.uncertainity_travels  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = depart_pointcall_uhgs_id and to_uhgs_id=arrivee_pointcall_uhgs_id)
            or (from_uhgs_id = arrivee_pointcall_uhgs_id and to_uhgs_id=depart_pointcall_uhgs_id)
            and r.pointpath is not null"""
        self.loader.execute_sql(query)

        #Insérer les geometries des retours avec les points dans l'ordre inverse 
        #, geom4326 = st_setsrid(st_transform(st_makeline(r.reverse_pointpath), 4326), 4326)
        query = """update navigoviz.uncertainity_travels t set pointpath = r.reverse_pointpath ,  geom = st_makeline(r.reverse_pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = depart_pointcall_uhgs_id  and to_uhgs_id=arrivee_pointcall_uhgs_id)
            and r.pointpath is not null and r.reverse_order is true"""
        self.loader.execute_sql(query)
        '''
        
        '''
        #Fait à la main le 27 juillet 2023
        
        insert into navigoviz.routepaths (from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr)
        select distinct bt.departure_uhgs_id, bt.destination_uhgs_id, bt.departure_fr , bt.destination_fr 
        from navigoviz.built_travels bt 
        where pointpath is null and bt.ship_id is not null  and departure_latitude is not null and bt.destination_latitude  is not null
        -- 1291

        '''

        if (computePath) : 
            #On estime de nouveaux segments ont été introduits, il faut les insérer dans la table navigoviz.routepaths
            # Mais on les calcule d'abord dans navigoviz.uncertainity_travels
            print('Start to compute routes paths !')
            p_offset = 10000 #Distance à la côte
            p_interdistance=50000 # Distance entre les points disposés le long de la route
            query = """update navigoviz.uncertainity_travels set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, %d, %f) 
                        where pointpath is null
                        """%(p_offset, p_interdistance)
            self.loader.execute_sql(query)
            # where ship_id in ('0004171N', '0004155N', '0002931N', '0002537N');
            self.loader.execute_sql(query)
            print('Finished to compute routes paths !')
            # Compléter la table navigoviz.routepaths qui thésaurise les routes calculées si on en a calculés de nouvelles.
            query = """insert into navigoviz.routepaths 
                (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
                select  from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, %d, %f, pointpath, elapseTime, array_length(pointpath, 1)
                from navigoviz.uncertainity_travels u
                where geom is null and pointpath is not null 
                    and (   (from_uhgs_id = depart_pointcall_uhgs_id and to_uhgs_id=arrivee_pointcall_uhgs_id)
                            or (from_uhgs_id = arrivee_pointcall_uhgs_id and to_uhgs_id=depart_pointcall_uhgs_id)
                        )"""%(p_offset, p_interdistance)
            self.loader.execute_sql(query)
            # mettre à jour geom de uncertainity_travels qui marquait par sa nullité les nouveaux cas.
            self.loader.execute_sql("update navigoviz.uncertainity_travels set geom = st_makeline(pointpath) where pointpath is not null")
        else:
            print('On ne calcule pas de nouvelles routes')

        print('End computeUncertainityTravels_WithPathInOption')

    def updateRoutePaths(self, config) : 
            #On estime de nouveaux segments ont été introduits, il faut les insérer dans la table navigoviz.routepaths
            print('Start to compute routes paths !')
            p_offset = 10000 #Distance à la côte
            p_interdistance=50000 # Distance entre les points disposés le long de la route

            i=0
            query = """
                select from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr from navigoviz.routepaths where pointpath is null
            """
            rows = self.loader.select_sql(query)
            ## Boucler sur le résultat
            for r in rows :
                fromUHGS = r[0]
                toUHGS = r[1]
                print('NUMERO DE BOUCLE : '+str(i)+' : '+str(r[2])+' -> '+str(r[3]))
                query = """
                    update  navigoviz.routepaths r set m_offset = (c).tampon, p_distance_ratio = (c).distance,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.portic_points_on_path('%s', '%s', %f, %d) as c
                    ) as calcul
                    where  r.from_uhgs_id = '%s' and r.to_uhgs_id = '%s' ;
                    
                    """%(fromUHGS, toUHGS, p_offset, p_interdistance,  fromUHGS, toUHGS )
                print(query)
                self.logger.debug(str(r[2])+' -> '+str(r[3])+'\n\t' + query)
                #-- or (r.to_uhgs_id = '%s' and r.from_uhgs_id = '%s'); , fromUHGS, toUHGS
                
                #if i%10 == 0 :
                #self.loader.check_and_open_connection(config)
                #print("CONNECTION CLOSED ?" + str(self.loader.postgresconn.closed))
                
                resultat = self.loader.execute_sql(query)
                if resultat <0:
                    #self.loader.check_and_open_connection(config)
                    print("GOT AN ERROR : re-open the connection et retry ")
                    self.loader.open_connection(config)
                    resultat = self.loader.execute_sql(query)
                i=i+1




    

        
    def computeTravels(self, config) :
        """
        Calcule les trajectoires à partir de la table navigoviz.pointcall et navigoviz.uncertainity_travels
        pour créer une table navigoviz.build_travels conforme à l'API

        Testée le 17 juin 2022
        """


        ## BUILT_TRAVELS (avec les trajets inventés) #999
        self.loader.execute_sql("drop table if exists navigoviz.built_travels cascade")
        query = """create table navigoviz.built_travels as (
                    select t.ship_id||'-'||to_char(t.travel_rank, 'FM999') as travel_id, 
                    d.record_id as departure_pointcall_id,
                    a.record_id as destination_pointcall_id,

                    t.departure, 
                    d.toponyme_fr as departure_fr, 
                    d.toponyme_en as departure_en,
                    substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int as departure_rank_dedieu,
                    d.pointcall_uhgs_id  as departure_uhgs_id,
                    d.latitude as departure_latitude,
                    d.longitude as departure_longitude,
                    d.pointcall_admiralty as departure_admiralty,
                    d.pointcall_province as departure_province,
                    d.pointcall_states as departure_states,
                    d.pointcall_substates as departure_substates,
                    d.state_1789_fr as departure_state_1789_fr, 
                    d.substate_1789_fr as departure_substate_1789_fr, 
                    d.state_1789_en as departure_state_1789_en, 
                    d.substate_1789_en as departure_substate_1789_en,
                    d.state_fr as departure_state_fr,
                    d.substate_fr as departure_substate_fr,
                    d.state_en as departure_state_en,
                    d.substate_en as departure_substate_en,
                    d.ferme_direction as departure_ferme_direction, 
                    d.ferme_direction_uncertainty as departure_ferme_direction_uncertainty, 
                    d.ferme_bureau as departure_ferme_bureau, 
                    d.ferme_bureau_uncertainty as departure_ferme_bureau_uncertainty, 
                    d.partner_balance_1789 as departure_partner_balance_1789, 
                    d.partner_balance_supp_1789 as departure_partner_balance_supp_1789, 
                    d.partner_balance_1789_uncertainty as departure_partner_balance_1789_uncertainty, 
                    d.partner_balance_supp_1789_uncertainty as departure_partner_balance_supp_1789_uncertainty,
                    d.shiparea as departure_shiparea,
                    d.pointcall_status as departure_status,
                    -- 26
                    d.nb_conges_1787_inputdone as departure_nb_conges_1787_inputdone, 
                    d.Nb_conges_1787_CR as departure_Nb_conges_1787_CR, 
                    d.nb_conges_1789_inputdone as departure_nb_conges_1789_inputdone, 
                    d.Nb_conges_1789_CR as departure_Nb_conges_1789_CR, 
                    d.pointcall_status_uncertainity as departure_status_uncertainity,
                    d.pointcall_point as departure_point,
                    t.departure_out_date,
                    t.departure_action,
                    t.outdate_fixed,
                    t.departure_navstatus,
                    t.departure_function,

                    t.destination,
                    a.toponyme_fr as destination_fr, 
                    a.toponyme_en as destination_en,
                    substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int as destination_rank_dedieu,
                    a.pointcall_uhgs_id  as destination_uhgs_id,
                    a.latitude as destination_latitude,
                    a.longitude as destination_longitude,
                    a.pointcall_admiralty as destination_admiralty,
                    a.pointcall_province as destination_province,
                    a.pointcall_states as destination_states,
                    a.pointcall_substates as destination_substates,
                    a.state_1789_fr as destination_state_1789_fr, 
                    a.substate_1789_fr as destination_substate_1789_fr, 
                    a.state_1789_en as destination_state_1789_en, 
                    a.substate_1789_en as destination_substate_1789_en,
                    a.state_fr as destination_state_fr,
                    a.substate_fr as destination_substate_fr,
                    a.state_en as destination_state_en,
                    a.substate_en as destination_substate_en,
                    a.ferme_direction as destination_ferme_direction, 
                    a.ferme_direction_uncertainty as destination_ferme_direction_uncertainty, 
                    a.ferme_bureau as destination_ferme_bureau, 
                    a.ferme_bureau_uncertainty as destination_ferme_bureau_uncertainty, 
                    a.partner_balance_1789 as destination_partner_balance_1789, 
                    a.partner_balance_supp_1789 as destination_partner_balance_supp_1789, 
                    a.partner_balance_1789_uncertainty as destination_partner_balance_1789_uncertainty, 
                    a.partner_balance_supp_1789_uncertainty as destination_partner_balance_supp_1789_uncertainty,
                    a.shiparea as destination_shiparea,
                    a.pointcall_status as destination_status,

                    a.nb_conges_1787_inputdone as destination_nb_conges_1787_inputdone, 
                    a.Nb_conges_1787_CR as destination_Nb_conges_1787_CR, 
                    a.nb_conges_1789_inputdone as destination_nb_conges_1789_inputdone, 
                    a.Nb_conges_1789_CR as destination_Nb_conges_1789_CR, 
                    a.pointcall_status_uncertainity as destination_status_uncertainity,
                    a.pointcall_point as destination_point,
                    t.destination_in_date,
                    t.destination_action,
                    t.indate_fixed,
                    t.destination_navstatus,
                    t.destination_function,

                    -- navire
                    d.ship_name,
                    t.ship_id,
                    d.tonnage,
                    d.tonnage_unit,
                    d.tonnage_class,
                    d.in_crew,
                    d.flag,
                    d.ship_flag_id,
                    d.ship_flag_standardized_fr, 
                    d.ship_flag_standardized_en, 
                    d.class,
                    d.ship_class_standardized,

                    d.homeport,
                    d.homeport_uhgs_id , 
                    d.homeport_latitude,
                    d.homeport_longitude,
                    d.homeport_admiralty,
                    d.homeport_province,
                    d.homeport_point,
                    d.homeport_state_1789_fr,
                    d.homeport_substate_1789_fr,
                    d.homeport_state_1789_en,
                    d.homeport_substate_1789_en,
                    d.homeport_toponyme_fr,
                    d.homeport_toponyme_en,

                    -- sources
                    (case when t.doc_destination = doc_depart then 'from' else 'both-from' end) as source_entry, 
                    d.source_doc_id,
                    d.source_text,
                    d.source_suite,
                    d.source_component,
                    d.source_main_port_uhgs_id, 
                    d.source_main_port_toponyme,
                    d.source_subset,

                    -- capitaine
                    d.captain_id,
                    d.captain_name,
                    d.birthplace,
                    d.birthplace_uhgs_id,
                    d.status,
                    d.citizenship,
                    d.citizenship_uhgs_id,

                    -- tous les produits listés au départ sauf si non direct 
                    -- si t.direct is false, ne garder que les In. (On ne sait pas ce qui part)
                    d.nb_cargo,
                    jsonb_strip_nulls(d.all_cargos) as all_cargos,
                                        
                    -- tous les produits de la liste d'arrivée
                    CASE WHEN d.source_suite='G5' and (d.all_cargos @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"')  and a.all_cargos is null 
                    then
                        -- cas de G5 : les In ne sont pas indiqués, seulement les OUT (Out, ou out)
                        -- si plus qu'une escale après le OUT, ou un IN-OUT en escale suivante, je perd la cargaison à l'arrivée
                        d.nb_cargo 
                    else
                        -- cas de Marseille : les In sont indiqués
                        a.nb_cargo 
                    END as destination_nb_cargo,

                    CASE WHEN d.source_suite='G5' and (d.all_cargos @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"') and a.all_cargos is null 
                    then
                        -- cas de G5 : les In ne sont pas indiqués, seulement les OUT
                        -- si plus qu'une escale après le OUT, ou un IN-OUT en escale suivante, je perd la cargaison à l'arrivée
                        -- https://mbork.pl/2020-02-15_PostgreSQL_and_null_values_in_jsonb
                        -- jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")') 
                        -- https://doc.elements-apps.com/elements-connect/6.0/jira-administrators/configuration/field-configuration/field-query-configuration/json-path-explained
                        replace(replace(replace(replace(jsonb_strip_nulls(jsonb_path_query_array(d.all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")') )::text ,'Out', 'IN'), 'out', 'IN'), 'ORIGINAL', 'ADDED_PORTIC_G5'), d.record_id ,a.record_id ) ::jsonb
                    else
                        -- cas de Marseille : les In sont indiqués
                        -- Il faudrait ne garder que les In, in, In-out
                        jsonb_strip_nulls(a.all_cargos) 
                    END as destination_all_cargos,

                   
                    -- Taxes
                    d.tax_concept1, d.tax_concept2, d.tax_concept3, d.tax_concept4, d.tax_concept5,
                    d.taxe_amount01, d.taxe_amount02, d.taxe_amount03, d.taxe_amount04, d.taxe_amount05,
                    d.all_taxes,

                    -- Uncertitude
                    d.ship_uncertainity,
                    d.tonnage_uncertainity ,
                    d.flag_uncertainity,
                    d.homeport_uncertainity ,
                    t.departure_uncertainity,
                    t.destination_uncertainity,
                    d.captain_uncertainity,
                    t.travel_uncertainity,
                    d.cargo_uncertainity,
                    d.taxe_uncertainity, 
                    d.pointcall_outdate_uncertainity,
                    d.shipclass_uncertainity,
                    d.birthplace_uhgs_id_uncertainity,
                    d.birthplace_uncertainity,
                    d.citizenship_uncertainity,
                    
                    -- geometries
                    d.pkid as departure_pkid,
                    a.pkid as destination_pkid,
                    t.travel_rank,                 
                    not(t.direct) as style_dashed,
                    t.couleur as uncertainty_color,
                    -- t.pointpath,
                    -- t.geom,
                    round(((st_distance(d.pointcall_point , a.pointcall_point ))/1000) :: numeric, 3) as orthodromic_distance_dep_dest_km

                from 
                    navigoviz.uncertainity_travels t,
                    navigoviz.pointcall d,
                    navigoviz.pointcall a
                where t.departure_pkid = d.pkid and t.destination_pkid = a.pkid
                order by travel_id,  travel_rank
	
        )"""
        self.loader.execute_sql(query)

        ## Fabriquer la géometrie qui contourne les terres (peut prendre du temps)
        self.loader.execute_sql("alter table navigoviz.built_travels add column pointpath geometry[]");
        self.loader.execute_sql("alter table navigoviz.built_travels add column geom geometry");

        # De base, aller chercher dans l'ensemble des routes déjà calculées 
        query = """update navigoviz.built_travels  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id and to_uhgs_id=destination_uhgs_id)
            and r.pointpath is not null"""
            #-- or (from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id)
        self.loader.execute_sql(query)

        #Insérer les geometries des retours avec les points dans l'ordre inverse 
        #, geom4326 = st_setsrid(st_transform(st_makeline(r.reverse_pointpath), 4326), 4326)
        query = """update navigoviz.built_travels t set pointpath = r.reverse_pointpath ,  geom = st_makeline(r.reverse_pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = destination_uhgs_id  and to_uhgs_id= departure_uhgs_id)
            and r.reverse_pointpath is not null
            """
            #-- and r.pointpath is not null and r.reverse_order is true
        self.loader.execute_sql(query)

        self.loader.execute_sql("alter table  navigoviz.built_travels alter departure_point type geometry using departure_point::geometry")
        self.loader.execute_sql("CREATE INDEX gist_built_travels_depart_point_idx ON navigoviz.built_travels USING GIST (departure_point)")
        self.loader.execute_sql("alter table  navigoviz.built_travels alter destination_point type geometry using destination_point::geometry")
        self.loader.execute_sql("CREATE INDEX gist_built_travels_destination_point_idx ON navigoviz.built_travels USING GIST (destination_point)")
        
        # exploitation de stages pour les distances
        self.loader.execute_sql("alter table navigoviz.built_travels add column distance_dep_dest_miles text")

        query = """update navigoviz.built_travels t set distance_dep_dest_miles = q.distance_dep_dest_miles
            from (
                    select distinct k.travel_id, round(length_miles::float ::float)  as  distance_dep_dest_miles from (
                        SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,   length_miles
                        FROM navigoviz.built_travels t , navigo.stages s 
                        where length_miles is not null and (length is null or length != 'xxx' )
                        and t.departure_uhgs_id = s.st01_id and t.destination_uhgs_id = s.st02_id 
                    ) as k
            ) as q
            where q.travel_id = t.travel_id;"""
        self.loader.execute_sql(query)


        #La géometrie en 4326 (coordonnées lat/long) pour les viz (shiproutes.portic.fr)
        self.loader.execute_sql("alter table navigoviz.built_travels add column geom4326 geometry")
        self.loader.execute_sql("update navigoviz.built_travels set geom4326 = st_setsrid(st_transform(geom, 4326), 4326)")

        # La distance calculée suivant la route maritime de l'algo en km
        self.loader.execute_sql("alter table navigoviz.built_travels add column distance_dep_dest_km float")
        self.loader.execute_sql("update navigoviz.built_travels set distance_dep_dest_km = round((st_length(geom)/1000.0)::numeric,3) where geom is not null")


        self.loader.execute_sql("alter table navigoviz.built_travels add column outvizdate_fixed date")
        self.loader.execute_sql("alter table navigoviz.built_travels add column invizdate_fixed date")
        self.loader.execute_sql("alter table navigoviz.built_travels add column duration int")
        query = """ 
            update navigoviz.built_travels b set duration = t.duration 
            from navigoviz.uncertainity_travels t where t.ship_id = b.ship_id and  t.travel_rank = b.travel_rank """
        self.loader.execute_sql(query)

        self.loader.execute_sql("update navigoviz.built_travels  t set  invizdate_fixed = indate_fixed")
        query = """ 
            update navigoviz.built_travels t set  invizdate_fixed = nextt.indate_fixed - nextt.duration/2
            from navigoviz.built_travels nextt 
            where nextt.ship_id = t.ship_id and nextt.travel_rank = t.travel_rank +1 
            and t.duration = 0
            and position('=' in t.destination_in_date)=0  and position('=' in t.departure_out_date)>0"""
        self.loader.execute_sql(query)

        self.loader.execute_sql("update navigoviz.built_travels t set  outvizdate_fixed = outdate_fixed")
        query = """
            update navigoviz.built_travels t set  outvizdate_fixed = t.indate_fixed - t.duration/2
            from navigoviz.built_travels previous 
            where previous.ship_id = t.ship_id and previous.travel_rank + 1 = t.travel_rank  
            and previous.duration = 0 """
        self.loader.execute_sql(query)

        #shipcaptain_travel_rank ordonne les troncons par ship_id et captain_id
        self.loader.execute_sql("alter table navigoviz.built_travels add column shipcaptain_travel_rank int")
        query = """
            update navigoviz.built_travels b  set shipcaptain_travel_rank = k.shipcaptain_travel_rank
            from (
                select departure_pkid, destination_pkid ,
                captain_id, ship_id , outdate_fixed, travel_rank, travel_uncertainity, departure , destination , travel_rank,
                row_number() over (PARTITION BY ship_id, captain_id order by outdate_fixed asc, travel_rank asc, destination_uncertainity asc) as shipcaptain_travel_rank
                from navigoviz.built_travels ut 
                where source_entry != 'both-to' 
            ) as k
            where b.departure_pkid = k.departure_pkid and b.destination_pkid = k.destination_pkid"""
        self.loader.execute_sql(query)

        #traitement des cargaisons pour filtrer les marchandises
        #au depart : garder les OUT
        #à l'arrivée : garder les IN
        # pas de marchandises sur les trajets déduits (les retours)
        self.loader.execute_sql("update navigoviz.built_travels set all_cargos = null, destination_all_cargos = null, nb_cargo =0, destination_nb_cargo =0 where style_dashed is true")

        query = """update navigoviz.built_travels 
            set all_cargos = jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Transit" || @.cargo_item_action == "In-out" || @.cargo_item_action == "Loading" || @.cargo_item_action == "OUT" || @.cargo_item_action == "Out" ||  @.cargo_item_action == "out")')  ::jsonb,
            destination_all_cargos = jsonb_path_query_array(destination_all_cargos, '$[*] ? (@.cargo_item_action == "Transit" || @.cargo_item_action == "In-out" || @.cargo_item_action == "Unloading" || @.cargo_item_action == "IN" || @.cargo_item_action == "In" ||  @.cargo_item_action == "in")')  ::jsonb
            where style_dashed is false"""
        self.loader.execute_sql(query)

        query = """update navigoviz.built_travels
            set nb_cargo = JSONB_ARRAY_LENGTH(all_cargos) , destination_nb_cargo = JSONB_ARRAY_LENGTH(destination_all_cargos)
            where style_dashed is false"""
        self.loader.execute_sql(query)


    def computeRawFlows(self, config) : 
        """
        Calcule les trajectoires à partir de la table navigoviz.pointcall pour créer une table navigoviz.raw_flows conforme à l'API
        Sans exploiter les ship_id ni les marqueurs A/Z de net_route_marker. On ne calcule donc par les retours des navires (qui font des allers-retours)
        On peut compter un départ de la Rochelle et un départ des Sables d'Olonne pour un même navire parti de LR, faisant une escale à Sables d'Olonne, et arrivé à Nantes
        Nouvellement fondée sur l'usage du pointcall_rank_dedieu, celui qui ordonne la lecture des pointcalls dans un même document
        cette version permet de reconstituer LR -> Sables d'Olonne -> Nantes 

        Testée le 06 july 2021
        Testée le 02 november 2022
        """
        self.loader.execute_sql("drop table if exists navigoviz.raw_flows cascade")
        query = """ create table navigoviz.raw_flows as (
            select  
                ROW_NUMBER () OVER () as travel_id,
                row_number() over (PARTITION BY d.source_doc_id order by d.pointcall_rank_dedieu asc) as travel_rank,
                    d.record_id as departure_pointcall_id,
                    a.record_id as destination_pointcall_id,

                    d.pointcall as departure,
                    d.pkid as departure_pkid,
                    substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int as departure_rank_dedieu,
                    d.toponyme_fr as departure_fr, 
                    d.toponyme_en as departure_en,
                    d.pointcall_uhgs_id  as departure_uhgs_id,
                    d.latitude as departure_latitude,
                    d.longitude as departure_longitude,
                    d.pointcall_admiralty as departure_admiralty,
                    d.pointcall_province as departure_province,
                    d.pointcall_states as departure_states,
                    d.pointcall_substates as departure_substates,
                    d.state_1789_fr as departure_state_1789_fr, 
                    d.substate_1789_fr as departure_substate_1789_fr, 
                    d.state_1789_en as departure_state_1789_en, 
                    d.substate_1789_en as departure_substate_1789_en,
                    d.state_fr as departure_state_fr,
                    d.substate_fr as departure_substate_fr,
                    d.state_en as departure_state_en,
                    d.substate_en as departure_substate_en,                    
                    d.ferme_direction as departure_ferme_direction, 
                    d.ferme_direction_uncertainty as departure_ferme_direction_uncertainty, 
                    d.ferme_bureau as departure_ferme_bureau, 
                    d.ferme_bureau_uncertainty as departure_ferme_bureau_uncertainty, 
                    d.partner_balance_1789 as departure_partner_balance_1789, 
                    d.partner_balance_supp_1789 as departure_partner_balance_supp_1789, 
                    d.partner_balance_1789_uncertainty as departure_partner_balance_1789_uncertainty, 
                    d.partner_balance_supp_1789_uncertainty as departure_partner_balance_supp_1789_uncertainty,
                    d.shiparea as departure_shiparea,
                    d.pointcall_status as departure_status,

                    d.nb_conges_1787_inputdone as departure_nb_conges_1787_inputdone, 
                    d.Nb_conges_1787_CR as departure_Nb_conges_1787_CR, 
                    d.nb_conges_1789_inputdone as departure_nb_conges_1789_inputdone, 
                    d.Nb_conges_1789_CR as departure_Nb_conges_1789_CR, 
                    d.pointcall_status_uncertainity as departure_status_uncertainity,
                    d.pointcall_point as departure_point,
                    d.pointcall_out_date as departure_out_date,
                    d.pointcall_action as departure_action,
                    d.date_fixed as outdate_fixed,
                    d.navigo_status as departure_navstatus,
                    d.pointcall_function as departure_function,

                    a.pointcall as destination,
                    a.pkid as destination_pkid,
                    substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int as destination_rank_dedieu,
                    a.toponyme_fr as destination_fr, 
                    a.toponyme_en as destination_en,
                    a.pointcall_uhgs_id  as destination_uhgs_id,
                    a.latitude as destination_latitude,
                    a.longitude as destination_longitude,
                    a.pointcall_admiralty as destination_admiralty,
                    a.pointcall_province as destination_province,
                    a.pointcall_states as destination_states,
                    a.pointcall_substates as destination_substates,
                    a.state_1789_fr as destination_state_1789_fr, 
                    a.substate_1789_fr as destination_substate_1789_fr, 
                    a.state_1789_en as destination_state_1789_en, 
                    a.substate_1789_en as destination_substate_1789_en,
                    a.state_fr as destination_state_fr,
                    a.substate_fr as destination_substate_fr,
                    a.state_en as destination_state_en,
                    a.substate_en as destination_substate_en,                    
                    a.ferme_direction as destination_ferme_direction, 
                    a.ferme_direction_uncertainty as destination_ferme_direction_uncertainty, 
                    a.ferme_bureau as destination_ferme_bureau, 
                    a.ferme_bureau_uncertainty as destination_ferme_bureau_uncertainty, 
                    a.partner_balance_1789 as destination_partner_balance_1789, 
                    a.partner_balance_supp_1789 as destination_partner_balance_supp_1789, 
                    a.partner_balance_1789_uncertainty as destination_partner_balance_1789_uncertainty, 
                    a.partner_balance_supp_1789_uncertainty as destination_partner_balance_supp_1789_uncertainty,
                    a.shiparea as destination_shiparea,
                    a.pointcall_status as destination_status,
                    -- 
                    a.nb_conges_1787_inputdone as destination_nb_conges_1787_inputdone, 
                    a.Nb_conges_1787_CR as destination_Nb_conges_1787_CR, 
                    a.nb_conges_1789_inputdone as destination_nb_conges_1789_inputdone, 
                    a.Nb_conges_1789_CR as destination_Nb_conges_1789_CR, 
                    a.pointcall_status_uncertainity as destination_status_uncertainity,
                    a.pointcall_point as destination_point,
                    a.pointcall_in_date as destination_in_date,
                    a.pointcall_action as destination_action,
                    a.date_fixed as indate_fixed,
                    a.navigo_status as destination_navstatus,
                    a.pointcall_function as destination_function,

                    d.ship_name,
                    d.ship_id,
                    d.tonnage,
                    d.tonnage_unit,
                    d.tonnage_class,
                    d.in_crew,
                    d.flag,
                    d.ship_flag_id,
                    d.ship_flag_standardized_fr, 
                    d.ship_flag_standardized_en, 
                    d.class,
                    d.ship_class_standardized,

                    d.homeport,
                    d.homeport_uhgs_id , 
                    d.homeport_latitude,
                    d.homeport_longitude,
                    d.homeport_admiralty,
                    d.homeport_province,
                    d.homeport_point,
                    d.homeport_state_1789_fr,
                    d.homeport_substate_1789_fr,
                    d.homeport_state_1789_en,
                    d.homeport_substate_1789_en,
                    d.homeport_toponyme_fr,
                    d.homeport_toponyme_en,

                    'from' as source_entry, 
                    d.source_doc_id,
                    d.source_text,
                    d.source_suite,
                    d.source_component,
                    --d.source_number,
                    --d.source_other,
                    d.source_main_port_uhgs_id, 
                    d.source_main_port_toponyme,
                    d.source_subset,

                    d.captain_id,
                    d.captain_name,
                    d.birthplace,
                    d.birthplace_uhgs_id,
                    d.status,
                    d.citizenship,
                    d.citizenship_uhgs_id,

                    d.nb_cargo,
                    jsonb_strip_nulls(d.all_cargos) as all_cargos,
                    
                    CASE WHEN d.source_suite='G5' and (d.all_cargos @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"')  and a.all_cargos is null 
                    then
                        -- cas de G5 : les In ne sont pas indiqués, seulement les OUT
                        -- si plus qu'une escale après le OUT, ou un IN-OUT en escale suivante, je perd la cargaison à l'arrivée
                        d.nb_cargo 
                    else
                        -- cas de Marseille : les In sont indiqués
                        a.nb_cargo 
                    END as destination_nb_cargo,
                    CASE WHEN d.source_suite='G5' and (d.all_cargos @@ '$.cargo_item_action[*] == "Out" || $.cargo_item_action[*] == "out"') and a.all_cargos is null 
                    then
                        -- cas de G5 : les In ne sont pas indiqués, seulement les OUT
                        -- si plus qu'une escale après le OUT, ou un IN-OUT en escale suivante, je perd la cargaison à l'arrivée
                        -- https://mbork.pl/2020-02-15_PostgreSQL_and_null_values_in_jsonb
                        -- jsonb_path_query_array(all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")') 
                        -- https://doc.elements-apps.com/elements-connect/6.0/jira-administrators/configuration/field-configuration/field-query-configuration/json-path-explained
                        replace(replace(replace(replace(jsonb_strip_nulls(jsonb_path_query_array(d.all_cargos, '$[*] ? (@.cargo_item_action == "Out" ||  @.cargo_item_action == "out")') )::text ,'Out', 'IN'), 'out', 'IN'), 'ORIGINAL', 'ADDED_PORTIC_G5'), d.record_id ,a.record_id ) ::jsonb
                    else
                        -- cas de Marseille : les In sont indiqués
                        jsonb_strip_nulls(a.all_cargos) 
                    END as destination_all_cargos,
                    
                    -- 
                    d.tax_concept1,
                    d.taxe_amount01,
                    d.tax_concept2,
                    d.taxe_amount02,
                    d.tax_concept3,
                    d.taxe_amount03,
                    d.tax_concept4,
                    d.taxe_amount04,
                    d.tax_concept5,
                    d.taxe_amount05,
                    d.all_taxes,
                    
                    d.ship_uncertainity,
                    d.tonnage_uncertainity ,
                    d.flag_uncertainity,
                    d.homeport_uncertainity ,
                    d.pointcall_uncertainity as departure_uncertainity,
                    d.pointcall_uncertainity as destination_uncertainity,
                    d.captain_uncertainity,
                    -1 as travel_uncertainity,
                    d.cargo_uncertainity,
                    d.taxe_uncertainity, 
                    d.pointcall_outdate_uncertainity,
                    d.shipclass_uncertainity,
                    d.birthplace_uhgs_id_uncertainity,
                    d.birthplace_uncertainity,
                    d.citizenship_uncertainity,

                    round(((st_distance(d.pointcall_point , a.pointcall_point ))/1000) :: numeric, 3) as orthodromic_distance_dep_dest_km

            
            from navigoviz.pointcall d , navigoviz.pointcall a 
            where 
                d.source_doc_id  = a.source_doc_id 
     			and (
                    substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int  + 1 = substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int
                    -- faire un bridge dans tous les docs de Marseille si le pointcall arrivée ou depart est inconnu
                /*or (case when d.source_suite != 'G5' and d.pointcall_uhgs_id  in ('H8888888', 'H4444444', 'H9999999', 'A9999997') 
                    and a.pointcall_uhgs_id  not in ('H8888888', 'H4444444', 'H9999999', 'A9999997') 
                    then substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int  + 2 = substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int
                    else true
                    end)
                or (case when a.source_suite != 'G5' and a.pointcall_uhgs_id  in ('H8888888', 'H4444444', 'H9999999', 'A9999997') 
                    and  d.pointcall_uhgs_id  not in ('H8888888', 'H4444444', 'H9999999', 'A9999997') 
                    then substring(d.pointcall_rank_dedieu , 0, strpos( d.pointcall_rank_dedieu, '.'))::int  + 2 = substring(a.pointcall_rank_dedieu , 0, strpos( a.pointcall_rank_dedieu, '.'))::int
                    else true
                    end)*/
                    )
                
                -- faire un bridge dans tous les docs de Marseille si le pointcall arrivée ou depart est inconnu
    			ORDER by source_doc_id,  d.pointcall_rank_dedieu
            )
            
            """
        self.loader.execute_sql(query)

        """#BUG DELETED on 02 november 2022
        (d.pointcall_action in ('Out', 'In-Out', 'Loading', 'Transit', 'In-out', 'Sailing around')) 
		        and (a.pointcall_action in ('In', 'In-Out', 'Unloading', 'Transit', 'In-out')) 
		        AND a.data_block_leader_marker = 'T'
		        and d.source_doc_id  = a.source_doc_id
            ORDER by coalesce(d.ship_id, d.source_doc_id), d.pointcall_rankfull
        """

        self.loader.execute_sql("alter table  navigoviz.raw_flows alter departure_point type geometry using departure_point::geometry")
        self.loader.execute_sql("CREATE INDEX gist_raw_flows_depart_point_idx ON navigoviz.raw_flows USING GIST (departure_point)")
        self.loader.execute_sql("alter table  navigoviz.raw_flows alter destination_point type geometry using destination_point::geometry")
        self.loader.execute_sql("CREATE INDEX gist_raw_flows_destination_point_idx ON navigoviz.raw_flows USING GIST (destination_point)")
        
        self.loader.execute_sql("alter table navigoviz.raw_flows add column distance_dep_dest_miles text");

        query = """update navigoviz.raw_flows t set distance_dep_dest_miles = q.distance_dep_dest_miles
            from (
                    select distinct k.travel_id, round(length_miles::float ::float)  as  distance_dep_dest_miles from (
                        SELECT t.travel_id, t.departure, t.departure_uhgs_id, t.destination, t.destination_uhgs_id,   length_miles
                        FROM navigoviz.raw_flows t , navigo.stages s 
                        where length_miles is not null and (length is null or length != 'xxx' )
                        and t.departure_uhgs_id = s.st01_id and t.destination_uhgs_id = s.st02_id 
                    ) as k
            ) as q
            where q.travel_id = t.travel_id;"""
        self.loader.execute_sql(query)


        #Few columns that will be filled in raw_flows (not like in built_travels), that ensure also compatibility with travels API
        self.loader.execute_sql("alter table navigoviz.raw_flows add column style_dashed boolean")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column uncertainty_color text default 'grey';")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column pointpath geometry[]")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column geom geometry")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column geom4326 geometry")

        # raw_flows ne contient pas de trajets déduits par définition : ils sont tous directs 
        # (sauf quand on fabriquera des bridges, plus tard)
        self.loader.execute_sql("update navigoviz.raw_flows set style_dashed = false  ; ")

        self.loader.execute_sql("update navigoviz.raw_flows set uncertainty_color = 'green', travel_uncertainity = 0 where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1); ")
        self.loader.execute_sql("update navigoviz.raw_flows set uncertainty_color = 'orange', travel_uncertainity = -2  where departure_uncertainity = -3 or destination_uncertainity =-3;")
        self.loader.execute_sql("update navigoviz.raw_flows set uncertainty_color = 'red', travel_uncertainity = -3 where destination_uncertainity = -5 or departure_uncertainity = -5; ")

        # Renseigner les géométries
        # De base, aller chercher dans l'ensemble des routes déjà calculées 
        query = """update navigoviz.raw_flows  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.pointpath), 4326), 4326)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id and to_uhgs_id=destination_uhgs_id)
            and r.pointpath is not null"""
        self.loader.execute_sql(query)

        query = """update navigoviz.raw_flows t set pointpath = r.reverse_pointpath ,  geom = st_makeline(r.reverse_pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.reverse_pointpath), 4326), 4326)
            from navigoviz.routepaths r
            where (from_uhgs_id = destination_uhgs_id  and to_uhgs_id= departure_uhgs_id)
            """
        self.loader.execute_sql(query)
        ##and r.pointpath is not null and r.reverse_order is true

        # La distance calculée suivant la route maritime de l'algo en km
        self.loader.execute_sql("alter table navigoviz.raw_flows add column distance_dep_dest_km float")
        self.loader.execute_sql("update navigoviz.raw_flows set distance_dep_dest_km = round((st_length(geom)/1000.0)::numeric, 3) where geom is not null")

        #Des attributs surtout utiles dans built_travels pour la visualisation
        self.loader.execute_sql("alter table navigoviz.raw_flows  add column outvizdate_fixed date")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column invizdate_fixed date")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column shipcaptain_travel_rank int")
        self.loader.execute_sql("alter table navigoviz.raw_flows add column duration int")

        self.loader.execute_sql("update navigoviz.raw_flows t set duration = indate_fixed - outdate_fixed")
        print('End compute raw_flows')

    def createReadOnlyUsers(self, config, role,  password, roles_exists=True):
        """
        Must be run as postgres 
        """
        print('createReadOnlyUsers for :'+config['base']['dbname'])
        print('role :'+role)

        self.logger.debug('createReadOnlyUsers for :'+config['base']['dbname'])
        self.logger.debug('role :'+role)
        
        database = config['base']['dbname']
        
        if not roles_exists : 
            try : 
                self.loader.execute_sql("REVOKE ALL PRIVILEGES ON all tables IN schema navigo, navigocheck, navigoviz, ports , public  FROM "+role+" cascade")
                self.loader.execute_sql("REVOKE ALL PRIVILEGES ON all sequences IN schema navigo, navigocheck, navigoviz,ports , public  FROM "+role+" cascade")
                self.loader.execute_sql("REVOKE ALL PRIVILEGES on schema navigo, navigocheck, navigoviz, ports, public  FROM "+role)
                self.loader.execute_sql("REVOKE ALL PRIVILEGES ON database "+database+"  FROM "+role)
                self.loader.execute_sql("DROP role if exists "+role)
            except {psycopg2.errors.InsufficientPrivilege, psycopg2.errors.DependentObjectsStillExist}: 
                print('REVOKE ALL FAILED :'+role)
                pass

            query = "CREATE ROLE "+role+" NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN"
            if password is not None : 
                self.logger.debug('password :'+password)
                query = query+" PASSWORD '%s'"% (password)
            self.loader.execute_sql(query)
        
            
        self.loader.execute_sql("GRANT USAGE ON SCHEMA navigoviz TO "+role)
        self.loader.execute_sql("GRANT USAGE ON SCHEMA navigocheck TO "+role)
        self.loader.execute_sql("GRANT USAGE ON SCHEMA navigo TO "+role)
        self.loader.execute_sql("GRANT USAGE ON SCHEMA public TO "+role)
        self.loader.execute_sql("GRANT USAGE ON SCHEMA ports TO "+role)
        self.loader.execute_sql("GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO "+role)
        self.loader.execute_sql("grant CONNECT on database "+database+" to "+role)
        self.loader.execute_sql("grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to "+role)

    def addCommentOnColumns(self, api, relation, schema='navigoviz'):
        """
        For adding comment on columns, we use the file stored in the GIT porticapi (it is an export of a Excel file maintained by Christine) : 
            import pandas as pd
            url="https://gitlab.huma-num.fr/api/v4/projects/712/repository/files/porticapi%2Fstatic%2Fdata%2Fapi_portic.csv/raw?ref=master"
            c = pd.read_csv(url,sep = ';',encoding="utf-8")
        This has been changed
        
        """
        import pandas as pd
        
        #APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
        #filename = os.path.join(APP_ROOT, 'api_portic.csv')
        #metadata = pd.read_csv(filename, sep = ';')

        url = "https://gitlab.huma-num.fr/portic/porticapi/-/raw/master/static/data/api_portic.csv"
        #url = "C:\\Travail\\Dev\\portic_humanum\\porticapi\\static\\data\\api_portic.csv" #en cas de bug réseau
        #url = "C:\\Travail\\ULR_owncloud\\ANR_PORTIC\\Data\\api_portic_05042021_utf8.csv"
        #url = "C:\\Travail\\ULR_owncloud\\ANR_PORTIC\\Data\\api_portic_29062022_utf8.csv"
        #url = "C:\\Travail\\ULR_owncloud\\ANR_PORTIC\\Data\\api_portic_13012023_utf8.csv"
        #url = "C:\\Travail\\ULR_owncloud\\ANR_PORTIC\\Data\\api_navigo_20220117_doc_UTF8.csv"
        #url = "C:\\Travail\\Projets\\ANR_PORTIC\\Data\\api_navigo_20230221_doc_UTF8.csv"
        metadata = pd.read_csv(url,sep = ';',encoding="utf-8")
        
        #Header : API;name;shortname;Exemple;Type;Description

        if api is not None  :
            #Filter to retain this API
            metadata = metadata[metadata['API']==api]

        print(metadata.columns)

        for k in metadata['name'].tolist():
            #print(k)
            #print(metadata[metadata['name']==k])
            #comment = str(metadata[metadata['name']==k]['Description'])
            #comment = metadata.loc[metadata.index[metadata['name']==k], 'Description']
            comment = metadata.loc[metadata['name']==k, 'Description'].tolist()[0]
            comment = comment.replace("'", "''")
            #print(comment)
            query = """comment on column %s.%s.%s is '%s'""" % (schema, relation, k, comment)
            #print(query)
            self.loader.execute_sql(query)

    def debug(self, config, relation):
        """
        Exporte le contenu d'une table  dans un fichier excel
        Param relation : le nom de la table à exporter
        Param Config : contient le nom du fichier Excel à créer

        Testée le 11 mai 2020
        """
        print("Print table in a XLSX file")

        # Create a workbook and add a worksheet.
        
        workbook = xlsxwriter.Workbook(config['outputs'][relation])
        print(config['outputs'][relation])
        # add new colour to palette and set RGB colour value
        cell_format_orange = workbook.add_format()
        cell_format_orange.set_bg_color('orange')

        cell_format_yellow = workbook.add_format()
        cell_format_yellow.set_bg_color('yellow')
        
        cell_format_grey = workbook.add_format()
        cell_format_grey.set_bg_color('#F0F0F0')

        worksheet = workbook.add_worksheet(relation)
        
        # select only on specified columns in config file
        columns = []
        cols = ", "
        liste_attributs = config['outputs'][relation+'_columns']
        print(liste_attributs)
        if liste_attributs == '':
            query = """SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE
            TABLE_SCHEMA = 'navigoviz' and TABLE_NAME = '%s' order by ordinal_position""" % (relation)
            rows = self.loader.select_sql(query)
            for r in rows :
                columns.append(r[0])
            print (cols.join(columns))
        else :
            columns = (config['outputs'][relation+'_columns']).split(',')

        excelcol = 0
        index_attribute_ship_id = 0
        index_attribute_source_doc_id = 0
        index_attribute_net_route_marker = 0
        for c in columns :
            if c == 'ship_id' :
                index_attribute_ship_id = excelcol
                print('index_attribute_ship_id : ' + str(index_attribute_ship_id))
            if c == 'source_doc_id' :
                index_attribute_source_doc_id = excelcol
                print('index_attribute_source_doc_id : ' + str(index_attribute_source_doc_id))
            if c == 'net_route_marker':
                index_attribute_net_route_marker = excelcol
                print('index_attribute_net_route_marker : ' + str(index_attribute_net_route_marker))
            worksheet.write(0, excelcol, c)
            excelcol=excelcol+1

        ## récupérer les données (triées par 2 attributs qui doivent être présents dans la selection : ship_id, pointcall_rankfull)
        query = """SELECT %s FROM navigoviz.%s order by ship_id, pointcall_rankfull::int """ % (cols.join(columns), relation)
        if relation == 'built_travels':
            query = """SELECT %s FROM navigoviz.%s order by travel_id """ % (cols.join(columns), relation)
        if relation == 'raw_flows':
            query = """SELECT %s FROM navigoviz.%s order by travel_id """ % (cols.join(columns), relation)

        where = (config['outputs'][relation+'_filter_clause'])
        doc_yellow = config['outputs'][relation+'_docs_depart']  
        if where != '': 
            if relation == 'pointcall' :
                query = """SELECT %s FROM navigoviz.%s where %s order by ship_id, pointcall_rankfull::int """ % (cols.join(columns), relation, where)
                #ship_id in (ship_list) and (source_doc_id in (docs_depart) or source_doc_id (docs_destination))
            if relation == 'built_travels':
                query = """SELECT %s FROM navigoviz.%s where %s order by travel_id """ % (cols.join(columns), relation, where)
            if relation == 'raw_flows':
                query = """SELECT %s FROM navigoviz.%s where %s order by travel_id """ % (cols.join(columns), relation, where)           
        self.logger.info (query)
        rows = self.loader.select_sql(query)
        excelrow = 1
        current_ship = 0
        impair = False
        yellow = False
        yellowZ = False
        for r in rows :
            excelcol = 0
            #print(r)
            for c in r :
                #print(c) 
                data = str(c)
                if c is not None : 
                    if (excelcol == index_attribute_ship_id) : 
                        if data!=current_ship : 
                            current_ship = data 
                            impair = not(impair)
                    if (excelcol == index_attribute_source_doc_id) and data in doc_yellow: 
                        yellow = True
                    elif (excelcol == index_attribute_source_doc_id) and data not in doc_yellow:
                        yellow = False
                    if (excelcol == index_attribute_net_route_marker) : 
                        yellowZ = True
                    if impair:
                        if yellow and yellowZ and data == 'Z': 
                            worksheet.write(excelrow, excelcol, data, cell_format_yellow)
                        else : 
                            worksheet.write(excelrow, excelcol, data, cell_format_grey)
                    else : 
                        if yellow and yellowZ and data == 'Z': 
                            worksheet.write(excelrow, excelcol, data, cell_format_yellow)
                        else : 
                            worksheet.write(excelrow, excelcol, data, cell_format_orange)
                excelcol=excelcol+1
            excelrow=excelrow+1

        workbook.close()

    

if __name__ == '__main__':
    # Passer en parametre le nom du fichier de configuration
    configfile = 'config_loadfilemaker.txt'
    if len(sys.argv) > 1 :
        configfile = sys.argv[1]
    print('USING config in : '+configfile)

    config = configparser.RawConfigParser()
    config.read(configfile)

    print("Fichier de LOGS : " + config.get('log', 'file'))   
    c = BuildNavigoviz(config)
    

    #c.updateRoutePaths(config)

    #c.setExtensionsFunctions(config)
  
    #c.addCommentOnColumns('ports', 'port_points', 'ports')

    #c.buildSourceTable(config)
    
    #c.buildCargoTaxes(config)
    
    #c.buildPointcallTable(config) 
    
    
    #c.updatePointcallUncertainity(config, True) 
    c.debug(config, 'pointcall')
    #c.addCommentOnColumns('pointcalls', 'pointcall')
    

    
    #c.computeUncertainityTravels_WithPathInOption(config, False)
    ## c.updateRoutePaths(config)
    #c.computeTravels(config)
    c.debug(config, 'built_travels')
    #c.addCommentOnColumns('travels', 'built_travels')
    
    
    #c.computeRawFlows(config) 
    c.debug(config, 'raw_flows')
    #c.addCommentOnColumns('travels', 'raw_flows')
    
    
    
    #c.createReadOnlyUsers(config, 'porticapi', 'portic')
    #c.createReadOnlyUsers(config, 'api_user',  'portic')
    
    #c.debug(config, 'pointcall')
    #c.debug(config, 'built_travels')
    #c.debug(config, 'raw_flows')

    #Clôturer la connexion à la base de données
    c.loader.close_connection()

    ## pour le lancer sur le serveur
    # nohup python3 BuildNavigoviz.py > out2.txt &
    