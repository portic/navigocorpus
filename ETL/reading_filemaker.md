
# stages

vue FM : Fichoz_geoassembling / stages_main
fichier Excel : C:\Travail\Data\Navigo_22fev2021\Stages_main.xlsx
5344 lignes / 5349 le 10 janvier



# taxes
vue FM :  taxes_main

# cargo
vue FM :  cargo_main


# pointcall

vue FM pointcalls_data_all_fields

Filtre : 
	- task : fr 
	- Input_date : 1789 ou 1787
	- Output_date : 1789 ou 1787

 k.pointcall_indate as pointcall_in_date, 
 k.pointcall_outdate as pointcall_out_date, 
            
 ### indate_fixed , outdate_fixed vient de navigo (pointcall_indate_date et pointcall_outdate_date)
 to_date(k.pointcall_indate_date, 'DD/MM/YYYY') as indate_fixed,
 to_date(k.pointcall_outdate_date, 'DD/MM/YYYY') as outdate_fixed,
	#### mais bug
 update navigoviz.pointcall set indate_fixed= to_date(pointcall_in_date2, 'YYYY=MM=DD') where pointcall_in_date is not null
 
 ### pointcall_in_date2 est calculé à partir de pointcall_in_date, en remplacant les < par jour-1, et sinon =
update navigoviz.pointcall set pointcall_in_date2 = 
	case when position('<' in pointcall_in_date ) > 0 then replace((replace(replace(replace(replace(pointcall_in_date, '<', '-'), '!', ''),'=', '-'),'>', '-')::date - 1)::text, '-', '=')  
        else case when position('!' in pointcall_in_date ) > 0 then replace(replace(pointcall_in_date, '!', ''), '>', '=')  else replace(pointcall_in_date, '>', '=')  end 
	end 
 where position('00' in pointcall_in_date ) = 0
			
 ###pointcall_out_date2 calculé vaut toujours un jour de moins que le prochain départ (dans l'ordre lexicographique) si <
update navigoviz.pointcall set pointcall_out_date2 = case when position('<' in pointcall_out_date ) > 0 
	then replace((replace(replace(pointcall_out_date, '<', '-'), '!', '')::date - 1)::text, '-', '=')   
	else replace(pointcall_out_date, '>', '=') end  
where position('00' in pointcall_out_date ) = 0 and position('99' in pointcall_out_date ) = 0
			
# Suites et component: pour comprendre

Pointcall_suite_description_all_fields

## G5 

Suite_class : **Tax register**
Suite_id : 00000003
Suite_content : 
	The source is an accounting document of the Amirauté de France. It lists in every French port of the Channel and of the Ocean all outbound ships along with the fees she paid to the Admiralty
	Each port makes an independent series, composed of various ledgers. Each ledger covers one or various years, depending on the traffic of the port. These ledgers are preserved in boxes, each of which contains various ledgers from one or various ports.
	Each ledger makes a component of the suite.
Suite_title : "Congé" registers of the French Admiralty - XVIIIth century

Congés, Dunkerque (1789)
ANF, G5-76/Dunkerque
Tax register


## Marseille

### Santé de Marseille

Suite_class : **Health_Mars**
Suite_id : 00000074
Suite_content : 
	Enregistrement des navires entrants, ports touchés antérieurement, navires rencontrés, cargaisons (incomplet; insistance sur les marchandises susceptibles d'infection)
	- Série: ADBdR, 200E, 474 à 604
	- Dépouillement: ADBdR, 200E, 543, 550, 551
Suite_title : Registre des déclarations de santé des navires entrant dans le port de Marseille (1700-1850)

Component source : **ADBdR, 200 E 550, 551, 541** :  ?
Component source : **ADBdR, 200 E 543** : task = FR_1787
Component source : **ADBdR, 200 E 505, 515, 525, 534, 545, 554, 555** : marseille entre 1749 et 1799

Component source : **ADBdR, 200 E 606**
Component title : Registre du petit cabotage du port de Marseille 02 sept. 1786 - 24 sept. 1787

Component source : **ADBdR, 200 E 607**
Component title : Registre du petit cabotage (1787-1789)

Component source : **ADBdR, 200 E 607**
Component title : Registre du petit cabotage (1789-1790)

Component source : **Amis Vieux Toulon, MA_11/**
Component title : Expéditions "coloniales" Marseille (1789)

### Consulat US de Marseille

Suite_class : **Consular list**
Suite_id : 00000001
Suite_content :
	Consular lists elaborated by the US consulats in the Western Mediterranean Sea from the 1790s, containing a list of all US ship calling at the ports which depended of the various consulates, or lists of ships arriving from the US and entitled to drawbacks.
	- Contains the name of the ship, of the owner or shipper, the tonnage or flag, etc., the date of arrival and the date of departure from the previous port
	- Contains a description of the cargo
	- Mentions the port of departure and eventually the port of destination





